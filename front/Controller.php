<?php

namespace front;
use \Parser, \BuildTimer, \front\models\Di, \Exception;

require_once "Front/LayoutStyle.php";
require_once "Front/LayoutScript.php";
require_once "Lex/Parser.php";
require_once "front/Autoloader.php";
require_once "Bof/Utils.php";

class Controller {
	
	/**
	 * 
	 * @var Di
	 */
	private $di;
	
	protected $request;
	protected $config;
	protected $db;
	
	private $layoutStyles = array();
	private $layoutScripts = array();
	
	private $usedModules = array();
	
	public function __construct() {
		$this->request = \Application::getAppRequest();
		$this->config = \Application::getAppConfigs();
		$this->db = \Application::getAppDbAdapter();
		
		$autoloader = \Zend_Loader_Autoloader::getInstance();
		$autoloader->pushAutoloader(array('\\front\\Autoloader', 'autoload'));
	}
	
	/**
	 * Добавить к странице JS файлы в тег <head>
	 * @param array|string $source - один или несколько URL до файлов с скриптами
	 */
	public function addLayoutScript($source) {
		if(!is_array($source)) {
			$source = array($source);
		}
		/* получим из source только те файлы, которые ещё не были добавлены в layoutScripts */
		$source = array_diff($source, $this->layoutScripts);
		$this->layoutScripts = array_merge($this->layoutScripts, $source);
	
	}
	/**
	 * Добавить один или несколько CSS файлов на страницу в тег <head>
	 * @param array|string $source - один или несколько URL CSS файлов
	 */
	public function addLayoutStyle($source) {
		if(!is_array($source)) {
			$source = array($source);
		}
		/* получим из source только те файлы, которые ещё не были добавлены в layoutSlyles */
		$source = array_diff($source, $this->layoutStyles);
		$this->layoutStyles = array_merge($this->layoutStyles, $source);
	}
	
	public function getLayoutScripts() {
		return $this->layoutScripts ? $this->layoutScripts : array();
	}
	public function getLayoutStyles() {
		return $this->layoutStyles ? $this->layoutStyles : array();
	}
	
	
	public function indexAction() {

		$page = $this->request->getParam("cms_page");
 
		$this->di = Di::getInstance();		
		$this->di->register('db', $this->db);
		$this->di->register("config", $this->config);
		$this->di->register("request", $this->request);
		
		$lex = new Parser();
		$lex->setCallback(array($this, "parse"));
		$this->di->register('lex', $lex);
		
		$page = new \front\models\Page($page);
		
		$this->di->register("page", $page);
		
		/* если страница является разделом, то, она должна передать управление модулю */
		while($page->isSectionPage() && $page->getChildURLParts()) {
			$moduleName = $page->getRouterName();
			/* @var $module FrontModuleAbstract */
			$module = $this->di->getModule($moduleName);
			$childPage = $module->route($page, $this->request);
						
			if($childPage) {
				if(is_callable($childPage)) {
					echo call_user_func($childPage);
					exit();
				} else {
					/* возможно, роут вернёт текущую страницу */
					if($childPage['id'] != $page->getID()) {
						$childPage['_parent'] = $page->asArray();
					}
					$page = new \front\models\Page($childPage);
				}
			} else {
				throw new Exception("Страница не найдена");
			}
		}
		
		/* придется перерегистрировать $page, потому что она могла измениться */
		$this->di->register("page", $page);
		
		$templateName = $page->getTemplateName();
// 		echo $templateName; die;
		if($templateName) {
			
			$template = $this->di->get("Templates")->find($templateName);
			BuildTimer::timing('find-template');
			
			$pageContent = $page->getContent();
			$pageContent = $lex->parse($pageContent, array(					
				'page' => $page->asArray()
			), array($this, 'parse'));
			BuildTimer::timing('parsed-page-content');
			
			/* Если в контенте страницы были модули, устанавливающие хлебные крошки? */
			foreach ($this->getUsedModules() as $moduleName) {
				$module = $this->di->getModule($moduleName);
				if(($breadcrumbs = $module->getBreadcrumbs()) != null) {
					$breadcrumbs = array_merge($page->getBreadcrumbs(), $breadcrumbs);
					$page->setBreadcrumbs($breadcrumbs);
					break;
				}
			}
			
			$page->setContent($pageContent);
			
			$content =  $lex->parse($template, array(
				'page' => $page->asArray() 
			), array($this, 'parse'));
			
			/* есть модули, вызовы которых не указаны явно в шаблонах, но они должны подключаться */ 
			$startupModules = $this->db->fetchAll("SELECT * FROM cms_modules WHERE active = 1 AND startup_load = 1 ORDER BY number DESC");
			if($startupModules) {
				foreach ($startupModules as $module) {
					$moduleName = str_replace("Module_", "", $module['name']);
					if(!in_array($moduleName, $this->getUsedModules())) {						
						$module = $this->di->getModule($moduleName);
						
						//пока подключим хотя бы просто JS и CSS таких модулей
						$this->exportModuleCSS($module);
						$this->exportModuleJS($module);
					}
				}
			}
			
			/*
			 * В процессе обработки контента страницы и шаблона 
			 * некоторые модули, возможно, захотели вставить свои JS и CSS файлы. 
			 */
			$content = $this->addScriptsAndStyles($content);
			
			BuildTimer::timing('parsed-template');
			echo $content;
		} else {
			$pageContent = $page->getContent();
			$pageContent = $lex->parse($pageContent, array(					
				'page' => $page->asArray()
			), array($this, 'parse'));
			BuildTimer::timing('parsed-page-content');
			echo $pageContent;
		}
	}
	
	protected function addScriptsAndStyles($content) {
		
		$jsHTML = "";
		if($this->config->js->minify) {
			/* @var $miniJS \Plugin_MinifyJs */
			$miniJS = $this->di->getPlugin("minifyJs");
			$external = array();
			foreach($this->getLayoutScripts() as $src) {
				/* в minify не добавляются внешние скрипты, имеющие src = //cdn.host. или http://code.google.com/ */
				if(!preg_match('#^(http:)?//#', $src)) {
					$miniJS->addScript($src);
				} else {
					$external[] = $src;
				}
			}
			/* в начале подключаем внешние библиотеки */
			foreach($external as $src) {
				$jsHTML .= '<script src="'.$src.'"></script>';
			}
			
			if($miniJS->getScripts()) {
				$jsHTML .= $miniJS->getHTML();
			}
			
		} else {
			
			foreach($this->getLayoutScripts() as $src) {
				$jsHTML .= '<script src="'.$src.'"></script>'."\n";
			}
		}
		
		$cssHTML = "";
		if($this->config->css->minify) {
			/* @var $miniCSS \Plugin_MinifyCss */
			$miniCSS = $this->di->getPlugin("minifyCss");
			$external = array();
			foreach($this->getLayoutStyles() as $href) {
				if(!preg_match('#^(http:)?//#', $href)) {
					$miniCSS->addStyle($href);
				} else {
					$external[] = $href;
				}
			}
			
			// как правило, сначала подключаются внешние библиотеки */
			$cssHTML = '';
			foreach($external as $href) {
				$cssHTML .= '<link rel="stylesheet" href="'.$href.'" />'."\n";
			}
			
			if($miniCSS->getStyles()) {
				$cssHTML .= $miniCSS->getHTML();
			}
			
		} else {
			foreach($this->getLayoutStyles() as $href) {
				$cssHTML .= '<link rel="stylesheet" href="'.$href.'" />';
			}
		}
		
		$content = str_replace(array('{js_html}', '{css_html}'), array($jsHTML, $cssHTML), $content);
		return $content;
	}
	
	protected function addUsedModule($moduleName) {
		if(!in_array($moduleName, $this->usedModules)) {
			$this->usedModules[] = $moduleName;
		}
	}
	protected function getUsedModules() {
		return $this->usedModules;
	}
	
	public function parse($method, $options, $content) {
		
		$callback = explode(".", $method);
		$pluginName = $callback[0];
		
		if($pluginName == 'module') {
			//module
			$moduleName = isset($callback[1]) ? $callback[1] : null;
			if($moduleName) {
				/* @var $module \front\modules\Module */
				$module = $this->di->getModule($moduleName);
				
				if($module->isActive()) {
					$methodName = isset($callback[2]) ? $callback[2] : null;
					
					$this->exportModuleCSS($module);
					$this->exportModuleJS($module);
					$this->addUsedModule($moduleName);
					
					if($methodName) {		
					
						if(method_exists($module, $methodName)) {
							return call_user_func(array($module, $methodName), $options, $content);
						} else {
							throw new Controller_Exception("Module '$moduleName' has no method '$methodName'");
						}
					}
				} else {
					return '<!--module '.$moduleName.' is not active-->';
				}
			}
		} else {
			//plugin
			$pluginMethodName = isset($callback[1]) ? $callback[1] : lcfirst($callback[0]);
			
			$text = "";
			/* @var $plugin PluginAbstract */
			if(($plugin = $this->di->getPlugin($pluginName)) != null) {
				$text =  call_user_func(array($plugin, $pluginMethodName), $options, $content);
					
				if($pluginName == 'template') {
					/*
					 * Данные, которые ранее были переданы парсеру,
					* он помнит и их передавать заново не надо.
					* Но callback парсер не запомниает, поэтому мы снова его указываем,
					* чтоб иметь возможность делать вложенные шаблоны
					*/
					$text = $this->di->get("lex")->parse($text, $options, array($this, 'parse'));
				}
			}
			return $text;
		}
	}
	
	private function exportModuleJS($module) {
		if(($jsFiles = $module->getJS()) != null) {
			$jsFiles = is_array($jsFiles) ? $jsFiles : array($jsFiles);
			$jsURLPrefix = $this->di->get("config")->js->url_prefix;
			foreach($jsFiles as $key => $filename) {
				if($filename[0] != '/' && !preg_match('#^(http:)?//#', $filename)) {
					$filename = str_replace("//", "/", '/'.$jsURLPrefix.'/'.$module->getName().'/'.$filename);
				}
				/* привет */
				$jsFiles[$key] = $filename;
			}
			$this->addLayoutScript($jsFiles);
		}
	}
	
	private function exportModuleCSS($module) {
		/* модуль может захотеть подключить свои JS или CSS файлы */
		if(($cssFiles = $module->getCSS()) != null) {
			$cssFiles = is_array($cssFiles) ? $cssFiles : array($cssFiles);
			$cssURLPrefix = $this->di->get("config")->css->url_prefix;
			foreach($cssFiles as $key => $filename) {
				if($filename[0] != '/' && !preg_match('#^(http:)?//#', $filename)) {
					$filename = str_replace("//", "/", $cssURLPrefix.'/'.$module->getName().'/'.$filename);
				}
				$cssFiles[$key] = $filename;
			}
			$this->addLayoutStyle($cssFiles);
		}
	}
}

class Controller_Exception extends Exception {
	
}