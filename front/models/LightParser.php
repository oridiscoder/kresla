<?php
namespace front\models;

class LightParser extends Model {
	/**
	 * Функция вытаскивает из текста отдельные шаблоны, обрамлённые с помощью спец. HTML комментариев.
	 * <!--t.name-->content<!--/t.name-->
	 * Цель: передать из шаблонизатора в модуль более одного шаблона ($content)
	 * @param string $content - большой кусок текста, который надо разбить на подшаблоны
	 * @return array [template_name => template_content, ..., template_n_name => template_n_content]
	 */
	public function extractTemplates($content) {
		//<!--t.name-->content<!--/t.name-->
		preg_match_all('#<!--(t\.(\w+))-->(.*?)<!--/(\1)-->#smi', $content, $matches);
	
		return array_combine($matches[2], $matches[3]);
	}
	
	public function insertValues($template, $item) {
		$keys = array_map(function ($item) {
			return '{'.$item.'}';
		}, array_keys($item));
		$values = array_values($item);
		return str_replace($keys, $values, $template);
	}
}