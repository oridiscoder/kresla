<?php
namespace front\models;

abstract class Model {
	
	/**
	 * 
	 * @var Di
	 */
	private $di;
	
	public function __construct() {
		require_once "Di.php";
		$this->di = Di::getInstance();		
	}
	
	public function getDi() {
		return $this->di;
	}
	public function setDi($di) {
		$this->di = $di;
	}
	/**
	 * @return \Zend_Db_Adapter_Abstract
	 */
	public function getDb() {
		return $this->getDi()->get("db");
	}
	/**
	 * @return \Bof_Request
	 */
	public function getRequest() {
		return $this->getDi()->get("request");
	}
}