<?php
namespace front\models;

class DateHelper extends Model {

	private $monthNum2ParentCase = array( 
		1 => 'января', 
		2 => 'февраля', 
		3 => 'марта', 
		4 => 'апреля',
		5 => 'мая', 
		6 => 'июня', 
		7 => 'июля', 
		8 => 'августа', 
		9 => 'сентября', 
		10 => 'октября', 
		11 => 'ноября', 
		12 => 'декабря' 
	);
	
	private $monthNum2name = array(
			1 => 'январь',
			2 => 'февраль',
			3 => 'март',
			4 => 'апрель',
			5 => 'май',
			6 => 'июнь',
			7 => 'июль',
			8 => 'август',
			9 => 'сентябрь',
			10 => 'октябрь',
			11 => 'ноябрь',
			12 => 'декабрь'
	);
	
	public function monthName($monthNumber) {
		return $this->monthNum2name[$monthNumber];
	}
	public function monthToParentCase($monthNumber) {
		return $this->monthNum2ParentCase[$monthNumber];
	}
}