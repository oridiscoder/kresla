<?php
namespace front\models;

require_once ('front/models/Model.php');

class YmlBuilder extends Model {
	private $shop;
	private $currencies;
	private $categories;
	private $offers;
	
	public function __construct() {
		parent::__construct();
		$this->currencies = array(
			array(
				"id" => "RUR", 
				"rate" => 1
			), 
			array(
				"id" => "USD",
				"rate" => "CBRF"
			),
			array(
				"id" => "EUR",
				"rate" => "CBRF"
			),
		);
	}
	
	/**
	 * Добавить магазин
	 * @param string $name
	 * @param string $companyName
	 * @param string $url
	 * @return \front\models\YmlBuilder
	 */
	public function addShop($name, $companyName, $url) {
		$this->shop = array(
			"name" => $name, 
			"company_name" => $companyName, 
			"url" => $url
		);
		return $this;
	}

	public function setCurrencies($currencies) {
		$this->currencies = $currencies;
		return $this;
	}
	
	public function setCategories($categories) {
		$this->categories = $categories;
		return $this;
	}
	public function addCategory($id, $name, $parentID = null) {
		$this->categories[] = array(
			"id" => $id, 
			"name" => $name, 
			"parent_id" => $parentID
		);
	}
	public function setOffers($offers) {
		$this->offers = $offers;
	}
	public function addOffer($offer) {
		foreach ($offer as $key => $value) {
			$offer[$key] = htmlspecialchars($value);
		}
		$this->offers[] = $offer;
	}
	
	public function getXML() {
		$template = '
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="{{doc_date}}">
  <shop>
    <name>{{shop.name}}</name>
    <company>{{shop.company_name}}</company>
    <url>{{shop.url}}</url>

    <currencies>
    {{currencies}}
      <currency id="{{id}}" rate="{{rate}}"/>
    {{/currencies}}
    </currencies>

    {{if categories}}
    <categories>
    	{{categories}}
			<category id="{{id}}" {{if parentId}}parentId="{{parentId}}"{{endif}}>{{name}}</category>
		{{/categories}}
   </categories>
   {{endif}}

    <offers>
    {{offers}}
		<offer id="{{id}}" available="{{if available}}true{{else}}false{{endif}}">
			<url>{{url}}</url>
			<price>{{price}}</price>
			<currencyId>{{currencyId}}</currencyId>
			<categoryId>{{categoryId}}</categoryId>
			<picture>{{picture}}</picture>
			<delivery>{{if delivery}}true{{else}}false{{endif}}</delivery>
			<local_delivery_cost>{{local_delivery_cost}}</local_delivery_cost>
			<name>{{name}}</name>
			{{if vendor}}
			<vendor>{{vendor}}</vendor>
			<vendorCode>{{vendorCode}}</vendorCode>
			{{endif}}
			<description>{{description}}</description>
			<manufacturer_warranty>{{if manufacturer_warranty}}true{{else}}false{{endif}}</manufacturer_warranty>
			<country_of_origin>{{country_of_origin}}</country_of_origin>
		</offer>
	{{/offers}}
	</offers>
	 </shop>
</yml_catalog>
		';
		
		$xml = $this->getDi()->get("lex")->parse($template, array(
			"doc_date" => date("Y-m-d H:i"),
			"shop" => $this->shop, 
			"categories" => $this->categories, 
			"currencies" => $this->currencies, 
			"offers" => $this->offers
		));
		
		$xml = '<?xml version="1.0" encoding="utf-8"?>'.$xml;
		
		return $xml;
	}
	
}

?>
