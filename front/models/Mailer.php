<?php
namespace front\models;

require_once 'Model.php';
require_once "PHPMailer/class.phpmailer.php";

class Mailer extends Model {
	private $mailer;
	
	public function __construct(){
		parent::__construct();
		$this->mailer = new \PHPMailer();
	}
	public function mail($email, $subject, $text, $from = array()) {
		
		$di = $this->getDi();
		$conf = $di->get("config")->mail;
		
		$fromName = isset($from['name']) ? $from['name'] : $conf->defaultFrom->name;
		$fromEmail = isset($from['email']) ? $from['email'] : $conf->defaultFrom->email;
		
		
		$this->mailer->CharSet = 'utf-8';
		
		if($conf->transport->type == 'smtp') {
			$this->mailer->isSMTP();
			$this->mailer->Priority   = 1;
			$this->mailer->SMTPAuth   = true;
			$this->mailer->Host       = $conf->transport->host;
			$this->mailer->Port       = $conf->transport->port;
			$this->mailer->Username   = $conf->transport->username;
			$this->mailer->Password   = $conf->transport->password;
			//$this->mailer->SMTPDebug  = 1;
		}
		
		
		$this->mailer->SetFrom($fromEmail, $fromName);
		$this->mailer->Subject    = $subject;

		$this->mailer->MsgHTML($text);
		$this->mailer->AddAddress($email);



   		$from    = $fromEmail;
     		$to      = $email;
       		$subject = $subject;
         	$message = $text;
           	$headers = "From: $from\r\n";
             	$headers .= "MIME-Version: 1.0\r\n";
               	$headers .= "Content-Type: text/html; charset=".$this->mailer->CharSet."\r\n"."Content-Transfer-Encoding: 8bit\r\n";

                 mail($to, $subject, $message, $headers, "-f ".$fromEmail);


		return true;//$this->mailer->Send();
	}
	
	public function AddAttachment($a1, $a2, $a3='base64', $a4='application/octet-stream'){
		$this->mailer->AddAttachment($a1, $a2, $a3, $a4);
	}
}