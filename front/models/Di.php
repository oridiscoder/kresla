<?php

namespace front\models;
/**
 * Dependency Injection
 * 
 * Класс-контейнер для объектов, 
 * которые должны создаваться 
 * не более одного раза. 
 * Класс реализует паттерн Singletone, автоматически подгружает 
 * запрашиваемые объекты
 *
 */
class Di {
	/**
	 * The instance of this object
	 * @var Di
	 */
	private static $instance;
	
	/**
	 * Массив созданных объектов
	 * @var array
	 */
	private $objects;
	
	private $modelsPath;
	private $pluginPath;
	private $modulesPath;
	
	private function __construct() {
		$this->pluginPath = realpath(APPLICATION_PATH . '/../../front/plugins');
		$this->modelsPath = realpath(APPLICATION_PATH . '/../../front/models');
		$this->modulesPath = realpath(APPLICATION_PATH . '/../../front/modules');
	}
	
	public function getDb() {
		$db = $this->get("db");
		if($db) {
			return $db;
		} else {
			throw new DiException("You must set Db first using register('db', db) method");
		}
	}
	
	public function setPluginPath($path) {
		$this->pluginPath = $path;
	}
	public function getPluginPath() {
		return $this->pluginPath;
	}
	public function setModulesPath($path) {
		$this->modulesPath = $path;
	}
	public function getModulesPath() {
		return $this->modulesPath;
	}
	public function setPath($path) {
		$this->modelsPath = $path;
	}
	public function getPath() {
		return $this->modelsPath;
	}
	
	/**
	 * Get the instance of Di
	 * @return Di
	 */
	public static function getInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * Получить объект по названию его класса
	 * 
	 * Если объект уже зарегистрирован, то новый экземпляр не создается. 
	 * Если объект ещё неизвестен, то загружаем описание его класса и создаем новый объект. 
	 * 
	 * @param string $className
	 * @return stdObject
	 */
	public function get($className) {
		$className = ucfirst($className);
		
		if(!isset($this->objects[$className])) {
			$className = '\\front\\models\\'.ucfirst($className);
			//require_once $this->getPath() . "/$className.php";
			//spl_autoload($className);
			//if(class_exists($className)) {
				$o = new $className();
				//$o->setDb($this->getDb());				
				$this->objects[$className] = $o;
			//} else {
			//s	throw new ModelNotFoundException("Class $className not found");
			//}
		}
		
		return $this->objects[$className];
	}
	
	/**
	 * Ручная регистрация объектов
	 * 
	 * Часто надо не просто создать объект, но и передать ему какие-то параметры, 
	 * настроить его, вызвать методы. Все это можно сделать вручную, а затем 
	 * зарегистрировать подготовленный объект в Di
	 * 
	 * @param string $name - название класса
	 * @param stdObject $object - регистрируемый объект
	 */
	public function register($name, $object) {
		$name = ucfirst($name);
		$this->objects[$name] = $object;
	}
	
	
	
	/**
	 * Получить плагин по его имени
	 * @param string $className - назвние класса плагина
	 * @return PluginAbstract
	 * @throws Exception
	 */
	public function getPlugin($className) {
	
		$className = 'Plugin_'.ucfirst($className);
		$keyName = $className;
	
		if(!isset($this->objects[$keyName])) {
			/* подгружаем файл с плагином */
			$fullname = $this->getPluginPath() . "/".str_replace("Plugin_", "", $className).".php";
			if(file_exists($fullname)) {
				require_once $fullname;
				if(class_exists($className)) {
					/* @var $o PluginAbstract */
					$o = new $className();
					$o->setDi($this);
					$this->objects[$keyName] = $o;
				} else {
					throw new PluginNotFoundException("Plugin «".$className."» not found in «".$fullname."»");
				}
			} else {
				return false;
				//throw new PluginNotFoundException("«".$className."» plugin file «".$fullname."» not exists");
			}
		}
	
		return $this->objects[$keyName];
	}
	
	public function registerPlugin($name, $pluginObject) {
		$key = 'Plugin_'.ucfirst($name);
		$this->objects[$key] = $pluginObject;
	}
	
	/**
	 * Получить плагин по его имени
	 * @param string $className - назвние класса плагина
	 * @return \front\modules\Module
	 * @throws Exception
	 */
	public function getModule($className) {
	
		$moduleName = ucfirst($className);
		$className = '\\front\\modules\\'.$moduleName;
		$keyName = $className;
	
		if(!isset($this->objects[$keyName])) {
			/* подгружаем файл с плагином
			 * @TODO сделать бы переопределяемые пользователем плагины, 
			 * front/modules
			 * front/modules_ext
			 * 
			 * и поиск, соответственно, сначала в front/modules_ext а потом в front/modules
			 * */
			$fullname = $this->getModulesPath() . "/$moduleName/".$moduleName.".php";
			if(file_exists($fullname)) {
				require_once $fullname;
				if(class_exists($className)) {
					/* @var $o FrontModuleAbstract */
					$o = new $className();
					$o->setDi($this);
					$this->objects[$keyName] = $o;
				} else {
					throw new ModuleNotFoundException("Module $className not found");
				}
			} else {
				throw new ModuleNotFoundException("Module file not found '$fullname'");
			}
		}
	
		return $this->objects[$keyName];
	}
	
	public function registerModule($name, $pluginObject) {
		$key = 'Module_'.ucfirst($name);
		$this->objects[$key] = $pluginObject;
	}
}

class DiException extends \Exception {
	
}
class ModelNotFoundException extends DiException {
	
}
class PluginNotFoundException extends DiException {
	
}
class ModuleNotFoundException extends DiException {
	
}