<?php

namespace front\models;

class Logger extends Model {
	
	private $di;
	private $filename;
	private $maxSize;
	private $active = true;
	
	public function __construct() {
		$this->setDi(DI::getInstance());
	}
	
	public function setDi($di) {
		$this->di = $di;
		$this->setLogFilename($di->get("config")->log->filename);
		$this->setMaxLogSize($di->get("config")->log->max_size);
		$this->setActive($di->get("config")->log->enable);
	}
	
	public function getDi() {
		return $this->di;
	}
	
	public function getLogFilename() {
		return $this->filename;
	}
	public function getMaxSize() {
		return $this->maxSize;
	}
	public function setLogFilename($filename) {
		$this->filename = $filename;
	}
	public function setMaxLogSize($size) {
		$multiplier = array('K' => 1024, 'M' => 1024 * 1024, 'G' => 1024 * 1024 * 1024);
		$sizeLength = strlen($size);
		$lastLetter = strtoupper($size[$sizeLength - 1]);
		if(!is_numeric($lastLetter)) {
			if(isset($multiplier[$lastLetter])) {
				$size = (int) substr($size, 0, $sizeLength - 1) * $multiplier[$lastLetter];
			} else {
				throw new LoggerException("Wrong size format $size");
			}
		}
		$this->maxSize = $size;
	}
	public function setActive($isActive) {
		$this->active = $isActive;
	}
	public function isActive() {
		return $this->active;
	}
	public function setDb($db) {
		$this->db = $db;
	}
	
	public function log($message) {
		if($this->isActive()) {
			
			$filename = $this->getLogFilename();
			
			if($message[strlen($message) - 1] != "\n") {
				$message .= "\n";
			}
			$message = '['.date('Y-m-d H:i:s').']'.$message;
			
			$this->appendFile($filename, $message, $this->getMaxSize());
		}
	}
	
	/**
	 * Записать данные в файл, учтя максимальный размер, установленый для этого файла
	 * @param string $filename
	 * @param string $data
	 * @param integer $maxSize - размер в байтах
	 */
	protected function appendFile($filename, $data, $maxSize) {
		if(file_exists($filename)) {
			clearstatcache();
			$fileSize = filesize($filename) + strlen($data);
			if($fileSize > $maxSize) {
				$fd = fopen($filename, "r");
				$seek = $fileSize - $maxSize;
				if($fd) {
					fseek($fd, $seek);
					do {
						$chr = fread($fd, 1);
					} while($chr != "\n" && !feof($fd));
				}
				$content = fread($fd, $fileSize);
				fclose($fd);
				file_put_contents($filename, $content . $data);
			} else {
				file_put_contents($filename, $data, FILE_APPEND);
			}
		} else {
			file_put_contents($filename, $data);
		}
	}
}

class LoggerException extends \Exception {
	
}