<?php
namespace front\models;

require_once ('front/models/Model.php');

use front\models\Model;

class View extends Model {
	private $templatesDirname;
	
	public function setTemplateDir($dirname) {
		$this->templatesDirname = $dirname;
	}
	
	public function render($templateName, $vars) {
		if(file_exists($this->templatesDirname . DIRECTORY_SEPARATOR . $templateName)) {
			$templateName = $this->templatesDirname . DIRECTORY_SEPARATOR . $templateName;
		} elseif (!file_exists($templateName)) {
			throw new \Exception("Template $templateName not found");
		}
		
		extract($vars);
		
		ob_start();
		include $templateName;
		return ob_get_clean();
	}
	
}