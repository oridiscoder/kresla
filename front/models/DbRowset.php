<?php
namespace front\models;

require_once ('Orm.php');
require_once ('DbRecord.php');

class DbRowset extends Orm implements \Iterator {
	
	private $rows;
	private $position;
	private $current;
	
	public function __construct($rows, $dbTable) {
		$this->dbTable = $dbTable;
		$this->rows = $rows;
		$this->position = 0;
	}
	
	public function rewind() {
		$this->position = 0;
	}
	
	public function current($className = null) {
		if($className) {
			return new $className($this->rows[$this->position], $this->dbTable);
		} else {
			return new DbRecord($this->rows[$this->position], $this->dbTable);
		}
	}
	
	public function key() {
		return $this->position;
	}
	
	public function next() {
		$this->position ++;
	}
	
	public function valid ()	{
		return isset($this->rows[$this->position]);
	}
	
	public function asArray() {
		return $this->rows ? $this->rows : array();
	}
	
}