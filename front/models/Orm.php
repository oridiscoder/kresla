<?php
namespace front\models;

class Orm {

	/**
	 * 
	 * @var \DbTableLight
	 */
	protected $dbTable;
	private $rowsetClassName;
	private $rowClassName;
	public static $dbPrefix;
	
	/* зависимые таблицы (потомки) */
	private $depends;
	
	public function __construct($tableName, $rowsetClassName = null, $rowClassName = null) {
		require_once "models/Table/DbTableLight.php";
		$db = Di::getInstance()->get("db");
		if(self::$dbPrefix === null) {
			self::$dbPrefix = Di::getInstance()->get("config")->db_prefix;
		} 
		$this->dbTable = new \DbTableLight($db, self::$dbPrefix.$tableName);
		$this->rowsetClassName = $rowsetClassName;
		$this->rowClassName = $rowClassName;
	}

	/**
	 * @return \Zend_Db_Adapter_Abstract
	 */
	protected function getDb() {
		return $this->dbTable->getAdapter();
	}
	
	public function setRowsetClassName($className) {
		$this->rowsetClassName = $className;
	}
	public function setRowClassName($className) {
		$this->rowClassName = $className;
	}
	public function getRowsetClassName() {
		return $this->rowsetClassName ? $this->rowsetClassName : "\\front\\models\\DbRowset";
	}
	public function getRowClassName() {
		return $this->rowClassName ? $this->rowClassName : "\\front\\models\\DbRecord";
	}
	
	/**
	 * Установить зависимость между текущей таблицей и родительской 
	 * @param string $parentTableName - название родительской таблицы
	 * @param string $myFieldName - название поля в текущей таблице, по которому осуществляется связь
	 */
	public function dependsOn($parentTableName, $myFieldName) {
		$this->depends[] = array(
			"table_name" => $parentTableName, 
			"field_name" => $myFieldName
		);
	}
	public function getDepends($tableName) {
		if($this->depends) {
			foreach($this->depends as $table) {
				if($table['table_name'] == $tableName) {
					return $table;
				}
			}
		}
	}
	
	/**
	 * Выбрать из таблицы записи согласно условиям, обозначенным в $options
	 * @param array $options - массив условий для выборки.
	 * @return array 
	 */
	protected function findRows($options) {
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = $this->getDb();
		
		$select = $db->select()
		->from($this->dbTable->getName())
		->where("hidden = 0");
		
		$where = array();
		foreach ($options as $key => $value) {
			if(!in_array((string)$key, array("parent_table_record_id", "limit", "order"))) {
				if(is_string($key)) {
					$where[] = array($key, $value);
				} else {
					$where[] = $value;
				}
			}
		}
		
		/* сложноватая конструкция получилась */
		if(isset($options['parent_table_record_id'])) {
			$where[] = array($this->depends[0]['field_name'].' = ?', $options['parent_table_record_id']);
		}
		
		foreach ($where as $w) {
			if(is_array($w)) {
				$select->where($w[0], $w[1]);
			} else {
				$select->where($w);
			}
		}
		
		if(isset($options['order'])) {
			$select->where($options['order']);
		}
		
		if(isset($options['limit'])) {
			$select->where($options['limit']);
		}
		
		$rows = $db->fetchAll($select);
		
		return $rows ? $rows : array();
	}
	
	/**
	 * Выбрать из таблицы записи, удоволетворяющие заданным в $options условиям
	 * @param array $options - массив условий
	 * @return \front\models\DbRowset
	 */
	public function find($options = array()) {
		$className = $this->getRowsetClassName();
		return new $className($this->findRows($options), $this->dbTable);
	}
	
	
	/**
	 * 
	 * @param array $options
	 * @return \front\models\DbRecord
	 */
	public function findFirst($options, $className = null) {
		$options['limit'] = 1;
		$rows = $this->findRows($options);
		if($rows) {
			$className = $this->getRowClassName();
			return new $className($rows[0], $this->dbTable);
		}
	} 
	
	/**
	 * Получить зависимую таблицу
	 * @param string $tableName - название зависимой таблицы без префикса
	 * @param string $fieldName - название поля в зависимой таблице, по которому мы связываемся, например, good_id
	 * @return Orm
	 * @throws \Exception
	 */
	public function childTable($tableName, $fieldName = null) {
		/* получим список зависимых таблиц для текущей */
		$dependentTables = $this->dbTable->getDependentTables();
		$dependentTable = null;
	
		/* ищем указанную таблицу среди зависимых */
		foreach($dependentTables as $table) {
			if($table['name'] == self::$dbPrefix.$tableName) {
				$dependentTable = $tableName;
				break;
			}
		}
	
		if($dependentTable) {
			$orm = new Orm($dependentTable);
			$orm->dependsOn($this->dbTable->getName(), $fieldName ? $fieldName : $table['_fields'][0]['name']);
			return $orm;
		} else {
			throw new \Exception("Dependent table $tableName not found for ".$this->dbTable->getName());
		}
	}
	
	/**
	 * Получить родительскую таблицу по её названию
	 * @param string $tableName - название родительской таблицы
	 * @param string $fieldName - название столбца в текущей таблице, по которому осуществляется связь
	 * @return Orm
	 */
	public function parentTable($tableName, $fieldName = null) {
		$parentTables = $this->dbTable->getParentTables();
		$parentTable = null;
		
		/* ищем указанную таблицу среди зависимых */
		foreach($parentTables as $table) {
			if($table['name'] == self::$dbPrefix.$tableName) {
				$parentTable = $tableName;
				break;
			}
		}
		
		if($parentTable) {
			$orm = new Orm($parentTable);
			$this->dependsOn($parentTable, $fieldName ? $fieldName : $table['_fields'][0]['name']);
			return $orm;
		} else {
			throw new \Exception("Parent table $tableName not found for ".$this->dbTable->getName());
		}
	}
	
}