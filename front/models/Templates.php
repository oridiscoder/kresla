<?php

namespace front\models;

class Templates extends Model {

	private $name2id;
	private $byId;
	
	public function find($key) {
		if(is_numeric($key)) {
			if(!isset($this->byId[$key])) {
				$t = $this->getDb()->fetchRow("SELECT * FROM cms_templates WHERE id = ? AND hidden = 0", $key);
				if($t) {
					$this->byId[$key] = $t;
					$this->name2id[$t['name']] = $key;
				} else {
					throw new TemplateNotFoundException("Template with id '$key' not found");
				}				
			}
			return $this->byId[$key]['content'];
		}
		if(is_string($key)) {
			if(!isset($this->name2id[$key])) {
				$t = $this->getDb()->fetchRow("SELECT * FROM cms_templates WHERE name = ? AND hidden = 0", $key);
				if($t) {
					$this->byId[$t['id']] = $t;
					$this->name2id[$t['name']] = $t['id'];
				} else {
					throw new TemplateNotFoundException("Template with name '$key' not found");
				}				
			}
			return $this->byId[$this->name2id[$key]]['content'];
		}
	}
	
	public function getAsArray($name) {
		$this->find($name);
		if(is_numeric($name)) {
			$key = $name;
		} else {
			$key = $this->name2id[$name];
		}		
		return $this->byId[$key];
	}
	
	public function delete($id) {
		$this->getDb()->delete("cms_templates", array("id = ?" => $id));
	}
}

class TemplateNotFoundException extends \Exception {
	
}