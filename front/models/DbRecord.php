<?php
namespace front\models;

class DbRecord extends Orm {
	
	private $data;
	
	public function __construct($data, $dbTable) {
		$this->data = $data;
		$this->dbTable = $dbTable;
	}
	
	public function __get($colname) {
		if(isset($this->data[$colname])) {
			return $this->data[$colname];
		} else {
			//магия
		}
	}
	
	public function getChilds($tableName, $fieldName = null, $options = array()) {
		$table = $this->childTable($tableName, $fieldName);
		$options['parent_table_record_id'] = $this->data['id'];
		return $table->find($options);
	}
	
	public function getParent($tableName, $fieldName = null, $options = array()) {
		
		$table = $this->parentTable($tableName, $fieldName);
		
		if($fieldName == null) {
			$t = $this->getDepends($tableName);
			if($t) {
				$fieldName = $t['field_name'];
			}
		}
		
		$options[] = 'id = '.$this->data[$fieldName];
		$rows = $table->find($options);
		
		return empty($rows) ? null : $rows->current();
	}
	
	public function asArray() {
		return $this->data;
	}
}