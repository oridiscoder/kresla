<?php
namespace front\models;

class Registry extends Model {
	/**
	 * Сюда склаюываем данные, полученные из БД, чтоб не совершать дополнительных запросов
	 * в процессе работы скрипта. Слово Кеш плохо подходит для именования данного свойства класса,
	 * потому что данные не сохраняются надолго в внешнее хранилище.
	 * @var array
	 */
	private $instantCache;
	
	/**
	 * Помещаем в instantCache новые данные. Каждая строка должна содержать поле id
	 * чтоб она могла быть помещена в кеш.
	 * @param array $rows
	 */
	public function addRows($rows) {
		foreach ($rows as $row) {
			$this->addRow($row);
		}
	}
	public function addRow($row) {
		if(isset($row['id'])) {
			$id = $row['id'];
			$this->cachedItems[$id] = $row;
		}
	}
	public function find($id) {
		return isset($this->cachedItems[$id]) ? $this->cachedItems[$id] : null;
	}
}