<?php

namespace front\models;

class Page extends Model {
	
	private $page;
	public function __construct($page) {
		parent::__construct();
		$this->page = $page;
		$this->page['ruri'] = $_SERVER['REQUEST_URI'];
		// $uri = strtok($_SERVER['REQUEST_URI'],'?');
		// echo substr($uri, 0, 21); die;
	}
	public function getID() {
		return $this->page['id'];
	}
	public function getName() {
		return $this->page['name'];
	}
	public function getTitle() {
		return $this->page['title'];
	}
	public function setTitle($title) {
		$this->page['title'] = $title;
	}
	public function getKeywords() {
		return $this->page['keywords'];
	}
	public function setKeywords($keywords) {
		$this->page['keywords'] = $keywords;
	}
	public function getDescription() {
		return $this->page['description'];
	}
	public function setDescription($description) {
		$this->page['description'] = $description;
	}
	public function getH1() {
		return isset($this->page['h1']) ? $this->page['h1'] : "";
	}
	public function setH1($h1) {
		$this->page['h1'] = $h1;
	}
	public function getSeoText() {
		return isset($this->page['seo_text']) ? $this->page['seo_text'] : "";
	}
	public function setSeoText($text) {
		$this->page['seo_text'] = $text;
	}
	public function inMenu() {
		return $this->page['in_menu'];
	}
	public function inSubMenu() {
		return $this->page['in_submenu'];
	}
	public function getMenuName() {
		return $this->page['menu_name'];
	}
	public function setMenuName($name) {
		return $this->page['menu_name'] = $name;
	}
	public function isActive() {
		return $this->page['active'];
	}
	public function getTemplateId() {
		return $this->page['template_id'];
	}
	public function getTemplateName() {
		return $this->page['template'];
	}
	public function getContent() {
		return $this->page['content'];
	}
	public function setContent($content) {
		$this->page['content'] = $content;
	}
	public function setMetaForPaginatorPages($content) {
		$this->page['metaForPaginatorPages'] = $content;
	}
	public function getMetaForPaginatorPages() {
		return $this->page['metaForPaginatorPages'];
	}

	public function getParent() {
		return isset($this->page['_parent']) ? $this->page['_parent'] : null;
	}
	public function setParent($parentPage) {
		return $this->page['_parent'] = $parentPage;
	}
	public function isSectionPage() {
		return $this->page['is_section'];
	}
	public function getRouterName() {
		return $this->page['router'];
	}
	public function getCurTables() {
		if(isset($this->page['curtables'])){
			return $this->page['curtables'];
		}else{
			return;
		}
	}	
	/**
	 * Уровень вложенности страницы. УВ главной = 0
	 * @return integer
	 */
	public function getLevel() {
		return $this->page['_level'];
	}
	/**
	 * Получить ту часть URL, которая относится к 
	 * вложенным страницам (для страниц-разделов)
	 * @return array
	 */
	public function getChildURLParts() {
		return isset($this->page['_child_url_parts']) ? $this->page['_child_url_parts'] : null;
	}
	public function getHierarcy() {
		$page = $this->page;
		$hierarcy = array();
		
		while($page) {
			$hierarcy[] = $page;
			
			/*if(isset($page['_parent'])) {
				$page = $page['_parent'];
			} else {*/
				if($page['parent_id']) {
					$page = self::find($page['parent_id']);
				} else {
					$page = null;
				}
			//}
		}
		
		return array_reverse($hierarcy);
	}
	
	public function getBreadcrumbs() {
		
		if(!isset($this->page['_breadcrumbs'])) {
			foreach($this->getHierarcy() as $page) {
				$href = isset($href) ? str_replace("//", "/", $href . '/' . $page['name'] .'/') : $page['name'];
				$items[] = array(
						'title' => $page['menu_name'],
						'href' => $href
				);
			}
			$this->page['_breadcrumbs'] = $items;
		}
		
  		return $this->page['_breadcrumbs'];
	}
	
	public function setBreadcrumbs($breadcrumbs) {
		$this->page['_breadcrumbs'] = $breadcrumbs;
	}
	public function pushBreadcrumb($breadcrumb) {
		$this->page['_breadcrumbs'][] = $breadcrumb;
	}
	
	public function getFullUrl() {
		$bc = $this->getBreadcrumbs();
		if($bc) {
			$end = end($bc);
			$url = $end['href'];
		} else {
			return $this->getName();
		}
		return $url;
	}
	
	public function getSectionUrl() {
		$h = $this->getHierarcy();
		$url = array();
		foreach($h as $page) {
			$url[] = $page['name'] == '/' ? '' : $page['name'];
			if($page['is_section']) {
				break;
			}
		}
		$url = implode("/", $url);
				
		return $url;
	}
	
	public function asArray() {
		/* инициализируем хлебные крошки */
		$this->getBreadcrumbs();
		return $this->page;
	}
	
	public function getChildPages() {
		$db = $this->getDi()->get("db");
		$select = $db->select()
			->from("cms_pages")
			->where("hidden = 0")
			->where("parent_id = ?", $this->getID())
			->order("number");
		return $db->fetchAll($select);
	}
	
	public function findChildPage($pageName) {
		$db = $this->getDi()->get("db");
		$select = $db->select()
			->from("cms_pages")
			->where("hidden = 0")
			->where("parent_id = ?", $this->getID())
			->where("name = ?", $pageName);
		return $db->fetchRow($select);
	}
	
	/**
	 * Получить главную страницу сайта
	 * @return \front\models\Page
	 */
	public static function getRoot() {
		$db = Di::getInstance()->get("db");
		$select = $db->select()
			->from("cms_pages")
			->where("hidden = 0")
			->where("name = '/'")
			->where("parent_id is NULL");
		$root = $db->fetchRow($select);
		if($root) {
			return new self($root);
		}
	}
	
	public static function find($cond) {
		if(is_numeric($cond)) {
			$db = Di::getInstance()->get("db");
			$page = $db->fetchRow("SELECT * FROM cms_pages WHERE id = ?", $cond);
			
			return $page;
		} else {
			// @TODO сделать поиск
		}
	}
}

class PageNotFound extends \Exception {
	
}