<?php
namespace front\models;

class RssBuilder extends Model {
	
	public static function rss($conf) {
		$wrapper = '<?xml version="1.0" encoding="utf-8"?>
		<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">
		{channel}
		</rss>';
		
		$channelTemplate = '
	 	<channel>
    		<title><![CDATA[{title}]]></title>
    		<link>{link}</link>
    		<description><![CDATA[{description}]]></description>
    		<pubDate>{pubdate}</pubDate>
    		<image>
      			<url>{image}</url>
      			<title><![CDATA[{title}]]></title>
      			<link>{link}</link>
    		</image>
    		{items}
    	</channel>
		';
		
		$itemTemplate = '
		<item>
     		<title><![CDATA[{title}]]></title>
      		<link>{link}</link>
      		<description><![CDATA[{description}]]></description>
      		<pubDate>{pubdate}</pubDate>
      		<yandex:full-text><![CDATA[{yandex:full-text}]]></yandex:full-text>
    	</item>';
		
		$itemsHTML = "";
		if($conf['entries']) foreach($conf['entries'] as $item) {
			$itemsHTML .= str_replace(
				array('{title}', '{link}', '{description}', '{pubdate}', '{yandex:full-text}'),
				array($item['title'], $item['link'], $item['description'], self::rfcDate($item['pubDate']), $item['yandex:full-text']),
			$itemTemplate);
		}
		
		$channelHTML = str_replace(
			array('{title}', '{link}', '{description}', '{pubdate}', '{image}', '{items}'), 
			array($conf['title'], $conf['link'], $conf['description'], self::rfcDate($conf['pubDate']), $conf['image'], $itemsHTML),
			$channelTemplate);
		
		return str_replace("{channel}", $channelHTML, $wrapper);
	}
	
	public static function rfcDate($date) {
		if(is_numeric($date)) {
			$pubDate = $date;
		} else {
			$pubDate = strtotime($date);
		}
		return  date('r', $pubDate);
	} 
	
}

?>