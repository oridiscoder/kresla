<?php

namespace front\modules;

use front\models\ModelNotFoundException;

abstract class Module {
	/**
	 * 
	 * @var \front\models\Di
	 */
	private $di;
	
	private $dbConfigData;
	
	/**
	 * 
	 * @var \front\modules\Registry
	 */
	private $registry;
	
	/**
	 * Список установленных модулей
	 * @var array
	 */
	protected static $installedList;
	
	public function setDi($di) {
		$this->di = $di;
	}
	/**
	 * @return \front\models\Di
	 */
	public function getDi() {
		return $this->di;
	}
	/**
	 * @return \Zend_Db_Adapter_Abstract
	 */
	public function getDb() {
		return $this->getDi()->get("db");
	}
	
	/**
	 * @return \front\models\Registry
	 */
	public function getRegistry() {
		if(!$this->registry) {
			$this->registry = new \front\models\Registry();
		}
		return $this->registry;
	}
	
	/**
	 * @return \Zend_Controller_Request_Http
	 */
	public function getRequest() {
		return $this->getDi()->get("request");
	}
	
	/**
	 * @return \front\models\Page
	 */
	public function getPage() {
		return $this->getDi()->get("page");
	}
	
	/**
	 * Если модулю необходимо подключить свои файлы стилей,
	 * то надо переопределить этот метод и вернуть массив с ссылками на css файлы
	 * @return array
	 */
	public function getCSS() {
		return array();
	}
	/**
	 * Если модулю необходимо подключить свои JS файлы,
	 * то надо переопределить этот метод и вернуть массив с ссылками на js файлы
	 * @return array
	 */
	public function getJS() {
		return array();
	}
	
	/**
	 * Получить фабрику по созданию таблиц и полей
	 * @return \DbTableCreator
	 */
	protected function getTableFactory() {
		require_once 'models/Table/DbTableCreator.php';
		return new \DbTableCreator($this->getDb());
	}
	
	/**
	 * Получит объект DbTableLight - легкий аналог DbTable для доступа к объектам и полям этих объектов, описанных в CMS
	 * @param string $tableName - имя таблицы
	 * @return \DbTableLight
	 */
	protected function getDbTableLight($tableName) {
		require_once "models/Table/DbTableLight.php";
		return new \DbTableLight($this->getDb(), $tableName);
	}
	
	/**
	 * Получить фабрику по созданию страниц в админке
	 * @return \PageCreator
	 */
	protected function getPageFactory() {
		require_once 'models/Page/PageCreator.php';
		return new \PageCreator($this->getDb());
	}
	
	/**
	 * Получить модель для работы с cms_pages
	 * @return \CMSPage
	 */
	protected function getCMSPagesFactory() {
		require_once 'models/Page/CMSPage.php';
		return new \CMSPage($this->getDb());
	}
	
	/**
	 * Получить список установленных модулей
	 * @return array [moduleNameOne => [...], moduleNameTwo => [...]]
	 */
	public function getInstalledModules() {
		$sql = "SELECT name, id, active FROM cms_modules ORDER BY id";
		$modules = $this->getDb()->fetchAssoc($sql);
		return $modules ? $modules : array();
	}
	/**
	 * Установлен ли текущий модуль
	 */
	public function isInstalled() {
		if(self::$installedList === null) {
			self::$installedList = $this->getInstalledModules();
		}
		return isset(self::$installedList[$this->getName()]);
	}
	/**
	 * Включен ли текущий модуль
	 * @return boolean
	 */
	public function isActive() {
		if($this->isInstalled()) {
			return self::$installedList[$this->getName()]['active'];
		} else {
			return false;
		}
	}
	
	/**
	 * Получить КОНТЕНТ шаблона по названию или ID
	 * 
	 * Если в качестве параметра передается название шаблона, то оно 
	 * будет модифицировано (добавлено имя модуля вначале). Этот метод можно 
	 * использовать только для получения шаблонов конкретно данного модуля.
	 * Для получения любых шаблонов, следует воспользоваться объектом Templates
	 * <code>$this->getDi()->get("Templates");</code>
	 * 
	 * @param string|int $name - название модуля или его ID
	 * @return string
	 */
	public function getTemplate($name, $local = false) {
		if($local == false) {
			/* @var $tModel Templates */
			$tModel = $this->getDi()->get("Templates");
			if(!is_numeric($name)) {
				$name = $this->getName().':'.$name;
			}
			return $tModel->find($name);
		} else {
			$moduleName = str_replace("Module_", "", $this->getName());
			$filename = dirname(__FILE__) . '/'.$moduleName.'/templates/'.$name;
			if(file_exists($filename)) {
				return file_get_contents($filename);
			} else {
				throw new ModelNotFoundException("Не найден файл $filename");
			}
		}
	}
	
	public function getTemplates($filter = array()) {
		$select = $this->getDb()->select()
		->from("cms_templates")
		->where("module_name  = ?", $this->getName())
		->order("number");
	
		if($filter) {
			$whitelist = array("id", "name", "content");
			foreach($filter as $name => $value) {
				if(in_array($name, $whitelist)) {
					$select->where($name . ' LIKE ?', '%'.$value.'%');
				}
			}
		}
	
		$templates =  $this->getDb()->fetchAll($select);
		if($templates) foreach($templates as &$template) {
			$template['name'] = str_replace($this->getName().':', '', $template['name']);
		}
	
		return $templates;
	}
	
	/**
	 * Аналогично getTemplate но возвращает не контент, а ассоциативный массив
	 * содержащий все поля шаблона
	 * @param string|int $name
	 * @return array
	 */
	public function getTemplateArray($name) {
		/* @var $tModel Templates */
		$tModel = $this->getDi()->get("Templates");
		if(!is_numeric($name)) {
			$name = $this->getName().':'.$name;
		}
		$t =  $tModel->getAsArray($name);
		$t['name'] = str_replace($this->getName().':', '', $t['name']);
		return $t;
	}
	
	public function saveTemplate($name, $content, $id = null) {
		$name = $this->getName().':'.$name;
		$data = array(
			'name' => $name, 
			'content' => $content			
		);
		
		if($id) {
			$this->getDb()->update("cms_templates", $data, array("id = ?" => $id));
		} else {
			$data['number'] = $this->getDb()->fetchOne("SELECT MAX(number) + 1 FROM cms_templates");
			$data['module_name'] = $this->getName();
			$this->getDb()->insert("cms_templates", $data);
			$id = $this->getDb()->lastInsertId();
		}
		
		return $id;
	}
	
	public function deleteTemplate($name) {
		try {
			$t = $this->getTemplateArray($name);
			if($t) {
				$this->getDi()->get("Templates")->delete($t['id']);
			}
		} catch (\front\models\TemplateNotFoundException $ex) {
			//а на нет и суда нет.
		}			
	}
	
	/**
	 * Получить название данного модуля. 
	 * 
	 * Название должно быть уникальным, поэтому метод возвращает в качестве 
	 * названия - имя текущего класса.
	 * Имя модуля используется для хранения его настроек в БД, обращения к нему 
	 * в панеле администратора с целью удалить, установить, настроить модуль.
	 * 
	 * @return string
	 */
	public function getName() {
		$className = get_class($this);
		return 'Module_'.str_replace(__NAMESPACE__.'\\', "", $className);
	}
	/**
	 * Загрузить настройки данного модуля из БД
	 * @return array key=>value массив
	 */
	public function loadDbConfig() {
		return $this->getDb()->fetchPairs("SELECT `key`,`value` FROM cms_modules_config WHERE module_name = ?", $this->getName());
	}
	public function saveDbConfig($config) {
		$moduleName = $this->getName();
		$dbConfig = $this->getDb()->fetchAll("SELECT * FROM cms_modules_config WHERE module_name = ?", $this->getName());
		$ids = array();
		
		foreach ($config as $key => $value) {
			$exists = false;
			foreach($dbConfig as $dbRow) {
				if($dbRow['key'] == $key) {
					$ids[] = $dbRow['id'];
					$this->getDb()->update("cms_modules_config", array("value" => $value), "id = ".$dbRow['id']);
					$exists = true;
					break;
				}
			}
			
			if($exists == false) {
				$this->getDb()->insert("cms_modules_config", array(
					'module_name' => $this->getName(), 
					'key' => $key, 
					'value' => $value
				));
				$ids[] = $this->getDb()->lastInsertId();
			}
		}
		
		foreach($dbConfig as $dbRow) {
			if(!in_array($dbRow['id'], $ids)) {
				$this->getDb()->delete("cms_modules_config", "id = ".$dbRow['id']);
			}
		}
		
	}
	
	public function cleanDbConfig() {
		$this->getDb()->delete("cms_modules_config", array("module_name = ?" => $this->getName()));
	}
	
	/**
	 * Получить настройки данного модуля
	 * 
	 * Метод возвращает массив настроек в ввиде структурированного массива, 
	 * где ключом является название конф.параметра, а значением - массив, 
	 * описывающий этот параметр с уже установленным значением value.
	 * Значение value подгрузается из БД, используя метод  loadDbConfig()
	 * Пример: 
	 * limit => {
	 * 		'type' => 'int', 
	 *		'value' => 10, 
	 *		'title' => 'Максимальное число'
	 * }
	 * 
	 * @return array
	 */
	public function getConfig($key = null) {
		$fields = $this->getConfigFields();
		if($fields) {
			
			/* не будем же мы каждый раз лезть в БД за конфигом. Закешируем его */
			if(!$this->dbConfigData) {
				$this->dbConfigData = $this->loadDbConfig();
			}
			
			$fieldValues = $this->dbConfigData;
			foreach ($fields as $name => $field) {
				if(isset($fieldValues[$name])) {
					$fields[$name]['value'] = $fieldValues[$name];
				} else {
					$fields[$name]['value'] = isset($field['default'])  ? $field['default'] : null;
				}
			}
		}
		
		/* Если клиент запрашивал конкретное поле, то возвращаем его */
		if($key) {
			if(array_key_exists($key, $fields)) {
				return $fields[$key]['value'];	//да-да, возвращаем сразу значение
			} else {
				throw new \Exception("Module ".$this->getName()." has no '$key' field");
			}
		} else {
			return $fields;
		}
	}
	
	public function setConfig($key, $value) {
		$fields = $this->getConfigFields();
		if(in_array($key, array_keys($fields))) {
			
			if($this->dbConfigData) {
				$this->loadDbConfig();
			}
			$this->dbConfigData[$key] = $value;
			$this->saveDbConfig($this->dbConfigData);
		}
	}
	
	protected function getDbPrefix() {
		return $this->getDi()->get("config")->db_prefix;
	}
	
	/**
	 * Каждый модуль может выступать в качестве Роутера. 
	 * Если страница является разделом и по URL мы запрашиваем её дочернюю страницу и у страницы объявлен 
	 * модуль - роутер, то этот модуль сам обрабатывает оставшуюся часть URl
	 * @param \front\models\Page $page - части URL которые относятся к дочерним страницам.
	 * @param \Zend_Controller_Request_Http $request
	 * @return array $page - дочерняя страница, callback или null
	 */
	public function route(\front\models\Page $page, $request) {
		$defaultRouter = \CMSPagesRoute::getInstance($this->getDi()->get("config"));
		$defaultRouter->setDbAdapter($this->getDb());
		$params =  $defaultRouter->match($request->getPathInfo(), $page->getLevel() + 1);
		return $params ? $params['cms_page'] : null;
	}
	
	/**
	 * Установить CSS файлы для текущего модуля. 
	 * Копирует все файлы из папки ИмяМодуля/css в public
	 * Затем у *.css файлов заменяет {url_prefix} на URL до изображений
	 */
	protected function installCSS() {
		$name = str_replace("Module_", "" , $this->getName());
		$source = dirname(__FILE__). DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'css';
		$dest = realpath($this->getDi()->get("config")->css->path).DIRECTORY_SEPARATOR .$this->getName();
		if($this->copyr($source, $dest)) {
			$files = glob($dest."/*.css");
			$this->replaceImgUrlPrefixInFiles($files);
			return true;
		} else {
			return false;
		}		
	}
	
	protected function installJS() {
		$name = str_replace("Module_", "" , $this->getName());
		$source = dirname(__FILE__). DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'js';
		$dest = realpath($this->getDi()->get("config")->js->path).DIRECTORY_SEPARATOR .$this->getName();
		if($this->copyr($source, $dest)) {
			$files = glob($dest."/*.js");
			$this->replaceImgUrlPrefixInFiles($files);
			return true;
		} else {
			return false;
		}
	}
	
	protected function replaceImgUrlPrefixInFiles($files) {
		foreach($files as $filename) {
			$content = file_get_contents($filename);
			$imgURLPrefix = str_replace("//", "/", '/'.$this->getDi()->get("config")->img->url_prefix . '/' . $this->getName());
			$content = str_replace("{url_prefix}", $imgURLPrefix, $content);
			file_put_contents($filename, $content);
		}
	}
	protected function installImages() {
		$name = str_replace("Module_", "", $this->getName());
		$source = dirname(__FILE__). DIRECTORY_SEPARATOR . $name. DIRECTORY_SEPARATOR . 'img';
		$dest = realpath($this->getDi()->get("config")->img->path).DIRECTORY_SEPARATOR .$this->getName();
		return $this->copyr($source, $dest);
	}
	protected function removeCSS() {
		$dest = realpath($this->getDi()->get("config")->css->path).DIRECTORY_SEPARATOR .$this->getName();
		$this->rmdir($dest);
	}
	protected function removeImages() {
		$dest = realpath($this->getDi()->get("config")->img->path).DIRECTORY_SEPARATOR .$this->getName();
		return $this->rmdir($dest);
	}
	protected function removeJS() {
		$dest = realpath($this->getDi()->get("config")->js->path).DIRECTORY_SEPARATOR .$this->getName();
		return $this->rmdir($dest);
	}
	protected function removeTable($name) {
		$tableFactory = $this->getTableFactory();
		$table = $tableFactory->getTableByName($name);
		if($table) {
			$pageFactory = $this->getPageFactory();
			$page = $pageFactory->findPage("obj_id = " . $table['id']);
			if($page) {
				$pageFactory->deletePage($page['id']);
			}
			$tableFactory->deleteTable($table['id']);
			
			$this->getDb()->query("DROP TABLE IF EXISTS `$name`");
		}
	}
	
	/**
	 * Удаляет страницу и всех её дочерних
	 * @param integer $id
	 */
	protected function removeCMSPage($id) {
		$factory = $this->getCMSPagesFactory();
		$page = \front\models\Page::find($id);
		$page = new \front\models\Page($page);
		
		foreach($page->getChildPages() as $childPage) {
			$this->removeCMSPage($childPage['id']);
		}
		
		$factory->deletePage($page->getID());
	}
	
	/**
	 * Рукурсивно копирует дирикторию
	 * @param string $source
	 * @param string $dest
	 * @return boolean
	 */
	protected function copyr($source, $dest) {
		if(is_dir($source)) {
			$dh = opendir($source);
			if($dh) {
	
				/* проверим, есть ли куда копировать */
				if(!file_exists($dest)) {
					if(!mkdir($dest)) {
						$this->getDi()->get("logger")->log("Can't create dir $dest");
						return false;
					}
				}
	
				while(($file = readdir($dh)) != false) {
					if($file != '.' && $file != '..') {
						$fqSourceName = $source . DIRECTORY_SEPARATOR . $file;
						$fqDestName = $dest . DIRECTORY_SEPARATOR . $file;
						if(is_dir($fqSourceName)) {
							if(!$this->copyr($fqSourceName, $fqDestName)) {
								$this->getDi()->get("logger")->log("Can't copy $fqSourceName to $fqDestName");
								return false;
							}
						}
						if(is_file($fqSourceName)) {
							if(!copy($fqSourceName, $fqDestName)) {
								$this->getDi()->get("logger")->log("Can't copy $fqSourceName to $fqDestName");
								return false;
							}
						}
					}
				}
				closedir($dh);
			}			
		}
	
		return true;
	}
	
	/**
	 * Удалить дирикторию и всё её содержимое рекурсивно
	 * @param string $dirname
	 */
	protected function rmdir($dirname) {
		if(is_dir($dirname)) {
			$dh = opendir($dirname);
			if($dh) {
				while(($filename = readdir($dh) ) != false) {
					if($filename != '.' && $filename != '..') {
						$fqFilename = $dirname . DIRECTORY_SEPARATOR . $filename;
						if(is_dir($fqFilename)) {
							$this->rmdir($fqFilename);
							rmdir($fqFilename);
						} else {
							unlink($fqFilename);
						}
					}
				}
				closedir($dh);
			}
			rmdir($dirname);
		}
	}
	
	/**
	 * Отрендерить PHP шаблоны
	 * @param string $templateName - название шаблона в поддириктории templates дириктории модуля
	 * @return string
	 */
	protected function render($templateName, $vars) {
		
		$dir = realpath(APPLICATION_PATH . '/../../front/modules/'.str_replace("Module_", "", $this->getName()).'/templates');
		
		/* @var $view \front\models\View */ 
		$view = $this->getDi()->get("View");
		$view->setTemplateDir($dir);
		
		return $view->render($templateName, $vars);
	}
	
	/**
	 * Возвращает читаемое название модуля. 
	 * 
	 * Обычно это название на кириллице. Используется для отображения пользователю.
	 * 
	 * @return string
	 */
	abstract public function getTitle();
	
	/**
	 * Если мы хотим описать детальнее работу модуля, то описание
	 * необходимо поместить в метод getDescription
	 * 
	 * @return string
	 */
	abstract public function getDescription();
	
	/**
	 *  Установка модуля. 
	 *  
	 *  По умолчанию, модули не установлены. 
	 *  Каждый модуль может требовать наличия определённой таблицы в БД, 
	 *  списка шаблонов, настроек. При установке модуля вызывается метод install(), 
	 *  который должен позаботиться о подготовке окружающей среды.
	 *  
	 *  @return boolean - true в случае успешной установки
	 */
	abstract public function install();
	
	/**
	 * Удаление модуля. 
	 * 
	 * После себя надо убрать весь мусор. Удалить созданные таблицы, настройки и т.п.
	 * 
	 * @return boolean
	 */
	abstract public function remove();
	
	/**
	 * Получить описание конфигурации модуля. 
	 * 
	 * Модуль можно настраивать. Для этого существует таблица cms_modules_config 
	 * и раздел "Настройки модуля" в CMS. Метод getConfigFields должен вернуть список
	 * конф.полей данного модуля.  
	 * Пример: 
	 * return array(
	 * 		'onpage' => array(
	 * 			'type' => 'int' //string, text, enum, 
	 * 			'title' => 'Кол-во на странице', 
	 * 			'default' => 10, 
	 * 			'values' => array(		//используется для типа enum
	 * 				'one' => 'Одна', 
	 * 				'two' => 'Две'
	 * 			)
	 * 		)
	 * )
	 * 
	 */
	abstract public function getConfigFields();
	
	/**
	 * Получить часть карты сайта.
	 * @return array
	 */
	public function getMap() {
		return array();
	}
	
	public function getBreadcrumbs() {
		return array();
	}
	
	/**
	 * Отрендерить указнный шаблон
	 * @param string $templateName - название файла шаблона
	 * @param array $data - данные, передаваемые в шаблон
	 * @return string
	 */
	protected function fetch($templateName, $data) {
		$content = $this->getTemplate($templateName, true);
		return $this->getDi()->get("lex")->parse($content, $data);
	}
}

?>