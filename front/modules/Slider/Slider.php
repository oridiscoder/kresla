<?php
namespace front\modules;

require_once ('front/modules/Module.php');

/**
 * Листалка. 
 * Нужна таблица с элементами листалки. 
 * Нужно подключить стили и JS
 * 
 * @author Gourry
 *
 */
class Slider extends Module {
	
	public function getTitle() {
		return "Слайдер";
	}
	public function getDescription() {
		return "";
	}
	public function getTableName() {
		return $this->getDbPrefix()."slider";
	}
	public function install() {
		$tableName = $this->getTableName();
		
		if(!$this->installCSS() || !$this->installImages()) {
			return false;
		}
		
		$sql = "		
		CREATE TABLE `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`number` INT UNSIGNED NOT NULL DEFAULT 0, 
			`hidden` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, 
			`title` VARCHAR(255), 
			`content` TEXT
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($tableName, "Слайдер")
			->addStringField("title", "Название")
			->addHTMLCodeField("content", "HTML");
		$tableID = $factory->getTableId();
		
		$factory = $this->getPageFactory();
		$factory->addPage("Слайдер", \PageCreator::SECTION_SITE, $tableID);
		
		$this->saveTemplate("wrap", file_get_contents(dirname(__FILE__) . DIRECTORY_SEPARATOR. "template.html"));
		
		return true;
	}
	
	public function remove() {
		
		$tableName = $this->getTableName();
		$this->getDb()->query("DROP TABLE `".$this->getTableName()."`");
		$this->removeTable($tableName);
		
		$this->removeCSS();
		$this->removeImages();
		$this->deleteTemplate("wrap");
		return true;
	}
	public function getConfigFields() {
		
	}
	public function getCSS() {
		return "slider.css";
	}
	
	public function index() {		
		$slides = $this->getDb()->fetchAll("SELECT id, content FROM `".$this->getTableName()."` WHERE hidden = 0 ORDER BY number");
		$slides = $slides ? $slides : array();
		if($slides) {
			$template = $this->getTemplate("wrap");
			$template = $this->getDi()->get("lex")->parse($template, array("slides" => $slides));
			return $template;
		}		
	}
}

?>