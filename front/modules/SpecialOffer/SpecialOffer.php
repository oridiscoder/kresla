<?php
namespace front\modules;

class SpecialOffer extends Publications {
	
	private $years;
	
	/* (non-PHPdoc)
	 * @see front\modules.Publications::getTableName()
	 */
	public function getTableName() {
		return $this->getDbPrefix().'special_offers';
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getTitle()
	 */
	public function getTitle() {
		return "Специальные предложения";		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
		
	}
	
	public function install() {
		$tableName = $this->getTableName();
		
		$sql = "DROP TABLE IF EXISTS `$tableName`";
		$this->getDb()->query($sql);
		
		$sql = "CREATE TABLE `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED DEFAULT 0,
			`hidden` TINYINT(1) DEFAULT 0,
			`title` VARCHAR(255),
			`url`	VARCHAR(255),
			`keywords` VARCHAR(500),
			`image` VARCHAR(255), 
			`description` VARCHAR(500),
			`annotation_short` VARCHAR(500),
			`annotation` TEXT,
			`text` TEXT,
			`date_create` DATETIME,
			`date_start` DATE,
			`date_stop` DATE,
			KEY(`url`),
			KEY(`date_start`, `date_stop`)
		) ENGINE=MyISAM";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($tableName, $this->getTitle())
			->addStringField('title', 'Заголовок')
			->addStringField('url', 'URL')
			->addStringField('keywords', 'Keywords', array('hidecolumn' => 1))
			->addStringField('description', 'Description', array('hidecolumn' => 1))
			->addImageField("image", "Картинка", array("draw_into" => '717x206'))
			->addTextField('annotation_short', 'Коротко')
			->addHTMLField('annotation', 'Аннотация')
			->addHTMLField('text', 'Текст', array('hidecolumn' => 1))
			->addDateField('date_create', 'Дата создания', array('hidecolumn' => 1))
			->addDateField('date_start', 'Дата начала')
			->addDateField('date_stop', 'Дата окончания');
		
		$pageFactory = $this->getPageFactory();
		$pageFactory->addPage($this->getTitle(), 2, $factory->getTableId());
		
		return true;
	}
	
	public function getConfigFields() {
		$fields = parent::getConfigFields();
		$fields['order']['default'] = 'number';
		unset($fields['order']['values']['date_publish DESC']);
		$fields['base_url']['default'] = '/special_offers';
		return $fields;
	}
	
	/**
	 * Получит список лет, в которых у нас есть публиковались новости
	 * @return array [2012, 2011, 2010]
	 */
	public function getYears() {
		if($this->years === null) {
			$sql = "SELECT DISTINCT YEAR(date_create) as year FROM `".$this->getTableName()."` ORDER BY year DESC";
			$this->years = $this->getDb()->fetchCol($sql);
			$this->years = $this->years ? $this->years : array();
		}
		return $this->years;
	}
	
	/**
	 * С каким годом сейчас работаем
	 */
	public function getCurrentYear() {
		/* @var $request Bof_Request */
		$request = $this->getDi()->get("request");
		$year = $request->getParam("year");
		$year = $year ? $year : $this->getLastYear();
		return $year;
	}
	
	public function getLastYear() {
		if(!$this->years) {
			$date = $this->getDb()->fetchOne("SELECT MAX(date_create) FROM `".$this->getTableName()."` WHERE hidden = 0");
			$date = $date ? strtotime($date) : time();
			$year =  date('Y', $date);
		} else {
			$year = $this->years[0];
		}
		return $year;
	}
	
	public function yearListHTML($options, $content) {
		
		/* @var $lp \front\models\LightParser */
		$lp = $this->getDi()->get("lightParser");
		
		$templates = $lp->extractTemplates($content);
		
		$currentYear = $this->getCurrentYear();
		$lastYear = $this->getLastYear();
		$years = $this->getYears();
		$html = '';
		if($years) {
			foreach($years as $year) {
				if($year == $currentYear) {
					$template = $templates['active'];
				} else {
					$template = $templates['normal'];
				}
				
				/* @var $page Page */
				$page = $this->getDi()->get("page");
				$url = str_replace("//", "/", '/'.$page->getSectionUrl().'/'.$year.'/');
				
				$html .= $lp->insertValues($template, array(
					'title' => $year, 
					'url' => $url
				));
			}
		}
		
		return $html;
	}
	
	public function getBaseURL() {
		return $this->getConfig("base_url");
	}
	
	public function prepareSelect($selectFields = null) {
		$selectFields = $selectFields ? $selectFields : array('id', 'title', 'date_create', 'date_start', 'date_stop','annotation', 'url');
		$select = $this->getDb()->select()
			->from($this->getTableName(), $selectFields)
			->where("hidden = 0")
			->where("YEAR(date_create) = ?", $this->getCurrentYear());
		return $select;
	}
	
	
	public function renderList($options, $content) {
		$items = $this->getList(array(
			"year" => $this->getRequest()->getParam("year", $this->getLastYear())
		));
		
		/* пометим активные предложения */
		foreach ($items as $key => $item) {
			$items[$key]['_is_active'] = $this->isActiveOffer($item);
		}
		
		$template = $content;
		
		return $this->getDi()->get("lex")->parse($template, array("items" => $items));
	}
	
	protected function isActiveOffer($offer) {
		
		$start = strtotime($offer['date_start']);
		$stop = strtotime($offer['date_stop']);
		
		if($offer['date_stop']) {			
			return time() <= $stop && time() >= $start;
		} else {
			return time() >= $start;
		}		
	}
	
	protected function getOfferDatesName($offer) {
		if($this->isActiveOffer($offer)) {
			if($offer['date_stop']) {
				$stopTime = strtotime($offer['date_stop']);
				$stopYear = date('Y', $stopTime);
				$stopMonth = date('n', $stopTime);
				$stopMonth = $this->getDi()->get("dateHelper")->monthToParentCase($stopMonth);
				return "Предложение действительно до $stopMonth $stopYear г.";
			} else {
				return "Бессрочное предложение";
			}
		} else {
			list($startMonth, $startYear) = explode("-", date('n-Y', strtotime($offer['date_start'])));
			list($stopMonth, $stopYear) = explode("-", date('n-Y', strtotime($offer['date_stop'])));
			/* @var $dh \front\Models\DateHelper */
			$dh = $this->getDi()->get("dateHelper");
			
			if($startMonth.'.'.$startYear == $stopMonth.'.'.$stopYear ) {
				return sprintf("%s %sг.", $dh->monthName($startMonth), $startYear);
			} else {
				return sprintf("%s %sг. - %s %sг.", $dh->monthName($startMonth), $startYear, $dh->monthName($stopMonth), $stopYear);
			}
		}
	}
	
	public function route(\front\models\Page $page, $request) {
		$result = parent::route($page, $request);
		$name = strtolower(str_replace("Module_", "", $this->getName()));
		if(isset($result[$name])) {
			$result[$name]['date_html'] = $this->getOfferDatesName($result[$name]);
			if($result[$name]['image']) {
				require_once "models/Table/DbTableLight.php";
				$dbTable = new \DbTableLight($this->getDb(), $this->getTableName());
				/* @var $imgField \ImageField */
				$imgField = $dbTable->getField("image");
				$result[$name]['image'] = $imgField->getURL($result[$name]);
				$result[$name]['has_image'] = true;
			} else {
				$result[$name]['has_image'] = false;
			}
		}
		return $result;
	}
	
	public function getList($options, $content = null) {
		//если хотим без ограничения, то указываем 0
		$onpage = isset($options['onpage']) ? $options['onpage'] : $this->getConfig("onpage");
		$order = isset($options['order']) ? $options['order'] : $this->getConfig("order");
		
		$pageNumber = isset($options['page_number']) ? $options['page_number'] : 1;
		$pageNumber = $this->getRequest()->getParam("p", $pageNumber);
		
		$select = $this->getDb()->select()
			->from($this->getTableName())
			->where("hidden = 0")
			->order($order);		
		
		if($onpage) {
			$offset = $pageNumber - 1 * $onpage;
			$offset = $offset < 0 ? 0 : $offset;
			$select->limit($onpage, $offset);
		}
		
		if(isset($options['active'])) {
			$select->where("date_start <= CURDATE() AND date_stop >= CURDATE()");
		}
		if(isset($options['year'])) {
			$select->where("YEAR(date_create) = ?", $options['year']);
		}
		
		$rows = $this->getDb()->fetchAll($select);
		$rows ? $rows : array();
		
		/* преобразуем урлы картинок в абсолютные */
		$rows = $this->getDbTableLight($this->getTableName())->expandImageUrls($rows);
		$rows = $this->expandUrls($rows);
		
		return $rows;
	}
	
	protected function expandUrls($rows) {
		foreach ($rows as $key => $row) {
			if(!$this->isAbsoluteUrl($row['url'])) {
				$url = empty($row['url']) ? $row['id'].'.html' : $row['url'] . '.html';
				$rows[$key]['url'] = $this->getConfig("base_url") . '/' . $url;
			}
		}
		
		return $rows;
	}
	
	/**
	 * Можно ли использовать текущий URL как есть (т.е. либо он абсолютный, либо начинается со слеша)
	 * @param string $url
	 * @return boolean
	 */
	protected function isAbsoluteUrl($url) {
		return !empty($url) && (substr($url, 0, 7) == "http://" || $url[0] == '/'); 
	}
	
	public function getSliderBlock($options, $content) {
		$items = $this->getList(array("active" => true));
		$template = $this->getTemplate("slider.html", true);
		return $this->getDi()->get("lex")->parse($template, array("items" => $items));
	}
	

}

?>