function Comments(formObject) {
	var self = this;
	var form = $(formObject);
	
	form.bind("submit", this.submit);
	
	this.submit = function () {
		var code = $('<input>', {type: "hidden", name:"captcha", value:this.generateCode()});
		form.append(code);
		
		var data = form.serialize();
		this.fetchRemote(data, function (html) {
			var data = eval('(' + html + ')');
			if(data.errors) {
				alert("Ошибки");
			} else {
				alert("OK");
			}
		});
		
		return false;
	}
	
	this.generateCode = function (length = 5) {
		var chars = "ABCDEFGHIJKLMNOPQRSTUVWXUZ";		
		var code = "";
		for(var i = 0; i < length; i++) {
			var key = Math.random() * (chars.length - 1);
			code += chars[key];
		}
		return code;
	}
	
	this.fetchRemote = function (data, callback) {
		var url = form.attr("action");
		var method = "POST";	//always post
		this.startAjax();
		$.post(url, data, function (html) {
			self.stopAjax();
			callback(html);
		});
	};
	
	this.startAjax = function () {
		var button = form.find('input[type="submit"]');
		button.attr("disabled", "disabled");
		button.attr("rel", button.attr("value"));
		button.attr("value", "Идет отправка");
	};
	
	this.stopAjax = function () {
		var button = form.find('input[type="submit"]');
		button.removeAttr("disabled");
		button.attr("value", button.attr("rel"));
		button.removeAttr("value");	//это может быть не лучшим вариантом
	}
	
}