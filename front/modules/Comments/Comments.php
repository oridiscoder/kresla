<?php
namespace front\modules;

require_once ('front/modules/Module.php');

class Comments extends \front\modules\Module {
	/* (non-PHPdoc)
	 * @see front\modules.Module::getTitle()
	 */
	public function getTitle() {
		return "Комментарии";	
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
		"Модуль для создания комментариев к объектам из заданной таблицы";		
	}
	
	public function getCommentsTableName() {
		return $this->getDbPrefix()."comments";
	}
	
	private $commentsCount = array();
	
	private $settings;

	/* (non-PHPdoc)
	 * @see front\modules.Module::install()
	 */
	public function install() {
		$tableName = $this->getCommentsTableName();
		$sql = "DROP TABLE IF EXISTS `$tableName`;";
		$this->getDb()->query($sql);
		
		$sql = "
		CREATE TABLE `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`number` INT UNSIGNED NOT NULL DEFAULT 0, 
			`hidden` TINYINT(1) NOT NULL DEFAULT 0, 
			`object_id` INT UNSIGNED NOT NULL, 
			`object_id__table` VARCHAR(20) NOT NULL, 
			`name` VARCHAR(100), 
			`date` DATETIME, 
			`grade` TINYINT DEFAULT 0, 
			`email` VARCHAR(100), 
			`text` VARCHAR(2000), 
			`show` TINYINT(1) DEFAULT 0
		) ENGINE=MyISAM ";
		$this->getDb()->query($sql);

		$factory = $this->getTableFactory();
		$factory->addTable($tableName, "Комментарии")
				->addIntegerField("object_id", "Объект")
				->addStringField("object_id__table", "Тип объекта")
				->addBooleanField("show", "Показывать")
				->addStringField("name", "Имя")
				->addDateField("date", "Дата")
				->addIntegerField("grade", "Оценка")
				->addEmailField("email", "E-Mail")
				->addTextField("text", "Текст");
		$tableID = $factory->getTableId();
		
		$this->getPageFactory()->addPage("Комментарии", \PageCreator::SECTION_SITE, $tableID);
		
		$this->installJS();
		
		return true;
		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::remove()
	 */
	public function remove() {
		$this->removeTable($this->getCommentsTableName());
		$this->removeJS();
		return true;
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getConfigFields()
	 */
	public function getConfigFields() {
		
	}
	
	
	public function form($options, $content) {
		/* @var $formModule \front\modules\Form */
		$formModule = $this->getDi()->getModule("Form");
		$options['comments_table_name'] = $this->getCommentsTableName();
		$options['form_name'] = "comment";
		
		return $formModule->render($options, $content);
	}
	
	public function initSettings($settings, $content) {
		$this->settings = $settings;
	}
	
	public function getList($options, $content) {
		
		$onpage = isset($options['onpage']) ? $options['onpage'] : 0;
		$pageNumber = isset($options['pageNumber']) ? $options['pageNumber'] : 1;
		$order = isset($options['order']) ? $options['order'] : "date DESC";
		
		$select = $this->getDb()->select()
			->from($this->getCommentsTableName())
			->where("object_id__table = ?", $this->settings['object_id__table'])
			->where("object_id = ?", $this->settings['object_id'])
			->order($order);
		
		if($onpage) {
			$select->limit($onpage, ($pageNumber - 1) * $onpage);
		}
		
		if(isset($this->settings['moderate']) && $this->settings['moderate']) {
			$select->where("`show` = 1");
		}
		
		$rows = $this->getDb()->fetchAll($select);
		
		if($content) {
			/* если шаблон уже задан, но сразу применяем его к полученным строкам */
			return $this->getDi()->get("lex")->parse($content, array("comments" => $rows));
		} else {
			return $rows;
		}
	}
	
	public function count($options, $content) {
		
		$type = $this->settings['object_id__table'];
		$objectID = $this->settings['object_id'];
		if(!isset($this->commentsCount[$type][$objectID])) {
			$select = $this->getDb()->select()
				->from($this->getCommentsTableName(), array("count" => "COUNT(*)"))
				->where("object_id__table = ?", $type)
				->where("object_id = ?", $objectID);
			
			if(isset($this->settings['moderate']) && $this->settings['moderate']) {
				$select->where("`show` = 1");
			}
			
			$this->commentsCount[$type][$objectID] = $this->getDb()->fetchOne($select);
		}
		
		return $this->commentsCount[$type][$objectID];
	}
	
	public function moreLink($options, $content) {
		$count = $this->count($options, $content);
		$limit = $options['limit'];
		
		if($count > $limit) {
			$moreCount = $count - $options['limit'];
		
			return $this->getDi()->get("lex")->parse($content, array("count" => $moreCount));
		}
	}


}

?>