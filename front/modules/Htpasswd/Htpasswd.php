<?php
namespace front\modules;

class Htpasswd extends Module{
	public function getTitle(){
		return "htpasswd авторизация";
	}
	public function getDescription(){
		return "";
	}
	public function install() {
		
		return true;
	}
	
	public function remove() {
		return true;
	}
	public function getConfigFields(){
		return array();
	}
	
	public function auth($params){
		$params['message'] = (isset($params['message']))? $params['message']: 'Error';
		
		
		$Message = $params['message'];
		$Realm = $this->translit( $Message);
		//$Message = iconv('utf-8', 'windows-1251', $Message);
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
					
			header('HTTP/1.0 401 Unauthorized');
			header('Content-Type: text/html; charset=utf-8');
			header('WWW-Authenticate: Basic realm="'.$Realm.'"');
			
			header("Pragma: no-cache");
			echo $Message;
			exit;
		} else {
			if($_SERVER['PHP_AUTH_USER']==$params['login']
			&& $_SERVER['PHP_AUTH_PW']==$params['pas']){
				
			}else{
				header('Charset: utf-8');
				echo $Message;
				exit;
			}
			
		}
		
	}
	
	public  function translit($string){
        $rus = array("/а/", "/б/", "/в/",
 "/г/", "/ґ/", "/д/", "/е/", "/ё/", "/ж/", 
"/з/", "/и/", "/й/", "/к/", "/л/", "/м/", 
"/н/", "/о/", "/п/", "/р/", "/с/", "/т/", 
"/у/", "/ф/", "/х/", "/ц/", "/ч/", "/ш/", 
"/щ/", "/ы/", "/э/", "/ю/", "/я/", "/ь/", 
"/ъ/", "/і/", "/ї/", "/є/", "/А/", "/Б/", 
"/В/", "/Г/", "/ґ/", "/Д/", "/Е/", "/Ё/", 
"/Ж/", "/З/", "/И/", "/Й/", "/К/", "/Л/", 
"/М/", "/Н/", "/О/", "/П/", "/Р/", "/С/", 
"/Т/", "/У/", "/Ф/", "/Х/", "/Ц/", "/Ч/", 
"/Ш/", "/Щ/", "/Ы/", "/Э/", "/Ю/", "/Я/", 
"/Ь/", "/Ъ/", "/І/", "/Ї/", "/Є/");
        $lat = array("a", "b", "v", 
"g", "g", "d", "e", "e", "zh", "z", "i", 
"j", "k", "l", "m", "n", "o", "p", "r", 
"s", "t", "u", "f", "h", "c", "ch", "sh", 
"sh'", "y", "e", "yu", "ya", "'", "'", "i", 
"i", "e",    "A", "B", "V", "G", "G", "D", 
"E", "E", "ZH", "Z", "I", "J", "K", "L", 
"M", "N", "O", "P", "R", "S", "T", "U", 
"F", "H", "C", "CH", "SH", "SH'", "Y", "E", 
"YU", "YA", "'", "'", "I", "I", "E");
        return preg_replace($rus, $lat, $string);
    } 
}