
        
			<div class="CatalogTopBord">
            	<h2 class="catalogZagolovok">Корзина</h2>
                <p><br /><br /></p>                                	
                    
                <div class="cart-table">
                <form action="/basket/" method="post" id="BasketForm">            	
                	<table cellpadding="0" cellspacing="0" width="100%">                                        
                    <tr>
                    <td class="l-space"> </td>

                    <td width="65" class="ct-title">Фото</td>
                    <td class="ct-title">Модель</td>
                    <td class="ct-title">Вариант обивки</td>
                    <td class="ct-title">Декор</td>
                    <td class="ct-title">Цена</td>
                    <td class="ct-title">Количество</td>

                    <td class="ct-title"> </td>
                    <td class="l-space"> </td>
                    </tr>
                    
<?php foreach($items as $item) : ?>
<tr class="cart-table-m" id="tr<?=$item['id']?>">
	<td class="l-bor">
		<img src="/img/ctleft.gif" width="10" height="72" alt="" />
	</td>
	<td>
		<a href="/catalog/<?=$item['_url']?>"><img src="<?=$item['thumb']?>"  /></a>
	
	</td>
	<td>
		<a href="/catalog/<?=$item['_url']?>"><?=$item['title']?></a>
	</td>
	<td>
	<?php if($item['_upholstery']): ?>
		<table><tr>
			<td style="border:none">
				<img height="54" width="54" src="/img/_ss2<?=$item['_upholstery']['on_order'] ? '_onorder' : ''?>.png" class="ss">
				<img src="<?=$item['_upholstery']['image']?>" height="54" width="54" class="im-left" style="float:none;"/>
			</td>
			<td style="border:none; margin:0; padding:0;">
				<div class="cart-description">Артикул: <span><?=$item['_upholstery']['artikul']?></span><br />Цвет: <span><?=$item['_upholstery']['title']?></span></div>
			</td>
		</tr></table>
	<?php else: ?>
		<table><tr><td style="border:none"></td>
		<td style="border:none; margin:0; padding:0;">
		<div class="cart-description">Без обивки</div>
		</td></tr></table>
	<?php endif; ?>
	</td>
	
	<td>
	<? if($item['_arm']):?>
	<table><tr>
			<td style="border:none">
				<img height="54" width="54" src="/img/_ss4.png" class="ss">
				<img src="<?=$item['_arm']['image']?>" height="54" width="54" class="im-left" style="float:none;"/>
			</td>
			<td style="border:none; margin:0; padding:0;">
				<div class="cart-description">Артикул: <span><?=$item['_arm']['artikul']?></span><br />Цвет: <span><?=$item['_arm']['title']?></span></div>
			</td>
		</tr></table>
	<? else: ?>
	<table><tr><td style="border:none"></td>
		<td style="border:none; margin:0; padding:0;">
		<div class="cart-description">Стандартный декор</div>
		</td></tr></table>
	<? endif;?>
	
	</td>
	<!-- td>
		<table><tr><td style="border:none"></td>
		<td style="border:none; margin:0; padding:0;">
		<div class="cart-description">Стандартный декор</div>
		</td></tr></table>
	</td-->
	<td><div class="cart-cost"><span><?=$item['_price_current']?></span> руб.</div></td>
	<td align="center" width="100"><div class="cart-input">
		<input class="basket_item_input" type="text" name="good[<?=$item['_basket_key']?>]" value="<?=$item['_count']?>" /> шт</div></td>
	<td align="center"><a href="javascript:void(0);" class="basket_remove_item">Удалить</a></td>
	<td class="r-bor"><img src="/img/ctright.gif" width="10" height="72" alt="" /></td>
</tr>
<tr><td colspan="8"> </td></tr>
<?php endforeach; ?>
<tr>
	<td colspan="5"> </td>
	<td class="cart-calc"><span id="Recalc">Пересчитывается...</span></td>
	<td colspan="2"> </td>
</tr>

<tr><td colspan="9"> </td></tr>

<?php if ($accessories): ?>
<!--remember-->
<tr>
                                    <td colspan="9">
                                    <?=count($accessories)?>
                                         
                                    </td>
                              </tr>
<!--remember-->
<?php endif; ?>
                    <tr class="cart-table-m">
                    <td class="l-bor"><img src="/img/ctleft.gif" width="10" height="72" alt="" /></td>
                    <td colspan="7" class="cost-itog">Итого: <span class="cost"><?=$totalCost?></span> руб.</td>
                    <td class="r-bor"><img src="/img/ctright.gif" width="10" height="72" alt="" /></td>
                    </tr>
                    
                    </table>
                    </form>
                </div>  
                <br />
<?php if($hideButtons == false): ?>
                <div class="im-left"><a href="/catalog/office/office_armchairs/" class="goback-link">Продолжить покупки</a></div>
                <div class="im-right"><a id="MakeOrderButton"  href="/basket/order_individual/" onclick="_gaq.push(['_trackPageview', '/step1.html']); return true;"><img src="/img/placeorderbut.gif" width="329" height="86" alt="" /></a></div>
<?php endif; ?>
<div class="clear"></div> 
<div class="CatalogTopBord">
                                                <h2 class="catalogZagolovok">Не забудьте купить:</h2>
                                                <div class="proWrap">
                                                      <table class="tableWrap" cellpadding="0" cellspacing="0" width="100%">
                                                      <?php for($i = 0; $i < count($accessories); $i+=4): ?>
                                                      <tr>
                                                      <?php for($j = $i; $j < $i + 4 && $j < count($accessories); $j++):
                                                      $good = $accessories[$j]; 
                                                      ?>
                                                                  <td>
                                                                        <div class="pro">
                                                                              <div class="proPic"><a href="/catalog/<?=$good['_url'];?>"><img src="<?=$good['thumb'];?>" alt="<?=$good['title'];?>"/></a></div>
                                                                              <div class="proInfo">
                                                                                    <div><a href="/catalog/<?=$good['_url'];?>"><?=$good['title'];?></a></div>
                                                                                    <div class="proPrice"><?=$good['price'];?> руб.</div>
                                                                              </div>
                                                                        </div>
                                                                  </td>
                                                      <?php endfor; ?>
                                                      </tr>
                                                      <?php endfor; ?>
                                                      </table>
                                                </div>
                                          </div>
                <div class="clear"></div>
			</div>
			