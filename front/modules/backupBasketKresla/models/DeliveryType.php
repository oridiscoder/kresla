<?php
namespace front\modules\BasketKresla\models;

use front\models\DbRecord;

class DeliveryType extends DbRecord {
	
	public function getPrice($orderCost) {
		if($orderCost >= $this->bound) {
			return intval($this->low_cost);
		} else {
			return intval($this->high_cost);
		}
	}
	
	public function getPriceTitle($orderCost) {
		if($orderCost >= $this->bound) {
			return $this->low_cost;
		} else {
			return $this->high_cost;
		}
	}
}

?>