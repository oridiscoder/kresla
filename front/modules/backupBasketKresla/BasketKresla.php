<?php
namespace front\modules;

use front\modules\BasketKresla\models\CookieBasket;

use front\modules\CatalogKresla\models\Category;

use \front\modules\CatalogKresla\models\Good;

class BasketKresla extends Basket {
	
	/**
	 * 
	 * @var \front\modules\BasketKresla\models\CookieBasket
	 */
	private $cookieBasket;
	
	public function getTitle() {
		return "Корзина kresla-otido.ru";
	}
	
	public function getBasketOrdersTableName() {
		return $this->getDbPrefix()."basket_orders";
	}
	public function getBasketGoodsTableName() {
		return $this->getDbPrefix()."basket_goods";
	}
	public function getBasketDeliveryTypeTableName() {
		return $this->getDbPrefix()."basket_delivery_types";
	}
	public function getBasketPaymentTypeTableName() {
		return $this->getDbPrefix()."basket_payment_types";
	}
	public function getGoodsTableName() {
		return $this->getDi()->getModule("CatalogKresla")->getGoodsTableName();
	}
	public function getGoodUpholsteriesTableName() {
		return $this->getDi()->getModule("CatalogKresla")->getGoodUpholsteryTableName();
	}
	public function getUpholsteryTableName() {
		return $this->getDi()->getModule("CatalogKresla")->getUpholsteryTableName();
	}
	
	public function install() {
		
		/* типы доставки */
		$deliveryTypeTableName = $this->getBasketDeliveryTypeTableName();
		$sql = "CREATE TABLE IF NOT EXISTS $deliveryTypeTableName (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0, 
			`title` VARCHAR(100),
			`low_cost` VARCHAR(100), 
			`high_cost` VARCHAR(100),
			`bound` DOUBLE DEFAULT 15000 
		)";
		$this->getDb()->query($sql);
		$this->getDb()->query("TRUNCATE TABLE `$deliveryTypeTableName`");
		$this->getDb()->query("INSERT INTO $deliveryTypeTableName (number, title, low_cost, high_cost, bound) VALUES
		(1, 'Москва в пределах МКАДа', 'бесплатно', '500 руб', 15000), 
		(2, 'Москва и МО за МКАД до 30 км.', 'Рассчитывается менеджером', 'Рассчитывается менеджером', 15000),
		(3, 'Самовывоз со склада (ул. Нижегородская, 70)', '', '', 15000);");
		
		$factory = $this->getTableFactory();
		$factory->addTable($deliveryTypeTableName, "Типы доставки")
			->addStringField("title", "Название")
			->addStringField("low_cost", "Цена со скидкой")
			->addStringField("high_cost", "Обычная цена")
			->addStringField("bound", "Пороговая цена", array("hint" => "Если заказ стоит больше этой величины, то подставляем цену со скидкой"));
		$deliveryTypeTableID = $factory->getTableId();
		
		/* типы оплаты */
		$paymentTypeTableName = $this->getBasketPaymentTypeTableName();
		$sql = "CREATE TABLE IF NOT EXISTS $paymentTypeTableName (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(50), 
			`slug` VARCHAR(10)
		)";
		$this->getDb()->query($sql);
		$this->getDb()->query("TRUNCATE TABLE `$paymentTypeTableName`");
		$this->getDb()->query("INSERT INTO $paymentTypeTableName (number, title, slug) VALUES
				(1, 'Наличными курьеру', 'nal'),
				(2, 'Квитанция на оплату в банк', 'beznal')"
		);
		
		$factory = $this->getTableFactory();
		$factory->addTable($paymentTypeTableName, "Типы оплаты")
			->addStringField("title", "Название")
			->addStringField("slug", "Код");
		$paymentTypeTableID = $factory->getTableId();
		
		/* заказы */
		$ordersTableName = $this->getBasketOrdersTableName();
		$sql = "CREATE TABLE IF NOT EXISTS $ordersTableName (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0, 
			`number` INT UNSIGNED DEFAULT 0,
			`name` VARCHAR(100) NOT NULL, 
			`date` DATETIME, 
			`delivery_type_id` INTEGER UNSIGNED, 
			`payment_type_id` INTEGER UNSIGNED,
			`client_type` ENUM('individual','juridical'), 
			`client_name` VARCHAR(100), 
			`client_email` VARCHAR(50), 
			`client_phone_number` VARCHAR(50), 
			`client_cellphone` VARCHAR(50), 
			`delivery_address` VARCHAR(300), 
			`company_name` VARCHAR(300), 
			`company_inn` VARCHAR(20), 
			`company_kpp` VARCHAR(34), 
			`comment` VARCHAR(3000), 
			`file` VARCHAR(100), 
			`complete` TINYINT(1) DEFAULT 0
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($ordersTableName, "Заказы")
				->addStringField("name", "Номер")
				->addDateField("date", "Дата")
				->addDescendantField("delivery_type_id", "Тип доставки", $deliveryTypeTableID)
				->addDescendantField("payment_type_id", "Тип оплаты", $paymentTypeTableID)
				->addStringField("client_type", "Тип клиента")
				->addStringField("client_name", "Контактное лицо")
				->addStringField("client_email", "E-Mail")
				->addStringField("client_phone_number", "Тел.")
				->addStringField("cleint_cellphone", "Тел.моб.")
				->addStringField("delivery_address", "Адрес доставки")
				->addStringField("company_name", "Название компании")
				->addStringField("company_inn", "ИНН")
				->addStringField("company_kpp", "КПП")
				->addStringField("comment", "Комментарий")
				->addFileField("file", "Файл")
				->addBooleanField("complete", "Сформирован", array("hint" => "Клиент прошел все этапы оформления заказа"));
				
		$ordersTableID = $factory->getTableId();

		/* товары в заказе */
		$basketGoodsTableName = $this->getBasketGoodsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS $basketGoodsTableName (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0, 
			`number` INT UNSIGNED DEFAULT 0,
			`order_id` INT UNSIGNED NOT NULL, 
			`good_id` INT UNSIGNED NOT NULL, 
			`upholstery_id` INTEGER UNSIGNED NOT NULL, 
			`count` INTEGER UNSIGNED NOT NULL DEFAULT 0,
			`price` DOUBLE DEFAULT 0  
		)";
		$this->getDb()->query($sql);
		
		/* получим таблицу товаров. Нам надо знать её ID */
		$goodsTable = $factory->getTableByName($this->getGoodsTableName());
		$upholsteryTable = $factory->getTableByName($this->getUpholsteryTableName());
		
		$factory = $this->getTableFactory();
		$factory->addTable($basketGoodsTableName, "Товары заказа")
			->addDescendantField("order_id", "Заказ", $ordersTableID)
			->addDescendantField("good_id", "Товар", $goodsTable['id'])
			->addDescendantField("upholstery_id", "Обивка", $upholsteryTable['id'])
			->addIntegerField("count", "Количество")
			->addStringField("price", "Цена");
		$basketGoodsTableID = $factory->getTableId();
		
		/* добавим страницы */
		$ordersPageID =  $this->getPageFactory()->addPage("Заказы", \PageCreator::SECTION_SITE, $ordersTableID);
		$this->getPageFactory()->addPage("Типы оплаты", \PageCreator::SECTION_SITE, $paymentTypeTableID, null, $ordersPageID);
		$this->getPageFactory()->addPage("Способы доставки", \PageCreator::SECTION_SITE, $deliveryTypeTableID, null, $ordersPageID);
		$this->getPageFactory()->addPage("Товары заказа", \PageCreator::SECTION_SITE, $basketGoodsTableID, null, $ordersPageID);
		
		
		$this->installJS();
		return true;
	}
	
	
	public function getConfigFields() {
		return array(
			"cookie_name" => array(
				'title' => "Название куки", 
				"type" => "string", 
				"default" => "basket"
			),
			"order_cookie_name" => array(
				'title' => "Название куки для хранения order_id",
				"type" => "string",
				"default" => "order_id"
			),
			"cookie_lifetime" => array(
				'title' => "Время жизни куки",
				"type" => "integer",
				"default" => 3600 * 24 * 30
			), 
			"system_email" => array(
				"title" => "E-Mail (куда отправляем заказы)", 
				"type" => "string", 
				"default" => "" 
			),
			"system_email2" => array(
					"title" => "E-Mail2 (куда отправляем заказы)",
					"type" => "string",
					"default" => ""
			)
		);
	}
	
	public function remove() {
		$tables = array();
		$tables[] = $this->getBasketDeliveryTypeTableName();
		$tables[] = $this->getBasketPaymentTypeTableName();
		$tables[] = $this->getBasketOrdersTableName();
		$tables[] = $this->getBasketGoodsTableName();
		
		foreach($tables as $table) {
			$this->removeTable($table);
		}
		
		$this->removeJS();
		return true;
	}
	
	/**
	 * Получить модель для работы с Куки-корзиной
	 * @return \front\modules\BasketKresla\models\CookieBasket
	 */
	public function getCookieBasket() {
		if($this->cookieBasket == null) {
			$this->cookieBasket = new CookieBasket($this->getConfig("cookie_name"), $this->getConfig("order_cookie_name"), $this->getConfig("cookie_lifetime"));
		}
		return $this->cookieBasket;
	}
	
	public function previewBlock($options, $content = null) {
		$basket = isset($options['basket']) ? $options['basket'] : $this->getCookieBasket()->getItems();
		$returnRawData = isset($options['return_raw_data']) ? $options['return_raw_data'] : false;
		$template = $this->getTemplate("basket_head.html", true);
		
		$goodsCount = 0;
		$totalCost = 0;
		
		if($basket) {
			foreach ($basket as $item) {
				$good = new Good($item['id']);
				$good->setActiveUpholstery($item['upholstery_id']);
				$price = $good->getPrice();
				$goodsCount += $item['count'];
				$totalCost += $price * $item['count'];
			}
		}		
		
		$basketInfo = array(
			"goodsCount" =>  $goodsCount, 
			"totalCost" => $totalCost
		);
		
		if($returnRawData) {
			return $basketInfo;
		} else {
			return $this->getDi()->get("lex")->parse($template, $basketInfo);
		}
	}
	
	private function getBasketGoodsIds() {
		return array();
	}
	
	public function basket($options = array(), $content = null) {
		$request = $this->getRequest();
		if($request->isPost()) {
			/* добавляем в корзину товар */
			if($request->getPost("action") == 'basket_add') {
				$item = array(
					"id" => $request->getPost("id"), 
					"upholstery_id" => $request->getPost("upholstery_id"),
					"arm_id" => $request->getPost("arm_id"),
					"count" => $request->getPost("count")
				);
				
				$this->getCookieBasket()->addItem($item);
				header("Location: /basket/");
				exit();
			}
			
			if($request->getPost("action") == "recalculate") {
				$counts = $request->getPost("good");
				$this->getCookieBasket()->setCounts($counts);
				$result = $this->previewBlock(array("return_raw_data" => true));
				echo json_encode($result);
				exit();
			}
			
			if($request->getPost("action") == "remove") {
				$goodID = $request->getPost("good_id");
				$upholsteryID = $request->getPost("upholstery_id");
				
				$basket = $this->getBasketGoodsIds();
				unset($basket[$goodID][$upholsteryID]);
				$this->saveBasket($basket);
				
				$result = $this->previewBlock(array("basket" => $basket, "return_raw_data" => true));
				echo json_encode($result);
				exit();
			}
			
		} else {
			/* мы просто открыли страницу корзины */
			$basket = $this->getCookieBasket()->getItems();
			
			$basketItems = array();
			$categories = array();
			
			if($basket) {
				
				$goods = array();
				$totalCost = 0;

				/* надо сформировать данные для отображения на странице */
				foreach ($basket as $bk => $item) {
					
					/* товары из корзины не удаляются, просто их кол-во = 0 */
					if($item['count']) {
						$good = new Good($item['id']);
						$good->setActiveUpholstery($item['upholstery_id']);
						
						$good->setActiveArm($item['arm_id']);
					
						$categories = \Utils::mergeUnique($categories, $good->getCategories());
						
						$basketItem = $good->asArray();
						
						/* добавим вычисляемые поля */
						$basketItem['_basket_key'] = $bk;
						$basketItem['_upholstery'] = $good->getActiveUpholstery();
						$basketItem['_arm'] = $good->getActiveArm();
						$basketItem['_count'] = $item['count'];
						$basketItem['_price_current'] = $good->getPriceActive();
						$basketItem['_cost'] = $item['count'] * $good->getPriceActive();
						$basketItem['_url'] = $good->getFullURL();
						$basketItem['thumb'] = \front\models\Di::getInstance()->getModule('image')->getImage(array('src'=>"goods.thumb.{$basketItem['id']}.55x60", 'name'=>$basketItem['thumb']));
						
						$basketItems[] = $basketItem;
							
						$totalCost += $basketItem['_cost'];
					}
				}
				
				//$basketItems = Good::expandImagesUrls($basketItems, $this->getGoodsTableName());
				
				$showAccessories = isset($options['show_accessories']) ? $options['show_accessories'] : false;

				if($showAccessories) {
					/* теперь нам надо собрать сопутсвующие товары */
					$accessories = array();
					foreach ($categories as $category) {
						$categoryObject = new Category($category['id']);
						$accessories = \Utils::mergeUnique($accessories, $categoryObject->getAttendantGoods());
					}
					
					/* уберем из сопутсвующих те товары, которые уже лежат в корзине */
					$goodsIds = \Utils::extractColumn($basketItems, "id");
					foreach ($accessories as $key => $row) {
						if(in_array($row['id'], $goodsIds)) {
							unset($accessories[$key]);
						}else{						
							$accessories[$key]['thumb'] = \front\models\Di::getInstance()->getModule('image')->getImage(array('src'=>"goods.thumb.{$row['id']}.65x65", 'name'=>$row['thumb']));
						}
					}
					
					$accessories = array_values($accessories);
					
					//$accessories = Good::expandImagesUrls($accessories, $this->getGoodsTableName());
					$accessories = Good::hitchData($accessories, array("url"));
				}
				
				return $this->render("basket.php", array(
					"items" => $basketItems, 
					"totalCost" => $totalCost,
					"accessories" => $showAccessories ? $accessories : array(), 
					"hideButtons" => isset($options['hide_buttons']) ? $options['hide_buttons'] : false
				));
			}
		}
	}
	
	public function orderForm($options, $content = null) {
		
		/* сформировать заказ */
		$order = $this->getOrder();
		
		$basket = $this->previewBlock(array('return_raw_data' => true));
		
		$data = array(
			"order" => $order, 
			"deliveryTypes" => $this->getDeliveryTypes(), 
			"deliveryTypesJson" => json_encode($this->getDeliveryTypes()),
			"paymentTypes" => $this->getPaymentTypes(), 
			"totalCost" => $basket['totalCost']
		);
		
		if($data['totalCost'] > $data['deliveryTypes'][0]['bound']) {
			$data['deliveryPrice'] = $data['deliveryTypes'][0]['low_cost'];
		} else {
			$data['deliveryPrice'] = $data['deliveryTypes'][0]['high_cost'];
		}
		
		$data['totalCostWithDelivery'] = $data['totalCost'] + intval($data['deliveryPrice']);
		
		/* рендерим саму форму заказа */
		$formTemplate = $this->getTemplate("order.form.html", true);
		$data['orderFormHTML'] = $this->getDi()->get("lex")->parse($formTemplate, $data);
		
		
		switch ($options['type']) {
			case 'individual':
				$content  =  $this->render("form_individual.php", $data);
				break;
			case 'juridical':
				$content  =  $this->render("form_juridical.php", $data);
				break;
			default:
				$content = "Укажите тип (type) формы";
		}
		
		return $content;
	}
	
	private function getOrder() {
		if(isset($_COOKIE[$this->getConfig("order_cookie_name")])) {
			$orderID = $_COOKIE[$this->getConfig("order_cookie_name")];
		} else {
			$order = $this->createOrder();
			setcookie($this->getConfig("order_cookie_name"), $order['id'], time() + $this->getConfig("cookie_lifetime"), '/');
			$orderID = $order['id'];
		}
		
		return $this->getDb()->fetchRow("SELECT * FROM ".$this->getBasketOrdersTableName()." WHERE id = ?", $orderID);
	}
	
	private function createOrder() {
		$ordersTablename = $this->getBasketOrdersTableName();
		//$this->getDb()->query("LOCK TABLES $ordersTablename WRITE");
		$nextID = 1 + (int) $this->getDb()->fetchOne("SELECT MAX(id) FROM $ordersTablename");
		$order = array(
			"name" => date('dm').'-'.$nextID, 
			"date" => date("Y-m-d H:i:s"), 
			"number" => 1 + (int) $this->getDb()->fetchOne("SELECT MAX(number) FROM $ordersTablename")
		);
		$this->getDb()->insert($ordersTablename, $order);
		$order['id'] = $this->getDb()->lastInsertId();
		//$this->getDb()->query("UNLOCK TABLES");
		return $order;
	}
	
	private function getDeliveryTypes() {
		$select = $this->getDb()->select()
			->from($this->getBasketDeliveryTypeTableName())
			->where("hidden = 0")
			->order("number");
		
		return $this->getDb()->fetchAll($select);
	}
	
	private function getPaymentTypes() {
		$select = $this->getDb()->select()
			->from($this->getBasketPaymentTypeTableName())
			->where("hidden = 0")
			->order("number");
	
		return $this->getDb()->fetchAll($select);
	}
	
	public function acceptOrder($options, $content = null) {
		$request = $this->getRequest();
		
		require_once 'front/modules/Form/FormData.php';
		
		if($request->isPost()) {
			
			$formTemplate = $this->getTemplate("order.form.html", true);
			$form = new \front\modules\Form\Forms\KreslaOrder($formTemplate);

			$dataObject = new \front\modules\Form\FormData($request->getPost());
			
			if($form->validate($dataObject)) {
				
				$order = $this->getOrder();
				
				/* А теперь сформируем и отправим e-mail */
				$basket = $this->getCookieBasket()->getItems();
				
				$dataObject->addSysData("basket", $basket);
				$dataObject->addSysData("orders_table_name", $this->getBasketOrdersTableName());
				$dataObject->addSysData("orders_goods_table_name", $this->getBasketGoodsTableName());
				$dataObject->addSysData("good_upholsteries_table_name", $this->getGoodUpholsteriesTableName());
				$dataObject->addSysData("order_id", $order['id']);
				$dataObject->addSysData("client_mail_template", $this->getTemplate("mail.client.html", true));
				$dataObject->addSysData("manager_mail_template", $this->getTemplate("mail.manager.html", true));
				$dataObject->addSysData("system_email", $this->getConfig("system_email"));
				$dataObject->addSysData("system_email2", $this->getConfig("system_email2"));
				
				$form->success($dataObject);
				
				
				$this->getCookieBasket()->clear();
				
				header("Location: /basket/success/?order_id=" . $order['id']);
				exit();
			} else {
				return $form->getErrorsAsHTML();
			}
		}
	}
	
	public function showOrderNumber() {
		$id = $this->getRequest()->getParam("order_id");
		
		if(is_numeric($id)) {
			return $this->getDb()->fetchOne("SELECT name FROM " . $this->getBasketOrdersTableName()." WHERE id = ?", $id);
		}

	}
	
	public function getGA(){
		$order_id = $this->getRequest()->getParam("order_id");
		
		$goods = $this->getDb()->fetchAll("SELECT * FROM " .$this->getBasketGoodsTableName()." WHERE order_id = ?", $order_id);
		
		$total_price=0;
		foreach ($goods as $good)
		{
			$total_price += $good['price'];
		}
		
		$yaParams =  ' var yaParams = {   order_id: "'.$order_id.'",
		order_price: '.$total_price.',
		currency: "RUR",
		exchange_rate: 1,
			goods:
			[      ';
		
		
		$out = '<script type="text/javascript">
  try{
  var pageTracker = _gat._getTracker("UA-35753499-1");
  pageTracker._trackPageview();
  pageTracker._addTrans(
      "'.$order_id.'",
      "Ot i Do",
      "'.$total_price.'",           // total - required
      "0",            // tax
      "0",           // shipping
      "Moscow",        // city
      "Moscow",      // state or province
      "Ru"              // country
    );';
		
		
		foreach ($goods as $key => $good)
		{
			$goods_detail = $this->getDb()->fetchRow("select * from ".$this->getGoodsTableName()." where id='{$good['id']}'");
			
		
			$yaParams .= ' {
		id: "'.$good['good_id'].'",
		name: "'.$goods_detail['title'].'",
		price: '.$good['price'].',
		quantity: '.$good['count'].'
	},';
		
		
			$out .= '
   pageTracker._addItem(
      "'.$order_id.'",           // order ID - necessary to associate item with transaction
      "'.$good['good_id'].'",           // SKU/code - required
      "'.$goods_detail['title'].'",        // product name
      "'.$goods_detail['pricecategory_id'].'",   // category or variation
      "'.$good['price'].'",          // unit price - required
      "'.$good['count'].'"               // quantity - required
   );
';
		}
		$yaParams = substr($yaParams, 0, -1);
		$yaParams .= '		]
	};';
		
		$out .= '
   pageTracker._trackTrans(); //submits transaction to the Analytics servers
} catch(err) {}
'.$yaParams.'
</script>
';
		
		return $out;
		
	}
	
}

?>