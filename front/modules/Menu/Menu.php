<?php
namespace front\modules;

class Menu extends Module {
	/* (non-PHPdoc)
	 * @see front\modules.Module::getTitle()
	 */
	public function getTitle() {
		return "Меню";		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::install()
	 */
	public function install() {
		
		return true;		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::remove()
	 */
	public function remove() {
		return true;		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getConfigFields()
	 */
	public function getConfigFields() {
		// TODO Auto-generated method stub
		
	}
	
	public function mainBlock($options, $content) {
		return $this->getDi()->get("templates")->find($options['name']);
	}
	public function sub($options, $content) {
		return $this->getDi()->get("templates")->find($options['name']);
	}
    //me
    public function menuList(){

        $sql="SELECT name, menu_name, parent_id FROM cms_pages WHERE in_menu=1 ";
        $pages = $this->getDb()->fetchAll($sql);


        return $pages;

    }

    public function getCurrent(){
        $page = $this->getDi()->get("page");
        $url = substr($page->getSectionUrl(),1);
        $url=explode('/',$url);
        return $url[0];

    }
    public function getSecondCurrent(){
        $page = $this->getDi()->get("page");
        $url = substr($page->getSectionUrl(),1);
        $url=explode('/',$url);
        if (isset($url[1])){
            return $url[1];
        }


    }

    public function menu(){
    	
        /* @var $lp \front\models\LightParser */
        $lp = $this->getDi()->get("lightParser");

        $templates = $lp->extractTemplates($this->getTemplate("menu"));

        $currentItem = $this->getCurrent();
        $length=strlen($currentItem);
        $items = $this->menuList();


        $html = '';
        if($items) {
            foreach($items as $item) {
                //print_r($item);


                $parent_url=$this->getDb()->fetchOne("SELECT name FROM cms_pages where id='".$item['parent_id']."'");
                if($currentItem == $item['name'] ||$currentItem==$parent_url){
                    $template = $templates['active'];
                } else {
                    $template = $templates['normal'];
                }
                //echo '<h1>'.$item['name'].'</h1><br><br>';
                if ($item['parent_id']!=1){
                    $item['name']=$parent_url.'/'.$item['name'];
                }
                //echo '<h1>'.$item['name'].'</h1><br><br>';
                /* @var $page Page */


                $html .= $lp->insertValues($template, array(
                    'title' => $item['menu_name'],
                    'url' => $item['name']
                ));
            }
        }


        return $html;



    }

    /**
     * Рендерим меню для обычных страниц
     * @return string
     */
    public function subMenu(){
    	
        /* @var $current \front\models\Page */
        $current = $this->getDi()->get("page");
        
        /* если $current =  /about/history/ , то подменю строим из дочерних страниц about, а не history */
        $h = $current->getHierarcy();
        $secondLevelPage = new \front\models\Page($h[1]);
        
        $children = $secondLevelPage->getChildPages();
       
      
        $menu = array();
        
        foreach ($children as $p) {
        	$childPage = new \front\models\Page($p);
        	if($childPage->isActive() && $childPage->getMenuName()) {
        		$menu[] = array(
        			"title" => $childPage->getMenuName(), 
        			"url" => $childPage->getFullUrl(), 
        			"active" => $childPage->getID() == $current->getID()
        		);
        	}
        }
       
        if($menu) {
        	$template = $this->getTemplate("submenu.html", true);
        	return $this->getDi()->get("lex")->parse($template, array("items" => $menu));
        }
    }
    
    
    public function catalogSubmenu($options, $content) {
    	/* @var $catalog \front\modules\CatalogKresla */
    	$catalog = $this->getDi()->getModule("catalogKresla");
    	$items = $catalog->getCategories();
    	
    	$template = '
    	<div class="partition">
    	<div class="title">{title}</div>
		<ul>
			{items}
		</ul>
		</div>
    	';
    	    	
    	$itemTemplate = '<li><a {class} href="{href}">{title}</a></li>';
    	$itemLinkTemplate = '<li><!--noindex--><a {class} rel="nofollow" href="{href}">{title}</a><!--/noindex--></li>';
    	
    	$lp = $this->getDi()->get("lightParser");
    	$html = "";
    	
    	foreach($items as $parent) {
    		$itemsHTML = '';
    		foreach ($parent['_children'] as $item) {
    			    			
    			if($item['ext_url']) {
    				$href = $item['ext_url'];
    				$class = 'outerLink';
    				$itemsHTML .= $lp->insertValues($itemLinkTemplate, array(
    						'href' => $href,
    						'class' => $class ? 'class="'.$class.'"' : '',
    						'title' => $item['title']
    				));
    			} else {
    				$href = '/catalog/' . $parent['url'].'/'.$item['url'].'/';
    				$class = null;
    				$itemsHTML .= $lp->insertValues($itemTemplate, array(
    						'href' => $href,
    						'class' => $class ? 'class="'.$class.'"' : '',
    						'title' => $item['title']
    				));
    			}
    			
    			
    		}
    		
    		$html .= $lp->insertValues($template, array(
    			'title' => $parent['title'], 
    			'items' => $itemsHTML
    		));
    	}
    	
    	return $html;
    }


}

?>