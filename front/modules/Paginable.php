<?php

namespace front\modules;

interface Paginable {
	public function getTotal();
	public function getOnPage();
}

?>