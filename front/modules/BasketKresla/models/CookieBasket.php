<?php
namespace front\modules\BasketKresla\models;
use front\models\Di;

// ini_set("display_errors", "on");
// error_reporting(E_ALL);

class CookieBasket {
	
	private $cookieName;
	private $cookieLifetime;
	private $orderCookieName;
	private $basketStructure = array("id", "upholstery_id", "arm_id", "count", "freedelivery");
	
	private $items;
	
	public function __construct($basketCookieName, $orderCookieName, $cookieLifetime) {
		$this->cookieName = $basketCookieName;
		$this->cookieLifetime = $cookieLifetime;
		$this->orderCookieName = $orderCookieName;
	}
	
	/**
	 * Получить список элементов корзины. 
	 * Элемент корзины - это связка Товар + Обивка + Количество + др.параметры
	 * @return array
	 */
	public function getItems() {
		if(!$this->items) {
			$items = array();
			if(isset($_COOKIE[$this->cookieName])) {
				$pairs = explode(",", $_COOKIE[$this->cookieName]);
				foreach($pairs as $pair) {
					$row = explode(":", $pair);
					$item = array();
					foreach ($row as $key => $value) {
						$item[$this->basketStructure[$key]] = $value;
					}
					$items[] = $item;
				}
			}
			
			
// 			if($_SERVER['REMOTE_ADDR'] == '80.90.120.182'){
				foreach($items as $ke => $i){
					$sql = "SELECT `id` FROM `kresla_goods` WHERE `id`= ". $i['id'];
					$t =  Di::getInstance()->get("db")->fetchAll($sql);
					if(!$t){
						unset($items[$ke]);
					}
				}
// 			}			

			
			$this->items = $items;
			
		}
		return $this->items;
	}
	
	/**
	 * Добавить элемент в корзину. 
	 * @param array $item
	 */
	public function addItem($item) {
		$items = $this->getItems();
		$found = false;
		foreach ($items as $key => $row) {
			$count = $row['count'];
			unset($row['count']);
			if($row == $item) {
				$items[$key]['count'] += $item['count'];
				$found = true;
				break;
			}
		}
		
		if(!$found) {
			$items[] = $item;
		}
		
		return $this->save($items);
	}
	
	public function setCounts($items) {
		$basketItems = $this->getItems();
		foreach ($items as $key => $count) {
			$basketItems[$key]['count'] = $count;
		}
		$this->save($basketItems);
	}
	
	public function save($items) {
		$pairs = array();
		foreach($items as $item) {
			
			$stringParts = array();
			foreach ($item as $key => $value) {
				$k = array_search($key, $this->basketStructure);
				if($k !== false) {
					$stringParts[$k] = $value;
				}
			}
			
			ksort($stringParts);
			$pairs[] = implode(":", $stringParts);
		}
		$this->items = $items;
		setcookie($this->cookieName, implode(",", $pairs), time() + $this->cookieLifetime, '/');
	}
	
	public function clear() {
		setcookie($this->cookieName, null, 0 , '/');
		setcookie($this->orderCookieName, null, 0, '/');
	}
}