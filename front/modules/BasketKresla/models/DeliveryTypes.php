<?php
namespace front\modules\BasketKresla\models;

require_once ('front/models/Orm.php');

use front\models\Orm;

class DeliveryTypes extends Orm {
	public function __construct() {
		parent::__construct("basket_delivery_types", null, __NAMESPACE__ ."\\DeliveryType");
	}
}