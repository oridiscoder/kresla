<?php
namespace front\modules;

class Breadcrumbs extends Module {
	/* (non-PHPdoc)
	 * @see front\modules.Module::getTitle()
	 */
	public function getTitle() {
		return "Хлебные крошки";		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::install()
	 */
	public function install() {
		return true;		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::remove()
	 */
	public function remove() {
		return true;		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getConfigFields()
	 */
	public function getConfigFields() {
		
	}
	
	public function render($options, $template) {
		/* @var $page \front\models\Page */
		$page = $this->getDi()->get("page");
		$template = $this->getTemplate("breadcrumbs.html", true);
		
		return $this->getDi()->get("lex")->parse($template, array("items" => $page->getBreadcrumbs()));
	}

	
}

?>