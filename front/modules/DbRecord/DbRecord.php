<?php
namespace front\modules;

class DbRecord extends \front\modules\Module {
	
	/**
	 * 
	 * @var \DbTableLight
	 */
	protected $dbTableLight;
	
	/* (non-PHPdoc)
	 * @see front\modules.Module::getTitle()
	 */
	public function getTitle() {
		return "Выборка записей из таблицы";
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
		return "Берёт данные из указанной таблицы, рендерит с ними заданный шаблон";		
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::install()
	 */
	public function install() {
		return true;
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::remove()
	 */
	public function remove() {
		return true;
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getConfigFields()
	 */
	public function getConfigFields() {
		
	}
	
	protected function prepareSelect($options) {
		
		if(isset($options['sql'])) {
			$select = $options['sql'];
		} elseif(isset($options['table'])) {			
			$tableName = $options['table'];
			$columns = isset($options['cols']) ? $options['cols'] : "*";
			$where = isset($options['where']) ? $options['where'] : null;
			$order = isset($options['order']) ? $options['order'] : null;
			$limit = isset($options['limit']) ? $options['limit'] : null;
				
			$select = $this->getDb()->select()
			->from($tableName, $columns);
			if($where) {
				$select->where($where);
			}
			if($order) {
				$select->order($order);
			}
			if($limit) {
				$select->limit($limit);
			}
		}
		return $select;
	}
	
	
	
	protected function parseTemplate($data, $template, $tableName = null) {
		
		if($tableName) {
			$dbTable = $this->getDbTable($tableName);
			
			if($dbTable) {
				
				$imageFields = $this->extractImageFields($dbTable->getFields());
				
				if($imageFields) {
					if(self::isRowSet($data)) {
						/* список строк */
						foreach ($data as $key => $row) {
							$data[$key] = $this->processImageFields($row, $imageFields);
						}
					} else {
						/* Одна строка */
						$data = $this->processImageFields($data, $imageFields);
					}
				}
			}
		}
		
		$content = $this->getDi()->get("lex")->parse($template, $data);
		
		return $content;
	}
	
	protected function extractImageFields($fields) {
		$imageFields = array();
		if($fields) {
			foreach($fields as $fieldName => $field) {
				if($field instanceof \ImageField) {
					$imageFields[$fieldName] = $field;
				}
			}
		}
		return $imageFields;
	}
	
	protected static function isRowSet($row) {
		$keys = array_keys($row);
		return is_numeric($keys[0]);
	}
	
	protected function processImageFields($row, $imageFields) {
		foreach($row as $fieldName => $fieldValue) {
			if(isset($imageFields[$fieldName])) {
				$row[$fieldName] = $imageFields[$fieldName]->getURL($row);
			}
		}
		return $row;
	}
	
	
	
	/**
	 * 
	 * @param string $tableName
	 * @return \DbTableLight
	 */
	protected function getDbTable($tableName) {
		require_once "models/Table/DbTableLight.php";
		try {
			$dbTable = new \DbTableLight($this->getDb(), $tableName);
			return $dbTable;
		} catch (\DbTable_TableNotFoundException $ex) {
			return null;
		}		
	}
	
	public function fetchRow($options, $template) {
		$content = "";
		$row = null;
		$select = $this->prepareSelect($options);
		
		if($select) {
			$row = $this->getDb()->fetchRow($select);
			if($row) {
				$tableName = isset($options['table']) ? $options['table'] : null;
				$content = $this->parseTemplate($row, $template, $tableName);
			}
		}		
		return $content;
	}
	
	public function fetchAll($options, $template) {
		$content = "";
		$row = null;
		$select = $this->prepareSelect($options);
		
		if($select) {
			$rows = $this->getDb()->fetchAll($select);
			if($rows) {
				$tableName = isset($options['table']) ? $options['table'] : null;
				$content = $this->parseTemplate($rows, $template, $tableName);
			}
		}		
		return $content;
	}
	


}

?>