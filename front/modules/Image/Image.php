<?php
namespace front\modules;

class Image extends Module{
	/*Имя папки в корневой директории, 
	 *в которой будут картинки 
	 */
	public $rootFolder = 'superImages';
	public $webroot = 'www';
	
	public function getTitle(){
		return "Каартинки";		
	}
	public function getDescription(){
		return "";
	}
	public function install() {
		mkdir($this->rootFolder);
		return true;
	}
	
	public function remove() {
		return true;
	}
	public function getConfigFields(){
		return array();
	}
	
	private function getParams($data)
	{
		if (isset($data['table'])) {
			return $this->getFullParams($data);
		}
		else {
			return $this->getShortParams($data);
		}
	}
	
	private function getFullParams($data){
		list($data['width'], $data['height']) = explode('x', $data['size']);
		return $data;
		
	}
	
	private function getShortParams($data){
		if(count(explode('.', $data['src']))==4){
			list($params['table'], $params['field'], $params['id'], $params['size']) = explode('.', $data['src']);
			list($params['width'], $params['height']) = explode('x', $params['size']);
		}elseif(count(explode('.', $data['src']))==3)
			list($params['table'], $params['field'], $params['id']) = explode('.', $data['src']);
		
		if(isset($data['file'])){
			$params['file'] = $data['file'];
		}
		else {
			$name = $this->getDb()->fetchAll("SELECT {$params['field']} FROM ".$this->getDbPrefix()."{$params['table']} WHERE id = {$params['id']}");
			$params['file'] = $name[0][$params['field']];
		}
		
		return $params;
	}
	
	
	public function getImage($options){
		$params = $this->getParams($options);
		
		if(!isset($params['size']))
			return $this->getOriginUrl($params);
		
		
		$url = "/$this->rootFolder/".$this->getDbPrefix()."{$params['table']}/{$params['field']}/{$params['id']}/{$params['width']}x{$params['height']}/{$params['file']}";
		
			
		if(!file_exists($_SERVER['DOCUMENT_ROOT'].$url))
			return $this->createImg($params);
		
		
		return $url;
	}
	public function createImg($params){
		require_once "Bof/ImageCropper.php";
		
		
		
		$path = "/$this->rootFolder/".$this->getDbPrefix()."{$params['table']}/{$params['field']}/{$params['id']}/{$params['width']}x{$params['height']}/";
		
		
		$url = $path.$params['file'];
		
		$this->make_path(__DIR__."/../../../".$this->webroot.$path);
		
		$destin = __DIR__."/../../../".$this->webroot.$url;
		
		$OriginUrl = $this->getOriginUrl($params);
		$OriginPath = __DIR__."/../../../".$this->webroot.$OriginUrl;
		if(!file_exists($OriginPath))
			return $OriginUrl;
		
		
		$resizer = new \ImageCropper($OriginPath);
		$resizer->saveAs($destin, $params['width'], $params['height']);
		
		return $url;
	}
	public function make_path($pathname, $is_filename=false){
		if($is_filename){
			$pathname = substr($pathname, 0, strrpos($pathname, '/'));
		}
		// Check if directory already exists
		if (is_dir($pathname) || empty($pathname)) {
			return true;
		}
		// Ensure a file does not already exist with the same name
		$pathname = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $pathname);
		if (is_file($pathname)) {
			trigger_error('mkdirr() File exists', E_USER_WARNING);
			return false;
		}
		// Crawl up the directory tree
		$next_pathname = substr($pathname, 0, strrpos($pathname, DIRECTORY_SEPARATOR));
		if ($this->make_path($next_pathname)) {
			if (!file_exists($pathname)) {
				return mkdir($pathname);
			}
		}
		return false;
	}
	
	public function getOriginUrl($params){
		return "/cms/cms-images/".$this->getDbPrefix()."{$params['table']}/{$params['id']}_{$params['field']}_{$params['file']}";
	}
	
}