<?php
namespace front\modules;

class PromoBlock extends Module {

	public function getTitle() {
		return "Промо блок";
	}


	public function getDescription() {
		"Блок, состоящий из картинки, текста и ссылки";
	}
	
	public function getTableName() {
		return $this->getDbPrefix()."promo_block";
	}

	public function install() {
		
		$tableName = $this->getTableName();
	
		$sql = "CREATE TABLE IF NOT EXISTS `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED DEFAULT 0,
			`hidden` TINYINT(1) DEFAULT 0,
			`title` VARCHAR(255),
			`text` TEXT, 
			`image` VARCHAR(255), 
			KEY(`number`)
		) ENGINE=MyISAM";
		$this->getDb()->query($sql);
	
		$factory = $this->getTableFactory();
		$factory->addTable($tableName, $this->getTitle())
				->addStringField('title', 'Заголовок')
				->addTextField('text', 'Текст')
				->addImageField('image', "Картинка", array(
					"hint" => 'Оптимальный размер 235x352'
				));
	
		$pageFactory = $this->getPageFactory();
		$pageFactory->addPage($this->getTitle(), \PageCreator::SECTION_SITE, $factory->getTableId());
	
		/* установим наши шаблоны */
		$this->saveTemplate("main", '
			<div class="new-project">
			<h2>Новая работа</h2>
			<!--noindex--><a class="show-description" href="#">
				<span>Показать описание</span>
				<span>Скрыть описание</span>
			</a><!--/noindex-->
			<span class="clip"></span>
			<div class="topic-block active">
				<a href="{{url}}"><img src="{{image}}" alt=""></a>
			</div><!-- .topic-block -->
			<div class="project-block projectBlockShadow">
				<h1><a href="#">{{title}}</a></h1>
				{{text}}
				<a class="detail" href="{{url}}"><span>Подробно</span></a>
			</div><!-- .project-block -->
		</div><!-- .new-project -->
		');
		
		$this->installImages();
		$this->installJS();
		$this->installCSS();
		
		return true;
	}
	
	public function getJS() {
		return 'promo_block.js';
	}
	public function getCSS() {
		return "promo_block.css";
	}

	public function remove() {
		
		$tableName = $this->getTableName();
		
		$sql = "DROP TABLE IF EXISTS `{$tableName}`";
		$this->getDb()->query($sql);
		
		$this->removeTable($tableName);
		
		//удалим наши шаблоны
		$this->deleteTemplate("main");
		
		$this->removeImages();
		$this->removeJS();
		$this->removeCSS();
		
		return true;
	}

	public function getConfigFields() {
		// TODO Auto-generated method stub
		
	}
	
	public function html() {
		$template = $this->getTemplate("main");
		
		$select = $this->getDb()->select()
			->from($this->getTableName())
			->order("number DESC")
			->limit(1);
		$last = $this->getDb()->fetchRow($select);
		if($last) {
			require_once "models/Table/DbTableLight.php";
			$table = new \DbTableLight($this->getDb(), $this->getTableName());
			/* @var $imageField \ImageField */
			$imageField = $table->getField("image");
			$last['image'] = $imageField->getURL($last);
			$template = $this->getDi()->get("lex")->parse($template, $last);
			return $template;
		}
	}


}