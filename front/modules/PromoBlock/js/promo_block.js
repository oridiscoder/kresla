$(function(){
	var newProjectBlock = $('.new-project'),
		topicBlock = newProjectBlock.find('.topic-block'),
		projectBlock = newProjectBlock.find('.project-block'),
		showDescription = newProjectBlock.find('.show-description');

		$(showDescription).click(function(e){
			e.preventDefault();
			$(this).toggleClass('active');
			topicBlock.toggleClass('active');
			projectBlock.toggleClass('active');
		});
		$(projectBlock).click(function(e){
			$(this).addClass('active');
			$(topicBlock).removeClass('active');
			showDescription.addClass('active');
		});
		$(topicBlock).click(function(e){
			if (!$(this).hasClass('active')) {
				e.preventDefault();
				$(this).addClass('active');
				$(projectBlock).removeClass('active');
				showDescription.removeClass('active');
			};
		});
});