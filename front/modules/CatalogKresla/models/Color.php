<?php
namespace front\modules\CatalogKresla\models;

require_once ('Model.php');

class Color extends Model {
	
	private static $colors;
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getColorsTableName();
	}
	
	public static function find($options) {
		$tableName = self::getTableNamesContainer()->getColorsTableName();
		
		if(is_numeric($options)) {
			return self::getColors($options);
		} else {
			return parent::find(self::getTableNamesContainer()->getColorsTableName(), $options);
		}		
	}
	
	public static function getColors($key = null) {
		$tableName = self::getTableNamesContainer()->getColorsTableName();
		if(!self::$colors) {
			self::$colors = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT * FROM $tableName WHERE hidden = 0 ORDER BY number");
		}
		if($key) {
			if(is_numeric($key)) {
				return isset(self::$colors[$key]) ? self::$colors[$key] : null;
			}
			if(is_array($key)) {
				$result = array();
				foreach ($key as $id) {
					if(isset(self::$colors[$id])) {
						$result[] = self::$colors[$id];
					}
				}
				return $result;
			}
		}
		return self::$colors;
	}
	public static function getNotEmptyColors($category_id, $material_id=null){
		$db = \front\models\Di::getInstance()->get("db");
		$mat='';
		if($material_id){
			$mat=" AND type_id= ".(int) $material_id;
		}
		
		$notEmptyColors = $db->fetchAll("SELECT DISTINCT color_id FROM `kresla_goods_count`
				WHERE category_id=".(int)$category_id.$mat);
		foreach($notEmptyColors as $col){
			$ids[]=$col['color_id'];
		}
		if(count($ids)==0)
			return 0;
		return self::getColors($ids);
	}
}