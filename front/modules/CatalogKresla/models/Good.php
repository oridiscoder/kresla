<?php
namespace front\modules\CatalogKresla\models;

require_once ('Model.php');

class Good extends Model {
	
	private $good;
	private $addCategoryNameInTitle;
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getGoodsTableName();
	}
	
	public function __construct($good=null) {
		parent::__construct();
		
		if(is_numeric($good) && $good!=null) {
			$tname = self::getTableNamesContainer()->getGoodsTableName();
			$good = $this->getDb()->fetchRow("SELECT * FROM `$tname` WHERE id = ?", $good);
			if($good == null) {
				throw new RecordNotFoundException("Товар не найден");
			}
		}		
		$this->good = $good;
	}
	
	public function loadByUrl($good){
		$tname = self::getTableNamesContainer()->getGoodsTableName();
		$good = $this->getDb()->fetchRow("SELECT * FROM `$tname` WHERE url = ?", $good);
		if($good == null) {
			throw new RecordNotFoundException("Товар не найден");
		}
		$this->good = $good;
	}
	
	public function getID() {
		return $this->good['id'];
	}
	public function getTitle() {
		return $this->good['title'];
	}
	public function getKeywords() {
		return $this->good['keywords'];
	}
	public function getDescription() {
		return $this->good['description'];
	}
	public function getText() {
		return $this->good['text'];
	}
	public function getPrice() {
		return $this->good['price'];
	}
	public function getPriceActive() {
		$up = $this->getActiveUpholstery();
		return $up ? $up['price'] : $this->getPrice();
	}
	public function getThumb() {
		return $this->good['thumb'];
	}
	public function getThumbLarge() {
		return $this->good['thumb_large'];
	}
	public function getLabelID() {
		return $this->good['label_id'];
	}
	public function isOnOrder() {
		return $this->good['to_order'];
	}
	public function isInYaMarket() {
		return $this->good['in_ya_market'];
	}
	public function getVendorCode() {
		return $this->good['vendor_code'];
	}
	public function getVendorID() {
		return $this->good['vendor_id'];
	}
	public function getVendorName() {
		if(!isset($this->good['_vendor_name'])) {
			$vendor = Vendor::find($this->getVendorID());
			$this->good['_vendor_name'] = $vendor ? $vendor[0]['name'] : null;
		}
		return $this->good['_vendor_name'];
	}
	public function getSalesNotes() {
		return $this->good['sales_notes'];
	}
	public function hasWarranty() {
		return $this->good['manufacturer_warranty'];
	}
	public function getCountryOfOrigin() {
		return $this->good['country_of_origin'];
	}
	public function getHeight() {
		return $this->good['height'];
	}
	public function getWidth() {
		return $this->good['width'];
	}
	public function getDeep() {
		return $this->good['deep'];
	}
	public function getWeightLimit() {
		return $this->good['weight_limit'];
	}
	public function hasBackAdjust() {
		return $this->good['back_adjust'];
	}
	public function getGoogleExperimentCode() {
		return $this->good['ga_experiment_code'];
	}
	public function asArray() {
		return $this->good;
	}
	public function getURL() {
		return $this->good['url'];
	}
	
	
	public function getCategoryID($ex = false) {
		if(!isset($this->good['_category_id'])) {
			$goodCatsName = self::getTableNamesContainer()->getGoodCategoriesTableName();
			$this->good['_category_id'] = $this->getDb()->fetchOne("SELECT category_id FROM `$goodCatsName` WHERE good_id = ? AND hidden = 0 ORDER BY `primary` DESC LIMIT 1", $this->getID());
		}
		if($ex){
			$goodCatsName = self::getTableNamesContainer()->getGoodCategoriesTableName();
			$this->good['_category_id'] = $this->getDb()->fetchAll("SELECT category_id FROM `$goodCatsName` WHERE good_id = ? AND hidden = 0", $this->getID());
		
		}
		return $this->good['_category_id'];
	}
	
	/**
	 * Получить ярлык текущего товара
	 * @return array
	 */
	public function getLabel() {
		$labelTableName = self::getTableNamesContainer()->getLabelsTableName();
		if($this->getLabelID()) {
			return Label::find($this->getLabelID());
		}
	}
	
	public function getPhotos() {
		
		if(!isset($this->good['_photos'])) {
			$tableName = self::getTableNamesContainer()->getGoodImagesTableName();		
			$select = $this->getDb()->select()
				->from($tableName)
				->where("good_id = ?", $this->getID())
				->where("hidden = 0")
				->order("number");
			$photos =  $this->getDb()->fetchAll($select);
			
			if($photos) {
				//require_once "models/Table/DbTableLight.php";
				//$table = new \DbTableLight($this->getDb(), $tableName);
				foreach ($photos as $key => $photo) {
					$photos[$key]['thumb'] = \front\models\Di::getInstance()->getModule('image')->getImage(array('src'=>"good_images.image.{$photo['id']}.60x90", 'name'=>$photo['image']));
					$photos[$key]['image'] = \front\models\Di::getInstance()->getModule('image')->getImage(array('src'=>"good_images.image.{$photo['id']}.380x400", 'name'=>$photo['image']));
					$photos[$key]['image_orig'] = \front\models\Di::getInstance()->getModule('image')->getImage(array('src'=>"good_images.image.{$photo['id']}", 'name'=>$photo['image']));
				}
			}
			
			$this->good['_photos'] = $photos;
		}
		
		return $this->good['_photos'];
	}
	
	
	
	public function getFirstPhoto() {
		$photos = $this->getPhotos();
		return $photos ? $photos[0]['image'] : null;
	}
	
	/**
	 * Получить список дополнительных услуг
	 */
	public function getServices() {
		return $this->getLinkedItems(
				self::getTableNamesContainer()->getServicesTableName(),
				self::getTableNamesContainer()->getGoodServicesTableName() ,
				"service_id"
		);
	}
	
	/**
	 * Получить список подлокотников
	 */
	public function getArms() {
		$this->good['_arms'] = $this->getLinkedItems(
				self::getTableNamesContainer()->getArmsTableName(),
				self::getTableNamesContainer()->getGoodArmsTableName(),
				"arms_id"
		);
		return $this->good['_arms'];
	}
	
	/**
	 * Получить список крестовин
	 */
	public function getCrosses() {
		return $this->getLinkedItems(
				self::getTableNamesContainer()->getCrossTableName(),
				self::getTableNamesContainer()->getGoodCrossTableName(),
				"cross_id"
		);
	}
	
	/**
	 * Получить список категорий, в которые входит товар
	 */
	public function getCategories() {
		return $this->getLinkedItems(
				self::getTableNamesContainer()->getCategoriesTableName(),
				self::getTableNamesContainer()->getGoodCategoriesTableName(),
				"category_id"
		);
	}
	
	/**
	 * Получить список обивок для данного товара
	 */
	public function getUpholsteries() {
		if(!isset($this->good['_upholsteries'])) {
			$this->good['_upholsteries'] = $this->getLinkedItems(
					self::getTableNamesContainer()->getUpholsteryTableName(),
					self::getTableNamesContainer()->getGoodUpholsteryTableName(),
					"upholstery_id", 
					array('price', 'price_old', 'on_order')
			);
		}
		
		return $this->good['_upholsteries'];
	}
	
	/**
	 * Получить текущую (выбранную) обивку
	 * @return array
	 */
	public function getActiveUpholstery() {
		if(isset($this->good['_upholsteries'])) {
			foreach($this->good['_upholsteries'] as $up) {
				if(isset($up['_active']) && $up['_active']) {
					return $up;
				}
			}
		}
	}
	
	/**
	 * Установить текущую (выбранную пользователем) обивку
	 * @param integer $upholsteryID
	 */
	public function setActiveUpholstery($upholsteryID) {
		$upholsteries = $this->getUpholsteries();
		if($upholsteries) {
			foreach ($upholsteries as $key => $upholstery) {
				if($upholstery['id'] == $upholsteryID) {
					$this->good['_upholsteries'][$key]['_active'] = true;
				} else {
					$this->good['_upholsteries'][$key]['_active'] = false;
				}
			}
		}
	}
	
	public function setActiveArm($ArmID) {
		$arms = $this->getArms();
		
		if($arms) {
			foreach ($arms as $key => $arm) {
				if($arm['id'] == $ArmID) {
					$this->good['_arms'][$key]['_active'] = true;
				} else {
					$this->good['_arms'][$key]['_active'] = false;
				}
			}
		}
	}
	
	public function getActiveArm(){
		if(isset($this->good['_arms'])) {
			foreach($this->good['_arms'] as $up) {
				if(isset($up['_active']) && $up['_active']) {
					return $up;
				}
			}
		}
		
	}
	
	
	
	
	
	public function getUpholsteryTypes() {
		$rows = $this->getUpholsteries();
		$parents = array();
		$types = array();
		if($rows) {
			foreach($rows as $row) {
				$parents[] = $row['parent_id'] ? $row['parent_id'] : $row['id'];	
			}
			
			/* теперь одним запросом подгружаем все родительские обивки */
			$select = $this->getDb()->select()
				->from(array('T'  => self::getTableNamesContainer()->getUpholsteryTypesTableName()))
				->join(array('U'  => self::getTableNamesContainer()->getUpholsteryTableName()), 'U.type_id = T.id', array())
				->where("U.id IN (?)", $parents)
				->where("U.hidden = 0")
				->order("U.number");
			$types = $this->getDb()->fetchAssoc($select);
		}
		 
		 return $types;
	}
	
	public function getColors() {
		if(!isset($this->good['_colors'])) {
			$upholsteries = $this->getUpholsteries();
			$ids = array();
			if($upholsteries) foreach($upholsteries as $up) {
				if($up['color_id']) {
					if(!in_array($up['color_id'], $ids)) {
						$ids[] = $up['color_id'];
					}
				}
			}
			
			$colors = array();
			
			if($ids) {
				$colors = Color::getColors($ids);
			}
			
			$this->good['_colors'] = $colors;
		}
		
		return $this->good['_colors'];
	}
	
	public function getCertificates() {
		return $this->getLinkedItems(
				self::getTableNamesContainer()->getCertificatesTableName(),
				self::getTableNamesContainer()->getGoodCertificatesTableName(),
				"certificate_id"
		);
	}
	
	/**
	 * Получить список элементов из другой таблицы, связанной с таблицей товаров посредоством связывающей таблицы
	 * @TODO Этот полезный метод хорошо бы вынести или на уровень выше или совсем в другой класс. 
	 *
	 * @param string $mainTableName - название табилицы, из которой мы хотим получить записи
	 * @param string $linkTableName - название таблицы, связывающей $mainTableName и таблицу товаров
	 * @param string $linkFieldName - название столбца в $linkTableName, указывающего на запись из $mainTableName
	 * @param array $linkedTableFieldsToSelect - поля, которые мы хотим взять из связывающей таблицы. По умолчанию - ничего не брать.
	 * @return array - список записей из таблицы $mainTableName
	 */
	private function getLinkedItems($mainTableName, $linkTableName, $linkFieldName, $linkedTableFieldsToSelect = array()) {
		
		$select = $this->getDb()->select()
			->from(array("LT" => $linkTableName), $linkedTableFieldsToSelect)
			->join(array("MT" => $mainTableName), "MT.id = LT.".$linkFieldName)
			->where("LT.good_id = ?", $this->getID())
			->where("LT.hidden = 0")
			->where("MT.hidden = 0")
			->order("LT.number DESC"); //было MT.number исправил для упорядочивания обивок у каждого кресла по отдельности 
		$rows = $this->getDb()->fetchAll($select);
		
		if($rows) {
			$rows = self::expandImagesUrls($rows, $mainTableName);
		}
		
		return $rows;
	}
	
	/**
	 * Получить похожие товары
	 */
	public function getSimilarGoods($limit = 4){
		$db = \front\models\Di::getInstance()->get("db");
		return $db->fetchAll("SELECT * FROM kresla_good_similar as s JOIN kresla_goods as g ON s.similar_id = g.id WHERE s.good_id=".$this->getID()." 
				UNION         SELECT * FROM kresla_good_similar as s JOIN kresla_goods as g ON s.good_id = g.id WHERE s.similar_id=".$this->getID()." LIMIT ".$limit)
		;
	}
	
	
	/* старый похожие
	public function getSameGoods($limit = 4) {
		return self::find(array(
			"category_id" => $this->getCategoryID(), 
			"where" => array(
				"price <= '" . ($this->getPrice() + 0.2 * $this->getPrice())."'", 
				"price >= '" . ($this->getPrice() - 0.2 * $this->getPrice())."'"
			), 
			"limit" => $limit
		));
	}*/
	
	public function getDefaultCategory() {
		if(!$this->good['_category']) {
			$this->good['_category'] = Category::find($this->getCategoryID());
		}
		return $this->good['_category'];
	}
	
	public function getFullURL() {
		$category = Category::find($this->getCategoryID());
		$url = $category->getFullUrl() . '/' . $this->getURL().'.html';
		return $url;
	}
	
	/**
	 * Получить массив, содержащий всю информацюо о товаре, в том числе из связанных таблиц
	 * @return array
	 */
	public function asFullArray() {
		$good = $this->asArray();
		$good['_photos'] = $this->getPhotos();
		$good['_first_photo'] = $good['_photos'] ? $good['_photos'][0] : null;
		$good['_services'] = $this->getServices();
		$good['_arms'] = $this->getArms();
		$good['_arm'] = $good['_arms'] ? $good['_arms'][0] : null;
		$good['_crosses'] = $this->getCrosses();
		$good['_cross'] = $good['_crosses'] ? $good['_crosses'][0] : null;
		$good['_certificates'] = $this->getCertificates();
		$good['category'] = Category::find($this->getCategoryID());
		
		/* Подгружаем обивки кресел и групируем их по parent_id (получается группировка по типу) */
		$upholsteries = $this->getUpholsteries();
		$upholsteriesParentIds = array();
		$upholsteriesParents = array();
		$upholsteriesByParent = array();
		
		/*echo "<pre>";
		print_r($upholsteries);*/
		if($upholsteries) {
			/* сначала соберём все parent_id */
			foreach($upholsteries as $row) {
				$parentId = $row['parent_id'];
				if(!isset($upholsteriesParents[$parentId])) {
					$upholsteriesParentIds[] = $parentId;
				}
				$upholsteriesByParent[$parentId][] = $row;
			}
	
			/* теперь одним запросом подгружаем все родительские обивки */
			$select = $this->getDb()->select()
				->from(self::getTableNamesContainer()->getUpholsteryTableName())
				->where("id IN (?)", $upholsteriesParentIds)
				->where("hidden = 0")
				->order("number");
			$upholsteriesParents = $this->getDb()->fetchAssoc($select);
			
			/* добавим каждому родителю своих детей */
			foreach ($upholsteriesParents as $key => $upholsteryParent) {
				foreach($upholsteriesByParent[$upholsteryParent['id']] as $upholstery) {
					$price = $upholstery['price'];
					$upholsteriesParents[$key]['_children'][$price]['_price']  = $price;
					$upholsteriesParents[$key]['_children'][$price]['_price_old']  = $upholstery['price_old'];
					$upholsteriesParents[$key]['_children'][$price]['_price_diff']  = $upholstery['price_old'] - $price;
					$upholsteriesParents[$key]['_children'][$price]['_title']  = $upholsteryParent['title'];
					$upholsteriesParents[$key]['_children'][$price]['_rows'][] = $upholstery;
					$upholsteriesParents[$key]['_children'][$price]['_rows_count'] = isset($upholsteriesParents[$key]['_children'][$price]['_rows_count']) ? $upholsteriesParents[$key]['_children'][$price]['_rows_count'] + 1 : 1;
					
					/* установим начальную цену товара и отметим обивку как "выделенную" */
					if(!isset($good['_default_price'])) {
						$good['_default_price'] = $price;
						$upholsteriesParents[$key]['_children'][$price]['_rows'][0]['_selected'] = 1;
					}
				}
				$upholsteriesParents[$key]['_children_count'] = count($upholsteriesByParent[$upholsteryParent['id']]);
			}
		}
		
		$good['_default_price'] = isset($good['_default_price']) ? $good['_default_price'] : $good['price'];
		
		$good['_upholsteries'] = $upholsteriesParents;
		
		return $good;
	}
	
	public static function find($options) {
		$select = self::prepareSelect($options);
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = \front\models\Di::getInstance()->get("db");
		
		if(isset($options['onpage']) && $options['onpage']) {
			$onpage = $options['onpage'];
			$pagenumber = isset($options['pagenumber']) ? $options['pagenumber'] : array();
			$select->limit($onpage, ($pagenumber - 1) * $onpage );
		}
		
		if(isset($options['limit'])) {
			$select->limit($options['limit']);
		}
				
		//echo $select.'<br />';	
		
		return $db->fetchAll($select);
	}
	
	public static function count($options) {
		$select = self::prepareSelect($options);
		$select->reset(\Zend_Db_Select::COLUMNS);
		$select->columns(array("count" => "COUNT(*)"));
		
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = \front\models\Di::getInstance()->get("db");
		
		return $db->fetchOne($select);
	}
	
	public static function prepareSelect($options) {
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = \front\models\Di::getInstance()->get("db");
		
		$goodsName = self::getTableNamesContainer()->getGoodsTableName();
		$catsName = self::getTableNamesContainer()->getCategoriesTableName();
		
		$select = $db->select()
			->from($goodsName)
			->where("hidden = 0");
		
		$whereIDS = null;	//если не задано, то и проверять на них не будем
		
		if(isset($options['id'])) {
			$whereIDS[] = $options['id'];
		}
		if(isset($options['ids'])) {
			$whereIDS = $options['ids'];
		}
		
		$links = array(
					
		);
		
		if(isset($options['category_id'])) {
			$goodCatsName = self::getTableNamesContainer()->getGoodCategoriesTableName();
			if(!is_array($options['category_id'])) {
				$options['category_id'] = array($options['category_id']);
			}
			$ids = $db->fetchCol("SELECT good_id FROM `$goodCatsName` WHERE category_id IN (?)", $options['category_id']);
			$ids = $ids ? $ids : array();
			$whereIDS = $whereIDS ? $whereIDS : array();
				
			foreach($ids as $id) {
				if(!in_array($id, $whereIDS)) {
					$whereIDS[] = $id;
				}
			}
		}
		
		if(isset($options['label_id'])) {
			$select->where("label_id = ?", $options['label_id']);
		}


		if(isset($options['in_ya_market'])){
			$select->where("in_ya_market = 1");		
		}
		
		if(isset($options['where'])) {
			if(is_array($options['where'])) {
				foreach ($options['where'] as $where) {
					if(is_string($where)) {
						$select->where($where);
					}
					if(is_array($where)) {
						foreach ($where as $cond => $value) {
							$select->where($cond, $value);
						}
					}
				}
			}
			if(is_string($options['where'])) {
				$select->where($options['where']);
			}
		}
		
		if(isset($options['order'])) {
			if(is_string($options['order'])) {
				$select->order($options['order']);
			}
			if(is_array($options['order'])) {
				$colname = $options['order']['name'];
				$direction = in_array($options['order']['dir'], array("ASC", "DESC")) ? $options['order']['dir'] : "ASC";
				$select->order($colname.' '.$direction);
			}
		}
		
		if(is_array($whereIDS)) {
			if($whereIDS) {
				$select->where("id IN (?)", $whereIDS);
			} else {
				$select->where("1 = 0");
			}
		}

		
		
		return $select;
	}
	
	public static function findPopular($options = array()) {
		$popularTableName = self::getTableNamesContainer()->getPopularGoodsTableName();
		$goodsTableName = self::getTableNamesContainer()->getGoodsTableName();
		
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = \front\models\Di::getInstance()->get("db");
		
		$ids = $db->fetchCol("SELECT good_id FROM $popularTableName WHERE hidden = 0");
		
		$select  = $db->select()
			->from(array("P" => $popularTableName), array())
			->join(array("G" => $goodsTableName), "G.id = P.good_id")
			->where("G.hidden = 0")
			->where("P.hidden = 0")
			->order("P.number");
		
		if(isset($options['limit'])) {
			$pagenumber = isset($options['pagenumber']) ? $options['pagenumber'] : 1;
			$select->limit($options['limit'], ($pagenumber - 1) * $options['limit']);
		}
		
		return $db->fetchAll($select);
		
	}
	
	public static function findNewGoods($options = array()) {
		$popularTableName = self::getTableNamesContainer()->getNewGoodsTableName();
		$goodsTableName = self::getTableNamesContainer()->getGoodsTableName();
		
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = \front\models\Di::getInstance()->get("db");
		
		$ids = $db->fetchCol("SELECT good_id FROM $popularTableName WHERE hidden = 0");
		
		$select  = $db->select()
			->from(array("P" => $popularTableName), array())
			->join(array("G" => $goodsTableName), "G.id = P.good_id")
			->where("G.hidden = 0")
			->where("P.hidden = 0")
			->order("P.number");
		
		if(isset($options['limit'])) {
			$pagenumber = isset($options['pagenumber']) ? $options['pagenumber'] : 1;
			$select->limit($options['limit'], ($pagenumber - 1) * $options['limit']);
		}
		
		return $db->fetchAll($select);
	}
	
	/**
	 * Найти все товары заданной категории
	 * @param integer $id - id категории
	 * @param integer $onpage - кол-во на странице. По умолчанию = 0 (без ограничения)
	 * @param integer $pageNumber - начиная с единицы!
	 */
	public static function findByCategory($id, $order = null, $onpage = 0, $pageNumber = 1) {
		return self::find(array(
			'category_id' => $id, 
			'order' => $order, 
			'onpage' => $onpage, 
			'pagenumber' => $pageNumber
		));
	}
	
	public static function countByCategory($id, $order = null, $onpage = 0, $pageNumber = 1) {
		$select = self::prepareSelect(array(
				'category_id' => $id,
				'order' => $order,
				'onpage' => $onpage,
				'pagenumber' => $pageNumber
		));
		$select->reset(\Zend_Db_Select::COLUMNS);
		$select->columns(array("count" => "COUNT(*)"));
		
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = \front\models\Di::getInstance()->get("db");
		
		return $db->fetchOne($select);
	}
	
	public static function getMinPrice() {
		return self::getMin("price");
	}
	
	public static function getMaxPrice() {
		return self::getMax("price");
	}
	
	public static function getMinHeight() {
		return self::getMin("height_min");
	}
	
	public static function getMaxHeight() {
		return self::getMax("height_max");
	}
	public static function getMaxWidth() {
		return self::getMax("width");
	}
	public static function getMinWidth() {
		return self::getMin("width");
	}
	public static function getMaxDeep() {
		return self::getMax("deep");
	}
	public static function getMinDeep() {
		return self::getMin("deep");
	}
	
	private static function getMin($colname) {
		
		$tableName = self::getTableNamesContainer()->getGoodsTableName();
		
		$callback = function () use ($tableName, $colname) {
			/* @var $db \Zend_Db_Adapter_Abstract */
			$db = \front\models\Di::getInstance()->get("db");
			//return $db->fetchOne("SELECT MIN(`$colname`) FROM $tableName WHERE hidden = 0 AND `$colname` > 0");
			//вытягивать минимальное значение только для кресел и стульев 
			return $db->fetchOne("SELECT ".$colname." FROM kresla_goods as g JOIN kresla_good_categories as g2c ON g.id = g2c.good_id JOIN kresla_categories as c ON g2c.category_id = c.id WHERE g.hidden = 0 AND g.".$colname." > 0 AND (c.title LIKE '%ресл%' OR c.title LIKE '%тул%') ORDER BY g.".$colname." LIMIT 1");
			//die("e");
		};
		
		return self::getCached($colname."_min", $callback);
	}
	
	private static function getMax($colname) {
		
		$tableName = self::getTableNamesContainer()->getGoodsTableName();
		
		$callback = function () use ($tableName, $colname) {
			/* @var $db \Zend_Db_Adapter_Abstract */
			$db = \front\models\Di::getInstance()->get("db");
			//return $db->fetchOne("SELECT MAX(`$colname`) FROM $tableName WHERE hidden = 0 AND `$colname` > 0");
			return $db->fetchOne("SELECT ".$colname." FROM kresla_goods as g JOIN kresla_good_categories as g2c ON g.id = g2c.good_id JOIN kresla_categories as c ON g2c.category_id = c.id WHERE g.hidden = 0 AND g.".$colname." > 0 AND (c.title LIKE '%ресл%' OR c.title LIKE '%тул%') ORDER BY g.".$colname." DESC LIMIT 1");
		};
		
		return self::getCached($colname."_max", $callback);
	}
	
	/**
	 * Прицепить данные из других таблиц к товарам
	 * @param array $goods
	 * @param array $dataNames
	 */
	public static function hitchData($goods, $dataNames) {
		
		/* Сначала соберём айдишники, чтоб по ним можно было делать 
		 * общие запросы к БД
		 */

		//self::addCategoryNameInTitle = false;
		// echo $this->good;
		$addCategoryNameInTitle = false;

		if(in_array("upholsteries", $dataNames)) {
			
			$linkTableName = self::getTableNamesContainer()->getGoodUpholsteryTableName();
			$destTableName = self::getTableNamesContainer()->getUpholsteryTableName();
			$linkFieldName = "upholstery_id";
			
			$goods = self::hitchItems($goods, "_upholsteries", $linkTableName, $destTableName, $linkFieldName, array("price"));
			$addCategoryNameInTitle = true;
		}
		
		if(in_array("categories", $dataNames)) {
				
			$linkTableName = self::getTableNamesContainer()->getGoodCategoriesTableName();
			$destTableName = self::getTableNamesContainer()->getCategoriesTableName();
			$linkFieldName = "category_id";
				
			$goods = self::hitchItems($goods, "_categories", $linkTableName, $destTableName, $linkFieldName);
			$addCategoryNameInTitle = true;
		}


		//костыль для добавления названия категории к title проекта
		$kostyl = array(
					'10'=>'Компьютерный стол',
					'12'=>'Стол-книжка',
					'14'=>'Журнальный столик',
 					//'17'=>'Прихожая',
					'18'=>'Вешалка'
		);
		
		foreach($goods as $key => $good) {
			$good = new self($good);

			if(in_array("photos", $dataNames)) {
				$goods[$key]['_photos'] = $good->getPhotos();
			}
			if(in_array("services", $dataNames)) {
				$goods[$key]['_services'] = $good->getServices();
			}
			if(in_array("arms", $dataNames)) {
				$goods[$key]['_arms'] = $good->getArms();
			}
			if(in_array("crosses", $dataNames)) {
				$goods[$key]['_crosses'] = $good->getCrosses();
			}
			if(in_array("certificates", $dataNames)) {
				$goods[$key]['_certificates'] = $good->getCertificates();
			}
			if(in_array("upholstery_types", $dataNames)) {
				$goods[$key]['_upholstery_types'] = $good->getUpholsteryTypes();
			}
			if(in_array("colors", $dataNames)) {
				$goods[$key]['_colors'] = $good->getColors();
			}
			if(in_array("label", $dataNames)) {
				$goods[$key]['_label'] = $good->getLabel();
			}
			if(in_array("url", $dataNames)) {
				$goods[$key]['_url'] = $good->getFullURL();
			}
			if(in_array("category_url", $dataNames)) {
				$goods[$key]['_category_url'] = Category::find($good->getCategoryID())->getFullUrl();
			}
			if(in_array("category_name", $dataNames)) {
				$goods[$key]['_category_name'] = Category::find($good->getCategoryID())->getTitle();
			}

			//Добавляем название категории к title проекта
			foreach($good->getCategoryID(true) as $catit){
				$cid = $catit['category_id'];
				if(isset($kostyl[$cid]) && $goods[$key]['id'] != 389 && $goods[$key]['id'] != 390 && $goods[$key]['hidecat'] != 1){
					$goods[$key]['title'] = $kostyl[$cid] . ' ' . $goods[$key]['title'];
				}
			}
		}
		
		return $goods;
	}	
	//#101996 04.02.2016 00:00 by Dmitrievskiy Sergey
	//перенес костыль для добавления названия категории к title проекта
	//из метода hitchItems в hitchData, чтобы название категории добавилось только один раз

	/**
	 * Прикрепить к товарам элементы из другой таблицы, связанной с товарами через линковочную таблицу
	 * @param array $goods - список товаров
	 * @param string $columnName - название ключа в массиве $goods, по которому будут прикреплены элементы
	 * @param string $linkTableName - название линковочной таблицы
	 * @param string $destTableName - название таблицы с линкуемыми данными
	 * @param string $linkFieldName - $destTableName.id =  $linkTableName.$linkFieldName
	 * @param array $fieldsFromLinkedTable - список полей, которые надо подцепить из линковочной таблицы
	 * @return array
	 */
	public static function hitchItems($goods, $columnName, $linkTableName, $destTableName, $linkFieldName, $fieldsFromLinkedTable = array()) {

		if($goods) {
			
			/* @var $db Zend_Db_Adapter_Abstract */
			$db = \front\models\Di::getInstance()->get("db");
			
			$goodsIds = self::extractColumnFromArray($goods, "id");
			
			/* записи из связывающей таблицы */
			$links = $db->fetchAll("SELECT * FROM $linkTableName WHERE good_id IN (".implode(",", $goodsIds).")");
			$destIds = array();
			$goodLinkedItems = array();
				
			foreach($links as $link) {
				$destIds[] = $link[$linkFieldName];
				$goodLinkedItems[$link['good_id']][] = $link;
			}
				
			/* подгружаем только нужные нам обивки */
			if(count($destIds)){
				$destItems = $db->fetchAssoc("SELECT * FROM $destTableName WHERE id IN (".implode(",", $destIds).")");
			}
			foreach($goods as $key => $good) {
				if(!empty($goodLinkedItems[$good['id']])){
				foreach ($goodLinkedItems[$good['id']] as $row) {
					$destID = $row[$linkFieldName];
			
					if(isset($destItems[$destID])) {
						$destItem = $destItems[$destID];
						
						/* прицепим поля из линковочной таблицы */
						foreach($fieldsFromLinkedTable as $fieldName) {
							$destItem[$fieldName] = $row[$fieldName];
						}
						
						$goods[$key][$columnName][] = $destItem;
					} else {
						$goods[$key][$columnName] = array();
					}
				}
				}
			}
		}

		return $goods;
	}
	
	private static function extractColumnFromArray($rows, $columnName) {
		$res = array();
		foreach($rows as $good) {
			$res[] = $good[$columnName];
		}
		return $res;
	}
}