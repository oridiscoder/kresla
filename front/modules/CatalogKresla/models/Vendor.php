<?php
namespace front\modules\CatalogKresla\models;

require_once ('front/models/Model.php');


class Vendor extends Model {
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getVendorsTableName();
	}
	
	public static function find($options) {
		return parent::find(self::getTableNamesContainer()->getVendorsTableName(), $options);
	}
}