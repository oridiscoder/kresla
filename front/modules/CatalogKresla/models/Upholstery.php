<?php
namespace front\modules\CatalogKresla\models;

require_once ('Model.php');

class Upholstery extends Model {
	
	private $row;
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getUpholsteryTableName();
	}
	
	public function __construct($row) {
		$this->row = $row;
	}
	public function getID() {
		return $this->row['id'];
	}
	public function getTitle() {
		return $this->row['title'];
	}
	public function getImage() {
		return $this->row['image'];
	}
	public function getParentID() {
		return $this->row['parent_id'];
	}
	public function getParent() {
		return self::find($this->getParentID());
	}
	public function getTypeID() {
		return $this->row['type_id'];
	}
	public function getType() {
		return self::findType($this->getTypeID());
	}
	public function getArtikul() {
		return $this->row['artikul'];
	}
	
	public static function findType($id) {
		require_once "UpholsteryType.php";
		return UpholsteryType::find($id);
	}
	
	/**
	 * @TODO реализовать метод
	 * @param unknown_type $options
	 */
	public static function find($options) {
		return parent::find(self::getTableNamesContainer()->getUpholsteryTableName(), $options);
	}
	
}