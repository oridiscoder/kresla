<?php
namespace front\modules\CatalogKresla\models;

require_once ('Model.php');

class Category extends Model {
	
	private $category;
	
	private static $categories;
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getCategoriesTableName();
	}
	
	public function __construct($id) {
		parent::__construct();
		$tname = self::getTableNamesContainer()->getCategoriesTableName();
		$category = $this->getDb()->fetchRow("SELECT * FROM `$tname` WHERE id = ?", $id);
		if($category == null) {
// 			throw new RecordNotFoundException("Не найдена категория с id = $id");
		}
		
		$this->category = $category;
	}
	
	public function findByUrl($url){
		parent::__construct();
		$tname = self::getTableNamesContainer()->getCategoriesTableName();
		$category = $this->getDb()->fetchRow("SELECT * FROM `$tname` WHERE url = ?", $url);
		if($category == null) {
			return false;
			//throw new RecordNotFoundException("Не найдена категория с url = $url");
		}
		
		$this->category = $category;
	}
	
	public function getId() {
		return $this->category['id'];
	}
	public function getTitle() {
		return $this->category['title'];
	}
	public function getTitle1() {
		return $this->category['title1'];
	}
	public function getPageTitle() {
		return $this->category['page_title'];
	}
	public function getDescription() {
		return $this->category['description'];
	}
	public function getKeywords() {
		return $this->category['keywords'];
	}
	public function getUrl() {
		return $this->category['url'];
	}
	public function getExternalUrl() {
		return $this->category['ext_url'];
	}
	public function getParentId() {
		return $this->category['parent_id'];
	}
	public function getParent() {
		$parentID = $this->getParentId();
		if($parentID) {
			return new Category($parentID);
		} else {
			return null;
		}
	}
	public function getImage() {
		return $this->category['image'];
	}
	
	/**
	 * Получить количество товаров в текущей категории
	 * @return integer
	 */
	public function getGoodsCount() {
		$goodCategoriesTablename = self::getTableNamesContainer()->getGoodCategoriesTableName();
		$select = $this->getDb()->select()
			->from($goodCategoriesTablename, array("total" => "COUNT(*)"))
			->where("category_id = ?", $this->getId())
			->where("hidden = 0");
		return $this->getDb()->fetchOne($select);
	}
	
	public function getCheapestPrice() {
		$goodsName = self::getTableNamesContainer()->getGoodsTableName();
		$catsName = self::getTableNamesContainer()->getCategoriesTableName();
		$goodCatsName = self::getTableNamesContainer()->getGoodCategoriesTableName();
		
		$select = $this->getDb()->select()
			->from(array("G" => $goodsName), array("price" => "MIN(price)"))
			->join(array("GC" => $goodCatsName), "GC.good_id = G.id", array())
			->where("G.hidden = 0")
			->where("GC.hidden = 0")
			->where("GC.category_id = ?", $this->getId())
			->where("G.price > 0");
		
		return $this->getDb()->fetchOne($select);
	}
	
	/**
	 * Получить иерархию категорий для текущей
	 * @return array
	 */
	public function getHierarcy() {
		$category = $this;
		$categories = array();
		do {
			$categories[] = $category;
		} while(($category = $category->getParent()) != null);
		
		return array_reverse($categories);
	}
	
	public function getFullUrl() {
		$urlParts = array();
		
		foreach($this->getHierarcy() as $category) {
			/* @var $category Category */
			$urlParts[] = $category->getUrl();
		}
		
		return implode("/", $urlParts);
	}
	
	/**
	 * Получить список сопутсвующих товаров
	 * @return array
	 */
	public function getAttendantGoods() {
		$select = $this->getDb()->select()
			->from(array("A" => self::getTableNamesContainer()->getCategoryAttendantGoodsTableName()), array())
			->join(array("G" => self::getTableNamesContainer()->getGoodsTableName()), "G.id = A.good_id")
			->where("A.category_id  = ?", $this->getId())
			->where("A.hidden = 0")
			->order("A.number DESC");
			
			
		return $this->getDb()->fetchAll($select);
	}
	
	public static function find($id) {
		if(!isset(self::$categories[$id])) {
			self::$categories[$id] = new self($id);
		}
		return self::$categories[$id];
	}
	
	public static function getList() {
		$db = \front\models\Di::getInstance()->get("db");
		$select = $db->select()
			->from(self::getTableNamesContainer()->getCategoriesTableName())
			->where("hidden = 0")
			->where("parent_id IS NULL OR parent_id <> 0")
			->order("number");
		return $db->fetchAll($select);
	}
	
	public static function getTree() {
		/* пока предполагается, что каталог максимум двухуровневый */
		$db = \front\models\Di::getInstance()->get("db");
		$tname = self::getTableNamesContainer()->getCategoriesTableName();
		$parents = $db->fetchAll("SELECT * FROM `$tname` WHERE hidden = 0 AND (parent_id IS NULL OR parent_id = 0)");
		if($parents) {
			foreach($parents as &$parent) {
				$children = $db->fetchAll("SELECT * FROM $tname WHERE hidden = 0 AND parent_id = ? ORDER BY number", $parent['id']);
				$parent['_children'] = $children ? $children : array();
			}
		}
		return $parents;
	}
	
	/**
	 * Функция используется для ЧПУ когда надо по урлу найти категорию, учитывая иерархию
	 * @param array $urls
	 * @return array
	 */
	public static function findTree($urls) {
		$parent = null;
		$db = \front\models\Di::getInstance()->get("db");
		$tname = self::getTableNamesContainer()->getCategoriesTableName();
		$count = count($urls);
		
		foreach ($urls as $key => $url) {
			$select = $db->select();
			if($key + 1 == $count) {
				$select->from($tname);
			} else {
				$select->from($tname, array("id", "parent_id"));
			}
			$select->where("url = ?", $url)
					->where("hidden = 0");
			if($parent) {
				$select->where("parent_id = ?", $parent['id']);
			}
			$parent = $db->fetchRow($select);
			if(!$parent) {
				return null;
			}
		}
		
		return $parent;
	}
	
	public function getNotEmptySets($category_id, $material_id=null, $color_id=null){
		$db = \front\models\Di::getInstance()->get("db");
		$mat='';
		$col='';
		if($material_id){
			$mat=", type_id=".(int) material_id;
		}
		if($color_id){
			$col=", type_id=".(int) color_id;
		}
		return $db->fetchAll("SELECT * FROM `kresla_goods_count`
				WHERE category_id=".$category_id.$mat.$col." 
ORDER BY category_id, type_id, color_id  DESC");
		
	}
	
}