<?php
namespace front\modules\CatalogKresla\models;

require_once ('front/models/Model.php');

class Certificate extends Model {
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getCertificatesTableName();
	}
	
	public static function find($options) {
		return parent::find(self::getTableNamesContainer()->getCertificatesTableName(), $options);
	}
}

?>