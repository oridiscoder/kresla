<?php
namespace front\modules\CatalogKresla\models;

require_once ('Table.php');

class Labels extends Table {
	
	public function getTableName() {
		return $this->getTableNamesContainer()->getLabelsTableName();
	}
	
}