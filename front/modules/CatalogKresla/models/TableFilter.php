<?php
namespace front\modules\CatalogKresla\models;

require_once ('Model.php');
require_once "Good.php";
define("UPTO5000", "upto5000");
define("FROM5000", "from5000");
define("COMPACT", "compact");
define("BIG", "big");

class TableFilter extends Model {
	public $params;
	public $space;
	public $ids;
	public $baseUri;
	public $sqlSeoItem;
	
	public $enum;

	public function getTableParams($catid = false){
		$colors = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT * FROM `table_color` WHERE hidden = 0 ORDER BY number");
		$types = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT * FROM `table_type` WHERE hidden = 0 ORDER BY number");
		$materials = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT * FROM `table_material` WHERE hidden = 0 ORDER BY number");
		$shapes = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT * FROM `table_shape` WHERE hidden = 0 ORDER BY number");
		$tableFilterArray = array();
		$tableFilterArray['types'] = $types;
		$tableFilterArray['colors'] = $colors;
		$tableFilterArray['materials'] = $materials;
		$tableFilterArray['shapes'] = $shapes;
		$tableFilterArray['sizes'] = array(
											'1'=>array('title'=>'большой', 'url' => BIG),
											'2'=>array('title'=>'компактный', 'url' => COMPACT)
											);
		$tableFilterArray['prices'] = array(
											'1'=>array('title'=>'до 5000 P', 'url' => UPTO5000),
											'2'=>array('title'=>'от 5000 Р', 'url' => FROM5000)
											);

											
											
											
		$tempu = explode('/',$_SERVER['REQUEST_URI']);
		if(isset($tempu[3]) && !$catid ){
			$category = \front\models\Di::getInstance()->get("db")->fetchRow("SELECT * FROM `kresla_categories` WHERE url = ?", $tempu[3]);
		}		
		if(!isset($category) && !$catid){
			return;
		}
		if($catid){
                    $category = \front\models\Di::getInstance()->get("db")->fetchRow("SELECT * FROM `kresla_categories` WHERE id = ?", $catid);
		}
// 		die('<pre>'.print_r( $category,1));
// 		if($_SERVER['REQUEST_URI'] == $this->baseUri){
			foreach($tableFilterArray as $paramName=>$params){
				foreach($params as $key=>$fitem){
					$rez = false;
					switch($paramName){
						case 'types': 
// 									$sql = "SELECT * FROM `t_types`  WHERE hidden=0 AND  type_id=".$fitem['id'];
									$sql = "	SELECT `t_types`.*, `kresla_good_categories`.good_id, `kresla_good_categories`.category_id
										FROM `t_types` 
										INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`t_types`.good_id AND `kresla_good_categories`.category_id=".$category['id']."
										INNER JOIN `kresla_goods`
										ON `kresla_goods`.id = `t_types`.good_id AND `kresla_goods`.hidden=0 AND `kresla_goods`.table_filter=1
										AND `t_types`.hidden=0 AND  `t_types`.type_id=".$fitem['id'];

// 									echo $sql; die;

									$rez = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
									break;
						case 'colors': 
// 									$sql = "SELECT * FROM `t_colors`  WHERE hidden=0 AND  color_id=".$fitem['id'];
									$sql = "	SELECT `t_colors`.*, `kresla_good_categories`.good_id, `kresla_good_categories`.category_id
										FROM `t_colors` 
										INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`t_colors`.good_id AND `kresla_good_categories`.category_id=".$category['id']."
										INNER JOIN `kresla_goods`
										ON `kresla_goods`.id = `t_colors`.good_id AND `kresla_goods`.hidden=0 AND `kresla_goods`.table_filter=1
										AND `t_colors`.hidden=0 AND  `t_colors`.color_id=".$fitem['id'];
										
									$rez = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
									break;
						case 'materials':
// 									$sql = "SELECT * FROM `t_material` WHERE hidden=0 AND material_id=".$fitem['id'];
									$sql = "	SELECT `t_material`.*, `kresla_good_categories`.good_id, `kresla_good_categories`.category_id
										FROM `t_material` 
										INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`t_material`.good_id AND `kresla_good_categories`.category_id=".$category['id']."
										INNER JOIN `kresla_goods`
										ON `kresla_goods`.id = `t_material`.good_id AND `kresla_goods`.hidden=0 AND `kresla_goods`.table_filter=1
										AND `t_material`.hidden=0 AND  `t_material`.material_id=".$fitem['id'];									
									$rez = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
									break;
						case 'sizes':
								if($fitem['url']==BIG){
								$sql = "SELECT `kresla_goods`.`id` FROM `kresla_goods` 
																			INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`kresla_goods`.id AND `kresla_goods`.hidden=0 AND
										`kresla_goods`.table_filter=1 AND `kresla_goods`.hidden = 0 AND `kresla_goods`.width>=100 AND `kresla_good_categories`.category_id=".$category['id']."
									";
									
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);
									
								}elseif($fitem['url']==COMPACT){
									$sql = "SELECT `kresla_goods`.`id` FROM `kresla_goods` 
																			INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`kresla_goods`.id AND `kresla_goods`.hidden=0 AND
										`kresla_goods`.table_filter=1 AND `kresla_goods`.hidden = 0 AND `kresla_goods`.width<=100 AND `kresla_good_categories`.category_id=".$category['id']."
									";
									
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);

								}
								
								break;
						case 'shapes':
									$sql = "SELECT `kresla_goods`.`id` FROM `kresla_goods` 
																			INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`kresla_goods`.id AND `kresla_goods`.hidden=0 AND
										`kresla_goods`.table_filter=1 AND `kresla_goods`.hidden = 0 AND `kresla_goods`.shape=".$fitem['id']." AND `kresla_good_categories`.category_id=".$category['id']."
									";
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);
								
								break;
						case 'prices':
								if($fitem['url']==UPTO5000){
									$sql = "SELECT `kresla_goods`.`id` FROM `kresla_goods` 
																			INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`kresla_goods`.id AND `kresla_goods`.hidden=0 AND
										`kresla_goods`.table_filter=1 AND `kresla_goods`.hidden = 0 AND `kresla_goods`.price<=5000 AND `kresla_good_categories`.category_id=".$category['id']."
									";
									
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);
								}elseif($fitem['url']==FROM5000){
									$sql = "SELECT `kresla_goods`.`id` FROM `kresla_goods` 
																			INNER JOIN `kresla_good_categories`
										ON `kresla_good_categories`.good_id =`kresla_goods`.id AND `kresla_goods`.hidden=0 AND
										`kresla_goods`.table_filter=1 AND `kresla_goods`.hidden = 0 AND `kresla_goods`.price>=5000 AND `kresla_good_categories`.category_id=".$category['id']."
									";
									
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);
								}
// 								echo $curtables;
// 								echo '<br>';
								break;
							}
							if(!count($rez)){
								unset($tableFilterArray[$paramName][$key]);
							}
				}

				if(!count($tableFilterArray[$paramName])){
					unset($tableFilterArray[$paramName]);
				}				
			}
// 		}
// die('<pre>'.print_r( $tableFilterArray,1));
		$this->space = array();
		$this->ids = array();
		
		
		foreach ($tableFilterArray as $type => $tar) {
			foreach ($tar as $num => $item) {
				$this->space[$item['url']] = $type;
				$this->ids[$item['url']] = $num;
			}
		}		
		$this->params = $tableFilterArray;
		// die;

	}

	public function __construct() {
		$this->getTableParams();
		
		if(strstr($_SERVER['REQUEST_URI'],'/catalog/home/tables/')){
			$this->baseUri = '/catalog/home/tables/';
			$this->enum = 255;
		}elseif(strstr($_SERVER['REQUEST_URI'],'/catalog/home/zhurnalnye-stoliki/')){
			$this->baseUri = '/catalog/home/zhurnalnye-stoliki/';
			$this->enum = 256;
		}
	}

	public function parseUri(){
		$uri = strtok($_SERVER['REQUEST_URI'],'?');
		$tablesBaseUrl = $this->baseUri;
		if(strstr($uri,$tablesBaseUrl) && $uri != $tablesBaseUrl){
			$relUri = str_replace($tablesBaseUrl,'',$uri);
			$relUriar = explode ('/',$relUri);
			if(!count($relUriar) || count($relUriar) > 6){
				return false;
			}
			$filterAr = array();

			foreach ($relUriar as $num => $uriPart) {
				if(isset($this->space[$uriPart]) ){
					if(isset($filterAr[$this->space[$uriPart]])){
						return false;
					}
					$filterAr[$this->space[$uriPart]] = $this->params[$this->space[$uriPart]][$this->ids [$uriPart] ];
				}elseif($uriPart!=''){

					throw new \Exception("Страница не найдена", 404);
				}
			}
			if(!count($filterAr)){
				return false;
			}else{
				return $filterAr;
			}
		}

	}
	private function filterStep($finalGoods,$newAr){
		foreach ($finalGoods as $id => $value) {
			if(!isset($newAr[$id])){	
				unset($finalGoods[$id]);
			}		
		}
		return $finalGoods;
	}

	public function fetchGoods($filterAr){

		$finalGoods = array();
		$i=0;
		$did = false;
		$pricensize = array(
						BIG => 'width>=100',
						COMPACT => 'width<=100',
						UPTO5000 => 'price<=5000',
						FROM5000 => 'price>=5000'

						);
		$seoItemSql = '';
		$sis = array(
				'table_type' => 'table_type=0',
				'table_color' => 'table_color=0',
				'table_material' => 'table_material=0',
				'sizes' => 'sizes=0',
				'prices' => 'prices=0'
				);
		foreach ($filterAr as $kind => $fitem) {
			
			switch($kind){
				case 'types':
						$sql = "SELECT `good_id` FROM `t_types` JOIN `kresla_goods` ON  `kresla_goods`.table_filter=1 AND `t_types`.good_id=`kresla_goods`.id  WHERE `t_types`.hidden = 0 AND `t_types`.type_id=".$fitem['id'];
// 						echo $sql; die;
// 						SELECT `good_id` FROM `t_types` JOIN `kresla_goods` ON `t_types`.good_id=`kresla_goods`.id WHERE `t_types`.hidden = 0 AND `t_types`.type_id=10
						$g = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);
						if(!$did){
							foreach ($g as $key => $value) {
								$finalGoods[$value['good_id']] = $i++;
							}
						}else{
							$finalGoods = $this->filterStep($finalGoods,$g);
						}
						$sis['table_type']  = 'table_type="'.$fitem['id'].'"';
						$did = true;
						break;
				case 'colors':
						$sql = "SELECT `good_id` FROM `t_colors` JOIN `kresla_goods` ON  `kresla_goods`.table_filter=1 AND `t_colors`.good_id=`kresla_goods`.id  WHERE `t_colors`.hidden = 0 AND `t_colors`.color_id=".$fitem['id'];
						$g = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);
						if(!$did){
							foreach ($g as $key => $value) {
								$finalGoods[$value['good_id']] = $i++;
							}
						}else{
							$finalGoods = $this->filterStep($finalGoods,$g);
						}
						$sis['table_color'] = 'table_color="'.$fitem['id'].'"';
						$did = true;
						break;
				case 'materials':
						$sql = "SELECT `good_id` FROM `t_material` JOIN `kresla_goods` ON  `kresla_goods`.table_filter=1 AND  `t_material`.good_id=`kresla_goods`.id  WHERE `t_material`.hidden = 0 AND `t_material`.material_id=".$fitem['id'];
						$g = \front\models\Di::getInstance()->get("db")->fetchAssoc($sql);
						
						if(!$did){
							foreach ($g as $key => $value) {
								$finalGoods[$value['good_id']] = $i++;
							}
						}else{
							$finalGoods = $this->filterStep($finalGoods,$g);
						}
						$sis['table_material'] = 'table_material="'.$fitem['id'].'"';
						$did = true;
						break;
				case 'sizes':
						if($fitem['url']==BIG){
							$g = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND width>=100");
							$sis['sizes'] = 'sizes=249';
							
						}elseif($fitem['url']==COMPACT){
							$g = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND width<=100");
							$sis['sizes'] = 'sizes=250';
						}
						if(!$did){
							foreach ($g as $key => $value) {
								$finalGoods[$value['id']] = $i++;
							}	
						}else{
							$finalGoods = $this->filterStep($finalGoods,$g);
						}
						break;
				case 'shapes':
						
						$g = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND shape=".$fitem['id']);
						
						if(!$did){
							foreach ($g as $key => $value) {
								$finalGoods[$value['id']] = $i++;
							}	
						}else{
							$finalGoods = $this->filterStep($finalGoods,$g);
						}
						$did = true;	
						break;
				case 'prices':
						if($fitem['url']==UPTO5000){
							$g = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND price<=5000");
							$sis['prices'] = 'prices=247';

							
						}elseif($fitem['url']==FROM5000){
							$g = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND price>=5000");
							$sis['prices'] = 'prices=248';
						}
						if(!$did){
							foreach ($g as $key => $value) {
								$finalGoods[$value['id']] = $i++;
							}	
						}else{
							$finalGoods = $this->filterStep($finalGoods,$g);
						}		
						$did = true;	
						break;

			}
		}
		// echo substr($seoItemSql,4); die;
			$sis['enum'] = 'category='.$this->enum;
			$seoItemSql = implode(' AND ', $sis);
			$sql = "SELECT * FROM `table_metatags` WHERE hidden=0 AND ".$seoItemSql;
			
// 			$cc = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
// 			echo $this->enum;
			
// 			<option value="255">Компьютерные столы</option>
// <option value="256">Журнальные столы</option>
			$its = \front\models\Di::getInstance()->get("db")->fetchRow($sql);
			if($its){
				$this->tableSeoItem = $its;				
			}else{
				$this->tableSeoItem = false;
			}
		

		$sql2 = implode(',',array_flip($finalGoods));
// die('<pre>'.print_r( get_defined_vars(),1));
		return $sql2;

		$sql = "SELECT `id` FROM `kresla_goods` WHERE id IN (".$sql2.")";
		$goods = \front\models\Di::getInstance()->get("db")->fetchAll("SELECT * FROM `kresla_goods` WHERE id IN (".$sql2.")");

		return $goods;
		// $goods = Good::hitchItems($g, 'types', 't_types', 'table_type', 'type_id', $fieldsFromLinkedTable = array());
		// $goods = Good::hitchItems($goods, 'material', 't_material', 'table_material', 'material_id', $fieldsFromLinkedTable = array());
		// $goods = Good::hitchItems($goods, 'material', 't_material', 'kresla_colors', 'color_id', $fieldsFromLinkedTable = array());
		
	}

	
	// public function find() {
	// 	$db = \front\models\Di::getInstance()->get("db");
	// 	// fetchOne("SELECT category_id FROM `$goodCatsName` WHERE good_id = ? AND hidden = 0 ORDER BY `primary` DESC LIMIT 1", $this->getID());
	// 	fetchOne("SELECT category_id FROM `$goodCatsName` WHERE good_id = ? AND hidden = 0 ORDER BY `primary` DESC LIMIT 1", $this->getID());

	// 	return isset(self::$labels[$id]) ? self::$labels[$id] : null;
	// }
}