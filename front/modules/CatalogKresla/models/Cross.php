<?php
namespace front\modules\CatalogKresla\models;

require_once ('Model.php');

class Cross extends Model {
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getCrossTableName();
	}
	
	public static function find($options) {
		return parent::find(self::getTableNamesContainer()->getUpholsteryTableName(), $options);
	}
	
	public static function getTypes() {
		$tableName = self::getTableNamesContainer()->getCrossTypesTableName();
		$db = \front\models\Di::getInstance()->get("db");
		$select = $db->select()
			->from($tableName)
			->where("hidden = 0")
			->order("number");
		return $db->fetchAll($select);
	}
}