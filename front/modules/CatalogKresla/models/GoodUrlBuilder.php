<?php
namespace front\modules\CatalogKresla\models;

class GoodUrlBuilder {
	private $db;
	private $categories = array();
	public function __construct() {
		$this->db = \front\models\Di::getInstance()->get("db");
	}
	public function getGoodURL(Good $good) {
		$category = $this->getCategory($good->getCategoryID());
		return $category->getFullUrl(). '/' . $good->getID().'.html';
	}
	
	/**
	 * @param integer $id
	 * @return Category
	 */
	private function getCategory($id) {
		if(!isset($this->categories[$id])) {
			$this->categories[$id] = new Category($id);
		}
		return $this->categories[$id];
	}
}