<?php
namespace front\modules\CatalogKresla\models;

require_once ('front/models/Model.php');

class UpholsteryType extends Model {
	
	private static $types;
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getUpholsteryTypesTableName();
	}

	public static function find($id) {		
		if(!self::$types) {
			$db = \front\Models\Di::getInstance()->get("db");
			$tableName = self::getTableNamesContainer()->getUpholsteryTypesTableName();
			self::$types = $db->fetchAssoc("SELECT * FROM $tableName WHERE hidden = 0 ORDER BY `number`");
		}
		return isset(self::$types[$id]) ? self::$types[$id] : null;
	}
	
	public function findByOptions($options){
		
		
		return parent::find(self::getTableName(), $options);
		
	}
	
	public static function getList() {
		$db = \front\Models\Di::getInstance()->get("db");
		$tableName = self::getTableNamesContainer()->getUpholsteryTypesTableName();
		$select = $db->select()->from($tableName)
			->where("hidden = 0")
			->order("number");
		return $db->fetchAll($select);
	}
	
	public function getNotEmptyTypes($category_id, $color_id=null){
		$db = \front\models\Di::getInstance()->get("db");
		$col='';
		if($color_id){
			$col=" AND color_id=".(int) $color_id;
		}
		
		$notEmptyTypes = $db->fetchAll("SELECT DISTINCT type_id FROM `kresla_goods_count`
				WHERE category_id=".(int)$category_id.$col);
		foreach($notEmptyTypes as $typ){
			$ids[]=$typ['type_id'];
		}
		
		if(count($ids)==0)
			return 0;
		
		$tableName = self::getTableNamesContainer()->getUpholsteryTypesTableName();
		$select = $db->select()->from($tableName)
			->where("hidden = 0")
			->order("number")
			->where("id IN(?)", $ids);
		return $db->fetchAll($select);
	}
}