<?php
namespace front\modules\CatalogKresla\models;

/**
 * Хранилище названий таблиц каталога
 * @author gourry
 */
class TableNames {
	/**
	 * Получить префикс для таблиц БД
	 */
	public function getDbPrefix() {
		$di = \front\models\Di::getInstance();
		return $di->get("config")->db_prefix;
	}
	/**
	 * Название таблицы категорий
	 */
	public function getCategoriesTableName() {
		return $this->getDbPrefix().'categories';
	}
	/**
	 * Название таблицы товаров
	 */
	public function getGoodsTableName() {
		return $this->getDbPrefix().'goods';
	}
	/**
	 * Название таблицы ярлыков
	 */
	public function getLabelsTableName() {
		return $this->getDbPrefix().'labels';
	}
	/**
	 * Название таблицы обивок
	 */
	public function getUpholsteryTableName() {
		return $this->getDbPrefix().'upholstery';
	}
	/**
	 * Название таблицы материалов подлокотников
	 */
	public function getArmTypesTableName() {
		return $this->getDbPrefix().'arm_types';
	}
	/**
	 * Название таблицы подлокотников
	 */
	public function getArmsTableName() {
		return $this->getDbPrefix().'arms';
	}
	/**
	 * Название таблицы сертификатов
	 */
	public function getCertificatesTableName() {
		return $this->getDbPrefix().'certificates';
	}
	/**
	 * Название таблицы типов крестовин
	 */
	public function getCrossTypesTableName() {
		return $this->getDbPrefix().'cross_types';
	}
	/**
	 * Название таблицы крестовин
	 */
	public function getCrossTableName() {
		return $this->getDbPrefix().'cross';
	}
	
	/**
	 * Название таблицы типов обивок
	 */
	public function getUpholsteryTypesTableName() {
		return $this->getDbPrefix()."upholstery_types";
	}
	
	public function getColorsTableName() {
		return $this->getDbPrefix()."colors";
	}
	
	public function getCategoryAttendantGoodsTableName()  {
		return $this->getDbPrefix()."category_attendant_goods";
	}
	/**
	 * Название таблицы производителей
	 */
	public function getVendorsTableName() {
		return $this->getDbPrefix().'vendors';
	}
	public function getGoodImagesTableName() {
		return $this->getDbPrefix().'good_images';
	}
	public function getGoodCategoriesTableName() {
		return $this->getDbPrefix().'good_categories';
	}
	public function getGoodUpholsteryTableName() {
		return $this->getDbPrefix().'good_upholsteries';
	}
	public function getGoodCrossTableName() {
		return $this->getDbPrefix().'good_crosses';
	}
	public function getGoodArmsTableName() {
		return $this->getDbPrefix().'good_arms';
	}
	public function getServicesTableName() {
		return $this->getDbPrefix()."services";
	}
	public function getGoodServicesTableName() {
		return $this->getDbPrefix()."good_services";
	}
	public function getGoodCertificatesTableName() {
		return $this->getDbPrefix().'good_certificates';
	}
	public function getPopularGoodsTableName() {
		return $this->getDbPrefix().'popular_goods';
	}
	public function getNewGoodsTableName() {
		return $this->getDbPrefix().'news_goods'; 
	}
}