<?php

namespace front\modules\CatalogKresla\models;

use front\models\Di;

abstract class Model {
	
	/**
	 * 
	 * @var \Zend_Db_Adapter_Abstract
	 */
	private $db;
	private static $tableNamesContainer;
	/**
	 * Сохраняем сюда значения, которые должны вычисляться одни раз в течение запроса
	 * @var array
	 */
	private static $cache;
	
	public function __construct() {
		$this->db = Di::getInstance()->get("db");
	}
	
	/**
	 * @return \Zend_Db_Adapter_Abstract
	 */
	protected function getDb() {
		return $this->db;
	}
	
	/**
	 * @return TableNames
	 */
	public static function getTableNamesContainer() {
		if(self::$tableNamesContainer == null) {
			self::$tableNamesContainer = new TableNames();
		}
		return self::$tableNamesContainer;
	}
	
	public static function find($tableName, $options) {
		
		/* @var $db \Zend_Db_Adapter_Abstract */
		$db = Di::getInstance()->get("db");
		
		require_once "models/Table/DbTableLight.php";
		$table = new \DbTableLight($db, $tableName);
				
		$select = $db->select()
			->from($tableName)
			->where("hidden = 0");
		
		if(is_numeric($options)) {
			$options = array("where" => array("id = ?" => $options));
		}
				
		
		if(isset($options['where'])) {
			if(is_array($options['where'])) {
				if(isset($options['where'][0])) {
					foreach ($options['where'] as $where) {
						$select->where($options['where']);
					}
				} else {
					foreach ($options['where'] as $whereCond => $whereValues) {
						$select->where($whereCond, $whereValues);
					}
				}
			} else {
				$select->where($options['where']);
			}			
		}
		
		if(isset($options['order'])) {
			$select->where($options['order']);
		}
		
		if(isset($options['limit'])) {
			$select->where($options['limit']);
		}

		$rows = $db->fetchAll($select);
		
		if($rows) {
			foreach ($table->getFields() as $field) {
				if($field instanceof \ImageField) {
					foreach ($rows as $key => $row) {
						$rows[$key][$field->getName()] = $field->getURL($row);
						$rows[$key][$field->getName().'_orig'] = $field->getOriginalURL($row);
					}
				}
			}
		}
		
		return $rows;
	}
	
	/**
	 * Раскрывает пути для изображений
	 * @param array $rows
	 * @param string $tableName - название таблицы
	 */
	public static function expandImagesUrls($rows, $tableName) {
		require_once "models/Table/DbTableLight.php";
		$table = new \DbTableLight(\front\models\Di::getInstance()->get("db"), $tableName);
		foreach ($table->getFields() as $field) {
			if($field instanceof \ImageField) {
				foreach ($rows as $key => $row) {
					$rows[$key][$field->getName()] = $field->getURL($row);
					$rows[$key][$field->getName().'_orig'] = $field->getOriginalURL($row);
				}
			}
		}
		
		return $rows;
	}
	
	/**
	 * Получить значение из кеша, если оно там есть. Иначе сохранить в кеш результат вызова функции $callback
	 * @param string $name
	 * @param callable $callback
	 * @return mixed 
	 */
	protected static function getCached($name, $callback) {
		if(!isset(self::$cache[$name])) {
			self::$cache[$name] = $callback();
		}
		return self::$cache[$name];
	}
	
	public static function getList() {
		$tableName = self::getTableName();
		if($tableName) {
			$db = \front\models\Di::getInstance()->get("db");
			$select = $db->select()
				->from($tableName)
				->where("hidden = 0")
				->order("number");
			return $db->fetchAll($select);
		} else {
			throw new WrongSchemaException("You must realize getTableName() static method");
		}
	}
	
}

class RecordNotFoundException extends \Exception {
	
}

class WrongSchemaException extends \Exception {
	
}