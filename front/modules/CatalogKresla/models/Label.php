<?php
namespace front\modules\CatalogKresla\models;

require_once ('Model.php');

class Label extends Model {
	
	private static $labels;
	
	public static function find($id) {
		$db = \front\models\Di::getInstance()->get("db");
		
		if(self::$labels == null) {
			$tname = self::getTableNamesContainer()->getLabelsTableName();
			self::$labels = $db->fetchAssoc("SELECT * FROM `$tname` WHERE hidden = 0");
		}
		
		return isset(self::$labels[$id]) ? self::$labels[$id] : null;
	}
}