<?php
namespace front\modules\CatalogKresla\models;

require_once ('front/models/Model.php');

class Service extends Model {
	public static function getTableName() {
		return self::getTableNamesContainer()->getServicesTableName();
	}
	
	public static function find($options) {
		return parent::find(self::getTableNamesContainer()->getServicesTableName(), $options);
	} 
}