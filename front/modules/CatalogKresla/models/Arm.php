<?php
namespace front\modules\CatalogKresla\models;

require_once ('front/models/Model.php');

class Arm extends Model {
	private $row;
	
	public static function getTableName() {
		return self::getTableNamesContainer()->getArmsTableName();
	}
	
	public function __construct($row) {
		$this->row = $row;
	}
	
	public static function find($options) {
		
		if(is_numeric($options)) {
			$options['where'] = array("id = ?" => $options);
		}
		
		$options['table_name'] = self::getTableNamesContainer()->getArmsTableName();
		
		return parent::find($options);
	}
	
	public static function getTypes() {
		$tableName = self::getTableNamesContainer()->getArmTypesTableName();
		$db = \front\models\Di::getInstance()->get("db");
		$select = $db->select()
			->from($tableName)
			->where("hidden = 0")
			->order("number");
		return $db->fetchAll($select);
	}
	
}