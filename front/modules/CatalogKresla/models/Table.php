<?php

namespace front\modules\CatalogKresla\models;

use front\models\Di;

abstract class Table {
	
	private static $tableNamesContainer;
	/**
	 * Сохраняем сюда значения, которые должны вычисляться одни раз в течение запроса
	 * @var array
	 */
	private $cache;
	
	private static $tables;
	
	/**
	 * @return string
	 */
	abstract public function getTableName();
	
	/**
	 * 
	 * @param string $className
	 * @return Table
	 */
	public static function getTable($className) {
		if(!isset(self::$tables[$className])) {
			$className = "\\front\\modules\\CatalogKresla\\models\\".ucfirst($className);
			self::$tables[$className] = new $className();
		}
		return self::$tables[$className];
	}
	
	/**
	 * @return \Zend_Db_Adapter_Abstract
	 */
	protected function getDb() {
		return Di::getInstance()->get("db");
	}
	
	/**
	 * @return TableNames
	 */
	protected function getTableNamesContainer() {
		if(self::$tableNamesContainer == null) {
			self::$tableNamesContainer = new TableNames();
		}
		return self::$tableNamesContainer;
	}
	
	/**
	 * Получить значение из кеша, если оно там есть. Иначе сохранить в кеш результат вызова функции $callback
	 * @param string $name
	 * @param callable $callback
	 * @return mixed 
	 */
	protected function getCached($name, $callback) {
		if(!isset($this->cache[$name])) {
			$this->cache[$name] = $callback();
		}
		return $this->cache[$name];
	}
	
	/**
	 * Получить все записи из таблицы, кроме hidden = 0. Упорядочены по number
	 * @return array
	 */
	public function getList() {
		$tableName = $this->getTableName();
		$db = $this->getDb();
		$callback = function () use ($tableName, $db){
			$select = $db->select()
				->from($tableName)
				->where("hidden = 0")
				->order("number");
			$rows = $db->fetchAll($select);
			return $rows ? $rows : array();
		};
		return $this->getCached("__table_rows__", $callback);
	}
	
}