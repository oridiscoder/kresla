<?php
namespace front\modules\CatalogKresla\models;
use front\models\Di;

class CookieStorage {
	public function saveGoodView($goodID) {
		
		$items = $this->getViewedGoods();
		if(!isset($items[$goodID])) {
			$items[$goodID] = time();
		}
		
		foreach ($items as $id => $time) {
			$items[$id] = $id.':'.$time;
		}
		
		setcookie("good_views", implode(",", $items), time() + 3600 * 24 * 30, '/');
	}
	
	public function getViewedGoods() {
		$items = isset($_COOKIE['good_views']) ? explode(",", $_COOKIE['good_views']) : array();
		$goods = array();
		foreach ($items as $key => $item) {
			list($id, $time) = explode(":", $item);
			$goods[$id] = $time;
		}
// 		if($_SERVER['REMOTE_ADDR'] == '80.90.120.182'){
			foreach($goods as $ke => $i){
				$sql = "SELECT `id` FROM `kresla_goods` WHERE `id`= ". $i['id'];
				$t =  Di::getInstance()->get("db")->fetchAll($sql);
				if(!$t){
					unset($goods[$ke]);
				}
			}
// 		}			
		
		return $goods;
	}
}