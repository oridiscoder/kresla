<div class="BekInCat">
<?php if(isset($_SERVER['HTTP_REFERER'])){ ?>
  <a href="<?=$_SERVER['HTTP_REFERER']?>"><img src="/img/beckStrelka.gif" alt="" /> <u>Назад к списку товаров</u></a>
<?php  }?>
  </div>
<!-- ======================================================================================================== -->  
  				<?php if (isset($_GET['dev'])): ?>
				<div class="blokCatalog">
					<div class="oneElementCatalog">
						<div class="productCard">
							<div class="productCardLeft">
								<div class="productPic">
		                    		<div class="lightBoxImg">
		                    		<?if ($good['model3d']):?>
										<div class="a-0" style="display: none;">
											<iframe src="/cms/cms-files/kresla_goods/<?=$good['id']?>_model3d_<?=$good['model3d']?>" width="100%" height="400" align="left"></iframe>
										</div>				
									<?endif?>
			                    		
			                    		
		                    		<?php if ($good['_photos']): foreach($good['_photos'] as $key => $photo): ?>
	                                  <a href="<?=$photo['image_orig']?>" title="<?=$good['title']?>" class="a-<?=$photo['id']?>" <?php if($key > 0): ?>style="display: none;"<?php endif; ?>>
	                                  	<div class="zoom img<?=$photo['id']?>"></div>
	                                  	<img  title="<?=$good['title']?>" alt="<?=$good['title']?>" class="bigImage" id="img<?=$photo['id']?>" src="<?=$photo['image']?>" />
	                                  </a>
									<?php endforeach; endif; ?>
									
		                    		</div>
		                    	</div>
		                    	<div class="cardCarousel">
			                    	
			                    	<div class="jcarousel-wrapper">
				        				<div class="jcarousel" data-jcarousel="true">
				    						<ul class="selectPik">
					    						
					    						<?if ($good['model3d']):?>
													<li id="thumb-0" data-photo-id="0" class="click-to-select-img  <?php if($key == 0): ?>selekt<?php endif; ?> ">
														<img src="/newImg/photo-icon.png" />
														<div class="d-label"></div>
													</li>
												<?endif?>
					    						
					                        	<?php if ($good['_photos']): foreach($good['_photos'] as $key => $photo): ?>
				                                  <li id="thumb<?=$photo['id']?>" data-photo-id="<?=$photo['id']?>" class="click-to-select-img  <?php if($key == 0): ?>selekt<?php endif; ?> "><img alt="" src="<?=$photo['thumb']?>" data-photo-id="<?=$photo['id']?>"/></li>
												<?php endforeach; endif; ?>
												
												
												
					                        </ul>
				        				</div>
				        				<a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true">‹</a>
										<a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true">›</a>
				    				</div>
		                    	</div>
		                    	
		                    	
		                    	
			                    		
			                    		
			                    		
		                        
		                        
		                        <div class="prodAdv">
			                        <div class="prodAdvItem">
										<a class="pic" href="/customers/delivery/"><img src="/newImg/tabsPropPic2.jpg" alt=""></a>
										<div class="prodAdvText">
											<a class="title" href="/customers/delivery/"><span>Доставка</span></a>
											<span>- каждый день от 500 руб.</span>
										</div>
									</div>
									<div class="prodAdvItem">
										<a class="pic" href="#"><img src="/newImg/prodAdv2.png" alt=""></a>
										<div class="prodAdvText">
											<a class="title" href="#"><span>Сборка</span></a>
											<span>- в день доставки от 300 руб.</span>
										</div>
									</div>
									<div class="prodAdvItem">
										<a class="pic" href="#"><img src="/newImg/prodAdv3.png" alt=""></a>
										<div class="prodAdvText">
											<span>Подарок с каждой покупкой!</span>
											<span class="small">Добавьте после покупки отзыв о кресле на <a href="#">Яндекс.Маркет</a> и получите гарантированный ПОДАРОК от нашей компании.</span>
										</div>
									</div>
									<div class="prodAdvItem">
										<a class="pic" href="#"><img src="/newImg/prodAdv4.jpg" alt=""></a>
										<div class="prodAdvText">
											<span>Возврат и обмен без проблем!</span>
										</div>
									</div>
									<div class="clear"></div>
		                        </div>
							</div>
							<div class="productCardRight">
								<h1><? if( $good['category']->getId() != 17 && $good['hidecat'] != 1){ ?><?=$good['category']->getTitle1();?> <? } ?><?=$good['title']?></h1>
								<span class="cardOptions">
									<span class="sborka"><span class="ico-sborka"></span>Требуется сборка</span>
									<?php if ($good['to_order'] == false):?>
									<span class="green">В&nbsp;наличии</span>
									<?php else: ?>
									<span>На&nbsp;заказ&nbsp;от&nbsp;3-х&nbsp;дней</span>
									<?php endif; ?>
								</span>
								  
                          
								  <form action="/basket/" method="POST" id="OrderForm">
										<input type="hidden" name="id" value="<?=$good['id']?>" />
										<input type="hidden" name="action" value="basket_add" />
										
										<div class="fabricBlock">
											<div class="title">Варианты цвета</div>
											<?=$good['_upholstery_html'];?>
										</div> <!-- .fabricBlock -->
										
										<?=$good['_arms_html'];?>
				                        
		                				<div class="cartBlock">
			                				<div class="cartPriceBlock">
				                				<div class="cartPrice">
					                				<span id="newPrice"><?=$good['_default_price']?></span> руб.<br/>
													<a href="#CheapClickDialog" class="btn-dialog-hid">Хочу дешевле!</a>
				                				</div>
												<div class="cartAmount">
													<span class="minus">-</span>
													<input type="text" name="count" value="1" size="5" id="productCount" maxlength="6" />
													<span class="plus">+</span>
												</div>
			                				</div>
			                				
			                				<div class="cartBtnsBlock">
				                				<div class="cartSubmitBtn">
													<a href="javascript:void(0);" class="ajax_submit_button">В корзину</a>
												</div>
												<a href="#OneClickDialog" class="cartFormLink btn-dialog-hid">Купить в 1 клик</a>
			                				</div>
										</div><!-- .cartBlock -->
					                    <p>Цена указана с учётом НДС</p>
					                    
									</form>
									
									<div class="hidden-form" id="OneClickDialog" >
										<div class="hidden-form-close">&times;</div>
										<div class="title">Купить в 1 клик</div>
										<span>Укажите ваше имя и телефон для оформления заказа. Менеджер свяжется с вами в ближайшее время.</span>
										<form id="OneClick" class="form">
											<div class="f-row">
												<div class="f-col-100">
													<label>Имя</label>
													<input type="text" name="oneclick-name" />
												</div>
												<div class="f-col-100">
													<label>Телефон</label>
													<input type="text" name="oneclick-phone" placeholder="+7"/>
												</div>
											</div>
											<input type="submit" value="заказaть" class="orange-btn">
										</form>
									</div>
									
									<div class="hidden-form" id="CheapClickDialog">
										<div class="hidden-form-close">&times;</div>
										<div class="title">Хочу дешевле</div>
										<span>Укажите ваше имя и телефон для оформления заказа. Менеджер свяжется с вами в ближайшее время.</span>
										<form id="CheapClick" class="form">
											<div class="f-row">
												<div class="f-col-100">
													<label>Имя</label>
													<input type="text" name="oneclick-name" />
												</div>
												<div class="f-col-100">
													<label>Телефон</label>
													<input type="text" name="oneclick-phone" placeholder="+7"/>
												</div>
											</div>
											<input type="submit" value="заказaть" class="orange-btn">
										</form>
									</div>
                          
							</div>
							
						</div> <!-- .productCard -->
					</div>
					
					<div class="advBlock">
						<div class="advItem">
							<span class="icon-adv-1"></span>
							<span>13 лет вместе: множество довольных клиентов и позитивного опыта работы с нами!</span>
							
						</div>
						<div class="advItem">
							<span class="icon-adv-2"></span>
							<span>Расширенная гарантия на любую продукцию нашей компании</span>
							
						</div>
						<div class="advItem">
							<span class="icon-adv-3"></span>
							<span>Консультация 20 часов в сутки, без выходных</span>
							
						</div>
						<div class="advItem">
							<span class="icon-adv-4"></span>
							<span>Бесплатный тест-драйв перед покупкой</span>
							
						</div>
						<div class="clear"></div>
					</div>
					
					<div class="productTabs">
						<div class="b-prodProp">
							<div class="b-prodProp-tabs">
								<a href="#" class="b-item active">Описание</a>
								<a href="#" class="b-item">Характеристики</a>
								<a href="#" class="b-item">Отзывы (<?=$good['_comments_count']?>)</a>
								<?if ($good['_certificates']): ?><a href="#" class="b-item">Сертификаты</a><? endif; ?>
							</div>
							<div class="b-prodProp-panes">
								<div class="b-item active">
		                          <?=$good['text']?>
		                          <br />
		                          <br />
		                          
		                          <?if ($good['model3d']):?>
		                          
									<!--noindex-->
									<div class="b-indLink m-table">
										<a class="pic"  rel=”nofollow” href="/cms/cms-files/kresla_goods/<?=$good['id']?>_model3d_<?=$good['model3d']?>"><img src="/newImg/tabsPropPic1.jpg" alt=""></a>
										<a class="title"  rel=”nofollow” href="/cms/cms-files/kresla_goods/<?=$good['id']?>_model3d_<?=$good['model3d']?>"><span>3D Модель</span></a>
										<span>покрутить, посмотреть, оценить</span>
									</div>
									<!--/noindex-->	
								
									<?endif?>
								</div>
								<div class="b-item">
									<table class="b-propTable">
									<?if($good['back_height']):?>
										<tr>
											<td>Высота спинки</td>
											<td><?=$good['back_height']?> см</td>
										</tr>
									<?endif?>
									<?if($good['back_adjust']):?>
										<tr>
											<td>Регулируемый угол наклона спинки</td>
											<td><?php if($good['back_adjust'] == false):?><span class="icon smallCloser"></span>Нет<?php else: ?>Да<?php endif; ?></td>
										</tr>
										<?endif?>
									<?if($good['_arm']['title']):?>
										<tr>
											<td>Подлокотник</td>
											<td>
											<?php foreach($good['_arms'] as $arm)
													echo $arm['title'].', ';
											
											?>
											</td>
											
										</tr>
									<?endif?>
									<?if($good['width'] || $good['deep'] || $good['height_max'] || $good['height_min']):?>
										<tr>
											<td>Габариты ( Ширина × Глубина × Высота )</td>
											<td><?=$good['width']?> см × <?=$good['deep']?> см × <?=$good['height_min']?> 
											<?
											if($good['height_min'] != $good['height_max']){ 
												if($good['height_min'] && $good['height_max']){
													echo "&#247; ";
												}
												echo $good['height_max'];
											}
												
											?>
											 см
											</td>
										</tr>
									<?endif?>
									<?if($good['weight_limit']):?>
										<tr>
											<td>Ограничение по весу</td>
											<td>до <?=$good['weight_limit']?> кг</td>
										</tr>
									<?endif?>
									
									<?if($good['_cross']):?>
										<tr>
											<td>Тип крестовины</td>
											<td><?=$good['_cross']['title']?></td>
										</tr>
									<?endif?>
									</table>
								</div>
								<div class="b-item">
									
									<?php if ($good['_comments_html']): ?>
										<?=$good['_comments_html']?>
									
										<?php if($good['_comments_count'] > $good['_comments_onpage']):
										/* вычислим количество комментариев на след. странице */
										$nextPageCommentsCount = $good['_comments_count'] > $good['_comments_onpage'] * 2 ? $good['_comments_onpage'] : $good['_comments_count'] -  $good['_comments_onpage'];
										?>
										<a class="b-moreLink" href="javascript:void(0);" onclick="comments.showMore();">Еще отзывы(<?=$nextPageCommentsCount?>)</a>
										<?php endif;?>
										
									<?php endif; ?>
									
									<div class="clear"></div>
									
									<a class="b-addReview-link" href="#">Добавить отзыв</a>
									
									<div class="b-addReview form">
										<?=$good['_comments_form']?>				
									</div><!-- .b-addReview -->
								</div>
								<div class="b-item">
								<?php if ($good['_certificates']): ?>									
									<ul class="b-sertList">
									<?php foreach($good['_certificates'] as $cert): ?>
										<li>
											<a href="<?=$cert['image_orig']?>" title="<?=htmlspecialchars($cert['title'])?>"><img src="<?=$cert['image']?>" alt="<?=htmlspecialchars($cert['title'])?>" title="<?=htmlspecialchars($cert['title'])?>"></a>
											<span><?=htmlspecialchars($cert['title'])?></span>
										</li>
									<?php endforeach; ?>
									</ul>
								<?php endif; ?>
								</div>
							</div>
						</div><!-- .b-prodProp -->
					</div> <!-- .productTabs -->
					
					

				</div> <!-- .blokCatalog  -->
				
				
				
			<?php if($good['_same_goods_html']):?>
				<div class="catalogDivider"></div>
			
				<div class="blokCatalog">
					<div class="concCarousel ">
	    				
	    				<div class="concLeft">
		    				<p class="b-header">Сопутствующие товары</p>
	    					<div class="jcarousel-wrapper">
		        				<div class="jcarousel" data-jcarousel="true">
		    						<?php echo $good['_same_goods_html'];?>
		        				</div>
		        				<a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true">‹</a>
								<a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true">›</a>
		    				</div>
	    				</div>
	    				
	    				<div class="concRight">
		    				<p class="b-header">Вас могут заинтересовать</p>
		    				<a href="#">Мебель для персонала</a>
		    				<a href="#">Мебель для кабинета руководителя</a>
		    				<a href="#">Мебель для приёмных</a>
		    				<a href="#">Мягкая офисная мебель</a>
		    				<a href="#">Офисные столы</a>
		    				<a href="#">Офисные кресла</a>
	    				</div>
	    				
	    				<div class="clear"></div>
	    				
	    			</div><!-- .prodCarousel -->
				</div>
    		<?php endif?>
    		
    		
    		
				
			<?php if($good['_same_goods_html']):?>
				<div class="catalogDivider"></div>
				
				<div class="blokCatalog">
					<div class="prodCarousel">
	    				<p class="b-header">Похожие товары</p>
	    				
	    				<div class="jcarousel-wrapper">
	        				<div class="jcarousel" data-jcarousel="true">
	    						<?php echo $good['_same_goods_html'];?>
	        				</div>
	        				<a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true">‹</a>
							<a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true">›</a>
	    				</div>
	    			</div><!-- .prodCarousel -->
				</div>
    		<?php endif?>
    			
				
				
				<div style="width: 100%;border-bottom: 3px solid black"></div>
				<?php endif ?>
<!-- ======================================================================================================== -->
  				<div class="blokCatalog">
    				<div class="oneElementCatalog">
                   		<div class="rightVizText">
						<div class="titleText">
                          <h1><? if( $good['category']->getId() != 17 && $good['hidecat'] != 1){ ?><?=$good['category']->getTitle1();?> <? } ?><?=$good['title']?></h1>
                          <?php if ($good['to_order'] == false):?>
                          <span class="green">В&nbsp;наличии</span>
                          <?php else: ?>
                          <span>На&nbsp;заказ&nbsp;от&nbsp;3-х&nbsp;дней</span>
                          <?php endif; ?>
                          </div>

							<div class="b-prodProp">
								<div class="b-prodProp-tabs">
									<a href="#" class="b-item active">Описание</a>
									<a href="#" class="b-item">Характеристики</a>
									<a href="#" class="b-item">Отзывы (<?=$good['_comments_count']?>)</a>
									<?if ($good['_certificates']): ?><a href="#" class="b-item">Сертификаты</a><? endif; ?>
								</div>
								<div class="b-prodProp-panes">
									<div class="b-item active">
                                      <?=$good['text']?>
                                      <br />
                                      <br />
                                      
                                      <?if ($good['model3d']):?>
                                      
										<!-- noindex -->
										<div class="b-indLink m-table">
											<a class="pic"  rel=”nofollow” href="/cms/cms-files/kresla_goods/<?=$good['id']?>_model3d_<?=$good['model3d']?>"><img src="/newImg/tabsPropPic1.jpg" alt=""></a>
											<a class="title"  rel=”nofollow” href="/cms/cms-files/kresla_goods/<?=$good['id']?>_model3d_<?=$good['model3d']?>"><span>3D Модель</span></a>
											<span>покрутить, посмотреть, оценить</span>
										</div>
										<!-- /noindex -->
									
										<?endif?>
										<div class="b-indLink">
											<a class="pic" href="#"><img src="/newImg/tabsPropPic2.jpg" alt=""></a>
											<a class="title" href="/customers/delivery/"><span>Доставка и оплата</span></a>
											<span>самовывоз, доставка, разгрузка и способы оплаты</span>
										</div>
									</div>
									<div class="b-item">
										<table class="b-propTable">
										<?if($good['back_height']):?>
											<tr>
												<td>Высота спинки</td>
												<td><?=$good['back_height']?> см</td>
											</tr>
										<?endif?>
										<?if($good['back_adjust']):?>
											<tr>
												<td>Регулируемый угол наклона спинки</td>
												<td><?php if($good['back_adjust'] == false):?><span class="icon smallCloser"></span>Нет<?php else: ?>Да<?php endif; ?></td>
											</tr>
											<?endif?>
										<?if($good['_arm']['title']):?>
											<tr>
												<td>Подлокотник</td>
												<td>
												<?php foreach($good['_arms'] as $arm)
														echo $arm['title'].', ';
												
												?>
												</td>
												
											</tr>
										<?endif?>
										<?if($good['width'] || $good['deep'] || $good['height_max'] || $good['height_min']):?>
											<tr>
												<td>Габариты ( Ширина × Глубина × Высота )</td>
												<td><?=$good['width']?> см × <?=$good['deep']?> см × <?=$good['height_min']?> 
												<?
												if($good['height_min'] != $good['height_max']){ 
													if($good['height_min'] && $good['height_max']){
														echo "&#247; ";
													}
													echo $good['height_max'];
												}
													
												?>
												 см
												</td>
											</tr>
										<?endif?>
										<?if($good['weight_limit']):?>
											<tr>
												<td>Ограничение по весу</td>
												<td>до <?=$good['weight_limit']?> кг</td>
											</tr>
										<?endif?>
										
										<?if($good['_cross']):?>
											<tr>
												<td>Тип крестовины</td>
												<td><?=$good['_cross']['title']?></td>
											</tr>
										<?endif?>
										</table>
									</div>
									<div class="b-item">
										<a class="b-addReview-link" href="#">Добавить отзыв</a>
										
										<div class="b-addReview">
											<?=$good['_comments_form']?>				
										</div>  
										
										<?php if ($good['_comments_html']): ?>
											<?=$good['_comments_html']?>
										
											<?php if($good['_comments_count'] > $good['_comments_onpage']):
											/* вычислим количество комментариев на след. странице */
											$nextPageCommentsCount = $good['_comments_count'] > $good['_comments_onpage'] * 2 ? $good['_comments_onpage'] : $good['_comments_count'] -  $good['_comments_onpage'];
											?>
											<a class="b-moreLink" href="javascript:void(0);" onclick="comments.showMore();">Еще отзывы(<?=$nextPageCommentsCount?>)</a>
											<?php endif;?>
											
										<?php endif; ?>
									</div>
									<div class="b-item">
									<?php if ($good['_certificates']): ?>									
										<ul class="b-sertList">
										<?php foreach($good['_certificates'] as $cert): ?>
											<li>
												<a href="<?=$cert['image_orig']?>" title="<?=htmlspecialchars($cert['title'])?>"><img src="<?=$cert['image']?>" alt="<?=htmlspecialchars($cert['title'])?>" title="<?=htmlspecialchars($cert['title'])?>"></a>
												<span><?=htmlspecialchars($cert['title'])?></span>
											</li>
										<?php endforeach; ?>
										</ul>
									<?php endif; ?>
									</div>
								</div>
							</div>  <!-- .b-prodProp -->
	                        
							<form action="/basket/" method="POST" id="OrderForm">
								<input type="hidden" name="id" value="<?=$good['id']?>" />
								<input type="hidden" name="action" value="basket_add" />
		                        <p class="sel-t-variants">Варианты цвета</p>
                              	<?=$good['_upholstery_html'];?>
                              	
                              	
                                <?=$good['_arms_html'];?>
                    			<? //if ($good['_arms']['type_id']==3) echo "BINGO";?>
                    			<p><br /><br /></p>
                                
                                <p class="sel-t-variants">Добавить в корзину</p>
                				<div class="incart">
                					<div class="zena-incart"><span id="Price"><?=$good['_default_price']?></span> руб.</div>
		                            <div class="form-amount"><input type="text" name="count" id="productCount" value="1" size="5" /> шт.</div>
		                            									
									<div class="incart-button">
										<a href="javascript:void(0);" class="ajax_submit_button"></a>
									</div>
									<span id="Notice"></span>
									<img src="/img/incartb2.gif" width="12" height="86" alt="" class="im-right" />
								</div><!-- .incart -->
			                    <p><br /><br /></p>
			                    
							</form>
							<p style="margin-left: 10px;"><i>Возврат и обмен без проблем!</i></p>
                    	</div><!-- .rightVizText -->
                        
	                    <div class="leftVisImg">
	                  <div style="height: 380px;">
	                    		<div class="lightBoxImg">
	                    		<?php if ($good['_photos']): foreach($good['_photos'] as $key => $photo): ?>
                                  <a href="<?=$photo['image_orig']?>" title="<?=$good['title']?>">
                                  	<div class="zoom img<?=$photo['id']?>" <?php if($key > 0): ?>style="display:none;"<?php endif; ?>></div>
                                  	<img  title="<?=$good['title']?>" alt="<?=$good['title']?>" class="bigImage" id="img<?=$photo['id']?>" <?php if($key > 0): ?>style="display:none;"<?php endif; ?> src="<?=$photo['image']?>" />
                                  </a>
								<?php endforeach; endif; ?> 
								
							
								
								
	                    		</div>
	                    	</div>
	                        <ul class="selectPik">
	                        	<?php if ($good['_photos']): foreach($good['_photos'] as $key => $photo): ?>
                                  <li id="thumb<?=$photo['id']?>" <?php if($key == 0): ?>class="selekt"<?php endif; ?>><img class="click-to-select-img" alt="" src="<?=$photo['thumb']?>" data-photo-id="<?=$photo['id']?>"/></li>
								<?php endforeach; endif; ?>
	                        </ul>
	                        <?php if($good['_services']): ?>                        
	                        <div class="servicesInfo">								
								<?php foreach($good['_services'] as $service): ?>
		                        <div class="servicesInfoItem">
		                        	<div class="itemContent">
			                        	<span><img alt="" src="<?=$service['image']?>" /></span>
			                            <!--i>Расширенная гарантия<br />  на любую продукцию<br />  нашей компании</i-->
										<i><?=$service['description']?></i>
			                            <div class="b-description">

			                            	<?=$service['description_detailed']?>
											<br>
											<br>
			                            	<?=$service['anchor']?>
			                            </div>
		                            </div>
		                        </div>
								<?php endforeach; ?>
							</div><!-- .servicesInfo -->
							<?php endif; ?>
		                      
	                        <div class="b-freeGift">
	                        	<img src="/newImg/freeGift.png" alt="">
	                        	<div class="b-description">
	                        		<p class="b-description-header">Подарок с каждой покупкой!</p>
									<p>Добавьте после покупки отзыв о кресле на <a href="http://market.yandex.ru/shop/52862/reviews">Яндекс.Маркет</a> и получите гарантированный ПОДАРОК от нашей компании. Подробности уточняйте у менеджеров. Приятных Вам покупок.</p>
	                        	</div>
	                        </div><!-- .b-freeGift -->
	                    </div><!-- .leftVisImg -->
                    	<div class="clear"></div>
                	</div><!-- .oneElementCatalog -->
    				<div class="clear"></div>	
    			</div><!-- .blokCatalog -->

      			<div class="clear"></div>	
      			<div class="CatalogTopBord">
      			
      				<!-- недавно просмотренные -->
      				<?=$good['_recently_viewed_html']?>
      				

					<div class="b-thisType">
					<?if($good['_same_goods_html']){?>
        				<p class="b-thisType-header">Похожие товары</p>
        				<?echo $good['_same_goods_html']; }?>
        			</div><!-- .b-thisType -->

    			</div><!-- .CatalogTopBord -->
<script>
function AddItem() {
	var data = $('#OrderForm').serialize();
	basket.push(data);
}
  $(function () {
    catalog = new Catalog();

	catalog.bindUpholsterySelectEvents();
	
	comments = new Comments(document.getElementById("GoodCommentsForm"));	
	comments.ajaxURL = '/catalog/ajax/';
	comments.objectID = <?=$good['id']?>;
	comments.setPagination(<?=$good['_comments_onpage']?>, <?=(int)$good['_comments_count']?>);

	basket = new Basket(document.getElementById("OrderForm"));
  });
</script>

<link rel="stylesheet" type="text/css" href="/css/magnific-popup.css" media="screen">

<script src="/js/jquery.jcarousel.min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
                    
                var vis_items = 4;

                if (width >= 700) {
                    width = width / 4;
                    vis_items = 4;
                } else if (width >= 350) {
                    width = width / 3;
                    vis_items = 3;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                
                var total_items = carousel.find('li').length;

				if(total_items <= vis_items)
				{                
				    carousel.siblings('.jcarousel-control-prev, .jcarousel-control-next').hide();
				}
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
            
            
        $('.lightBoxImg').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				tCounter:'<span class="mfp-counter">%curr% из %total%</span>',
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			}
		});
            
	});
</script>	
