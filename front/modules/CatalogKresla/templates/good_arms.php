<div class="fabricBlock">
	<div class="title">Декор</div>
	<div class="materialBlock">
		<div class="select-block-mat">
			<ul class="sel-tabs">
				{{arms}}
					{{if type_id == 3}}
					<li class="selectable {{if _selected == 1}}selected{{endif}}">
						<input type="radio" {{if _selected == 1}}checked="checked"{{endif}} name="arm_id" id="sss{{id}}" value="{{id}}" >
						<label for="sss{{id}}">
							<img height="54" width="54" class="ss" src="/img/_ss4{{if on_order}}_onorder{{endif}}.png">
							<img height="54" width="54" class="ss {{if _selected == 1}}hidden{{endif}}" src="/img/_ss4{{if on_order}}_onorder{{endif}}.png">
							<img src="{{image}}" width="54" height="54" alt="{{title}}" title="{{title}}">
							<div title="title" class="colorName">{{title}}</div>
						</label>
					</li>
					{{endif}}
				{{/arms}}
			</ul>
		</div><!-- .select-block-mat -->	
	</div>
</div> <!-- .fabricBlock -->
