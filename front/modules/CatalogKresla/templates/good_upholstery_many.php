<div class="materialBlock" data-price="{{_price}}">
	<div class="b-priceBlock">
		{{if _price_old}}
		<!-- У нас скидка! -->
			<span class="materialOldPrice">{{_price_old}} руб.</span>
			<span class="materialPrice">{{_price}} руб.</span>
			<span class="materialName">{{_title}} </span>
			<div class="b-priceBlock-descr">Акция! Купив сейчас, вы сэкономите {{_price_diff}} руб.!</div>
		{{else}}
			<!-- Цена стандартная -->
			<span class="materialPrice">{{_price}} руб.</span>
			<span class="materialName">{{_title}} </span>
		{{endif}}
	</div><!-- .b-priceBlock -->
	
	<div class="select-block-mat">
	
		<ul class="sel-tabs">
			{{_rows}}
				<li class="selectable {{if _selected == 1}}selected{{endif}}" data-color="{{id}}">
					<input type="radio" {{if _selected == 1}}checked="checked"{{endif}} name="upholstery_id" id="s{{id}}" value="{{id}}" rel="{{price}}">
					<label for="s{{id}}">
						{{if type_id !=7 }}
							<img height="54" width="54" class="ss" alt="" src="/img/_ss2{{if on_order}}_onorder{{endif}}.png">
							<img height="54" width="54" alt="" class="ss" src="/img/_ss2{{if on_order}}_onorder{{endif}}.png">
						{{endif}}	
						<img src="{{image}}" width="54" height="54" alt="{{title}}" title="{{title}}">
						<div title="title" class="colorName">{{title}}</div>
					</label>
				</li>
			{{/_rows}}
		</ul>
	</div><!-- .select-block-mat -->
	
	<div class="clear"></div>
	
</div>


                 				
                 	