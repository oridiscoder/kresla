<?php
namespace front\modules;

use front\models\Orm;

use front\modules\CatalogKresla\models\Colors;

use front\modules\CatalogKresla\models\Cross;

use front\modules\CatalogKresla\models\Arm;

use front\modules\CatalogKresla\models\Color;

use front\modules\CatalogKresla\models\UpholsteryType;

use front\modules\CatalogKresla\models\CookieStorage;

use front\modules\CatalogKresla\models\Label;

use front\modules\CatalogKresla\models\Table;
use front\modules\CatalogKresla\models\TableFilter;

use front\modules\CatalogKresla\models\Upholstery;

use front\modules\CatalogKresla\models\GoodUrlBuilder;

use front\modules\CatalogKresla\models\Good;

use front\modules\CatalogKresla\models\Category;

use \front\modules\CatalogKresla\models\TableNames;

require_once "models/Good.php";
require_once "models/TableFilter.php";
require_once "models/Category.php";

class CatalogKresla extends Catalog implements \front\modules\Paginable {
	
	/**
	 * Иерархичный список категорий каталога
	 * @var array
	 */
	private $categories;
	/**
	 * 
	 * @var TableNames
	 */
	private $tableNamesContainer;
	/**
	 * 
	 * @var \front\modules\Comments
	 */
	private $commentsModule;
	
	private $goods;
	private $goodsTotal;
	private $goodsOnpage;

	public $tableFilterArray;
	public $tableFilterCheck;
	public $tableFilterUrls;
	public $tableBaseUri;
	public $tableFilterCurrent;
	public $tableSeoItem;
	
	public $gids;
	public $minPrice;
	public $maxPrice;
	

	
	public function setOnpage($count) {
		$this->goodsOnpage = $count;
	}
	public function getOnPage() {
		return $this->goodsOnpage;
	}
	public function setTotal($count) {
		$this->goodsTotal = $count;
	}
	public function getTotal() {
		return $this->goodsTotal;
	}
	public function setGoods($goods) {
		$this->goods = $goods;
	}
	public function getGoods() {
		return $this->goods;
	}
	
	public function getDescription() {
		return "Модуль каталога сайта kresla-otido.ru";
	}
	/**
	 * @return \front\modules\CatalogKresla\models\TableNames
	 */
	public function getTableNamesContainer() {
		if($this->tableNamesContainer == null) {
			$this->tableNamesContainer = new TableNames();
		}
		return $this->tableNamesContainer;
	}
	/* основные таблицы */
	public function getCategoriesTableName() {
		return $this->getTableNamesContainer()->getCategoriesTableName();
	}
	public function getLabelsTableName() {
		return $this->getTableNamesContainer()->getLabelsTableName();
	}
	public function getUpholsteryTableName() {
		return $this->getTableNamesContainer()->getUpholsteryTableName();
	}
	public function getUpholsteryTypesTableName() {
		return $this->getTableNamesContainer()->getUpholsteryTypesTableName();
	}
	public function getArmTypesTableName() {
		return $this->getTableNamesContainer()->getArmTypesTableName();
	}
	public function getArmsTableName() {
		return $this->getTableNamesContainer()->getArmsTableName();
	}
	public function getCertificatesTableName() {
		return $this->getTableNamesContainer()->getCertificatesTableName();
	}
	public function getCrossTypesTableName() {
		return $this->getTableNamesContainer()->getCrossTypesTableName();
	}
	public function getCrossTableName() {
		return $this->getTableNamesContainer()->getCrossTableName();
	}
	public function getVendorsTableName() {
		return $this->getTableNamesContainer()->getVendorsTableName();
	}
	public function getServicesTableName() {
		return $this->getTableNamesContainer()->getServicesTableName();
	}
	public function getColorsTableName() {
		return $this->getTableNamesContainer()->getColorsTableName();
	}
	/* товары и связывающие их таблицы */
	public function getGoodsTableName() {
		return $this->getTableNamesContainer()->getGoodsTableName();
	}
	public function getGoodImagesTableName() {
		return $this->getTableNamesContainer()->getGoodImagesTableName();
	}
	public function getGoodCategoriesTableName() {
		return $this->getTableNamesContainer()->getGoodCategoriesTableName();
	}
	public function getGoodUpholsteryTableName() {
		return $this->getTableNamesContainer()->getGoodUpholsteryTableName();
	}
	public function getGoodCrossTableName() {
		return $this->getTableNamesContainer()->getGoodCrossTableName();
	}
	public function getGoodArmsTableName() {
		return $this->getTableNamesContainer()->getGoodArmsTableName();
	}
	public function getGoodServicesTableName() {
		return $this->getTableNamesContainer()->getGoodServicesTableName();
	}
	public function getGoodCertificatesTableName() {
		return $this->getTableNamesContainer()->getGoodCertificatesTableName();
	}
	
	public function getPopularGoodsTableName() {
		return $this->getTableNamesContainer()->getPopularGoodsTableName();
	}
	public function getNewGoodsTableName() {
		return $this->getTableNamesContainer()->getNewGoodsTableName();
	}
	
	public function getCategoryAttendantGoodsTableName() {
		return $this->getTableNamesContainer()->getCategoryAttendantGoodsTableName();
	}
	
	public function install() {
		
		/* ----------- Категории ----------- */
		$categoriesTableName = $this->getCategoriesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$categoriesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0, 
			`number` INT UNSIGNED DEFAULT 0,			
			`title` VARCHAR(255), 
			`description` VARCHAR(500), 
			`keywords` VARCHAR(500),
			`url` VARCHAR(255),  
			`parent_id` INT UNSIGNED DEFAULT NULL, 
			`ext_url` VARCHAR(100) DEFAULT NULL, 
			`image` VARCHAR(100), 
			`titles` VARCHAR(100)
		)";
		$this->getDb()->query($sql);
		
		$sql = "TRUNCATE TABLE `$categoriesTableName`";
		$this->getDb()->query($sql);
        
        $sql = "INSERT INTO `$categoriesTableName` (`id`, `hidden`, `number`, `title`, `titles`, `description`, `keywords`, `url`, `parent_id`, `ext_url`, `image`) VALUES
		(1, 0, 0, 'В офис', '',  '', '', 'office', 0, '', NULL),
		(2, 0, 1, 'Офисные кресла', 'кресло,кресла,кресел', '', '', 'office_armchairs', 1, '', NULL),
		(3, 0, 2, 'Конференц-кресла', 'кресло,кресла,кресел', '', '', 'conference_chairs', 1, '', NULL),
		(4, 0, 3, 'Кресла для персонала', 'кресло,кресла,кресел', '', '', 'stuff_chairs', 1, '', NULL),
		(5, 0, 4, 'Кресла для руководителей', 'кресло,кресла,кресел', '', '', 'chief_chairs', 1, '', NULL),
		(6, 0, 5, 'Офисные стулья', 'стул,стула,стульев', '', '', 'office_stool', 1, '', NULL),
		(7, 0, 6, 'Домой', '', '', '', 'home', 0, '', NULL),
		(8, 0, 7, 'Компьютерные кресла и стулья', 'кресло,кресла,кресел', '', '', 'armchairs', 7, '', NULL),
		(9, 0, 8, 'Детские кресла', 'кресло,кресла,кресел', '', '', 'children', 7, '', NULL),
		(10, 0, 9, 'Компьютерные столы ', 'стол,стола,столов', '', '', 'tables', 7, '', NULL),
		(11, 0, 10, 'Прочее', 'товар,товара,товаров', '', '', 'other', 0, '', NULL),
		(12, 0, 11, 'Театральные кресла', 'кресло,кресла,кресел', '', '', 'theatre', 11, '', NULL),
		(13, 0, 12, 'Аксессуары', 'аксессуар,аксесуара,аксессуаров', '', '', 'accessories', 11, '', NULL)";
        $this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($categoriesTableName, "Категории")
				->addStringField("title", "Название")
				->addStringField("keywords", "Keywords")
				->addStringField("description", "Description")
				->addStringField("url", "URL")
				->addDescendantField("parent_id", "Родительская категория", $factory->getTableId())
				->addStringField("ext_url", "Внешний URL")
				->addImageField("image", "Картинка")
				->addStringField("titles", "Названия", array("hint" => "Для числительных.Пример:<b>стул,стула,стульев</b>"));
		$categoriesTableID = $factory->getTableId();
		
		/* ----------- Ярлыки ----------- */
		$labelsTableName = $this->getLabelsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$labelsTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(255),
			`class_name` VARCHAR(20)
		)";
		$this->getDb()->query($sql);
		$factory = $this->getTableFactory();
		$factory->addTable($labelsTableName, "Ярлыки")
				->addStringField("title", "Название")
				->addStringField("class_name", "CSS класс");
		$labelsTableID = $factory->getTableId();
		
		/* Типы обивок */
		$upholsteryTypesTableName = $this->getUpholsteryTypesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$upholsteryTypesTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(100),
			`code` VARCHAR(100)
		)";
		$this->getDb()->query($sql);
		$this->getDb()->query("TRUNCATE TABLE `$upholsteryTypesTableName`");
		$sql = "
		INSERT INTO `$upholsteryTypesTableName` (`title`, `code`, `number`) VALUES 
		('Натуральная кожа', 'mat-icon1', 1),
		('Экокожа', 'mat-icon2', 2),
		('Кожзаменитель', 'mat-icon3', 3),
		('Сетка', 'mat-icon4', 4),
		('Ткань', 'mat-icon5', 5),
		('Полиэстр', 'mat-icon6', 6),
		('Прочие', 'mat-icon7', 7)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($upholsteryTypesTableName, "Типы обивок")
			->addStringField("title", "Название")
			->addStringField("code", "Код");
		$upholsteryTypeTableID = $factory->getTableId();
		
		/* ----------- Цвета обивок ----------- */
		$colorsTablename = $this->getColorsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$colorsTablename`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(100),
			`class_name` VARCHAR(20)
		)";
		$this->getDb()->query($sql);
		
		$sql = "TRUNCATE $colorsTablename";
		$this->getDb()->query($sql);
		$sql = "INSERT INTO $colorsTablename (`title`, `class_name`, `number`) VALUES 
		('Чёрный', 		'color-icon1', 1),
		('Серый', 		'color-icon2', 2),
		('Коричневый',	'color-icon3', 3),
		('Бежевый',		'color-icon4', 4),
		('Синий',		'color-icon5', 5),
		('Зелёный',		'color-icon6', 6),
		('Красный',		'color-icon7', 7),
		('Оранжевый',	'color-icon8', 8),
		('Разноцветный','color-icon9', 9),
		('Маска',		'color-icon10', 10);";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($colorsTablename, "Цвета")
			->addStringField("title", "Название")
			->addStringField("class_name", "CSS класс");
		$colorsTableID = $factory->getTableId();
		
		
		/* ----------- Обивки ----------- */
		$upholsteryTableName = $this->getUpholsteryTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$upholsteryTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(100),
			`image` VARCHAR(127),
			`color_id` INT UNSIGNED DEFAULT NULL,
			`parent_id` INT UNSIGNED DEFAULT NULL,
			`type_id` INT UNSIGNED DEFAULT NULL,  
			`artikul` VARCHAR(50)
		)";
		$this->getDb()->query($sql);
		$factory = $this->getTableFactory();
		$factory->addTable($upholsteryTableName, "Обивки")
				->addStringField("title", "Название")
				->addImageField("image", "Картинка")
				->addStringField("artikul", "Артикул")
				->addDescendantField("parent_id", "Родительская обивка", $factory->getTableId())
				->addDescendantField("type_id", "Тип", $upholsteryTypeTableID)
				->addDescendantField("color_id", "Цвет", $colorsTableID);
						
		$upholsteryTableID = $factory->getTableId();
		
		/* ----------- Материал подлокотников ----------- */
		$armTypesTableName = $this->getArmTypesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$armTypesTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(50)
		)";
		$this->getDb()->query($sql);
		$this->getDb()->query("TRUNCATE TABLE $armTypesTableName");
		$this->getDb()->query("INSERT INTO  $armTypesTableName (number, title) VALUES 
			(1, 'Металлический'), 
			(2, 'Пластиковый'),
			(3, 'Деревяный') 
		");
		
		$factory = $this->getTableFactory();
		$factory->addTable($armTypesTableName, "Материалы подлокотников")
			->addStringField("title", "Название");
		$armTypesTableID = $factory->getTableId();
		
		
		/* ----------- Подлокотники ----------- */
		$armsTableName = $this->getArmsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$armsTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(50),
			`image` VARCHAR(127),
			`artikul` VARCHAR(50), 
			`type_id` INT UNSIGNED NOT NULL
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($armsTableName, "Подлокотники")
			->addStringField("title", "Название")
			->addImageField("image", "Картинка")
			->addStringField("artikul", "Артикул")
			->addDescendantField("type_id", "Материал", $armTypesTableID);
		$armsTableID = $factory->getTableId();
		
		/* ----------- Типы крестовин ----------- */
		$crossTypesTableName = $this->getCrossTypesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$crossTypesTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(50)
		)";
		$this->getDb()->query($sql);
		$this->getDb()->query("TRUNCATE TABLE $crossTypesTableName");
		$this->getDb()->query("INSERT INTO  $crossTypesTableName (number, title) VALUES
			(1, 'Пластик'),
			(2, 'Металл'),
			(3, 'Дерево')
		");
		
		$factory = $this->getTableFactory();
		$factory->addTable($crossTypesTableName, "Материалы")
			->addStringField("title", "Название");
		$crossTypesTableID = $factory->getTableId();
		
		/* ----------- Крестовины ----------- */
		$crossTableName = $this->getCrossTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$crossTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(50),
			`image` VARCHAR(127),
			`artikul` VARCHAR(50),
			`type_id` INT UNSIGNED NOT NULL
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($crossTableName, "Крестовины")
				->addStringField("title", "Название")
				->addImageField("image", "Картинка")
				->addStringField("artikul", "Артикул")
				->addDescendantField("type_id", "Материал", $crossTypesTableID);
		$crossTableID = $factory->getTableId();
		
		/* ----------- Производители ----------- */
		$vendorsTableName = $this->getVendorsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$vendorsTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`name` VARCHAR(50)
		)";
		$this->getDb()->query($sql);
		$factory = $this->getTableFactory();
		$factory->addTable($vendorsTableName, "Крестовины")
				->addStringField("name", "Название");
		$vendorsTableID = $factory->getTableId();
		
		/* ----------- Дополнительные услуги ----------- */
		$servicesTableName = $this->getServicesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$servicesTableName`(
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(255),
			`description` VARCHAR(500),
			`description_detailed` VARCHAR(500),
			`image` VARCHAR(255)
		)";
		$this->getDb()->query($sql);
		$factory = $this->getTableFactory();
		$factory->addTable($servicesTableName, "Услуги")
			->addStringField("title", "Название")
			->addTextField("description", "Краткое описание")
			->addTextField("description_detailed", "Детальное описание")
			->addImageField("image", "Картинка");
		$servicesTableID = $factory->getTableId();
		
		/* Сертификаты */
		$certificatesTableName = $this->getCertificatesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$certificatesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(100),
			`image` VARCHAR(100)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($certificatesTableName, "Сертификаты")
			->addStringField("title", "Название")
			->addImageField("image", "Картинка", array("draw_into" => '100x142'));
		$certificatesTableID = $factory->getTableId();
		
		/* ----------- Товары ----------- */
		$goodsTableName = $this->getGoodsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodsTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`title` VARCHAR(255),
			`description` VARCHAR(500),
			`keywords` VARCHAR(500),
			`url` VARCHAR(255),
			`text` TEXT,
			`price` DOUBLE DEFAULT NULL, 
			`thumb` 			VARCHAR(255) DEFAULT NULL,
			`thumb_large` 		VARCHAR(255) DEFAULT NULL,
			`pricecategory_id` 	INT UNSIGNED DEFAULT NULL,			
			`label_id` 			INT UNSIGNED DEFAULT NULL,
			`to_order` 			TINYINT(1) DEFAULT 0,
			`in_ya_market` TINYINT(1) DEFAULT 1,
			`vendor_code` 		VARCHAR(255), 
			`vendor_id` 		INT UNSIGNED DEFAULT NULL,
			`sales_notes`		VARCHAR(1000),
			`manufacturer_warranty`		TINYINT(1) DEFAULT 0,
			`country_of_origin`			VARCHAR(255),
			`ga_experiment_code`		TEXT,
			`height_min`				DOUBLE DEFAULT NULL,
			`height_max`				DOUBLE DEFAULT NULL,
			`width`						DOUBLE DEFAULT NULL,
			`deep`						DOUBLE DEFAULT NULL,
			`weight_limit`				INT UNSIGNED DEFAULT NULL,
			`back_adjust`				TINYINT(1) DEFAULT 0, 
			`back_height`				INT UNSIGNED DEFAULT NULL, 
			`inner_deep`				INT DEFAULT NULL
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($goodsTableName, "Кресла")
			->addStringField("title", "Название")
			->addStringField("keywords", "Keywords")
			->addStringField("description", "Description")
			->addStringField("url", "URL")
			->addTextField("text", "Описание")
			->addStringField("price", "Цена")
			->addImageField("thumb", "Превью")
			->addImageField("thumb_large", "Превью большое")
			->addDescendantField("label_id", "Ярлык", $labelsTableID)
			->addBooleanField("to_order", "На заказ")
			->addBooleanField("in_ya_market", "Я.Маркет")
			->addStringField("vendor_code", "VendorCode") 
			->addDescendantField("vendor_id", "Производитель", $vendorsTableID)
			->addStringField("sales_notes", "Sales notes")
			->addBooleanField("manufacturer_warranty", "Гарантия производителя")
			->addStringField("country_of_origin", "Страна")
			->addTextField("ga_experiment_code", "Код Гугл-Эксперимента")
			->addStringField("height_min", "Высота мин.", array("hint" => "Высота в см."))
			->addStringField("height_max", "Высота макс.", array("hint" => "Высота в см."))
			->addStringField("width", "Ширина", array("hint" => "Ширина в см."))
			->addStringField("deep", "Глубина", array("hint" => "Глубина в см."))
			->addStringField("weight_limit", "Максимальная масса")
			->addBooleanField("back_adjust", "Регулируемая спинка")
			->addIntegerField("inner_deep", "Внутренняя глубина")
			->addIntegerField("back_height", "Высота спинки");
		
		$goodsTableID = $factory->getTableId();
		
		/* ----------- Картинки товаров ----------- */
		$goodImagesTableName = $this->getGoodImagesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodImagesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0, 
			`good_id` INT UNSIGNED NOT NULL, 
			`thumb` VARCHAR(100), 
			`image` VARCHAR(100)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory();
		$factory->addTable($goodImagesTableName, "Картинки")
				->addDescendantField("good_id", "Кресло",  $goodsTableID)
				->addImageField("thumb", "Превью", array("draw_into" => '60x82'))
				->addImageField("image", "Картинка", array("draw_into" => '290x400'));
		$goodImagesTableID = $factory->getTableId();
		
		/* ----------- Категории товаров ----------- */
		$goodCategoriesTableName = $this->getGoodCategoriesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodCategoriesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`good_id` INT UNSIGNED NOT NULL,
			`category_id` INT UNSIGNED NOT NULL,
			`primary` TINYINT(1) DEFAULT 0,
			KEY(`good_id`), 
			KEY(`category_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($goodCategoriesTableName, "Категории товаров")
			->addDescendantField("good_id", "Кресло", $goodsTableID)
			->addDescendantField("category_id", "Категория", $categoriesTableID)
			->addBooleanField("primary", "Основная");
		$goodCategoriesTableID = $factory->getTableId();
		
		/* ----------- Услуги товаров ----------- */
		$goodServicesTableName = $this->getGoodServicesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodServicesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`good_id` INT UNSIGNED NOT NULL,
			`service_id`INT UNSIGNED NOT NULL,
			KEY(`good_id`), 
			KEY(`service_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($goodServicesTableName, "Услуги")
			->addDescendantField("good_id", "Кресло",  $goodsTableID)
			->addDescendantField("service_id", "Услуга", $servicesTableID);
		$goodServicesTableID = $factory->getTableId();
		
		/* ----------- Крестовины товаров ----------- */
		$goodCrossesTableName = $this->getGoodCrossTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodCrossesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`good_id` INT UNSIGNED NOT NULL,
			`cross_id` INT UNSIGNED NOT NULL,
			KEY(`good_id`),
			KEY(`cross_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($goodCrossesTableName, "Крестовины")
			->addDescendantField("good_id", "Кресло",  $goodsTableID)
			->addDescendantField("cross_id", "Крестовина", $crossTableID);
		$goodCrossesTableID = $factory->getTableId();
		
		/* ----------- Подлокотники товаров ----------- */
		$goodArmsTableName = $this->getGoodArmsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodArmsTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`good_id` INT UNSIGNED NOT NULL,
			`arms_id` INT UNSIGNED NOT NULL,
			KEY(`good_id`),
			KEY(`arms_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
				->addTable($goodArmsTableName, "Подлокотники")
				->addDescendantField("good_id", "Кресло",  $goodsTableID)
				->addDescendantField("arms_id", "Подлокотник", $armsTableID);
		$goodArmsTableID = $factory->getTableId();
		
		/* ----------- Обивки товаров ----------- */
		$goodUpholsteriesTableName = $this->getGoodUpholsteryTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodUpholsteriesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`good_id` INT UNSIGNED NOT NULL,
			`upholstery_id`INT UNSIGNED NOT NULL,
			`price` DOUBLE DEFAULT NULL,
			KEY(`good_id`),
			KEY(`upholstery_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($goodUpholsteriesTableName, "Обивки")
			->addDescendantField("good_id", "Кресло",  $goodsTableID)
			->addDescendantField("upholstery_id", "Обивка", $upholsteryTableID)
			->addStringField("price", "Цена");
		$goodUpholsteriesTableID = $factory->getTableId();
		
		
		/* ----------- Сертификаты товаров ----------- */
		$goodCertificatesTableName = $this->getGoodCertificatesTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$goodCertificatesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`good_id` INT UNSIGNED NOT NULL,
			`certificate_id` INT UNSIGNED NOT NULL,
			KEY(`good_id`),
			KEY(`certificate_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
				->addTable($goodCertificatesTableName, "Сертификаты")
				->addDescendantField("good_id", "Кресло",  $goodsTableID)
				->addDescendantField("certificate_id", "Сертификат",  $certificatesTableID);
		$goodCertificatesTableID = $factory->getTableId();
		
		/* ----------- Популярные товары ----------- */
		$popularGoodsTableName = $this->getPopularGoodsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$popularGoodsTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`number` INT UNSIGNED DEFAULT 0,
			`good_id` INT UNSIGNED NOT NULL,
			KEY(`good_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($popularGoodsTableName, "Популярные товары")
			->addDescendantField("good_id", "Кресло",  $goodsTableID);
		$popularGoodsTableID = $factory->getTableId();
		
		
		/* ----------- Сопутсвующие товары ----------- */
		$attendantGoodsTableName = $this->getCategoryAttendantGoodsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$attendantGoodsTableName` (
				  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `number` int(11) NOT NULL DEFAULT '0',
				  `hidden` tinyint(1) NOT NULL DEFAULT '0',
				  `category_id` int(11) DEFAULT NULL,
				  `good_id` int(11) DEFAULT NULL,
				  KEY (`category_id`),
				  KEY (`good_id`) 
				);";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($attendantGoodsTableName, "Сопутсвующие товары", array("param2" => "NOTNULL"))
			->addDescendantField("category_id", "Категория",  $categoriesTableID, array("filter" => "parent_id IS NOT NULL AND parent_id > 0", "param2" => "NOTNULL"))
			->addDescendantField("good_id", "Кресло",  $goodsTableID);
		$attendantGoodsTableID = $factory->getTableId();
		
		
		/* ----------- Новые товары (новинки)----------- */
		$newGoodsTableName = $this->getNewGoodsTableName();
		$sql = "CREATE TABLE IF NOT EXISTS `$newGoodsTableName` (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
		`number` INT UNSIGNED DEFAULT 0,
		`good_id` INT UNSIGNED NOT NULL,
		KEY(`good_id`)
		)";
		$this->getDb()->query($sql);
		
		$factory = $this->getTableFactory()
			->addTable($newGoodsTableName, "Новинки")
			->addDescendantField("good_id", "Кресло",  $goodsTableID);
		$newGoodsTableID = $factory->getTableId();
		
		/* добавляем страницы в CMS */
		$pFactory = $this->getPageFactory();
		$categoriesPageID = $pFactory->addPage("Категории", \PageCreator::SECTION_SITE, $categoriesTableID);		
		$labelsPageID = $pFactory->addPage("Ярлыки", \PageCreator::SECTION_SITE, $labelsTableID);
		$upholsteryPageID = $pFactory->addPage("Обивки", \PageCreator::SECTION_SITE, $upholsteryTableID);
		$upholsteryTypesPageID = $pFactory->addPage("Типы обивок", \PageCreator::SECTION_SITE, $upholsteryTypeTableID, null, $upholsteryPageID);
		$armsPageID = $pFactory->addPage("Подлокотники", \PageCreator::SECTION_SITE, $armsTableID);
		$armTypesPageID = $pFactory->addPage("Материалы", \PageCreator::SECTION_SITE, $armTypesTableID, null, $armsPageID);
		$crossPageID = $pFactory->addPage("Крестовины", \PageCreator::SECTION_SITE, $crossTableID);
		$crossTypesPageID = $pFactory->addPage("Материалы", \PageCreator::SECTION_SITE, $crossTypesTableID, null, $crossPageID);
		$vendorsPageID = $pFactory->addPage("Производители", \PageCreator::SECTION_SITE, $vendorsTableID);
		$servicesPageID = $pFactory->addPage("Услуги", \PageCreator::SECTION_SITE, $servicesTableID);
		$certificatesPageID = $pFactory->addPage("Сертификаты", \PageCreator::SECTION_SITE, $certificatesTableID);
		$colorsPageID = $pFactory->addPage("Цвета", \PageCreator::SECTION_SITE, $colorsTableID);
		
		$pFactory->addPage("Сопутсвующие товары",\PageCreator::SECTION_SITE, $attendantGoodsTableID, null, $categoriesTableID);
		
		$goodsPageID = $pFactory->addPage("Товары", \PageCreator::SECTION_SITE, $goodsTableID);
		$goodCategoriesPageID = $pFactory->addPage("Категории", \PageCreator::SECTION_SITE, $goodCategoriesTableID, null, $goodsPageID);
		$goodImagesPageID = $pFactory->addPage("Картинки", \PageCreator::SECTION_SITE, $goodImagesTableID, null, $goodsPageID);
		$goodUpholsteriesPageID = $pFactory->addPage("Обивки", \PageCreator::SECTION_SITE, $goodUpholsteriesTableID, null, $goodsPageID);
		$goodArmsPageID = $pFactory->addPage("Подлокотники", \PageCreator::SECTION_SITE, $goodArmsTableID, null, $goodsPageID);
		$goodCrossesPageID = $pFactory->addPage("Крестовины", \PageCreator::SECTION_SITE, $goodCrossesTableID, null, $goodsPageID);
		$goodCertificatesPageID = $pFactory->addPage("Сертификаты", \PageCreator::SECTION_SITE, $goodCertificatesTableID, null, $goodsPageID);
		$goodServicesPageID = $pFactory->addPage("Услуги", \PageCreator::SECTION_SITE, $goodServicesTableID, null, $goodsPageID);
		
		$pFactory->addPage("Популярные товары", \PageCreator::SECTION_SITE, $popularGoodsTableID);
		$pFactory->addPage("Новинки", \PageCreator::SECTION_SITE, $newGoodsTableID);
		
		return true;
	}
	public function remove() {
		$tables = array();
		$tables[] = $this->getCategoriesTableName();
		$tables[] = $this->getLabelsTableName();
		$tables[] = $this->getUpholsteryTableName();
		$tables[] = $this->getArmsTableName();
		$tables[] = $this->getCrossTableName();
		$tables[] = $this->getVendorsTableName();
		$tables[] = $this->getGoodsTableName();
		$tables[] = $this->getGoodImagesTableName();
		$tables[] = $this->getGoodCategoriesTableName();
		$tables[] = $this->getGoodUpholsteryTableName();
		$tables[] = $this->getGoodCrossTableName();
		$tables[] = $this->getGoodArmsTableName();
		$tables[] = $this->getCertificatesTableName();
		$tables[] = $this->getGoodCertificatesTableName();
		$tables[] = $this->getServicesTableName();
		$tables[] = $this->getGoodServicesTableName();
		$tables[] = $this->getUpholsteryTypesTableName();
		$tables[] = $this->getColorsTableName();
		$tables[] = $this->getPopularGoodsTableName();
		$tables[] = $this->getNewGoodsTableName();
		$tables[] = $this->getCrossTypesTableName();
		$tables[] = $this->getArmTypesTableName();
		
		foreach($tables as $tableName) {
			$this->removeTable($tableName);
		}
		
		$templateDir = dirname(__FILE__) . DIRECTORY_SEPARATOR .'templates';
		$templates = glob($templateDir . DIRECTORY_SEPARATOR . '*.html');
		foreach ($templates as $templateFilename) {
			$this->deleteTemplate(basename($templateFilename));
		}
		
		return true;
	}
	
	public function getConfigFields() {
		return array(
			"comments_onpage" => array(
				"title" => "Кол-во комментариев на странице", 
				"default" => 4, 
				"type" => "integer"
			), 
			"recently_viewed_limit" => array(
				"title" => 'Количество элементов в Недавно просмотренных', 
				"default" => 4, 
				"type" => "integer"
			), 
			"recently_viewed_top_limit" => array(
					"title" => 'Макс.количество элементов в Недавно просмотренных',
					"default" => 16,
					"type" => "integer"
			),
			"same_goods_limit" => array(
				"title" => 'Количество Похожих товаров',
				"default" => 8,
				"type" => "integer"
			), 
			"popular_goods_limit" => array(
				"title" => 'Количество популярных на главной',
				"default" => 12,
				"type" => "integer"
			), 
			"new_goods_limit" => array(
				"title" => 'Количество новинок на главной',
				"default" => 12,
				"type" => "integer"
			),
			"market_delivery_cost" => array(
				"title" => 'Стоимость доставки (Я.Маркет)',
				"default" => 500,
				"type" => "integer"
			), 
			"onpage" => array(
				"title" => 'Товаров на странице',
				"default" => 60,
				"type" => "integer"
			) 
		);
	}

	public function getTitle() {
		return "Каталог kresla-otido.ru";
	}
	
	public function getCategories() {
		return Category::getTree();
	}
	
	public function popularList($options, $content) {
		//return $this->getDi()->get("templates")->find($options['name']);
	}
	public function filters($options, $content) {
        

	
		
		$template = $this->getTemplate("filters.html", true);
		if(!$this->tableFilterCheck){
			$request = $this->getRequest();
			
			$price =	$this->getSliderValues("price", Good::getMinPrice(), Good::getMaxPrice());
			$height =	$this->getSliderValues("height", Good::getMinHeight(), Good::getMaxHeight());
			$deep =		$this->getSliderValues("deep", Good::getMinDeep(), Good::getMaxDeep());
			$width =	$this->getSliderValues("width", Good::getMinWidth(), Good::getMaxWidth());
			

			$categories =	$this->markAsSelected(Category::getList(), $request->getParam("categories"));
			$upTypes = 		$this->markAsSelected(UpholsteryType::getList(), $request->getParam("upholstery_type"));
			$colors =		$this->markAsSelected(Color::getColors(), $request->getParam("colors"));
			$armTypes =		$this->markAsSelected(Arm::getTypes(), $request->getParam("arm_type"));
			$crossTypes =	$this->markAsSelected(Cross::getTypes(), $request->getParam("cross_type"));
			
			$wLimitParam = $request->getParam("weight_limit");
			$weightLimit = array(
				"lt120" => isset($wLimitParam['lt120']), 
				"gt120" => isset($wLimitParam['gt120'])
			);
			
			
			$labels = $this->markAsSelected(Table::getTable("Labels")->getList(), $request->getParam("label"));
		}
		$CountSin =0;
		foreach($colors as $c)
			if($c['parent']) $CountSin++;
		$tempu = explode('/',$_SERVER['REQUEST_URI']);
		if(isset($tempu[3]) ){
			$category = \front\models\Di::getInstance()->get("db")->fetchRow("SELECT * FROM `kresla_categories` WHERE url = ?", $tempu[3]);
		}
		if(isset($category['id'])){
			$tf = $this->prepareTableFilter();
		}
// 		die('<pre>'.print_r( $price,1));
// echo $this->minPrice; die;
		if(isset($this->minPrice)){
			$price['min']['current'] = $this->minPrice;
// 			$price['min']['default'] = $this->minPrice;
			$price['max']['current'] = $this->maxPrice;
// 			$price['max']['default'] = $this->maxPrice;
			
		}
		$filters = array(
			'price' => $price,  
			'categories' => $categories, 
			'upholstery_types' => $upTypes, 
			'colors' => $colors, 
			'arm_types' =>$armTypes, 
			'cross_types' => $crossTypes, 
			'height' => $height, 
			'width' => $width, 
			'deep' => $deep, 
			"weight_limit" => $weightLimit, 
			"labels" => $labels,
			"tablefilter" => (isset($tf) ? $tf : false),
			"tablefilter_active" => (int)$this->tableFilterCheck,
			"CountSin" => $CountSin
		);
		// echo $this->tableFilterCheck; die;
		return $this->getDi()->get("lex")->parse($template, $filters);
	}

	public function prepareTableFilter() {
		$tf = new TableFilter();
		$result = $tf->parseUri();
				if($_SERVER['REMOTE_ADDR'] == '80.90.120.182'){
// 				echo 1; die;
// 			die('<pre>'.print_r( $result,1));
		}
		$this->tableFilterArray = $tf->params;
		$this->tableFilterCurrent = $result;

// 		$paramsOrder = array (
// 				  0 => 'types',
// 				  1 => 'colors',
// 				  2 => 'materials',
// 				  3 => 'sizes',
// 				  4 => 'prices',
// 				);
		$paramsOrder = array (
				  0 => 'shapes',
				  1 => 'types',
				  2 => 'colors',
				  3 => 'materials',
				  4 => 'sizes',
				  5 => 'prices',
				);

		$baseUri = $tf->baseUri;
		$this->tableBaseUri = $tf->baseUri;
		if(strstr($_SERVER['REQUEST_URI'],$this->tableBaseUri)){
			$this->tableFilterCheck = 1;
		}else{
			$this->tableFilterCheck = 0;
		}
		$buildUri = array();
		$page = $this->getDi()->get("page");
// 		echo $page->getCurtables(); die;
		
		foreach ($this->tableFilterArray as $paramName => $values) {
			foreach ($values as $key => $fitem) {
				if(isset($result[$paramName])){
					if($result[$paramName]['url'] == $fitem['url']){
						$this->tableFilterArray[$paramName][$key]['active'] = 1;
					}
				}

				$newUrl = '';
				$i = 0;
				$bu = array();
				foreach ($paramsOrder as $pn) {
					if(isset($result[$pn]) && $paramName != $pn){
						$bu[$i++] = $result[$pn]['url'];
					}elseif($paramName == $pn){
						$bu[$i++] = $fitem['url'];

					}
			
				}	
				$curtables = $page->getCurtables();
// 				die('<pre>'.print_r($this->tableFilterCurrent ,1));
				if(isset($this->tableFilterCurrent[$paramName]) &&  $this->tableFilterCurrent[$paramName]['url'] == $fitem['url']){
					$this->tableFilterArray[$paramName][$key]['chosen'] = 1;
					$this->tableFilterArray[$paramName][$key]['activecross'] = 1;
					
				}elseif(isset($this->tableFilterCurrent[$paramName])){
					$this->tableFilterArray[$paramName][$key]['chosen'] = 1;
					$this->tableFilterArray[$paramName][$key]['inactive'] = 1;
				}
// ini_set('display_errors', 'On');     # don't show any errors...
// error_reporting(E_ALL );  # ...but do log them





				$tempu = explode('/',$_SERVER['REQUEST_URI']);
				if(isset($tempu[3]) && $curtables != ''){
					$category = $this->getDb()->fetchRow("SELECT * FROM `kresla_categories` WHERE url = ?", $tempu[3]);
// 	// 				echo $curtables; die;
					$tem = \front\models\Di::getInstance()->get("db")->fetchAll("SELECT `good_id` FROM  `kresla_good_categories` WHERE  `category_id` =".$category['id']." AND good_id IN (".$curtables.")");
					$nc = array();
					foreach($tem as $ite){
// 					
						$nc[] = $ite['good_id'];
					}
					$curtables = implode(',',$nc);
				}
				
				
				
				
				
// 				die('<pre>'.print_r( $curtables,1));
				if($curtables != ''){
					$todelete = false;
					$this->tableFilterArray['show_reset'] = '<div class="table-filter-item-right"><a class="entire-reset" href="'.$baseUri.'">Сбросить фильтр</a></div>';
// 									$sql = "SELECT * FROM `kresla_goods` WHERE table_filter=1 AND  id IN (".$sql2.")";

// 				$goods = \front\models\Di::getInstance()->get("db")->fetchAll($sql);

					switch($paramName){
						case 'types': 
									$sql = "SELECT * FROM `t_types` WHERE type_id=".$fitem['id']." AND  good_id IN (".$curtables.")";
									$rez = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
									break;
						case 'colors': 
									$sql = "SELECT * FROM `t_colors` WHERE color_id=".$fitem['id']." AND  good_id IN (".$curtables.")";
									$rez = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
									break;
						case 'materials':
									$sql = "SELECT * FROM `t_material` WHERE material_id=".$fitem['id']." AND  good_id IN (".$curtables.")";
									$rez = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
									break;
						case 'shapes':
									$sql = "SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND shape=".$fitem['id']." AND id IN (".$curtables.")";
									$rez = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
									break;
						case 'sizes':
								if($fitem['url']==BIG){
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND width>=100 AND id IN (".$curtables.")");
									
								}elseif($fitem['url']==COMPACT){
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND width<=100 AND id IN (".$curtables.")");
								}
								
								break;
						case 'prices':
								if($fitem['url']==UPTO5000){
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND price<=5000 AND id IN (".$curtables.")");
								}elseif($fitem['url']==FROM5000){
									$rez = \front\models\Di::getInstance()->get("db")->fetchAssoc("SELECT `id` FROM `kresla_goods` WHERE table_filter=1 AND hidden = 0 AND price>=5000 AND id IN (".$curtables.")");
								}
// 								echo $curtables;
// 								echo '<br>';
								break;
							}
							if(!count($rez)){
								$this->tableFilterArray[$paramName][$key]['inactive'] = 1;
							}
				}
				if(isset($this->tableFilterCurrent[$paramName]) && $this->tableFilterCurrent[$paramName]['url'] == $fitem['url']){
					$this->tableFilterArray[$paramName][$key]['active'] = 1;
					
				}
				$this->tableFilterArray[$paramName][$key]['raw_url'] = $this->tableFilterArray[$paramName][$key]['url'];
				$this->tableFilterArray[$paramName][$key]['url'] = $baseUri.implode('/',$bu).'/';
				$this->tableFilterArray[$paramName][$key]['reset_url'] = str_replace('/'.$this->tableFilterArray[$paramName][$key]['raw_url'],'',$this->tableFilterArray[$paramName][$key]['url'] );
				

			}
		}
		
	
	
		
		$template = $this->getTemplate("table_filters.html", true);
		// $simple = array(
		// 				1=>array('name'=>'testtest'),
		// 				2=>array('name'=>'testtest2')
		// 	);
		// $test = array(
		// 	'parameters'=>array(
		// 		1=>array(
		// 			'test'=> $simple
		// 	)));

		// echo $this->getDi()->get("lex")->parse($template, $this->tableFilterArray);
		// die;
// 		echo '<pre>'; print_r($this->tableFilterArray); echo '</pre>';
// 		die;
		return $this->getDi()->get("lex")->parse($template, $this->tableFilterArray);
	}

	public function getGTableFilterArray(){
		

	}
	public function getGTables(){
		$tf = new TableFilter();
		$result = $tf->parseUri();
		if(!$result){
			return false;
		}else{
			$this->tableFilterCurrent = $result;
			$goods = $tf->fetchGoods($result);
			$this->tableSeoItem = $tf->tableSeoItem;
			
				$tempu = explode('/',$_SERVER['REQUEST_URI']);
// 				die('<pre>'.print_r( $goods,1));
				if(isset($tempu[3]) && $goods != ''){

// 					echo 123;
					$category = \front\models\Di::getInstance()->get("db")->fetchRow("SELECT * FROM `kresla_categories` WHERE url = ?", $tempu[3]);
// 					echo $curtables; die;
					$sq = "SELECT `good_id` FROM  `kresla_good_categories` WHERE  `category_id` =".$category['id']." AND good_id IN (".$goods.")";
// 					echo $sq; die;
					$tem = \front\models\Di::getInstance()->get("db")->fetchAll($sq);
// 					die('<pre>'.print_r( $tem,1));
					$nc = array();
					foreach($tem as $ite){
// 					
						$nc[] = $ite['good_id'];
					}
					$goods = implode(',',$nc);
				}
				
			return $goods;
		}
	}
	
// 	public function isTableFilter(){
// 		$uri = strtok($_SERVER['REQUEST_URI'],'?');
// 		$tablesBaseUrl = '/catalog/home/tables/';
// 		$relUri = str_replace($tablesBaseUrl,'',$uri);
// 		return false;
// 	}

	
	/**
	 * Список товаров. Страница категории
	 * @param array $options - массив параметров. [category]
	 * @param string $content - шаблон для генерации HTML
	 * @return string - сгенерированный из $content HTML
	 */
	public function goods($options, $content) {
// 109.252.68.168
		
		if(isset($options['tablegoods'])){
			$sql2 = $options['tablegoods'];
			// echo $sql2; die;
			$t = explode(',',$sql2);

			$this->goodsOnpage = count($t);
				$sql = "SELECT * FROM `kresla_goods` WHERE table_filter=1 AND  id IN (".$sql2.")";

				$goods = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
// 				die('<pre>'.print_r( $goods,1));

				
		}else{


			$categoryID = isset($options['category']) ? $options['category'] : null;
			//$page = $this->getDi()->get("page");
			
		
			if($categoryID) {
				
				$pageNumber = $this->getRequest()->getParam("p", 1);
				$onpage = $this->getConfig("onpage");
				
				list($order, $orderDir) = $this->getOrder();
				$goodsOrder = $order.' '.$orderDir;
				
				$params = array(
					'category_id' => $categoryID,
					'order' => $goodsOrder,
					'onpage' => $onpage,
					'pagenumber' => $pageNumber, 
					'where' => $this->getRequest()->getQuery("hits") ? "label_id = 3" : null
				);
				$tparams = $params;
					$tparams['onpage'] = null;
					$tparams['pageNumber'] = null;				
				$tgoods = Good::find($tparams);
				$minPrice = Good::getMaxPrice();
				$maxPrice = Good::getMinPrice();		
				if($tgoods) foreach($tgoods as $good) {
					if($good['price'] < $minPrice){
						$minPrice = $good['price'];
					}
					if($good['price'] > $maxPrice){
						$maxPrice = $good['price'];
					}
					$ids[] = $good['id'];
				}
				$this->ids = implode(',',$ids);
				if($minPrice >= $maxPrice){
					$minPrice = Good::getMinPrice();
					$maxPrice = Good::getMaxPrice();
				
				}

				$this->minPrice = $minPrice;
				$this->maxPrice = $maxPrice;					
								
				
				if($pageNumber == 'all'){
					$params['onpage'] = null;
					$params['pageNumber'] = null;
					$onpage = &$goodsCount;
				}
				$Category = Category::find($categoryID);
				$page = $this->getDi()->get("page");
				$page->setTitle($Category->getPageTitle());
				
				$goodsCount = Good::count($params);
				$goods = Good::find($params);


				$this->setOnpage($onpage);			
				$this->setGoods($goods);
				$this->setTotal($goodsCount);
			
			
			}
			
		}

		$page = $this->getDi()->get("page");
		$query = $this->getRequest()->getQuery();

		//если пагинаторная страница или страница "Показать все"
		if ( isset($query['p']) )
			$page->setMetaForPaginatorPages('<meta name="robots" content="noindex, follow"/>');
		else
			$page->setMetaForPaginatorPages('<meta name="robots" content="all"/>');


		return $this->renderGoods($goods);
	}

	public function renderGoods($goods) {
		
		$goods = Good::hitchData($goods, array("upholsteries", "categories", "upholstery_types", "colors", "label", "url" ));
			
		//костыль, необходим, чтобы столбец "Спинка" не отображался в таблице для категорий не кресел

		if (!isset($goods[0]['_upholsteries'])) {
			$back = false;
		}else{
			$back = ($goods[0]['_upholsteries'][0]['type_id']==7) ? false : true ;
		}
		for($i=0; $i<count($goods); $i++){
			$goods[$i]['haveback'] = $back;
		}
		
		/////////////////////////////////////////////////////////////////////////////////////
		//для копьютерных столов и журнальных столиков colors берем из таблицы "table_colors"
		///////////////////////////////////////////////////////////////////////////////////////
		if ($goods[1]["_categories"][0]["id"] == 10 || $goods[1]["_categories"][0]["id"] == 14 ) {
			foreach ($goods as &$good) {
				$sql = "SELECT `table_color`.title, `table_color`.class_name FROM `t_colors`
						INNER JOIN `table_color` ON `t_colors`.color_id = `table_color`.id
						WHERE `t_colors`.good_id =".$good['id'];
				
 				$rs = \front\models\Di::getInstance()->get("db")->fetchAll($sql);
 				$good['_t_colors'] = $rs;
 			}
		}
		//////////////////////////////////////////////////////////////////////////////////////////
		//#101062 20.01.2016 00:00 by Dmitrievskiy Sergey
		
		return $this->getDi()->get("lex")->parse($this->getTemplate("good_list.html", true), array("goods" => $goods, "haveback"=>$back ));
	}
	


	/**
	 * Получить значения для слайдера (min, max) учитывая введённные пользователем данные
	 * @param string $name
	 * @param int $defaultMin - минимум по умолчанию
	 * @param int $defaultMax - максимум по умолчанию
	 */
	private function getSliderValues($name, $defaultMin, $defaultMax) {
		
		$request = $this->getRequest();
		$minName = $name."_min";
		$maxName = $name."_max";
		
		$values =  array(
			'min' => array(
					'current' => $request->getParam($minName, $defaultMin),
					'default' =>$defaultMin
			),
			'max' => array(
					'current' => $request->getParam($maxName, $defaultMax),
					'default' => $defaultMax
			)
		);
		
		return $values;
	}
	
	/**
	 * Пометить элементы, как выделенные
	 * @param array $items - элементы, которые надо пометить
	 * @param array $requestedItems - массив id элементов, которые выбраны пользователем
	 * @return $items - возвращаем исходные $items с добавленным флагом _selected
	 */
	private function markAsSelected($items, $requestedItems) {
		foreach($items as $key => $item) {
			if(is_array($requestedItems) && in_array($item['id'], $requestedItems)) {
				$items[$key]['_selected'] = true;
			} else {
				$items[$key]['_selected'] = false;
			}
		}
		return $items;
	}
	
	/**
	 * Получить список популярных товаров
	 * @param array $options
	 * @param string $content
	 * @return array
	 */
	public function getPopularList($options, $content) {
		
		$limit = isset($options['limit']) ? $options['limit'] : $this->getConfig("popular_goods_limit");
		
		$goods = Good::findPopular(array(
			"limit" => $limit
		));
		
		if($goods) {
			$goods = Good::hitchData($goods, array("url", "category_url", "category_name"));
		}
		
		return $this->getDi()->get("lex")->parse($this->getTemplate("goods_list_with_title.html", true), array("goods" => $goods, "blockName" => "Популярные товары"));
	}
	
	public function getNewList($options, $content) {
		
		$limit = isset($options['limit']) ? $options['limit'] : $this->getConfig("new_goods_limit");
		
		$goods = Good::findNewGoods(array(
			"limit" => $limit
		));
		
		if($goods) {
			$goods = Good::hitchData($goods, array("url", "category_url", "category_name"));
		}
		
		return $this->getDi()->get("lex")->parse($this->getTemplate("goods_list_with_title.html", true), array("goods" => $goods, "blockName" => "Новинки"));
	}
	
	public function getSolutions($options, $content) {
		return $this->getDi()->get("templates")->find($options['name']);
	}

	public function categories($options, $content) {
		$template = $this->getTemplate("categories.html", true);
				
		$categories = Category::getTree();
		
		foreach ($categories as &$rootCategory) {
			$rc = new Category($rootCategory['id']);
			if($rootCategory['_children']) foreach($rootCategory['_children'] as &$childCategory) {
				$cc = new Category($childCategory['id']); 
				$childCategory['_goods_count'] = $cc->getGoodsCount();
				$childCategory['_cheapest_price'] = $cc->getCheapestPrice();
				$childCategory['url'] = '/catalog/'.$rootCategory['url'].'/'.$childCategory['url'].'/';
			}
		}
		
		return $this->getDi()->get("lex")->parse($template, array("categories" => $categories));
	}

	/**
	 * Получить сортировку, заданную в запросе
	 */
	public function getOrder() {
		$order = $this->getRequest()->getQuery("order", "number");
		$orderDir = $this->getRequest()->getQuery("order_dir", "ASC");
			
		if(!in_array($order, array("price", "number"))) {
			$order = "number";
		}
		if(!in_array($orderDir, array("ASC", "DESC"))) {
			$orderDir = "ASC";
		}
		return array($order, $orderDir);
	}
	
	/**
	 * Рендерим блок сортировок
	 * @param unknown_type $options
	 * @param unknown_type $template
	 * @return string
	 */
	public function sortBlock($options, $template) {
		list($order, $orderDir) = $this->getOrder();
		
		$orders = array(
			array(
				"title" => "Популярности", 
				"name" => "number"
			),
			array(
				"title" => "Цене",
				"name" => "price"
			)
		);
		
		$template = '<a href="{href}" rel="nofollow" {active}>{title}</a> {dir}';
		$html = 'Сортировать товар по ';
		
		foreach ($orders as $o) {
			$active = $o['name'] == $order;
			
			$o['active'] = $active ? 'class="active"' : '';
			$o['href'] = $this->getRequest()->getRequestUri();
			
			if($active) {
				$o['dir'] = $orderDir == 'ASC' ? '↓' : '↑';
			} else {
				$o['dir'] = '↑';
			}
			
			$path = $this->getRequest()->getPathInfo();
			$query = $this->getRequest()->getQuery();
			$query['order'] = $o['name'];
			
			
			$query['order_dir'] = $orderDir == 'ASC' ? 'DESC' : 'ASC';
			
			if($o['name']=='number' && $order =='price' )
				$query['order_dir'] = 'ASC';
			
			$o['href'] = $path . '?' . http_build_query($query);
			
			$html .= $this->getDi()->get("lightParser")->insertValues($template, $o);
		}
		
		return $html;
	}
	
	public function linkShowAll(){
		$path = $this->getRequest()->getPathInfo();
		$query = $this->getRequest()->getQuery();
		$query['p']='all';
				if($_SERVER['REMOTE_ADDR'] == '109.252.68.173'){
// 			echo $_SERVER['REQUEST_URI']; die;
// 			echo 1; die;
// 			/catalog/office/?p=all
			if($_SERVER['REQUEST_URI'] == '/catalog/office/?p=all'){
				$this->getPage()->setTitle('Кресла в офис - Кресла От и До');
			}
			
		}
		return '<!--noindex--><a rel="nofollow" href="'.$path . '?' . http_build_query($query).'">Показать всё</a><!--/noindex-->';
		
	}
	
	/**
	 * Рендерим блок фильтров
	 * @param unknown_type $options
	 * @param unknown_type $template
	 */
	public function filterBlock($options, $template) {
		$data = array(
			"checked" => $this->getRequest()->getQuery("hits") ? 'checked="checked"' : ''
		);
		return $this->getDi()->get("lex")->parse($template, $data);
	}
	
	/**
	 * 
	 * @param integer $goodID
	 * @return \front\modules\Comments
	 */
	protected function getCommentsModel($goodID) {
		if($this->commentsModule == null) {
			/* @var $commentsModule \front\modules\Comments */
			$commentsModule = $this->getDi()->getModule("comments");
			$commentsModule->initSettings(array(
					"object_id" => $goodID,
					"object_id__table" => $this->getGoodsTableName(),
					"moderate" => true
			), null);
			$this->commentsModule = $commentsModule;
		}
		return $this->commentsModule;
	}
	
	public function goodPage($options, $content) {
		/* @var $goodObject \front\modules\CatalogKresla\models\Good */
		$goodObject = $this->getDi()->get("good");
		$good = $goodObject->asFullArray();
		
		$commentsModule = $this->getCommentsModel($good['id']);
		
		$good['_comments_html'] = $commentsModule->getList(array(
				"onpage" => $this->getConfig("comments_onpage")
		), $this->getTemplate("comments.html", true));
		$good['_comments_count'] = $commentsModule->count(array(), null);
		$good['_comments_form'] = $commentsModule->form(array("object_id" => $goodObject->getID(), "object_id__table" => $this->getGoodsTableName()), file_get_contents(dirname(__FILE__) . '/templates/comments_form.html'));
		$good['_comments_onpage'] = $this->getConfig("comments_onpage");
		$good['_upholstery_html'] = $this->upholsterySelect(array('upholsteries' => $good['_upholsteries']), null);
		$good['_arms_html'] = $this->armsSelect(array('arms' => $good['_arms']), null);
		

		$good['_recently_viewed_html'] = $this->recentlyViewed(array("is_good_page" => 1));
		$good['_same_goods_html'] =  $this->sameGoods();
		
		if( $good['category']->getId() != 17 && $good['hidecat'] != 1){
			$titl = $good['category']->getTitle1().' '.$good['title'].' - Кресла От и До';
		}else{
			$titl = $good['title'].' - Кресла От и До';
		
		}
		$this->getPage()->setTitle($titl);
		$this->getPage()->setKeywords(mb_strtolower($good['category']->getTitle1().' '.$good['title']));
		
		/*
		$i=0;
		$descr='';
		while($i<150 || $good['text'][$i]!='.'){
			$descr.= $good['text'][$i++];
		}
		*/
		
		$descr = $good['text'];
		if (strlen($descr) > 150) {
			if (preg_match('/(^.{150}[^\.]*)\./', $descr, $m)) {
				$descr = $m[1];
			}elseif(preg_match('/(^.{150}[^\s]*)\s+/s', $descr, $m)){
				$descr = $m[1];
			}
		}
		
		$this->getPage()->setDescription(preg_replace('/<.*?>/', '', $descr));
		
		$good['text'] = str_replace("\n", '<br />', $good['text']);
		
		/* последние просмативаемые товары */
		require_once 'models/CookieStorage.php';
		$cs = new CookieStorage();
		$cs->saveGoodView($good['id']);
// 	die('<pre>'.print_r( $good,1));
		return $this->render("good_page.php", array(
			"good" => $good, 
			"returnPath" => $this->getRequest()->getServer("HTTP_REFERER")
		));
	}
	
	/**
	 * Похожие товары - извлекает из БД и рендерит список похожих товаров. 
	 * @param array $options
	 * @param string $content
	 * @return string
	 */
	public function sameGoods($options = array(), $content = null) {
		if(isset($options['good_id'])) {
			$good = new Good($options['good_id']);
		} else {
			$good = $this->getDi()->get("good");
		}
		
		if($good) {
			$goods = $good->getSimilarGoods($this->getConfig("same_goods_limit"));
			$goods = Good::hitchData($goods, array("upholstery_types", "colors", "label", "url"));
			
			$template = $this->getTemplate("same_goods.html", true);
			return $this->getDi()->get("lex")->parse($template, array("goods" => $goods));
		}
	}
	
	public function recentlyViewed($options = array(), $content = null) {
		$limit = isset($options['limit']) ? $options['limit'] : $this->getConfig("recently_viewed_limit");
		$topLimit = isset($options['top_limit']) ? $options['top_limit'] : $this->getConfig("recently_viewed_top_limit");
		$content = $content ? $content : $this->getTemplate("recently_viewed.html", true);
		
		/* последние просмативаемые товары */
		require_once 'models/CookieStorage.php';
		$cs = new CookieStorage();
		
		$views = $cs->getViewedGoods();
		
		if($views) {
			$views_m = array_flip(array_reverse(array_flip($views)));
			$views = $views_m;
			foreach($views as $kv => $v){
				$tlatestViews = Good::find(array("ids" => array('0'=>$kv)));
				$latestViews[] = $tlatestViews[0];
			}
			$latestViews = Good::hitchData($latestViews, array("upholsteries", "upholstery_types", "colors", "url"));
			$raw_latestViews = $latestViews;
			
			$visibleItems = array();
			$invisibleItems = array();
			foreach ($latestViews as $key => $g){
				if($key < $limit) {
				
					$visibleItems[] = $latestViews[$key];
					unset($latestViews[$key]);
				
				}else{
					$invisibleItems[] = $latestViews[$key];
				}
				
			}
 			
			$latestViews = array_slice($latestViews, 0, $topLimit);
			$viewsCount = count($invisibleItems);
			return $this->getDi()->get("lex")->parse($content, array(
					"items" => $raw_latestViews,
					"items_visible" => $visibleItems, 
					"items_invisible" => $invisibleItems, 
					"limit" => $limit,
					"more_count" => $viewsCount, 
					"is_good_page" => isset($options['is_good_page']) ? true : false
			));
		}
	}
	
	
	public function ajax() {
		/* @var $request \Bof_Request */
		$request = $this->getDi()->get("request");
		/* @var $page \front\models\Page */
		$page = $this->getDi()->get("page");
		
		/* запрос пришел со страницы товара. Подгружаем комментарии */
		if(($goodID = $request->getParam("good_id")) != null) {
			$good = new Good($goodID);
			
			if($request->getParam("get_comments")) {
				$commentsModule = $this->getCommentsModel($goodID);
				return $commentsModule->getList(array(
					"onpage" => $request->getParam("onpage"), 
					"pageNumber" =>  $request->getParam("page_number")
				), $this->getTemplate("comments.html", true));
			}			
		}
		
		$categoryPage = $page->findChildPage("category_page");
		$whereOptions = $request->getParam("where");
		
		$options = array(
			'category_id' => $whereOptions['category_id']
		);
		if(isset($whereOptions['get_hits'])) {
			$options['label_id'] = $whereOptions['get_hits']  == 'true' ? 3 : null;
		}
		
		if(($sort = $request->getParam("sort")) != null) { 
			if(in_array($sort['type'], array("price", "number")) && in_array($sort['dir'], array("ASC", "DESC"))) {
				$options['order'] = $sort['type'] . ' ' . $sort['dir'];
			}
		}
		
		$goods = Good::find($options);
			
		$response = $this->renderGoods($goods);
		
		header("Content-type:text/html; charset=utf-8");
		echo $response;
	}
	
	/**
	 * Список обивок выбранного кресла на странице товара. 
	 * @param array $options
	 * @param string $content
	 */
	public function upholsterySelect($options, $content) {
		/* @var $good \front\modules\CatalogKresla\Models\Good */
		$upholsteries = $options['upholsteries'];
		
		if (isset($_GET['dev'])) {
			//$tOne = $this->getTemplate("good_upholstery_one.php", true);
			$tMany = $this->getTemplate("good_upholstery_many.php", true);
			
			$html = "";
		
			if($upholsteries) foreach($upholsteries as $parentUpholstery) {
				foreach($parentUpholstery['_children'] as $price => $rows) {
					$rowsCount = count($rows['_rows']);
						$html .= $this->getDi()->get("lex")->parse($tMany, $rows);
				}
			}
			
			return $html;
			
			
		} else {
			
			$tOne = $this->getTemplate("good_upholstery_one.html", true);
			$tMany = $this->getTemplate("good_upholstery_many.html", true);
			
			$html = "";
		
			if($upholsteries) foreach($upholsteries as $parentUpholstery) {
				foreach($parentUpholstery['_children'] as $price => $rows) {
					$rowsCount = count($rows['_rows']);
					if($rowsCount == 1) {
						$html .= $this->getDi()->get("lex")->parse($tOne, $rows);
					} else {
						$html .= $this->getDi()->get("lex")->parse($tMany, $rows);
					}
				}
			}
			
			return $html;
			
			
			
		}
		//$tOne = $this->getTemplate("good_upholstery_one.html", true);
		//$tMany = $this->getTemplate("good_upholstery_many.html", true);
		
/*
		$html = "";
		
		if($upholsteries) foreach($upholsteries as $parentUpholstery) {
			foreach($parentUpholstery['_children'] as $price => $rows) {
				$rowsCount = count($rows['_rows']);
				if($rowsCount == 1) {
					$html .= $this->getDi()->get("lex")->parse($tOne, $rows);
				} else {
					$html .= $this->getDi()->get("lex")->parse($tMany, $rows);
				}
			}
		}
		
		return $html;
*/
	}
	
	public function armsSelect($options, $content) {
		/* @var $good \front\modules\CatalogKresla\Models\Good */
		$arms = $options['arms'];
		
		if (isset($_GET['dev'])) {
			$tOne = $this->getTemplate("good_arms.php", true);
		} else {
			$tOne = $this->getTemplate("good_arms.html", true);
		}
	
		//$tOne = $this->getTemplate("good_arms.html", true);
		
		//$tMany = $this->getTemplate("good_upholstery_many.html", true);
	
		$html = "";
	
		
		$a=0;
		if($arms) 
			foreach($arms as $arm){
				if($arm['type_id']==3)
					$a=1;
			}
				
		if($a)
			$html .= $this->getDi()->get("lex")->parse($tOne, array('arms'=>$arms));
				
			
		
	
		return $html;
	}
	

	
	public function search($options, $content = null) {
		
		$request = $this->getRequest();
		
		/* запрос с формы поиска */
		require_once "Bof/Utils.php";
			
		$where = array();
			
		$where[] = array('price >= ?' => $request->getParam("price_min", 0));
		$where[] = array('price <= ?' => $request->getParam("price_max", Good::getMaxPrice()));
			
		$where[] = array('height_min >= ?' => $request->getParam("height_min", 0));
		$where[] = array('height_max <= ?' => $request->getParam("height_max", Good::getMaxHeight()));
			
		$where[] = array('width >= ?' => $request->getParam("width_min", 0));
		$where[] = array('width <= ?' => $request->getParam("width_max", Good::getMaxWidth()));
			
		$where[] = array('deep >= ?' => $request->getParam("deep_min", 0));
		$where[] = array('deep <= ?' => $request->getParam("deep_max", Good::getMaxDeep()));
			
		$goodsIds = array();
			
		/* фильтр по категориям */
		if(($ids = $request->getParam("categories")) != null) {
			$select = $this->getDb()->select()
				->from($this->getGoodCategoriesTableName(), array("good_id"))
				->where("category_id IN (?)", $ids);
			$ids = $this->getDb()->fetchCol($select);
			$goodsIds[] = $ids ? $ids : array();
		}
			
		/* фильтр по материалу обивки */
		if(($ids = $request->getParam("upholstery_type")) != null) {
			$select = $this->getDb()->select()
			->from(array("GU" => $this->getGoodUpholsteryTableName()), array("good_id"))
			->join(array("U" => $this->getUpholsteryTableName()), "U.id = GU.upholstery_id", array())
			->where("U.type_id IN (?)", $ids);
			$ids = $this->getDb()->fetchCol($select);
			$goodsIds[] = $ids ? $ids : array();
		}
			
		/* фильтр по цвету */
		if(($ids = $request->getParam("colors")) != null) {
			
			//Синонимы отключи
			/*foreach($ids as $idd => $iidd){
				$isel = $this->getDb()->select()
				->from(array($this->getColorsTableName()), array("parent"))
				->where('id = (?)', $ids[$idd]);
				$ids[] = $this->getDb()->fetchCol($isel);
			}
			//$ids = array_unique($ids);*/
			$select = $this->getDb()->select()
			->from(array("GU" => $this->getGoodUpholsteryTableName()), array("good_id"))
			->join(array("U" => $this->getUpholsteryTableName()), "U.id = GU.upholstery_id", array())
			->where("U.color_id IN (?)", $ids);
			$ids = $this->getDb()->fetchCol($select);
			
			$goodsIds[] = $ids ? $ids : array();
		}
			
		/* фильтр по материалу подлокотника */
		if(($ids = $request->getParam("arm_type")) != null) {
			$select = $this->getDb()->select()
			->from(array("GA" => $this->getGoodArmsTableName()), array("good_id"))
			->join(array("A" => $this->getArmsTableName()), "A.id = GA.arms_id", array())
			->where("A.type_id IN (?)", $ids);
			$ids = $this->getDb()->fetchCol($select);
			$goodsIds[] = $ids ? $ids : array();
		}
			
			
		/* фильтр по материалу крестовины */
		if(($ids = $request->getParam("cross_type")) != null) {
			$select = $this->getDb()->select()
			->from(array("GA" => $this->getGoodCrossTableName()), array("good_id"))
			->join(array("A" => $this->getCrossTableName()), "A.id = GA.cross_id", array())
			->where("A.type_id IN (?)", $ids);
			$ids = $this->getDb()->fetchCol($select);
			$goodsIds[] = $ids ? $ids : array();
		}
			
		if($goodsIds) {
			$ids = $goodsIds[0];
			for($i = 1; $i < count($goodsIds); $i++) {
				$ids = array_intersect($ids, $goodsIds[$i]);
			}
			
			if($ids) {
				$where[] = array("id in (?)" => $ids);
			} else {
				$where[] = array("1 = 0");
			}
		}
			
		/* Ограничение по весу. 
		 * Особый случай для WHERE условия: 
		 * если заданы 2 значения, значит, ограничения нет совсем, иначе подставляем выбранное ограничение
		 * Если  добавится 3 условия по весу, то придется сюда вносить изменения
		 * */
		if(($ids = $request->getParam("weight_limit")) != null) {
			$wlWhereParts = array();
			foreach ($ids as $limit) {
				if(preg_match('/^(lt|gt)(\d+)$/', $limit, $m)) {
					$signs = array("gt" => ">", "lt" => "<=");
					/* это where условие должно работать как OR а не AND */
					$wlWhereParts[] = "weight_limit " . $signs[$m[1]]. " ".$m[2];
				}
			}
			$where[] = implode(" OR ", $wlWhereParts);
		}
		
		/* регулируемая спинка */
		if($request->getParam("back_adjust")) {
			$where[] = "back_adjust = 1";
		}
			
		/* фильтр по материалу крестовины */
		if(($ids = $request->getParam("label")) != null) {
			$where[] = array("label_id IN (?)" => $ids);
		}
		
		
		$searchParams = array("where" => $where,
				
				);
		
		$page = $this->getDi()->get("page");
			
		$p = \front\models\Page::find(15);
		$p = new \front\models\Page($p);
		$pageNumber = $this->getRequest()->getParam("p", 1);
		$onpage = $this->getConfig("onpage");
		
		
		$this->setOnpage($onpage);
		
		$goodsCount = Good::count($searchParams);
		$this->setTotal($goodsCount);
				
		if($request->isXmlHttpRequest()) {
			$goods = Good::find($searchParams);
			
			$this->setGoods($goods);
			
			
			$ids = array();
			$minPrice = Good::getMaxPrice();
			$maxPrice = Good::getMinPrice();
			
			if($goods) foreach($goods as $good) {
				if($good['price'] < $minPrice){
					$minPrice = $good['price'];
				}
				if($good['price'] > $maxPrice){
					$maxPrice = $good['price'];
				}
				$ids[] = $good['id'];
			}
			$this->ids = implode(',',$ids);
			if($minPrice >= $maxPrice){
				$minPrice = Good::getMinPrice();
				$maxPrice = Good::getMaxPrice();
			
			}
			header("Content-type: text/html; charset=utf-8");
			if($request->getQuery("action") == 'get_list') {
				echo $this->renderGoods($goods);
			}
			if($request->getQuery("action") == 'get_info') {
				echo json_encode(array(
					'count' => count($goods),
					'url' => $request->getRequestUri(),
					"ids" => $ids,
					"min_price" => $minPrice,
					"max_price" => $maxPrice
				));
			}
			exit();
		} else {
			$tgoods = Good::find($searchParams);
// 			die('<pre>'.print_r( $tgoods,1));
			if($pageNumber != 'all'){
				$searchParams = array("where" => $where,
						'onpage' => $onpage,
						'pagenumber' => $pageNumber,
				);
			}else{
				$this->setOnpage($goodsCount);
			}
			
			
			if( $request->getParam("order") ) {
				$searchParams['order']= $request->getParam("order").' '.$request->getParam("order_dir");
			}else{
				$searchParams['order']= 'number';
			}
				
			
			$goods = Good::find($searchParams);
			$ids = array();
			$minPrice = Good::getMaxPrice();
			$maxPrice = Good::getMinPrice();
			
			if($tgoods) foreach($tgoods as $good) {
				if($good['price'] < $minPrice){
					$minPrice = $good['price'];
				}
				if($good['price'] > $maxPrice){
					$maxPrice = $good['price'];
				}
				$ids[] = $good['id'];
			}
			$this->ids = implode(',',$ids);
			if($minPrice >= $maxPrice){
				$minPrice = Good::getMinPrice();
				$maxPrice = Good::getMaxPrice();
			
			}
			$this->minPrice = $minPrice;
			$this->maxPrice = $maxPrice;
			
			return $this->renderGoods($goods);
		}
		
		
	}

	public function route(\front\models\Page $page, $request) {
		// echo 1; die;
		$parts = $page->getChildURLParts();		
		$last = end($parts);
		if(preg_match('/^(.*?)\.html$/', $last, $m)) {
			$Url = $m[1];
			try {
				/* страница товара */
				//если придет айдишник перебросим на нужную страницу
				if(is_numeric($Url)){
					$good = new Good($Url);
					header("HTTP/1.1 301 Moved Permanently");
					$url = $_SERVER['HTTP_HOST'].'/catalog/'.$good->getFullURL();
					header('location: http://'.$url); 
					exit();
				}
				$good = new Good();
				$good->loadByUrl($Url);
				
				array_pop($parts);
				$category = Category::findTree($parts);				
				if($category['id'] == $good->getCategoryID()) {
					$category = new Category($category['id']);
					$goodPage = $page->findChildPage("good_page");
					if($goodPage) {
						$goodPage['title'] = $good->getTitle();
						$goodPage['keywords'] = $good->getKeywords();
						$goodPage['description'] = $good->getDescription();
						$this->getDi()->register("good", $good);
						
						$breadcrumbs = $page->getBreadcrumbs();
						$breadcrumbs[] = array(
								"title" => $category->getTitle(),
								"href" => $page->getFullUrl().$category->getFullUrl().'/'
						);
						$breadcrumbs[] = array(
								"title" => $goodPage['title'],
								"href" => $this->getRequest()->getRequestUri()
						);
						$goodPage['_breadcrumbs'] = $breadcrumbs;
						return $goodPage;
					}
				}
			} catch (\front\modules\CatalogKresla\models\RecordNotFoundException $ex) {
				return null;
			}			
		} elseif(count($parts) == 1 && $last == 'ajax') {
			/* системные обработчики */
			return array($this, "ajax");
		} elseif($last == 'search') {
			/* страница поиска */
			$page = $page->findChildPage("search");
			return $page;
		} elseif($last == 'textsearch') {
			/* страница текстового поиска */
			$page = $page->findChildPage("textsearch");
			return $page;
		} elseif($last == 'yml') {
			return array($this, "yml");
		} else {
			/* страница категории */
			$category = Category::findTree($parts);
			if($category) {
				$categoryPage = $page->findChildPage("category_page");
				if($page) {
					$categoryPage['title'] = $category['title'];
					$categoryPage['keywords'] = $category['keywords'];
					$categoryPage['description'] = $category['description'];
					$categoryPage['category'] = $category;
					
					$breadcrumbs = $page->getBreadcrumbs();
					$breadcrumbs[] = array(
						"title" => $category['title'], 
						"href" => $this->getRequest()->getRequestUri()
					);
					$categoryPage['_breadcrumbs'] = $breadcrumbs;
					/* сохраним id категории в request для того, чтоб она была выбрана в фильтрах */
					$request->setParam("categories", array($category['id']));
					return $categoryPage;
				}
			}
		}
	}
	
	public function getMap($pageURL) {
		$categories = Category::getList();
		$items = array();
		foreach ($categories as $category) {
			$category = new Category($category['id']);
			$items[] = array(
				"title" => $category->getTitle(), 
				"href" => $pageURL . $category->getUrl() . '/', 
				"_children" => array()
			);
		}
		
		return $items;
	}
	
	public function textsearch($options, $template) {
		$data = array(
			"query" => htmlspecialchars($this->getRequest()->getQuery("q", 'Поиск по названию / артикулу'))
		);
		return $this->fetch("textsearch.html", $data);
	}
	
	public function yml() {
		/* пишем тупо руками */
		/* @var $ymlBuilder \front\models\YmlBuilder */
		$ymlBuilder = $this->getDi()->get("YmlBuilder");
		
		$ymlBuilder->addShop("Кресла От и До", 'ООО "Мебель От и До"', 'http://www.kresla-otido.ru/');
		
		$categories = Category::getList(); 		
		
		$categories_indexes = array();

		foreach ($categories as $category) {
			$ymlBuilder->addCategory($category['id'], $category['title']);
			$categories_indexes[$category['id']] = 1; 
		}
		
		$goods  = Good::find(array("in_ya_market" => 1));
		//$goods = Good::expandImagesUrls($goods, $this->getGoodsTableName());
		

		foreach ($goods as $good) {
			$good = new Good($good);
			$offer = array(
				"id" => $good->getID(), 
				"available" => !$good->isOnOrder(),  
				"url" => "http://www.kresla-otido.ru/catalog/".$good->getFullURL(), 
				"price" => $good->getPrice(), 
				"currencyId" => "RUR", 
				"categoryId" => $good->getCategoryID(), 
				"picture" => $good->getFirstPhoto() ? "http://www.kresla-otido.ru" . str_replace(' ','%20',$good->getFirstPhoto()) : null, 
				"delivery" => true, 
				"local_delivery_cost" => $this->getConfig("market_delivery_cost"), 
				"name" => $good->getSalesNotes(), 
				"vendor" => $good->getVendorName(), 
				"vendorCode" => $good->getVendorCode(), 
				"description" => $good->getText(),
				"manufacturer_warranty" => $good->hasWarranty(), 
				"country_of_origin" => $good->getCountryOfOrigin()
			);
                        if(isset($categories_indexes[$good->getCategoryID()])){
				$ymlBuilder->addOffer($offer);			
			}

		}
		
		$yml = $ymlBuilder->getXML();
		header("Content-type: text/xml; charset=utf8");
		return $yml;
	}
	
	public function showPhone(){
		date_default_timezone_set( 'Europe/Moscow' );
		$h = date('H');
		$dayNumber = date("w") ; 
		  $basePhone = '<img src="/img/phone.jpg" alt="(495) 678-3999" />';
		  $hPhone =   '<img src="/img/phone2.jpg" alt="(495) 972-9842" />';
		$label = '<br>Ежедневно с 10-00 до 23-00';

		if(($h >=19 && $h<23 ) || $dayNumber == 6 || $dayNumber == 0){
			return $hPhone.$label;
		}else{
			return $basePhone.$label;
		}
		
		
	}
	
	public function showSeoText($options, $content){
		$request = $this->getRequest();
		if(!$request->getParam('p'))
			return $this->getDi()->get("lex")->parse($content);
	}
	
	
	public function readySet($options, $content){
		$viewCats = array('office_armchairs', 'conference_chairs', 'stuff_chairs', 'chief_chairs', 'office_stool', 'armchairs', 'children');
		
		
		$request = $this->getRequest();
		$urlParts = explode('/', trim($request->getPathInfo(), '/'));
		
	
		
		if($urlParts[0]!='catalog'){
			//костыль, чтобы клайстер работал на /catalog/cat, и не работал на /
			throw new \Exception("Страница не найдена", 404);
		}
		
		if(!isset($urlParts[1])){
			//не отображать блок если находимся на странице /catalog/ (не выбрана категория)
			return '';
		}
		
		if($urlParts[1]=='search' || $urlParts[1]=='textsearch'){
			//если на странице поиска тоже не отображать блок
			return '';
		}
		
		
		
		
		$color_ = false;
		$material_ = false;
		$color = false;
		$material = false;
		if($request->getParam("categories")){
			$category = new Category ($request->getParam("categories"));
			if($request->getParam("upholstery_type"))
				$material_ = Upholstery::find($request->getParam("upholstery_type"));
			if($request->getParam("color"))
				$color_ = Color::find($request->getParam("color"));
		}else{
			if(isset($urlParts[2])){				
				$category = new Category (1);
				$category->findByUrl($urlParts[2]);
			}
			
			if($urlParts[3]){
				$color_ = Color::find(array( 'where'=> array('url =?' => $urlParts[3]) ));
			
				if(!$color_){
					$material_ = UpholsteryType::findByOptions(array( 'where'=> array('url =?' => $urlParts[3]) ));
					
					if(isset($urlParts[4]))
						$color_ = Color::find(array( 'where'=> array('url =?' => $urlParts[4]) ));
					
				}
			}
	
		}
		if($color_)
			$color = $color_[0];
		if($material_)
			$material = $material_[0];
		
		if(!in_array($category->getUrl(), $viewCats)){
			return '';
		}
		
	
		/*
		echo "Cat=".$category->getTitle()."<br>";
		echo "Color=".$color['title']."<br>";
		echo "Material=".$material['title']."<br>";
		*/
		
		/*$sets = Category::getNotEmptySets($category->getId());
		echo "<pre>";
		print_r($sets);*/
		
		
		$colors=null;
		if(!$color){
			$colors =	Color::getNotEmptyColors($category->getId(), $material['id']);
			foreach($colors as $k=>$v){
				$maturl = ( ($material) ? $material['url']."/" : '');
				$colors[$k]['path']= "/catalog/".$category->getParent()->getUrl()."/".$category->getUrl()."/".$maturl.$colors[$k]['url']."/";
				
				$matanc = ( ($material) ? " из ".$material['title_parent'] : '');
				$colors[$k]['anchor']=$colors[$k]['title_parent']." ".mb_strtolower($category->getTitle()).$matanc;
			}
		}
		
		
		
		$upTypes=null;
		if(!$material){
			$upTypes = 	UpholsteryType::getNotEmptyTypes($category->getId(), $color['id']);
			foreach($upTypes as $k=>$v){
				$colurl = (($color) ? $color['url']."/": '');
				$upTypes[$k]['path'] = "/catalog/".$category->getParent()->getUrl()."/".$category->getUrl()."/".$upTypes[$k]['url']."/".$colurl;
				
				$colanc = (($color) ? $color['title_parent']." " : '');
				
				$upTypes[$k]['anchor'] = $colanc.mb_strtolower($category->getTitle())." из ".$upTypes[$k]['title_parent'];
				$upTypes[$k]['anchor'] = mb_strtoupper(mb_substr($upTypes[$k]['anchor'], 0, 1)).mb_substr($upTypes[$k]['anchor'], 1, mb_strlen($upTypes[$k]['anchor']));
			}
		}
		
		if($material && $color)
			return;
		
		return $this->fetch("readySet.html", array('types'=>$upTypes, 'colors'=>$colors));
	
	}
	
	
	
	
}
