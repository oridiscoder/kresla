<?php
namespace front\modules;

class Portfolio extends \front\modules\Module {
	
	public function getTitle() {
		return "Портфолио DEV";
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
				
	}
	
	public function getTableName() {
		return $this->getDbPrefix()."portfolio";
	}
	
	public function getTypesTableName() {
		return $this->getDbPrefix()."portfolio_types";
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::install()
	 */
	public function install() {
		
		$tableName = $this->getTableName();
		$typesTableName = $this->getTypesTableName();
		
		$sql = "
		CREATE TABLE IF NOT EXISTS `$typesTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED NOT NULL DEFAULT 0,
			`hidden` TINYINT(1) NOT NULL DEFAULT 0,
			`title` VARCHAR(255),
			`keywords` VARCHAR(255),
			`description` VARCHAR(500),
			`url` VARCHAR(255),
			`text` TEXT
		)";
		$this->getDb()->query($sql);
		
		$tableFactory = $this->getTableFactory();
		$tableFactory->addTable($typesTableName, "Типы проектов");
		$tableFactory->addStringField("title", "Название")
					->addStringField("keywords", "Keywords")
					->addStringField("description", "Description")
					->addStringField("url", "URL")
					->addTextField("text", "Описание");
		$typesTableID = $tableFactory->getTableId();
		
		$sql = "
		CREATE TABLE IF NOT EXISTS `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`number` INT UNSIGNED NOT NULL DEFAULT 0, 
			`hidden` TINYINT(1) NOT NULL DEFAULT 0, 
			`title` VARCHAR(255), 
			`keywords` VARCHAR(255), 
			`description` VARCHAR(500),
			`url` VARCHAR(255),
			`annotation` VARCHAR(500), 
			`image` VARCHAR(255), 
			`site_url` VARCHAR(255), 
			`text` TEXT, 
			`client_name` VARCHAR(255), 
			`type_id` INT UNSIGNED DEFAULT NULL, 
			`date` DATE 
		)";
		$this->getDb()->query($sql);
		
		$tableFactory->addTable($tableName, $this->getTitle());
		$tableFactory->addStringField("title", "Название")
			->addStringField("keywords", "Keywords")
			->addStringField("description", "Description")
			->addStringField("url", "URL")
			->addTextField("annotation", "Краткое описание")
			->addImageField("image", "Картинка")
			->addTextField("text", "Описание")
			->addStringField("site_url", "URL сайта")
			->addStringField("client_name", "Название клиента")
			->addDescendantField("type_id", "Тип проекта", $typesTableID)
			->addDateField("date", "Дата");
					
		$tableID = $tableFactory->getTableId();
		
		$pageFactory = $this->getPageFactory();
		$pageID = $pageFactory->addPage($this->getTitle(), \PageCreator::SECTION_SITE, $tableID);
		
		$pageFactory->addPage("Типы проектов", \PageCreator::SECTION_SITE, $typesTableID, null, $pageID);
		
		//insert some data
		$types = array(
			array(
				'title' => 'Порталы', 
				'url' => 'portals'
			), 
			array(
					'title' => 'Интернет-магазины',
					'url' => 'shops'
			), 
			array(
					'title' => 'Баннеры',
					'url' => 'banners'
			), 
			array(
					'title' => 'Логотипы',
					'url' => 'logo'
			)
		);
		foreach ($types as $type) {
			$this->getDb()->insert($typesTableName, $type);
		}
		
		$this->saveTemplate("list", '
		{{projects}}
		<div class="some-project">
        <a class="pic" href="/dev/{{type_url}}/{{url}}/"><img src="{{image}}" alt="{{title}}"></a>
        <p>{{annotation}}</p>
		</div><!-- .some-project -->
		{{/projects}}
		');
		
		$this->installJS();
		
		return true;
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::remove()
	 */
	public function remove() {
		$tableName = $this->getTableName();
		$typesTableName = $this->getTypesTableName();
		
		$this->removeTable($tableName);
		$this->removeTable($typesTableName);
		$this->getDb()->query("DROP TABLE IF EXISTS `$tableName`");
		$this->getDb()->query("DROP TABLE IF EXISTS `$typesTableName`");
		$this->deleteTemplate("list-item");
		
		$this->removeJS();
		
		return true;
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getConfigFields()
	 */
	public function getConfigFields() {
		return array(
			'onpage' => array(
				'title' => 'Выводить по:', 
				'type' => 'int', 
				'default' => 5
			), 
			'order' => array(
				'title' => "Сортировка",
				'type' => "enum", 
				'values' => array(
					'number' => 'По номеру', 
					'date DESC' => 'По дате'
				), 
				'default' => 'number'
			)
		);
	}
	
	/**
	 * @return \Zend_Db_Select
	 */
	public function prepareSelect() {
		$select = $this->getDb()->select()
			->from(array('P' => $this->getTableName()))
			->join(array('T' => $this->getTypesTableName()), 'T.id = P.type_id', array('type_url' => 'url'))
			->where("P.hidden = 0");
		return $select;
	}
	
	public function processRows($rows) {
		if($rows) {
			require_once "models/Table/DbTableLight.php";
			$table = new \DbTableLight($this->getDb(), $this->getTableName());
			/*@var $imgField \ImageField */
			$imgField = $table->getField("image");
				
			foreach($rows as $key => $row) {
				$rows[$key]['image'] = $imgField->getURL($row);
			}
		}
		return $rows ? $rows : array();
	}
	
	public function getFresh($options, $content = null) {
		
		$onpage = isset($options['onpage']) ? $options['onpage'] : $this->getConfig("onpage");
		$order =  isset($options['order']) ? $options['order'] : $this->getConfig("order");
		$pageNumber = isset($options['p']) ? $options['p'] : 1;
		
		$withTotal = !isset($options['without_total'])  || !$options['without_total'];
		$asArray = isset($options['as_array']) ? $options['as_array'] : false;
		
		$template = $content ? $content : $this->getTemplate("list");
		
		$select = $this->prepareSelect();
		
		if($withTotal) {
			$totalSelect = clone $select;
			$totalSelect->reset(\Zend_Db_Select::COLUMNS);
			$totalSelect->columns(array("count" => "COUNT(*)"));
		}
		
		$select->order($order);
		$select->limit($onpage, ($pageNumber - 1) * $onpage);
		
		$rows = $this->getDb()->fetchAll($select);
		$rows = $this->processRows($rows);
		$html = $this->getDi()->get("lex")->parse($template, array("projects" => $rows));
		
		if($withTotal) {
			$total = $this->getDb()->fetchOne($totalSelect);
			$pagesCount = ceil($total / $onpage);
			$pages = array();
			for($i = 0; $i < $pagesCount; $i++) {
				$number = $i + 1;
				if($number == $pagesCount) {
					$onThisPage = $total - ($pagesCount - 1) * $onpage;
				} else {
					$onThisPage = $onpage;
				}
				$pages[] = $onThisPage;
			}
			
			$html .= '<script>
			var portfolio = new Module_Portfolio();
			portfolio.setPages(['.implode(', ', $pages).']);
			portfolio.setOnpage('.$onpage.');
			$(function () {
				portfolio.init();
			});
			</script>';
		}
		
		return $asArray ? $rows : $html;
	}
	
	public function getJS() {
		return "portfolio.js";
	}
	
	public function more() {
		
		$request = $this->getRequest();
		
		$onpage = $request->getQuery("onpage", $this->getConfig("onpage"));
		$order = $request->getQuery("order", $this->getConfig("order"));
		$pageNumber = $request->getQuery("p");
		
		$html = $this->getFresh(array(
			'onpage' => $onpage, 
			'order' => $order, 
			'p' => $pageNumber, 
			'without_total' => true
		));
		
		return $html;
	}
	
	public function route(\front\models\Page $page, $request) {
		$parts = $page->getChildURLParts();
		$partsLength = count($parts);
		if($partsLength == 1) {
			$projectName = $parts[0];
			
			$select = $this->prepareSelect();
			$select->where("P.url = ?", $projectName);
			$select->where("T.url = ?", $page->getName());
			$select->limit(1);
			
			$project = $this->getDb()->fetchRow($select);
			
			if($project) {
				
				$parentPage = new \front\models\Page($page->getParent());
				$parentPage->setDi($this->getDi());
				
				$projectPage = $parentPage->findChildPage("single");
				
				if($projectPage) {
					$projectPage['title'] = $project['title'];
					$projectPage['keywords'] = $project['keywords'];
					$projectPage['description'] = $project['description'];
					$projectPage['menu_name'] = $project['title'];
					$projectPage['project'] = $project;
					
					return $projectPage;
				} else {
					throw new \Exception("Не найдена подстраница single для отображения проекта");
				}
			}
		}
	}


}

?>