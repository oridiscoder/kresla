<?php
namespace front\modules\Form\Forms;
use front\modules\Form\FormData;
use front\modules\Form\Validators\Validator;
require_once ('Form.php');

class Standart extends Form {	
	
	public function validate(FormData $dataObject) {
		$data = $dataObject->getUserData();
		$this->setData($data);
		
		$fields = $this->getFieldsToValidate();
		
		foreach($fields as $fieldName => $field) {
			
			$fieldValue = $data[$fieldName];		//значение поля, введённое пользователем
			$fieldAttr = $field['attr'];			//свойства поля, например title="Ваше имя"
			
			foreach($field['validators'] as $validator) {
				if(is_array($validator)) {
					$validatorName = $validator['name'];
					$validatorParams = $validator['params'];
				} else {
					$validatorName = $validator;
					$validatorParams = array();
				}
					
				$validator = Validator::factory($validatorName);
				$validator->setParams($validatorParams);
				if(!$validator->isValid($fieldValue)) {
					/* если значение не прошло один из валидаторов, то не надо проверять его дальше */
					$fieldTitle = isset($fieldAttr['title']) ? $fieldAttr['title'] : $fieldName;
					$this->addError($fieldName, $fieldTitle.':'.$validator->getFirstError());
					break;
				}
			}
		}
	}

	public function success(FormData $dataObject) {
		$this->mailFormData($dataObject);
		$this->saveFormData($dataObject);
	}
	
	public function mailFormData(FormData $dataObject) {
		/* отправить по почте */
		$mailTo = $dataObject->getSysData("mailto");
		$mailSubject = $dataObject->getSysData("mail_subject");
		$mailTemplate = $dataObject->getSysData("mail_template");
		$mailHTML = null;
		
		/* @var $logger Logger */
		$logger = $this->getDi()->get("logger");
		
		if($mailTemplate) {
			$mailData = array();
			foreach($dataObject->getUserData() as $key => $value) {
				$mailData[] = array(
						'key' => htmlspecialchars($key),
						'value' => htmlspecialchars($value)
				);
			}
			$mailHTML = $this->getDi()->get("lex")->parse($mailTemplate, array('data' => $mailData));
			
			$dataObject->addSysData("mail_html", $mailHTML);
				
			if($mailTo) {
				$mailer = $this->getDi()->get("mailer");
				if($mailer->mail($mailTo, $mailSubject, $mailHTML)) {
					$logger->log("Form_Standart:Email has been sent to $mailTo. Form #".$this->getFormAttributes("id"));
				} else {
					$logger->log("Form_Standart:Can't send email to $mailTo Form #".$this->getFormAttributes("id"));
				}
			} else {
				$logger->log("Form_Standart:No email address specified to send form #".$this->getFormAttributes("id"));
			}
				
		} else {
			$logger->log("Form_Standart:No mail template has been set for #".$this->getFormAttributes("id"));
		}
	}
	
	public function saveFormData(FormData $dataObject) {
		/* @var $logger Logger */
		$logger = $this->getDi()->get("logger");
		
		if(($tableName = $dataObject->getSysData("table_name")) != null) {
			$data = array(
					'form_id' => $this->getFormAttributes("id"),
					'date' => date('Y-m-d H:i:s'),
					"data" => serialize($dataObject->getUserData()),
					'data_html' => $dataObject->getSysData("mail_html")
			);
			$this->getDb()->insert($tableName, $data);
		} else {
			$logger->log("Form_Standart: no table name has been specified to save form data");
		}
	}
}

?>