<?php
namespace front\modules\Form\Forms;

use front\modules\Form\FormData;

require_once 'OridisStandart.php';

class Anketa extends OridisStandart {
	
	public function success(FormData $dataObject) {
		$data = $dataObject->getUserData();
		
		$dom = new \DOMDocument("1.0", "UTF-8");
		$doc = $dom->createElement("doc");

		$fields = $this->getFields();
		
		foreach($data as $name => $value) {
			if(isset($fields[$name])) {
				
				if(isset($fields[$name]['type'])  &&  $fields[$name]['type'] == 'checkbox' && $value == 'on') {
					$value = 1;
				}
				$item = $dom->createElement("item", $value);
				$item->setAttribute("name", $name);
				$doc->appendChild($item);
			}
		}
		
		$dom->appendChild($doc);
		$xml = $dom->saveXML();
		
		try {
			$status = $this->saveRemote($this->getBOFUrl(), $xml);
			
			if($status != "OK") {
				throw new BofException("Something wron on bof. I wanted 'OK', got ".substr($status, 0, 127));
			}
		} catch (BofException $ex) {
			$this->getDi()->get("logger")->log($ex->getMessage());
			parent::success($dataObject);
		}
	}
	
	public function getBOFUrl() {
		return "http://bof5.oridis.ru/api/anketa";
	}
}

?>