<?php
namespace front\modules\Form\Forms;

require_once ('Standart.php');

use front\modules\Form\FormData;

class ManagerCall extends Standart {
	
	public function validate(FormData $dataObject) {
		
		$this->setData($dataObject->getUserData());
		
		$valid = true;
		if($dataObject->getUserData("company_name") == "") {
			$this->addError("company_name", "Укажите, пожалуйста, название Вашей организации.");
			$valid = false;
		}
		
		if($dataObject->getUserData("name") == "") {
			$this->addError("name", "Укажите контактное лицо.");
			$valid = false;
		}
		
		if($dataObject->getUserData("phone") == "") {
			$this->addError("phone", "Укажите контактный номер телефона.");
			$valid = false;
		}
		
		if($dataObject->getUserData("email") == "") {
			$this->addError("email", "Укажите контактный E-Mail.");
			$valid = false;
		}
		
		if($dataObject->getUserData("address") == "") {
			$this->addError("address", "Укажите адрес Вашей организации");
			$valid = false;
		}
		
		return $valid;
	}
	
	public function success(FormData $dataObject) {
	
		$logger = $this->getDi()->get("logger");
		
		$template = $this->getModule()->getTemplate("mail.managercall");
		$logger->log("Отправка формы ".$this->getFormAttributes("id"));
		
		if($template) {
			$mailData = $this->escapeDataForHTML($dataObject->getUserData());
			$mailHTML = $this->getDi()->get("lex")->parse($template, $mailData);
				
			$dataObject->addSysData("mail_html", $mailHTML);
			
			$email = $dataObject->getSysData("mailto");
			
			$mailer = $this->getMailer();
			$success = $mailer->mail($email, "Вызов менеджера. www.kresla-otido.ru", $mailHTML);
			
			$logger->log($success ? "Сообщение успешно отправлено на $email" : "Невозможно отправить сообщение на $email");
		} else {
			$logger->log("Не найден шаблон mail.managercall");
		}
		$this->saveData($dataObject);
	}
	
	public function saveData(FormData $dataObject) {
	
		$logger = $this->getDi()->get("logger");
	
		if(($tableName = $dataObject->getSysData("table_name")) != null) {
			$data = array(
					'form_id' => $this->getFormAttributes("id"),
					'date' => date('Y-m-d H:i:s'),
					"data" => serialize($dataObject->getUserData()),
					'data_html' => $dataObject->getSysData("mail_html")
			);
			$this->getDb()->insert($tableName, $data);
		} else {
			$logger->log("Form_ManagerCall: no table name has been specified to save form data");
		}
	}
}

?>