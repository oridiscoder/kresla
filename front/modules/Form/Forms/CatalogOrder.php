<?php
namespace front\modules\Form\Forms;

use front\modules\Form\FormData;

require_once ('front/modules/Form/Forms/Standart.php');


class CatalogOrder extends Standart {
	
	public function success(FormData $dataObject) {
		
		/* формы не понимают записи вида name="interests[name]" */
		$data = $_POST;
		
		$mail = '';
		$mail .= 'Конт.лицо:      '.$data['contactperson']."\n";
		$mail .= 'Пол:            '.(empty($data['sex'])?'Жен':'Муж')."\n";
		$mail .= 'Компания:       '.$data['organization']."\n";
		$mail .= 'Отдел,должность:'.$data['department']."\n";
		$mail .= 'Адрес:          '.$data['zipcode'].' '.$data['address']."\n";
		$mail .= 'Телефон:        '.$data['phone']."\n";
		$mail .= 'Факс:           '.$data['fax']."\n";
		$mail .= 'e-mail:         '.$data['email']."\n";
	
		$mail .= 'Пост-й клиент:  '.(empty($data['permanent_client'])?'Нет':'Да')."\n";
		$mail .= 'Посещал салон:  '.(empty($data['visited'])?'Нет':'Да')."\n";
	
		//что клиента интересует
		/*$mail .= 'Интересует:     ';
		foreach ($data['interests'] as $row) {
			$mail .= $row.', ';
		}
		$mail .= "\n";*/
	
		$mail .= 'Ищет для:       ';
		foreach ($data['usegroup'] as $row) {
			$mail .= $row.', ';
		}
		$mail .= "\n";
	
		$mail .= 'Гл.критерий выбора:'.$data['guidline']."\n\n";
		//$mail .= 'Пожелания:      '.$data['wishes']."\n";
	
		//$mail .= 'Информативность:'.$data['self_descriptiveness']."\n";
		$mail .= 'Навигация:      '.$data['navigation']."\n";
		$mail .= 'Дизайн :        '.$data['design']."\n";
	
		$mail .= 'Откуда узнали:  '.$data['source_information']['strict'].', '.$data['source_information']['other']."\n";
		
		$mail = nl2br($mail);
		
		$email = $dataObject->getSysData("mailto");
		
		$this->getMailer()->mail($email, "Заказ каталога", $mail);
		
	}

}

?>