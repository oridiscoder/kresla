<?php
namespace front\modules\Form\Forms;
use front\modules\BasketKresla\models\DeliveryTypes;

use front\models\Orm;

use front\modules\Form\FormData;
use \front\modules\CatalogKresla\models\Good;

require_once ('Standart.php');

class KreslaOrder extends Standart {
	
	public function validate(FormData $dataObject) {
		$success = true;
		
		$name = $dataObject->getUserData("client_name");
		if(empty($name)) {
			$this->addError("client_name", "Представьтесь, пожалуйста.");
			$success = false;
		}
		
		$email = $dataObject->getUserData("client_email");
		$phone = $dataObject->getUserData("client_phone_number");
		$cellphone =  $dataObject->getUserData("client_cellphone");
		
		if(empty($email) && empty($phone) && empty($cellphone)) {
			$this->addError("email", "Вы должны указать E-Mail, Телефон, или Моб.Телефон, чтоб мы могли с вами связаться");
			$success = false;
		}
		
		return $success;
	}
	
	public function success(FormData $dataObject) {
		
		/* @var $logger \front\models\Logger */
		$logger = $this->getDi()->get("logger");
		
		$ordersTablename = $dataObject->getSysData("orders_table_name");
		$orderGoodsTablename = $dataObject->getSysData("orders_goods_table_name");
		$goodUpholsteriesTableName = $dataObject->getSysData("good_upholsteries_table_name");
		$id = $dataObject->getSysData("order_id");
		
		/* К заказу можно прикрепить файл */
		if(isset($_FILES['attach']) && $_FILES['attach']['error'] == UPLOAD_ERR_OK) {
			$filename = $_FILES['attach']['name'];
			$filename = iconv("utf-8", "cp1251", $filename);
			$filename = $this->escapeFilename($filename);
			
			$ext = strtolower(substr($filename, strrpos($filename, ".") + 1));
			
			$denyExtensions = array("php", "html", "htm", "js", "php5", "php4", "htaccess");
			
			if(!in_array($ext, $denyExtensions)) {
				$path = $_SERVER['DOCUMENT_ROOT'].'/cms/cms-files/'.$ordersTablename;
				if(!file_exists($path)) {
					mkdir($path);
				}
					
				move_uploaded_file($_FILES['attach']['tmp_name'], $path . '/' . $id . '_file_' . $filename);
			}
		} else {
			$filename = null;
		}
		
		$order = array(
			'delivery_type_id' => $dataObject->getUserData("delivery_type_id"), 
			'payment_type_id' => $dataObject->getUserData("payment_type_id"),
			'client_type' => $dataObject->getUserData("client_type"),
			'client_name' => $dataObject->getUserData("client_name"),
			'client_email' => $dataObject->getUserData("client_email"),
			'client_phone_number' => $dataObject->getUserData("client_phone_code") . $dataObject->getUserData("client_phone_number"),
			'client_cellphone' => $dataObject->getUserData("client_cellphone"),
			'delivery_address' => $dataObject->getUserData("delivery_address"),
			'company_name' => $dataObject->getUserData("company_name"),
			'company_inn' => $dataObject->getUserData("company_inn"),
			'company_kpp' => $dataObject->getUserData("company_kpp"),
			'comment' => $dataObject->getUserData("comment"),
			'file' => $filename,
			'complete' => true,
			'assembly' => $dataObject->getUserData("assembly"),
			'lifting' => $dataObject->getUserData("lifting")
		);
		
		$this->getDb()->update($ordersTablename, $order, "id = $id");
		
		$basket = $dataObject->getSysData("basket");

		/* Сформируем массив где каждая строчка - это строчка в корзине
		 * с полями good_id, upholstery_id, count, price 
		 * */
		$basketGoods = array();
		$mailGoods = array();		//сформируем отдельный массив с товарами для писем
		$totalCost = 0;
		
		$number = 1 + (int) $this->getDb()->fetchOne("SELECT MAX(number) FROM $orderGoodsTablename");
		foreach($basket as $item) {
			if($item['count']) {
				$good = new Good($item['id']);
				$good->setActiveUpholstery($item['upholstery_id']);
				$good->setActiveArm($item['arm_id']);
				
				$basketGoods[] = array(
					'order_id' => $id,
					'good_id' => $good->getID(),
					'upholstery_id' => $item['upholstery_id'],
					'arm_id' => $item['arm_id'],
					'count' => $item['count'],
					'price' => $good->getPriceActive(),
					'number' => $number
				);
				
				$mailGood = $good->asArray();
				$mailGood['count'] = $item['count'];
				$mailGood['price'] = $good->getPriceActive();
				$mailGood['cost'] = $good->getPriceActive() * $item['count'];
				$mailGood['upholstery'] = $good->getActiveUpholstery();
				$mailGood['arm'] = $good->getActiveArm();
				$mailGoods[] = $mailGood;
				
				$totalCost += $mailGood['cost'];
				
				$number ++;
			}
		}
		
		foreach ($basketGoods as $good) {
			$this->getDb()->insert($orderGoodsTablename, $good);
		}
		
		
		require_once 'models/Table/DbTableCreator.php';
		require_once 'models/Table/DbTable.php';
		$factory = new \DbTableCreator($this->getDb());
		$ordersTable = $factory->getTableByName($ordersTablename);
		
		$ordersTable = new \DbTable($this->getDb(), $ordersTable['id']);
		$order = $ordersTable->getRecord($id);
		$order['delivery_type'] = $ordersTable->getFields("delivery_type_id")->extractValue($order);
		$order['payment_type'] = $ordersTable->getFields("payment_type_id")->extractValue($order);
		$order['goods_cost'] = $totalCost;
		
		$deliveryTypesTable = new DeliveryTypes();
		/* @var $deliveryType \front\modules\BasketKresla\models\DeliveryType */
		$deliveryType = $deliveryTypesTable->findFirst(array("id = ".$order['delivery_type_id']));
		$order['delivery_price'] = $deliveryType->getPrice($totalCost);
		$order['delivery_price_title'] = $deliveryType->getPriceTitle($totalCost);
		$order['promocode']  = $dataObject->getUserData("promocode");
		$order['total_cost'] = $order['goods_cost'] + $order['delivery_price'];
		
		//очеловечиваем вывод значений для "нужна сборка" и "подъем на этаж"
		if($order['assembly']) $order['assembly'] = 'Да';
		else $order['assembly'] = 'Нет';

		if($order['lifting']) $order['lifting'] = 'Да';
		else $order['lifting'] = 'Нет';

		//$mailGoods = Good::expandImagesUrls($mailGoods, "kresla_goods");
		
		$validator = new \Zend_Validate_EmailAddress();
		
		if($order['client_email']) {

			$email = $order['client_email'];
			if($validator->isValid($email)) {
				$logger->log("Отправляем письмо клиенту на адрес $email");
				$sentStatus = $this->mailOrder($order, $mailGoods, $email, "Заказ с сайта www.kresla-otido.ru", $dataObject->getSysData("client_mail_template"));
				$logger->log($sentStatus ? "Письмо успешно отправлено" : "Не отправлено");
			} else {
				$logger->log("Клиент указан невалидный email: $email");
			}
		}

		$systemEmail = $dataObject->getSysData("system_email");
		if($validator->isValid($systemEmail)) {
			$logger->log("Отправляю письмо о заказа администратору на $systemEmail");
			$sentStatus = $this->mailOrder($order, $mailGoods, $systemEmail, "Кресла От и До Поступил новый заказ № ".$order['name'], $dataObject->getSysData("manager_mail_template"));
			$logger->log($sentStatus ? "Письмо успешно отправлено" : "Не отправлено");
		}
		
		$systemEmail2 = $dataObject->getSysData("system_email2");
		if($validator->isValid($systemEmail2)) {
			$logger->log("Отправляю письмо о заказа администратору на $systemEmail2");
			$sentStatus = $this->mailOrder($order, $mailGoods, $systemEmail2, "Кресла От и До Поступил новый заказ № ".$order['name'], $dataObject->getSysData("manager_mail_template"));
			$logger->log($sentStatus ? "Письмо успешно отправлено" : "Не отправлено");
		}
				
	}
	
	private function mailOrder($order, $goods, $email, $subject, $template) {
		
		//предотвращаем XSS
		$order = $this->escapeFields($order);
		
		$html = $this->getDi()->get("lex")->parse($template, array("order" => $order, "goods" => $goods));
		$file_path= $_SERVER['DOCUMENT_ROOT']."/cms/cms-files/kresla_basket_orders/{$order['id']}_file_{$order['file']}";
		$mailer = $this->getDi()->get("mailer");
		
		if ($order['file'])
			$mailer->AddAttachment($file_path, $order['file']);
		return $mailer->mail($email, $subject, $html);
	}
	
	private function mailAdmin($order, $goods, $template) {
		
	}

	/**
	 * Экранируем поля массива для безопасного использования в HTML
	 * @param array $array
	 * @return array - исходный массив с экранированными полями
	 */
	private function escapeFields($array) {
		foreach ($array as $key => $value) {
			if(is_array($value)) {
				$array[$key] = $this->escapeFields($value);
			} else {
				$array[$key] = htmlspecialchars($value);
			}
		}
		return $array;
	}
	
	private function escapeFilename($filename) {
		require_once 'Bof/Utils.php';
		return \Utils::escapeFilename($filename);
	}
}