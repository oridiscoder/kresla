<?php
namespace front\modules\Form\Forms;
use front\modules\Form\FormData;

require_once 'Standart.php';

class OridisStandart extends Standart {

	public function success(FormData $dataObject) {
		$userData = $dataObject->getUserData();
		
		$moduleOptions = $dataObject->getSysData("module_options");
		
		$data = array(
			'name' => $dataObject->getUserData("name"), 
			'phone' => $dataObject->getUserData("phone"), 
			'email' => $dataObject->getUserData("email"), 
			'request_type' => isset($moduleOptions['request_type']) ? $moduleOptions['request_type'] : "", 
			'html' => $this->renderDataHTML($userData)
		);
		
		$xml = $this->saveDataAsXml($data);
		
		try {
			$this->saveRemote($this->getBOFUrl(), $xml);
		} catch (BofException $ex) {
			$this->getDi()->get("logger")->log("Can't save form to BOF: ".$ex->getMessage());
			$dataObject->addSysData("mail_html", $data['html']);
			$this->saveFormData($dataObject);
		}
	}
	
	protected function renderDataHTML($userData) {
		$html = '<table>{items}</table>';
		$itemsHTML = "";
		foreach ($userData as $fieldName => $fieldValue) {
			$field = $this->getFields($fieldName);
			if($field) {
				$title = isset($field['title']) ? $field['title'] : $fieldName;
		
				$itemsHTML .= '<tr><td>'.htmlspecialchars($title).'</td><td>'.htmlspecialchars($fieldValue).'</td></tr>';
			}
		}
		
		$html = str_replace('{items}', $itemsHTML, $html);
		return $html;
	}
	
	protected function saveDataAsXml($data) {
		$doc = new \DOMDocument("1.0", "UTF-8");
		$rootEl = $doc->createElement("doc");
		
		foreach($data as $key => $value) {
			$el = $doc->createElement($key, $value);
			$rootEl->appendChild($el);
		}
		
		$doc->appendChild($rootEl);
		return $doc->saveXML();
	}
	
	public function getBOFUrl() {
		return "http://bof5.oridis.ru/api/oridis_form_recieve";
	}
	
	public function saveRemote($url, $xml) {
	
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	
		$content = curl_exec($ch);
		if(curl_errno($ch)) {
			$error = "Can't send data to bof. Curl error: ".curl_error($ch);
			curl_close($ch);
			throw new BofException($error);
		} else {
			curl_close($ch);
			return $content;
		}
	
	}
}

class BofException extends \Exception {

}

?>