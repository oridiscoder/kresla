<?php
namespace front\modules\Form\Forms;
use front\modules\Form\FormData;

require_once ('front/modules/Form/Forms/Standart.php');

class Comment extends Standart {
	
	public function success(FormData $dataObject) {
		/* лучше всего здесь реализовать поле вида Множественный Потомок */
		$commentsTableName = $dataObject->getSysData("comments_table_name");
		
		$data = array(
			"name" =>  $dataObject->getUserData("name"), 
			"date" => date("Y-m-d H:i:s"), 
			"text" =>  $dataObject->getUserData("text"),
			"email" =>  $dataObject->getUserData("email"),
			"object_id" => $dataObject->getSysData("object_id"), 
			"object_id__table" =>  $dataObject->getSysData("object_id__table") 
		);
		
		$this->getDb()->insert($commentsTableName, $data);
	}
}

?>