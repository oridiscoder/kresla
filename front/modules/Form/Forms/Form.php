<?php

namespace front\modules\Form\Forms;

use \front\models\Di, 
	\front\modules\Form\FormData;

abstract class Form {
	
	protected $formHTML;
	protected $formAttributes;
	protected $formFields;
	private $data;
	private $errors;
	
	private $injectInputHTML = '<input type="hidden" name="_form_id" value="{id}" />';
	/**
	 * 
	 * @var \front\modules\Form
	 */
	private $module;
	
	/**
	 * 
	 * @var Di
	 */
	private $di;
	
	public function __construct($formHTML) {
		$this->formAttributes = self::extractFormAttributes($formHTML);
		$formHTML = $this->injectFormId($this->formAttributes['id'], $formHTML);
		$this->formFields = self::extractFormFields($formHTML);
		$this->formHTML = $formHTML;
	}
	/**
	 * @return Di
	 */
	public function getDi() {
		return $this->di ? $this->di : Di::getInstance();
	}
	public function setDi($di) {
		$this->di = $di;
	}
	/**
	 * @return \Zend_Db_Adapter_Abstract
	 */
	public function getDb() {
		return $this->getDi()->get("db");
	}
	public function getData() {
		return $this->data;
	}
	public function setData($data) {
		$this->data = $data;
	}
	public function setModule($module) {
		$this->module = $module;
	}
	/**
	 * @return \front\modules\Form
	 */
	public function getModule() {
		return $this->module;
	}
	public function __toString() {
		return $this->formHTML;
	}
	
	/**
	 * Получить список аттрибутов текущей формы
	 * @param string $name - имя конкретного аттрибута
	 * @return array|string - список аттрибутов или значение конкретного аттрибута
	 */
	public function getFormAttributes($name = null) {
		if($name) {
			return isset($this->formAttributes[$name]) ? $this->formAttributes[$name] : null;
		} else {
			return $this->formAttributes;
		}
	}
	/**
	 * Получить список полей текущей формы
	 * @param string $name - можно задать имя конкретного поля
	 * @return array
	 */
	public function getFields($name = null) {
		if($name) {
			return isset($this->formFields[$name]) ? $this->formFields[$name] : null;
		} else {
			return $this->formFields;
		}		
	}
	
	/**
	 * Добавить ошибку в список ошибок валидации. 
	 * Этот метод должен использоваться в методе validate() для сохранения ошибок валидации
	 * @param string $fieldName - имя поля формы
	 * @param string $errorString - сообщение об ошибке
	 */
	public function addError($fieldName, $errorString) {
		$this->errors[$fieldName] = $errorString;
	}	
	public function getErrors() {
		return $this->errors;
	}
	public function setErrors($errors) {
		$this->errors = $errors;
	}
	/**
	 * Получит HTML код с ошибками валидации, который будет показан клиенту. 
	 * Не хочется лишний раз здесь использовать Lexer, потому HTML код я поместил
	 * прям в тело метода. Можно было бы использовать шаблоны модуля Form, но что, если 
	 * сама форма будет использоваться без модуля?
	 * @return string
	 */
	public function getErrorsAsHTML() {
		$errors = $this->getErrors();
		$html = null;
		if($errors) {
			$wrapper = '<div class="form_errors"><ul>{errors}</ul></div>';
			$itemTemplate = '<li>{error}</li>';
			foreach($errors as $error) {
				$html .= str_replace("{error}", $error, $itemTemplate);
			}
			
			$html = str_replace("{errors}", $html, $wrapper);
		}
	
		return $html;
	}
	
	/**
	 * Получить HTML код успешной отправки формы.
	 */
	public function getSuccessHTML() {
		return '<div class="form_success" id="'.$this->getFormAttributes('id').'">Ваше сообщение успешно отправлено</div>';
	}
	
	/**
	 * Получит список полей, которые надо проверить валидаторами
	 * @return array [ field_name => [attr => [...], validators => [...]] ]
	 */
	public function getFieldsToValidate() {
		$fields = $this->getFields();
		
		$fields2validate = array();
		
		foreach($fields as $fieldName => $fieldAttr) {
			if(isset($fieldAttr['data-validate'])) {
				$validators = self::extractValidatorsFromString($fieldAttr['data-validate']);						
				if($validators) {
					$fields2validate[$fieldName] = array(
							'attr' => $fieldAttr,
							'validators' => $validators
					);
				}
			}
		}
		
		return $fields2validate;
	}
	
	public function injectFormId($id, $content) {
		$input = str_replace('{id}', $id, $this->injectInputHTML);
		return preg_replace('#(<form[^>]+>)#im', '\\1'.$input, $content);
	}
	
	
	/**
	 * Проверить форму на валидность введённых пользователем данных. 
	 * Каждая форма должна реализовать метод проверки данных.
	 * На валидность формы влияет не возвращаемое данным методом значение, 
	 * а наличие ошибок при вызове Form::getErrors(), поэтому при реализации метода Form::validate
	 * необходимо использовать метод Form::addError(), если каое-то из полей не проходит валидацию
	 * 
	 * @param FormData $dataObject - объект данных
	 */
	abstract public function validate(FormData $dataObject);
	/**
	 * Метод Form::success() вызывается когда форма успешно прошла валидацию. 
	 * Каждая форма сама решает, что ей делать в случае успеха валидации, потому 
	 * данные метод необходимо реализовать в классах-потомках
	 * @param FormData $dataObject
	 */
	abstract public function success(FormData $dataObject);
	
	/**
	 * Форма устанавливает checked="checked" для checkbox и radio
	 * @param array $data - данные, введённые пользователем
	 * @return string - HTML формы
	 */
	public function processCheckedFields($data) {
		$content = $this->__toString();
		/* обработаем чекбоксы и радио отдельно */
		$globalOffset = 0;
		foreach($this->getFields() as $fieldName => $fieldParams) {
			$type = isset($fieldParams['type']) ? $fieldParams['type'] : null;
			if($type) {
		
				if($type == 'checkbox') {
						
					$startPosition = $fieldParams['_offset'] - 1 + $globalOffset;
					$endPosition = strpos($content, ">", $startPosition);
					$fieldLength = $endPosition - $startPosition;
					$fieldStr = substr($content, $startPosition, $fieldLength);
						
					$checked = isset($data[$fieldName]) && $data[$fieldName];
						
					$fieldStr = str_replace(array('checked="checked"', 'checked', "checked='checked'"), "", $fieldStr);
						
					if($checked) {
						$fieldStr .= ' checked="checked"';
					}
						
					$globalOffset += strlen($fieldStr) - $fieldLength;
						
					$content = substr_replace($content, $fieldStr, $startPosition, $fieldLength);
				}
		
				if($type == 'radio') {
					/* radio содержит поле _values где перечислены 2й и следующие input'ы с тем же именем */
					$radios = array($fieldParams);
					$radios = array_merge($radios, $fieldParams['_values']);
					unset($radios[0]['_values']);
						
					foreach ($radios as $fieldParams) {
							
						$startPosition = $fieldParams['_offset'] - 1 + $globalOffset;
						$endPosition = strpos($content, ">", $startPosition);
						$fieldLength = $endPosition - $startPosition;
						$fieldStr = substr($content, $startPosition, $fieldLength);
							
						$checked = isset($data[$fieldName]) && $data[$fieldName] == $fieldParams['value'];
							
						$fieldStr = str_replace(array('checked="checked"', 'checked', "checked='checked'"), "", $fieldStr);
							
						if($checked) {
							$fieldStr .= ' checked="checked"';
						}
							
						$globalOffset += strlen($fieldStr) - $fieldLength;
							
						$content = substr_replace($content, $fieldStr, $startPosition, $fieldLength);
		
					}
				}
			}
		}
		
		return $content;
	}

	/**
	 * Метод extractFormAttributes() парсит HTML код тега <form>, ищет в нем аттрибуты 
	 * и возвращает их в качестве pairs массива (key=value)
	 * @param string $formHTML
	 * @return array - ассоциативный массив, где ключи - названия аттрибутов, а значения - это значения аттрибутов
	 */
	public static function extractFormAttributes($formHTML) {
		$attr = array();
		if(preg_match('/<form\s*([^>]*)/m', $formHTML, $m)) {
			preg_match_all('/(\w+)="([^"]+)"/m', $m[1], $am);
			$attr = array_combine($am[1], $am[2]);
		}
		return $attr;
	}
	
	/**
	 * Метод extractFormFields() разбирает HTML код формы в поиске элементов ввода (полей формы)
	 * @param string $formHTML
	 * @return array - ассоциативный массив, где ключами являются названия полей (name="field_name"), а значениями - массив аттрибутов данного поля
	 */
	public static function extractFormFields($formHTML) {
		
		preg_match_all('#<(input|textarea|select)([^/>]+)/?>#', $formHTML, $matches, PREG_OFFSET_CAPTURE);
		
		$fields = array();
		foreach($matches[1] as $key => $capture) {
			$fieldParamsString = $matches[2][$key][0];
			$fieldOffset = $capture[1];
			preg_match_all('#(\s*([-\w]+)="([^"]+)"\s*)#', $fieldParamsString, $pm);
			$params = array_combine($pm[2], $pm[3]);
			if(isset($params['name'])) {
				$fieldName = $params['name'];
				$params['_offset'] = $fieldOffset;
				if(isset($fields[$fieldName])) {
					$fields[$fieldName]['_values'][] = $params; 
				} else {
					$fields[$params['name']] = $params;
				}
			}
		}
		
		return $fields;
	}
	
	/**
	 * Метод extractValidatorsFromString разбирает заданную строку в поиске перечисленных в ней валидаторов
	 *  
	 * @param string $string - строка вида "require&length[5-10]&nonexists[users,id]"
	 * @return array - массив вида ['require', []], ['length', ['5-10']], ['require', ['users', 'id']]
	 */
	public static function extractValidatorsFromString($string) {
		$validatos = array();
		if(!empty($string)) {
			$items = explode("&", $string);
			foreach($items as $item) {
				if(preg_match('/^([-\w]+)\[(.*?)\]$/', $item, $m)) {
					$validatos[] = array(
							'name' => $m[1],
							'params' => explode(",", $m[2])
					);
				} else {
					$validatos[] = $item;
				}
			}
		}
	
		return $validatos;
	}
	
	/**
	 * @return \front\models\Mailer
	 */
	protected function getMailer() {
		return $this->getDi()->get("mailer");
	}
	
	/**
	 * Подготовить данные для вставки в HTML
	 * @param array $rows
	 * @return array
	 */
	public function escapeDataForHTML($rows) {
		foreach ($rows as $key => $value) {
			if(is_array($value)) {
				$rows[$key] = $this->escapeDataForHTML($value);
			} else {
				$rows[$key] = htmlspecialchars($value);
			}
		}
		return $rows;
	}
}

?>