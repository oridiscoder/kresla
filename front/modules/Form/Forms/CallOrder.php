<?php
namespace front\modules\Form\Forms;

require_once ('OridisStandart.php');

class CallOrder extends OridisStandart {
	
	private $jsCode =  '<script>$(function () {$(\'a.order-call\').click();});</script>';
	
	public function getSuccessHTML() {
		return 'Ваше сообщение успешно отправлено!'.$this->jsCode;
	}
	
	public function getErrorsAsHTML() {
		$html = parent::getErrorsAsHTML();
		return $html . $this->jsCode;
	}
}

?>