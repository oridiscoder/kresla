var Form = function (formObject, options) {
	this.form = formObject;
	this.validator = new _validator({
        form: formObject
    });
	this.ajax = false;
	if(options != undefined) {
		if(options.ajax) {
			this.ajax = options.ajax == true;
		}
		if(options.containerSuccess) {
			this.containerSuccess = options.containerSuccess;
		}
		if(options.containerError) {
			this.containerError = options.containerError;
		}
	}
	this.containerSuccess = null;
	this.containerError = null;
	
	
	this.submit = function () {
		this.validator.touchFields();
		this.validator.validate();
		if(this.validator.isValid()) {
			if(this.ajax) {
				this.ajaxSubmit();
			} else {
				this.form.submit();
			}
		}
	};
	
	this.ajaxSubmit = function () {
		var form = $(this.formObject);
		var data = form.serialize();
		$.ajax({
			url: form.attr("action"), 
			type: form.attr("method"), 
			success: function (html) {
				try {
					var data = eval('(' + html + ')');
					if(data.errors) {
						this.showErrors(data.errors);
					} else {
						this.showSuccess();
					}
				} catch (ex) {
					alert("Произошла ошибка на сервере");
				}
			}
		});
	};
	
	this.showErrors = function (errors) {
		if(this.containerError) {
			for(fieldName in errors) {
				this.validator.showHint(fieldName, "server-error", errors[fieldName]);
			}
		}
	};
	
	this.showSuccess = function () {
		if(this.containerSuccess) {
			$(this.form).hide();
			$(this.containerSuccess).show();
		}
	};
}