/// Oridis SuperValidator
/// By Brusher ( brusher.designer@gmail.com )
/// Need to include EBSdetect: http://jsfiddle.net/brusher/y8DE5/
var client=function(){var e={ie:0,gecko:0,webkit:0,khtml:0,opera:0,ver:null};var t={ie:0,firefox:0,safari:0,konq:0,opera:0,chrome:0,safari:0,ver:null};var n={win:false,mac:false,x11:false,iphone:false,ipod:false,nokiaN:false,winMobile:false,macMobile:false,wii:false,ps:false};var r=navigator.userAgent;if(window.opera){e.ver=t.ver=window.opera.version();e.opera=t.opera=parseFloat(e.ver)}else if(/AppleWebKit\/(\S+)/.test(r)){e.ver=RegExp["$1"];e.webkit=parseFloat(e.ver);if(/Chrome\/(\S+)/.test(r)){t.ver=RegExp["$1"];t.chrome=parseFloat(t.ver)}else if(/Version\/(\S+)/.test(r)){t.ver=RegExp["$1"];t.safari=parseFloat(t.ver)}else{var i=1;if(e.webkit<100){i=1}else if(e.webkit<312){i=1.2}else if(e.webkit<412){i=1.3}else{i=2}t.safari=t.ver=i}}else if(/KHTML\/(\S+)/.test(r)||/Konqueror\/([^;]+)/.test(r)){e.ver=t.ver=RegExp["$1"];e.khtml=t.konq=parseFloat(e.ver)}else if(/rv:([^\)]+)\) Gecko\/\d{8}/.test(r)){e.ver=RegExp["$1"];e.gecko=parseFloat(e.ver);if(/Firefox\/(\S+)/.test(r)){t.ver=RegExp["$1"];t.firefox=parseFloat(t.ver)}}else if(/MSIE ([^;]+)/.test(r)){e.ver=t.ver=RegExp["$1"];e.ie=t.ie=parseFloat(e.ver)}t.ie=e.ie;t.opera=e.opera;var s=navigator.platform;n.win=s.indexOf("Win")==0;n.mac=s.indexOf("Mac")==0;n.x11=s=="X11"||s.indexOf("Linux")==0;if(n.win){if(/Win(?:dows )?([^do]{2})\s?(\d+\.\d+)?/.test(r)){if(RegExp["$1"]=="NT"){switch(RegExp["$2"]){case"5.0":n.win="2000";break;case"5.1":n.win="XP";break;case"6.0":n.win="Vista";break;default:n.win="NT";break}}else if(RegExp["$1"]=="9x"){n.win="ME"}else{n.win=RegExp["$1"]}}}n.iphone=r.indexOf("iPhone")>-1;n.ipod=r.indexOf("iPod")>-1;n.nokiaN=r.indexOf("NokiaN")>-1;n.winMobile=n.win=="CE";n.macMobile=n.iphone||n.ipod;n.wii=r.indexOf("Wii")>-1;n.ps=/playstation/i.test(r);return{engine:e,browser:t,system:n}}()
/// http://jsfiddle.net/brusher/gzq4G/

//window.debug = true;
//Дебаг
function log() {
    if(window.debug && arguments.length>0) {
        for(var i=0;i<arguments.length;i++) {
            console.log(arguments[i]);
        }
    }
}

//Функция плавного скролла
function scrollTo(to, duration, callback) {
    if (duration < 10) {
        if(typeof callback == "function") {
            callback();
        }
        return false;
    }
    var y = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    var difference = to - y;
    var perTick = difference / duration * 10;

    setTimeout(function() {
        window.scrollTo(0, y + perTick);
        scrollTo(to, duration - 10, callback);
    }, 10);
};


//Считаем кол-во элементов в объекте
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

//Кроссбраузерные и удобные ивенты
HTMLElement.prototype.addEvent = function (type, handler) {
  var type = type.split(" "),
      elem = this;
    
    for(var i=0;i<type.length;i++) {
      if (elem.addEventListener){
        elem.addEventListener(type[i], handler, false);
      } else {
        elem.attachEvent("on"+type[i], handler);
      }
    }
    
};
HTMLElement.prototype.removeEvent = function (type, handler) {
  var type = type.split(" "),
      elem = this;
    for(var i=0;i<type.length;i++) {
      if (elem.removeEventListener){
        elem.removeEventListener(type[i], handler, false);
      } else {
        elem.dettachEvent("on"+type[i], handler);
      }
    }
};

//Returns true if it is a DOM element    
function isElement(o){
  return (
    typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
);
};

var _validator = function(formObject) {
    var validator = this;
    this.form = {};
    this.hints =  [];           //Хранилище подсказок
    this.hintClass = "hint";    //Дефолтный класс для подсказок
    this.errClass  = "error";   //Дефолтный класс для ошибок
    this.fields = {};           //Хранилище полей
    this.active = true;         //Форма по-дефолту активна
    this.submitBtn = {};        //Ссылка на кнопку Сабмита

    this.Events = {       //События валидатора
        formValid:        function() {}, //Если форма прошла валидацию
        formInvalid:      function() {}, //Если форма не прошла валидацию
        formSubmit:       function() {}, //На сабмите формы
        fieldValid:       function() {}, //Вызывается на каждом поле, которое прошло валидацию
        fieldInvalid:     function() {}, //Вызывается на каждом поле, которое не прошло валидацию
        fieldTouched:     function() {}, //Вызывается на каждом поле 1 раз, когда человек сделал BLUR или KEYDOWN
        inited:           function() {}, //Вызывается после инициализации объекта
        validatorAdded:   function() {}, //Вызывается при добавлении нового валидатора
        validatorRemoved: function() {}, //Вызывается при удалении валидатора
        conditionAdded:   function() {}, //Вызывается при добавлении условия к полю
        conditionRemoved: function() {}  //Вызывается при удалении условия у поля
    };

    if( isElement(arguments[0]) ) {
        this.form = arguments[0];
    }
    if( typeof arguments[1] == "string" && arguments[1].length>1 ) {
        this.hintClass = arguments[1];
    }
    if( typeof arguments[2] == "string" && arguments[2].length>1 ) {
        this.errClass = arguments[2];
    }
    if( typeof arguments[3] == "object" && Object.size(arguments[3])>0 ) {
        for(var key in arguments[3]) {
            this.Events[key] = arguments[3][key];
        }
    }
    if( typeof arguments[0] == "object" && Object.size(arguments[0])>0 ) {
        for(var key in arguments[0]) {

            if(key == "form" || key == "formObject") { this.form = arguments[0][key]; }
            if(key == "hintClass" || key == "hints") { this.hintClass = arguments[0][key]; }
            if(key == "errClass" || key == "errorClass" || key == "errors") { this.errClass = arguments[0][key]; }
            if((key == "events" || key == "Events") && Object.size(arguments[0][key])>0) { 
                for(var kkey in arguments[0][key]) {
                    this.Events[kkey] = arguments[0][key][kkey];
                }
            }
            if(key == "onFormValid") { this.Events.formValid = arguments[0][key]; }
            if(key == "onFormInalid") { this.Events.formInvalid = arguments[0][key]; }
            if(key == "submit") { this.Events.formSubmit = arguments[0][key]; }
            if(key == "onSubmit") { this.Events.formSubmit = arguments[0][key]; }
            if(key == "onFormSubmit") { this.Events.formSubmit = arguments[0][key]; }
            if(key == "onFieldValid") { this.Events.fieldValid = arguments[0][key]; }
            if(key == "onFeldInvalid") { this.Events.fieldInvalid = arguments[0][key]; }
            if(key == "onFieldTouched") { this.Events.fieldTouched = arguments[0][key]; }
            if(key == "onInit") { this.Events.inited = arguments[0][key]; }
            if(key == "onValidatorAdded") { this.Events.validatorAdded = arguments[0][key]; }
            if(key == "onValidatorRemoved") { this.Events.validatorRemoved = arguments[0][key]; }
            if(key == "onConditionAdded") { this.Events.conditionAdded = arguments[0][key]; }
            if(key == "onConditionRemoved") { this.Events.conditionRemoved = arguments[0][key]; }
        }
    }

    this.preventSubmit = function(e) {
        e.preventDefault();
        return false;
    };
    this.showError = function(e) {
        console.log("!");
        var f = validator.fields;

        for(var k in f) {
            f[k].touched = true;
        }
        var err = validator.validate();
        if(err) {
            //Двигаем window.scroll к инпуту с ошибкой
            var y = err.object.link.offsetTop;
            scrollTo(y, 500);
        }
    }
    
    //Модель групп
    this.groups = {
        childs:         {},
        add:    function( name, object) {
            if(!this.childs[name]) {
                this.childs[name] = [];
                this.childs[name].push(object);
            } else {
                this.childs[name].push(object);
            }
            return this.childs[name];
        },
        remove:    function( name ) {
            if(this.childs[name]) {
                delete this.childs[name];
            } else return false;
        },
        read: function( name ) {
            return (this.childs[name])? this.childs[name] : false;
        },
        list: function() {
            log("List of the groups: ", this.childs);
            return this.childs;
        }
    };
    
    //Прототип подсказки
    this._hint = function(Obj) {
        var hint = this;
        this.link = Obj;
        this.visible = false;
        this.style = (this.link.currentStyle) ? 
                      this.link.currentStyle.display : 
                      getComputedStyle(this.link, null).display;
        
        if(!this.link.getAttribute("data-for")) {
            return {};
        } else {
            this.for = this.link.getAttribute("data-for");
                this.name = this.for;
            if (this.link.getAttribute("data-type")) {
                this.type = this.link.getAttribute("data-type");
                this.name = this.for+"_"+this.type;
            }
        }
        
        this.link.style.display = "none";
    };
    this._hint.prototype.show = function() {
        this.link.style.display = this.style;
        this.visible = true;
    };
    this._hint.prototype.hide = function() {
        this.link.style.display = "none";
        this.visible = false;
    };
    this._hint.prototype.toggle = function() {
        if(this.visible) {
            this.hide();
        } else {
            this.show();
        }
    };
    //Прототип поля
    this._field = function() {
        var field = this;
        this.name = ''; //Имя поля
        this.type = ''; //Тип поля
        this.link = {}; //Ссылка на HTMLDOMelement
        this.touched = false; //Трогал ли человек это поле
        this.conditions = { //Объект с проверками, берется из data-validate
            required: false
        }; 
        
        if( typeof arguments[0] == "object" && !isElement(arguments[0]) ) {
            for( var k in arguments[0] ) {
                this[k] = arguments[0][k];
                if(k == "link" ) {
                    if(arguments[0][k].type == "radio" ) {
                        this.group = validator.groups.add(arguments[0][k].name, this);
                    }
                    if(arguments[0][k].type == "password" ) {
                        this.group = validator.groups.add("password", this);
                    }
                    this.type = (arguments[0][k].tagName=="INPUT")? arguments[0][k].type : arguments[0][k].tagName;
                }
            }
        } else if ( isElement(arguments[0]) ) {
            this.link = arguments[0];
            this.type = (this.link.tagName=="INPUT")? this.link.type : this.link.tagName;
            if(this.link.name) {
                this.name = this.link.name;
            } else if(this.link.id) {
                this.name = this.link.id;
            } else {
                this.name = (this.link.tagName=="INPUT")? this.link.type : this.link.tagName;
            }            
            if(this.link.type == "radio" ) {
                this.group = validator.groups.add(this.link.name, this);
            }
            if(this.link.type == "password" ) {
                this.group = validator.groups.add("password", this);
            }
        }
        
        //Ивент «потрогали» поле
        var touched = function(event) {
            var type = event.type;
            if(type=="blur" || type=="keydown") {
                type = "blur keydown";
            }
            field.touched = true;
            field.link.removeEvent(type, arguments.callee);
            validator.Events.fieldTouched.call(this);
        };
        if(this.link.type =="checkbox" || 
           this.link.type =="radio"    || 
           this.link.type =="file"     || 
           this.link.tagName == "SELECT"
        ) {
              this.link.addEvent("change", touched);
        } else {
              this.link.addEvent("blur keydown", touched);    
        }
        
        //Определяем кондишены
        if(this.link.getAttribute("data-validate")) {
            var dataValidate = this.link.getAttribute("data-validate").split("&");
            for(var dVi=0; dVi<dataValidate.length; dVi++) {
                this.parseCondition(dataValidate[dVi]);
            }
        }
    };
    
    //Валидатор range (кол-во символов)
    this._field.prototype._range = function(input) {
        var range = input.split("-"),
            val = this.link.value,
            res = false,
            rangeSet = [false, false];
        
        if( range[0] !== "" && range[0] !== undefined ) {
            rangeSet[0] = true;
        }
        if( range[1] !== "" && range[1] !== undefined ) {
            rangeSet[1] = true;
        }
        
        if(  rangeSet[0] && rangeSet[1] ) {
            // range[5-10]
            res = (val.length>=range[0] && val.length<=range[1]) ? true : false;
        } else if (!rangeSet[0] && rangeSet[1]) {
            // range[-10]
            res = (val.length<=range[1]) ? true : false;
        } else if ( !rangeSet[0] && !rangeSet[1] ) {
            // range[] или range[-]
            res = (val.length>0) ? true : false;
        } else if (rangeSet[0] && !rangeSet[1] ) {
            // range [10] или range[10-]
            res = (val.length>=range[0]) ? true : false;
        }
            return res;
    };
    //Валидатор _required
    this._field.prototype._required = function(input) {
        if(this.conditions.required) {
        if( //Тестовое поле
            this.link.type    ==    "text"      || 
            this.link.type    ==    "email"     || 
            this.link.type    ==    "tel"       ||
            this.link.type    ==    "url"       || 
           (this.link.type    ==    "password" && this.group.length==1) || 
            this.link.tagName ==    "TEXTAREA"  
        ) {
            var val = this.link.value;
            return (val.length>0) ? true : false;
        } else if ( //Чекбокс
            this.link.type    ==    "checkbox"
        ) {
            return (this.link.checked===true) ? true : false;
        } else if ( //Радиобаттон
            this.link.type    ==    "radio"
        ) {
            var name = this.link.name;
            var group = this.group;
            var result = false;
            var reqfields = 0,
                checked   = 0;
            for(i=0; i<group.length; i++) {
                var thisgroup = group[i];
                if( thisgroup.conditions.required ) {
                    reqfields++;
                    if (thisgroup.link.checked) checked++;
                }
            }
            if( (reqfields>0 && checked>0) || reqfields==0 ) {
                return true;
            } else {
                return false;
            }
        } else if( //Селект
            this.link.tagName    ==    "SELECT"
        ) {
            var options = this.link.options;
            var selected = options.selectedIndex;
            //А еще проверить если 
            return (!options[selected].disabled)? true : false;
        } else if( //file
            this.type == "file"
        ) {
            if(typeof input == "string") {
                var path = this.link.value.split("\\");
                var extension = path[path.length-1].split('.').pop();
                var allowed = input.split(",");
                return (allowed.indexOf(extension) != -1)? true : false;
            } else return true;
        } else if( //password confirmation (group)
            this.link.type == "password" && this.group.length>1
        ) {
            if(this.link !== this.group[0].link) {
                var val    = this.link.value,
                    valids = 0;
                for(i=0;i<this.group.length;i++) {
                    if(this.group[i].link.value == val) valids++;
                }
                return (valids==this.group.length)? true : false;
            } else if (this.link.value.length>0) return true;
              else return false;
        }
        } else return true;
    };
    this._field.prototype._email = function(input) {
        var val = this.link.value;
        var reg = new RegExp(input, "ig");
        if( typeof input === "string" && val.match(reg)===null ) return false;

        if(/^[\w-\._\+%]+@(?:[\w-]+\.)+[\w]{2,6}$/.test(val)) return true;
        else return false;
    };
    this._field.prototype._regexp = function(input) {
        var val = this.link.value;
        var reg = new RegExp(input, "ig");
        return ( typeof input === "string" && val.match(reg)===null ) ? false : true;
    };
    
    
    //Глобальные методы
    //Валидируйся!
    this._field.prototype.validate = function(event) {
        var conds = Object.size(this.conditions);
            //Есть условия
            if( (conds>0) ) {
                var valid = 0;
                for(var key in this.conditions) {
                    // Если таковая проверка существует
                    if( typeof this["_"+key] === "function" ) {
                        //Получаем ответ проверки, и если TRUE, то плюсуем кол-во валидаций
                        if( this["_"+key].call(this, this.conditions[key]) && this.conditions.required) {
                            valid++;
                        }
                        //А если это поле не является обязательным, то мы так же плюсуем кол-во валидаций и вызываем метод отображения подсказки с этим ключем.
                        else if ( !this["_"+key].call(this, this.conditions[key]) && !this.conditions.required) {
                            valid++;
                            if(event && ( event.target === this.link || event.src === this.link )) {
                                validator.showHint(this.name, key);
                            }
                        }
                        //А если оно прошло валидацию - убираем подсказку!
                        else if ( this["_"+key].call(this, this.conditions[key]) && !this.conditions.required) {
                            valid++;
                            validator.hideHint(this.name);
                        }
                        //Иначе возвращаем название не пройденной валидации
                        else if ( !this["_"+key].call(this, this.conditions[key]) && this.conditions.required ){
                            return key;
                        }
                    } else {
                        valid++;
                    }
                }
                if(valid==conds) {
                    validator.Events.fieldValid.call(this);
                    return true;
                } else {
                    validator.Events.fieldInvalid.call(this);
                    return false;
                }
            //Нет условий
            } else {
                validator.Events.fieldValid.call(this);
                return true;
            }
    };
    //Добавление и удаление условий, получение, парсинг и туСтринг
    this._field.prototype.addCondition = function(name, val) {
        if(val==="") { var val = true; }
        this.conditions[name] = val;
        log("«"+this.name+"» field: Condition «"+name+"» added with value: «"+val+"».");

        validator.Events.conditionAdded.call(this, arguments);
    };
    this._field.prototype.removeCondition = function(name) {
        delete this.conditions[name];
        log("«"+this.name+"» field: Condition «"+name+"» removed.");

        validator.Events.conditionRemoved.call(this, arguments);
    };
    this._field.prototype.getCondition = function(name) {
        return this.conditions[name];
    };
    this._field.prototype.parseCondition = function(input) {
       var str = input.replace(/([a-zA-Z]+)[\[]*([a-zA-Z\d\-|\(\)\\\.@\_\-\+\*\&\^\%\$\!\?]*)[\]]*/g, "$1,$2"); //([a-zA-Z]+)[\[]*([\d-]*)[\]]*
       var output = str.split(",");
        this.addCondition(output[0], output[1]);
        
    };
    this._field.prototype.toStringCondition = function(name) {
        if(this.conditions[name] !== undefined) {
            if(this.conditions[name]===true) {
                return name;
            } else {
                return name + "[" + this.conditions[name] + "]";
            }
        } else {
            return false;
        }
    };
    //DUMP объекта в консоль
    this._field.prototype.dump = function() {
        console.log(this);
    };
    //Добавление и удаление обработчика условий
    this._field.prototype.addValidator = function(fname, func) {
        this["_"+fname] = func;
        log("Added new validator function «"+fname+"» to all _field objects!");

        validator.Events.validatorAdded(arguments);
    };
    this._field.prototype.removeValidator = function(fname) {
        delete this["_"+fname];
        log("Removed validator function «"+fname+"» from all _field objects!");

        validator.Events.validatorRemoved(arguments);
    };
    
    var init = function() {
       
        var inputs = Array.prototype.slice.call(validator.form.getElementsByTagName("INPUT")); //Инпуты
        var textareas = Array.prototype.slice.call(validator.form.getElementsByTagName("TEXTAREA")); //Текстареа
        var select = Array.prototype.slice.call(validator.form.getElementsByTagName("SELECT")); //Селекты
        var fields = inputs.concat(textareas.concat(select));

        //Добавляем поля и кнопку сабмита
        for(i=0;i<fields.length;i++) {
            if(fields[i].type!="hidden" && fields[i].type!="submit" && fields[i].type!="reset") {
                validator.addField(fields[i]);
            }
            if(fields[i].type=="submit") {
                validator.submitBtn = fields[i];
            }
        }
        
        //Задаем ивенты
        for(var key in validator.fields) {
            if(validator.fields[key].link.type == "checkbox" ||
               validator.fields[key].link.type == "radio" ||
               validator.fields[key].link.type == "file" ||
               validator.fields[key].link.tagName == "SELECT"
             ) {
                validator.fields[key].link.addEvent("blur change", function(event) { validator.validate(event); });
            } else {
                validator.fields[key].link.addEvent("blur keyup", function(event) { validator.validate(event); });
            }

            //Добавляем проверку на autocomplete и драгндроп
            if(client && (client.engine.gecko || client.engine.webkit || client.engine.opera ) ) {
                validator.fields[key].link.addEvent("input dragdrop", function(event) { validator.validate(event); });
            }
            else if(client && client.engine.ie ) {
                validator.fields[key].link.addEvent("propertychange", function(event) { validator.validate(event); });
            } else {
                validator.fields[key].link.addEvent("keyup DOMCharacterDataModified", function(event) { validator.validate(event); });
            }

        }

        //Подключаем подсказки
        var hints = validator.form.getElementsByClassName(validator.hintClass);
        for(var i = 0; i<hints.length; i++) {
            validator.hints.push(new validator._hint(hints[i]));
        }
        
        validator.Events.inited();
        validator.validate();
        log("_validator inited", validator);
        
    };
    
    init();
};
_validator.prototype.addField = function(obj) {
    if(obj.name) {
        if(typeof this.fields[obj.name] === "undefined") {
            var name = obj.name;
        } else {
            var done = false;
            var iN = 1;
            while(done!=true) {
                if(typeof this.fields[obj.name+iN] === "undefined") {
                    var name = obj.name+iN;
                    done = true;
                } else iN++;
            }
        }
    } else if (obj.id) {
        if(typeof this.fields[obj.id] === "undefined") {
            var name = obj.id;
        } else {
            var done = false;
            var iN = 1;
            while(done!=true) {
                if(typeof this.fields[obj.id+iN] === "undefined") {
                    var name = obj.id+iN;
                    done = true;
                } else iN++;
            }
        }
    } else if (obj.type) {
        var i = Object.size(this.fields);
        var name = obj.type+i;
    } else {
        var i = Object.size(this.fields);
        var name = obj.tagName+i;
    }
    this.fields[name] = new this._field(obj);
};
_validator.prototype.removeField = function(name) {
    delete this.fields[name];
};
_validator.prototype.fieldList = function() {
    return this.fields;
};
_validator.prototype.validate = function(event) {
    var fields = this.fields,
        groups = [],
        allFields = 0,
        valids = 0,
        type   = (event)? event.type : false;

    for(var key in fields) {
        var fieldResult = fields[key].validate(event);
        //Посчитаем сколько всего полей
        if(!fields[key].group) {
            allFields++;
        } else {
            //Если это группа - записываем только раз!
            if(typeof groups[fields[key].name] == "undefined") {
                groups[fields[key].name] = false;
                allFields++;
            }
        }
        //Если  ответ валидатора ФОЛС
        if(fieldResult !== true) {
            //И если мы трогали поле
            if(fields[key].touched) {
                //То подсвечиваем поле еррором
                fields[key].link.className = this.errClass;
                if( event && (event.target === fields[key].link || event.src === fields[key].link) && event.type == "blur") {
                    //Скрываем подсказку о верности поля
                    this.hideHint(fields[key].name, "true");
                } else {
                    //Показываем нужные подсказки
                    log(fields[key].name + " type: "+fieldResult);
                    this.showHint(fields[key].name, fieldResult);
                    return {errorType: fieldResult, object:fields[key]};
                    //break;
                }
            }
                valids--;
            
            //Если поле не трогали - ничего не делаем
        //Если ответ валидатора ТРУ
        } else {
            //Убираем класс ошибки
            fields[key].link.className = fields[key].link.className.replace(this.errClass, "");
            //Прячем все подсказки если поле было обязательным
            if(fields[key].conditions.required) { 
                this.hideHint(fields[key].name);
            }
            //Прячем подсказку если поле было не обязательным и сделало блюр
            else if (event && (event.target === fields[key].link || event.src === fields[key].link) && event.type != "blur" &&  event.type != "keyup") {
                this.hideHint(fields[key].name);
            }
            if(fields[key].touched && event && (event.target === fields[key].link || event.src === fields[key].link) && event.type != "blur") {
                //Показываем подсказку о том что все круто
                this.showHint(fields[key].name, "true");
            }

            //Проверяем группа ли это
            //Если группа и она еще не TRUE, то делаем TRUE и valids++

            if(!fields[key].group) {
                valids++;
            } else {
                if(groups[fields[key].name] !== true){
                    groups[fields[key].name] = true;
                    valids++;
                }
            }
        }
    }
    
    console.log(valids);
    console.log(allFields);

    if(valids==allFields) { //Object.size(fields)
        log("Form valid");
        this.enableForm();
        this.Events.formValid();
        return true;
    } else {
        log("Form invalid");
        this.disableForm();
        this.Events.formInvalid();
        return false;
    }
    log(valids+" / "+allFields);
    log(this);
};
_validator.prototype.disableForm = function() {
    if(this.active) {
        this.form.removeEvent("submit", this.Events.formSubmit);
        this.form.addEvent("submit", this.preventSubmit);
        this.submitBtn.addEvent("click", this.showError);
        this.submitBtn.disabled = true;
        this.active = false;
    }
    return true;
};
_validator.prototype.enableForm = function() {
    if(!this.active) {
        this.form.addEvent("submit", this.Events.formSubmit);
        this.form.removeEvent("submit", this.preventSubmit);
        this.submitBtn.removeEvent("click", this.showError);
        this.submitBtn.disabled = false;
        this.active = true;
    }
    return true;
};
_validator.prototype.showHint = function(name) {
    var aHints = []; //Актуальные подсказки
    for( var i=0; i<this.hints.length; i++ ) {
        if(this.hints[i].for == name) {
            if( 
                (arguments[1] && this.hints[i].type == arguments[1]) ||
                !arguments[1] ||
                !this.hints[i].type
            ) {
            	if(typeof(arguments[2]) === "string") {
            		this.hints[i].innerHTML = arguments[2];
            	}
                this.hints[i].show();
                log(this.hints[i]);
            }
        }
    }
    /*
    if(aHints) {
        if(aHints.length==1) aHints[0].show();
        if(aHints.length>1) {
            for(var i = 0; i<aHints.length; i++) {
                aHints[i].show();
            }
        }
    }
    */
};
_validator.prototype.hideHint = function(name) {
    var aHints = []; //Актуальные подсказки
    for( var i=0; i<this.hints.length; i++ ) {
        if(this.hints[i].for == name) {
            if( 
                (arguments[1] && this.hints[i].type == arguments[1]) ||
                !arguments[1] ||
                !this.hints[i].type
            ) {
                aHints.push(this.hints[i]);
                log("hide "+this.hints[i].for);
            }
        }
    }
    if(aHints) {
        if(aHints.length==1) aHints[0].hide();
        if(aHints.length>1) {
            for(var i = 0; i<aHints.length; i++) {
                aHints[i].hide();
            }
        }
    }
};
_validator.prototype.addValidator = function(fname, func) {
    this._field.prototype["_"+fname] = func;
    log("Added new validator function «"+fname+"» to all _field objects!");
};
_validator.prototype.removeValidator = function(fname) {
    delete this._field.prototype["_"+fname];
    log("Removed validator function «"+fname+"» from all _field objects!");
};

_validator.prototype.addCondition = function(fieldName, condName, condVal) {
    if(condVal==="") { var condVal = true; }
    if(this.fields[fieldName]) {
        this.fields[fieldName].addCondition(condName, condVal);
    }
};
_validator.prototype.removeCondition = function(fieldName, condName) {
    if(this.fields[fieldName]) {
        this.fields[fieldName].removeCondition(condName);
    }
};
_validator.prototype.parseCondition = function(fieldName, condition) {
    if(this.fields[fieldName]) {
        this.fields[fieldName].parseCondition(condition);
    }
};
_validator.prototype.dump = function() {
    console.log(this);
};
_validator.prototype.addEvent = function(ename, efunc) {
    if(typeof ename == "string" && typeof efunc == "function") {
        if(ename == "onFormValid") { this.Events.formValid = efunc; return true; }
        if(ename == "onFormInalid") { this.Events.formInvalid = efunc; return true; }
        if(ename == "submit") { this.Events.formSubmit = efunc; return true; }
        if(ename == "onSubmit") { this.Events.formSubmit = efunc; return true; }
        if(ename == "onFormSubmit") { this.Events.formSubmit = efunc; return true; }
        if(ename == "onFieldValid") { this.Events.fieldValid = efunc; return true; }
        if(ename == "onFeldInvalid") { this.Events.fieldInvalid = efunc; return true; }
        if(ename == "onFieldTouched") { this.Events.fieldTouched = efunc; return true; }
        if(ename == "onInit") { this.Events.inited = efunc; return true; }
        if(ename == "onValidatorAdded") { this.Events.validatorAdded = efunc; return true; }
        if(ename == "onValidatorRemoved") { this.Events.validatorRemoved = efunc; return true; }
        if(ename == "onConditionAdded") { this.Events.conditionAdded = efunc; return true; }
        if(ename == "onConditionRemoved") { this.Events.conditionRemoved = efunc; return true; }

        this.Events[ename] = efunc;
    }
};
_validator.prototype.removeEvent = function(ename) {
    if(typeof ename == "string") {
        this.Events[ename] = function() {};
    }
};
_validator.prototype.touchFields = function () {
	/* будем считать, что мы поторогали все поля, даже если мы этого не делали */
	for(var key in this.fields) {
		this.fields[key].touched = true;
	}
};
_validator.prototype.isValid = function () {
	return this.active;
};