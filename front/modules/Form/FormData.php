<?php

namespace front\modules\Form;
/**
 * Класс-контейнер
 *
 */
class FormData {
	/**
	 * Данные, которые были введены пользователем и пришли к нам с фронта
	 * @var array
	 */
	private $userData;
	
	/**
	 * Контейнер для различных системных данных. 
	 * Например, тут можно хранить e-mail для отправки формы, шаблон для письма и т.п.
	 * @var array
	 */
	private $sysData;
	
	public function __construct($userData) {
		$this->userData = $userData;
	}

	public function getUserData($key = null) {
		if($key) {
			return isset($this->userData[$key]) ? $this->userData[$key] : null;
		} else {
			return $this->userData ? $this->userData : array();
		}		
	}
	public function getSysData($key = null) {
		if($key) {
			return isset($this->sysData[$key]) ? $this->sysData[$key] : null;
		} else {
			return $this->sysData ? $this->sysData : array();
		}		
	}
	public function setUserData($userData) {
		$this->userData = $userData;
	}
	public function setSysData($sysData) {
		$this->sysData = $sysData;
	}
	public function addUserData($key, $value) {
		$this->userData[$key] = $value;
	}
	public function addSysData($key, $value) {
		$this->sysData[$key] = $value;
	}
	
	
}
