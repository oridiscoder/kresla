<?php
namespace front\modules\Form\Validators;

require_once ('Validator.php');

class Email extends Validator {
	/* (non-PHPdoc)
	 * @see Form_Validator_Abstract::isValid()
	 */
	public function isValid($value) {		
		$validator = new \Zend_Validate_EmailAddress();
		if(!$validator->isValid($value)) {
			$this->pushError("Некорректный e-mail адрес");
			return false;
		} else {
			return true;
		}
	}


}

?>