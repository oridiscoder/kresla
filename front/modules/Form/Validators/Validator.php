<?php
namespace front\modules\Form\Validators;

abstract class Validator  {
	
	private static $validators;
	
	private $params;
	private $errors;
	
	public function getParams() {
		return $this->params;
	}
	public function getErrors() {
		return $this->errors;
	}
	public function setParams($params) {
		$this->params = $params;
	}
	public function setErrors($errors) {
		$this->errors = $errors;
	}
	public function pushError($error) {
		$this->errors[] = $error;
	}
	public function getFirstError() {
		return $this->errors ? $this->errors[0] : null;
	}
	public function getLastError() {
		return $this->errors ? end($this->errors) : null;
	}

	abstract public function isValid($value);
	
	/**
	 * 
	 * @param string $validatorName
	 * @throws Form_Validator_NotFoundException
	 * @return Form_Validator_Abstract
	 */
	public static function factory($validatorName) {
		$validatorName = ucfirst($validatorName);
		$className = __NAMESPACE__.'\\'.$validatorName;
		$fileName = $validatorName . '.php';

		if(!isset(self::$validators[$validatorName])) {
			if(file_exists(dirname(__FILE__).'/'.$fileName)) {
				require_once $fileName;
				if(class_exists($className)) {
					$validator = new $className();
					self::$validators[$validatorName] = $validator;
				} else {
					throw new NotFoundException("Validator «".$className."» not found");
				}
			} else {
				throw new NotFoundException("Validator file «".$fileName."» not found");
			}
		}
		
		return self::$validators[$validatorName];
	}
}

class NotFoundException extends \Exception {
	
}

?>