<?php
namespace front\modules\Form\Validators;

require_once ('Validator.php');

class Required extends Validator {
	
	/* (non-PHPdoc)
	 * @see Form_Validator_Abstract::isValid()
	 */
	public function isValid($value) {
		if(empty($value)) {
			$this->pushError("Не должно быть пустым");
			return false;
		} else {
			return true;
		}
	}


}

?>