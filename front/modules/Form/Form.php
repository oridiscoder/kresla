<?php

namespace front\modules;

use front\modules\Form\FormData;

class Form extends Module {
	
	private $forms;
	
	const MAIL_TEMPLATE_NAME = 'mail_template';
	
	public function getTitle() {
		return "Форма обратной связи";
	}
	
	public function getDescription() {
		"Модуль позволяет создавать формы обратной связи";
	}
	
	public function getTableName() {
		return $this->getDbPrefix().'forms';
	}

	public function getJS() {
		return array("validator.js", "form.js");
	}
	
	public function getCSS() {
		return array("validator.css");
	}

	public function install() {
		$tableName = $this->getTableName();
		$sql = "CREATE TABLE `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`number` INT UNSIGNED NOT NULL DEFAULT 0, 
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`form_id` VARCHAR(255), 
			`date` DATETIME, 
			`data` TEXT, 
			`data_html` TEXT, 
			KEY(`form_id`)
		)";
		
		$this->getDb()->query($sql);
		
		//добавить описание таблицы в боф
		require_once 'models/Table/DbTableCreator.php';
		require_once 'models/Page/PageCreator.php';
		
		$factory = new \DbTableCreator($this->getDb()); 
		$factory->addTable($tableName, $this->getName())
				->addStringField("form_id", "ID Формы")
				->addDateField("date", "Дата")
				->addTextField("data", "Данные")
				->addTextField("data_html", "Данные в HTML");
		$tableID = $factory->getTableId();
		
		$pageFactory = new \PageCreator($this->getDb());
		$pageFactory->addPage("Обратная связь", 2, $tableID);
		
		$this->saveTemplate(self::MAIL_TEMPLATE_NAME, '<table>{{data}}<tr><td>{{key}}</td><td>{{value}}</td></tr>{{/data}}</table>');
		
		$this->installJS();
		$this->installCSS();
		
		return true;
	}

	public function remove() {
		
		$tableName = $this->getTableName();
		
		$this->removeTable($tableName);
		$this->getDb()->query("DROP TABLE IF EXISTS `$tableName`");
		
		$this->deleteTemplate(self::MAIL_TEMPLATE_NAME);
		
		$this->removeJS();
		$this->removeCSS();
		
		return true;
	}

	public function getConfigFields() {
		return array(
			'mailto' => array(
				'title' => 'E-mail', 
				'type' => 'string', 
				'default' => null
			),
			'mail_subject' => array(
					'title' => 'Тема письма',
					'type' => 'string',
					'default' => null
			),
			'mail_template' => array(
				'type' => 'string', 
				'title' => 'Название шаблона письма', 
				'default' => self::MAIL_TEMPLATE_NAME
			)
		);
	}
	
	protected function extractFieldsValues($fields, $rawData) {
		$data = array();
		foreach($fields as $name) {
			if(preg_match_all('/\[(.*?)\]/', $name, $m)) {
				$varname = substr($name, 0, strpos($name, "["));
				$keys =	array_merge(array($varname), $m[1]);
				$value = $rawData;
				
				while(is_array($value)) {
					$key = array_shift($keys);
					if(isset($value[$key])) {
						$value = $value[$key];
					} else {
						$value = null;
					}
				}
				$data[$name] = $value;
			} else {
				$data[$name] = isset($rawData[$name]) ? $rawData[$name] : null;
			}
		}
		return $data;
	}
	
	/**
	 * Метод применяет htmlspecialchars на одномерном массиве данных
	 * @param array $data
	 * @return array
	 */
	public function escapeData($data) {
		foreach ($data as $key=>$value) {
			$data[$key] = htmlspecialchars($value);
		}
		return $data;
	}
	
	/**
	 * Получить объект формы
	 * @param string $formName - имя формы
	 * @param string $formHTML - HTML код формы
	 * @return \front\modules\Form\Forms\Form
	 */
	public function getForm($formName, $formHTML = null) {
		$formName = ucfirst($formName);
		
		if(!isset($this->forms[$formName])) {
			$className = __NAMESPACE__.'\\Form\\Forms\\'.$formName;
			$fileName = $formName.'.php';
			$fullFilename = dirname(__FILE__).'/Forms/'.$fileName;
			
			if(file_exists($fullFilename)) {
				require_once $fullFilename;
				if(class_exists($className)) {
					/* @var $form Feedback_Form_Abstract */
					$form =  new $className($formHTML);					
					$form->setDi($this->getDi());
					$form->setModule($this);
					$this->forms[$formName] = $form;
				} else {
					throw new FormNotFoundException("Form object $className not found");
				}
			} else {
				throw new FormNotFoundException("Form file $fileName not found in Forms");
			}
		}
		
		return $this->forms[$formName];
	}
	
	public function render($options, $content) {
		
		$request = $this->getRequest();
		
		$formName = isset($options['form_name']) ? $options['form_name'] : 'Standart';		
		$form = $this->getForm($formName, $content);
		$formID = $form->getFormAttributes("id");
		$content = (string) $form;	//форма внедрит в контент <input name="_form_id" />
		$data = array();
		$fields = array_keys($form->getFields());		
		
		if($request->getParam("_form_id") == $formID) {
			if($request->isPost()) {
				
				require_once "FormData.php";
				
				$data = $this->extractFieldsValues($fields, $request->getPost());
				$form->validate(new FormData($data));
				$errors = $form->getErrors();
					
				if($errors) {
					if($request->isXmlHttpRequest()) {
						echo json_encode(array(
								'data' => $data,
								'errors' => $errors, 
								'success' > false
						));
						exit();
					} else {
						$this->redirectError($form);
					}
				} else {
					
					/* сформируем системные данные для формы */
					$dataObject = new FormData($data);
					$dataObject->addSysData("mailto", isset($options['maito']) ? $options['maito'] : $this->getConfig('mailto'));
					$dataObject->addSysData("mail_subject", isset($options['mail_subject']) ? $options['mail_subject'] : $this->getConfig('mail_subject'));
					
					$templateName = isset($options['mail_template']) ? $options['mail_template'] : 'mail_template';
					$dataObject->addSysData("mail_template", $this->getTemplate($templateName));
					$dataObject->addSysData("table_name", $this->getTableName());
					$dataObject->addSysData("module_options", $options);
					
					foreach ($options as $key => $value) {
						$dataObject->addSysData($key, $value);
					}
					
					$form->success($dataObject);
					
					if($request->isXmlHttpRequest()) {
						echo json_encode(array(
								'data' => $data,
								'errors' => null, 
								'success' => true
						));
						exit();
					} else {
						$this->redirectSuccess($form);
					}
				}
					
			} else {
				$data = $this->extractFieldsValues($fields, $request->getParams());
				$data = $this->escapeData($data);
				
				if(($errors = $request->getParam("errors")) != null) {
					/* @var $form Form_Abstract */
					$form->setErrors($errors);
					$data['errors_html'] = $form->getErrorsAsHTML();
					$content = $form->processCheckedFields($data);
				}
				if($request->getParam("success")) {
					$content = $form->getSuccessHTML();
				}
			}
		}
		
		return $this->getDi()->get("lex")->parse($content, $data);
	}
	
	protected function redirectError($form) {
		$referer = $this->getDi()->get("request")->getServer("HTTP_REFERER");
		/* возможны ситуации, когда REFERER нету */
		$urlData = parse_url($referer);
		//$queryData = explode("=", $urlData['query']);
		/* дальше писать мне лень. потом как-нибудь напишем обработку GET параметров */
		
		$returnPath = $urlData['path'];
		$formID = $form->getFormAttributes("id");
		$formData = $form->getData();

		$formData['errors'] = $form->getErrors();
		$formData['_form_id'] = $formID;
		
		$returnURL = $returnPath . '?' . http_build_query($formData).'#'.$formID;
		
		header("Location: $returnURL");
		exit();
	}
	
	protected function redirectSuccess($form) {
		$referer = $this->getDi()->get("request")->getServer("HTTP_REFERER");
		/* возможны ситуации, когда REFERER нету */
		$urlData = parse_url($referer);
		//$queryData = explode("=", $urlData['query']);
		/* дальше писать мне лень. потом как-нибудь напишем обработку GET параметров */
		
		$returnPath = $urlData['path'];
		$formID = $form->getFormAttributes("id");
		$formData = $form->getData();
		$formData['success'] = true;
		$formData['_form_id']  = $formID;
		
		$returnURL = $returnPath . '?' . http_build_query($formData).'#'.$formID;
		
		header("Location: $returnURL");
		exit();
	}

}

class FormNotFoundException extends \Exception {
	
}

?>