<?php

namespace front\modules;

class Vacancy extends Module {
	
	protected $activeVacancies;
	
	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getTitle()
	 */
	public function getTitle() {
		return "Вакансии";		
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getDescription()
	 */
	public function getDescription() {
		
	}
	
	public function getTableName() {
		$dbPrefix = $this->getDbPrefix();
		return $dbPrefix.'vacancy';
	}
	
	public function getStatusTableName() {
		return $this->getTableName().'_status';
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::install()
	 */
	public function install() {
		$tableName = $this->getTableName();
		
		$sql = "
		CREATE TABLE `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
			`number` INT UNSIGNED NOT NULL DEFAULT 0, 
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0, 
			`title` VARCHAR(255),
			`keywords` VARCHAR(255), 
			`description` VARCHAR(500), 
			`text` TEXT, 
			`image` VARCHAR(255), 
			`status` TINYINT UNSIGNED DEFAULT 0
		)";
		$this->getDb()->query($sql);
		
		$statusTableName = $this->getStatusTableName();
		$sql = "
		CREATE TABLE `$statusTableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED NOT NULL DEFAULT 0,
			`hidden` TINYINT(1) UNSIGNED DEFAULT 0,
			`title` VARCHAR(255)
		)";
		$this->getDb()->query($sql);
		
		//добавить описание таблицы в боф
		require_once 'models/Table/DbTableCreator.php';
		require_once 'models/Page/PageCreator.php';
		require_once 'models/Enums.php';
		
		/* добавляем таблицу статусов */
		$factory = new \DbTableCreator($this->getDb());
		$factory->addTable($statusTableName, "Статус")
				->addStringField("title", "Название");
		$statusTableID = $factory->getTableId();
		
		/* добавляем таблицу вакансий */
		$factory->addTable($tableName, $this->getTitle())
				->addStringField("title", "Название")
				->addHTMLField("text", "Описание")
				->addImageField("image", "Картинка", array('draw_into' => '162x112'))
				->addDescendantField("status", "Статус", $statusTableID);
		$tableID = $factory->getTableId();
		
		$pageFactory = new \PageCreator($this->getDb());
		$pageID = $pageFactory->addPage($this->getTitle(), 2, $tableID);
		$pageFactory->addPage("Статус", 2, $statusTableID, null, $pageID);
		
		$this->loadInitialData();
		
		return true;
	}
	
	protected function loadInitialData() {
		$tableName = $this->getTableName();
		$statusTableName = $this->getStatusTableName();
		
		$this->getDb()->insert($statusTableName, array(
			'title' => 'Очень нужен'
		));
		
		$this->saveTemplate('item', '
		<div class="some-pane{{if active}} active{{endif}}" id="vac_{{id}}">
		<a href="{{href}}" class="vac-name{{if status}} need{{endif}}">{{title}}</a>
		<div class="description">
	      <div class="photo">{{if image}}<img src="{{image}}" alt="{{title}}" >{{endif}}</div>
			<div class="content">
				{{text}}
			</div>
		</div>
	      <br clear="all" />
		</div><!-- .some-pane -->');
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::remove()
	 */
	public function remove() {
		
		$tableName = $this->getTableName();
		$statusTableName = $this->getStatusTableName();
		
		$sql = "DROP TABLE IF EXISTS `{$tableName}`";
		$this->getDb()->query($sql);
		$sql = "DROP TABLE IF EXISTS `{$statusTableName}`";
		$this->getDb()->query($sql);
		
		$this->removeTable($tableName);
		$this->removeTable($statusTableName);
		
		//удалим наши шаблоны
		$this->deleteTemplate("item");
		
		return true;
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getConfigFields()
	 */
	public function getConfigFields() {
		
	}
	
	protected function getActiveVacancies() {
		if(!$this->activeVacancies) {
			$select = $this->getDb()->select()
				->from($this->getTableName())
				->where("hidden = 0")
				->order("number");
			$this->activeVacancies = $this->getDb()->fetchAll($select);
		}
		
		return $this->activeVacancies;
	}
	
	public function hasActive($options, $template = null) {
		return $this->getActiveVacancies() != null;
	}
	
	public function getList($options, $template = null) {
		$items =  $this->getActiveVacancies();
		return $items;
	}
	
	public function totalActive($options, $template = null) {
		return count($this->getActiveVacancies());
	}
	
	public function getTotalLiteral($options, $template = null) {
		$total = $this->totalActive(array());
		$names = array(
			0 => 'пока нет', 
			1 => 'одна', 
			2 => 'две', 
			3 => 'три',
			4 => 'четыре',
			5 => 'пять',
			6 => 'шесть',
			7 => 'семь'
		);
		return isset($names[$total]) ? $names[$total] : $total;
	}
	
	public function renderList($options, $template = null) {
		$items = $this->getActiveVacancies();
		$template = $template ? $template : $this->getTemplate("item");
		
		require_once 'cms/application/models/Table/DbTableLight.php';
		$dbTable = new \DbTableLight($this->getDb(), $this->getTableName());
		$imageField = $dbTable->getField("image");
				
		$outHTML = ""; 
		if($items) foreach($items as $key => $item) {
			$item['href'] = '/vacancy/#vac_'.$item['id'];
			$item['image'] = $imageField->getURL($item);
			$item['image'] = $item['image'] ? $item['image'] : '/img/hh.jpg';
			$item['active'] = $key == 0 ? true : false;
			$outHTML .= $this->getDi()->get("lex")->parse($template, $item);
		}
		return $outHTML;
	}

}

?>