<?php
namespace front\modules;

class Articles extends News {
	
	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getTitle()
	 */
	public function getTitle() {
		return "Статьи";
	}
	
	public function getConfigFields() {
		$config = parent::getConfigFields();
		$config['base_url']['default'] = '/articles';
		return $config;
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getDescription()
	 */
	public function getDescription() {
		return "Модуль управления статьями";		
	}
	
	public function getTableName() {
		return $this->getDbPrefix()."articles";
	}
}

class ChildPageNotFoudException extends \Exception {
	
}