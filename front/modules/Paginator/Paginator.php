<?php

namespace front\modules;

class Paginator extends Module {
	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getTitle()
	 */
	public function getTitle() {
		return "Paginator";		
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getDescription()
	 */
	public function getDescription() {
		
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::install()
	 */
	public function install() {

		$templates['wrapper'] = '<ul class="paginator">{items}</ul>';
		$templates['normal'] = '<li><a href="{url}">{title}</a></li>';
		$templates['active'] = '<li><span class="active">{title}</span></li>';
		$templates['middle'] = '<li><span class="more">...</span></li>';
		
		foreach($templates as $name => $content) {
			$this->saveTemplate($name, $content);
		}
		
		return true;		
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::remove()
	 */
	public function remove() {
		return true;		
	}

	/* (non-PHPdoc)
	 * @see FrontModuleAbstract::getConfigFields()
	 */
	public function getConfigFields() {
		
	}
	
	public function html($options, $template) {
		
		$getName = 'p';
		$space = 5;
		
		if(!isset($options['source_module'])) {
			throw new PaginatorException("No source module has been set to Paginator");
		} else {
			$sourceModule = $this->getDi()->getModule($options['source_module']);
			if($sourceModule instanceof Paginable) {
				$total = $sourceModule->getTotal();
				$onpage = isset($options['onpage']) ? $options['onpage'] : $sourceModule->getOnPage();
				
				$pages = ceil($total / $onpage);
				$paginatorHTML = '';
				if($pages > 1) {
					/* @var $request Bof_Request */
					$request = $this->getDi()->get("request");
					$current = $request->getQuery("p");
					$current = $current ? $current : 1;
					
					$path = $request->getPathInfo();
					$query = $request->getQuery();
					
					for($p = 1; $p <= $pages; $p++) {
						if($p == $current) {
							$itemHTML = $this->getTemplate("active");
						} elseif(abs($current - $p) <= $space) {
							$itemHTML = $this->getTemplate("normal");
						} elseif($p == round($current / 2)) {	/* левая середина */
							$itemHTML = $this->getTemplate("middle");
						} elseif($p == $current + round(($pages - $current) / 2)) {
							$itemHTML = $this->getTemplate("middle"); /* правая середина */
						} elseif ($p == 1 || $p == $pages) {
							$itemHTML = $this->getTemplate("normal");
						} else {
							$itemHTML = "";
						}
						$query[$getName] = $p;
						$temp = $query;
						if($p == 1){
							unset($temp['p']);
						}
						$url = $path.'?'.http_build_query($temp);
						$itemHTML = str_replace(array('{title}', '{url}'), array($p, $url), $itemHTML);
						$paginatorHTML .= $itemHTML;						
					}
					$paginatorHTML = str_replace('{items}', $paginatorHTML, $this->getTemplate("wrapper"));
				}
				
				return $paginatorHTML;
				
			} else {
				throw new PaginatorException("The Source Module must implements Paginable interfce");
			}
		}
	}
}

class PaginatorException extends \Exception {
	
}

?>