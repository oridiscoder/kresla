<?php
namespace front\modules;

abstract class Publications extends Module implements Paginable {
	
	public function install() {
	
		$tableName = $this->getTableName();
	
		$sql = "DROP TABLE IF EXISTS `$tableName`";
		$this->getDb()->query($sql);
	
		$sql = "CREATE TABLE `$tableName` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED DEFAULT 0,
			`hidden` TINYINT(1) DEFAULT 0,
			`title` VARCHAR(255),
			`url`	VARCHAR(255),
			`keywords` VARCHAR(500),
			`description` VARCHAR(500),
			`annotation_short` VARCHAR(500),
			`annotation` TEXT,
			`text` TEXT,
			`date_create` DATETIME,
			`date_publish` DATETIME,
			KEY(`url`),
			KEY(`date_publish`)
		) ENGINE=MyISAM";
		$this->getDb()->query($sql);
	
		$factory = $this->getTableFactory();
		$factory->addTable($tableName, $this->getTitle())
				->addStringField('title', 'Заголовок')
				->addStringField('url', 'URL')
				->addStringField('keywords', 'Keywords', array('hidecolumn' => 1))
				->addStringField('description', 'Description', array('hidecolumn' => 1))
				->addTextField('annotation_short', 'Коротко')
				->addTextField('annotation', 'Аннотация')
				->addTextField('text', 'Текст', array('hidecolumn' => 1))
				->addDateField('date_create', 'Дата создания', array('hidecolumn' => 1))
				->addDateField('date_publish', 'Дата публикации');
	
		$pageFactory = $this->getPageFactory();
		$pageFactory->addPage($this->getTitle(), 2, $factory->getTableId());
	
		/* установим наши шаблоны */
		$this->saveTemplate("list", '
			<ul>
			{{items}}
			<li><a href="{{href}}">{{name}}</a></li>
			{{/items}}
			</ul>
		');
	
		$this->saveTemplate("read", '
			<article>
			<header>
			<h1>Apple</h1>
			<p><time pubdate="pubdate" datetime="{{date_datetime}}">{{date}}</time></p>
			</header>
			{{text}}
			</article>
		');
	
		return true;
	}
	
	public function remove() {
	
		$tableName = $this->getTableName();
	
		$sql = "DROP TABLE IF EXISTS `{$tableName}`";
					$this->getDb()->query($sql);

		$this->removeTable($tableName);

		//удалим наши шаблоны
		$this->deleteTemplate("list");
		$this->deleteTemplate("read");

		return true;
	}
	
	public function getConfigFields() {
		return array(
				'onpage' => array(
						'type' => 'int',
						'default' => 10,
						'title' => 'Кол-во на странице'
				),
				'fresh_limit' => array(
						'type' => 'int',
						'default' => 4,
						'title' => 'Кол-во последних'
				),
				'order' => array(
						'type' => 'enum',
						'title' => 'Порядок сортировки',
						'default' => 'date_publish DESC',
						'values' => array(
								'date_publish DESC' => 'По дате публикации',
								'date_create DESC' => 'По дате создания',
								'number DESC' => 'По порядку'
						)
				),
				'base_url' => array(
						'type' => 'string',
						'default' => '/publications',
						'title' => 'Базовый URL'
				), 
				'rss_channel_title' => array(
						'type' => 'string',
						'default' => 'Публикации',
						'title' => 'Название RSS канала'
				), 
				'rss_channel_description' => array(
						'type' => 'text',
						'default' => '',
						'title' => 'Описание RSS канала'
				),
				'rss_channel_logo' => array(
						'type' => 'text',
						'default' => '/img/logo_rss.png',
						'title' => 'Лого для RSS канала. MAX:100x100px'
				),
		);
	}
	
	public function hasLast() {
		$db = $this->getDb();
		$select = $db->select()
			->from($this->getTableName(), array("cnt" => 'COUNT(*)'))
			->where("date_publish <= CURDATE()")
			->where("hidden = 0");
		return $db->fetchOne($select);
	}
	
	public function getLast($options, $content) {
		$limit = isset($options['limit']) ? $options['limit'] : $this->getConfig("fresh_limit");
		$select = $this->getDb()->select()
			->from($this->getTableName(), array('id', 'title', 'url', 'date_create', 'date_publish', 'annotation_short'))
			->where("hidden = 0")
			->where("date_publish <= CURDATE()")
			->order($this->getConfig("order"))
			->limit($limit);
		$rows =  $this->getDb()->fetchAll($select);
		if($rows) {
			$this->getRegistry()->addRows($rows);
			foreach ($rows as $key=>$row) {
				$rows[$key]['annotation_short'] = preg_replace('/\[\[(.*?)\]\]/ms', '<a href="'.$this->url($row['id']).'">\\1</a>', $row['annotation_short']);
			}			
		}
		return $rows;
	}
	
	public function prepareSelect($selectFields = null) {
		$selectFields = $selectFields ? $selectFields : array('id', 'title', 'date_create', 'date_publish', 'annotation', 'url');
		$select = $this->getDb()->select()
			->from($this->getTableName(), $selectFields)
			->where("hidden = 0");
		return $select;
	}
	
	public function getList($options, $content) {
		/* @var $request Bof_Request */
		$request = $this->getDi()->get("request");
		$pageNumber = $request->getQuery("p");
		$pageNumber = $pageNumber ? $pageNumber - 1 : 0;
	
		$order = isset($options['order']) ? $options['order'] : $this->getConfig("order");
		$limit = isset($options['onpage']) ? $options['onpage'] : $this->getConfig("onpage");
	
		$select = $this->prepareSelect();
		$select->order($order);
		$select->limit($limit, $pageNumber * $limit);
	
		$rows =  $this->getDb()->fetchAll($select);
		if($rows) {			
			$this->getRegistry()->addRows($rows);
			foreach ($rows as &$row) {
				$row['url'] = $this->url($row['id']);
			}
		}
		return $rows ? $rows : array();
	}
	
	public function find($id) {
		$select = $this->getDb()->select()
			->from($this->getTableName());
		if(is_numeric($id)) {
			$select->where("id = ?", $id);
		} else {
			$select->where("url = ?", $id);
			$select->where("hidden = 0");
		}
		return $this->getDb()->fetchRow($select);
	}
	
	public function getTotal() {
		$select = $this->prepareSelect();
		$select->reset(\Zend_Db_Select::COLUMNS);
		$select->columns(array('cnt' => 'COUNT(*)'));
		return $this->getDb()->fetchOne($select);
	}
	
	public function getOnPage() {
		return $this->getConfig("onpage");
	}
	
	/**
	 * Строит URL для доступа к отдельной публикации
	 * @param array $options - {id: 5}
	 * @param string $content - не используется
	 * @return string|null
	 */
	public function url($options, $content = null) {
	
		if(is_numeric($options)) {
			$id = $options;
		} elseif(is_array($options) && isset($options["id"])) {
			$id = $options["id"];
		} else {
			$id = null;
		}
	
		if($id) {
			
			if(($row = $this->getRegistry()->find($id)) == false) {
				$row = $this->find($id);
				if($row) {
					$this->getRegistry()->addRow($row);
				}
			}
	
			return $row ? $this->getBaseURL().'/'.$row['url'].'.html' : null;
		}
	}
	
	public function rss() {
		$select = $this->getDb()->select()
			->from($this->getTableName())
			->where("hidden = 0")
			->where("date_publish <= CURDATE()")
			->where("date_publish >= DATE_SUB(CURDATE(), INTERVAL 2 WEEK)")
			->order("date_publish DESC");
		$rows = $this->getDb()->fetchAll($select);
	
		/*@var $builder \front\models\RssBuilder */
		$builder = $this->getDi()->get("rssBuilder");
	
		$serverURL = 'http://'.$this->getRequest()->getServer("SERVER_NAME");
		$baseURL = $this->getConfig("base_url");
	
		$entries = array();
		if($rows) {
			$channelPubDate = $rows[0]['date_publish'];
			foreach ($rows as $row) {
				$entries[] = array(
					'title' => $row['title'],
					'link' => $serverURL . $this->url($row['id']), 
					'description' => $row['annotation'],
					'pubDate' => $row['date_publish'],
					'yandex:full-text' => $row['text']
				);
			}
		}
	
		$channel = array(
				'title' => $this->getConfig("rss_channel_title"),
				'link' => $serverURL.'/',
				'description' => $this->getConfig("rss_channel_description"),
				'pubDate' => isset($channelPubDate) ? $channelPubDate : null,
				'image' => $serverURL.$this->getConfig("rss_channel_logo"),
				'entries' => $entries
		);
		header("Content-type: text/xml; charset=utf8");
		return $builder::rss($channel);
	}
	
	public function route(\front\models\Page $page, $request) {
		$parts = $page->getChildURLParts();
		if(count($parts) == 1) {
			/* RSS */
			if($parts[0] == 'rss') {
				return array($this, 'rss');
			} else {
				/* страница пуликации */
				$name = $parts[0];
				$name = str_replace(".html", "", $name);
				$row = $this->find($name);
				if($row) {
					$childPage = $page->findChildPage("single");
					if($childPage) {
						$childPage['title'] = $row['title'];
						$childPage['keywords'] = $row['keywords'];
						$childPage['description'] = $row['description'];
						$childPage['menu_name'] = $row['title'];
						$name = "_object";
						$childPage[$name] = $row;
						
						//установим хлебные крошки */
						$breadcrumbs = $page->getBreadcrumbs();
						$breadcrumbs[] = array(
							"title" => $row['title'], 
							"href" => $this->getRequest()->getRequestUri()
						);
						$childPage['_breadcrumbs'] = $breadcrumbs;
						 
						return $childPage;
					} else {
						throw new RouteException("Page ".$page->getFullUrl().' must have child page with name "single"');
					}
				} else {
					/* страница года */
					$years = $this->getYears();
					if(in_array($parts[0], $years)) {
						$request->setParam("year", $parts[0]);
						return $page->asArray();
					}
					return false;
				}
			}
		} else {
			return false;
		}
	}
	
	abstract public function getTableName();
	
}

class RouteException extends \Exception {

}