<?php

namespace front\modules;

use front\models\Orm;

class News extends Publications  {
	
	private $itemId;
    private $fullURL;
    private $years;
	
	public function getTitle() {
		return "Новости";
	}

	public function getDescription() {
		return "Модуль новостей. Создает таблицу новостей, страницу в разделе Сайт";
	}
	
	public function getTableName() {
		$dbPrefix = $this->getDbPrefix();
		return $dbPrefix.'news';
	}
	
	public function prepareSelect($selectFields = null) {
		$select = parent::prepareSelect($selectFields);
		
		$timeStart = mktime(0, 0, 0, 1, 1, $this->getCurrentYear());
		$timeStop = mktime(0, 0, 0, 1, 1, $this->getCurrentYear() + 1);
		$select->where("date_publish >= '".date('Y-m-d', $timeStart)."'");
		$select->where("date_publish < '".date('Y-m-d', $timeStop)."'");
		return $select;
	}
	
	public function getConfigFields() {
		$config = parent::getConfigFields();
		$config['base_url']['default'] = '/news';
		return $config;
	}
	
	/**
	 * Получит список лет, в которых у нас есть публиковались новости
	 * @return array [2012, 2011, 2010]
	 */
	public function getYears() {
		if($this->years === null) {
			$sql = "SELECT DISTINCT YEAR(date_publish) as year FROM `".$this->getTableName()."` ORDER BY year DESC";
			$this->years = $this->getDb()->fetchCol($sql);
			$this->years = $this->years ? $this->years : array();
		}
		return $this->years;
	}
	
	public function getCurrentYear() {
		/* @var $request Bof_Request */
		$request = $this->getDi()->get("request");
		$year = $request->getParam("year");
		$year = $year ? $year : $this->getLastYear();
		return $year;
	}
	
	public function getLastYear() {
		if(!$this->years) {
			$date = $this->getDb()->fetchOne("SELECT MAX(date_publish) FROM `".$this->getTableName()."` WHERE hidden = 0");
			$date = $date ? strtotime($date) : time();
			$year =  date('Y', $date);
		} else {
			$year = $this->years[0];
		}
		return $year;
	}
	
	public function yearListHTML($options, $content) {
		
		/* @var $lp \front\models\LightParser */
		$lp = $this->getDi()->get("lightParser");
		
		$templates = $lp->extractTemplates($content);
		
		$currentYear = $this->getCurrentYear();
		$lastYear = $this->getLastYear();
		$years = $this->getYears();
		$html = '';
		if($years) {
			foreach($years as $year) {
				if($year == $currentYear) {
					$template = $templates['active'];
				} else {
					$template = $templates['normal'];
				}
				
				/* @var $page Page */
				$page = $this->getDi()->get("page");
				$url = '/news/?year='.str_replace("/", "", '/'.$page->getSectionUrl().'/'.$year.'/');
				
				$html .= $lp->insertValues($template, array(
					'title' => $year, 
					'url' => $url
				));
			}
		}

        $sql="SELECT * FROM `".$this->getTableName()."`";
        $lowlevel = $this->getDb()->fetchAll($sql);

        return $html;
	}
	
	public function renderList($options, $template = null) {
		$orm = new Orm("news");
		
		if($this->getRequest()->getParam("year")) {
			$params["YEAR(date_publish) = ?"] =  $this->getRequest()->getParam("year");
		} else {
			$timeStart = mktime(0, 0, 0, 1, 1, $this->getCurrentYear());
			$timeStop = mktime(0, 0, 0, 1, 1, $this->getCurrentYear() + 1);
			$params["date_publish >= ?"] = date('Y-m-d', $timeStart);
			$params["date_publish < ?"]  = date('Y-m-d', $timeStop);
		}
		
		$rows = $orm->find($params)->asArray();
		foreach ($rows as $key => $row) {
			$rows[$key]['url'] = $this->getBaseURL().'/'.$row['url'].'.html';
		}
		
		return $this->getDi()->get("lex")->parse($template, array("items" => $rows));
	}
	
	public function getBaseURL() {
		return $this->getConfig("base_url");
	}

    public function route(\front\models\Page $page, $request) {
        $parts = $page->getChildURLParts();
        if(count($parts) == 1) {
            if($parts[0] == 'rss') {
                return array($this, 'rss');
            } else {
                $name = $parts[0];
                $name = str_replace(".html", "", $name);
                $row = $this->find($name);
                if($row) {
                    $childPage = $page->findChildPage("single");
                    if($childPage) {


                        $this->itemId=$name;  //itemId - id или url новости
                        $this->fullURL=$page->getFullUrl().$name;
                        $childPage['title'] = $row['title'];
                        $childPage['keywords'] = $row['keywords'];
                        $childPage['description'] = $row['annotation'];
                        $childPage['menu_name'] = $row['title'];
                        
                        $childPage["_single"] = $row;
                        
                      
                        return $childPage;
                    } else {
                        throw new RouteException("Page ".$page->getFullUrl().' must have child page with name "single"');
                    }
                } else {
                    $years = $this->getYears();
                    if(in_array($parts[0], $years)) {
                        $request->setParam("year", $parts[0]);
                        return $page->asArray();
                    }
                    return false;
                }
            }
        } else {
            return false;
        }
    }
}

?>