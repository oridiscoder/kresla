<?php
namespace front\modules;

use front\modules\CatalogKresla\models\Good;

class Search extends Module {
	public function getTitle() {
		return "Поиск";
	}
	public function getDescription() {
		return "Модуль поиска по указанным сущностям";
	}
	public function install() {
		return true;
	}
	public function remove() {
		return true;
	}
	
	public function getConfigFields() {
		return array(
			"fields" => array(
				"title" => "Поля, по которым производится поиск",
				"type" => "string",  
				"default" => null
			), 
			"min_word_length" => array(
				"title" => "Минимальная длина слова", 
				"type" => "int", 
				"default" => 3
			) 
		);
	}
	
	public function find($query, $fieldIds = null) {
		require_once "models/Table/Field/DbFieldAbstract.php";
		
		$m = explode(" ", $query);
		
		foreach ($m as $w) {
			$w = trim($w);
			if(strlen($w) >=  $this->getConfig("min_word_length")) {
				$words[] = $w;
			}
		}
		
		$tables = array();
		$fields = array();
		
		if($words) {
			$fieldIds = $fieldIds ? $fieldIds : $this->getConfig("fields");
			
			if($fieldIds) {
				$fieldIds = explode(",", $fieldIds);
				foreach ($fieldIds as $fieldID) {
					$field = $this->getDb()->fetchRow("SELECT * FROM bof_fields WHERE id = ?", trim($fieldID));
					if($field) {
						$field['_db_adapter'] = $this->getDb();
						$fields[] = \DbFieldAbstract::factory($field);
					}
				}
			}
			
			foreach ($fields as $field) {
				/* @var $field \DbFieldAbstract */
				if(!isset($tables[$field->getTableID()])) {
					$tables[$field->getTableID()] = $field->getTable();
				}
				$tables[$field->getTableID()]['_search_fields'][] = $field->getName();
			}
			
			
			foreach ($tables as $id => $table) {
				$select = $this->getDb()->select()
					->from($table['name']);
				foreach ($table['_search_fields'] as $fieldName) {
					foreach ($words as $word) {
						$select->orWhere("`$fieldName` LIKE ? ", "%$word%");
					}
				}
				$select->where("hidden = 0");
				$tables[$id]['_rows'] = $this->getDb()->fetchAll($select);
			}
		}
		
		return $tables;
	}
	
	public function go($options, $template) {
		$fieldIds = isset($options['fields']) ? $options['fields'] : $this->getConfig("fields");
		$tables = $this->find($this->getRequest()->getQuery("q"), $fieldIds);
		
		$items = array();
		foreach ($tables as $table) {
			if($table['name'] == 'kresla_goods') {
				foreach ($table['_rows'] as $row) {
					$good = new Good($row);
					$items[] = array(
						"id" => $row['id'],
						"title" => $row['title'], 
						"text" => mb_substr($row['text'], 0, 400)."...",
						"thumb" => $row['thumb'],
						"price" => number_format($row['price'], 0, '', ' '),
						"url" => '/catalog/'.$good->getFullURL()
					);
				}
			}
			if($table['name'] == 'kresla_news') {
				foreach ($table['_rows'] as $row) {
					$items[] = array(
							"title" => $row['title'],
							"text" => $row['text'],
							"url" => '/news/'.$row['url'].'.html'
					);
				}
			}
		}
		
		foreach($items as &$item) {
			$item['words'] = $this->getRequest()->getQuery("q");
		}
		
		return $this->fetch("results.html", array(
			"items" => $items
		));
	}
	
}

?>