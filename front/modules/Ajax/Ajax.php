<?php
namespace front\modules;
use front\models\ModuleNotFoundException;

class Ajax extends Module {
	public function route(\front\models\Page $page, $request) {
		$parts = $page->getChildURLParts();
		$partsLength = count($parts);
		if($partsLength) {
			$modulename = $parts[0];
			$methodname = $parts[1];
			
			try {
				/* @var $module \front\modules\Module */
				$module = $this->getDi()->getModule($modulename);
				if(method_exists($module, $methodname)) {
					return array($module, $methodname);
				}
			} catch (ModuleNotFoundException $ex) {
				
			}
		}
	}
	public function getTitle() {
		return "Ajax-обработчик";
	}

	public function getDescription() {
	}

	public function install() {
		return true;
	}

	public function remove() {
		return true;
	}

	public function getConfigFields() {
		
	}

}