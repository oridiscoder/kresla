var Module_SeoPortfolio = function (pages) {
	that = this;
	this.pages = pages;
	this.showMoreButton = null;
	this.pagenumber = 1;
	this.onpage = 10;
	this.setPages = function(pages) {
		this.pages = pages;
	}
	this.setOnpage = function (onpage) {
		this.onpage = onpage;
		
	}
	this.init = function () {
		this.showMoreButton = $('#ShowMorePortfolio');
		this.showMoreButton.click(this.loadMore);
		if(this.pagenumber == this.pages.length) {
			this.hideButton();
		} else {
			this.setShowMoreCount(this.getItemsCountOnNextPage());
		}
	}
	this.loadMore = function () {
		var onpage = that.onpage;
		var url = '/ajax/SeoPortfolio/more/';
		$.get(url, {p: that.pagenumber + 1, onpage: onpage}, function (html) {
			$('.seo-portfolio > .block:last').after(html);
			that.pagenumber ++;
			if(that.pagenumber == that.pages.length) {
				that.hideButton();
			} else {
				that.setShowMoreCount(that.getItemsCountOnNextPage());
			}
		});
		return false;
	}
	this.getItemsCountOnNextPage  = function () {
		var p = this.pagenumber + 1;
		if(p <= this.pages.length) {
			return this.pages[p -1];
		} else {
			return 0;
		}
	}
	this.hideButton = function () {
		this.showMoreButton.hide();
	}
	this.showButton = function () {
		this.showMoreButton.show();
	}
	this.setShowMoreCount = function (count) {
		this.showMoreButton.find('span.more_count').text(count);
	}
}