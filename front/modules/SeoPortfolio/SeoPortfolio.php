<?php
namespace front\modules;

class SeoPortfolio extends Module {
	
	public function getTitle() {
		return "SEO портфолио";
	}

	public function getDescription() {
		
	}
	
	public function getSitesTableName() {
		return $this->getDbPrefix() . "seo_portfolio_sites";
	}
	public function getQueriesTableName() {
		return $this->getDbPrefix() . "seo_portfolio_queries";
	}
	/**
	 * Название таблицы тематик
	 */
	public function getSubjectsTableName() {
		return $this->getDbPrefix() . "seo_portfolio_subjects";
	}

	public function install() {
		$sitesTablename = $this->getSitesTableName();
		$queriesTablename = $this->getQueriesTableName();
		$subjectsTablename = $this->getSubjectsTableName();
		
		$sql = "
		CREATE TABLE IF NOT EXISTS `$sitesTablename` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED NOT NULL DEFAULT 0,
			`hidden` TINYINT(1) NOT NULL DEFAULT 0,
			`name` VARCHAR(255),
			`url` VARCHAR(255),
			`annotation` VARCHAR(500),
			`image` VARCHAR(255),
			`subject_id` INT,
			KEY(`number`)
		)";
		$this->getDb()->query($sql);
		
		$sql = "
		CREATE TABLE IF NOT EXISTS `$queriesTablename` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED NOT NULL DEFAULT 0,
			`hidden` TINYINT(1) NOT NULL DEFAULT 0,
			`name` VARCHAR(255),
			`site_id` INT NOT NULL DEFAULT 0,
			`google` TINYINT,
			`yandex` TINYINT,
			KEY(`site_id`, `number`)
		)";
		$this->getDb()->query($sql);
		
		$sql = "
		CREATE TABLE IF NOT EXISTS `$subjectsTablename` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`number` INT UNSIGNED NOT NULL DEFAULT 0,
			`hidden` TINYINT(1) NOT NULL DEFAULT 0,
			`name` VARCHAR(255),
			`slug` VARCHAR(100),
			`title` VARCHAR(255), 
			`keywords` VARCHAR(255), 
			`description` VARCHAR(500), 
			`annotation` VARCHAR(255), 
			`content`	TEXT,			
			`image` VARCHAR(255),
			`css_class` VARCHAR(20),
			KEY(`slug`)
		)";
		$this->getDb()->query($sql);

		
		$factory = $this->getTableFactory();
		$factory->addTable($subjectsTablename, "Тематики")
			->addStringField("name", "Название")
			->addStringField("slug", "slug")
			->addStringField("title", "Title")
			->addStringField("keywords", "Keywords")
			->addStringField("description", "Description")
			->addTextField("annotation", "Краткое описание")
			->addTextField("content", "Полное описание")
			->addImageField("image", "Картинка", array("draw_into" => "150x120"))
			->addStringField("css_class", "CSS класс");
		$subjtID = $factory->getTableId();
		
		$factory = $this->getTableFactory();
		$factory->addTable($sitesTablename, "Сайты")
				->addStringField("name", "Название")
				->addStringField("url", "URL сайта")
				->addTextField("annotation", "Аннотация")
				->addImageField("image", "Картинка", array("draw_into" => '160x260'))
				->addDescendantField("subject_id", "Тематика", $subjtID);
		$stID = $factory->getTableId();
		
		$factory = $this->getTableFactory();
		$factory->addTable($queriesTablename, "Запросы")
			->addStringField("name", "Запрос")
			->addDescendantField("site_id", "Сайт", $stID)
			->addIntegerField("google", "Google")
			->addIntegerField("yandex", "Yandex");
		$qtID = $factory->getTableId();
		
		$pageFactory = $this->getPageFactory();
		$spID = $pageFactory->addPage("SEO сайты", \PageCreator::SECTION_SITE, $stID);
		$qpID = $pageFactory->addPage("Запросы", \PageCreator::SECTION_SITE, $qtID, null, $spID);
		$spID = $pageFactory->addPage("Тематики", \PageCreator::SECTION_SITE, $subjtID, null, $spID);
		
		$content = file_get_contents(dirname(__FILE__). DIRECTORY_SEPARATOR . "portfolio.html");
		$this->saveTemplate("portfolio", $content);
		$content = file_get_contents(dirname(__FILE__). DIRECTORY_SEPARATOR . "thematics.html");
		$this->saveTemplate("thematics", $content);
		
		$this->installJS();
		
		return true;
	}

	public function remove() {
		$sitesTablename = $this->getSitesTableName();
		$queriesTablename = $this->getQueriesTableName();
		$subjectsTablename = $this->getSubjectsTableName();
		
		$this->removeTable($sitesTablename);
		$this->removeTable($queriesTablename);
		$this->removeTable($subjectsTablename);
		$this->deleteTemplate("portfolio");
		$this->deleteTemplate("thematics");		
		$this->removeJS();
		
		return true;
	}

	public function getConfigFields() {
		return array(
			'last_update_date' => array(
				'type' => 'string', 
				'title' => 'Дата последнего обновления', 
				'default' => null
			) , 
			'cache_lifetime' => array(
				'type' => 'string', 
				'title' => 'Время жизни кеша в сек.', 
				'default' => 3600 * 24
			), 
			'onpage' => array(
				'type' => 'int', 
				'title' => 'Показывать по', 
				'default' => 8
			),
			'top_yandex' => array(
				'type' => 'int',
				'title' => 'Топ для Яндекса',
				'default' => 5
			),
			'top_google' => array(
				'type' => 'int',
				'title' => 'Топ для Google',
				'default' => 5
			),
			'queries_limit' => array(
					'type' => 'int',
					'title' => 'Запросов в карте',
					'default' => 5
			),
			'order' => array(
				'type' => 'enum', 
				'title' => 'Сортировка', 
				'default' => 'number', 
				'values' => array(
					'number' => 'По номеру', 
					'name' => 'По названию'
				)
			), 
		);
	}
	
	public function getJS() {
		return "seo_portfolio.js";
	}
	
	public function getSiteQueries($siteID) {
		$rows = $this->getDb()->fetchAll("SELECT * FROM ".$this->getQueriesTableName()." WHERE site_id = ? AND hidden = 0 ORDER BY number", $siteID);
		
		$queries = array(
			'yandex' => array(), 
			'google' => array()
		);
		
		$yandexCount = 0;
		$googleCount = 0;		
		
		
		/* из всех строк выберем те, которые подходят нам по условиям. 
		 * Расперделим их на yandex и google 
		 */
		if($rows) {
			foreach($rows as $row) {
				if($row['yandex'] && $row['yandex'] <= $this->getConfig('top_yandex')) {
					$queries['yandex'][] = array(
							'name' => $row['name'],
							'position' => $row['yandex']
					);
					$yandexCount ++;
				}
				if($row['google'] && $row['google'] <= $this->getConfig('top_google')) {
					$queries['google'][] = array(
							'name' => $row['name'],
							'position' => $row['google']
					);						
					$googleCount ++;
				}				
			}
		}
		
		$limit = $this->getConfig("queries_limit");
		
		/* если запросов больше, чем надо, то рандомно уберем лишние */
		while($yandexCount > $limit) {
			$key = rand(0, $yandexCount - 1);
			unset($queries['yandex'][$key]);
			$queries['yandex'] = array_values($queries['yandex']);
			$yandexCount --;
		}
		while($googleCount > $limit) {
			$key = rand(0, $googleCount - 1);
			unset($queries['google'][$key]);
			$queries['google'] = array_values($queries['google']);
			$googleCount --;
		}
		
		return $queries;
	}
	
	public function fetchRemote() {
		$ch = curl_init("http://bof5.oridis.ru/api/portfolio_queries/");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		
		$content = curl_exec($ch);
		
		if(curl_errno($ch)) {
			$error = curl_error($ch);			
			throw new ConnectionException("Can't fetch data from bof. Curl error: $error");
		}  else {
			curl_close($ch);
			return $content;
		}
	}
	
	public function import() {
		try {
			$data = $this->fetchRemote();
			$sitesTableName = $this->getSitesTableName();
			$queriesTableName = $this->getQueriesTableName();
			$subjectsTableName = $this->getSubjectsTableName();
				
			$this->getDb()->query("TRUNCATE TABLE `$queriesTableName`");
				
			$sxml = simplexml_load_string($data);
				
			if($sxml) {
					
				$sites = $this->getDb()->fetchPairs("SELECT url, id FROM `$sitesTableName` WHERE hidden = 0");
				$number = 1 + (int) $this->getDb()->fetchOne("SELECT MAX(number) FROM $sitesTableName");
				$qnumber = 1 + (int) $this->getDb()->fetchOne("SELECT MAX(number) FROM $queriesTableName");
				$snumber = 1 + (int) $this->getDb()->fetchOne("SELECT MAX(number) FROM $subjectsTableName");
			
				$subjects = $this->getDb()->fetchPairs("SELECT slug, id FROM `$subjectsTableName` WHERE hidden = 0");
			
				foreach ($sxml->query as $query) {
						
					$siteURL = (string) $query['domain'];
					$yandex = (int) $query['yandex'];
					$google = (int) $query['google'];
						
					$subject = (string) $query['type_slug'];
					
					if($subject) {
						if(!isset($subjects[$subject])) {
							$slug2css = array(
								'clothes' => 'clothing',
								'automotive' => 'autoParts',
								'real_estate' => 'realty',
								'futniture' => 'furniture',
								'landscape_design' => 'landscaping'
							);
							$this->getDb()->insert($subjectsTableName, array(
									'name' => $query['type_name'],
									'slug' => $subject,
									'css_class' => isset($slug2css[$subject]) ? $slug2css[$subject] : null,
									'number' => $snumber ++
							));
							$subjectID = $this->getDb()->lastInsertId();
							$subjects[$subject] = $subjectID;
						} else {
							$subjectID = $subjects[$subject];
						}
					} else {
						$subjectID = null;
					}
						
					if(!isset($sites[$siteURL])) {
						$this->getDb()->insert($sitesTableName, array(
								'name' => $siteURL,
								'url' => $siteURL,
								'annotation' => '<a href="http://'.$siteURL.'">'.$siteURL.'</a>',
								'subject_id' => $subjectID,
								'number' => $number ++
						));
						$siteID = $this->getDb()->lastInsertId();
						$sites[$siteURL] = $siteID;
					} else {
						$siteID = $sites[$siteURL];
					}
						
					if($yandex || $google) {
						$this->getDb()->insert($queriesTableName, array(
								'name' => (string) $query,
								'site_id' => $siteID,
								'yandex' => $yandex,
								'google' => $google,
								'number' => $qnumber ++
						));
					}
				}
			
				$this->getDb()->delete($sitesTableName, array("id NOT IN (?)" => array_values($sites)));
			
				$this->setConfig("last_update_date", date('Y-m-d H:i:s'));
			} else {
				$this->getDi()->get("logger")->log("Query import error: non-xml file");
			}
		} catch (ConnectionException $ex) {
			$this->getDi()->get("logger")->log($ex->getMessage());
		}
	}
	
	
	
	/**
	 * @return \Zend_Db_Select
	 */
	public function prepareSelect() {
		$validSitesIds = $this->getValidSitesIds();
		$select = $this->getDb()->select()
			->from($this->getSitesTableName())
			->where("hidden = 0")
			->order("number");
		if($validSitesIds) {
			$select->where("id IN (?)", $validSitesIds);
		} else {
			$select->where("1 = 0");
		}
		return $select;
	}
	
	protected function getValidSitesIds() {
		$select = $this->getDb()->select()
			->distinct(true)
			->from($this->getQueriesTableName(), array('site_id'))
			->where("yandex <= ?", $this->getConfig("top_yandex"))
			->orWhere("google <= ?", $this->getConfig("top_google"))
			->order("site_id");
		$ids = $this->getDb()->fetchCol($select);
		return $ids;
	}
	
	public function sites($options, $content) {
		
		$onpage = isset($options['onpage']) ? $options['onpage'] : $this->getConfig("onpage");
		$order = isset($options['order']) ? $options['order'] : $this->getConfig("order");
		$pagenumber = isset($options['p']) ? $options['p'] : 1;
		$template = $content ? $content : $this->getTemplate("portfolio");
		$withTotal = !(isset($options['without_total'])  &&  $options['without_total']);
		
		$select = $this->prepareSelect();
		
		if($withTotal) {
			$totalSelect = clone $select;
			$totalSelect->reset(\Zend_Db_Select::COLUMNS);
			$totalSelect->columns(array("count" => "COUNT(*)"));
			
			$total = $this->getDb()->fetchOne($totalSelect);
			
			if($total == 0 || $this->cacheExpired()) {
				/* и все же несовсем это правильно, импортировать прям во время запроса. Надо по крону */
				$this->import();
			}
			
			$pagesCount = ceil($total / $onpage);
			$pages = array();
			for($i = 1; $i <= $pagesCount; $i ++) {
				$onThisPage = $onpage;
				if($i == $pagesCount) {
					$onThisPage = $total - ($i - 1) * $onpage;
				}
				$pages[] = $onThisPage;
			}
			
			$jsCode = '<script>
				var p = new Module_SeoPortfolio(['.implode(',', $pages).']);
				p.setOnpage('.$onpage.');
				$(function () {p.init();});
				</script>
			';
		}
		$select->order($order);
		$select->limit($onpage, $onpage * ($pagenumber - 1));
		$sites = $this->getDb()->fetchAll($select);
		$sites = $this->fillSites($sites);
		
		$html = $this->getDi()->get("lex")->parse($template, array('sites' => $sites));
		if(isset($jsCode)) {
			$html .= $jsCode;
		}		
		
		return $html;
	}
	
	public function showMoreButton($options, $content) {
		return '<!--noindex--><a class="type-button" href="#" id="ShowMorePortfolio" rel="nofollow">Показать еще (<span class="more_count">0</span>)</a><!--/noindex-->';
	}
	
	public function cacheExpired() {
		$lastUpdateDate = $this->getConfig("last_update_date");
		$cacheLifetime = $this->getConfig("cache_lifetime");
		if(empty($lastUpdateDate) || time() - strtotime($lastUpdateDate) > $cacheLifetime) {
			return true;
		} else {
			return false;
		}
	}
	
	public function fillSites($sites) {
		if($sites) {
			require_once "models/Table/DbTableLight.php";
			$table = new \DbTableLight($this->getDb(), $this->getSitesTableName());
			$imageField = $table->getField("image");
			foreach($sites as $key => $site) {
				$site['image'] = $imageField->getURL($site);
				$site['queries'] = $this->getSiteQueries($site['id']);
				if(empty($site['queries']['yandex']) && empty($site['queries']['google'])) {
					unset($sites[$key]);
				}  else {
					$sites[$key] = $site;
				}				
			}
		}
		
		return $sites;
	}

	public function more() {
		$request = $this->getRequest();
		
		$onpage = $request->getQuery("onpage", $this->getConfig("onpage"));
		$pageNumber = $request->getQuery("p", 1);
		
		$html = $this->sites(array(
			'without_total' => true, 
			'onpage' => $onpage, 
			'p' => $pageNumber
		), null);
		
		return $html;
	}
	
	
	public function thematics($options, $template = null) {
		$select = $this->getDb()->select()
			->from($this->getSubjectsTableName(), array('id', 'slug', 'image', 'css_class', 'name', 'annotation'))
			->where("hidden = 0")
			->order("number");
		$rows = $this->getDb()->fetchAll($select);
		if($rows) {
			$template = $template ? $template : $this->getTemplate("thematics");
			require_once "models/Table/DbTableLight.php";
			$table = new \DbTableLight($this->getDb(), $this->getSubjectsTableName());
			$imageField = $table->getField("image");
			foreach($rows as $key => $row) {
				$rows[$key]['href'] = '/seo/thematics/'.$row['slug'].'/';
				$rows[$key]['title'] = '/seo/thematics/'.$row['name'].'/';
				$rows[$key]['image'] = $imageField->getURL($row);
			}
			return $this->getDi()->get("lex")->parse($template, array('items' => $rows));
		} else {
			return "";
		}
	}
	
	public function route(\front\models\Page $page, $request) {
		$parts = $page->getChildURLParts();
		$partsLength = count($parts);
		if($partsLength == 1) {
			$thematic = $this->findThematicBySlug($parts[0]);
			if($thematic) {
				$singlePage = $page->findChildPage("single");
				if($singlePage) {
					$singlePage['title'] = $thematic['title'];
					$singlePage['keywords'] = $thematic['keywords'];
					$singlePage['description'] = $thematic['description'];
					$singlePage['menu_name'] = $thematic['name'];
					
					$thematic['content'] = $this->getDi()->get("lex")->parse($thematic['content'], $thematic);
					$singlePage['thematic'] = $thematic;
					return $singlePage;
				} else {
					throw new \PageNotFoundException("Текущий адрес соответствует существующей тематике, но не указана страница single для рендеринга");
				}
			}
		}
	}
	
	public function findThematicBySlug($slug) {
		$select = $this->getDb()->select()
			->from($this->getSubjectsTableName())
			->where("hidden = 0")
			->where("slug = ?", $slug)
			->order("number");
		return $this->getDb()->fetchRow($select);
	}
}

class ConnectionException extends \Exception {
	
}

?>