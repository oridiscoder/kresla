<?php
namespace front\modules;

use front\models\Orm;

use front\modules\Cluster\models as ClusterModels;

use front\modules\Cluster\models\Clusters;
use front\modules\Cluster\models\Router;

use front\modules\CatalogKresla\models\Upholstery;

use front\modules\CatalogKresla\models\UpholsteryType;

use front\models\ModuleNotFoundException;

require_once ('front/modules/Module.php');

	 function sortByNumberAsc($a, $b) {
        return $a['order'] - $b['order'];
    }
	 function sortByNumberDesc($b, $a) {
        return $a['order'] - $b['order'];
    }
	 function sortByPriceAsc($a, $b) {
        return $a['price'] - $b['price'];
    }
	 function sortByPriceDesc($b, $a) {
        return $a['price'] - $b['price'];
    }

    
class Cluster extends Module {
	
	/**
	 * 
	 * @var \front\modules\Cluster\models\Page
	 */
	private $page;
	/**
	 * 
	 * @var \front\modules\Cluster\models\Cluster
	 */
	private $cluster;
	
/* (non-PHPdoc)
	 * @see front\modules.Module::getTitle()
	 */
	public function getTitle() {
		return "Кластеры";
	}

/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
		"Модуль для отображения содержимого класетеров";
	}

/* (non-PHPdoc)
	 * @see front\modules.Module::install()
	 */
	public function install() {
		return true;		
	}

/* (non-PHPdoc)
	 * @see front\modules.Module::remove()
	 */
	public function remove() {
		return true;		
	}

/* (non-PHPdoc)
	 * @see front\modules.Module::getConfigFields()
	 */
	public function getConfigFields() {
		
	}
	
	/**
	 * @return \front\modules\Cluster\models\Cluster
	 */
	protected function getCluster() {
		return $this->cluster;
	}
	/**
	 * 
	 * @param \front\modules\Cluster\models\Cluster $cluster
	 */
	protected function setCluster($cluster) {
		$this->cluster = $cluster;
	}
	
	
	
	public function meta($options, $template) {
		$name = $options['name'];
		$meta = $this->getCluster()->getMeta();
		switch($name) {
			case "title": 
				return $meta['title'] ? $meta['title'] : $this->getCluster()->getTitle();
			case "keywords":
				return $meta['keywords'] ? $meta['keywords'] : $this->getCluster()->getKeywords();
			case "description":
				return $meta['description'] ? $meta['description'] : $this->getCluster()->getDescription();
			case "h1":
				return $meta['h1'] ? $meta['h1'] : $this->getCluster()->getH1();
				
		}
	}
	
	
	
	
	private function initPageMeta() {
		$meta = $this->getCluster()->getMeta();
		
		/* @var $page \front\models\Page */
		$page = $this->getPage();
		
		/* инициализация метатегов */
		$title = "";
		if($meta && $meta['title']) {
			$title = $meta['title'];
		} else {
			$title = $this->getCluster()->getTitle();
		}
		
		$page->setTitle($title);
		$page->setMenuName($title);
		
		if($meta && $meta['keywords']) {
			$page->setKeywords($meta['keywords']);
		} else {
			$page->setKeywords($this->getCluster()->getKeywords());
		}
		
		if($meta && $meta['description']) {
			$page->setDescription($meta['description']);
		} else {
			$page->setDescription($this->getCluster()->getDescription());
		}
		
		if($meta && $meta['h1']) {
			$page->setH1($meta['h1']);
		} else {
			$page->setH1($this->getCluster()->getH1());
		}
		
		if($meta) {
			$page->setSeoText($meta['text']);
		}
	}
	
	public function render($options, $template) {
		
		$methodName = $this->getCluster()->getModuleName();
		$methodName = $methodName ? $methodName : 'renderGoods';
		
		if(method_exists($this, $methodName)) {
			$this->initPageMeta();
			return call_user_func(array($this, $methodName));
		} else {
			throw new ModuleNotFoundException("У модуля Cluster не найден метод $methodName");
		}
	}
	
	public $goods;
	


	public function loadGoods(){
        $order = $this->getDi()->getModule("catalogKresla")->getOrder();

		if(!$this->goods){
			$ids = $this->getTargetTableIds($this->getCluster());
			
			if($ids) {
				$orm = new Orm("goods");
				$this->goods = $orm->find(array("id IN (?)" => $ids))->asArray();
			} else {
				$this->goods = array();
			}
		}
		$arr = $this->goods;
		if (in_array("price", $order)) {
                if (in_array("ASC", $order)) {
                            usort($arr, function($a, $b) {
                                return $a['price'] - $b['price'];
                            });
                }else{
                            usort($arr, function($b, $a) {
                                return $a['price'] - $b['price'];
                            });
                }
        }elseif (in_array("number", $order)) {
                if (in_array("ASC", $order)) {
                            usort($arr, function($a, $b) {
                                return $a['number'] - $b['number'];
                            });
                }else{
                            usort($arr, function($b, $a) {
                                return $a['number'] - $b['number'];
                            });
                }
            
        }
		$this->goods = $arr;
		return $this->goods;
	}
	

	public function renderGoods() {
		
		
		
		$this->getPage()->setBreadcrumbs(array(
				array('href'=>'/', 'title'=>'Главная'),
				array(
							"title" => $this->getPage()->getH1(),
							"href" => $this->getCluster()->getUrl()
					)
		));
		return $this->getDi()->getModule("catalogKresla")->renderGoods($this->loadGoods());
			
	}
	
	public $seotext;
	public function getSeotext(){
		if($this->seotext)
			return $this->seotext;
		
		$count = count($this->loadGoods());
		
		$category = false;
		$material = false;
		$color= false;
		$this->seotext = false;
		//расчленим клайстер
		
		foreach($this->getCluster()->getData() as $cl){
			if(array_key_exists('description', $cl)){
				$category = $cl;
			}elseif(array_key_exists('code', $cl)){
				$material = $cl;
			}elseif( array_key_exists('class_name', $cl)){
				$color = $cl;
			}
		}
		
		$category['trueName'] = mb_strtolower( $this->getDi()->getPlugin('Spellcount')->spellcountWO(array('value'=>$count, 'words'=>$category['title2']), 'Второй аргумент надо, ато варнинг') );
		$mat = $material ? ' из '.$material['title_parent'] :'';
		
		if($color){
			$osn = mb_strtolower( mb_substr($color['title'], 0, mb_strlen($color['title'])-2 ) );
	
			if(mb_substr($color['title'], mb_strlen($color['title'])-2, 1) == 'ы'){
				$words = $category['end_color'].',ых,ых';
			}else{
				if($category['end_color']=='ое') $c='ее';
				else $c='ий';
				$words = $c.',их,их';
			}
			$end = $this->getDi()->getPlugin('Spellcount')->spellcountWO(array('value'=>$count, 'words'=>$words), 'Второй аргумент надо, ато варнинг');
			
			$col = $osn.$end;;
		}else{
			$col = '';
		}
		$mat= $mat ? ' '.$mat:'';
		$Texts[]="Мы нашли для вас $count $col {$category['trueName']}$mat: в наличии и на заказ!";
		$Texts[]="В нашем каталоге представлено $count $col {$category['trueName']}$mat: в наличии и на заказ!";
		$Texts[]="$count $col {$category['trueName']}$mat  в наличии и на заказ представлено в нашем каталоге.";
		
		$check = $category['id'].$material['id'].$color['id'];
		
		$this->seotext = $Texts[$check % 3];
		$this->getCluster()->setDescription(mb_substr($this->seotext, 0, 200));
		return $this->seotext;
		
	}
	
	public function renderLinks($options, $content) {
		$variants = $this->getCluster()->getVariants();
		$html = '';
		foreach ($variants as $variant) {
			$html .= '<a href="/'.$variant.'/">/'.$variant.'</a><br />';
		}
		return $html;
	}
	
	/**
	 * Получить id записей из таблицы-источника, удовлетворяющих фильтрам заданного кластера
	 * @param \front\modules\Cluster\models\Cluster
	 * @return array
	 */
	private function getTargetTableIds($cluster) {
		
		$data = $cluster->getData();
// 		if($_SERVER['REMOTE_ADDR'] == '80.90.120.182'){
// 			die('<pre>'.print_r( $data,1));
// 		}
		
		/* получим таблицу - источник данных */
		$tagretTableID = $cluster->getTargetTableID();
		$targetTableIds = null;
		require_once "models/Table/DbTable.php";
		\BuildTimer::timing("Creating DbTable");
		$table = new \DbTable($this->getDb(), $tagretTableID);
		\BuildTimer::timing("DbTable created");
		
		for ($i = 0; $i < 3; $i++) {
				
			/**
			 * $object - это одна из частей кластера = [id, name, field_name]
			 * id - id таблицы
			 * name - назнвание таблицы
			 * field_name - название url-поля таблицы
			 * @var array
			 */
			$object = $cluster->getObject($i);
				
			if($object) {
				
				if($object['name'] == 'clusters_table_filters') {
					/* фильтруем строки таблицы-источника данных по значению её собственных полей */
					$fieldID = $data[$i]['field_id'];
					
					foreach ($table->getFields() as $field) {
						if($field->getID() == $fieldID) {
							
							$fieldValue = $data[$i]['value'];
							if(strpos($fieldValue, "?") !== false) {
								$fieldWhere = str_replace("?", $field->getName(), $fieldValue);
							} else {
								$fieldWhere = $field->getName().' = "'.$fieldValue.'"';
							}
							
							$select = $this->getDb()->select()
								->from($table->getName(), array("id"))
								->where("hidden = 0")
								->where($fieldWhere);
							
							$ids = $this->getDb()->fetchCol($select);
							$ids = $ids ? $ids : array();
							
							if(is_array($targetTableIds)) {
								$targetTableIds = array_intersect($ids, $targetTableIds);
							} else {
								$targetTableIds = $ids;
							}
							
							break;
						}
					}
					
				} else {
		
					$chain = array();
					\BuildTimer::timing("Find own fields");
					/*
					 * 1 вариант. У меня есть поле-потомок, указывающее на текущий объект $object
					*/
					foreach ($table->getFields() as $field) {
						if($field instanceof \DescendantField && $field->getParentTableId() == $object['id']) {
							$table->setFieldValue($field->getName(), $data[$i]['id']);
							$rows = $table->getRows();
							$ids = \Utils::extractColumn($rows, "id");
			
							if(is_array($targetTableIds)) {
								$targetTableIds = array_intersect($ids, $targetTableIds);
							} else {
								$targetTableIds = $ids;
							}
			
							$chain[] = $field;
							break;
						}
					}
					\BuildTimer::timing("stop find own fields");
			
					if(!$chain) {
						/*
						 * 2 вариант. Связь таблиц = N-M и осуществляется через связующую таблицу.
						* Найдем сначала все поля, которые ссылаются на меня. Возможно одно из них - это поле
						* связующей таблицы.
						*/
						
						$select = $this->getDb()->select()
							->from("clusters_chains")
							->where("hidden = 0")
							->where("target_id = ?", $table->getID())
							->where("object_id = ?", $object['id'])
							->order("number");
						$chainRows = $this->getDb()->fetchAll($select);
						
						if($chainRows) {
							foreach ($chainRows as $chainRow) {
								$field = $this->getDb()->fetchRow("SELECT * FROM bof_fields WHERE id = ?", $chainRow['field_id']);
								$field['_db_adapter'] = $this->getDb();
								$chain[] = \DbFieldAbstract::factory($field);
							}
						} else {
						
							$select = $this->getDb()->select()
								->from("bof_fields")
								->where("hidden = 0")
								->where("param = ?", $table->getID())
								->where("obj_id <> ?", $table->getID())	//уберем ссылки на самого себя
								->where("type = ?", \DbFieldAbstract::TYPE_DESCENDANT)
								->order("number");
							\BuildTimer::timing("Find children");
							$fields = $this->getDb()->fetchAll($select);
							\BuildTimer::timing("Children found");
								
							if($fields) {
								\BuildTimer::timing("Looking for chain");
								foreach ($fields as $field) {
									/*
									 * По каждой зависимой таблице будем осуществять рекурсивный поиск.
									* Возможно, одна из цепочек полей-потомков приведёт нас к объекту $object
									*/
									$subChain = $this->findLinkedTable($field['obj_id'], $object['id']);
									if($subChain) {
										/*
										 * Если $subChain не пуста, значит, мы нашли цепочку полей-потомков, которые
										* приведут нас к объекту $object
										*/
										$field['_db_adapter'] = $this->getDb();
										$chain[] = \DbFieldAbstract::factory($field);
										$chain = array_merge($chain, $subChain);
										break;
									}
								}
								\BuildTimer::timing("Stop chain searching");
							}
							
							if($chain) {
								$number = 1 + (int)$this->getDb()->fetchOne("SELECT MAX(number) FROM clusters_chains");
								foreach ($chain as $field) {
									$this->getDb()->insert("clusters_chains", array(
										'target_id' => $table->getID(), 
										'object_id' => $object['id'], 
										'field_id' => $field->getID(), 
										'number' => $number ++
									));
								}
							}
						}
							
						/*
						 * Если у нас найдена цепочка потомков, то теперь надо в обратном порядке осуществлять поиск
						* по таблицам этих полей. В начале у нас дан id записи из таблицы, на которую указывает последний потомок из $chain
						* т.е значение поля end($chain)->getFieldName() должно равняться $data[$i]['id'], где $data - это данные кластера
						*/
						if($chain) {
							//echo '<pre>';
							$ids = array($data[$i]['id']);
			
							$j = count($chain) - 1;
							\BuildTimer::timing("Start filtering");
			
							/* @var \DescendantField $field */
			
							/*
							 * Идем вниз по цепочке пока поле в цепочке не указывает на таблицу-источник данных
							*/
							while($chain[$j]->getParentTableId() != $table->getID() && $ids) {
								$field = $chain[$j];
								$fieldTable = $field->getTable();
								$select = $this->getDb()->select()
								->from($fieldTable['name'], array('id'))
								->where("hidden = 0")
								->where($field->getName().' IN (?)', $ids);
								$ids = $this->getDb()->fetchCol($select);
								$j--;
							}
			
							/*
							 * Последний из $chain указывает на таблицу-источник данных.
							* Если на каком-то этапе предыдущего цикла $ids стало пустым, это значит,
							* что цепь разорвана и в таблице-истонике данных искать уже нечего
							*/
							if($ids) {
								$field = $chain[$j];
								/*
								 * Пример. Вытащить столбец  good_upholsteries.good_id где  good_upholsteries.id IN ($ids)
								*/
								$select = $this->getDb()->select()
								->distinct(true)
								->from($fieldTable['name'], array($field->getName()))
								->where("hidden = 0")
								->where('id IN (?)', $ids);
								$ids = $this->getDb()->fetchCol($select);
								$ids = $ids ? $ids : array();
									
								if(is_array($targetTableIds)) {
									$targetTableIds = array_intersect($ids, $targetTableIds);
								} else {
									$targetTableIds = $ids;
								}
							}
							\BuildTimer::timing("Stop Filtering");
						} else {
							/* Вообще такого варианта быть не должно */
							$targetTableIds = array();
						}
					}
				}
			}
		}
// 		print_r();
		if(!count($targetTableIds)){
				$sql="SELECT * FROM  `oldPages` WHERE  `name` =  '".$_SERVER['REQUEST_URI']."'";
				$db = $this->getDb();
				$c=$db->fetchAll($sql);
				if(!count($c)){
					throw new \Exception("Страница не найдена", 404);
				}
		}
		return $targetTableIds;
	}
	
	/**
	 * Рекурсивный поиск таблицы в потомках указанной таблицы. 
	 * @param integer $sourceID - ID таблицы, у которой ищем
	 * @param integer $needleID - ID таблицы, которую ищем
	 * @param array $chain - цепочка объектов DescendantFields до найденной таблицы
	 */
	private function findLinkedTable($sourceID, $needleID, $chain = array()) {
		$table = new \DbTable($this->getDb(), $sourceID);
		echo $table->getName().":\n";
		foreach($table->getFields() as $field) {
			if($field instanceof \DescendantField) {
				echo "\t".$field->getName().' => '.$field->getParentTableName()."\n";
				if($field->getParentTableId() == $needleID) {
					$chain[] = $field;
					echo "OK\n";
					return $chain;
				} elseif($field->getParentTableId() != $table->getID()) {
					$subChain = $this->findLinkedTable($field->getParentTableId(), $needleID);
					if($subChain) {
						$chain[] = $field;
						$chain = array_merge($chain, $subChain);
						return $chain;
					}
				}
			}
		}
		echo "/".$table->getName()."\n";
	}
	
	public function getSolutions($options, $template) {
		$clusterModel = new Clusters();
		$clusters = $clusterModel->getClusters();
		
		require_once "models/Cluster.php";
		
		$links = array();
		
		if($clusters) foreach ($clusters as $cluster) {
			$cluster = new ClusterModels\Cluster($cluster);
			if(($meta = $cluster->getMetaList()) != null) {
				foreach ($meta as $row) {
					$links[] = array(
						"url" => $row['url'], 
						"title" => $row['title']
					);
				}
			}
		}
		
		return $links;
		
	}
	
	public function route(\front\models\Page $page, $request) {
	
		$router = new Router();
		if(($cluster = $router->match($request->getPathInfo())) != null) {
			$this->setCluster($cluster);
			return $page->findChildPage("cluster");
		} else {
			$parts = explode('/', trim($request->getPathInfo(), '/'));
			
			if(isset($parts[3]) && preg_match('#.html#',$parts[3])!=1){

				//0 - catalog
				//1 - parent category
				//2 - category
				$cluster = $router->match('/'.implode('/', array_slice($parts, 2)).'/');

				$ck = new \front\modules\CatalogKresla();

				
					
				
				if(!$cluster){

					$goods = $ck->getGTables();
					

					if($goods){
						// echo 123123; die;
						$p = $page->find(21);
						// echo '<pre>'; print_r($p); echo '</pre>';
						// die;
						$p['content'] = str_replace('{{ module.catalogKresla.goods category="{{page.category.id}}" /}}','{{ module.catalogKresla.goods tablegoods="'.$goods.'" }} {{page.seotext}}',$p['content']);
						$ad = '';
						$ssql = '';
						$i=0;
						$p['curtables'] = $goods;

// 247
// 3
// До 5000р.
// 6
// 0

// 248
// 3
// От 5000р.
// 7
// 0

// 249
// 4
// Большой (ш. от 100)
// 8
// 0

// 250
// 4
// Компактный (ш. до 100)
// 9
// 0
// Размер Тип компьютерные столы Материал Цвет Цена
// 1) Тип идет до "компьютерные столы"
// угловые
// письменные
// детские
// 
// 2) тип идет после "компьютерные столы"
// для ноутбука
// на колесиках
// с надстройкой
// хай-тек
// трасформеры
// со стеллажом
// 10	Со стеллажом	so_stellazhom	          
// 	9	Трансформер	transformer	          
// 	8	Хай-тек	hi-tech	          
// 	7	С надстройкой	s_nadstroikoi	          
// 	6	На колесиках	na_kolesikah	          
// 	5	Детские	detskie	          
// 	4	Для ноутбука	dlya_noutbuka	          
// 	3	Письменные	pismennye	          
// 	2	Угловые	uglovye	          
// 	Итого	 	 	
// показать удаленные
//       
						if(!$ck ->tableSeoItem){

							if(strstr($_SERVER['REQUEST_URI'],'zhurnalnye-stoliki')){
								$sbase = 'журнальные';
								$journal = true;
// 							}elseif(strstr($_SERVER['REQUEST_URI'],'/catalog/home/tables/')){
							}else{
								$sbase = 'компьютерные';
								$journal = false;
							}
							$so = array();
							$so['title'] = array(	
											'0'=> 'sizes',
											'1'=> 'types',
											'2'=> 'shapes',
											'3'=> 'stitle',
											'4'=> 'materials',
											'5'=> 'colors',
											'6'=> 'prices');
// 							компьютерные столы, размер, тип, материал, цвет, цена
							$so['keyw'] = array(	
											'0'=> 'stitle',
											'1'=> 'sizes',
											'2'=> 'shapes',
											'3'=> 'types',
											'4'=> 'materials',
											'5'=> 'colors',
											'6'=> 'prices');	
							$so['desc'] = array(	
											'0'=> 'Вашему вниманию предлагаются',
											'1'=> 'В нашем каталоге представлены',
											'2'=> 'Ознакомьтесь с представленными товарами категории',
											'3'=> 'В наличии и на заказ');
											
											



									$ch = '';
							if(isset($ck->tableFilterCurrent['types'])){
								
								$ti = $ck->tableFilterCurrent['types']['id'];
								if($ti != '2' && $ti != '3' && $ti != '5'){
										$so['title']['1'] = 'stitle';
										$so['title']['3'] = 'types';
								}
							}
											
							$at = '';
							$ak = '';
// 							die('<pre>'.print_r( $so,1));
							foreach($so['title'] as $par){
								if(isset($ck->tableFilterCurrent[$par])){
									$ch = $ch.$ck->tableFilterCurrent[$par]['url'];
									$at .= mb_strtolower($ck->tableFilterCurrent[$par]['title']) .' ';
								}elseif($par == 'stitle'){
									if(!$journal){
									
									$at .= $sbase.' столы ';
									}else{
										if(!$journal){
											
											$at .= $sbase.' столы ';
										}else{
											$at .= $sbase.' столики (столы) ';
// 											$ah1 .= $sbase.' столики (столы) ';
										}
									}
								}
							}

							foreach($so['keyw'] as $par){
								if(isset($ck->tableFilterCurrent[$par])){
									$ak .= ' '.mb_strtolower($ck->tableFilterCurrent[$par]['title']) .',';
								}elseif($par == 'stitle'){
									$ak .= $sbase.' столы,';
									
								}
							}							
							$ak = mb_substr($ak,0,mb_strlen($ak)-1);
							$at = str_replace('большой','большие',$at);
							$at = str_replace('компактный','компактные',$at);
							
							$at = str_replace('круглый','круглые',$at);
							$at = str_replace('овальный','овальные',$at);
							$at = str_replace('квадратный','квадратные',$at);
							$at = str_replace('прямоугольный','прямоугольные',$at);
							
							
							
							
							$ak = str_replace('большой','большие',$ak);
							$ak = str_replace('компактный','компактные',$ak);							
							$at = str_replace('дсп','из ДСП',$at);
							$at = str_replace('стекло','из стекла',$at);
							$at = str_replace('металл','из металла',$at);
							$at = str_replace('пластик','из пластика',$at);
							$at = str_replace('','',$at);
							$at = str_replace('','',$at);
							$at = str_replace('','',$at);
							$rc = (crc32($ch))%count($so['desc']); 
							$ad = $so['desc'][$rc].' '.$at;
							$at = $this->my_ucfirst($at);
							$ah1 = $at;
							if($journal){
								$at .= ' купить в Москве в интернет-магазине';
							}
/*4) Содержимое тэга keywords по умолчанию сделать:
компьютерные столы, размер, тип, материал, цвет, цена

5) Содержимое тэга description по умолчанию сделать:
Рандомно кусок текста из перечисленных ниже + title:
Вашему вниманию предлагаются
В нашем каталоге представлены
Ознакомьтесь с представленными товарами категории
В наличии и на заказ
---
Для одной страницы этот кусок текста должен быть постоянен.
В результате автогенерации мы получим дескрипшены вида:
"Вашему вниманию предлагаются Большие компьютерные столы от 5000р"

6) На страницы со списком товаров вывести заголовок h1 = title
вывести куда-то сюда http://take.ms/tMYkM*/							
							
// 							foreach ($ck->tableFilterCurrent as $k => $v) {
// 								if($k == 'sizes'){
// 
// 								}elseif($k == 'prices'){
// 
// 								}
// 
// 
// 								// $ssql .=
// 								if(!$i){
// 									$ad .= $v['title']. ' ';
// 									$i++;
// 								}else{
// 									$ad .= mb_strtolower($v['title']) .' ';
// 								}
// 							}
// 							
// 							echo $ad; die;
// 							С надстройкой яблоня локарно дсп компактный от 5000 р
// 							
// 							по умолчанию title = Размер Тип компьютерные столы Материал Цвет Цена
// 
// Меняем для тайтлов:
// большой -> Большие
// компактный -> Компактные
// 
// дсп -> из ДСП
// стекло -> из стекла
// металл -> из металла
// пластик -> из пластика
// 
// Тип (это уже опциально, поправим точечно руками если сложно)
// 1) Тип идет до "компьютерные столы"
// угловые
// письменные
// детские
// 
// 2) тип идет после "компьютерные столы"
// для ноутбука
// на колесиках
// с надстройкой
// хай-тек
// трасформеры
// со стеллажом
							$p['title'] = $at . ' - Кресла От и До';
							$p['description'] = $ad;
							$p['keywords'] = $ak ;
							$p['category']['title'] = $at;
							if($journal){
								$p['category']['title'] = $this->my_ucfirst($ah1);
								
							}	
							$p['title'] = str_replace('  ',' ',$p['title']);
// 							echo $p['content']; die;
// 							$p['content'] = str_replace('<h1>{{page.category.title}}</h1>','<h1>{{page.category.title}}</h1><br>',$p['content']);

						}else{
// 						die('<pre>'.print_r( $ck ->tableSeoItem,1));
							if($ck ->tableSeoItem['h1'] != ''){
							
								$p['category']['h1'] = $ck ->tableSeoItem['h1'];
							}
							$p['title'] = $ck ->tableSeoItem['seo_title'];
							$p['category']['title'] = $p['title'];
							$p['description'] = $ck ->tableSeoItem['seo_desciption'];
							$p['keywords'] = $ck ->tableSeoItem['seo_keywords'];
// 							$p['content'] = str_replace('<h1>{{page.category.title}}</h1>','<h1>{{page.category.title}}</h1><br>',$p['content']);
							$p['seotext'] = '<div>'.$ck ->tableSeoItem['seo_text'].'</div>';
	
						}

							
// die('<pre>'.print_r( $p,1));
						return $p;
						
					}else{
						throw new \Exception("Страница не найдена", 404);
					}

				}
				
				$this->setCluster($cluster);
				return $page->findChildPage("cluster");
			}
			
			
			
			
			return parent::route($page, $request);
			
		}
	}
	
	/**
	 * @return \phpMorphy
	 */
	public function phpMorphy() {
		require_once('phpmorphy/src/common.php');
		
		$opts = array(
				// storage type, follow types supported
				// PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
				// PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
				// PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
				'storage' => PHPMORPHY_STORAGE_FILE,
				// Enable prediction by suffix
				'predict_by_suffix' => true,
				// Enable prediction by prefix
				'predict_by_db' => true,
				// TODO: comment this
				'graminfo_as_text' => true,
		);
		
		$dir = realpath(APPLICATION_PATH . '/../library/phpmorphy/dicts');
		$lang = 'ru_RU';
		
		$morphy = new \phpMorphy($dir, $lang, $opts);
		
		return $morphy;
	}
	
	private function my_ucfirst($string, $e ='utf-8') { 
        if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string)) { 
            $string = mb_strtolower($string, $e); 
            $upper = mb_strtoupper($string, $e); 
            preg_match('#(.)#us', $upper, $matches); 
            $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e); 
        } else { 
            $string = ucfirst($string); 
        } 
        return $string; 
    } 
	
}


?>
