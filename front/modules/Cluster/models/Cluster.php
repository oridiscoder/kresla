<?php
namespace front\modules\Cluster\models;

class Cluster extends \front\models\Model {
	
	private $cluster;
	
	public function __construct($cluster) {
		parent::__construct();
		$this->cluster = $cluster;
	}
	
	public function getId() {
		return $this->cluster['id']; 
	}
	
	public function getName() {
		return $this->cluster['name'];
	}
	public function getUrl() {
		$parts = array();
		if($this->getUrlPrefix()) {
			$parts[] = $this->getUrlPrefix();
		}
		
		
		foreach ($this->getData() as $i => $row) {
			$number = $i + 1;
			$fieldName = $this->cluster['object_'.$number.'_field_name'];
			$parts[] = $row[$fieldName];
		}
		
		return '/'.implode("/", $parts).'/';
	}
	public function getUrlPrefix() {
		return $this->cluster['url_prefix'];
	}
	/**
	 * Получить объект (таблицу) заданного уровня кластера
	 * @param int $number - уровень кластера. от 1 до 3х
	 */
	public function getObject($number) {
		$number = $number + 1;
		if(isset($this->cluster['object_'.$number]) && $this->cluster['object_'.$number]) {
			return array(
				"id" => $this->cluster['object_'.$number], 
				"name" => $this->cluster['object_'.$number.'_name'],
				"field_name" => $this->cluster['object_'.$number.'_field_name'],
			);
		}
	}
	public function getTargetTableID() {
		return $this->cluster['target_id'];
	}
	
	public function getModuleName() {
		return $this->cluster['module_name'];
	}
	public function getParentId() {
		return $this->cluster['parent_id'];
	}
	public function getPages() {
		return $this->cluster['_pages'];
	}
	public function getData() {
		return $this->cluster['_data'];
	}
	/**
	 * Преобразовать данные кластера в формат для вставки в шаблоны: заголовка, h1 и т.д.
	 * @return array
	 */
	private function getDataForTemplate() {
		$data = array();
		foreach($this->getData() as $i => $object) {
			foreach ($object as $key => $value) {
				$dataKey = ($i + 1).'.'.$key;
				$data[$dataKey] = $value;
			}
		}
		return $data;
	}
	/**
	 * Вставить в шаблон данные
	 * @param string $template
	 * @param array $data - массив вида [1.name=kresla, 2.name_parent=iz_kozhi]
	 * @return string
	 */
	private function parseTemplate($template, $data) {
		/* @var $parser \front\models\LightParser */
		$parser = $this->getDi()->get("LightParser");
		return $parser->insertValues($template, $data);
	}
	 
	public function getTitle() {
		return $this->mb_ucfirst($this->parseTemplate($this->cluster['template_title'], $this->getDataForTemplate()));
	}
	public function getKeywords() {
		return $this->mb_ucfirst($this->parseTemplate($this->cluster['template_keywords'], $this->getDataForTemplate()));
	}
	public function getDescription() {
		return $this->description ? $this->description :mb_substr($this->mb_ucfirst($this->parseTemplate($this->cluster['template_description'], $this->getDataForTemplate())), 0, 200);
	}
	public function getH1() {
		return $this->mb_ucfirst($this->parseTemplate($this->cluster['template_h1'], $this->getDataForTemplate()));
	}
	
	public $description;
	public function setDescription($descr){
		$this->description = $descr;
	}
	
	function mb_ucfirst($str, $encoding='UTF-8')
	{
		$str = mb_strtolower($str, $encoding);
		$str = mb_ereg_replace('^[\ ]+', '', $str);
		$str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).
		mb_substr($str, 1, mb_strlen($str), $encoding);
		return $str;
	}
	
	/**
	 * Получить список ассоцированных с текущим кластером метатегов
	 * @return array
	 */
	public function getMetaList() {
		$select = $this->getDb()->select()
			->from("clusters_meta")
			->where("hidden = 0")
			->where("cluster_id = ?", $this->getId());
		
		 return $this->getDb()->fetchAll($select);
	}
	
	public function getMeta() {
		if(!isset($this->cluster['_meta'])) {
			$select = $this->getDb()->select()
				->from("clusters_meta")
				->where("hidden = 0")
				->where("cluster_id = ?", $this->getId())
				->limit(1);
			
			/* добавим условие по объектам текущего кластера */
			$data = $this->getData();
			for($i = 0; $i < count($data); $i++) {
				$select->where("object_".($i+1)."_id = ?", $data[$i]['id']);
			}
			
			$this->cluster['_meta'] = $this->getDb()->fetchRow($select);
		}
		return $this->cluster['_meta'];
	}
	
	/**
	 * Получить все страницы кластера
	 * @return array
	 */
	public function getVariants() {
		$variants = array();
		$objects = array();
		for($i = 0; $i < 3; $i++) {
			$object = $this->getObject($i);
			if($object) {
				$tableName = $object['name'];
				$fieldName = $object['field_name'];
				$object['_urls'] = $this->getDb()->fetchCol("SELECT `$fieldName` FROM $tableName WHERE hidden = 0");
				$objects[] = $object;
			} else {
				break;
			}
		}
// 		if($_SERVER['REMOTE_ADDR'] == '80.90.120.182'){
// 			die('<pre>'.print_r( $objects,1));
// 		}
		$length = count($objects);
		if($objects) {
			foreach ($objects[0]['_urls'] as $obj1url) {
				if(isset($objects[1])) {
					foreach ($objects[1]['_urls'] as $obj2url) {
						if(isset($objects[2])) {
							foreach($objects[2]['_urls'] as $obj3url) {
								$variants[] = implode("/", array($obj1url, $obj2url, $obj3url));
							}
						} else {
							$variants[] = implode("/", array($obj1url, $obj2url));
						}
					}
				}	else {
					$variants[] = implode("/", array($obj1url));
				}
			}
		}
		
		return $variants;
	}
	
}

?>
