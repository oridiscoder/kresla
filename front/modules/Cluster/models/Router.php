<?php
namespace front\modules\Cluster\models;

use front\models\Model;

class Router extends Model {
	
	/**
	 * 
	 * @var Clusters
	 */
	private $clustersModel;
	
	/**
	 * @return \front\modules\Cluster\models\Clusters
	 */
	private function getClustersModel() {
		if($this->clustersModel == null) {
			$this->clustersModel = new Clusters("clusters");
		}
		return $this->clustersModel;
	}
	
	/* (non-PHPdoc)
	 * @see Zend_Controller_Router_Route_Interface::match()
	*/
	public function match($path) {
		
		$path = preg_replace('#^/(.*?)/?$#', '$1', $path);
		$parts = explode("/", $path);
		$parts = array_filter($parts);
		$partsCount = count($parts);
		
		$model = $this->getClustersModel();
	
		if($partsCount >= 1 && $partsCount <= 5) {
			$clusters = $model->getMatchClusters($partsCount);
				
			if($clusters) {
				$clustersMatches = array();
				for($i = 0; $i < $partsCount; $i++ ) {
					$urlPart = $parts[$i];
					foreach ($clusters as $key=>$cluster) {
						if(($row = $this->matchCluster($cluster, $i, $urlPart)) != null) {
							$clustersMatches[$key][] = $row;
							//break;
						} else {
							unset($clusters[$key]);
							continue;
						}
					}
				}
	
				foreach ($clustersMatches as $key => $rows) {
					if(count($rows) == $partsCount) {
						$cluster = $clusters[$key];
						$cluster['_data'] = $rows;
						return new Cluster($cluster);
					}
				}
			}
		}
	}
	
	/**
	 * Метод проверяет, подходит ли указанный кластер заданной части URL
	 * @param array $cluster - проверяемый кластер
	 * @param integer $num - номер части URL, начиная с 0
	 * @param string $word - часть URL, по которой проверяется кластер
	 * @return array | boolean | null - строка, если часть URL указывает на запись в Таблице БД, boolean когда проверяется url_prefix проутера, null если ни то ни другое.
	 */
	private function matchCluster($cluster, $num, $word) {
		$num = $num + 1;
		if($cluster['url_prefix']) {
			if($num == 1) {
				return $cluster['url_prefix'] == $word;
			} else {
				$num  = $num - 1;
			}
		}
	
		$key = "object_$num";
	
		if(array_key_exists($key, $cluster)) {
			if($cluster[$key]) {
				$tableName = $cluster[$key.'_name'];
				$fieldName = $cluster[$key.'_field_name'];
					
				$select = $this->getDb()->select()
				->from($tableName)
				->where($fieldName.' = ?', $word)
				->where("hidden = 0");
					
				$row = $this->getDb()->fetchRow($select);
					
				return $row;
			} else {
				return null;
			}
		} else {
			return isset($cluster['_pages'][$word]) ? $cluster['_pages'][$word] : false;
		}
	}
}

?>
