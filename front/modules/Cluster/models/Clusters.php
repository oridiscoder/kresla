<?php
namespace front\modules\Cluster\models;

use front\models\Model;

class Clusters  extends Model {
	
	private $clusters;
	
	/**
	 * Получить список кластеров системы.
	 * @return array
	 */
	public function getClusters() {
		if($this->clusters == null) {
			$select = $this->getDb()->select()
				->from(array("C" => 'clusters'))
				->joinLeft(array("O1" => 'bof_objects'), "O1.id = C.object_1", array('object_1_name' => 'name'))
				->joinLeft(array("O1F" => 'bof_fields'), "O1F.obj_id = O1.id AND O1F.is_url_field = 1", array("object_1_field_name" => 'name'))
				->joinLeft(array("O2" => 'bof_objects'), "O2.id = C.object_2", array('object_2_name' => 'name'))
				->joinLeft(array("O2F" => 'bof_fields'), "O2F.obj_id = O2.id AND O2F.is_url_field = 1", array("object_2_field_name" => 'name'))
				->joinLeft(array("O3" => 'bof_objects'), "O3.id = C.object_3", array('object_3_name' => 'name'))
				->joinLeft(array("O3F" => 'bof_fields'), "O3F.obj_id = O3.id AND O3F.is_url_field = 1", array("object_3_field_name" => 'name'))
				->where("C.hidden = 0")
				->order("C.number");
		
			$this->clusters = $this->getDb()->fetchAll($select);
		}
		return $this->clusters;
	}
	
	public function getMatchClusters($partsCount) {
		
		$clusters = $this->getClusters();
		
		if($clusters) {
				
			$clustersPages = $this->getClustersPages($this->clusters);
				
			foreach ($this->clusters as $key=>$cluster) {
				/*
				 * Если у кластера задан url_prefix, то первая часть урла уже отведена на этот префикс.
				* Значит, отсчет начинаем с 1 а не с 0
				*/
				$pc = $cluster['url_prefix'] ? 1 : 0;
	
				/*
				 * Теперь посчитаем, сколько частей URL требует данный кластер
				*/
				for($i = 1; $i <= 3; $i++) {
					$k = "object_".$i;
					if($cluster[$k]) {
						$pc ++;
					} else {
						break;
					}
				}
	
				/*
				 * Сразу надо откинуть кластеры, которые не подходят по заданному количеству
				* частей URL.
				* Например, если задан URL /doma/kirpich/moscow/, который состоит из 3х частей, то нам нужны
				* кластеры, которые требуют 3 части в URL. Не больше и не меньше.
				*/
				if($pc != $partsCount) {
					/*
					 * Даем шанс тем кластерам, которые имеют внутренние страницы из clusters_pages
					*/
					if($partsCount - $pc != 1  || !isset($clustersPages[$cluster['id']])) {
						unset($clusters[$key]);
					}
				}
	
				if(isset($clusters[$key])) {
					$clusters[$key]['_pages'] = isset($clustersPages[$cluster['id']]) ? $clustersPages[$cluster['id']] : array();
				}
			}
			$clusters = array_values($clusters);
		}
	
		return $clusters;
	}
	
	/**
	 * Возвращает список страниц указанных кластеров
	 * @param array $clusters
	 * @return array [cluster_id][page_name] = page
	 */
	public  function getClustersPages($clusters) {
		
		$clustersIds = self::extractColumn($clusters, "id");
		
		$pages = array();
		if($clustersIds) {
			$select = $this->getDb()->select()
				->from("clusters_pages")
				->where("cluster_id IN (?)", array($clustersIds))
				->where("hidden = 0");
			$pages = $this->getDb()->fetchAll($select);
			if(!empty($pages)) {
				$np = array();
				foreach ($pages as $page) {
					$np[$page['cluster_id']][$page['name']] = $page;
				}
				$pages = $np;
			}
		}

		return $pages;
	}
	
	public static function extractColumn($rows, $colname) {
		$column = array();
		foreach ($rows as $row) {
			$column[] = $row[$colname];
		}
		return $column;
	}
}

?>
