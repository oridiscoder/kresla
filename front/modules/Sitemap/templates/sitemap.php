<?php echo'<?xml version="1.0" encoding="UTF-8"?>'?>
<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php foreach($links as $url):?>
        <?php if(isset($url['link'])):?>
            <url>
                <loc><?=$domain.$url['link']?></loc>
                <priority><?=$this->getModule('Sitemap')->getPriority($url['link'])?></priority>
            </url>
        <?php endif;?>
    <?php endforeach;?>
</urlset>
