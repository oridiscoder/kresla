<link rel="stylesheet" href="/css/publications.css">
<div class="categories text-content publications-list clear">
    <h1>Карта сайта</h1>
        <ul style="margin-left:60px">
            <?php foreach($links as $link):?>
                <?php if(isset($link['viewname'])):?>
                    <li>
                        <a href="<?=$link['link']?>"><?=$link['viewname']?></a>
                    </li>
                <?php endif;?>
                <?php if (isset($link['divName'])):?>
                    <b><?=$link['divName']?></b>
                <?php endif;?>
            <?php endforeach;?>
        </ul>
</div>