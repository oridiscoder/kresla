<?php
namespace front\modules;

use front\modules\CatalogKresla\models\Good;
use front\modules\CatalogKresla\models\Category;

use front\models\Orm;

use front\modules\Cluster\models as ClusterModels;

use front\modules\Cluster\models\Clusters;
use front\modules\Cluster\models\Router;

use front\modules\CatalogKresla\models\Upholstery;

use front\modules\CatalogKresla\models\TableFilter;

use front\modules\CatalogKresla\models\UpholsteryType;

use front\models\ModuleNotFoundException;


require_once ('front/modules/Module.php');

/**
 * Карта сайта. 
 *
 * @author Ivan
 *
 */
class Sitemap extends Module {
	
	public $siteurl;
	public $staticPagesTable='`cms_pages`';
	public $mainPageId=1;
	
	
	
	public $tableFilterArray;
	public $tableFilterCheck;
	public $tableFilterUrls;
	public $tableBaseUri;
	public $tableFilterCurrent;
	public $tableSeoItem;
	
	public $exceptStaticIds = array(
							'41'=>1,
							'19'=>1,
							'40'=>1,
							'36'=>1,
							'37'=>1,
							'36'=>1,
							'35'=>1,
							'32'=>1,
							'27'=>1,
							'30'=>1,
							'26'=>1,
							'21'=>1,
							'20'=>1,
							'23'=>1,
							'24'=>1,
							'33'=>1
		);
	public $level2priority=array( '1'=>'1',
			    '2'=>'0.8',
			    '3'=>'0.6',
			    '4'=>'0.5'
			  );
		
	/*
	 * Определение динамических страниц
	 * 
	 * key= начальная часть урл                   
	 * table - таблица, из которой берётся column 
	 * column - переменная чать урл               
	 * Опционально:
	 * tail - есть ли .html на конце
	 * priority,lastmod_col,changefreq 
	 * - теги на весь раздел
	 * *priority-приоритет корневой
	 */

	public $dynamicPagesConfig= array(
						'/news/'=> array (
											'table'=>'kresla_news',
											'column'=>'url',
											'priority'=>'0.6',
										      'lastmod_col'=>'date_publish',
										      'changefreq'=>'weekly',
										      'tail'=>'.html'
										    )
// 						'/dev/portals/'=> array (
// 											'table'=>'oridis_portfolio',
// 											'column'=>'url',
// 											'priority'=>'0.6',
// 										      'lastmod_col'=>'date',
// 										      'changefreq'=>'weekly'
// 										    ),
// 						'/articles/'=> array (
// 											'table'=>'oridis_articles',
// 											'column'=>'url',
// 											'priority'=>'0.8',
// 										      'lastmod_col'=>'date_publish',
// 										      'changefreq'=>'weekly',
// 										      'tail'=>'.html'
// 										    ),
// 						'/news/'=> array (
// 											'table'=>'oridis_news',
// 											'column'=>'url',
// 											'priority'=>'0.8',
// 										      'lastmod_col'=>'date_publish',
// 										      'changefreq'=>'weekly',
// 										      'tail'=>'.html'
// 										    ),
// 						'/seo/thematics/'=> array (
// 											'table'=>'oridis_seo_portfolio_subjects',
// 											'column'=>'slug',
// 											'priority'=>'0.6',
// 										      'changefreq'=>'weekly'
// 										    )
			);

	

	/* (non-PHPdoc)
	 * @see front\modules.Module::getTitle()
	 */
	public function getTitle() {
		return "Карта сайта";
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::getDescription()
	 */
	public function getDescription() {
		return "Модуль карта сайта. Создаёт страницу sitemap.xml.";
	}

	/* (non-PHPdoc)
	 * @see front\modules.Module::install()
	 */
	public function install(){
// 	$sql="INSERT INTO `cms_templates` (`number`, `hidden`, `name`, `content`, `page_template`, `module_name`) VALUES
// 		(36, 0, 'Module_Sitemap:itemXML', '<url>\r\n	<loc>\r\n		{{loc}}\r\n	</loc>{{more}}\r\n</url>\r\n', 0, 'Module_Sitemap'),
// 		(37, 0, 'Module_Sitemap:wrapperXML', '<urlset xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n  {{mapcontent}}\r\n</urlset>', 0, 'Module_Sitemap');";
// 	$this->getDb()->query($sql);
// 		$this->saveTemplate('itemXML', file_get_contents(dirname(__FILE__).'/itemXML.html'));
// 		$this->saveTemplate('wrapperXML', file_get_contents(dirname(__FILE__).'/wrapperXML.html'));
		return true;
	}

	
	/* (non-PHPdoc)
	 * @see front\modules.Module::remove()
	 */
	public function remove(){
		$this->deleteTemplate('itemXML');
		$this->deleteTemplate('wrapperXML');
		
		return true;

	}

	/* (non-PHPdoc)
 	 * @see front\modules.Module::getConfigFields()
 	 */
	public function getConfigFields(){

	}
	public function getView($templatename,$arrdata){
		$template = $this->getTemplate($templatename, true);
		return $this->getDi()->get("lex")->parse($template, $arrdata);
		
	}
	
	
	public function fillItemXML($item){

// 		$template = $this->getTemplate("itemXML");

        //в lastmod оставляем только дату
        if (isset($item['lastmod'])) {
            $lastmod = explode(" ", $item['lastmod']);
            $lastmod = $lastmod[0];
        }

		$more='';
		isset($item['lastmod']) ? $more.=chr(10).'<lastmod>'.$lastmod.'</lastmod>' : 1 ;
		isset($item['priority']) ? $more.=chr(10).'<priority>'.$item['priority'].'</priority>' : 1 ;
		isset($item['changefreq']) ? $more.=chr(10).'<changefreq>'.$item['changefreq'].'</changefreq>' : 1 ;
// 		$outXML = $this->getDi()->get("lex")->parse($template, array('loc'=>$this->siteurl.$item['loc'],'more'=>$more));



		$uri2save =  $item['loc'];

		

		$sql="SELECT * FROM  `oldPages` WHERE  `name` =  '".$uri2save."'";
		$db = $this->getDb();
		$c=$db->fetchAll($sql);
		if(!count($c)){
						$sql = "INSERT INTO  `u24576_kresla`.`oldPages` (
						`id` ,
						`name` ,
						`number` ,
						`hidden`
						)
						VALUES (
						NULL ,  '".$uri2save."',  '0',  '0'
						);"		;
			$db->query($sql);
			
		
		}


		$u = $this->siteurl.$item['loc'];

		$outXML = $this->getView("itemXML.html", array('loc'=>$u,'more'=>$more));
		return $outXML;
		
	}
	
	public function makeStaticItemXML($item,$preURI){
		$itemToFill=array();
		$itemToFill['loc']=$preURI.$item['name'].'/';
		$itemToFill['priority']=0.6;
		
		if($item['id']==$this->mainPageId){
			$itemToFill['changefreq']='daily';
			$itemToFill['priority']= 1;
		}
		else{
			// $itemToFill['loc'] = substr($itemToFill['loc'],0,-1);
			$itemToFill['changefreq']='monthly';
		}
		$staticXML=$this->fillItemXML($itemToFill);
		//заполнили. дальше проверим, нет ли потомков
		$sql='SELECT  id,parent_id,name FROM '.$this->staticPagesTable.'WHERE `parent_id` ='.$item['id'].' AND hidden=0 AND is_section=0 AND name != "sitemap.xml"';
		$childs=$this->getDb()->fetchAll($sql);
		//если есть - будем заполнять их рекурсивно
		if($childs){
			foreach($childs as $child){
				$chid = $child['id'];
				if(isset($this->exceptStaticIds[$chid])){
					continue;

				}
				$staticXML.=$this->makeStaticItemXML($child,$itemToFill['loc']);
			}
		}
		return $staticXML;
	}

	
	public function staticPagesXML($options){
		$sql='SELECT * FROM '.$this->staticPagesTable.' WHERE `id` = '.$this->mainPageId ;
		$main=$this->getDb()->fetchRow($sql);
		$main['name']='';
		$staticXML=$this->makeStaticItemXML($main,'');
		return $staticXML;
		
	}
	//column,table - обязательны
	/*Array
(
    [url] => novyy_rabochiy_sezon_nachalsya_s_zapuska_reorganizovannyh_saytov
    [lastmod] => 2009-01-20 00:00:00
    [changefreq] => monthly
    [priority] => 0.6
    [loc] => /news/novyy_rabochiy_sezon_nachalsya_s_zapuska_reorganizovannyh_saytov.html
)*/

	public function getSolutions($options, $template) {
		$clusterModel = new Clusters();
// 		echo 1; die;
// // 		require_once "models/Cluster.php";
		$clusters = $clusterModel->getClusters();
		
// 		return $clusterModel->getClustersPages($clusters);
		
		$links = array();
		$n = array();
// 		die('<pre>'.print_r( $this->getClusterSets(),1));
		$clusterLinks = array();
		if($clusters) foreach ($clusters as $cluster) {
			$cluster = new ClusterModels\Cluster($cluster);
// 			$clusterLinks = array_merge($clusterLinks,$this->getTargetTableIds($cluster));
			
			$clusterLinks = array_merge($clusterLinks,$cluster->getVariants());
		}
		$shortLinks = array();
		foreach($clusterLinks as $url){
			$temp = explode('/',$url);
			if(count($temp) == 3){
				unset($temp[2]);
				$shorter = implode('/',$temp);
				if(!isset($shortLinks[$shorter])){
					$clusterLinks[] = $shorter;
					$shortLinks[$shorter] = 1;
				}
				
			}
		}

		
		return $clusterLinks;
		
	}

	


	public function kreslaItemsToXML(){
	
// 		die('<pre>'.print_r( $this->getClusterSets(),1));
		$this->getGTables();
 		$outXML='';

	
		$sql='SELECT * FROM `kresla_categories` where hidden=0';
		$cats = $this->getDb()->fetchAll($sql);
 		foreach($cats as $item){
			$category = new Category($item['id']);
			
			$item['changefreq']='weekly';
			$item['priority']=0.8;
 			$item['loc']='/catalog/'.$category->getFullUrl().'/';
 			$outXML.=$this->fillItemXML($item);
			$categoryID = $item['id'];
			if($categoryID) {
				

				$params = array(
					'category_id' => $categoryID,
					'onpage' => null,
					'pagenumber' => null,

				);
				
				

				$Category = Category::find($categoryID);
				$goodsCount = Good::count($params);
				$pagesNumbers = ceil($goodsCount/32);

				
				for ($x = 2; $x <= $pagesNumbers; $x++) {
					$numLink = $item;
					$numLink['loc'] = $item['loc'].'?p='.$x;
					$outXML.=$this->fillItemXML($numLink);
				} 
				$numLink = $item;
				$numLink['loc'] = $item['loc'].'?p=all';
				$outXML.=$this->fillItemXML($numLink);
					
				
				
				
			
			
			}
			
 			
 			
 		}
//  		echo $outXML; die;
 		
		$sql='SELECT * FROM `kresla_goods` where hidden=0';
		$goods = $this->getDb()->fetchAll($sql);
 		foreach($goods as $item){
			$good = new Good($item['id']);
			$item['changefreq']='weekly';
			$item['priority']=0.7;
 			$item['loc']='/'.$good->getFullUrl();
 			$outXML.=$this->fillItemXML($item);
 		}
 		
 		foreach($this->getClusterSets() as $uri=>$i){
			$item = array();
			$item['changefreq']='weekly';
			$item['priority']=0.8;
 			$item['loc']='/catalog/'.$uri.'/';
 			$outXML.=$this->fillItemXML($item);
 		}
 		foreach($this->comboLinkTemplates as $uri=>$i){
			$item = array();
			$item['changefreq']='weekly';
			$item['priority']=0.8;
 			$item['loc']='/catalog/home/tables'.$uri.'/';
 			$outXML.=$this->fillItemXML($item);
 		}
 		
 		return $outXML;
	}
	
	//column,table - обязательны
	public function tableItemsToXML($configs){
		$select='';
		$priority2level=array_flip($this->level2priority);
		$thislevel=$priority2level[$configs['priority']]+1;
		$priority=$this->level2priority[$thislevel];
		$loc=$configs['loc'];
		isset($configs['tail']) ? $tail='.html' : $tail='/';
		isset($configs['lastmod_col']) ? $select=','.$configs['lastmod_col'].' as lastmod' : 1;
		$sql='SELECT  '.$configs['column'].$select.' FROM '.$configs['table'].' WHERE hidden=0';
		//echo $sql.'<br>';
		//echo $sql;
		$items=$this->getDb()->fetchAll($sql);
 		$outXML='';
 		foreach($items as $item){
			$item['changefreq']='monthly';
			$item['priority']=0.5;
 			$item['loc']=$loc.$item[$configs['column']].$tail;
 			$outXML.=$this->fillItemXML($item);
 		}
 		return $outXML;
	}

	public function dynamicPagesXML($options){
		$outXML='';
		$outXML .= $this->kreslaItemsToXML();
/*
		$select = $this->getDb()->select()
								->from('catalog_category')
								->where("hidden = 0");
		$cats = $this->getDb()->fetchAll($select);	
		$itemsXmlItems = array();
		$tagsXmlItems = array();
		$tagsXmlItemsCheck = array();
		foreach ($cats as $c) {
			$select = $this->getDb()->select()
								->from('catalog_item')
								->where("hidden = 0")
								->where("category_id =".$c['id']);
			$curalias = $c['name'];
			$items = $this->getDb()->fetchAll($select);									
			foreach ($items as $it) {
				$temp = array();
				$t['loc'] = '/'.$curalias.'/'.$it['name'].'/';
				$t['priority'] = 0.6;
				$t['changefreq'] = 'weekly';
				$itemsXmlItems[] = $t;
				$tags = explode(', ',$it['tags']);
				foreach ($tags as $tagviewname) {
					
					$select = $this->getDb()->select()
								->from('bof_tag')
								->where("hidden = 0")
								->where("viewname ='".$tagviewname."'");
					$tag = $this->getDb()->fetchRow($select);
					$tagAlias = $tag['name'];
					if($tagAlias == ''){
						continue;
					}
					$tagloc = '/'.$curalias.'/'.$tagAlias.'/';
					if(!isset($tagsXmlItemsCheck[$tagloc])){
						$tagsXmlItemsCheck[$tagloc] = 1;
						$tagsXmlItems[] = array('loc'=>$tagloc,'priority'=>0.6,'changefreq'=>'weekly');
					}

				}


				
			}

			
		}
			foreach ($itemsXmlItems as $item) {
				$outXML.=$this->fillItemXML($item);
			}
			foreach ($tagsXmlItems as $item) {
				$outXML.=$this->fillItemXML($item);
			}*/
			

		foreach($this->dynamicPagesConfig as $uri=>$configs){
			$configs['loc']=$uri;
			$temp = $configs;
// 			$temp['loc'] = substr($temp['loc'] ,0,-1).'/';
			$outXML.=$this->fillItemXML($temp);
			$outXML.=$this->tableItemsToXML($configs);
			 
		}

		return $outXML;
	}
	//главный вывод
	public function xml($options,$template){
		ini_set("display_errors", "on");
		error_reporting(E_ALL);
		$this->siteurl='http://'.$_SERVER['HTTP_HOST'];
		$staticXML=$this->staticPagesXML('');
		// echo 1; die;
		$dynamicXML=$this->dynamicPagesXML('');
// 		$dynamicXML='';
// 		$template = $this->getTemplate("wrapperXML");
		
		//header("Content-type: text/xml; charset=utf8");
		$head='<?xml version="1.0" encoding="UTF-8"?>'.chr(10);
		$xml = $this->getView("wrapperXML.html", array('mapcontent'=> $staticXML.$dynamicXML));
		file_put_contents('sitemap-dev.xml', $xml);
		return $xml;
// 		return $head.$this->getDi()->get("lex")->parse($template, array('mapcontent'=> $staticXML.$dynamicXML));
		
		
	}

    
    public function level($item,$level){
		$level++;
		if ($item['id']!=$this->mainPageId){
			$sql='SELECT * FROM '.$this->staticPagesTable.' WHERE `id` = '.$item['parent_id'];
			$parent=$this->getDb()->fetchRow($sql);
			$level=$this->level($parent,$level);
		}
		return $level;
    }


	
	public function getClusterSets(){
		
		$cats = $this->getDb()->fetchAll('SELECT * FROM `kresla_categories` where hidden=0');		
		$colors = $this->getDb()->fetchAll('SELECT * FROM `kresla_colors` where hidden=0');		
		$upholsTypes = $this->getDb()->fetchAll('SELECT * FROM `kresla_upholstery_types` where hidden=0');		
		
		$upholsTypesId2Url = array();
		$colorsId2Url = array();
		$catsId2Url = array();
		
		foreach($upholsTypes as $uphol){
			$upholsTypesId2Url[$uphol['id']] = $uphol['url'];
		}
		foreach($colors as $color){
			$colorsId2Url[$color['id']] = $color['url'];
		}
		foreach($cats as $key=>$cat){
			if($cat['id'] == 1 || $cat['id'] == 7 || $cat['id'] == 11 ){
				unset($cats[$key]);
				continue;
			}
			$category = new Category($cat['id']);
			$catsId2Url[$cat['id']] = $category->getFullUrl();
		}

		
		
		
		$uphols = $this->getDb()->fetchAll('SELECT * FROM  `kresla_upholstery` where hidden=0');		
		$upholsUrls = array();
		foreach($uphols as $uphol){
			$t = array();
			if($uphol['type_id'] != NULL){
				if(isset($upholsTypesId2Url[$uphol['type_id']])){
					$t[] = $upholsTypesId2Url[$uphol['type_id']];
				}else{
					continue;
				}
			}
			if($uphol['color_id'] != NULL){
				if(isset($colorsId2Url[$uphol['color_id']])){
					$t[] = $colorsId2Url[$uphol['color_id']];
				}else{
					continue;
				}
			}
			$upholsUrls[$uphol['id']] = array();
			foreach($t as $fr){
				if($fr == '') {continue;}
				$upholsUrls[$uphol['id']][] = $fr;
			}
			$upholsUrls[$uphol['id']][] = implode('/',$t);
			
		}
// 		die('<pre>'.print_r( $upholsUrls,1));
		$goodUpholsUrls = array();
		
		
		
		$kreslaUphols = $this->getDb()->fetchAll('SELECT * FROM  `kresla_good_upholsteries` where hidden=0');
		$finalLinks = array();
		
		foreach($kreslaUphols as $k){
			$upIp = $k['upholstery_id'];
			if($upIp == 0){ continue; }
// 			echo $k['id'];
// 			echo '<br>';
			$goodcats = $this->getDb()->fetchAll('SELECT `category_id` FROM  `kresla_good_categories` WHERE  `good_id` ='.$k['good_id'].' AND hidden=0');
			foreach($goodcats as $gcat){
				if($gcat['category_id'] == 1 || $gcat['category_id'] == 7 || $gcat['category_id'] == 11 ){
					continue;
				}	
				foreach($upholsUrls[$upIp] as $u){
					if($u == ''){
						continue;
					}
					$result = $catsId2Url[$gcat['category_id']].'/'.$u;
					$finalLinks[$result] = 1;
				}
				
			}
			
			
		}
		
// 		die('<pre>'.print_r( $this->getGTables(),1));

		return $finalLinks;
		
		
		
	}
	
	public function pc_array_power_set($array) {
		// initialize by adding the empty set
		$results = array(array( ));

		foreach ($array as $element)
			foreach ($results as $combination)
				array_push($results, array_merge(array($element), $combination));

		return $results;
	}

// 	$arr = array('peter', 'paul', 'mary');

	public function addFillParam($link,$param){
		$r = array();
		foreach($param as $item){
			$r[] = $link . '/'.$item['url'];
		}
	}
	public $comboLinkTemplates = array();
	public function fillCombo($combo,$params,$combonum = 0, $links = array(), $curlink = ''){
		$curParam = $combo[$combonum];

		if (is_array($params[$curParam])) {
			foreach ($params[$curParam] as $item) {
			$newlink = $curlink.'/'.$item['url'];
			$fallen = false;
			if($this->checkTableUri($this->tf, '/catalog/home/tables'.$newlink) == ''){
				continue;
			}
			
			$this->comboLinkTemplates[$newlink] = 1;
			$last = count($combo)-1;
			if(($last != $combonum) /*&& !$fallen*/){
				$this->fillCombo( $combo, $params, $combonum+1, array(), $newlink);
			}
			}
		}

	
		return $links;
	
	}
	
	public $tf;
	public function getGTables(){


		$baseTablesUri = '/catalog/home/tables';
		$tf = new TableFilter();
		$tf->getTableParams();
		$this->tf = $tf;
		$paramsOrder = array (
				  0 => 'types',
				  1 => 'colors',
				  2 => 'materials',
				  3 => 'sizes',
				  4 => 'prices',
				);
		$combinations = $this->pc_array_power_set(array_reverse($paramsOrder));//вот это комбинации всех наборов параметров, начиная от единичных, заканчивая когда они есть все сразу. здесь их при нормальной работе было 32
		$comboLinkTemplates = array();
		foreach($combinations as $combo){
			if(!count($combo)) continue;
			$this->fillCombo($combo,$tf->params);
		}
// 		$j = 0;
// 		foreach($this->comboLinkTemplates as $key=>$val){
// 		
// 			echo ++$i.'&nbsp;&nbsp;<a href="/catalog/home/tables'.$key.'/">'.$key.'</a><br>';
// 		}
// 		die;
// 		die('<pre>'.print_r( $this->comboLinkTemplates,1));
// 		foreach(

		//пох
		$uri = '';
		$tableFilterLinks = array();
		foreach ($tf->params as $paramName => $values) {
			foreach ($values as $key => $fitem) {
				$i = 0;
				$bu = array();
				foreach ($paramsOrder as $pn) {
					if($paramName = $pn){
						$bu[$i++] = $fitem['url'];
					}
				}	
				$tableFilterLinks[] = $baseTablesUri.implode('/',$bu).'/';
			}
		}		
		
				
				

	}	
	
	public function checkTableUri($tf,$uri){
		$_SERVER['REQUEST_URI'] = $uri;	
		$result = $tf->parseUri();
// 		die('<pre>'.print_r( $result,1));
		if(!$result){
			return false;
		}else{
			$this->tableFilterCurrent = $result;
			$goods = $tf->fetchGoods($result);
			return $goods;
		}	
	}

}
