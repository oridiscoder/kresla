<?php

abstract class PluginAbstract {
	private $di;
	
	public function setDi($di) {
		$this->di = $di;
	}
	/**
	 * @return Di
	 */
	public function getDi() {
		return $this->di;
	}
	/**
	 * @return Zend_Db_Adapter_Abstract
	 */
	public function getDb() {
		return $this->di->get("db");
	}
}