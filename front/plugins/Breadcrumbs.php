<?php

require_once ('PluginAbstract.php');

class Plugin_Breadcrumbs extends PluginAbstract {
	public function breadcrumbs() {
		$breadcrumbs = $this->getDi()->get("page")->getBreadcrumbs();
		$html = '';
		if($breadcrumbs) {
			
			$wrapper = '<ul class="breadcrubs">{items}</ul>';
			$itemTemplate = '<li><a href="{href}">{title}</a></li>';
			$itemActiveTemplate = '<li><span>{title}</span></li>';
			
			$total = count($breadcrumbs);
			for($i = 0; $i < $total; $i++) {				
				if($i + 1 != $total) {
					$itemHTML = $itemTemplate;					
				} else {
					$itemHTML = $itemActiveTemplate;
				}
				
				$itemHTML = str_replace("{href}", $breadcrumbs[$i]['href'], $itemHTML);
				$itemHTML = str_replace("{title}", $breadcrumbs[$i]['title'], $itemHTML);
				$html .= $itemHTML;
			}
			
			$html = str_replace("{items}", $html, $wrapper);
		}
		
		return $html;
	}
}

?>