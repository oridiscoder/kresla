<?php

require_once 'PluginAbstract.php';

class Plugin_DateToInterval extends PluginAbstract {
	
	public function dateToInterval($options, $content) {		
		if(isset($options['date'])) {
			$now = time();
			$dest = is_numeric($options['date']) ? $options['date'] : strtotime($options['date']);
			
			if($dest < $now) {
				$diffSec = $now - $dest;
				$diffHours = $diffSec / 3600;
				$diffDays = $diffHours / 24;
				$diffMonths = $diffDays / 30;
				
				$months = floor($diffMonths);
				$days = floor($diffDays);
				$hours = floor($diffHours);
				
				if($months) {
					return self::spellcount($months, "месяц", "месяца", "месяцев") . ' назад';
				} elseif($days) {
					return self::spellcount($days, "день", "дня", "дней") . ' назад';
				} elseif($hours) {
					return self::spellcount($hours, "час", "часа", "часов") . ' назад';
				} else {
					return "в ". date('m:i', $dest);
				}
			}
		}
	}
	
	public static function spellcount($num, $one, $two, $many, $nodigit = false) {
	    if ($num%10==1 && $num%100!=11){
	        return  ((!$nodigit)?$num:'').' '.$one;
	    }
	    elseif($num%10>=2 && $num%10<=4 && ($num%100<10 || $num%100>=20)){
	        return  ((!$nodigit)?$num:'').' '.$two;
	    }
	    else{
	        return  ((!$nodigit)?$num:'').' '.$many;
	    }
	}
}

?>