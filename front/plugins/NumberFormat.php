<?php

require_once "PluginAbstract.php";

class Plugin_NumberFormat extends PluginAbstract {
	public function numberFormat($options, $content) {
		$number = $options['value'];
		$number = empty($number) ? 0 : $number;		
		return number_format($number, 0, ".", " ");
	}
	
}

?>