<?php

require_once "PluginAbstract.php";

class Plugin_Twitter extends PluginAbstract {
	
	private $username;
	
	/**
	 * Вывести последний твит заданного пользователя
	 * @param array $options - параметры
	 * @param string $content - шаблон
	 * @return string
	 */
	public function getLastTweet($options, $content) {
		
		$username = isset($options['username']) ? $options['username'] : null;
		
		
		if($username) {
			$content = $this->getContentThrowCache($this->getFeedURL($username));
			$tweet = self::extractLastFeed($content);
			return $tweet;
		} else {
			return '<!-- You must specify username param -->';
		}
	}
	
	private function getContentThrowCache($url) {
		return file_get_contents($url);
	}
	
	private function getFeedURL($username) {
		return  "http://search.twitter.com/search.atom?q=from:" . $username . "&rpp=1";
	}
	
	public static function extractLastFeed($content) {
		$stepOne = explode("<content type=\"html\">", $content);
		$stepTwo = explode("</content>", $stepOne[1]);
		$tweet = $stepTwo[0];
		$tweet = str_replace("&lt;", "<", $tweet);
		$tweet = str_replace("&gt;", ">", $tweet);
		return $tweet;
	}
}

?>