<?php

require_once ('PluginAbstract.php');

class Plugin_DateFormat extends PluginAbstract {
	
	public function dateFormat($options, $content = null) {
		$date = $options['date'];
		$format = $options['format'];
		
		if(!is_numeric($date)) {
			$date = strtotime($date);
		}
		
		$str = strftime($format, $date);
		
		if(isset($options['rumonth'])) {
			$str = $this->monthEn2Ru($str);
		}
		
		return $str;
	}
	
	protected function  monthEn2Ru($date) {
		$months = array(
				'January'=>'Января',
				'February'=>'Февраля',
				'March'=>'Марта',
				'April'=>'Апреля',
				'May'=>'Мая',
				'June'=>'Июня',
				'July'=>'Июля',
				'August'=>'Августа',
				'September'=>'Сентября',
				'October'=>'Октября',
				'November'=>'Ноября',
				'December'=>'Декабря'
		);
		foreach ($months as $eng=>$rus) {
			$date = str_replace($eng, $rus, $date);
		}
		return $date;
	}
}

?>