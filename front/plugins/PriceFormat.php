<?php

require_once ('PluginAbstract.php');

class Plugin_PriceFormat extends PluginAbstract {
	public function priceFormat($options, $content = null) {
		$price = $options['value'];
		
		$number = empty($price) ? 0 : $price;
		return number_format($number, 0, ".", " ");
	}
}

?>