<?php

require_once ('PluginAbstract.php');

class Plugin_Highlight extends PluginAbstract {
	
	private $words;
	
	public function highlight($options, $template) {
		
		$this->words = isset($options['words']) ? $options['words'] : '';
		$this->words = explode(",", $this->words);
		
		$value = isset($options['value'])  ? $options['value'] : "" ;
		if($value && $this->words) {
			$className = isset($options['classname']) ? $options['classname'] : 'hl';
			
			foreach ($this->words as $word) {
				$value = str_ireplace($word, "<span class=\"$className\">$word</span>", $value);
			}
		}
		return $value;
	}
}

?>