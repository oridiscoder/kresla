<?php
require_once ('PluginAbstract.php');

/**
 * Плагин подгрузки шаблонов
 * 
 * Подгружает шаблоны из админки по их названию.
 * Использование: {{template name="template_name"}}
 *
 */
class Plugin_Template extends PluginAbstract {
	public function template($options, $content) {
		if(isset($options['name'])) {
			return $this->getDi()->get("Templates")->find($options['name']);
		}
	}
}
?>