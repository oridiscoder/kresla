<?php

require_once ('front/plugins/PluginAbstract.php');

class Plugin_Escape extends PluginAbstract {
	
	public function escape($options, $content) {
		return htmlspecialchars( $options['value']);
	}
}

?>