<?php

require_once ('PluginAbstract.php');

class Plugin_MinifyJs extends PluginAbstract {
	
	/**
	 * 
	 * @var LayoutScript
	 */
	private $minifier;
	
	public function __construct() {
		require_once 'Front/LayoutScript.php';
		$this->minifier = LayoutScript::getInstance();
		$this->minifier->setBuidPath($_SERVER['DOCUMENT_ROOT'] .'/js/build');
	}
	
	public function add($options, $content) {
		if(isset($options['src'])) {
			$this->addScript($options['src']);
		}
	}
	public function addScript($src) {
		$this->minifier->addScript($src);
	}
	
	public function getHTML() {
		return $this->minifier->getMergedAndMinified();
	}
	
	public function getScripts() {
		return $this->minifier->getSources();
	}
}

?>