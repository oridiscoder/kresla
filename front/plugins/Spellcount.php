<?php

require_once "PluginAbstract.php";

class Plugin_Spellcount extends PluginAbstract {
	public function spellcount($options, $content) {
		
		$num = intval($options['value']);
		
		list($one, $two, $many) = explode(",", $options['words']);
		$nodigit = isset($options['nodigit']) ?  $options['nodigit'] : false;
		
		if ($num%10==1 && $num%100!=11){
			return  ((!$nodigit)?$num:'').' '.$one;
		}
		elseif($num%10>=2 && $num%10<=4 && ($num%100<10 || $num%100>=20)){
			return ((!$nodigit)?$num:'').' '.$two;
		}
		else{
			return ((!$nodigit)?$num:'').' '.$many;
		}
	}
	
	//нужен метод, который будет возвращать просто строку в нужной форме, а не число и строку
	public function spellcountWO($options, $content) {
	
		$num = intval($options['value']);
	
		list($one, $two, $many) = explode(",", $options['words']);
		$nodigit = isset($options['nodigit']) ?  $options['nodigit'] : false;
	
		if ($num%10==1 && $num%100!=11){
			return $one;
		}
		elseif($num%10>=2 && $num%10<=4 && ($num%100<10 || $num%100>=20)){
			return $two;
		}
		else{
			return $many;
		}
	}
}

?>