<?php

require_once ('PluginAbstract.php');

class Plugin_MinifyCss extends PluginAbstract {
	
	/**
	 * 
	 * @var LayoutStyle
	 */
	private $minifier;
	
	public function __construct() {
		require_once "Front/LayoutStyle.php";
		$this->minifier = LayoutStyle::getInstance();
		$this->minifier->setBuidPath($_SERVER['DOCUMENT_ROOT'].'/css/build');
	}
	
	public function add($options, $content) {
		if(isset($options['href'])) {
			$this->addStyle($options['href']);
		}
	}
	public function addStyle($href) {
		$this->minifier->addStyle($href);
	}
	
	public function getHTML() {
		return $this->minifier->getMergedAndMinified();
	}
	
	public function getStyles() {
		return $this->minifier->getStyles();
	}
}

?>