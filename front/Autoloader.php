<?php
namespace front;

class Autoloader {
	public static function autoload($className) {
		$fqFilename = str_replace('\\', '/', $className.'.php');
		$path = realpath(APPLICATION_PATH.'/../../');
		$fqFilename = $path . DIRECTORY_SEPARATOR . $fqFilename;

		if(file_exists($fqFilename)) {
			require_once $fqFilename;
		} elseif(strpos($className, "modules") !== false) {
			/* модули хранятся в подпапках. Кроме корневого Module.php */
			$className = str_replace('\\', '/', $className);
			$modulename = basename($className);
			$dirname = dirname($className);			
			$fqFilename = $dirname . DIRECTORY_SEPARATOR . $modulename . DIRECTORY_SEPARATOR . $modulename . '.php';
			require_once $fqFilename;
		}
	}
}