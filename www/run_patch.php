<?php
/* поднимаемся на уровень выше */
chdir(dirname(__FILE__).'/../');

$patchNumber = $_GET['patch_number'];

$response = array(
	'success'=> false, 
	'error_code' => 0, 
	'error' => null
);

if(is_numeric($patchNumber)) {
	$filename = "patches/$patchNumber/patch.php";
	if(file_exists($filename)) {
		$command = "php $filename 2>&1";
		$lines = array();
		$code = 0;
		set_time_limit(300);	//позволим скрипту работать до 5 минут
		if(function_exists("exec")) {
			exec($command, $lines, $code);
			if($code == 0) {
				$response['success'] = true;
			} else {
				/* произошла какая-то ошибка */
				$response['success'] = false;
				$response['error_code'] = $code;
				$response['error'] = "exec() failed with code $code. Output:".implode("\n", $lines);
			}
		} else {
			try {
				ob_start();
				include $filename;
				$content = ob_get_clean();
				$response['success'] = true;
				//$response['content'] = $content;
			} catch (Exception $ex) {
				/* произошла какая-то ошибка */
				$response['success'] = false;
				$response['error_code'] = 1;
				$response['error'] = $ex->getMessage()."\n".$ex->getTraceAsString();
			}
		}
	} else {
		$response['success'] = false;
		$response['error'] = "Can't find patch file $filename";
	}
} else {
	$response['success'] = false;
	$response['error'] = "Vaiable patch_number must be int!";
}

echo json_encode($response);