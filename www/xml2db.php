<?php
/* поднимаемся на уровень выше */
$rootPath = realpath(dirname(__FILE__).'/..');
chdir($rootPath);

require_once 'flusher.php';

$flusher = new Flusher();

if(isset($_GET['command'])) {
	$command = $_GET['command'];
	$isDebug = isset($_GET['debug']) ? $_GET['debug'] : false;
	
	ob_start();
	if(strpos($command, "p") !== false) {
		$flusher->importPages($rootPath . '/db/pages');
	}
	if(strpos($command, "o") !== false) {
		$flusher->importObjects($rootPath . '/db/objects');
	}
	if(strpos($command, "t") !== false) {
		$flusher->importTemplates($rootPath . '/db/templates');
	}	
	$content  = ob_get_clean();
	
	if($isDebug) {
		echo $content;
	} else {
		echo "OK";
	}
}