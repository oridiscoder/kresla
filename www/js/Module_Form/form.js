var Form = function (formObject, options) {
	this.form = formObject;
	this.validator = new _validator({
        form: formObject, 
        onSubmit: function(e) {
            e.preventDefault();
            return false;
        }
    });
	this.ajax = false;
	
	this.containerSuccess = null;
	
	if(options != undefined) {
		if(options.ajax) {
			this.ajax = options.ajax == true;
		}
		if(options.containerSuccess) {
			this.containerSuccess = options.containerSuccess;
			this.validator
		}
		if(options.containerError) {
			this.containerError = options.containerError;
		}
	}
	
	
	this.submit = function () {
		this.validator.touchFields();
		this.validator.validate();
		if(this.validator.isValid()) {
			if(this.ajax) {
				this.ajaxSubmit();
			} else {
				this.form.submit();
			}
		}
	};
	
	this.ajaxSubmit = function () {
		var form = $(this.form);
		var data = form.serialize();
		var self = this;
		$.ajax({
			url: form.attr("action"), 
			type: form.attr("method").toUpperCase(), 
			data: data, 
			success: function (html) {
				try {
					var data = eval('(' + html + ')');
					if(data.errors) {
						self.showErrors(data.errors);
					} else {
						self.showSuccess();
					}
				} catch (ex) {
					alert("Произошла ошибка на сервере: " + ex);
				}
			}
		});
	};
	
	this.showErrors = function (errors) {
		for(fieldName in errors) {
			this.validator.showHint(fieldName, "server-error", errors[fieldName]);
		}
	};
	
	this.showSuccess = function () {
		if(this.containerSuccess) {
			$(this.form).hide();
			$(this.containerSuccess).show();
		}
	};
}