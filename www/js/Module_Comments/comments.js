function Comments(formObject) {
	var self = this;
	var form = $(formObject);
	
	this.total = 0;
	this.onpage = 0;
	this.pageNumber = 1;
	this.ajaxURL = window.location.pathname;
	this.objectID = null;
	
	this.submit = function (event) {
		var code = $('<input>', {type: "hidden", name:"captcha", value:self.generateCode()});
		form.append(code);
		
		self.fetchRemote(form.serialize(), function (html) {
			var data = eval('(' + html + ')');
			if(data.errors) {
				self.showErrors(data.errors);
			} else {
				self.showSuccess();
			}
		});
		
		return false;
	};
	
	this.setPagination = function (onpage, total) {
		this.onpage = onpage;
		this.total = total;
		this.pagesCount = Math.ceil(total / onpage);
		
		/* сформирую массив вида номерстраницы = количество комментов на ней */
		this.pages = [];		
		for(var i = 1; i <= this.pagesCount; i++) {
			if(i != this.pagesCount) {
				this.pages.push(onpage);
			} else {
				this.pages.push(total - (i - 1) * onpage);
			}
		}
	}
	
	form.bind("submit", this.submit);
	
	this.generateCode = function (length) {
		var chars = "ABCDEFGHIJKLMNOPQRSTUVWXUZ";		
		var code = "";
		length = length == undefined ? 4 : length;
		for(var i = 0; i < length; i++) {
			var key = Math.random() * (chars.length - 1);
			code += chars.charAt(key);
		}
		return code;
	}
	
	this.fetchRemote = function (data, callback) {
		var url = form.attr("action");
		var method = "POST";	//always post
		this.startAjax();
		$.post(url, data, function (html) {
			self.stopAjax();
			callback(html);
		});
	};
	
	this.showErrors = function (errors) {
		form.find("div.errors").remove();
		var div = $('<div>').addClass("errors");
		var ul = $('<ul>');
		for(key in errors) {
			$('<li>').text(errors[key]).appendTo(ul);
		}
		div.append(ul);
		form.prepend(div);
	};
	
	this.showSuccess = function () {
		form.hide();
		var div = $('<div>').addClass('success').text("Ваше сообщение успешно отправлено");
		form.after(div);
	}
	
	this.startAjax = function () {
		var button = form.find('input[type="submit"]');
		button.attr("disabled", "disabled");
		button.attr("rel", button.attr("value"));
		button.attr("value", "Идет отправка");
	};
	
	this.stopAjax = function () {
		var button = form.find('input[type="submit"]');
		button.removeAttr("disabled");
		button.attr("value", button.attr("rel"));
		button.removeAttr("value");	//это может быть не лучшим вариантом
	};
	
	this.loadComments = function (good_id, onpage, pageNumber, callback) {
		var data = {
			good_id : good_id, 
			onpage : onpage, 
			page_number: pageNumber, 
			get_comments : 1
		};
		$.get(this.ajaxURL, data, function (html) {
			$('.b-review:last').after(html);
			callback(html);
		});
	};
	
	this.showMore = function () {
		this.loadComments(this.objectID, this.onpage, this.pageNumber + 1, function () {
			self.pageNumber ++;
			if(self.pages[self.pageNumber]) {
				$('.b-moreLink').text('Ещё ' + self.pages[self.pageNumber] + ' комментариев');
			} else {
				$('.b-moreLink').hide();
			}
		});
	}
	
}