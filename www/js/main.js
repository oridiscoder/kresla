$(function() {
	$.fn.autoClear = function () {
		// сохраняем во внутреннюю переменную текущее значение
		$(this).each(function() {
			$(this).data("autoclear", $(this).attr("value"));
		});
		$(this)
			.bind('focus', function() {   // обработка фокуса
				if ($(this).attr("value") == $(this).data("autoclear")) {
					$(this).attr("value", "").addClass('autoclear-normalcolor');
				}
			})
			.bind('blur', function() {    // обработка потери фокуса
				if ($(this).attr("value") == "") {
					$(this).attr("value", $(this).data("autoclear")).removeClass('autoclear-normalcolor');
				}
			});
		return $(this);
	}
});
// ======================================================================================================

/* осуществляем поиск кресел по заданным в форме параметрам */
function search(element) {
	
	var serchRes = $('.b-filter-resulSearch');
  	var filterOffset = $('.b-filter').offset();
	var filterOffsetTop = filterOffset.top;
	var offset = $(element).offset();
	var offsetTop = offset.top;
	
	/* отправляем запрос и после того рисуем блок с результатом */
	if(formCatalog != undefined) {
		formCatalog.importFormData($('#SearchForm'));
		formCatalog.find(function (data) {
			serchRes.find('span').text(data.count);
			serchRes.find('a').attr("href", data.url);
			serchRes.css('display', 'block').css({
	    		'top': offsetTop - filterOffsetTop,
	    		'marginTop': -8
	    	});
		});
	}
}

$(function(){



//$('.sertificates').lightBox();
	//выбираем картинку для показа на странице товара
	$('.click-to-select-img').click(function(){
		catalog.selectGoodImage($(this).data('photo-id'));
	});

/*
	$('.lightBoxImg > a').lightBox({
      imageLoading: "/img/lightbox-ico-loading.gif", 
      imageBtnClose: "/img/lightbox-btn-close.gif",
      imageBtnNext: "/img/lightbox-btn-next.gif",
      imageBtnPrev: "/img/lightbox-btn-prev.gif"
    });
*/

	

    $('.sertificates > a').lightBox({
      imageLoading: "/img/lightbox-ico-loading.gif", 
      imageBtnClose: "/img/lightbox-btn-close.gif",
      imageBtnNext: "/img/lightbox-btn-next.gif",
      imageBtnPrev: "/img/lightbox-btn-prev.gif"
    });

 

	$('.index-search input[type=text]').autoClear();

	// Слайдер для цены
	var price = {
		min : {
			curr: parseInt($('.b-filter-price input.amount-min').val()),
			def: parseInt($('.b-filter-price .ui-slider-min').text())
		}, 
		max : {
			curr: parseInt($('.b-filter-price input.amount-max').val()),
			def: parseInt($('.b-filter-price .ui-slider-max').text()),
		}
	};
	
	$( ".b-filter-price .b-slider").slider({
		range: true,
		stop: function( event, ui ) {
			var left = parseInt($(ui.handle).css('left'));
			var uiHandle = $(ui.handle).closest('.b-slider').find('.ui-handle');
			uiHandle.html(ui.value);
			
			var uiHandlePos = left - uiHandle.outerWidth()/2;
			$(uiHandle).css({
				'left': uiHandlePos,
				'display': 'block'
			});
			function hide() {
				$(uiHandle).css('display', 'none');
			}
			setTimeout(hide, 3000);
			
			search(this);
		},
		slide: function( event, ui ) {
			$( ".b-filter-price .amount-min" ).val(ui.values[ 0 ]);
			$( ".b-filter-price .amount-max" ).val(ui.values[ 1 ]);
		}
	});
	
	$( ".b-filter-price .b-slider").slider({min: price.min.def, max: price.max.def, values: [ price.min.curr, price.max.curr ], step: 100});
	$( ".b-filter-price .amount-min" ).val($( ".b-filter-price .b-slider" ).slider( "values", 0 ));
	$( ".b-filter-price .amount-max" ).val($( ".b-filter-price .b-slider" ).slider( "values", 1 ));
	
	
	// Слайдеры для размеров
	// ширина
	var width = {
		min : {
			curr: parseInt($('.m-wide input.amount-min').val()),
			def:  parseInt($('.m-wide .ui-slider-min').text())
		}, 
		max : {
			curr: parseInt($('.m-wide input.amount-max').val()),
			def:  parseInt($('.m-wide .ui-slider-max').text()),
		}
	};
	$( ".b-sizes .b-slider").slider({range: true,});
	$( ".m-wide .b-slider").slider({
		min: width.min.def,
		max: width.max.def,
		values: [ width.min.curr, width.max.curr ],
		step: 5,
		stop: function( event, ui ) {
			var left = parseInt($(ui.handle).css('left'));
			var uiHandle = $(ui.handle).closest('.b-slider').find('.ui-handle');
			uiHandle.html(ui.value);
			
			var uiHandlePos = left - uiHandle.outerWidth()/2;
			$(uiHandle).css({
				'left': uiHandlePos,
				'display': 'block'
			});
			function hide() {
				$(uiHandle).css('display', 'none');
			}
			setTimeout(hide, 3000);
		},
		slide: function( event, ui ) {
			$( ".m-wide .amount-min" ).val(ui.values[ 0 ]);
			$( ".m-wide .amount-max" ).val(ui.values[ 1 ]);
		}
	});
	$( ".m-wide .amount-min" ).val($( ".m-wide .b-slider" ).slider( "values", 0 ));
	$( ".m-wide .amount-max" ).val($( ".m-wide .b-slider" ).slider( "values", 1 ));

	// глубина
	var deep = {
		min : {
			curr: parseInt($('.m-deep input.amount-min').val()),
			def: parseInt($('.m-deep .ui-slider-min').text())
		},
		max : {
			curr: parseInt($('.m-deep input.amount-max').val()),
			def: parseInt($('.m-deep .ui-slider-max').text())
		}
	};
	
	$( ".m-deep .b-slider").slider({
		min: deep.min.def,
		max: deep.max.def,
		values: [ deep.min.curr, deep.max.curr ],
		step: 5,
		stop: function( event, ui ) {
			var left = parseInt($(ui.handle).css('left'));
			var uiHandle = $(ui.handle).closest('.b-slider').find('.ui-handle');
			uiHandle.html(ui.value);
			
			var uiHandlePos = left - uiHandle.outerWidth()/2;
			$(uiHandle).css({
				'left': uiHandlePos,
				'display': 'block'
			});
			function hide() {
				$(uiHandle).css('display', 'none');
			}
			setTimeout(hide, 3000);
		},
		slide: function( event, ui ) {
			$( ".m-deep .amount-min" ).val(ui.values[ 0 ]);
			$( ".m-deep .amount-max" ).val(ui.values[ 1 ]);
		}
	});
	$( ".m-deep .amount-min" ).val($( ".m-deep .b-slider" ).slider( "values", 0 ));
	$( ".m-deep .amount-max" ).val($( ".m-deep .b-slider" ).slider( "values", 1 ));
	
	// высота
	// Слайдер для высоты
	var height = {
		min : {
			curr: parseInt($('.m-height input.amount-min').val()),
			def:  parseInt($('.m-height .ui-slider-min').text())
		}, 
		max : {
			curr: parseInt($('.m-height input.amount-max').val()),
			def:  parseInt($('.m-height .ui-slider-max').text()),
		}
	};
	$( ".m-height .b-slider").slider({
		min: height.min.def,
		max: height.max.def,
		values: [ height.min.curr, height.max.curr ],
		step: 5,
		stop: function( event, ui ) {
			var left = parseInt($(ui.handle).css('left'));
			var uiHandle = $(ui.handle).closest('.b-slider').find('.ui-handle');
			uiHandle.html(ui.value);
			
			var uiHandlePos = left - uiHandle.outerWidth()/2;
			$(uiHandle).css({
				'left': uiHandlePos,
				'display': 'block'
			});
			function hide() {
				$(uiHandle).css('display', 'none');
			}
			setTimeout(hide, 3000);
		},
		slide: function( event, ui ) {
			$( ".m-height .amount-min" ).val(ui.values[ 0 ]);
			$( ".m-height .amount-max" ).val(ui.values[ 1 ]);
		}
	});
	$( ".m-height .amount-min" ).val($( ".m-height .b-slider" ).slider( "values", 0 ));
	$( ".m-height .amount-max" ).val($( ".m-height .b-slider" ).slider( "values", 1 ));

	// Шкала значений над слайдеров
	$('.b-slider').each(function(){
		var inpMin = $(this).slider( "option", "min" );
		var inpMax = $(this).slider( "option", "max" );
		var inpMed = parseFloat((inpMin + inpMax)/2);

		$(this).parent('div').find('.b-slider-scale .ui-slider-min').html(inpMin);
		$(this).parent('div').find('.b-slider-scale .ui-slider-max').html(inpMax);
		$(this).parent('div').find('.b-slider-scale .ui-slider-medium').html(inpMed);
	});


	$(".b-filter .amount-min").each(function(){
		$(this).change(function(){
			var value1 = $(this).val();
			var value2 = $(this).closest('.b-filter-inputs').find('.amount-max').val();

			if(parseInt(value1) > parseInt(value2)) {
				value1 = value2;
				$(this).val(value1);
			}
			$(this).closest('.b-filter-inputs').siblings('.b-slider').slider("values",0,value1);
		});
	});

	$(".b-filter .amount-max").each(function(){
		$(this).change(function(){
			var value1 = $(this).closest('.b-filter-inputs').find('.amount-min').val();
			var value2 = $(this).val();

			if(parseInt(value1) > parseInt(value2)) {
				value2 = value1;
				$(this).val(value2);
			}
			$(this).closest('.b-filter-inputs').siblings('.b-slider').slider("values",1,value2);
		});
	});

	var filterBlockTitle = $('.b-filter .b-title-name'),
		filterCont = $('.b-filter .b-filterContent');
	
	filterCont.show();

	filterBlockTitle.click(function(e){
		e.preventDefault();

		$(this).toggleClass('active');
		$(this).closest('.b-title').parent('div').find(filterCont).slideToggle(500);

	});

	var hint = $('.b-hint'),
		hintTool = $('.b-hint div');

	hint.click(function(){
		hint.not($(this)).removeClass('active');
		hint.not($(this)).find('div').css({
			'display': 'none',
			'opacity' : '0',
			'marginTop' : -35
		});

		$(this).toggleClass('active');

		if( $(this).hasClass('active') ) {
			$(this).find('div').css('display', 'block');
			$(this).find('div').animate({
				'marginTop' : -5,
				'opacity' : 1
			}, 500);
		} else {
			$(this).find('div').animate({
				'marginTop' : -35,
				'opacity' : 0
			}, 500, function(){
				hintTool.css('display', 'none');
			});
		}
	});

	$(document).bind('click', function (e) {
		if ($(e.target).closest(hint).length == 0) {
			hint.removeClass('active');
		  	hintTool.animate({
				'marginTop' : -35,
				'opacity' : 0
			}, 500, function(){
				hintTool.css('display', 'none');
			});
			$(document).unbind('click.myEvent');
		}
	});

	// special-offer slider
	var spSlider = $('.special-offer'),
		spProgress = spSlider.find('.progress'),
		spPrScroll = spProgress.find('.scroll'),
		spPrev = spSlider.find('.prev'),
		spNext = spSlider.find('.next'),
		spSlide = spSlider.find('.content'),
		spImage = spSlider.find('.img-wrap img');

	$(window).load(function(){
		var sl_length = spSlide.length;
		spPrScroll.width(spProgress.width()/sl_length);
	});


	spNext.click(function(e){
		var spSlideActive = spSlider.find('.content.active');
		var spImgActive = spSlider.find('.img-wrap img.active');
		e.preventDefault();
		var idx = spSlideActive.index();
		var next = idx+1;

		if(idx == spSlide.length-1) {
			idx = 0;
			next = 0;
		}

		spSlideActive.fadeOut(300, function(){
			spSlideActive.removeClass('active')
			spSlide.eq(next).fadeIn(300, function(){
				spSlide.eq(next).addClass('active')
			});
		});
		spImgActive.fadeOut(300, function(){
			spImgActive.removeClass('active')
			spImage.eq(next).fadeIn(300, function(){
				spImage.eq(next).addClass('active')
			});
		});

		spPrScroll.animate({
			marginLeft: spPrScroll.width()*next
		}, 300);
	});

	spPrev.click(function(e){
		var spSlideActive = spSlider.find('.content.active');
		var spImgActive = spSlider.find('.img-wrap img.active');
		e.preventDefault();
		var idx = spSlideActive.index();
		var prev = idx-1;

		if(idx == 0) {
			idx = spSlide.length-1;
			prev = spSlide.length-1;
		}

		spSlideActive.fadeOut(300, function(){
			spSlideActive.removeClass('active')
			spSlide.eq(prev).fadeIn(300, function(){
				spSlide.eq(prev).addClass('active')
			});
		});
		spImgActive.fadeOut(300, function(){
			spImgActive.removeClass('active')
			spImage.eq(prev).fadeIn(300, function(){
				spImage.eq(prev).addClass('active')
			});
		});

		spPrScroll.animate({
			marginLeft: spPrScroll.width()*prev
		}, 300);
	});

	// jscrollpane
	$(window).load(function(){
    	$('.list-wrap').jScrollPane({
    		mouseWheelSpeed: 10
    	});
    });

	var serchRes = $('.b-filter-resulSearch');
	
    $('.b-filter label > input').bind('change', function(e){
    	e.stopPropagation();
    	
    	search(this);
    });
    
   $('.l-catalog.m-modal').modalWindow({
		overlay: {
			initStyles: {
				opacity: 0,
				background: 'black'
			},
			showAnimate: {
				opacity: .65
			},
			showDuration: 1000
		}
	});
	$('.b-recentlyViewed .b-more').click(function(e){
		e.preventDefault();
		var modalBlock = $('.l-catalog.m-modal');
		
		var list = $(this).parents('div.b-list').eq(0).parent().find(".b-catalog-list").eq(0);
		
		modalBlock.find("h3").text("Недавно просмотренные").after(list.show());
		modalBlock.css('display', 'block');
		modalBlock.modalWindow('open');
	});

	$('.b-catalog-wrapper .b-item').click(function(e){
		var href = $(this).attr('data-ref');
		window.location = href;
	});

	var propTabs = $('.b-prodProp-tabs .b-item'),
		propPanes = $('.b-prodProp-panes .b-item');

	propTabs.click(function(e){
		e.preventDefault();
		var idx = $(this).index();

		propTabs.removeClass('active');
		$(this).addClass('active');

		propPanes.removeClass('active');
		propPanes.eq(idx).addClass('active');
	});	

	$('.b-addReview-link').click(function(e){
		e.preventDefault();

		$(this).toggleClass('active');
		$('.b-addReview').slideToggle(500);
	});
	
	var viewTypeLinks = $('.b-catalog-filters .b-viewType a');
	viewTypeLinks.click(function(e){
		e.preventDefault();

		if($(this).is('active')) return false;

		if($(this).hasClass('table')) {
			viewTypeLinks.removeClass('active');
			$(this).addClass('active');
			
			$.cookie("viewType", "table");
			$('.l-catalog .b-catalog-list').css('display', 'none');
			$('.b-catalog-table').css('display', 'block');
		} else {
			viewTypeLinks.removeClass('active');
			$(this).addClass('active');
			
			$.cookie("viewType", "list");
			$('.b-catalog-table').css('display', 'none');
			$('.l-catalog .b-catalog-list').css('display', 'block');
		}
	});
});


function spellcount(num, one, two, many) {
	nodigit = false;
	if (num%10==1 && num%100!=11){
		return  ((!nodigit)?num:'')+' '+one;
	}
	else if(num%10>=2 && num%10<=4 && (num%100<10 || num%100>=20)){
		return ((!nodigit)?num:'')+' '+two;
	}
	else{
		return ((!nodigit)?num:'')+' '+many;
	}

}

$(function(){
	
	
	var a = window.CountSin;
	var more = $('.b-colors .sinonimus');
	var but  = $('.b-colors .b-moreItems');
	more.css('display','none');
	but.find('span').html('Ещё '+spellcount(a, 'цвет', 'цвета', 'цветов'));
	
	but.on('click', function(){
		if(more.css('display')=='none'){
			
			more.css('display', 'inline');
			but.find('span').html('Убрать');
		}
		else{
			
			more.css('display', 'none');
			but.find('span').html('Ещё '+spellcount(a, 'цвет', 'цвета', 'цветов'));
		}
		
		return false;
	});
	
	

});