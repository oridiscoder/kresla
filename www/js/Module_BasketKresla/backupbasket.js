function Basket (formObject) {
	
	var self = this;
	this.formObject = $(formObject);
	
	this.formObject.find('.ajax_submit_button').click(function () {
		$(this).off();
		$(this).css("background", "url(/img/progress.gif)");
		self.formObject.submit();
	});
	
	this.formObject.find('.basket_item_input').keyup(function () {
		if(isNaN(this.value) || this.value < 0) {
			this.value = 0;
		}
		self.recalculate();
	});
	
	this.formObject.find('.basket_remove_item').click(function () {
		if(confirm("Правда не хотите купить это замечательное кресло?")) {
			var row = $(this).parents("tr").eq(0);
			row.find(".basket_item_input").val(0);
			
			var data = self.formObject.serialize();
			data += '&action=recalculate';
			
			self.showProgress();
			$.post(self.formObject.attr("action"), data, function (html) {
				self.stopProgress();
				try {
					var response = eval("(" + html + ")");
					$('.cost').text(response.totalCost);
					row.remove();
					$('#DeliveryType').change();
				} catch (ex) {
					alert("К сожалению, произошла ошибка.");
				}
			});
		}
	});
	
	this.push = function (data) {
		$.post(this.url, data, function (html) {
			$('#Basket').html(html);
		});
	};
	
	this.recalculate = function () {
		var data = this.formObject.serialize();
		data += '&action=recalculate';
		
		this.showProgress();
		
		$.post(this.formObject.attr("action"), data, function (html) {
			self.stopProgress();
			try {
				var response = eval("(" + html + ")");
				$('.cost').text(response.totalCost);
				$('#DeliveryType').change();
			} catch (ex) {
				alert("К сожалению, произошла ошибка.");
			}
		});
	};
	
	this.showProgress = function () {
		$('#Recalc').show();
	};
	this.stopProgress = function () {
		$('#Recalc').hide();
	};
	
	this.switchForm = function (type) {
		if(type == 'juridical') {
			$('.juridical').show();
			$('.individual').hide();
		} else {
			$('.juridical').hide();
			$('.individual').show();
		}
		$('#client_type').val(type);
	};
	
	this.changeDeliveryType = function (event) {
		var select = this;
		if(typeof deliveryTypes == "object") {
			var orderCost = parseInt($('.cost').eq(0).text());
			for(key in deliveryTypes) {
				var dt = deliveryTypes[key];
				if(dt.id == select.value) {
					var deliveryPrice = '';
					if(orderCost >= dt.bound) {
						deliveryPrice = dt.low_cost;
					} else {
						deliveryPrice = dt.high_cost;
					}
					$('#DeliveryPrice').text(deliveryPrice);
					deliveryPrice = parseInt(deliveryPrice);
					
					if(!isNaN(deliveryPrice)) {
					
						$('#TotalCost').text(orderCost + deliveryPrice);
						$('#BaseCost').val(orderCost + deliveryPrice);
						
					}else{
						$('#TotalCost').text(orderCost);
						$('#BaseCost').val(orderCost);
						
						
						
						
						
						
					}
				}
			}
		}
	};
	
	$('#DeliveryType').change(this.changeDeliveryType);
	
	this.submitOrderForm = function () {
		var pr = $('#load');
		var ok = $('#ok');
		ok.css('display', 'none');
		pr.css('display', 'block');
		var form = $('#OrderForm');
		var valid = true;
		var nameInput = form.find('input[name="client_name"]');
		if(nameInput.val().length == 0) {
			this.showErrorBox(nameInput, "Представьтесь, пожалуйста");
			nameInput.keyup(function () {
				if(this.value.length > 0) {
					self.hideErrorBox($(this));
				}
			});
			nameInput.focus();
			valid = false;
		}
		
		var email = form.find('input[name="client_email"]');
		var phone = form.find('input[name="client_phone_number"]');
		var cellphone = form.find('input[name="client_cellphone"]');
		
		if(email.val().length == 0 && phone.val().length == 0 && cellphone.val().length ==0) {
			this.showErrorBox(email, "Укажите свой e-mail или номер телефона");
			email.keyup(function () {
				if(this.value.length > 0) {
					self.hideErrorBox(email);
					self.hideErrorBox(phone);
					self.hideErrorBox(cellphone);
				}
			});
			email.focus();
			valid = false;
		} else {
			self.hideErrorBox(email);
			self.hideErrorBox(phone);
			self.hideErrorBox(cellphone);
		}
		
		if(valid) {
			
			form.submit();
		}else{
			pr.css('display', 'none');
			ok.css('display', 'block');
		}
	};
	
	this.showErrorBox = function (element, message) {
		var div = $('<div>').addClass("error_box").text(message);
		element.after(div);
	};
	
	this.hideErrorBox = function (element) {
		var next = element.next();
		if(next.hasClass('error_box')) {
			next.remove();
		}
	}
	$('#BaseCost').val($('#TotalCost').text());
}