

if (typeof LTX_VERSION === 'undefined') {
  window.LTX_VERSION = '3.0.1';
}

if (typeof LTX_URL === 'undefined') {
  window.LTX_URL = '//cs15.livetex.ru';
  if (typeof window.livetexTest !== 'undefined') {
    window.LTX_URL += ':' + window.livetexTest;
  }
  window.LTX_URL += '/';
}

function ltxNop() {}

function ltxLoadScript(url, opt_callback) {
  var callback = opt_callback || ltxNop;
  var headElem = document.getElementsByTagName("head")[0];

  function completeHandler() {
    script.onreadystatechange = ltxNop;
    script.onload = ltxNop;

    headElem.removeChild(script);

    clearTimeout(timeout);

    callback();
  }

  var timeout = setTimeout(completeHandler, 1000);
  var script = document.createElement('script');

  script.onreadystatechange = function() {
    if (script.readyState === 'complete' ||
        script.readyState === 'loaded') {
      completeHandler();
    }
  };

  script.onload = completeHandler;
  script.src = LTX_URL + url + '?' + LTX_VERSION;

  headElem.appendChild(script);
}


(function(window, undefined){
  var ltx = window['LiveTex'];

  var onLiveTexReady = ltxNop;

  if (typeof ltx === 'object' && typeof ltx['onLiveTexReady'] === 'function') {
    onLiveTexReady = ltx['onLiveTexReady'];
  }

  ltxLoadScript('js/lt.js', function() {
    ltxLoadScript('js/livetexSettings.js', function() {
      ltxLoadScript('js/client_main.js', function() {
        LiveTex.replaceWidgets(function() {
          onLiveTexReady.call(ltx);
        });
      });
    });
  });
})(this);
