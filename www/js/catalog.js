var Catalog = function () {
	var self = this;
	/* куда будем отправлять запросы */
	this.ajaxURL = '/catalog/ajax/';
	this.params = [];
	this.hits_label_id = 3;
	this.order = {
		name : null, 
		dir: "ASC"
	};
	
	this.importFormData = function (formObject) {
		this.params = formObject.serializeArray();
	};
	
	this.clearParams = function (exclude) {
		for(k in this.params) {
			if(exclude != undefined && exclude.indexOf(this.params[k].name) == -1) {
				this.params.splice(k, 1);
			}
		}
	};
	
	this.removeParam = function (name) {
		for(k in this.params) {
			if(this.params[k].name == name) {
				this.params.splice(k, 1);
			}
		}
	};
	
	this.where = function (key, value) {
		if(value === undefined) {
			for(k in this.params) {
				if(this.params[k].name == key) {
					return this.params[k].value;
				}
			}
			return this.params[key];
		} else {
			var found = false;
			for(k in this.params) {				
				if(this.params[k].name == key) {
					if(value === null) {
						var params = this.params;
						params.splice(k, 1);
						this.params = params;
					} else {
						this.params[k].value = value;
					}
					found = true;
					break;
				}
			}
			if(found == false) {
				this.params.push({
					name: key, 
					value: value
				});
			}
		}
	};
	
	this.find = function (callback) {
		this.where("action", "get_info");
		$.get('/catalog/search/',this.params, function (html) {
			try {
				var data = eval('(' + html + ')');
				callback(data);
			} catch (ex) {
				alert("Произошла ошибка: " + ex);
			}
		});
	}
	
	this.setFilter = function (input) {
		
		var input = $(input);
		if(input.attr("type") == 'checkbox') {
			var queryString = window.location.search;
			var query = queryString.replace("?", "").split('&');
			
			var found = false;
			var qLength = 0;
			for(key in query) {
				if(query[key] != undefined && query[key].length > 0) {
					pair = query[key].split("=");
					if(pair[0] == input.attr("name")) {
						found = true;
						if(!input.is(":checked")) {
							query.splice(key, 1);
						}
					}
				} else {
					query.splice(key, 1);
				} 
			}
			
			if(!found && input.is(":checked")) {
				query.push(input.attr('name') + '=1');
			}
			
			queryString = query.join('&');
			
			window.location.search = queryString;
		}
	};
	
	this.showHits = function (show) {
		this.clearParams(["categories[]", "order"]);
		if(show) {
			this.where("label[]", this.hits_label_id);
		} else {
			this.removeParam("label[]");
		}
		this.update();
	};
	
	/* запрашиваем новые данные с сервера и обновляем контейнер с списком товаров */
	this.update = function () {
		this.request(function (html) {
			self.updateCatalogHTML(html);
		});
	}
	
	/* отправка запроса на сервер */
	this.request = function (callback) {
		this.showAjaxLoader();
		/* как сортируем */
		if(this.order.name) {
			this.where("order[name]", this.order.name);
			this.where("order[dir]", this.order.dir);
		}
		this.where("action", "get_list");
		
		$.get('/catalog/search/',this.params, callback);
	};
	
	
	this.sort = function (colname) {
		
		if(this.order.name == colname) {
			this.order.dir = this.order.dir == "ASC" ? "DESC" : "ASC"; 
		} else {
			this.order = {
				name : colname, 
				dir: "ASC"
			}
		}
		
		this.update();
	}	
	this.updateCatalogHTML = function (html) {
		$('.MiddelRight > .m-inside > .b-catalog-list').html(html);
	};
	this.showAjaxLoader = function () {
		$('.b-catalog-list .b-item').animate({opacity: 0.1}, 1000);
	};
	this.hideAjaxLoader = function () {
		//пока нечего показывать
	}
	this.showAsCards = function () {
		$('.b-catalog-list').removeClass("as-table");
	};
	this.showAsTable = function () {
		$('.b-catalog-list').addClass("as-table");
	};
	
	
	/* переключалка фотографий на странице товара */
	this.selectGoodImage = function (imageId) {
		
		//$('.lightBoxImg a, .lightBoxImg div').hide();
		
		$('.lightBoxImg img').hide();
		$('.lightBoxImg .zoom').hide();

		$('.selectPik li').removeClass("selekt");		
		
		$('#img' + imageId).show();
		$('.img' + imageId).show();
		
		//$('.a-' + imageId).show();

		$('#thumb' + imageId).addClass("selekt");
		
		
		
		
	};
	
	/* выбираем обивку кресла на странице товара */
	this.bindUpholsterySelectEvents = function () {
		$('.selectable').click(function () {
			
/*			как карусель переключить на нужный цвет
			var id = $(this).attr('data-color');
			$('li[data-photo-id="'+num+'"]').click();
*/

		    $('.selectable').each(function () {
		      $(this).removeClass('selected');
		      $(this).find('label img').eq(1).removeClass('hidden');
		    });
		    $(this).addClass('selected');
		    $(this).find('label img').eq(1).addClass('hidden');

		  });
		$('.selectable_p').click(function () {
		    $('.selectable_p').each(function () {
		      $(this).removeClass('selected');
		      $(this).find('label img').eq(1).removeClass('hidden');
		    });
		    $(this).addClass('selected');
		    $(this).find('label img').eq(1).addClass('hidden');
		  });
		

	};
	
	$('input[name="upholstery_id"]').click(function () {
	  $('#Price').text($(this).attr('rel'));
	  
	  var newPrice = +($('#productCount').val()) * ($(this).attr('rel'));
	  $('#newPrice').text(newPrice);
	});
	
	$('#productCount').change(function(){
		var newPrice = +($(this).val()) * ($('input[name="upholstery_id"]:checked').attr('rel'));
		$('#newPrice').text(newPrice);
	});
	
	$('.cartAmount .minus').click(function() {
		var oldCount = $('#productCount').val();
		
		if (oldCount > 1) {
			oldCount--;
			$('#productCount').val(oldCount);
			$('#productCount').change();
		} else return;
	});
	
	$('.cartAmount .plus').click(function() {
		var oldCount = $('#productCount').val();
		
		oldCount++;
		$('#productCount').val(oldCount);
		$('#productCount').change();
	});
	
	
	$("#productCount").keypress(function( key ) {
		var length = $(this).val().length;
		var re = /[0-9]/;
		var a = key.charCode;

		
		if (a==48 && length==0) {
			return false;
		}
		
		var b = String.fromCharCode(a);
		return !!( a==0 || b.match(re));
	});
	
	$("#productCount").keyup(function(){
		$('#productCount').change();
		if ($("#productCount").val().length == 0) {
			$("#productCount").addClass('error');
		} else {
			$("#productCount").removeClass('error');
		}
	});
	
	$('.btn-dialog-hid').click(function(){
		var id = $(this).attr('href');
		$('.hidden-form').hide();
		$(id).fadeIn(300);
		var coords = $(this).offset();
		setCoords(id, coords.top-100, coords.left-120);	
		event.preventDefault();

	});
	
	function setCoords(id, cTop, cLeft) {
		$(id).offset({top: cTop, left: cLeft});
	}
	
	$('.hidden-form-close').click(function(){
		$(this).parents('.hidden-form').fadeOut();
	});
	
	
	
// 	$('.kreslo-toggle').click(function () {
// 
// 		$('#tables-filter').addClass("hidden");
// 		$('#SearchForm').removeClass("hidden");
// 		$('.active').removeClass('active');
// 		$(this).addClass('active');
// 		
// 	});
// 	$('.table-toggle').click(function () {
// 
// 		$('#SearchForm').addClass("hidden");
// 		$('#tables-filter').removeClass("hidden");
// 		$('.active').removeClass('active');
// 		$(this).addClass('active');
// 		
// 	});

}

