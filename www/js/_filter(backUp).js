function filterObj(filter) {
	that = this;
	that.filter = filter;
	that.filterForm = that.filter.find('#b-filter-form');
	that.inpCheckbox = that.filter.find('input[type=checkbox]');
	that.inpText = that.filter.find('.b-filter-inputs input[type=text]');

	that.init = function() {
		that.filterSliderInit();
		that.onChangeEvent();
		that.filterHint();
		that.openBox();
		that.submitForm();
	};

	that.filterSliderInit = function() {
		var ft_slider = that.filter.find('.b-slider');
		var sliderBlock = that.filter.find('.b-sliderWrap');

		ft_slider.each(function(){
			var	ft_slider_min = $(this).closest(sliderBlock).find('.amount-min'),
				ft_slider_max = $(this).closest(sliderBlock).find('.amount-max');
			var amounMin = +ft_slider_min.attr('data-ref'),
				amounMax = +ft_slider_max.attr('data-ref'),
				step = +($(this).attr('data-ref'));
			ft_slider_min.val(amounMin);
			ft_slider_max.val(amounMax);

			$(this).slider({
				range: true,
				min: amounMin,
				max: amounMax,
				values: [ amounMin, amounMax ],
				step: step,
				slide: function( event, ui ) {
					ft_slider_min.val(ui.values[ 0 ]);
					ft_slider_max.val(ui.values[ 1 ]);

					var left = parseInt($(ui.handle).css('left'));
					var uiHandle = $(ui.handle).closest('.b-slider').find('.ui-handle');
					uiHandle.html(ui.value);
					
					var uiHandlePos = left - uiHandle.outerWidth()/2;
					$(uiHandle).css({
						'left': uiHandlePos,
						'display': 'block'
					});
					function hide() {
						$(uiHandle).css('display', 'none');
					}
					setTimeout(hide, 7000);
				}
			});
			$(amounMin).val(ft_slider.slider( "values", 0 ));
			$(amounMax).val(ft_slider.slider( "values", 1 ));

			// Изменение параметра слайдера по вводу значения в инпут
			ft_slider_min.change(function(){
				var value1 = $(this).val();
				var value2 = $(this).closest('.b-filter-inputs').find('.amount-max').val();

				if(parseInt(value1) > parseInt(value2)) {
					value1 = value2;
					$(this).val(value1);
				}
				$(this).closest('.b-filter-inputs').siblings('.b-slider').slider("values",0,value1);
			});
			ft_slider_max.change(function(){
				var value1 = $(this).closest('.b-filter-inputs').find('.amount-min').val();
				var value2 = $(this).val();

				if(parseInt(value1) > parseInt(value2)) {
					value2 = value1;
					$(this).val(value2);
				}
				$(this).closest('.b-filter-inputs').siblings('.b-slider').slider("values",1,value2);
			});

			// Значения на шкалах
			var labelMin = $(this).closest(sliderBlock).find('.ui-slider-min'),
				labelMedium = $(this).closest(sliderBlock).find('.ui-slider-medium'),
				labelMax = $(this).closest(sliderBlock).find('.ui-slider-max');
			labelMin.html(amounMin);
			labelMedium.html((amounMin+amounMax)/2);
			labelMax.html(amounMax);
		});
	};

	// изменение любого элемента
	that.onChangeEvent = function() {

	};

	// всплывающие подсказки
	that.filterHint = function() {
		var hint = that.filter.find('.b-hint'),
			hintTool = hint.find('div');

		hint.click(function(){
			hint.not($(this)).removeClass('active');
			hint.not($(this)).find('div').css({
				'display': 'none',
				'opacity' : '0',
				'marginTop' : -35
			});

			$(this).toggleClass('active');

			if( $(this).hasClass('active') ) {
				$(this).find('div').css('display', 'block');
				$(this).find('div').animate({
					'marginTop' : -5,
					'opacity' : 1
				}, 500);
			} else {
				$(this).find('div').animate({
					'marginTop' : -35,
					'opacity' : 0
				}, 500, function(){
					hintTool.css('display', 'none');
				});
			}
		});

		$(document).bind('click', function (e) {
			if ($(e.target).closest(hint).length == 0) {
				hint.removeClass('active');
			  	hintTool.animate({
					'marginTop' : -35,
					'opacity' : 0
				}, 500, function(){
					hintTool.css('display', 'none');
				});
				$(document).unbind('click.myEvent');
			}
		});
	};

	// Открытие фильтров
	that.openBox = function() {
		this.filterBlockTitle = that.filter.find('.b-title-name');

		that.inpCheckbox.each(function(i){
			if($(this).is(':checked')) {
				$(this).closest('.b-checkbox').find('.b-title-name').addClass('active')
			}
		});

		this.filterBlockTitle.each(function(){
			if( $(this).hasClass('active') ) $(this).closest('.b-checkbox').find('.b-filterContent').css('display', 'block');
		});


		this.filterBlockTitle.bind('click', function(e){
			e.preventDefault();

			$(this).toggleClass('active');
			$(this).closest('.b-checkbox').find('.b-filterContent').slideToggle(500);
		});
	};

	// Создание объекта с отмеченными чекбоксами и открытыми фильтрами
	that.configJsonObj = function() {
		this.checkbox = that.filter.find('input[type=checkbox]');
		var checkBoxCheck = [];
		var checkBoxOpenArr = [];
		var checkBoxObj = {};

		this.checkbox.each(function(i){
			if( $(this).is(':checked') ) {
				$(this).closest('.b-checkbox').addClass('open');
				checkBoxCheck.push(i);
			}
		});

		$('.b-checkbox').each(function(i){
			if( $(this).hasClass('open')) {
				checkBoxOpenArr.push(i)
			}
		});

		var checkBoxObj = {
			checkOpen : checkBoxOpenArr,
			checkSelect : checkBoxCheck
		}

		console.log(checkBoxObj);

		return false;

	};
	
	that.submitForm = function() {
		this.submitForm_1 = that.filter.find('.showSearchResult');
		this.submitForm_2 = that.filter.find('input[type=submit]');

		this.submitForm_1.bind('click', function(e){
			e.preventDefault();
			that.configJsonObj();
			// that.filterForm .submit();
		});

		this.submitForm_2.bind('click', function(e){
			e.preventDefault();
			that.configJsonObj();
			// that.filterForm .submit();
		});
	};
}



/////////////////////////////////////////////////////////////////////////

$(function(){
	var filter = $('.MiddelLeft .b-filter');
	var Obj = new filterObj(filter);
	Obj.init();
});