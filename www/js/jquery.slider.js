(function($){
	$.plugin('slider', {
		defaults: {
			slideshowSpeed: 0,
			animateSpeed: 1000,
			effect: '',
			effects: {},
			elements: {
				wrapEl: '.slider-wrap',
				slidesEl: '.slider-list',
				slidesEls: '.slider-list > *',
				prevEl: '.slider-prev',
				nextEl: '.slider-next',
				indicatorsEl: '.slider-indicators'
			},
			indicatorActiveClass: 'active'
		},
		idx: 0,
		slideshowInterval: null,
		init: function(){
			var that = this;

			this.bindAll('prev', 'next');
			this.findAll(this.options.elements);

			this.indicatorsEl.empty();

			this.slidesEls.each(function(i){
				$('<a/>', {href:'#'}).appendTo(that.indicatorsEl);
			});

			this.indicators = this.indicatorsEl.children();

			this.indicators.click(function(e){
				e.preventDefault();
				that.setActive(that.indicators.index(this));
			});

			if ($.isFunction(this.options.effects[this.options.effect]))
				this.effectFn = this.options.effects[this.options.effect];

			this.prevEl.click(this.prev);
			this.nextEl.click(this.next);

			this.setActive(0);
		},
		effectFn: function(newSlide, newIdx){
			newSlide.show().siblings().hide();
		},
		prev: function(e){
			if (e && e.preventDefault)
				e.preventDefault();

			this.setActive((this.idx-1)%this.slidesEls.length);
		},
		next: function(e){
			if (e && e.preventDefault)
				e.preventDefault();

			this.setActive((this.idx+1)%this.slidesEls.length);
		},
		setActive: function(newIdx){
			if (newIdx < 0)
				newIdx = this.slidesEls.length-1;

			var newSlide = this.slidesEls.eq(newIdx);

			this.indicators.eq(newIdx).addClass(this.options.indicatorActiveClass).siblings().removeClass(this.options.indicatorActiveClass);

			this.wrapEl.attr('href', newSlide.data('href'));

			if (this.options.slideshowSpeed) {
				clearInterval(this.slideshowInterval);
				this.slideshowInterval = setInterval(this.next, this.options.slideshowSpeed);
			}

			if (this.idx !== newIdx)
				this.effectFn.call(this, newSlide, newIdx);

			this.idx = newIdx;
		}
	});

	$.slider.defaults.effects['fade'] = function(newSlide, newIdx){
		this.slidesEls.eq(this.idx).css({
			zIndex: 2
		}).siblings().css({
			zIndex: 1
		});

		newSlide.css({
			zIndex: 3,
			opacity: 0
		}).animate({
			opacity: 1
		}, this.options.animateSpeed, 'linear');
	};

	$.slider.defaults.effects['hslide'] = function(newSlide, newIdx){
		this.slidesEl.stop().animate({
			'margin-left': -newSlide.position().left
		}, this.options.animateSpeed);
	};

	$.slider.defaults.effects['hcycle'] = function(newSlide, newIdx){
		var slide = this.slidesEls.eq(this.idx);
		var isLastToFirst = (this.idx === this.slidesEls.length-1 && newIdx === 0);
		var isFirstToLast = (newIdx === this.slidesEls.length-1 && this.idx === 0);
		var dirLeft = isFirstToLast || (newIdx < this.idx && !isLastToFirst);
		
		if (this.slidesEls.length === 2)
			dirLeft = false;

		if (dirLeft) {
			this.slidesEl.css({
				'margin-left': -newSlide.width()
			});
			
			slide.prependTo(this.slidesEl).before(newSlide);

			this.slidesEl.stop().animate({
				'margin-left': 0
			}, this.options.animateSpeed);
		} else {
			this.slidesEl.css({
				'margin-left': 0
			});
			
			slide.prependTo(this.slidesEl).after(newSlide);

			this.slidesEl.stop().animate({
				'margin-left': -slide.width()
			}, this.options.animateSpeed);
		}
	};
})(jQuery);