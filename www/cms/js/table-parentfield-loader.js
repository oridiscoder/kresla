ParentLoader =  {
	init: function() {
		$('span.home_ico2').off('click');
		$('span.home_ico2').click(this.loadParent);
	}, 
	loadParent: function () {
		//$this - это уже span
		ParentLoader.showLoader(this);
		ParentLoader.fetchRemoteData(this);
		return false;
	}, 
	showLoader: function(span) {
		var img = $('<img>').attr('src', '/cms/images/ajax-loader.gif').css('margin-left', '-4px');
		$(span).append(img);
	}, 
	hideLoader: function(data) {
		this.getSpan(data).find('img').remove();
	}, 
	fetchRemoteData: function (span) {
		var rel = $(span).parents('tr').eq(0).attr('rel');
		if(rel) {
			var id = rel.replace("item_", "");
		} else {
			var id = $(span).parents('form').find('input[name="id"]').val();
		}
		var data = $(span).attr('rel').split(',');
		var id = data[0];
		var fieldID = data[1];
		if(id) {			
			var url = window.location.pathname + 'load_parent/';
			var data = {
				record_id: id, 
				field_id: fieldID
			};
			
			$.get(url, data, ParentLoader.processResponse);		
		}
	}, 
	processResponse: function(html) {
		var data = eval('(' + html + ')');
		if(!data.error) {
			ParentLoader.drawResponse(data);
			ParentLoader.afterLoad();
		} else {
			ParentLoader.drawError(data);
		}
		ParentLoader.hideLoader(data);
	},
	getSpan: function (data) {
		var recordID = data.record_id;
		var fieldID = data.field_id;
		var trRel = 'item_' + recordID;
		var spanRel = recordID + ',' + fieldID;
		
		return $('span[rel="'+spanRel+'"]').eq(0);
	}, 
	drawResponse: function(data) {
		
		var span = this.getSpan(data);
		var div = $('<div>').addClass("parent-field-loaded");
		var titleDiv = $('<div>').addClass("title");
		var aClose = $('<span>').addClass('close_button').click(function() {
			$(this).parent().parent().remove();
		}).text('Закрыть');
		titleDiv.append(aClose);
		div.append(titleDiv);
		var table = $('<table>');
		for(var i = 0; i < data.data.length; i++) {
			var tr = $('<tr>').attr('rel', 'item_' + data.id);
			$('<td>').addClass('name').html(data.data[i].viewname).appendTo(tr);
			$('<td>').html(data.data[i].html).appendTo(tr);
			table.append(tr);
		}
		div.append(table);
		span.after(div);
		div.draggable({
			handle: ".title"
		});
		ParentLoader.init();
	}, 
	drawError: function (data) {
		var span = this.getSpan(data);
		var div = $('<div>').addClass("parent-field-loaded").addClass('error');
		var aClose = $('<span>').addClass('close_button').click(function() {
			$(this).parent().remove();
		}).text('Закрыть');
		div.append(aClose);
		var p = $('<p>').text(data.error);
		div.append(p);
		span.after(div);
	}, 
	afterLoad: function() {
		$('tr.tr-data a.show_hidden_text').click(function () {
			 $(this).siblings('div.hidden_text').toggle();
			 return false;
		 });
	}
};