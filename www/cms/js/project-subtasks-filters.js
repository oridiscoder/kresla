/**
 * Класс для фильтрации задач, подгруженных на странице списка проектов. 
 */
var SubTaskFilter = {
		url: null, 
		projectID: null, 
		init: function(id, url) {
			this.url = url;
			var table = $('#ProjectItem_' + id + ' div.entry table.general_i').eq(0);
			table.find('tr.form input[type="text"]').keyup(this.filter);
			table.find('tr.form select').change(this.filter);
			table.find('tr.form input[type="checkbox"]').click(this.showClosedClick);
			table.find('tr.form a.fire').click(this.fireClick);
			table.find('tr.form a.new').click(this.newClick);
			table.find('tr.form a.plus_none').click(this.commentsClick);
			table.find('tr.form a.orderby').click(this.orderByClick);
		},
		
		getFilterFormElements: function () {
			var id = SubTaskFilter.projectID;
			var table = $('#ProjectItem_' + id + ' div.entry table.general_i').eq(0);
			return table.find('tr.form input, tr.form select');
		},

		filter: function(event) {
			if(window.filterTimeout) {
				window.clearTimeout(window.filterTimeout);
			}
			
			SubTaskFilter.projectID = SubTaskFilter.getProjectIdByFormElement(this);
			window.filterTimeout = window.setTimeout(SubTaskFilter.applyFilters, 200);
		},
		applyFilters: function () {
			
			var postData = SubTaskFilter.getFilters();
			
			var url = SubTaskFilter.url.replace('{id}', SubTaskFilter.projectID);
			
			$.get(url, postData, SubTaskFilter.repaintTable);
		},
		repaintTable: function (html) {
			var id = SubTaskFilter.projectID;
			var table = $('#ProjectItem_' + id + ' div.entry table.general_i').eq(0);
			table.find('tr.tr-data').remove();
			table.find('tr.form').eq(0).after(html);	//проблеманя строчка в проектах
		},
		getProjectIdByFormElement: function (element) {
			var id = SubTaskFilter.projectID = $(element).parents('.post').attr('id').replace("ProjectItem_", "");
			return id;
		},
		showClosedClick : function (evt) {
			SubTaskFilter.projectID = SubTaskFilter.getProjectIdByFormElement(this);
			SubTaskFilter.applyFilters();
		}, 
		fireClick: function(evt) {
			var input = $(this).parent().find('input[name="fire"]').eq(0);
			var checked = parseInt(input.val());
			if(checked) {
				input.val("");
			} else {
				input.val(1);
			}
			
			SubTaskFilter.projectID = SubTaskFilter.getProjectIdByFormElement(this);
			SubTaskFilter.applyFilters();
			return false;
		},
		newClick: function(evt) {
			var input = $(this).parent().find('input[name="new"]').eq(0);
			var checked = parseInt(input.val());
			if(checked) {
				$(this).children('img').attr('src', '/cms/images/new_none.png');
				input.val("");
			} else {
				$(this).children('img').attr('src', '/cms/images/new.png');
				input.val(1);
			}
			SubTaskFilter.projectID = SubTaskFilter.getProjectIdByFormElement(this);
			SubTaskFilter.applyFilters();
			return false;
		},
		commentsClick: function(evt) {
			var input = $(this).parent().find('input[name="comments"]').eq(0);
			var checked = parseInt(input.val());
			if(checked) {
				input.val("");
				$(this).children('img').attr('src', '/cms/images/plus_none.png');
			} else {
				$(this).children('img').attr('src', '/cms/images/plus_blue.png');
				input.val(1);
			}
			SubTaskFilter.projectID = SubTaskFilter.getProjectIdByFormElement(this);
			SubTaskFilter.applyFilters();
			return false;
		}, 
		orderByClick: function (evt) {
			
			var url = $(this).attr('href');
			url = url.substr(url.indexOf("?") + 1);
			url = url.split('&');
			var urlData = {};
			for(key in url) {
				var pair = url[key].split('='); 
				urlData[pair[0]] = pair[1];
			}
			
			SubTaskFilter.projectID = SubTaskFilter.getProjectIdByFormElement(this);
			var data = SubTaskFilter.getFilters();
			for(key in data) {
				urlData[key] = data[key];
			}
			urlData.show_filtered = 0;
			
			var url = SubTaskFilter.url.replace('{id}', SubTaskFilter.projectID);
			
			$.get(url, urlData, function (html) {
				$('#ProjectItem_' + SubTaskFilter.projectID).children('.entry').html(html);
				SubTaskFilter.init(SubTaskFilter.projectID, SubTaskFilter.url);
			});
			return false;
		}, 
		getFilters: function () {
			inputs = SubTaskFilter.getFilterFormElements();
			var postData = {show_filtered: true};
			inputs.each(function () {
				var name = this.getAttribute('name');
				var value = this.value;				
				if(name && name.length && value.length && name != 'check_all') {
					if(this.getAttribute('type') == 'checkbox') {
						if(this.checked) {
							postData[name] = value;
						}
					} else {
						postData[name] = value;
					}
				}
			});
			return postData;
		}
};