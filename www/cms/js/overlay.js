function overlay() {
	that = this;
	that.overlayLayer = $('<div/>', {id: 'overlay'});

	that.init = function() {
		$('body').prepend(this.overlayLayer);
		that.hide();
		that.closeLayer();
	}
	that.show = function() {
		this.overlayLayer.css('display', 'block');
	}
	that.hide = function() {
		this.overlayLayer.hide();
	}
	that.closeLayer = function() {
		that.overlayLayer.bind('click', function() {
			this.hide();
			this.modal = $('#auditors-modal');
			this.modal.hide();
		});
	}
	that.click = function(callback) {
		this.overlayLayer.click(callback);
	}
};