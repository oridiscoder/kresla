function overlay() {
	that = this;
	that.overlayLayer = $('<div id="overlay"></div>');

	that.init = function() {
		$('body').prepend(this.overlayLayer);
		that.show();
		that.hide();
		that.closeLayer();
	}
	that.show = function() {
		this.overlayLayer.css('display', 'block');
	}
	that.hide = function() {
		this.overlayLayer.hide();
	}
	that.closeLayer = function() {
		that.overlayLayer.bind('click', function() {
			that.hide();
			this.modal = $('#auditors-modal');
			this.modal.hide();
		});
	}

};

function AuditorsObj(params) {
	
	that = this;
	this.init = function(urlLink) {
		that.getData = urlLink;
		that.renderData();
		that.selectAllAud();
		that.clearAllAud();
		that.closeModal();
		that.groupCount();
		that.addAuditor();
		$('#submitBtn').bind('click', function() {that.submit(); });
	};

	that.auditorsModal = $('<div id="auditors-modal"><div class="header"><a class="closer" href="#"></a><p>Выберите аудиторов для «Я сделал: закупил 32 ссылки на сток...»</p></div><!-- .header --><div class="left-col"><ul class="project-list"></ul></div><!-- .left-col --><div class="content"><ul class="check-filters"><li><a class="add-all" href="#">выбрать всех</a></li><li><a class="clear-all" href="#">снять выделение</a></li></ul><div class="scroll-pane"><ul class="auditors-list"></ul></div><!-- .aud-wrap --></div><!-- .content --><div class="choose-panel"><p>Вы выбрали аудиторами:</p><div class="aud-choose-list"></div><div class="add-panel"><input type="submit" value="Добавить аудиторов" id="submitBtn"><a class="clear-auditors" href="#">Я передумал, обойдусь без них!</a></div></div></div><!-- #auditors-modal -->');
	that.auditorsModal.css({
		position : 'absolute',
		left : '50%',
		top: '50%',
		marginTop: '-280px',
		marginLeft : '-270px'
	});
	that.projectActive = $('#auditors-modal .project-list li.active');
	that.audChooseList = $('#auditors-modal .aud-choose-list');
	that.clearAllLink = $('#auditors-modal .check-filters .clear-all');
	
	that.audNameArr = [];
	that.audIdArr = [];
	that.testObj = {};

	that.submit = function() {

		that.auditorsModal.hide();
		this.overlay = $('#overlay');
		this.overlay.hide();

		$(".auditors-final-list").html('');

		for (var key in that.testObj) {
			if(that.testObj[key].checked == 1) {
				var li = $('<li/>').appendTo( $(".auditors-final-list") );
			 	var span = $('<span/>', {
					text: that.testObj[key].name,
				}).appendTo(li);
			 	var wid = that.testObj[key].id;
				var input = $('<input type="hidden" name="coworkers[]" value="' + wid + '" id='+wid+'>').appendTo(li);
			}
		}
	};

	that.closeModal  = function() {
		this.closeModal = $('#auditors-modal .closer');
		this.closeModalClear = $('#auditors-modal .clear-auditors');

		this.closeModal.bind('click', function(e){
			e.preventDefault();
			that.auditorsModal.hide();
			this.overlay = $('#overlay');
			this.overlay.hide();
		});
		this.closeModalClear.bind('click', function(e){
			e.preventDefault();
			that.auditorsModal.hide();
			$('.num').html('').css('display','none');
			this.overlay = $('#overlay');
			this.overlay.hide();
			that.auditorsListItem.each(function(){
				$(this).find('input').prop('checked', false);
			});
			for (var key in that.testObj) {
				that.testObj[key].checked = 0;
			}
			that.addAuditor();
		});
	};

	that.groupCount = function() {
		this.groupsArr = [];
		this.listAudId = [];
		that.projectListItem = $('#auditors-modal .project-list li');
		for (var key in that.testObj) {
			if(that.testObj[key].checked == 1) {
				this.groupsArr.push(that.testObj[key].groups);
				this.listAudId.push(that.testObj[key].id);
			}
		}
		$('.num').html('').css('display','none');
		if ( this.groupsArr.length !== 0 ) {
			this.groupsArrStr = this.groupsArr.join(' ');
			var gArr = this.groupsArrStr.split(' ');
			for(i=0;i<gArr.length;i++) {
				var span = $('#count_'+gArr[i]);
				var c = +span.text()+1;
				span.text(c).css('display','inline-block');
			}
		}
	};
	
	that.clearAllAud = function() {
		this.clickLink = $('#auditors-modal .check-filters .clear-all');
		this.clickLink.bind('click', function(e){
			e.preventDefault();
			that.auditorsListItem.each(function(){
				$(this).find('input').prop('checked', false);
			});
			
			for (var key in that.testObj) {
				that.testObj[key].checked = 0;
			}
			that.addAuditor();
			that.groupCount();
		});
	};

	that.addAuditor = function() {
		that.auditorsList = $('.auditors-list');
		that.auditorsListItem = $('.auditors-list li');
		that.addAudWrap = $('#auditors-modal .aud-choose-list');
		that.addAudWrap.html('');
		for (var key in that.testObj) {
			if(that.testObj[key].checked == 1) {
				that.addAudWrap.append(that.testObj[key].name + ', ');
			}
		}
	};

	that.visibleAuditors = function() {
		that.auditorsList = $('#auditors-modal .auditors-list');
		that.auditorsListItem = $('#auditors-modal .auditors-list li');

		that.auditorsListItem.each(function(i){
			var str = $(this).find('input').attr('data-ref');
			if( str.indexOf(that.activeGroup) !== -1) {
				$(this).css('display', 'block');
			}
		});
	};

	that.selectAllAud = function() {
		that.addAllLink = $('#auditors-modal .check-filters .add-all');
		that.addAllLink.bind('click', function(e){
			e.preventDefault();
			
			that.auditorsListItem.each(function(){
				$(this).find('input').prop('checked', true);
			});
			for (var key in that.testObj) {
				that.testObj[key].checked = 1;
			}
			that.addAuditor();
			that.groupCount();
		});
	};

	that.renderData = function() {
		$('body').append(that.auditorsModal);
		that.auditorsModal.css('display', 'block');
		that.projectList = $('#auditors-modal .project-list');
		
		/* уже выбранные аудиторы */
		var ids = [];
		$(".auditors-final-list > li > input").each(function() {
			ids.push(parseInt(this.value));
		});

		$.getJSON( this.getData, {}, function(json){
			for(var i in json.groups) {
				var li = $('<li/>').appendTo(that.projectList);
				var a = $('<a/>', {
					text: json.groups[i].name,
					href: '#'
				}).attr('data-ref', json.groups[i].id).appendTo(li).bind('click', function(e){
					e.preventDefault();
					that.projectList.find('li').removeClass('active');
					$(this).closest('li').addClass('active');
					that.activeGroup = $(this).attr('data-ref');
					that.clickGroupItem();
				});
				if(i == 0) li.addClass('active');
				var num = $('<span id="count_'+json.groups[i].id+'" class="num" />').appendTo(li)
			}
			for(var i in json.peoples) {
				var uid = json.peoples[i].id;
				var li = $('<li/>').appendTo(that.auditorsList);
				var checkBox = $('<input type="checkbox" data-ref="'+json.peoples[i].groups+'" id="'+ uid +'"/>').appendTo(li);				
				var label = $('<label/>', {
					text: json.peoples[i].name,
					'for': json.peoples[i].id
				}).appendTo(li);
				that.testObj[json.peoples[i].id] = { id: json.peoples[i].id, name: json.peoples[i].name, groups: json.peoples[i].groups, checked: 0 };
				
				checkBox.bind('click', function(){
					if (!$(this).prop('checked')) {
						that.testObj[$(this).attr('id')].checked = 0;
						that.addAuditor();
					} else {
						that.testObj[$(this).attr('id')].checked = 1;
						that.addAuditor();
					}
					that.groupCount();
				});
				
				var i = ids.indexOf(uid);
				if(i >= 0) {
					checkBox.attr('checked', 'checked');
					that.testObj[uid].checked = 1;
					that.addAuditor();
				}
			}
			that.visibleAuditors();
		});
	};

	that.clickGroupItem = function() {
		that.auditorsListItem.css('display', 'none');
		that.visibleAuditors();
	};

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	


$(function(){
	function addAuditors(a, b) {
		console.log(a, b);
	}

	var audLink = $('.json-button');
	var overlayObj = new overlay();
	overlayObj.init();


	var Obj = new AuditorsObj(addAuditors);
	
	audLink.bind('click', function(e){
		e.preventDefault();
		if( $('#auditors-modal').length == 0) {
			Obj.init($(this).attr('href'));
			// overlayObj.show();
		} else {
			$('#auditors-modal').show();
		}
	});
});