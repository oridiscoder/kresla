$(document).ready(function(){

	$('.plus_white_link').click(function(){
		$(this).parent().nextAll('.plus_white_container').eq(0).fadeIn(300);  		
  		$(this).parent().addClass('log_link_active');
  	});
	
 	$('.close_btn').click(function(){
  		$('.plus_white_container').fadeOut(300);
  		$('.log_link_active').removeClass('log_link_active');
  	});

 	/* убираем события по клику на чекбокс */
 	$('.plus_white_block input.deativate_event').click(function() {
 		var id = $(this).val();
 		$.post('/backoffice/backoffice/stopevent/', {id:id}, function() {
 			var li = $('#evt' + id);
 			var lis = li.siblings('li');
 			li.hide('slow'); 			
 			
 			if(lis.length == 0) {
 				li.parents('.plus_white_container').prev().hide('.plus_white_container').hide();
 				$('.close_btn').click();
 			}
 			
 			li.remove();
 		});
 	});
 	
 	/* убираем комментарии по клику на чекбокс */
 	$('.plus_white_block input.deativate_comment').click(function() {
 		var checkbox = $(this);
 		var id = checkbox.val();
 		var li = $(checkbox).parents('li').eq(0);
 		var url = li.find('.plus_white_commnets_add > a').attr('href');
 		$.get(url, function() {
 			var lis = li.siblings('li');
 			li.hide('slow'); 			
 			
 			if(lis.length == 0) {
 				li.parents('.plus_white_container').prev().hide('.plus_white_container').hide();
 				$('.close_btn').click();
 			}
 			
 			li.remove();
 		});
 	});
 	
 	$('.plus_white_block a.check_all').click(function() {
 		$(this).parents('.plus_white_block').eq(0).find('input[type="checkbox"]').click();
 	});
	

	var formObiect = $(".form_popup");
     formObiect.focus(function(){
     $(this).parents('.txtarea_block').find('.popup_block').fadeIn(200);
     });
     formObiect.blur(function(){
     $(this).parents('.txtarea_block').find('.popup_block').fadeOut(200);
	 });
	 formObiect.keyup(function(){
     $(this).parents('.txtarea_block').find('.popup_block').fadeOut(200);
	 });
	
	var formObiect = $(".form_popup");
     formObiect.focus(function(){
     $(this).parents('.code_block').find('.popup_block').fadeIn(200);
     });
     formObiect.blur(function(){
     $(this).parents('.code_block').find('.popup_block').fadeOut(200);
	 });
	 formObiect.keyup(function(){
     $(this).parents('.code_block').find('.popup_block').fadeOut(200);
	 });
	 
	 $('#form-filters input[type="text"]').keyup(filter);
	 $('#form-filters select').change(filtersFormSubmit);
	 
	 $('input.datepicker').datepicker({
		 	firstDay: 1, 
			dateFormat:'yy-mm-dd',
			dayNames: ['Вскр', 'Пнд', 'Втр', 'Срд', 'Чтв', 'Птн', 'Сбт'], 
			dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август','Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
	});
	 
	 $('tr.tr-data a.show_hidden_text').click(function () {
		 $(this).siblings('div.hidden_text').toggle();
		 return false;
	 });
	 
	 /* Иногда старые таблицы бывают очень широкими. 
	  * В таких случаях надо подравнять под них их родительский контейнер
	  */
	 var table = $('#ItemListTable');
	 if(table.length) {
		 var width = table.eq(0).width();
		 var div = table.eq(0).parents('div.table').eq(0);
		 var divWidth = div.width();
		 
		 if(divWidth < width) {
			 div.css('width', width + 'px');
			 var header = $('#header');
			 header.css({
				 position: 'relative', 
				 width: (width + 221 ) + 'px'
			 });
			 $('div.but_top').css('right', (divWidth - width + 10) + 'px');
			 $('#footer').css('width', (width + 221 ) + 'px');
			 $('div.button').css('width', width + 'px');
		 }
	 }
	 

	 $('.form textarea[maxlength]').keyup(function(event) {
		 var length = this.value.length;
		 var maxlength = this.getAttribute('maxlength');
		 
		 if(length > maxlength) {
			 this.value = this.value.substr(0, maxlength);
		 }
		 
		 $(this).siblings('div.maxlength_counter').text(length + ' из ' + maxlength);
	 });
	 
	 //спрашиваем на сколько дней ставить паузу для задачи:
	 $('a.pauseplay').click(function() {
		 var a = $(this);
		 var status = a.attr('rel');
		 
		 if(status == 'pause') {
			 
			 var regexp = /card\/(\d+)/;
			 var matches = regexp.exec(a.attr('href'));
			 var taskID = matches[1];
			 var url = '/backoffice/task/card/' + taskID + '/pausedays/';			 
			 var onPauseURL = a.attr('href');
			 
			 $.get(url, function(days)  {
				 var days = prompt("Через сколько дней напомнить о задаче?", days);
				 if(days && days.match(/^\d+$/)) {
					 var url = onPauseURL + '?days=' + days;
					 window.location = url;
				 }
			 });
			 return false;
		 }
	 });
	 
	 RowsSelector.init();
	 TableSorter.init();
	 Dependencies.init();
	 ParentLoader.init();
	 
	 var wysiwygs = $('textarea.wysiwyg');
	 if(wysiwygs.length) {
			 LazyLoad.load('/cms/js/tinymce/tiny_mce.js', function() {
				 /* 
				  * опера не успевает к этому моменту построить объект tinyMCE
				  * Поэтому мы немножко подождем 
				  */
				 window.setTimeout(function() {
					 tinyMCE.init({
						 mode : "specific_textareas",
					     editor_selector : "wysiwyg", 
					     language: 'ru', 
					     height: 300, 
					     width:"99%", 
					     valid_elements: "*[*]", 
					     convert_urls : false, 
					     forced_root_block: false
					}); 
				 }, 500);
			 });
	 }
	 
	 var htmlCode = $('textarea.htmlcode');
	 if(htmlCode.length) {
		 LazyLoad.load([
		     '/cms/js/codemirror/codemirror.js',
		     /*'/cms/js/codemirror/show-hint.js',
		     '/cms/js/codemirror/xml-hint.js',	
		     '/cms/js/codemirror/html-hint.js',	*/
		     '/cms/js/codemirror/xml.js',
		     '/cms/js/codemirror/javascript.js',
		     '/cms/js/codemirror/htmlmixed.js'
		 ], function() {
			 
			 var style = $('<link>').attr({
				 rel:"stylesheet", 
				 type: "text/css", 
				 href: "/cms/css/codemirror/codemirror.css"
			 }).appendTo('head');
			 var style = $('<link>').attr({
				 rel:"stylesheet", 
				 type: "text/css", 
				 href: "/cms/css/codemirror/eclipse.css"
			 }).appendTo('head');
			 
			 function completeAfter(cm, pred) {
			        var cur = cm.getCursor();
			        if (!pred || pred()) setTimeout(function() {
			          if (!cm.state.completionActive)
			            CodeMirror.showHint(cm, CodeMirror.hint.xml, {schemaInfo: tags, completeSingle: false});
			        }, 100);
			        return CodeMirror.Pass;
			      }
			 
			 /* 
			  * опера не успевает к этому моменту построить объект tinyMCE
			  * Поэтому мы немножко подождем 
			  */
			 window.setTimeout(function() {
				 var editors = [];
				 for(var i = 0; i < htmlCode.length; i++) {
					 var cm = CodeMirror.fromTextArea(htmlCode[i], {
						 mode: 'htmlmixed', 
						 lineNumbers: true, 
						 theme: 'eclipse', 
						 lineWrapping: true 
					 });
					 //CmModeFullscreen.init(cm); - не разобрался, как сдлеать, чтоб работало в бофе
					 editors.push(cm);
				 }
				 window.codeEditors = editors;
			 }, 500);
		 });
 }
});

