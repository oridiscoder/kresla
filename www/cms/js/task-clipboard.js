var pasteTo = null;
function pasteHandler(e) {
	
	if(e.clipboardData) {
	    var items = e.clipboardData.items;
	    if(!items){
	        //alert("Image Not found");
	    }
	    for (var i = 0; i < items.length; ++i) {
	    if (items[i].kind === 'file' && items[i].type === 'image/png') {
	        	var blob = items[i].getAsFile(),
	            source = window.webkitURL.createObjectURL(blob);
	        	
	        	//console.log("image pasted");
	        	
	        	//надо сделать 2 канвы - с превьюшкой картинки и с оригиналом
	        	pastedImage = new Image();
	        	pastedImage.onload = function() {
	        		
	        		var width = pastedImage.width;
	        		var height = pastedImage.height;
	        		
	        		//console.log("width: " + width, ", height = " + height);
	        		
	        		var origCanvas = document.createElement("canvas");
	        		origCanvas.setAttribute("width", width);
	        		origCanvas.setAttribute("height", height);	        		
	        		
	        		var origCanvasCtx = origCanvas.getContext( '2d' );
	        		origCanvasCtx.clearRect(0, 0, width,height);
	        		origCanvasCtx.drawImage(pastedImage, 0, 0);

		        	source = origCanvas.toDataURL();
		        	
		        	if(pasteTo) {
		        		 pasteTo.find('input[type="hidden"]').val(source);
		        	}
		        	
		        	var previewCanvas = document.createElement("canvas");
		        	height = 43;
		        	width = 43;
		        	previewCanvas.setAttribute("width", width);
		        	previewCanvas.setAttribute("height", height);	        		
	        		
	        		var previewCanvasCtx = previewCanvas.getContext( '2d' );
	        		previewCanvasCtx.clearRect(0, 0, width,height);
	        		previewCanvasCtx.drawImage(pastedImage, 0, 0, width, height);

		        	source = previewCanvas.toDataURL();
		        	
		        	if(pasteTo) {
		        		pasteTo.find('input[type="file"]').remove();
		        		pasteTo.find('img.paste_preview').remove();
		        		var img = document.createElement('img');
		        		img.setAttribute('src', source);
		        		img.setAttribute('class', 'paste_preview');
		        		pasteTo.children('.file_add_block').prepend(img);
		        	}
	        	};
	        	
	        	pastedImage.src = source;
	        }
	    }
	}
}

window.addEventListener("paste", pasteHandler);