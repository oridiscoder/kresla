var TaskLoader = {
	state: false,
	owner: false, 
	init: function() {
		$('.post span.plus_minus').each(this.bindClickEvent);
	}, 
	bindClickEvent: function() { 
		$(this).click(TaskLoader.spanClick);
	}, 
	
	getURL: function(div, id) {
		rel = div.attr('rel');
		var url = null;
		if(rel == 'project') {
			if(TaskLoader.owner) {
				url = '/backoffice/project/' + id + '/myLastTasks/';
			} else {
				url = '/backoffice/project/' + id + '/lastTasks/';
			}				
		}
		if(rel == 'task') {
			if(TaskLoader.owner) {
				url = '/backoffice/task/card/' + id + '/mysubtasks/';
			} else {
				url = '/backoffice/task/card/' + id + '/subtasks/';
			}	
		}
		return url;
	}, 
	spanClick: function() {
		var value = $(this).parent().find('input[type="checkbox"]').eq(0).attr('id');
		var id = parseInt(value.replace('id', ''));
		var postDiv = $(this).parents('div.post').eq(0);
		
		if(postDiv.hasClass('inactive')) {
			
			url = TaskLoader.getURL(postDiv, id);
			
			if(url) {
				$.get(url, function (html) {
					postDiv.children('.title').eq(0).siblings('div.entry').remove();
					var entry = $('<div>').addClass('entry').html(html);
					postDiv.append(entry);
					entry.slideDown();
					entry.find('span.plus_minus').click(TaskLoader.spanClick);
					postDiv.toggleClass('inactive');
					if(typeof SubTaskFilter != 'undefined') {
						SubTaskFilter.init(id, url);
					}
					
				});
			}
		} else {
			postDiv.children('.title').siblings('div.entry').slideUp();
			postDiv.toggleClass('inactive');
		}
	}, 
	setTaskType: function (type) {
		this.taskType = type;
		return this;
	}
};