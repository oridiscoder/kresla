var CmModeFullscreen = {
		
	isFullScreen: function(cm) {
	  return /\bCodeMirror-fullscreen\b/.test(cm.getWrapperElement().className);
	}, 
	winHeight: function () {
	  return window.innerHeight || (document.documentElement || document.body).clientHeight;
	}, 
	setFullScreen : function (cm, full) {
	  var wrap = cm.getWrapperElement();
	  if (full) {
	    wrap.className += " CodeMirror-fullscreen";
	    wrap.style.height = this.winHeight() + "px";
	    document.documentElement.style.overflow = "hidden";
	  } else {
	    wrap.className = wrap.className.replace(" CodeMirror-fullscreen", "");
	    wrap.style.height = "";
	    document.documentElement.style.overflow = "";
	  }
	  cm.refresh();
	},
	
	init: function (cm) {
		
		CodeMirror.on(window, "resize", function() {
		  var showing = document.body.getElementsByClassName("CodeMirror-fullscreen")[0];
		  if (!showing) return;
		  showing.CodeMirror.getWrapperElement().style.height = CmModeFullscreen.winHeight() + "px";
		});
		
		cm.addKeyMap({
			"F11": function(cm) {
				CmModeFullscreen.setFullScreen(cm, !CmModeFullscreen.isFullScreen(cm));
			},
			"Esc": function(cm) {
			     if (CmModeFullscreen.isFullScreen(cm)) CmModeFullscreen.setFullScreen(cm, false);
			}
		});
	}
}
