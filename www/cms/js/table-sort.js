var TableSorter = {
	offsetTop : 0, 
	heightTop : 0, 
	heightTable : 0, 
	offsetTable: 0, 
	currentRow: null, 
	preventOrderSaving: false, 
	init: function () {
		$('#ItemListTable').sortable({
			items:'tr', 
			axis:'y', 
			handle: 'td.first', 
			start: function(event, ui) {
				TableSorter.fixRowSize(ui);
				TableSorter.initTableSizes();		
			},
			sort: function(event, ui) {
				var currentRowTopOffset = ui.item.offset().top;
				ui.item.removeClass('goingToNextPage');
				
				TableSorter.currentRow = null;
				TableSorter.preventOrderSaving = false;
				
				if(TableSorter.isOutsideTheTable(currentRowTopOffset)) {
					ui.item.css('top', (TableSorter.offsetTable + TableSorter.heightTable + 1) + 'px');	
					ui.item.addClass('goingToNextPage');
					TableSorter.currentRow = ui.item;
					TableSorter.preventOrderSaving = true;
					return false;
				}
			}, 
			stop: function (event, ui) {
				if(!TableSorter.preventOrderSaving) {				
					TableSorter.saveOrder();
				}
			}		
		});

		$('#records_form').droppable({
			drop: function (event, ui) {
				if(TableSorter.isOutsideTheTable(ui.position.top)) {
					TableSorter.moveElementToAnotherPage(event);
				}
			}
		});

		$('#ItemListTable td.first').css('cursor', 'move');
		moveUpInterval = null;
		moveUpElement = null;	
	}, 
	fixRowSize: function (ui) {
		var sizes = TableSorter.getTdSizes();
		var tds = ui.item.find('td');
		for(var i = 0; i < tds.length; i++) {
			tds.eq(i).css('width', sizes[i] + 'px');
		}
	}, 
	initTableSizes: function () {
		TableSorter.offsetTop = $('#ItemListTable tr').eq(1).offset().top;
		TableSorter.heightTop = $('#ItemListTable tr').eq(1).height();
		TableSorter.heightTable = $('#ItemListTable').eq(0).height();
		TableSorter.offsetTable = $('#ItemListTable').eq(0).offset().top;	
	}, 
	getTdSizes: function () {
		var sizes = [];
		var trs = $('#ItemListTable tr');
		var tds = trs.eq(1).find('td');
		for(var i = 0; i < tds.length; i++) {
			sizes[i] = tds.eq(i).width();
		}
		return sizes;
	},
	isOutsideTheTable: function (offsetY) {
		return this.rowCrossedTopLine(offsetY) || this.rowCrossedBottomLine(offsetY);
	}, 
	rowCrossedTopLine: function (offset) {
		return (offset < TableSorter.offsetTop + TableSorter.heightTop / 3);
	}, 
	rowCrossedBottomLine: function (offset) {
		return (offset > (TableSorter.offsetTable + TableSorter.heightTable));
	},
	saveOrder: function () {
		var data = $('#ItemListTable').sortable('serialize', {attribute:'rel'});
		var pageNumber = parseInt($('#PageNumber').text());
		var onPage = parseInt($('#OnPage').text());
		//var pageID = parseInt($('#PageID').text());
		data += '&pageNumber='+pageNumber+'&onPage='+onPage;
		$.post(window.location.pathname + 'sort', data, function () {});
	}, 
	getScrollXY: function () {
		var scrOfX = 0, scrOfY = 0;
		  if( typeof( window.pageYOffset ) == 'number' ) {
		    //Netscape compliant
		    scrOfY = window.pageYOffset;
		    scrOfX = window.pageXOffset;
		  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
		    //DOM compliant
		    scrOfY = document.body.scrollTop;
		    scrOfX = document.body.scrollLeft;
		  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
		    //IE6 standards compliant mode
		    scrOfY = document.documentElement.scrollTop;
		    scrOfX = document.documentElement.scrollLeft;
		  }
		  return [ scrOfX, scrOfY ];
	}, 
	
	moveElementToAnotherPage: function (event) {
		var div = $('#moveUpWindow');
		
		if(!div.length) {
			div = $('<div id="moveUpWindow"></div>')
			.addClass("popupWindow")
			.html($('#MoveToPage').html())
			.appendTo('body');
			$('#moveUpWindow select').change(function (event) {
			
				$('#moveUpWindow').html('<span class="moveTitle">Переносим на страницу №' + this.value + '</span><img src="/img/download.gif">');				
				id = moveUpElement.attr('rel').replace('item_', '');
				
				var pageNumber = parseInt($('#PageNumber').text());
				var onPage = parseInt($('#OnPage').text());
				var objId = parseInt($('#ObjId').text());
				var data = 'id='+id+'&moveToPage='+this.value;
				data += '&pageNumber='+pageNumber+'&onPage='+onPage+'&ajaxpage=move_to_page&objId='+objId;
				var time = new Date();
				time = time.getTime();
								
				
				$.post('/ajax_cms.php?time='+time, data, function(html) {
					document.location.reload();
				});
			});
		} 
				
		
		scrollInfo = TableSorter.getScrollXY();
		var left = (event.clientX+5 + scrollInfo[0]);
		var top = (event.clientY + scrollInfo[1]);
		
		var top = parseInt($(window).height() / 3 + scrollInfo[1]);
		$('#moveUpWindow').css('top', top + 'px');
		
		var pageCount = $('#moveUpWindow select option').length;

		if(pageCount > 1) {
			div.show();
			if(moveUpInterval) {window.clearTimeout(moveUpInterval);}
			moveUpInterval = window.setTimeout(function () {
				$('#moveUpWindow').hide();
			}, 7000);
		}
	  	
		
		return false;
	}
};


