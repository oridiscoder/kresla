RowsSelector.loadFieldEditForm =  function (postData, fieldName) {
	var url = window.location.pathname + "?tbl_action=field_control";
	postData += '&field_name=' + fieldName;
	$.post(url, postData, function (html) {
		$('#EditSingleFieldForm').html(html);
		RowsSelector.showEditForm();
	});
};

RowsSelector.deleteItems =  function (postData) {		
	var url = window.location.pathname + "?tbl_action=delete";
	$.post(url, postData, function () {
		window.location.reload();
	});	
};

RowsSelector.moveItemsToRecycleBin =  function (postData) {
	var url = window.location.pathname + "?tbl_action=to_recycle_bin";
	$.post(url, postData, function () {
		window.location.reload();
	});		
};

RowsSelector.restoreItemsFromRecycleBin =  function (postData) {
	var url = window.location.pathname + "?tbl_action=restore_from_recycle_bin";
	$.post(url, postData, function () {
		window.location.reload();
	});
};