var Dependencies = {
		container : null, 
		init: function () {
			this.container = $('#Dependencies');
			if(this.container.length) {
				 $('a.dependencies').click(this.showList);
				 this.container.find('div.close a').click(this.closeWindow);
				 this.initBreadcrumbsDependencies();
			} else {
				if(window.console) {
					console.log('Container #Dependencies not found on this page');
				}
			}
		},
		/**
		 * Сгенерировать и показать окошко с выбором
		 * зависимых таблиц при нажатии на кнопку "Зависимые" у каждой строки
		 * @param event
		 * @returns {Boolean}
		 */
		showList: function (a) {
			 var a = a ? $(a) : $(this); 
			 var offsetTop = a.offset().top - 89;
			 var offsetLeft = a.offset().left;
			 var plateWidth = Dependencies.container.width();
			 var url = a.attr('href');
			 
			 Dependencies.startAjax(a);
			 
			 $.get(url, function (response) {
				Dependencies.stopAjax(a);
				//try {
					Dependencies.draw(
							eval('(' + response + ')')
					);
					Dependencies.container.css({
						 left: (offsetLeft - plateWidth - 15) + 'px',
						 top: offsetTop + 'px'
					}).show();
				/*} catch (ex) {
					alert("Произошла ошибка получения данных");
					if(window.console) {
						console.log(response.substr(0, 100));
					}
				}*/
				return false;
			 });
			 
			 return false;
			 
			// Dependencies.rowID = a.parents('tr').eq(0).attr('rel').replace('item_', '');
			 
			 //Dependencies.pathName = window.location.pathname;
			 
			 //$('#Dependencies li').each(Dependencies.generateLink);
			 
		},
		startAjax: function (a) {
			Dependencies.imgSrc = a.children('img').attr('src');
			a.children('img').attr('src', '/cms/images/ajax-loader.gif');
		}, 
		stopAjax: function (a) {
			a.children('img').attr('src', Dependencies.imgSrc);
		},
		draw: function (data) {
			this.container.find('ul').children().remove();
			for(key in data) {
				var li = $('<li>');
				$(document.createElement('a')).attr('href', data[key].url).text(data[key].title).appendTo(li);
				
				if(data[key]['_fields']) {
					var sUL = $('<ul>');
					for(k in data[key]['_fields']) {
						var f = data[key]['_fields'][k];
						
						var sLI = $('<li>');
						var sA = $('<a>').attr('href', f.url).text(f.title).appendTo(sLI);
						sUL.append(sLI);
					}
					li.append(sUL);
				}
				this.container.find('ul').append(li);
			}
		},
		closeWindow: function () {
			Dependencies.container.hide();
			return false;
		}, 
		/**
		 * Инициализируем всплывающие окошки с выбором зависимых таблиц в хлебных крошках
		 */
		initBreadcrumbsDependencies: function () {
			$('ul.breadcrumbs li.dependency > a').click(function () {
				$('ul.breadcrumbs li.dependency > ul').hide();
				var visibleFlag = $(this).attr('rel'); 
				if(visibleFlag != 'visible') {
					$(this).parent().children('ul').show();
					$(this).attr('rel', 'visible'); 
				} else {
					$(this).parent().children('ul').hide();
					$(this).attr('rel', 'hidden');
				}
				
				return false;
			});
			$('body').click(function () {
				$('ul.breadcrumbs li.dependency > ul').hide();
				$('ul.breadcrumbs li.dependency > a').attr('rel', 'hidden');
			});
		}
		
};