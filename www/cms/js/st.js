$(function(){
        new Scroll({ horizontal:true,start:true });
        //$('#ch').removeAttr('checked');
        $('#ch').change(function(){
            $('.nr1').toggleClass('active');
        });
        $('.close-hint').click(function(){
            $(this).parent().hide();
            $(this).parent().parent().removeClass('active');
        });
        $('.events-item > i').click(function(){
            $(this).parents('td').siblings().find('.events-item.active').removeClass('active');
            $(this).parents('td').siblings().find('.events-item-hint').hide();
            $(this).parent().toggleClass('active');
            $(this).parent().parent().find('.events-item-hint').toggle();
            /*$(document).live('click',function(event){
                if ($(event.target).parents('.events-item').length != 1) {
                    $('.events-item').removeClass('active');
                    $('.events-item-hint').hide();
                }
            });*/
        });
        function initPos(){
            $('.events-item.active').each(function(indx, element){
                var pos = $(element).offset().left;
                var allW = $(document).outerWidth();
                var hintW = $('.events-item-hint').width()+80;
                var delta = allW-pos;
                if(delta<hintW){
                    $(element).addClass('pos-right');
                }else {
                    $(element).removeClass('pos-right');
                }
            })
        };
        $('.scroll-wrap').scroll(function(){
            initPos();
        });
        $('.events-item').hover(function(){
            var pos = $(this).offset().left;
            var allW = $(document).outerWidth();
            var hintW = $('.events-item-hint',this).width()+80;
            var delta = allW-pos;
            if(delta<hintW){
                $(this).addClass('pos-right');
            }
        });
        var allWtrack = $('.table-cente-wrap').width();
        $('.scroll-track').width(allWtrack);
        $(window).resize(function(){
            var allWtrack = $('.table-cente-wrap').width();
            $('.scroll-track').width(allWtrack);
        });
        
        function calculateDays() {
        	var inputs = $('input.date_selector');
        	var start = inputs.eq(0).val();
        	var end = inputs.eq(1).val();
        	var days = Math.floor((end - start) / (3600 * 24));
        	$('#DaysInput').text(days);
        }
        
        function initDates() {
        	$('input.date_selector').datepicker({
        		onSelect: function(dateText, data) {
        			var name  = data.input.eq(0).attr('name');			
        			if(name == 'date[start]') {
        				var index = 0;
        			}
        			if(name == 'date[end]') {
        				var index = 1;
        			}
        			var span = $('span.date_select').eq(index);
        			var img = span.children('img').eq(0);
        			span.text(dateText);
        			span.append(img);
        			var date = new Date(data.selectedYear, data.selectedMonth, data.selectedDay, 0, 0, 0, 0);
        			data.input.eq(0).val(date.getTime() / 1000);
        			calculateDays();
        			initDates();
        		}, 
        		dateFormat: 'dd MM yy', 
        		dayNames: ['Вскр', 'Пнд', 'Втр', 'Срд', 'Чтв', 'Птн', 'Сбт'], 
        		dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        		dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август','Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'], 
        		maxDate: new Date()
        	});
        	$('span.date_select > img').click(function() {
        		var rel = $(this).parent().attr("rel");
        		$('input[name="date[' + rel + ']"]').eq(0).datepicker('show');
        	});
        }
        initDates();
});

function submitShow() {
	$('#ExportInput').val(0);
	$('#QForm').submit();
}

function exportPositions() {
	$('#ExportInput').val('positions');
	$('#QForm').submit();
}

function exportQueries() {
	$('#ExportInput').val('queries');
	$('#QForm').submit();
} 