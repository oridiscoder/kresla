$.fn.windowCenter = function(){
	var top = ($(window).height() - this.outerHeight()) / 2;
	var left = ($(window).width() - this.outerWidth()) / 2;

	if (top < 20)
		top = 20;

	if (left < 20)
		left = 20;

	this.css({
		'position': 'absolute',
		'top': $(window).scrollTop()+top,
		'left': left
	});
	return this;
};

$(function(){
    if($('input[name=table_filter]').is(":checked")) {
    	$('input[name="table_material[]"]').parents().closest('tr').show();
    	$('input[name="table_types[]"]').parents().closest('tr').show();
    	$('input[name="table_colors[]"]').parents().closest('tr').show();
    }else{
    	$('input[name="table_material[]"]').parents().closest('tr').hide();
    	$('input[name="table_types[]"]').parents().closest('tr').hide();
    	$('input[name="table_colors[]"]').parents().closest('tr').hide();
    }	
    $('td > input[name=table_filter]').change(function() {
        if($(this).is(":checked")) {
        	$('input[name="table_material[]"]').parents().closest('tr').show();
        	$('input[name="table_types[]"]').parents().closest('tr').show();
        	$('input[name="table_colors[]"]').parents().closest('tr').show();
        }else{
        	$('input[name="table_material[]"]').parents().closest('tr').hide();
        	$('input[name="table_types[]"]').parents().closest('tr').hide();
        	$('input[name="table_colors[]"]').parents().closest('tr').hide();
        }
        
    });
    	
	var doneButton = $('.done-button'),
		doneTaskWindow = $('.done-task-window'),
		doneTaskCloser = doneTaskWindow.find('.closer-modal'),
		overlay = $('#overlay'),
		txtAr = $('#DoneTaskForm > textarea'),
		iHd = $('#IhadDone');

		txtAr.bind('keyup', function(e) {
			var v = $(this).val();
			if(v.length>25) {
				v = v.substr(0,23) + "...";
			}
			if($.trim(v).length == 0 ) {
				v = 'что-то, сам не знаю что';
			}
			iHd.html("&laquo;Я сделал: "+v+"&raquo;");			
		});
		
		doneButton.click(function(e){
			e.preventDefault();
			$('body').css('overflow', 'hidden');
			overlay.fadeIn(300);
			doneTaskWindow.fadeIn(500);
			doneTaskWindow.find('.description').focus();
		});
		doneTaskCloser.click(function(e){
			e.preventDefault();
			$('.done-task-window .auditors-final-list').html('');
			$('body').css('overflow', 'auto');
			$('#auditors-modal').hide();
			overlay.fadeOut(500).hide();
			doneTaskWindow.fadeOut(300);
		});
		overlay.click(function(e){
			e.preventDefault();
			$('body').css('overflow', 'auto');
			$('#auditors-modal').hide();
			overlay.fadeOut(500);
			doneTaskWindow.fadeOut(300);
		});

	var	eventLink = $('.event-link'),
		jsonHref = '/bof-json/json.json',
		eventsModal = $('<div id="events-modal-window"><div class="modal-header"><a class="closer-modal" href="#"><span>Ага, заценил</span><span class="table-icons closer"></span></a><p class="title">События </p></div><ul class="events-list"></ul></div><!-- #events-modal-window -->'),
		eventsModalCloser = eventsModal.find('.closer-modal'),
		eventsContainer = eventsModal.find('.events-list'); /// Контейнер для событий

	eventLink.click(function(e){
		e.preventDefault();
		$(this).closest('td').append(eventsModal);
		eventsModalCloser.click(function(e){
			e.preventDefault();
			$(this).closest('td').find(eventLink).removeClass('active');
			$(this).closest('#events-modal-window').fadeOut(300, function(){
				$(this).closest('#events-modal-window').remove();	
			});
		});
		

		$(eventLink).not(this).removeClass('active');
		$(this).toggleClass('active');
		if( $(this).is('.active') ) {
			$(this).next(eventsModal).stop().fadeIn(500);
		} else {
			$(this).next(eventsModal).stop().fadeOut(500);
		}
	

		$.getJSON( jsonHref, {}, function(json){
			eventsContainer.html('');
			for(var i in json) {

				var li = $('<li/>').appendTo(eventsContainer);
				var a = $('<a/>', {
					href: json[i].url,
					text: json[i].title
				}).appendTo(li);

				if(json[i].bof==1) {
					li.prepend('<span>BOF: </span>').addClass('bof');
				}
			}
			console.log(json[i].url)
		});
	});
});