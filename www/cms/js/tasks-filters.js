TaskFilter = {
		init: function() {
			if(!this.base) {
				this.base = $('table.general').eq(0);
			}
			if(!this.url) {
				this.url = window.location.pathname;
			}
			this.base.find('tr.form input[type="text"]').keyup(this.filter);
			this.base.find('tr.form select').change(this.filtersFormSubmit);
			this.base.find('tr.form input[type="checkbox"]').click(this.filtersFormSubmit);
			this.base.find('tr.form a.fire').click(this.fireClick);
			this.base.find('tr.form a.new').click(this.newClick);
			this.base.find('tr.form a.plus_none').click(this.commentsClick);
		}, 
		filter: function(event) {
			if(event.which == 13) {
				TaskFilter.filtersFormSubmit();
			} else {
				if(window.filterTimeout) {
					window.clearTimeout(window.filterTimeout);
				}
				window.filterTimeout = window.setTimeout(TaskFilter.applyFilters, 200);
			}
		}, 
		fireClick: function() {
			var fireInput = TaskFilter.base.find('tr.form input[name="fire"]').eq(0);
			var checked = parseInt(fireInput.val());
			if(checked) {
				fireInput.val("");
			} else {
				fireInput.val(1);
			}
			TaskFilter.filtersFormSubmit();
			return false;
		},
		newClick: function() {
			var fireInput = TaskFilter.base.find('tr.form input[name="new"]').eq(0);
			var checked = parseInt(fireInput.val());
			if(checked) {
				fireInput.val("");
			} else {
				fireInput.val(1);
			}
			TaskFilter.filtersFormSubmit();
			return false;
		},
		commentsClick: function() {
			var fireInput = TaskFilter.base.find('tr.form input[name="comments"]').eq(0);
			var checked = parseInt(fireInput.val());
			if(checked) {
				fireInput.val("");
			} else {
				fireInput.val(1);
			}
			TaskFilter.filtersFormSubmit();
			return false;
		},
		applyFilters: function () {
			inputs = TaskFilter.getFilterFormElements();
			var postData = {};
			inputs.each(function () {
				var name = this.getAttribute('name');
				var value = this.value;
				var type = this.getAttribute('type');
				
				if(type == 'checkbox' && !$(this).is(':checked')) {
					return;
				}
				
				if(name && name.length && value.length && name != 'check_all') {
					postData[name] = value;
				}
			});	
			
			var url = window.location.pathname;
			
			$.get(url, postData, TaskFilter.repaintTable);
		},

		getFilterFormElements: function () {
			return TaskFilter.base.find('tr.form input, tr.form select');
		},

		repaintTable: function (html) {
			TaskFilter.base.find('tr.tr-data').remove();
			TaskFilter.base.find('tr.form').eq(0).after(html);	//проблеманя строчка в проектах
		},

		filtersFormSubmit: function () {
			var form = $('#FiltersForm');
			if(!form.length) {
				form = $('<form>').attr({
					id:'FiltersForm', 
					action:TaskFilter.url, 
					method: 'get'
				}).appendTo('body').hide();
			}
			form.children().remove();
			elements = TaskFilter.getFilterFormElements();
			for(var i = 0; i < elements.length; i++) {				
				var elementName = elements[i].getAttribute('name');
				var elementValue = $(elements[i]).val();
				var type = elements[i].getAttribute('type');
				
				if(type == 'checkbox' && !$(elements[i]).is(':checked')) {
					continue;
				}
				
				if(elementValue != '' && elementName != 'check_all') {
					var element = $('<input>').attr({
						type: 'hidden', 
						name:  elementName,
						value: elementValue
					}).appendTo(form);
				}
			}
			form.submit();
		}
};