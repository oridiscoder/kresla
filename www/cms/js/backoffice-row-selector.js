var RowSelector = {
	url: '/backoffice/project/groupOperation/', 
	init: function() {
		this.bindSelectAll();
		$('input.itemCheckbox').change(this.toggleSelector);
		$('#GroupSelectSelector').hide();
		$('#GroupSelectSelector select').change(function() {
			var items = $('input.itemCheckbox:checked');
			var postData = {
				action:'get',
				operationName:$(this).val(), 
				items: RowSelector.extractIdFromCheckboxes(items)
			};
			if(postData.operationName.length > 0) {
				$.get(RowSelector.url, postData, function(html) {
					$('#GroupSelectFormContainer').html(html);
					$('#GroupSelectFormContainer').show();
				});
			}
		});
	}, 
	bindSelectAll: function() {
		$('#CheckAll').change(function() {
			if($(this).is(':checked')) {
				$('input.itemCheckbox').attr('checked', 'checked');
			} else {
				$('input.itemCheckbox').removeAttr('checked');
			}
			RowSelector.toggleSelector();
		});
	}, 
	toggleSelector : function () {
		var checkedInputs = $('input.itemCheckbox:checked');
		if(checkedInputs.length > 0) {
			$('#GroupSelectSelector').show();
		} else {
			$('#GroupSelectSelector').hide();
		}
	}, 
	extractIdFromCheckboxes: function (checkboxes) {
		var ids = new Array();
		for(var i = 0; i < checkboxes.length; i++) {
			ids.push(parseInt(checkboxes[i].value));
		}
		return ids;
	} 
	
};