var RowsSelector = {
		
	url: null, 
		
	init: function () {
		$('#ItemListTable input.itemCheckbox').removeAttr('checked');
		$('#ItemListTable input.itemCheckbox').change(this.selectRow);	
		this.initCheckAll();
		this.initOperationsSelect();
	}, 

	selectRow: function (event) {
		
		var inputs = $('#ItemListTable input.itemCheckbox:checked');
		if($(this).is(':checked')) {
			RowsSelector.highlightRow(this.parentNode.parentNode);
		} else {
			RowsSelector.revokeHighlightningRow(this.parentNode.parentNode);
		}
		
		if(inputs.length) {
			RowsSelector.showOperationsSelect();
		} else {
			RowsSelector.hideOperationsSelect();
		}
	}, 
	
	showOperationsSelect: function () {
		$('#GroupSelectContainer').show();
	}, 
	hideOperationsSelect: function () {
		$('#GroupSelectContainer').hide();
		$('#EditSingleFieldForm').hide();
	}, 
	highlightRow:function (rowDomElement) {
		if(rowDomElement) {
			$(rowDomElement).css('background-color', '#eaeaea');
		} else {
			$('#ItemListTable tr.tr-data').css('background-color', '#eaeaea');
		}
	},
	revokeHighlightningRow: function (rowDomElement) {
		if(rowDomElement) {
			$(rowDomElement).css('background-color', '#FFF');
		} else {
			$('#ItemListTable tr.tr-data').css('background-color', '#FFF');
		}
	},
	initCheckAll: function () {
		$('.check_all_rows').change(function (event) {
			if($(this).is(':checked')) {
				$('#ItemListTable input.itemCheckbox').each(function () {
					$(this).attr('checked', 'checked');
				});
				$('.check_all_rows').attr('checked', 'checked');
				RowsSelector.showOperationsSelect();
				RowsSelector.highlightRow();
			} else {
				$('#ItemListTable input.itemCheckbox').each(function () {
					$(this).removeAttr('checked');				
				});
				$('.check_all_rows').removeAttr('checked');
				RowsSelector.hideOperationsSelect();
				RowsSelector.revokeHighlightningRow();
			}
		});
	},
	initOperationsSelect: function () {
		$('#GroupSelectContainer select').change(function(event) {
			var inputs = $('#ItemListTable input.itemCheckbox:checked').serializeArray();
			if(inputs.length) {
				
				var postData = $.param(inputs);
				var url = this.value;				
				var option = $(this).find('option:selected');
				var isAjaxRequest = option.attr('data-ajax');
				var confirmMessage = option.attr('data-confirm');
				var operationName = option.attr('data-name');
				postData += '&name=' + operationName;
				
				if(confirmMessage && !confirm(confirmMessage)) {
					option.removeAttr("selected");
					this.value = "";
					return false;
				}
				
				if(isAjaxRequest) {
					$.post(url, postData, function (html) {
						RowsSelector.showEditForm(html);
					});
				} else {					
					var ids = new Array();
					for(var i = 0; i < inputs.length; i++) {
						ids.push(inputs[i].value);
					}
					url = url + '?ids=' + ids.join(',');
					window.location = url;
					return true;
				}
			}
		});
	},
	showEditForm: function (html) {
		if(html != undefined) {
			$('#EditSingleFieldForm').html(html);
		}
		$('#EditSingleFieldForm').show();
	}, 
	hideEditForm: function () {
		$('#EditSingleFieldForm').hide();
	}
};
