function recycleBinConfirm(id) {
	return confirm('Действительно хотите поместить запись id:' + id + ' в мусорную корзину?');	
}
//--------------------------- ФИЛЬТРЫ ------------------------
function filter(event) {	
	if(event.which == 13) {
		filtersFormSubmit();
	} else {
		if(window.filterTimeout) {
			window.clearTimeout(window.filterTimeout);
		}
		/* костыль для всех сайтов рунета */
		if(window.location.pathname != '/section/12/151/') {
			window.filterTimeout = window.setTimeout(applyFilters, 200);
		}		
	}
}

function applyFilters() {
	inputs = getFilterFormElements();
	var postData = {};
	inputs.each(function () {
		var name = this.getAttribute('name');
		var value = this.value;
		if(name && name.length && value.length && name != 'check_all') {
			postData[name] = value;
		}
	});	
	
	var url = window.location.pathname;
	
	$.get(url, postData, repaintTable);
}

function getFilterFormElements() {
	return $('#form-filters input, #form-filters select');
}

function repaintTable(html) {
	$('#ItemListTable tr.tr-data').remove();
	$('#form-filters').after(html);
	Dependencies.init();
}

function filtersFormSubmit() {
	var form = $('#FiltersForm');
	if(!form.length) {
		form = $('<form>').attr({
			id:'FiltersForm', 
			action:window.location.pathname, 
			method: 'get'
		}).appendTo('body').hide();
	}
	form.children().remove();
	elements = getFilterFormElements();
	for(var i = 0; i < elements.length; i++) {
		var elementName = elements[i].getAttribute('name');
		var elementValue = $(elements[i]).val();
		
		if(elementValue != '' && elementName != 'check_all') {
			var element = $('<input>').attr({
				type: 'hidden', 
				name:  elementName,
				value: elementValue
			}).appendTo(form);
		}
	}
	form.submit();
}

function clone(obj){
    var temp = new obj.constructor(); 
    for(var key in obj) {
        temp[key] = obj[key];//clone(obj[key]);
    }
    return temp;
}

function runScript(obj) {
	var tr = $(obj).parents('.tr-data').eq(0);
	var href = obj.getAttribute("href");
	if(tr.length) {
		$.get(href, function(html) {
			$('tr.tr-data').remove();
			$('#form-filters').after(html);
		});
	}
}

function isLocalStorageAvailable() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function loadMDTableVariants(select) {
	
	select = $(select);
	
	var fieldName = select.attr('name').replace('__table', '');
	
	var data = {
		request_field_via_ajax : true, 
		field_name: fieldName, 
		table_id: select.val()
	};
	
	$.get(window.location.pathname, data, function(html) {
		try {
			data = eval('(' + html + ')');
			select.siblings('select').remove();			
			var newSelect = $('<select>').attr('name', fieldName);
			
			for(var i = 0; i < data.length; i++) {
				$('<option>').attr('value', data[i].id).text(data[i].name).appendTo(newSelect);
			}
			
			select.after(newSelect);
			
		} catch(Error) {
			select.siblings('select').remove();	
		} 
	});
}

function simple_tooltip(target_items, name) {
	$(target_items).each(function(i){
	   $("body").append("<div class='"+name+"' id='"+name+i+"'><p>"+$(this).attr('title')+"</p></div>");
		var my_tooltip = $("#"+name+i);

		$(this).removeAttr("title").mouseover(function(){
		  my_tooltip.css({opacity:0.8, display:"none"}).fadeIn(40);
		}).mousemove(function(kmouse){
		  my_tooltip.css({left:kmouse.pageX+15, top:kmouse.pageY+15});
		}).mouseout(function(){
		  my_tooltip.fadeOut(40);
		});
	});
}

function datePad(n){return n<10 ? '0'+n : n}
function dateSetNow(button) {
	var siblings = $(button).siblings();
	var date = new Date();
	
	$(siblings[0]).val(date.getFullYear() + '-' + datePad(date.getMonth() + 1) + '-' + datePad(date.getDate()));
	
	if(siblings.length > 1) {
		$(siblings[1]).val(datePad(date.getHours()));
		$(siblings[2]).val(datePad(date.getMinutes()));
		$(siblings[3]).val(datePad(date.getSeconds()));
	}
}

$(document).ready(function(){
	simple_tooltip("div nobr a","tooltip");
});
