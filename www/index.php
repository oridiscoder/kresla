<?php
// phpinfo();
date_default_timezone_set('Europe/Moscow');

define('APPLICATION_CHARSET', 'utf-8');

define('APPLICATION_PATH', dirname(__FILE__).'/../cms/application');

if($_COOKIE['tester'] && $_COOKIE['tester']=='magicmagic')
	define('APPLICATION_ENV', 'development');
else
	define('APPLICATION_ENV', 'production');

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH),
		realpath(APPLICATION_PATH . '/../library'),
		realpath(APPLICATION_PATH . '/../../'),
		get_include_path(),
)));

if(APPLICATION_ENV == 'development') {
	ini_set("display_errors", 1);
	error_reporting(E_ALL);
}

require_once 'Bof/BuildTimer.php';
BuildTimer::start();

require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();

require_once APPLICATION_PATH . '/Application.php';
Application::setEnvironment(APPLICATION_ENV);
Application::setConfigs(array(
		APPLICATION_PATH . '/configs/application.ini',
		APPLICATION_PATH . '/configs/database.ini',
		APPLICATION_PATH . '/configs/routes.ini'
));
Application::run();

$req = Application::getAppRequest();
if(APPLICATION_ENV == 'development' && !$req->isXmlHttpRequest()) {
	echo "<hr />".BuildTimer::getWorkTime();
	echo '<pre>'.BuildTimer::getIntervalsPrintable().'</pre>';
}
