<?php
function smarty_modifier_is_image($ext)
 {
     $ext = strtolower(str_replace('.', '', $ext));
     
     
     $imagesExtensions = array('jpg', 'jpeg', 'gif', 'bmp', 'png');
     
     return in_array($ext, $imagesExtensions);
 }
?>