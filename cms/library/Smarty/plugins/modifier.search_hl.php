<?php

function smarty_modifier_search_hl($text, $query) {

	$str = "";
	if($query) {
		$text = strip_tags($text);
		$pos = 0;
		
		$width = 10;	//������ ����������� ������
		$textLength = strlen($text);
		
		/* 
		 * ���� �� ���� ������� � ������� � ������ ���������, 
		 * ����� ������������ preg_replace
		 */
		if($textLength > 200) {
			while(($pos = stripos($text, $query, $pos)) !== false) {
				$start = $pos - $width;
				if($start < 0) {
					$start = 0;
				}
				
				$stop = $pos + $width;
				if($stop > $textLength - 1) {
					$stop = $textLength - 1;
				}
				
				$str .= substr($text, $start, $stop);
				
				$pos ++;
			}
		} else {
			$str = $text;
		}
		
		if(empty($str)) {
			$str = substr($text, 0, 2 * $width + strlen($query));
		}
		
		$str = str_ireplace($query, '<span class="hl">'.$query.'</span>', $str);

	}
	return $str;
}