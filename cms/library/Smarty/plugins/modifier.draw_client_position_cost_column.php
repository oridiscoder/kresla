<?php
function smarty_modifier_draw_client_position_cost_column($query, $value, $searcher)
{
   $today = $value;
   $tomorrow = $value + 3600 * 24;
   
   $todayMonth = date('n', $today);
   $tomorrowMonth = date('n', $tomorrow);
   
   $todayDay = date('d', $today);
   $todayDayName = date('D', $today);
   
   $months = array(
   		'���', '���', '����','���', '���', '����', '����', '���', '���', '���', '����', '���'
   	);
   
   switch ($todayDayName) {
   	case 'Sun':
   	case 'Sat':
   		$result = '<td class="weekend">{position}</td>';
   		break;
   	default:
   		$result = '<td>{position}</td>';
   }
   
   $todayPosition = isset($query['_positions'][date('Y-m-d', $today)]) ? $query['_positions'][date('Y-m-d', $today)][$searcher] : 0;
   $yesterdayPosition = isset($query['_positions'][date('Y-m-d', $today - 24 * 3600)]) ? $query['_positions'][date('Y-m-d', $today - 24 * 3600)][$searcher] : 0;
   
   if(empty($yesterdayPosition)) {
   	/*
   	 * �� �����, ����� ����� ���� �������.
   	 */
   	if(!empty($todayPosition)) {
   		//���� - �������, ������� �� ���������
   		$class = 'green';
   		$diff = '-';
   	} else {
   		$class = 'blue';
   		$diff  ='-';
   	}
   } else {
   	//��������� ������� ��������.
   	if(!empty($todayPosition)) {
   		//����������� ������� ���� ��������. �������, � ����� ������� ��� ����������.
   		if($todayPosition > $yesterdayPosition) {
   			$class = 'red';
   			$diff = '-'.($todayPosition - $yesterdayPosition);
   		} elseif ($todayPosition < $yesterdayPosition) {
   			$class = 'green';
   			$diff = '+'.($yesterdayPosition - $todayPosition);
   		} else {
   			$class = 'blue';
   			$diff = 0;
   		}
   	} else {
   		//�������� �� ������. ����� - ������, ���� �������, �� ������� �� ���������
   		$class = 'red';
   		$diff = '-';
   	}
   }
   
   $todayCost = 0;
   $s = substr($searcher, 0, 1);
   $tops = array(
   	1 => $query[$s.'_1'],
   	3 => $query[$s.'_3'],
   	5 => $query[$s.'_5'],
   	10 => $query[$s.'_10'],
   	15 => isset($query[$s.'_15']) ? $query[$s.'_15'] : 0,
   	20 => isset($query[$s.'_20']) ? $query[$s.'_15'] : 0
   );
   if($todayPosition && $todayPosition <= 20) {
   	  foreach ($tops as $top => $cost) {
   	  	if($todayPosition <= $top) {
   	  		$todayCost = $cost;
   	  		break;
   	  	}
   	  }
   }
   $todayCost = round(100 * $todayCost) / 100;
   
   $pHTML = '<div class="'.$class.'"><span class="t1">'.$todayPosition.'</span>-&#8212;<span class="t2">'.$todayCost.'</span></div>';
   $result = str_replace("{position}", $pHTML, $result);
   
   
   if($todayMonth != $tomorrowMonth) {
   		$result .= '<td class="month_end"></td>';
   }
   
   echo $result;
}

?>