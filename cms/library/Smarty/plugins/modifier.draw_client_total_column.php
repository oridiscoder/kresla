<?php
function smarty_modifier_draw_client_total_column($value, $queries, $searcher)
{
   $today = $value;
   $tomorrow = $value + 3600 * 24;
   
   $todayMonth = date('n', $today);
   $tomorrowMonth = date('n', $tomorrow);
   
   $todayDay = date('d', $today);
   $todayDayName = date('D', $today);
   
   $months = array(
   		'���', '���', '����','���', '���', '����', '����', '���', '���', '���', '����', '���'
   	);
   
   $totalQueries = count($queries);
   $totalInTop10 = 0;
   $todayDate = date('Y-m-d', $today);
   foreach($queries as $query) {
   	if(isset($query['_positions'][$todayDate])) {
   		$position = $query['_positions'][$todayDate][$searcher];
   		if($position && $position <= 10) {
   			$totalInTop10 ++;
   		}
   	}
   }
   $visibility = round(100 * $totalInTop10 / $totalQueries);
   
   switch ($todayDayName) {
   	case 'Sun':
   	case 'Sat':
   		$result = '<td class="weekend"><div rel="'.$today.'">'.$visibility.'</div></td>';
   		break;
   	default:
   		$result = '<td><div rel="'.$today.'">'.$visibility.'</div></td>';
   }
   
   if($todayMonth != $tomorrowMonth) {
   		$result .= '<td class="month_end"></td>';
   }
   
   echo $result;
}

?>