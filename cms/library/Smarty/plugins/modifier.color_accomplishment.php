<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 *
 * @param string  $string      field content
 * @param DbField  $field      field object
 * @return string
 */
function smarty_modifier_color_accomplishment($value)
{
    $class = null;
    if($value > 0) {
    	$class = "diff_green";
    } elseif($value < 0) {
    	$class = "diff_red";
    }
    
    if($class) {
    	echo '<span class="'.$class.'">'.(($value > 0) ? '+'.$value : $value ).'</span>';
    } else {
    	echo $value;
    }
}

?>