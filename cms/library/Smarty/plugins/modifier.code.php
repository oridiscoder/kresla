<?php

function my_htmlspecialchars($matches) {
	return '<code>'.htmlspecialchars($matches[1]).'</code>';
}

function smarty_modifier_code($text) {
	$text =  preg_replace_callback('#<code>(.*?)</code>#ism', 'my_htmlspecialchars', $text);
	return $text;
}