<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 *
 * @param string  $string      field content
 * @param DbField  $field      field object
 * @return string
 */
function smarty_modifier_render_column($columnValue, $field)
{
    echo $columnValue;
}

?>