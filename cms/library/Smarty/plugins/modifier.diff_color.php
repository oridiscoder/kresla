<?php
function smarty_modifier_diff_color($value)
{
   if($value > 0) {
   		echo '<span class="diff_green">'.$value.'</span>';
   } elseif($value < 0) {
   		echo '<span class="diff_red">'.$value.'</span>';
   } else {
   		echo '<span class="diff">'.$value.'</span>';
   }
}

?>