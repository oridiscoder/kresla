<?php
/**
 * ����������� nl2br. �������� ����� - �� ��������� br ������ ������� (�� ������ ���� td �����)
 * @param string $string - �������� ������
 * @return string
 */
function smarty_modifier_my_nl2br($string) {
	
	$string = preg_replace('#</p>\s*<p>#ism', '</p><p>', $string);
	$string = preg_replace('#</li>\s*<li>#ism', '</li><li>', $string);
	$string = preg_replace('#<ul>\s*<li>#ism', '<ul><li>', $string);
	$string = preg_replace('#</li>\s*<ul>#ism', '</li><ul>', $string);

	$string = nl2br($string);
	
	$isTable = false;
	$allowReplace = false;
	

	/* ���� � ���� ����������, ��� �� ���� ��� ��������� ������� ����������� 
	� ����� ��� ������� ����� preg_replace */
	$length = strlen($string);
	for($i = 0; $i < $length; $i ++) {
		if($i + 7 <= $length) {
			$tableTag = substr($string, $i, 6);
			if($tableTag == '<table') {
				$isTable = true;
				$allowReplace = true;
			}
		}
		if($i + 3 <= $length) {
			$tdTag = substr($string, $i, 3);
			
			if($tdTag == '<td') {
				$allowReplace = false;
			}
		}
		if($i + 5 <= $length) {
			$tdTag = substr($string, $i, 5);
				
			if($tdTag == '</td>' && $isTable) {
				$allowReplace = true;
			}
		}
		if($i + 8 <= $length) {
			$tableTag = substr($string, $i, 7);
			if($tableTag == '</table>') {
				$isTable = false;
				$allowReplace = false;
			}
		}
		
		if($allowReplace) {
			if($i + 6 <= $length) {
				$brTag = substr($string, $i, 6);
				if($brTag == '<br />') {
					for($j = $i; $j <= $i + 5; $j ++) {
						$string[$j] = " ";
					}
				}
			}
		}
	}
	
	return $string;
}