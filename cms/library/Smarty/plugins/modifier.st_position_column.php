<?php
/**
 * @param CDateHelper $date
 * @param array $query 
 * @param string $searcher
 */
function smarty_modifier_st_position_column($date, $query, $searcher)
{
	
	$template = '<td {weekend_class}><span class="{color_class}">{position}</span></td>';
	$weekendClass = $date->isWeekend() ? 'class="weekend"' : '';

   
   $todayPosition = $date->getQueryPosition($query['id'], $searcher); 
   $yesterdayPosition = isset($query['_positions'][date('Y-m-d', $date->getTime() - 24 * 3600)]) ? $query['_positions'][date('Y-m-d', $date->getTime() - 24 * 3600)][$searcher] : 0;
   
   if(empty($yesterdayPosition)) {
   	/*
   	 * �� �����, ����� ����� ���� �������.
   	 */
   	if(!empty($todayPosition)) {
   		//���� - �������, ������� �� ���������
   		$class = 'green';
   		$diff = '-';
   	} else {
   		$class = 'blue';
   		$diff  ='-';
   	}
   	
   } else {
   	//��������� ������� ��������.
   	if(!empty($todayPosition)) {
   		//����������� ������� ���� ��������. �������, � ����� ������� ��� ����������.
   		if($todayPosition > $yesterdayPosition) {
   			$class = 'red';
   			$diff = '-'.($todayPosition - $yesterdayPosition);
   		} elseif ($todayPosition < $yesterdayPosition) {
   			$class = 'green';
   			$diff = '+'.($yesterdayPosition - $todayPosition);
   		} else {
   			$class = 'blue';
   			$diff = 0;
   		}
   	} else {
   		//�������� �� ������. ����� - ������, ���� �������, �� ������� �� ���������
   		$class = 'red';
   		$diff = '-';
   	}
   }
   
   $result = str_replace(array(
   		'{weekend_class}',  
   		'{color_class}',
   		'{position}'
   ), array(
   		$weekendClass,
   		$class,
   		$todayPosition
   ), $template);
   
   
   if($date->isEndOfMonth()) {
   		$result .= '<td class="month_end"></td>';
   }
   
   return $result;
}

?>