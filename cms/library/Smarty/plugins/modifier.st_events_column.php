<?php
/**
 * @param CDateHelper $date
 */
function smarty_modifier_st_events_column($date)
{
	
	$eventsCount = $date->getEventsCount();
	if($eventsCount) {
		$template = '<td {weekend_class}>
			<div class="events-item">
			<i><span>{events_count}</span></i>
			<div class="events-item-hint">
			<h3>������� {date}</h3>
			<div class="events-item-body">
			{events}
			</div>
			<span class="close-hint"><span>���, �������</span></span>
			<div class="arr-hint"></div>
			</div>
			</div>
		</td>';
		
		$eventTemplate = '<p {bof_class}>{bof_prefix}<a href="{event_url}">{event_name}</a></p>';
		$eventsHTML = "";
		
		foreach($date->getEvents() as $event) {
			$eventsHTML .= str_replace(
				array('{bof_class}', '{bof_prefix}', '{event_url}', '{event_name}'),
				array(
					$event['is_bof'] ? 'class="warn"' : '', 
					$event['is_bof'] ? 'BOF: ' : '',
					'http://bof5.oridis.ru/backoffice/task/card/'.$event['id'].'/', 
					htmlspecialchars($event['name'])
				), 
				$eventTemplate 
			);
		}
		
		$template = str_replace('{events}', $eventsHTML, $template);
		$template = str_replace('{events_count}', $eventsCount, $template);
	} else {
		$template = '<td {weekend_class}></td>';
	}
	
	$weekendClass = $date->isWeekend() ? 'class="weekend"' : '';
	
	if($date->isEndOfMonth()) {
		$template .= '<td></td>';
	}
	
	return str_replace(array('{weekend_class}', '{date}'),
			array($weekendClass, $date->getDate()),
			$template
	);
}