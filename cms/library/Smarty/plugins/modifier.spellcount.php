<?php
/*
 * Smarty plugin
 * ------------------------------------------------------------
 * ���:        modifier
 * ���:        spell�ount
 * ����������: ��������������� ������� ��������� �����:
 *             "1 ����������"
 *             "2 ����������"
 *                "� �.�."
 *
 * ������:     {$date|spell�ount:����:���:����}
 *
 * ������:     1.5
 * ����:       18.08.2009
 *
 * ���������:  �����y�� � ����� � ���������
 * ������:     ��������� ������� (http://rmcreative.ru),
 *             SerguisD (http://sergiusd.ru)
 * ------------------------------------------------------------
 */

function smarty_modifier_spellcount($num, $one, $two, $many, $nodigit = false) {
    if ($num%10==1 && $num%100!=11){
        echo ((!$nodigit)?$num:'').' '.$one;
    }
    elseif($num%10>=2 && $num%10<=4 && ($num%100<10 || $num%100>=20)){
        echo ((!$nodigit)?$num:'').' '.$two;
    }
    else{
        echo ((!$nodigit)?$num:'').' '.$many;
    }
}