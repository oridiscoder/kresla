<?php
function smarty_modifier_draw_client_date_column($value)
{
   $today = $value;
   $tomorrow = $value + 3600 * 24;
   
   $todayMonth = date('n', $today);
   $tomorrowMonth = date('n', $tomorrow);
   
   $todayDay = date('d', $today);
   $todayDayName = date('D', $today);
   
   $months = array(
   		'���', '���', '����','���', '���', '����', '����', '���', '���', '���', '����', '���'
   	);
   
   switch ($todayDayName) {
   	case 'Sun':
   	case 'Sat':
   		$result = '<td class="weekend"><div>'.$months[$todayMonth - 1].'</div><span>'.$todayDay.'</span></td>';
   		break;
   	default:
   		$result = '<td><div>'.$months[$todayMonth - 1].'</div><span>'.$todayDay.'</span></td>';
   }
   
   if($todayMonth != $tomorrowMonth) {
   		$result .= '<td class="month_end"></td>';
   }
   
   echo $result;
}

?>