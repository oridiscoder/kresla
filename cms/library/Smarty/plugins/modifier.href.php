<?php

function smarty_modifier_href($text) {

	$text = preg_replace('#(?<!href=")(?<!src=")((?:https?|ftp)://[^\s<:]+)#', '<a href="$1" target="_blank">$1</a>', $text);
	
	return $text;
}