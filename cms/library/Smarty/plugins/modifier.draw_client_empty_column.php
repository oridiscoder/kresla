<?php
function smarty_modifier_draw_client_empty_column($value)
{
   $today = $value;
   $tomorrow = $value + 3600 * 24;
   
   $todayMonth = date('n', $today);
   $tomorrowMonth = date('n', $tomorrow);
   
   $todayDay = date('d', $today);
   $todayDayName = date('D', $today);
   
   $months = array(
   		'���', '���', '����','���', '���', '����', '����', '���', '���', '���', '����', '���'
   	);
   
   switch ($todayDayName) {
   	case 'Sun':
   	case 'Sat':
   		$result = '<td class="weekend"></td>';
   		break;
   	default:
   		$result = '<td></td>';
   }
   
   if($todayMonth != $tomorrowMonth) {
   		$result .= '<td class="month_end"></td>';
   }
   
   echo $result;
}

?>