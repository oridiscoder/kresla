<?php

class BuildTimer {
	
	private static $startTime;
	private static $intervals;
	private static $times;
	
	public static function start() {
		self::$times[] = microtime(true);
	}
	
	public static function timing($label = null) {
		$prevTime = end(self::$times);
		$now = microtime(true);
		if($label) {
			self::$intervals[$label] = $now - $prevTime;
		} else {
			self::$intervals[] = $now - $prevTime;
		}
		self::$times[] = $now;		
	}
	
	public static function getWorkTime() {
		$start = self::$times[0];
		$end = microtime(true);
		return round(($end - $start), 3);
	}
	
	public static function getIntervalsPrintable() {
		$html = "";
		foreach (self::$intervals as $key=>$item) {
			$html .= sprintf("%s\t=\t%s\n", $key, $item);
		}
		return $html;
	}
}

?>