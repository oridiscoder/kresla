<?php
class ObserverEvent {
	
	private $type;
	private $object;
	private $objectID;
	private $data;
	
	public function __construct($type) {
		$this->type = $type;
	}
	public function setObject($object) {
		$this->object = $object;
	}
	public function setObjectID($id) {
		$this->objectID = $id;
	}
	public function getType() {
		return $this->type;
	}
	public function getObject() {
		return $this->object;
	}
	public function getObjectID() {
		return $this->objectID;
	}
	public function setData($data) {
		$this->data = $data;
	}
	public function getData() {
		return $this->data;
	}
}

?>