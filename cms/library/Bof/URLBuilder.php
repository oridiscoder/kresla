<?php

class Bof_URLBuilder {
	/**
	 *
	 * @var Zend_Controller_Request_Http
	 */
	protected $request;
	
	/**
	 *
	 * @var Zend_Uri_Http
	 */
	protected $zUri;
	
	protected $requestUriString;
	
	protected $recycleURL;
	
	protected $clearURL;
	
	public function __construct($request) {
		$this->request = $request;
		$this->requestUriString = $request->getScheme().'://'.$this->request->getHttpHost().$this->request->getRequestUri();
		$this->zUri = Zend_Uri_Http::fromString($this->requestUriString);
	}
	
	/**
	 *
	 * @param DbFieldAbstract $field
	 */
	public function orderBy($field) {
		if(is_string($field)) {
			return $this->orderByFiledString($field);
		} else {
			return $this->orderByFieldObject($field);
		}
	}
	
	private function orderByFieldObject($field) {
		$query = $this->request->getQuery();
		$query['orderBy'] = $field->getName();
		unset($query['pageNumber']);
		
		$sortType = $field->getSortingType();
		if($sortType && $sortType == 'ASC') {
			$query['orderType'] = 'DESC';
		} else {
			$query['orderType'] = 'ASC';
		}
		$this->zUri->setQuery($query);
		return $this->zUri->__toString();
	}
	
	private function orderByFiledString($fieldName) {
		$query = $this->request->getQuery();
		$query['orderBy'] = $fieldName;
		unset($query['pageNumber']);
		
		$orderType = $this->request->getParam("orderType");
		$orderType = $orderType ? $orderType : 'ASC';
		if($orderType == 'ASC') {
			$query['orderType'] = 'DESC';
		} else {
			$query['orderType'] = 'ASC';
		}
		$this->zUri->setQuery($query);
		return $this->zUri->__toString();
	}
	
	public function showHiddenColumns() {
		$query = $this->request->getQuery();
		if(isset($query['_show_hidden_columns'])) {
			unset($query['_show_hidden_columns']);
		} else {
			$query['_show_hidden_columns'] = 1;
		}
		$this->zUri->setQuery($query);
		return $this->zUri->__toString();
	}
	
	public function getRecycleUrl() {
		$query = $this->request->getQuery();
		unset($query['pageNumber']);
		
		if($this->isRecycleURL()) {
			unset($query['is_recycle']);			
		} else {
			$query['is_recycle'] = 1;
		}
		
		$this->zUri->setQuery($query);
		return (string) $this->zUri;
	}
	
	public function isRecycleURL() {
		$query = $this->request->getQuery();
		return isset($query['is_recycle']) ? $query['is_recycle'] : false;
	}
	
	public function getFilterValue($fieldName) {
		return $this->request->getQuery($fieldName);
	}
	
	public function set($key, $value) {
		$query = $this->request->getQuery();
		$query[$key] = $value;
		$this->zUri->setQuery($query);
		return $this->zUri->__toString();
	}
	
	public function setRecycleURL($url) {
		$this->recycleURL = $url;
	}
	
	/* public function getRecycleURL() {
		if($this->recycleURL) {
			return $this->recycleURL;
		} else {
			return $this->set("is_recycle", 1);
		}
	} */
	
	public function setClearURL($url) {
		$this->recycleURL = $url;
	}
	
	public function getClearURL() {
		if($this->clearURL) {
			return $this->clearURL;
		} else {
			return $this->request->getPathInfo();
		}
	}
	
	/**
	 * @return Bof_Request
	 */
	public function getRequest() {
		return $this->request;
	}
	
}

?>