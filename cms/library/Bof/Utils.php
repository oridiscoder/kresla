<?php

class Utils {
	
	public static function getDaysInMonth($monthNumber) {
		global $days_in_month;
		$days_in_month[1] = 31;
		$days_in_month[2] = 29; //!!!!insert visokosny day support!!!!!!!!
		$days_in_month[3] = 31;
		$days_in_month[4] = 30;
		$days_in_month[5] = 31;
		$days_in_month[6] = 30;
		$days_in_month[7] = 31;
		$days_in_month[8] = 31;
		$days_in_month[9] = 30;
		$days_in_month[10] = 31;
		$days_in_month[11] = 30;
		$days_in_month[12] = 31;
	}
	
	public static function string2URL($string) {
		require_once 'Transliteratorer.php';
		$newString = Transliteratorer::toTranslit($string);		
		$newString = trim($newString);
		$newString = str_replace(array(" ", "-", "/"), "_", $newString);
		$newString = preg_replace('/[^_\w]/', '', $newString);
		$newString = preg_replace('/_{2,}/', '_', $newString);		
		$newString = trim($newString, '_');
		$newString = strtolower($newString);
		return $newString;
	}
	
	public static function escapeFilename($filename) {
		require_once 'Transliteratorer.php';
		$filename = \Transliteratorer::toTranslit($filename);
		$filename = preg_replace('/[^a-zA-Z0-9_\-.]/', '_', $filename);
		return $filename;
	}
	
	public static function mergeUnique($one, $two) {
		if(is_array($two)) {
			foreach ($two as $el) {
				if(!in_array($el, $one)) {
					$one[] = $el;
				}
			}
		}
		return $one;
	}
	
	public static function extractColumn($rows, $colname) {
		$column = array();
		foreach($rows as $row) {
			$column[] = $row[$colname];
		}
		return $column;
	}
	
}

?>