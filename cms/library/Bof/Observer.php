<?php
interface Observer {
	public function notify(ObserverEvent $event);
}

?>