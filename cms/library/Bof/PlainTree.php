<?php

class PlainTree {
	private $items;
	private $idFieldName;
	private $parentIdFiledName;
	private $plainTree;
	
	public function __construct($items, $idFieldName, $parentIdFieldName) {
		$this->items = $items;
		$this->idFieldName = $idFieldName;
		$this->parentIdFiledName = $parentIdFieldName;
	}
	
	protected function parents() {
		$parents = array();
	
		foreach($this->items as $key=>$item) {
			$parents[$item[$this->parentIdFiledName]][] = array($item[$this->idFieldName], $key);
		}
	
		ksort($parents);
	
		return $parents;
	}
	
	
	protected function children(&$parents, $parentID = null) {
		if($parentID !== null) {
			if(isset($parents[$parentID])) {
				$children = $parents[$parentID];				
				foreach ($children as $ch) {
					$childID = $ch[0];
					$childKey = $ch[1];
					$this->plainTree[$childKey] = $childID;
					$this->children($parents, $childID);
				}
				unset($parents[$parentID]);
			}
		} else {
			foreach ($parents as $parentID => $children) {
				$this->children($parents, $parentID);
			}
		}
	}
	
	
	public function getTree() {
		$parents = $this->parents();
		$keys = array_keys($parents);
		$this->children($parents);
		return $this->plainTree;
	}
	
	public static function multisort($items, $keys) {
		$result = array();
		foreach ($keys as $key) {
			$result[] = $items[$key];
		}
		return $result;
	}
}

?>