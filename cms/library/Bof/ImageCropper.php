<?php
class ImageCropper {
	
	const TYPE_JPG = 1;
	const TYPE_PNG = 2;
	const TYPE_GIF = 3;
	
	protected $source;
	protected $sourceTemp;
	protected $fileType;
	protected $ext;
	protected $type;
	protected $fileName;
	protected $sourceWidth;
	protected $sourceHeight;
	protected $sourceTempWidth;
	protected $sourceTempHeight;

	public function __construct($sourceFileName) {
		if(!file_exists($sourceFileName)) {
			throw new Exception("Image file $sourceFileName not found");
		}
		$this->fileName = $sourceFileName;
		$this->ext = self::getFileExtension($this->fileName);
		$this->type = self::getImageTypeByExtension($this->ext);
		
		list($this->sourceWidth, $this->sourceHeight) = getimagesize($sourceFileName);
		
		$this->source = $this->loadImage();
		$this->sourceTemp = $this->source;
		$this->sourceTempWidth = $this->sourceWidth;
		$this->sourceTempHeight = $this->sourceHeight;
	}
	
	protected function loadImage() {
		switch($this->type) {
			case self::TYPE_GIF: {
				return imagecreatefromgif($this->fileName);
			}
			case self::TYPE_JPG: {
				return imagecreatefromjpeg($this->fileName);
			}
			case self::TYPE_PNG: {
				return imagecreatefrompng($this->fileName);
			}
		}
	}
	
	/**
	 * ��������� ����������� ��� $destinationFileName
	 * @param string $destinationFileName
	 * @param integer $width - optional
	 * @param integer $height - optional
	 * @return ImageCropper
	 */
	public function saveAs($destinationFileName, $width = null, $height = null) {
		
		if($width && $height) {
			$this->drawIn($width, $height);
		}
		
		$ext = self::getFileExtension($destinationFileName);
		$type = self::getImageTypeByExtension($ext);
		
		switch($this->type) {
			case self::TYPE_GIF: {
				imagegif($this->sourceTemp, $destinationFileName);
			}
			case self::TYPE_JPG: {
				imagejpeg($this->sourceTemp, $destinationFileName);
			}
			case self::TYPE_PNG: {
				imagepng($this->sourceTemp, $destinationFileName);
			}
		}
		return $this;
	}
	
	protected function watermark() {
		//TODO �������� ���������� ����������
	}
	
	/**
	 * ��������� ����������� � �������������
	 * @param integer $width
	 * @param integer $height
	 * @return ImageCropper
	 */
	public function drawIn($width, $height) {
		
		//���� �������� ����������� ������ ��������, � ������� �� ��� ���������, �� ��������� ������� ��
		//�������� ��������� �����������
		if ($width > $this->sourceTempWidth)
			$width = $this->sourceTempWidth;
		if ($height > $this->sourceTempHeight)
			$height = $this->sourceTempHeight;
		
		if ($this->sourceTempWidth/$width < $this->sourceTempHeight/$height)
			$width = $this->sourceTempWidth/$this->sourceTempHeight * $height;
		else
			$height = $this->sourceTempHeight / $this->sourceTempWidth * $width;
		
		//��������� � �������� �������������
		$imageHandler = imagecreatetruecolor($width, $height);
		imagealphablending( $imageHandler, false );
		imagesavealpha( $imageHandler, true);
		imagecopyresampled($imageHandler, $this->sourceTemp, 0, 0, 0, 0, $width, $height, $this->sourceTempWidth, $this->sourceTempHeight);
		imagealphablending( $imageHandler, false );
		imagesavealpha( $imageHandler, true);
		
		$this->sourceTemp = $imageHandler;
		$this->sourceTempWidth = $width;
		$this->sourceTempHeight = $height;
		
		return $this;
	}
	
	/**
	 * ���������� ��������� ����������� � �������������, �������� ����, ������� ������� �� ����� ����� ��������������
	 * @param integer $width
	 * @param integer $height
	 * @return ImageCropper
	 */
	public function drawInAndCrop($width, $height) {
		
		$actualWidth = $this->sourceTempWidth;
		$actualHeight = $this->sourceTempHeight;

		$needWidth = (int) $width;
		$needHeight = (int) $height;

		//���������, �� ����� ������� ����� ��������:
		if($actualWidth > $actualHeight) {
			//������ ������ ������:
			/*
			* |---------------|
			* |               |
			* |               |
			* |---------------|
			*/
			//$width = round($actualWidth * $needHeight / $actualHeight);
						
			/* ������� ����������� ������ �������� */
			$imageHandler = imagecreatetruecolor($needWidth, $needHeight);
			/* ��������,����� ������ ��������� ����������� ���� ��������� */
			$height = round($needHeight * $actualWidth / $needWidth);
			/* ��������, ����� �������� ������ ���� �� ��������� */
			$marginTop = round(($actualHeight - $height) / 2);
			 
			imagecopyresampled($imageHandler, $this->sourceTemp, 0, 0, 0, $marginTop, $needWidth, $needHeight, $actualWidth, $height);
				
		} else {
			
			//������ ������ ������:
			/*
			* |-----|
			* |     |
			* |     |
			* |     |
			* |     |
			* |-----|
			*/
				
			$height = round($actualHeight * $needWidth / $actualWidth);
			$im2 = imagecreatetruecolor($needWidth, $height);
			imagecopyresampled($im2, $this->sourceTemp, 0, 0, 0, 0, $needWidth, $height, $actualWidth, $actualHeight);
				
			//������ crop:
			$margin = round(($height - $needHeight) / 2);
			$imageHandler = imagecreatetruecolor($needWidth, $needHeight);
			imagecopy($imageHandler, $im2, 0, 0, 0, $margin, $needWidth, $needHeight);
				
			imagedestroy($im2);
		}
		
		$this->sourceTemp = $imageHandler;
		$this->sourceTempWidth = $width;
		$this->sourceTempHeight = $height;
		
		return $this;
	}
	
	/**
	 * @return ImageCropper
	 */
	public function restoreSource() {
		imagedestroy($this->sourceTemp);
		$this->sourceTemp = $this->source;
		$this->sourceTempWidth = $this->sourceWidth;
		$this->sourceTempHeight = $this->sourceHeight;
		return $this;
	}
	
	public function __destruct() {
		imagedestroy($this->source);
	}
	
	public static function getFileExtension($fileName) {
		return substr($fileName, strrpos($fileName, ".") + 1);
	}
	public static function getImageTypeByExtension($ext) {
		$ext = strtolower($ext);
		switch($ext) {
			case 'jpg':
			case 'jpeg':
				return self::TYPE_JPG;
			case 'png':
				return self::TYPE_PNG;
			case 'gif':
				return self::TYPE_GIF;
			default:
				throw new InvalidImageTypeException("Unknown image extension: $ext");
		}
	}
	public static function isValidImageExtension($filename) {
		return preg_match('/\.(gif|jpg|jpeg|png)$/', $filename);
	}
		
}

class InvalidImageTypeException extends Exception {
	
}

?>