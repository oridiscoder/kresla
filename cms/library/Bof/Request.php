<?php
require_once ('Zend/Controller/Request/Http.php');

class Bof_Request extends Zend_Controller_Request_Http {
	public function getQuery($key = null, $default = null) {
		$query = parent::getQuery($key, $default);
		
		if($this->isXmlHttpRequest() && Application::getAppCharset() == 'windows-1251') {
			if(is_array($query)) {
				foreach ($query as $key=>$value) {
					$query[$key] = iconv('utf-8', 'cp1251', $value);
				}
			} elseif(is_string($query)) {
				$query = iconv('utf-8', 'cp1251', $query);
			}
		}
		
		return $query;
	}
	
	public function setQuery($spec, $value = null) {
		if($this->isXmlHttpRequest() && Application::getAppCharset() == 'windows-1251') {
			if(is_string($spec)) {
				$value = iconv('cp1251', 'utf-8', $value);
			}
		}
		parent::setQuery($spec, $value);
	}
	
	public function getInfoAbountDependentTables() {
		$pathName = $this->getPathInfo();
		$matches = array();
		$dependencies = null;
		if(preg_match('#/\d+,\d+/#', $pathName)) {
			preg_match_all('#(\d+),(\d+)#', $pathName, $matches);
			$dependencies = array();
			foreach ($matches[0] as $key=>$match) {
				$dependencies[] = array(
						'row' => $matches[1][$key],
						'table' => $matches[2][$key]
				);
			}
		}
		return $dependencies;
	}
	
	public function getPathInfo() {
		$path = parent::getPathInfo();
		if($path[0] != '/') {
			$path = '/'.$path;
		}
		if($path[strlen($path) - 1] != '/') {
			$path = $path .'/';
		}
		return $path;
	}
}

?>