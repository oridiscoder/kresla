<?php
require_once 'BOFEvent.php';

class BOFEventException extends Exception {
	
}

/**
 * �������� �����-�������. �� ������ ����� ������� ����� ��� �� ����� ��������� ������ ������������� �����
 * @author gourry
 *
 */
class BOFEvent {
	const TYPE_404 = 1;
	const TYPE_PAGE_PARAMETERS_CHANGED = 2;
	const TYPE_PLAIN_TEXT_NOT_FOUND = 3;
	const TYPE_HTML_NOT_FOUND = 4;
	const TYPE_ANY_SEO_EVENT = 5;
	const TYPE_ANY_LT_EVENT = 6;
	const TYPE_QUERY_FREQUENCY_CHANGED = 7;
	const TYPE_PLAGIARISM_FOUND = 8;
	const TYPE_SAMEPAGE_FOUND = 9;
	const TYPE_PAGE_DIFFER_YANDEX = 10;
	const TYPE_PAGE_DIFFER_GOOGLE = 11;
	const TYPE_WM_CHANGES = 13;
	const TYPE_WM_SITE_NOT_FOUND = 14;
	const TYPE_WM_AUTH_ERROR = 15;
	const TYPE_CAPTCHABOT_EMPTY_WALLET = 16;
	const TYPE_DBTABLE_INSERT = 17;
	const TYPE_DBTABLE_UPDATE = 18;
	const TYPE_DBTABLE_DELETE = 19;
	const TYPE_PROJECT_UPDATE = 20;
	const TYPE_CLIENT_CONTACT = 21;
	const TYPE_NEED_CLIENT_CONTACT = 22;
	const TYPE_SAPE_SITE_STEAL = 23;
	const TYPE_LT_200_ERROR = 24;
	const TYPE_LT_CODE_EXIST = 25;	
	const TYPE_LT_PAGES_YANDEX = 26;
	const TYPE_LT_PAGES_GOOGLE = 27;
	const TYPE_LT_PR = 28;
	const TYPE_LT_CY = 29;
	const TYPE_LT_YACA_NO = 30;
	const TYPE_LT_YACA_YES = 31;
	const TYPE_LT_PROFIT_SAPE = 32;
	const TYPE_LT_PROFIT_TRUSTLINK = 33;
	const TYPE_LT_DMOZ_YES = 34;
	const TYPE_LT_DMOZ_NO = 35;
	const TYPE_LT_SAPE_ERROR_LINKS = 36;
	const TYPE_LT_TRUSTLINK_ERROR_LINKS = 37;
	const TYPE_LT_TRUSTLINK_MANY_EXTERNAL_LINKS = 38;
	const TYPE_LT_MANY_LINKS = 39;
	const TYPE_THROUGH_BLOCK = 40;
	const TYPE_DYNAMIC_CONTENT = 41;
	const TYPE_SAPE_REINDEX_PR = 42;
	const TYPE_SAPE_REINDEX = 43;
	const TYPE_TRUSTLINK_REINDEX = 44;
	const TYPE_LT_NEW_REQUEST = 45;
	const TYPE_MANUAL_CHECK = 46;
	const TYPE_ROBOTS_TXT_NOT_FOUND = 47;
	const TYPE_ROBOTS_TXT_CHANGED = 48;
	const TYPE_TEXT_NOT_UNIQUE = 49;
		
	const TYPE_WRONG_CY = 51;
	const TYPE_WRONG_PR = 52;
	const TYPE_WRONG_PAGES_YANDEX = 53;
	const TYPE_WRONG_PAGES_GOOGLE = 54;
	const TYPE_WRONG_YACA = 55;
	const TYPE_WRONG_DMOZ = 56;
	const TYPE_LT_PERMISSIONS = 57;
	const TYPE_LT_BASEMENT_LINKS = 58;
	const TYPE_LT_NO_PHRASES = 59;
	const TYPE_DBC_NO_MONEY = 60;
	
	
	protected $type;
	protected $objectID;
	protected $timeStart;
	protected $timeFinish;
	protected $id;
	protected $title;
	protected $text;
	
	protected $executerId;
	protected $initiatorId;
	protected $priority;
	protected $projectId;
	protected $notice = false;
	protected $deadlineDate = null;
	
	public function __construct($type, $objectID, $text = null) {
		$this->type = $type;
		$this->objectID = $objectID;
		$this->timeStart = time();
		$this->text = $text;
	}
	
	/**
	 * @return integer
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * @return integer
	 */
	public function getObjectID() {
		return $this->objectID;
	}
	
	public function getDateStart() {
		return strftime("%Y-%m-%d %H:%M:%S", $this->timeStart);
	}
	public function getDateFinish() {
		return strftime("%Y-%m-%d %H:%M:%S", $this->timeFinish);
	}
	
	public function setDateStart($date) {
		$this->timeStart = strtotime($date);
	}
	public function setDateFinish($date) {
		$this->timeFinish = strtotime($date);
	}
	public function getID() {
		return $this->id;
	}
	public function setID($id) {
		$this->id = $id;
	}
	public function getText() {
		return $this->text;
	}
	public function setText($txt) {
		$this->text = $txt;
	}
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($t) {
		$this->title = $t;
	}
	/**
	 * ���������� �������������� �� ������ �������
	 * @param integer $id
	 */
	public function setExecuterId($id) {
		$this->executerId = $id;
	}
	/**
	 * ���������� ������������ ������
	 * @param integer $id
	 */
	public function setInitiatorId($id) {
		$this->initiatorId = $id;
	}
	
	/**
	 * ��� ������������� �� ������ �������
	 */
	public function getExecuterId() {
		return $this->executerId;
	}
	
	/**
	 * ��� ������ ���� ������������� ������
	 */
	public function getInitiatorId() {
		return $this->initiatorId;
	}
	
	/**
	 * ���������� ��������� ������, ������� ����� ������� �� ����� �������
	 * @param integer $priority
	 */
	public function setPriority($priority) {
		if($priority != 215 && $priority != 216 && $priority != 217) {
			throw new BOFEventException("Priority  $priority not allowed. Allowed are 215, 216, 217");
		}
		$this->priority = $priority;
	}
	
	public function getPriority() {
		return $this->priority;
	}
	
	/**
	 * ���������� ������������ ������
	 * @param integer $id
	 */
	public function setProjectId($id) {
		$this->projectId = $id;
	}
	
	/**
	 * ��� ������������� �� ������ �������
	 */
	public function getProjectId() {
		return $this->projectId;
	}
	
	public function isNotice($notice = null) {
		if(is_bool($notice)) {
			$this->notice = $notice;
		} else {
			return $this->notice;
		}
	}
	/**
	 * ���������� ���� �������� ��� ����������� ������
	 * @param string $datetime - 2012-10-01 10:30:00
	 */
	public function setDeadlineDate($datetime) {
		$this->deadlineDate = $datetime;
	}
	
	public function getDeadlineDate() {
		return $this->deadlineDate;
	}
	
	/**
	 * ������� �������� ������ BOFEvent �� �������������� �������, ���������������� ������ ������� bof_events
	 * @param array $dataArray
	 * @return BOFEvent
	 */
	public static function fromArray($dataArray) {
		if(!$dataArray['type']) throw new BOFEventException("Field `type` required and must be integer");
		if(!$dataArray['object_id']) throw new BOFEventException("Field `object_id` required and must be integer");
		
		$event = new self($dataArray['type'], $dataArray['object_id']);
		if(isset($dataArray['date_start'])) $event->setDateStart($dataArray['date_start']);
		if(isset($dataArray['date_finish'])) $event->setDateFinish($dataArray['date_finish']);
		if(isset($dataArray['id'])) $event->setID($dataArray['id']);
		
		return $event;
	}
}

/**
 * ��������������� �������. ���������� ������������ ������ �����, ���� �� �����,
 * ����� ��� �������� ������� - ��� ������� ����������� � �� �� ������� �������
 * � ����������� ������ � ��� ������, ���� ��� ��� �� �������.
 *
 * @author Gourry
 *
 */
class BOFLongEvent extends BOFEvent {
	
}

/**
 * ������������ �������. ��� ���������� ������ ������� - ��� ����� ������ ���������� � ��
 * ��� �������� �� ��� ������������
 * @author Gourry
 *
 */
class BOFShortEvent extends BOFEvent {
	
}