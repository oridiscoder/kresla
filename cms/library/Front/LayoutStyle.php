<?php
require_once "LayoutMinify.php";

class LayoutStyle extends LayoutMinify {
	/**
	 *
	 * @var LayoutMinify
	 */
	protected static $instance;
	
	public function init() {
		$this->setBuidPath($_SERVER['DOCUMENT_ROOT'].'/css/build');
		$this->setSourcePath($_SERVER['DOCUMENT_ROOT']);
		$this->setExt("css");
	}
	
	public function addStyle($filename) {
		$this->addSource($filename);
	}
	
	public function getStyles() {
		return $this->getSources();
	}
	
	public function getStylesHTML() {
		$html = '';
		foreach($this->getStyles() as $filename) {
			$html .= '<link rel="stylesheet" type="text/css" href="'.$filename.'">';
		}		
		return $html;
	}
	
	public function getMergedAndMinified() {
		$href = $this->getBuidPath().'/'.$this->mergeAndMinify();
		$href = $this->relativeName($href, $this->getSourcePath());
		$href = str_replace("//", "/", $href);
		return '<link rel="stylesheet" type="text/css" href="'.$href.'">';
	}
	
	/**
	 * @return LayoutStyle
	 */
	public static function getInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}

?>