<?php
require_once "LayoutMinify.php";

class LayoutScript extends LayoutMinify {
	/**
	 *
	 * @var LayoutMinify
	 */
	protected static $instance;
	
	public function init() {
		$this->setSourcePath($_SERVER['DOCUMENT_ROOT']);
		$this->setBuidPath($_SERVER['DOCUMENT_ROOT'].'/js/build');
		$this->setExt("js");
	}
	
	public function addScript($filename) {
		$this->addSource($filename);
	}
	
	public function getScripts() {
		return $this->getSources();
	}
	
	public function getMergedAndMinified() {
		$src = $this->getBuidPath().'/'.$this->mergeAndMinify();
		$src = $this->relativeName($src, $this->getSourcePath());
		$src = str_replace("//", "/", $src);
		return '<script type="text/javascript" src="'.$src.'"></script>';
	}
	
	/**
	 * @return LayoutStyle
	 */
	public static function getInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}

?>