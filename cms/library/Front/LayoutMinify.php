<?php
class LayoutMinify {
	/**
	 *
	 * @var LayoutMinify
	 */
	protected static $instance;
	private $files = array();
	private $buidPath;
	private $sourcePath;
	private $fileExtension;
	
	protected function __construct() {
		$this->init();
	}
	
	public function init() {
		
	}
	
	public function addSource($src) {
		if(!in_array($src, $this->files)) {
			$this->files[] = $src;
		}
	}
	
	public function getSources() {
		return $this->files ? $this->files : array();
	}
	
	public function setBuidPath($path) {
		$this->buidPath = $path;
	}
	public function getBuidPath() {
		return str_replace("\\", "/", $this->buidPath);
	}
	public function setSourcePath($path) {
		$this->sourcePath = $path;
	}
	public function getSourcePath() {
		return str_replace("\\", "/", $this->sourcePath);
	}
	
	public function getHashcode($sources) {
		return md5(implode("", $sources));
	}
	
	public function getMergedFiles($buildPath, $hashCode) {
		return glob($buildPath.'/'.$hashCode.'*');
	}
	
	public function relativeName($filename, $path) {
		$filename = str_replace("\\", "/", $filename);
		$filename = str_replace($path, "", $filename);
		return $filename;
	}
	
	public function getNextFilename($filename, $hashCode) {
		if($filename) {
			if(preg_match('/_(\d+)\.(?:css|js)$/', $filename, $m)) {
				$num = $m[1] + 1;
			} else {
				$num = 1;
			}
		} else {
			$num = 1;
		}
		
		return $hashCode.'_'.$num.'.'.$this->getExt();
	}
	
	public function getExt() {
		return $this->fileExtension;
	}
	public function setExt($ext) {
		$this->fileExtension = $ext;
	}
	
	public function mergeAndMinify() {
		
		$sources = $this->getSources();
		$hashCode = $this->getHashcode($sources);
		
		$buildPath = $this->getBuidPath();
		$sourcePath = $this->getSourcePath();
		
		if($sources) {
	
			$rebuild = false;
			
			$files = $this->getMergedFiles($buildPath, $hashCode);
	
			if($files) {
				
				$filename = $this->relativeName(end($files), $buildPath);
				$mtime = filemtime($buildPath.'/'.$filename);
				
				/* проверка, менялись ли исходники */
				foreach($sources as $sourceFilename) {
					if(file_exists($sourcePath.'/'.$sourceFilename)) {
						if(filemtime($sourcePath.'/'.$sourceFilename) > $mtime) {
							$rebuild = true;
							break;
						}
					}
				}
				
			} else {
				$rebuild = true;
				$filename = null;
			}
	
			if($rebuild) {

				if(!defined('MINIFY_MIN_DIR')) {
					define('MINIFY_MIN_DIR', APPLICATION_PATH . '/../library/Minify');
				}
				
				// load config
				require MINIFY_MIN_DIR . '/config.php';
				
				// setup include path
				set_include_path($min_libPath . PATH_SEPARATOR . get_include_path());
				
				require_once 'Minify.php';
				
				Minify::$uploaderHoursBehind = $min_uploaderHoursBehind;
				Minify::setCache(
						isset($min_cachePath) ? $min_cachePath : ''
						,$min_cacheFileLocking
				);
	
				$content = Minify::combine($sources);
	
				/* очистим предыдущие билды */
				if($files) {
					foreach ($files as $fname) {
						unlink($fname);
					}
				}
				
				$filename = $this->getNextFilename($filename, $hashCode);
	
				file_put_contents($buildPath.'/'.$filename, $content);
			}
			
			return $filename;
		}
	}
	
	/**
	 * @return LayoutStyle
	 */
	public static function getInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}
?>