<?php /* Smarty version Smarty-3.1.8, created on 2015-01-26 11:04:02
         compiled from "/home/u24576/kresla-otido.ru/cms/application/templates/layout.html" */ ?>
<?php /*%%SmartyHeaderCode:1290391015538dc7b0d00fe1-93503025%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '998df950ae180b5c313f77f4bd9a35b5581a554e' => 
    array (
      0 => '/home/u24576/kresla-otido.ru/cms/application/templates/layout.html',
      1 => 1422244120,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1290391015538dc7b0d00fe1-93503025',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_538dc7b0d6ea97_29981825',
  'variables' => 
  array (
    'pageTitle' => 0,
    'layoutStyles' => 0,
    'href' => 0,
    'baseURL' => 0,
    'layoutScripts' => 0,
    'src' => 0,
    'user' => 0,
    'menuTop' => 0,
    'breadcrumbs' => 0,
    'breadcrumbsRenderer' => 0,
    'content' => 0,
    'menuLeft' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_538dc7b0d6ea97_29981825')) {function content_538dc7b0d6ea97_29981825($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/u24576/kresla-otido.ru/cms/library/Smarty/plugins/modifier.date_format.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru" >
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php if ($_smarty_tpl->tpl_vars['pageTitle']->value){?><?php echo $_smarty_tpl->tpl_vars['pageTitle']->value;?>
<?php }?></title>
	<link rel="stylesheet" href="/cms/css/general.css" type="text/css" media="all" />
    <link rel="stylesheet" href="/cms/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/cms/css/new-styles.css" type="text/css" />
    <link rel="stylesheet" href="/cms/css/auditors-style.css" type="text/css" />    
    <link rel="stylesheet" href="/cms/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/cms/js/uploadify/uploadify.css" />
    <!--[if IE ]><link rel="stylesheet" type="text/css" href="/cms/css/ie.css" /><![endif]-->
    <?php  $_smarty_tpl->tpl_vars['href'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['href']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['layoutStyles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['href']->key => $_smarty_tpl->tpl_vars['href']->value){
$_smarty_tpl->tpl_vars['href']->_loop = true;
?>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['href']->value;?>
" type="text/css" />
    <?php } ?>
    <!--[if IE]><script language="javascript" type="text/javascript" src="/cms/js/excanvas.compiled.js"></script><![endif]-->
    <script type="text/javascript" src="/cms/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/cms/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/cms/js/uploadify/jquery.uploadify.min.js"></script>    
    <script type="text/javascript" src="/cms/js/lazyload-min.js"></script>    
    <script type="text/javascript" src="/cms/js/table-row-selector.js" ></script>
    <script type="text/javascript" src="/cms/js/table-sort.js" ></script>
    <script type="text/javascript" src="/cms/js/table-parentfield-loader.js" ></script>
    <script type="text/javascript" src="/cms/js/dependencies.js" ></script>
    <script type="text/javascript" src="/cms/js/functions.js" ></script>
    <script type="text/javascript" src="/cms/js/main.js" ></script>
    <script type="text/javascript" src="/cms/js/auditors.js" ></script>
    <script type="text/javascript">
    	var baseURL = '<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
';
    </script>
    <?php  $_smarty_tpl->tpl_vars['src'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['src']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['layoutScripts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['src']->key => $_smarty_tpl->tpl_vars['src']->value){
$_smarty_tpl->tpl_vars['src']->_loop = true;
?>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['src']->value;?>
" ></script>
    <?php } ?>
    <script type="text/javascript" src="/cms/js/autoload.js" ></script>
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
</head>

<body>

<div id="wrapper">

	<div id="header">

	<div class="logo"><a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/"><img src="/cms/images/logo.png" /></a></div>

   <?php if ($_smarty_tpl->tpl_vars['user']->value){?>
   <div class="user">
        <div>
            <div>
      	      <a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/backoffice/user/<?php echo $_smarty_tpl->tpl_vars['user']->value->getEmployeeID();?>
/"><?php echo $_smarty_tpl->tpl_vars['user']->value->getFullName();?>
</a>
            </div>
        </div>
	</div>	
	
	<?php }?>
    
    <div class="top_right">
    
    <div class="exit"><a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
/auth/logout">Выход</a></div>
    
    </div>
    
    <div class="menu">
        <?php echo $_smarty_tpl->tpl_vars['menuTop']->value;?>

        <span class="tl"></span>
        <span class="tr"></span>
    </div>

	</div><!-- #header-->

	<div id="middle">

		<div id="container">
			<div id="content">
            
	            <!-- breadcrumbs -->	            
	            <?php if (!empty($_smarty_tpl->tpl_vars['breadcrumbs']->value)){?>
	            	<?php echo $_smarty_tpl->tpl_vars['breadcrumbsRenderer']->value->render();?>

	            <?php }?>
                <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

			</div><!-- #content-->			
		</div><!-- #container-->
		
		<div id="left">
			<div class="menu_left">
			    <div>
			        <div class="padd">
			            <?php echo $_smarty_tpl->tpl_vars['menuLeft']->value;?>

			        </div>
			    </div>
			</div>
			
			<div class="menu_left_b">
			    <div class="top_bg">
			        <div class="bottom_bg">
			            <?php echo $_smarty_tpl->getSubTemplate ("informers/informers.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			        </div>
			    </div>
			</div>
		</div><!-- Left -->

	</div><!-- #middle-->

</div><!-- #wrapper -->

<div id="footer">© 2005—<?php echo smarty_modifier_date_format(time(),"%Y");?>
 ORIDIS Software</div><!-- #footer -->

</body>
</html><?php }} ?>