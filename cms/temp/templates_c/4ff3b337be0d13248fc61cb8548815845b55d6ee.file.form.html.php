<?php /* Smarty version Smarty-3.1.8, created on 2015-06-15 11:48:23
         compiled from "/home/u24576/kresla-otido.ru/www/../cms/application/templates//operations/EditFieldOperation/form.html" */ ?>
<?php /*%%SmartyHeaderCode:151773193853a2e67b434f66-63526828%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ff3b337be0d13248fc61cb8548815845b55d6ee' => 
    array (
      0 => '/home/u24576/kresla-otido.ru/www/../cms/application/templates//operations/EditFieldOperation/form.html',
      1 => 1422244239,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '151773193853a2e67b434f66-63526828',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_53a2e67b710600_19036574',
  'variables' => 
  array (
    'fieldDataError' => 0,
    'error' => 0,
    'saved' => 0,
    'formURL' => 0,
    'data' => 0,
    'tables' => 0,
    'fieldTypes' => 0,
    'details' => 0,
    'aligns' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a2e67b710600_19036574')) {function content_53a2e67b710600_19036574($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/home/u24576/kresla-otido.ru/cms/library/Smarty/plugins/function.html_options.php';
?><?php if ($_smarty_tpl->tpl_vars['fieldDataError']->value){?><div class="home_eror" style="margin-bottom:10px;">
<ul><?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldDataError']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
$_smarty_tpl->tpl_vars['error']->_loop = true;
?><li><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</li><?php } ?></ul>
</div><?php }?>
<?php if ($_smarty_tpl->tpl_vars['saved']->value){?><div class="home_success" style="margin-bottom:10px;">Сохранено</div><?php }?>

<div class="home_messages">
<form action="<?php echo $_smarty_tpl->tpl_vars['formURL']->value;?>
" method="post" enctype="multipart/form-data" onsubmit="return checkFieldForm(this);">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" />
<table>
<tr>
	<td class="name">Таблица</td>
	<td class="form"><select name="data[obj_id]">
	<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['tables']->value,'selected'=>$_smarty_tpl->tpl_vars['data']->value['obj_id']),$_smarty_tpl);?>

	</select></td>
</tr>
<tr>
	<td class="name">Название поля в таблице</td>
	<td class="form"><input type="text" name="data[name]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['name'];?>
" id="FieldName"/></td>
</tr>
<tr>
	<td class="name">Имя-отображение</td>
	<td class="form"><input type="text" name="data[viewname]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['viewname'];?>
" /></td>
</tr>
<tr>
	<td class="name">Подсказка</td>
	<td class="form">
		<input type="text" name="data[hint]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['hint'];?>
" />
		<div class="field_hint">Будет отображаться под элементом редактирования</div>
	</td>
</tr>
<tr>
	<td class="name">Тип поля</td>
	<td class="form">
	<select name="data[type]" id="FieldType">
	<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['fieldTypes']->value,'selected'=>$_smarty_tpl->tpl_vars['data']->value['type']),$_smarty_tpl);?>

	</select>
	</td>
</tr>
<?php echo $_smarty_tpl->tpl_vars['details']->value;?>


<tr>
	<td class="name">SQL-фильтр</td>
	<td class="form"><input type="text" name="data[filter]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['filter'], ENT_QUOTES, 'utf-8', true);?>
" /></td>
</tr>

<tr>
	<td class="name">Показывать только при редактировании</td>
	<td class="form"><input type="checkbox" name="data[hidecolumn]" <?php if ($_smarty_tpl->tpl_vars['data']->value['hidecolumn']){?>checked="checked"<?php }?> /></td>
</tr>
<tr>
	<td class="name"><span title="Это поле является URL-полем для данной таблицы">URL</span></td>
	<td class="form"><input title="Это поле является URL-полем для данной таблицы" type="checkbox" name="data[is_url_field]" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_url_field']){?>checked="checked"<?php }?> /></td>
</tr>
<tr>
	<td class="name">Выравнивание</td>
	<td class="form">
		<select name="data[align]"><?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['aligns']->value,'selected'=>$_smarty_tpl->tpl_vars['data']->value['align']),$_smarty_tpl);?>
</select>
	</td>
</tr>
</table>
<div class="sep">
    <input class="home_but_save" type="submit" value="" />
    <?php if ($_smarty_tpl->tpl_vars['data']->value['id']){?> или <input type="submit" onclick="return deleteFieldConfirm();" name="delete_this_record" value="Удалить" /><?php }?>
</div>
</form>
</div>
<script>

$('#FieldType').change(function() {
	var tr = this.parentNode.parentNode;
	var data = {
		target: 'getDetails', 
		type_id : this.value, 
		table_id : $('input[name="data[obj_id]"]').eq(0).val() 
	}
	$.get(window.location.pathname, data, function (html) {
		$('form tr.uploaded').remove();
		$(tr).after(html);
	});
});
function loadParentTableSortFieldSelect(select) {
	var tableID = $(select).val();
	$.get(window.location.pathname, {target:'getTableFields', table_id: tableID}, function (html) {
		var fields = eval('(' + html + ')');
		$('#SortColumn > input').remove();
		$('#SortColumn > select').remove();
		var select = $('<select>').attr('name', 'data[sort_column]');
		for(var i = 0; i < fields.length; i++) {
			var option = $('<option>').attr('value', fields[i].id).text(fields[i].viewname);
			select.append(option);
		}
		$('#SortColumn').prepend(select);
	});
}
function checkFieldForm(form) {
	var fieldName = $('#FieldName');
	var fieldNameRexexp = /^[-\w]+$/;
	if(!fieldName.val().match(fieldNameRexexp)) {
		fieldName.focus();
		addError(fieldName, "Имя не должно быть пустым и может содержать только цифры, латинские буквы, минус и символ нижнего подчеркивания");
		return false;
	}
	return true;
}
function addError(element, errorText) {
	var errorElement = $(element).parent().children('label.error');
	if(!errorElement.length) {
		var errorElement = $('<label>').addClass('error').appendTo($(element).parent());
	}
	errorElement.text(errorText);
}
function deleteFieldConfirm() {
	return confirm("Вы действительно хотите удалить это поле?");
}

function addMDFTable() {
	var container = $('.add-table').eq(0).clone();
	container.find('.table select option').removeAttr('selected');
	container.find('.field').hide();
	container.find('.options').hide();
	$('.add-table:last').after(container);
}
function loadMDFTableFields(select) {
	select = $(select);
	var tableID = select.val();
	$.get(window.location.pathname, {target:'getTableFields', table_id: tableID}, function (html) {
		var fields = eval('(' + html + ')');
		
		var fieldsSelect = select.parents('.add-table').find('.field select').eq(0);		
		fieldsSelect.children().remove();
		for(var i = 0; i < fields.length; i++) {
			var option = $('<option>').attr('value', fields[i].id).text(fields[i].viewname);
			fieldsSelect.append(option);
		}
		select.parents('.add-table').children().show();
		enumerateMDFields();
	});
}
function enumerateMDFields() {
	$('.add-table').each(function(number) {
		var selName = 'data[_md][tables][' + number + ']';
		$(this).find('.table select').attr('name', selName);
		
		var fieldName = 'data[_md][fields][' + number + ']';
		$(this).find('.field select').attr('name', fieldName);
		
		var checkboxName = 'data[_md][notnull][' + number + ']';
		$(this).find('.options input[type="checkbox"]').eq(0)
			.attr('name', checkboxName)
			.attr('id', 'md_notnull_' + number);
		
		$(this).find('.options label').eq(0).attr('for', 'md_notnull_' + number);
		
		var checkboxName = 'data[_md][tree][' + number + ']';
		$(this).find('.options input[type="checkbox"]').eq(1)
			.attr('name', checkboxName)
			.attr('id', 'md_tree_' + number);
		
		$(this).find('.options label').eq(1).attr('for', 'md_tree_' + number);
		
		
	});
}

</script>
<style>
label.error {display:block; color:red;}
</style><?php }} ?>