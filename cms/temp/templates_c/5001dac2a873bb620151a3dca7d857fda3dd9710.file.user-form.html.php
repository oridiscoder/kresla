<?php /* Smarty version Smarty-3.1.8, created on 2015-08-07 14:35:26
         compiled from "/home/u24576/kresla-otido.ru/www/../cms/application/templates//modules/UserManager/user-form.html" */ ?>
<?php /*%%SmartyHeaderCode:134210918255c497fe66f938-98739350%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5001dac2a873bb620151a3dca7d857fda3dd9710' => 
    array (
      0 => '/home/u24576/kresla-otido.ru/www/../cms/application/templates//modules/UserManager/user-form.html',
      1 => 1422244238,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '134210918255c497fe66f938-98739350',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'formURL' => 0,
    'user' => 0,
    'groups' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_55c497fe72f8f9_09430313',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c497fe72f8f9_09430313')) {function content_55c497fe72f8f9_09430313($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/home/u24576/kresla-otido.ru/cms/library/Smarty/plugins/function.html_options.php';
?>
<style>
table.access_table {
	width:auto;
}
table.access_table td {
	border-bottom:1px solid #BAC1C9;
}
table span.table-name {
	cursor:pointer;
}
table span.table-name:hover {
	font-weight:bold;
}
#AccessTables td {
	vertical-align: top;
}
</style>

<br clear="all" />
<div class="home_messages">
<form action="<?php echo $_smarty_tpl->tpl_vars['formURL']->value;?>
" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" />
<table>
<tr>
	<td class="name">Логин</td>
	<td class="form"><input type="text" name="user[login]" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['login'];?>
" /></td>
</tr>
<tr>
	<td class="name">Пароль</td>
	<td class="form"><input type="password" name="user[password]" value="" /></td>
</tr>
<tr>
	<td class="name">Группа</td>
	<td class="form"><select name="user[group_id]" id="GroupSelect">
	<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['groups']->value,'selected'=>$_smarty_tpl->tpl_vars['user']->value['group_id']),$_smarty_tpl);?>

	</select></td>
</tr>
<tr>
	<td class="name">Суперюзер</td>
	<td class="form"><input type="checkbox" name="user[superuser]" value="1" <?php if ($_smarty_tpl->tpl_vars['user']->value['superuser']){?>checked="checked"<?php }?>/></td>
</tr>
</table>
<div class="sep" id="AccessTables">
<?php echo $_smarty_tpl->getSubTemplate ("access.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</div>
<div class="sep">
    <input class="home_but_save" type="submit" value="" />
    <?php if ($_smarty_tpl->tpl_vars['user']->value['id']){?> или <input type="submit" name="delete" value="Удалить" /><?php }?>
    <!-- input class="home_but_del" type="submit" name="delete" value="" / -->
</div>
</form>
</div>

<script>
$('input.selectAll').click(function() {
	var number = this.value;
	if($(this).is(':checked')) {
		$(this).parent().parent().siblings('.data').each(function () {
			$(this).find('input').eq(number).attr('checked', 'checked');
		});
	} else {
		$(this).parent().parent().siblings('.data').each(function () {
			$(this).find('input').eq(number).removeAttr('checked');
		});
	}
});

function initCheckboxes() {
	$('span.table-name').click(function () {
		var tr = $(this).parent().parent();
		var checked = tr.find('input:checked');
		var check = false;
		var tableID = $(this).attr('title');
		var changeTableAccess = false;
		var isSectionSelector = $(this).parent().parent().hasClass('section');
		if(!isNaN(tableID) || isSectionSelector) {
			if(isSectionSelector) {
				changeTableAccess = confirm("Изменить доступ к соответствующим таблицам?"); 
			} else {
				var tableName = $('#t' + tableID + ' span.table-name').text();
				changeTableAccess = confirm("Изменить доступ к соответствующей таблице \"" + tableName + '"');
			}
		}
		if(checked.length == 3) {
			tr.find('input').removeAttr('checked');
			if(changeTableAccess) {
				$('#t' + tableID).find('input').removeAttr('checked');
			}
			check = false;
		} else {
			tr.find('input').attr('checked', 'checked');
			if(changeTableAccess) {
				$('#t' + tableID).find('input').attr('checked', 'checked');
			}
			check = true;
		}
		
		if(isSectionSelector) {
			var sectionID = tr.attr('id').replace('s', '');
			if(check) {
				$('tr[rel="' + sectionID + '"]').find('input').attr('checked', 'checked');
			} else {
				$('tr[rel="' + sectionID + '"]').find('input').removeAttr('checked');
			}
			
			if(changeTableAccess) {
				var spans = $('tr[rel="' + sectionID + '"]').find('span.table-name').each(function() {
					var tableID = $(this).attr('title');
					if(check) {				
						$('#t' + tableID).find('input').attr('checked', 'checked');
					} else {
						$('#t' + tableID).find('input').removeAttr('checked');
					}
					
				});
			}
			
		}
		
	});
}

	$('#GroupSelect').change(function () {
		var id = this.value;
		var url = window.location.pathname.replace(/user\/\d+/, 'group_access/' + id);
		$.get(url, function (html) {
			$('#AccessTables').html(html);
			initCheckboxes();
		});
	});
initCheckboxes();
</script>
<?php }} ?>