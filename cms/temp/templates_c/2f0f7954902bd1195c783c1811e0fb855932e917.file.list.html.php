<?php /* Smarty version Smarty-3.1.8, created on 2015-08-07 14:35:21
         compiled from "/home/u24576/kresla-otido.ru/www/../cms/application/templates//modules/UserManager/list.html" */ ?>
<?php /*%%SmartyHeaderCode:83449670355c497f9695f43-23170271%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f0f7954902bd1195c783c1811e0fb855932e917' => 
    array (
      0 => '/home/u24576/kresla-otido.ru/www/../cms/application/templates//modules/UserManager/list.html',
      1 => 1422244238,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '83449670355c497f9695f43-23170271',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'groups' => 0,
    'baseURL' => 0,
    'group' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_55c497f9795cc4_21646611',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c497f9795cc4_21646611')) {function content_55c497f9795cc4_21646611($_smarty_tpl) {?>
<style>
ul.groupList {
	font: bold 18px Tahoma,Geneva,sans-serif;
}
ul.groupList > li {
	margin-bottom:18px;
}
ul.groupList > li > ul {margin-left:10px;}
ul.groupList > li > ul > li {
	list-style:circle inside none;
}
ul.groupList > li > ul > li.add_another {
	list-style-type: disc;
	color:#AAA;
}
ul.groupList > li > ul > li.add_another a {
	color:#AAA;
}
ul.userList li.superuser {
	color:red;
	list-style:disc inside none;
}
div.addGroup {
	margin-top:20px;
	font: bold 18px Tahoma,Geneva,sans-serif;
}
div.addGroup a{
	text-decoration: none;
	border-bottom:1px dashed #013466;
}
div.addGroupForm, .addUserForm {
	margin-top:4px;
	display:none;
}
</style>

<br clear="all" />
<ul class="groupList">
	<?php  $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['group']->key => $_smarty_tpl->tpl_vars['group']->value){
$_smarty_tpl->tpl_vars['group']->_loop = true;
?>
	<li>
		<a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
group/<?php echo $_smarty_tpl->tpl_vars['group']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['group']->value['name'];?>
</a>
			<ul class="userList">
			<?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['group']->value['_users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
?>
			<li <?php if ($_smarty_tpl->tpl_vars['user']->value['superuser']){?>class="superuser" title="Суперпользователь"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
user/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['user']->value['login'];?>
</a></li>
			<?php } ?>
			<li class="add_another"><a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
user/0/?group_id=<?php echo $_smarty_tpl->tpl_vars['group']->value['id'];?>
">Добавить пользователя</a></li>
			</ul>
	</li>
	<?php } ?>
</ul>
<div class="addGroup">
	<a href="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
group/0/" onclick="$('.addGroupForm').toggle();return false;">Добавить группу</a>
	<div class="addGroupForm"><form action="<?php echo $_smarty_tpl->tpl_vars['baseURL']->value;?>
group/0/" method="post">
	Название: <input type="text" name = "group[name]"/>
	<input type="submit" value="Сохранить" />
	</form></div>
</div><?php }} ?>