<?php /* Smarty version Smarty-3.1.8, created on 2014-06-06 10:59:32
         compiled from "/home/u24576/kresla-otido.ru/www/../cms/application/templates//operations/EditClusterMetaOperation/form.html" */ ?>
<?php /*%%SmartyHeaderCode:89063703539166d4935128-26107187%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9cefe5c7f971675b762d77820a8bb0bf8ad22340' => 
    array (
      0 => '/home/u24576/kresla-otido.ru/www/../cms/application/templates//operations/EditClusterMetaOperation/form.html',
      1 => 1383828594,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '89063703539166d4935128-26107187',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'formURL' => 0,
    'meta' => 0,
    'table' => 0,
    'objectsHTML' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_539166d49a17b8_70365037',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539166d49a17b8_70365037')) {function content_539166d49a17b8_70365037($_smarty_tpl) {?><div class="home_messages">
<form action="<?php echo $_smarty_tpl->tpl_vars['formURL']->value;?>
" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['meta']->value['id'];?>
" />
<table>
<tr>
	<td class="name">
			Кластер
	</td>
	<td class="form">
	<?php echo $_smarty_tpl->tpl_vars['table']->value->getFields('cluster_id')->renderEdit($_smarty_tpl->tpl_vars['meta']->value);?>

	</td>
</tr>
<tr>
	<td class="name">
			Страница кластера
	</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['table']->value->getFields('cluster_page_id')->renderEdit($_smarty_tpl->tpl_vars['meta']->value);?>
</td>
</tr>
<tr id="URL_TR">
	<td class="name">URL</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['objectsHTML']->value;?>
</td>
</tr>
<tr>
	<td class="name">
			Title
	</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['table']->value->getFields('title')->renderEdit($_smarty_tpl->tpl_vars['meta']->value);?>
</td>
</tr>
<tr>
	<td class="name">
			Keywords
	</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['table']->value->getFields('keywords')->renderEdit($_smarty_tpl->tpl_vars['meta']->value);?>
</td>
</tr>
<tr>
	<td class="name">
			Description
	</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['table']->value->getFields('description')->renderEdit($_smarty_tpl->tpl_vars['meta']->value);?>
</td>
</tr>
<tr>
	<td class="name">
			H1
	</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['table']->value->getFields('h1')->renderEdit($_smarty_tpl->tpl_vars['meta']->value);?>
</td>
</tr>
<tr>
	<td class="name">
			Text
	</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['table']->value->getFields('text')->renderEdit($_smarty_tpl->tpl_vars['meta']->value);?>
</td>
</tr>
</table>
<div class="sep">
    <input class="home_but_save" type="submit" value="" />
     или <input type="submit" name="delete" value="Удалить" onclick="return confirm('Вы действительно хотите удалить запись?');"/>    <!-- input class="home_but_del" type="submit" name="delete" value="" / -->
</div>
</form>
</div>
<script>

$('select[name="cluster_id"]').change(function () {
	$.get(window.location.pathname, {action_name: "get_objects", cluster_id: $(this).val()}, function (html) {
		$('#URL_TR td.form').html(html);
	});
});

</script><?php }} ?>