<?php /* Smarty version Smarty-3.1.8, created on 2014-12-09 16:54:50
         compiled from "/var/www/k.lo/www/../cms/application/templates//operations/EditOperation/form.html" */ ?>
<?php /*%%SmartyHeaderCode:2666647345486ff2ae35271-60691917%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '885c18945bde17fe5a2278544d3d278fe463c1e8' => 
    array (
      0 => '/var/www/k.lo/www/../cms/application/templates//operations/EditOperation/form.html',
      1 => 1418120426,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2666647345486ff2ae35271-60691917',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'fieldDataError' => 0,
    'saved' => 0,
    'action' => 0,
    'data' => 0,
    '_return_path' => 0,
    'fields' => 0,
    'field' => 0,
    '_afterSaveAction' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5486ff2af17562_51427300',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5486ff2af17562_51427300')) {function content_5486ff2af17562_51427300($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['fieldDataError']->value){?><div class="home_eror" style="margin-bottom:10px;"><?php echo $_smarty_tpl->tpl_vars['fieldDataError']->value;?>
</div><?php }?>
<?php if ($_smarty_tpl->tpl_vars['saved']->value){?><div class="home_success" style="margin-bottom:10px;">Сохранено</div><?php }?>

<div class="home_messages">
<form action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" />
<input type="hidden" name="_return_path" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_return_path']->value, ENT_QUOTES, 'utf-8', true);?>
" />
<table>
<?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value){
$_smarty_tpl->tpl_vars['field']->_loop = true;
?>
<?php if ($_smarty_tpl->tpl_vars['field']->value->isEditable()){?>
<tr>
	<td class="name">
	<?php if ($_smarty_tpl->tpl_vars['field']->value->isURLField()){?><img class="url_field_icon" src="/cms/images/url_field.png" alt="URL-поле" title="Это поле является URL-полем. Допустимы только латинские буквы, цифры, минус и знак подчеркивания. Должно быть уникальным. "/><?php }?>
		<?php echo $_smarty_tpl->tpl_vars['field']->value->getViewname();?>

	</td>
	<td class="form"><?php echo $_smarty_tpl->tpl_vars['field']->value->renderEdit($_smarty_tpl->tpl_vars['data']->value);?>
<?php if ($_smarty_tpl->tpl_vars['field']->value->getHint()){?><div class="field_hint"><?php echo $_smarty_tpl->tpl_vars['field']->value->getHint();?>
</div><?php }?></td>
</tr>
<?php }?>
<?php } ?>
<tr>
	<td class="name"><nobr><b>После сохранения</b></nobr></td>
	<td class="form">
	<select name="_afterSaveAction">
		<option value="edit" <?php if ($_smarty_tpl->tpl_vars['_afterSaveAction']->value=="edit"){?>selected="selected"<?php }?>>Редактировать запись</option>
		<option value="add" <?php if ($_smarty_tpl->tpl_vars['_afterSaveAction']->value=="add"){?>selected="selected"<?php }?>>Добавить ещё</option>
		<option value="add_the_same" <?php if ($_smarty_tpl->tpl_vars['_afterSaveAction']->value=="add_the_same"){?>selected="selected"<?php }?>>Добавить ещё такую же</option>
		<option value="show_table" <?php if ($_smarty_tpl->tpl_vars['_afterSaveAction']->value=="show_table"){?>selected="selected"<?php }?>>Показать таблицу</option>
	</select>
	</td>
</tr>
</table>
<div class="sep">
    <input class="home_but_save" type="submit" value="" />
    <?php if ($_smarty_tpl->tpl_vars['data']->value['id']){?> или <input type="submit" name="delete" value="Удалить" onclick="return confirm('Вы действительно хотите удалить запись?');"/><?php }?>
    <!-- input class="home_but_del" type="submit" name="delete" value="" / -->
</div>
</form>
</div><?php }} ?>