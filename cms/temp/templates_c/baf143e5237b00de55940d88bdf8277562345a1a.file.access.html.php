<?php /* Smarty version Smarty-3.1.8, created on 2015-08-07 14:35:26
         compiled from "/home/u24576/kresla-otido.ru/www/../cms/application/templates//modules/UserManager/access.html" */ ?>
<?php /*%%SmartyHeaderCode:182318673055c497fe76e945-96350594%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'baf143e5237b00de55940d88bdf8277562345a1a' => 
    array (
      0 => '/home/u24576/kresla-otido.ru/www/../cms/application/templates//modules/UserManager/access.html',
      1 => 1422244238,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '182318673055c497fe76e945-96350594',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'projectAccessTypes' => 0,
    'tables' => 0,
    'table' => 0,
    'access' => 0,
    'group' => 0,
    'tablePairs' => 0,
    'areas' => 0,
    'section' => 0,
    'sectionAccess' => 0,
    'page' => 0,
    'pagesAccess' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_55c497fe86fe16_95272449',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c497fe86fe16_95272449')) {function content_55c497fe86fe16_95272449($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/home/u24576/kresla-otido.ru/cms/library/Smarty/plugins/function.html_options.php';
?><?php if (isset($_smarty_tpl->tpl_vars['projectAccessTypes']->value)){?><?php echo $_smarty_tpl->getSubTemplate ("access-projects.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }?>
<table>
<tr valign="top">
	<td>
		<h3>Доступ к объектам:</h3>
		<table class="access_table">
		<tr>
			<th></th>
			<th width="40">Чтн</th>
			<th width="40">Зап</th>
			<th width="40">Удал.</th>
		</tr>
		<tr>
			<th></th>
			<th width="40"><input class="selectAll" type="checkbox" value="0"/></th>
			<th width="40"><input class="selectAll" type="checkbox" value="1"/></th>
			<th width="40"><input class="selectAll" type="checkbox" value="2"/></th>
		</tr>
		<?php  $_smarty_tpl->tpl_vars['table'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['table']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tables']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['table']->key => $_smarty_tpl->tpl_vars['table']->value){
$_smarty_tpl->tpl_vars['table']->_loop = true;
?>
			<tr class="data" id="t<?php echo $_smarty_tpl->tpl_vars['table']->value['id'];?>
">
			<td ><span class="table-name"><?php echo $_smarty_tpl->tpl_vars['table']->value['name'];?>
</span></td>
			<td align="center"><input type="checkbox" name="access[<?php echo $_smarty_tpl->tpl_vars['table']->value['id'];?>
][read]" <?php if ($_smarty_tpl->tpl_vars['access']->value[$_smarty_tpl->tpl_vars['table']->value['id']]['read']){?>checked="checked"<?php }?>/></td>
			<td align="center"><input type="checkbox" name="access[<?php echo $_smarty_tpl->tpl_vars['table']->value['id'];?>
][write]" <?php if ($_smarty_tpl->tpl_vars['access']->value[$_smarty_tpl->tpl_vars['table']->value['id']]['write']){?>checked="checked"<?php }?>/></td>
			<td align="center"><input type="checkbox" name="access[<?php echo $_smarty_tpl->tpl_vars['table']->value['id'];?>
][delete]" <?php if ($_smarty_tpl->tpl_vars['access']->value[$_smarty_tpl->tpl_vars['table']->value['id']]['delete']){?>checked="checked"<?php }?>/></td>
			</tr>
		<?php } ?>
		</table>
		<?php if (isset($_smarty_tpl->tpl_vars['group']->value)){?>
		<div class="field_access">
			<h3>Запретить доступ к полям:</h3>
			<ul><?php echo $_smarty_tpl->getSubTemplate ("denied-fields.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
</ul>
			<div id="AddFieldContainer">
				<select name="field[table_id]" onchange="loadTableFields(this);">
					<option value="">Выбрать</option>
					<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['tablePairs']->value),$_smarty_tpl);?>

				</select>
				<br /><button onclick="denyField(this); return false;">Запретить</button>
			</div>
		</div>
		<?php }?>
	</td>
	<td>
		<h3>Доступ к Разделам и страницам:</h3>
		<table class="access_table">
		<tr>
			<th></th>
			<th width="40">Чтн</th>
			<th width="40">Зап</th>
			<th width="40">Удал.</th>
		</tr>
		<tr>
			<th></th>
			<th width="40"><input class="selectAll" type="checkbox" value="0"/></th>
			<th width="40"><input class="selectAll" type="checkbox" value="1"/></th>
			<th width="40"><input class="selectAll" type="checkbox" value="2"/></th>
		</tr>
		<?php  $_smarty_tpl->tpl_vars['section'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['section']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['areas']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['section']->key => $_smarty_tpl->tpl_vars['section']->value){
$_smarty_tpl->tpl_vars['section']->_loop = true;
?>
			<tr class="data section" id="s<?php echo $_smarty_tpl->tpl_vars['section']->value['id'];?>
">
			<td ><span class="table-name"><strong><?php echo $_smarty_tpl->tpl_vars['section']->value['name'];?>
</strong></span></td>
			<td align="center"><input type="checkbox" name="section_access[<?php echo $_smarty_tpl->tpl_vars['section']->value['id'];?>
][read]" <?php if ($_smarty_tpl->tpl_vars['sectionAccess']->value[$_smarty_tpl->tpl_vars['section']->value['id']]['read']){?>checked="checked"<?php }?>/></td>
			<td align="center"><input type="checkbox" name="section_access[<?php echo $_smarty_tpl->tpl_vars['section']->value['id'];?>
][write]" <?php if ($_smarty_tpl->tpl_vars['sectionAccess']->value[$_smarty_tpl->tpl_vars['section']->value['id']]['write']){?>checked="checked"<?php }?>/></td>
			<td align="center"><input type="checkbox" name="section_access[<?php echo $_smarty_tpl->tpl_vars['section']->value['id'];?>
][delete]" <?php if ($_smarty_tpl->tpl_vars['sectionAccess']->value[$_smarty_tpl->tpl_vars['section']->value['id']]['delete']){?>checked="checked"<?php }?>/></td>
			</tr>
			<?php if (isset($_smarty_tpl->tpl_vars['section']->value['pages'])){?><?php  $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['page']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['section']->value['pages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['page']->key => $_smarty_tpl->tpl_vars['page']->value){
$_smarty_tpl->tpl_vars['page']->_loop = true;
?>
			<tr class="data" rel="<?php echo $_smarty_tpl->tpl_vars['section']->value['id'];?>
">
			<td > -- <span class="table-name" title="<?php echo $_smarty_tpl->tpl_vars['page']->value['obj_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value['viewname'];?>
</span></td>
			<td align="center"><input type="checkbox" name="page_access[<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
][read]" <?php if ($_smarty_tpl->tpl_vars['pagesAccess']->value[$_smarty_tpl->tpl_vars['page']->value['id']]['read']){?>checked="checked"<?php }?>/></td>
			<td align="center"><input type="checkbox" name="page_access[<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
][write]" <?php if ($_smarty_tpl->tpl_vars['pagesAccess']->value[$_smarty_tpl->tpl_vars['page']->value['id']]['write']){?>checked="checked"<?php }?>/></td>
			<td align="center"><input type="checkbox" name="page_access[<?php echo $_smarty_tpl->tpl_vars['page']->value['id'];?>
][delete]" <?php if ($_smarty_tpl->tpl_vars['pagesAccess']->value[$_smarty_tpl->tpl_vars['page']->value['id']]['delete']){?>checked="checked"<?php }?>/></td>
			</tr>
			<?php } ?><?php }?>
		<?php } ?>
		</table>
	</td>
</tr>
</table><?php }} ?>