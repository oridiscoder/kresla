SET NAMES UTF8;
INSERT INTO `bof_groups` (`id`, `name`, `number`, `parent_id`, `hidden`) VALUES 
(1, 'Администраторы', '1', '0', '0');
INSERT INTO `bof_users` (`id`, `login`, `password`, `number`, `group_id`, `hidden`, `superuser`) VALUES 
(1, 'super', MD5('supersonic'), '1', '1','0', '1');

INSERT INTO `bof_razdels` (`id`, `name`, `number`, `hidden`, `url`, `gui`) VALUES 
(1, 'Система', '2', '0', NULL, NULL), 
(2, 'Сайт', '1', '0', NULL, NULL);

INSERT INTO `bof_fieldtypes` (`id`, `name`, `value`, `number`, `hidden`) VALUES
(1, 'Строка', 1, 15, 0),
(2, 'Пароль', 2, 14, 0),
(3, 'Текст', 3, 13, 0),
(4, 'Дата', 4, 12, 0),
(5, 'Ссылка', 5, 11, 0),
(6, 'E-mail', 6, 10, 0),
(7, 'Галочка', 7, 9, 0),
(8, 'HTML', 8, 8, 0),
(11, 'Потомок', 11, 7, 0),
(12, 'Перечень', 12, 6, 0),
(15, 'Неявный потомок', 15, 3, 0),
(16, 'Виртуальный столбец', 16, 5, 0),
(17, 'Виртуальный потомок', 17, 4, 0),
(18, 'Файл', 18, 2, 0),
(19, 'Изображение', 19, 1, 0),
(20, 'Целое число', 20, 16, 0);

INSERT INTO `bof_objects` (`id`, `name`, `viewname`, `parent_id`, `number`, `hidden`, `sort_asc`) VALUES 
(1, 'bof_objects', 		'Объекты', 				'0', 	'1', '0', '0'),
(2, 'bof_enum_types', 	'Типы перечислений', 	'0', 	'2', '0', '0'), 
(3, 'bof_fields', 		'Поля', 				'1', 	'3', '0', '0'), 
(4, 'bof_enums', 		'Перечисления', 		'2', 	'4', '0', '0'), 
(5, 'bof_fieldtypes', 	'Типы полей', 			'3', 	'5', '0', '0'), 
(6, 'bof_razdels', 		'Разделы', 				'0', 	'6', '0', '0'), 
(7, 'bof_pages', 		'Страницы', 			'0', 	'7', '0', '0'), 
(8, 'bof_groups', 		'Пользов. группы', 		'0', 	'8', '0', '0'),
(9, 'bof_users', 		'Пользователи', 		'8', 	'9', '0', '0'),
(10,'bof_default_section_page', 'Страницы по умолчанию','6','10','0','0'), 
(11,'bof_events', 		'События',				'0',	'11','0', '0'), 
(12,'cms_pages', 		'Страницы',				'0',	'12','0', '0'),
(13,'clusters', 		'Кластеры',				'0',	'13','0', '0'),
(14,'clusters_pages', 	'Страницы кластеров',	'14',	'15','0', '0'), 
(15,'bof_users_logins', 'Логи авторизации',		'0',	'16','0', '0'),
(16,'cms_modules', 		'Модули',				'0',	'17','0', '0'),
(17,'cms_modules_config', 'Настройки модулей',	'16',	'18','0', '0');

INSERT INTO `bof_pages` (`id`, `viewname`, `parent_id`, `type`, `razdel`, `obj_id`, `name`, `number`, `hidden`) 
VALUES 
(1, 'Объекты', 				'0', '2', '1', '1', 'bof_objects', 		'1', '0'),
(2, 'Типы перечислений', 	'0', '2', '1', '2', 'bof_enum_types',	'2', '0'), 
(3, 'Поля объектов', 		'1', '2', '1', '3', 'bof_fields', 		'3', '0'), 
(4, 'Перечисления', 		'2', '2', '1', '4', 'bof_enums', 		'4', '0'),
(5, 'Типы полей', 			'3', '2', '1', '5', 'bof_fieldtypes', 	'5', '0'),
(6, 'Разделы', 				'0', '2', '1', '6', 'bof_razdels', 		'6', '0'),
(7, 'Страницы', 			'6', '2', '1', '7', 'bof_pages', 		'7', '0'),
(8, 'Пользов. группы', 		'0', '2', '1', '8', 'bof_groups', 		'8', '0'),
(9, 'Пользователи', 		'8', '2', '1', '9',	'bof_users', 		'9', '0'),
(10,'Страницы по умолчанию','6', '2', '1', '10','bof_default_section_page','10', '0'), 
(11, 'События', 			'0', '2', '1', '11','bof_events', 		'11', '0'),
(12, 'Страницы', 			'0', '2', '2', '12','cms_pages', 		'12', '0'),
(13, 'Кластеры', 			'0', '2', '1', '13','clusters', 		'13', '0'),
(14, 'Страницы кластеров', 	'13','2', '1', '14','clusters_pages', 	'14', '0'),
(15, 'Логи авторизации', 	'9', '2', '1', '15','bof_users_logins', '15', '0'),
(16, 'Модули', 				'0', '3', '1', '16','Modules', 			'16', '0');


INSERT INTO `bof_fields` 
(`id`, `name`, `viewname`,  `hint`, `obj_id`, `type`, `param`, `param2`, `filter`, `hidecolumn`, `align`, `number`,`hidden`, `sort_column`) 
VALUES
(1, 'name', 		'name', 		'', 1, 1, 	null,	null, null,	0, null, 1, 0, null),
(2, 'viewname', 	'viewname', 	'', 1, 1, 	null,	null, null,	0, null, 2, 0, null),
(3, 'sort_asc', 	'По возраст.', 	'', 1, 7, 	null,	null, null,	0, null, 3, 0, null),
(4, 'name', 		'name', 		'', 3, 1, 	null,	null, null,	0, null, 4, 0, null),
(5, 'viewname', 	'viewname', 	'', 3, 1, 	null,	null, null, 0, null, 5, 0, null),
(6, 'hint', 		'Подсказка', 	'', 3, 1, 	null,	null, null, 1, null, 6, 0, null),
(7, 'obj_id', 		'Таблица', 		'', 3, 11,	1,		null, null, 0, null, 7, 0, null),
(8, 'type',			'Тип поля', 	'', 3, 11, 	5, 		null, null, 0, null, 8, 0, null),
(9, 'param', 		'Параметр 1', 	'', 3, 1, 	null, 	null, null, 0, null, 9, 0, null),
(10,'param2', 		'Параметр 2', 	'', 3, 1, 	null, 	null, null, 1, null,10, 0, null),
(11,'filter', 		'SQL фильтр', 	'', 3, 1, 	null, 	null, null, 0, null,11, 0, null),
(12,'hidecolumn',	'Спрятать', 	'', 3, 7, 	null, 	null, null, 0, null,12, 0, null),
(13,'align', 		'Выравнивание', '', 3, 12, 	1, 		null, null, 0, null,13, 0, null), 
(14,'name', 		'Название', 	'', 2, 1, 	null, 	null, null, 0, null,14, 0, null), 
(15,'name', 		'Название', 	'', 4, 1, 	null, 	null, null, 0, null,15, 0, null),
(16,'enum_id', 		'Категория', 	'', 4, 11, 	2, 'NOTNULL', null, 0, null,16, 0, null), 
(17,'name', 		'name', 		'', 5, 1, 	null, 	null, null, 0, null,17, 0, null), 
(18,'name', 		'name', 		'', 6, 1, 	null, 	null, null, 0, null,18, 0, null),
(19,'url', 			'ЧПУ', 			'', 6, 1, 	null, 	null, null, 0, null,19, 0, null),
(20,'viewname', 	'viewname', 	'', 7, 1, 	null, 	null, null, 0, null,20, 0, null),
(21,'name', 		'Название', 	'', 7, 1, 	null, 	null, null, 0, null,21, 0, null),
(22,'razdel', 		'Раздел', 		'', 7, 11, 	6, 		null, null, 0, null,22, 0, null),
(23,'type', 		'Тип', 			'', 7, 1, 	null, 	null, null, 0, null,23, 0, null),
(24,'obj_id', 		'Таблица', 		'', 7, 11, 	1, 		null, null, 0, null,24, 0, null),
(25,'parent_id',	'parent_id', 	'', 7, 11, 	7, 		null, null, 0, null,25, 0, null),
(26,'name',			'Название', 	'', 8, 1, 	null, 	null, null, 0, null,26, 0, null),
(27,'login',		'Логин', 		'', 9, 1, 	null, 	null, null, 0, null,27, 0, null),
(28,'password',		'Пароль', 		'', 9, 2, 	null, 	null, null, 0, null,28, 0, null),
(29,'group_id',		'Группа', 		'', 9, 11, 	8, 		null, null, 0, null,29, 0, null), 
(30,'section_id',	'Раздел', 		'', 10,11,	6, 'NOTNULL', null, 0, null,30, 0, null),
(31,'page_id',		'Страница', 	'', 10,11, 	7, 'NOTNULL', null, 0, null,31, 0, null),
(32,'user_id',		'Пользователь', '', 10,11, 	8, 'NOTNULL', null, 0, null,32, 0, null), 
(33,'date_start',	'Дата', 		'', 11,4, 	null,	null, null, 0, null,33, 0, null),
(34,'user_id',		'Пользователь', '', 11,11, 	8, 		null, null, 0, null,34, 0, null),
(35,'object_id',	'Объект', 		'', 11,1, 	null,	null, null, 0, null,35, 0, null),
(36,'title',		'Заголовок', 	'', 11,1, 	null, 	null, null, 0, null,36, 0, null),
(37,'message',		'Описание', 	'', 11,1, 	null, 	null, null, 0, null,37, 0, null), 
(38,'name',			'Имя', 			'Имя страницы на латинице, будет использоваться для построения URL адреса', 
										12,1, 	null, 	null, null, 0, null,38, 0, null),
(39,'parent_id',	'Родительская', '', 12,11, 	12, 	null, null, 0, null,39, 0, null),
(40,'title',		'Title', 		'', 12,1, 	null, 	null, null, 0, null,40, 0, null),
(41,'keywords',		'Keywords', 	'', 12,1, 	null, 	null, null, 0, null,41, 0, null),
(42,'description',	'Description', 	'', 12,1, 	null, 	null, null, 0, null,42, 0, null),
(43,'content',		'Тело страницы','', 12,8, 	null, 	null, null, 0, null,43, 0, null),
(44,'in_menu',		'Отобр. в меню','', 12,7, 	null, 	null, null, 0, null,44, 0, null),
(45,'menu_name',	'Название в меню','',12,1, 	null, 	null, null, 0, null,45, 0, null),
(46,'active',		'Активна',		'', 12,7, 	null, 	null, null, 0, null,46, 0, null),
(47,'template',		'Файл шаблона', '', 12,1, 	null, 	null, null, 0, null,47, 0, null),
(48,'name',			'Название', 	'', 13,1, 	null, 	null, null, 0, null,48, 0, null),
(49,'url_prefix',	'Префикс', 		'', 13,1, 	null, 	null, null, 0, null,49, 0, null),
(50,'object_1',		'Объект 1', 	'', 13,11, 	1, 		null, null, 0, null,50, 0, null),
(51,'object_2',		'Объект 2', 	'', 13,11, 	1, 		null, null, 0, null,51, 0, null),
(52,'object_3',		'Объект 3', 	'', 13,11, 	1, 		null, null, 0, null,52, 0, null),
(53,'target_id',	'Объект данных','', 13,11, 	1, 		null, null, 0, null,53, 0, null),
(54,'module_name',	'Класс-обработчик','',13,1, null,	null, null, 0, null,54, 0, null),
(55,'parent_id',	'Родительский кластер','',13,11, 13,null, null, 0, null,55, 0, null), 
(56,'name',			'URL',			'',14,1,	null,	null, null, 0, null,56, 0, null),
(57,'title',		'Название',		'',14,1,	null,	null, null, 0, null,57, 0, null),
(58,'cluster_id',	'Кластер',		'',14,11,	13,		null, null, 0, null,58, 0, null),
(59,'module_name',	'Класс-обработчик','',14,1, null,	null, null, 0, null,59, 0, null),
(60,'sort_col', 	'Столбец сортировки','', 1, 1, 	null,	null, null,	0, null, 60, 0, null),
(61,'is_section', 	'Является разделом',
	'Остановить поиск вложенных страниц. Эта страница должна содержать обработчик, который сам анализирует URL и решает, есть ли вложенная страница или нет', 
	12, 7, 	null,	null, null,	0, null, 61, 0, null),
(62, 'ip',			'ip', 		'',	15, 1,	null,null, null,0, null, 62, 0, null),
(63, 'date',		'Дата',		'',	15, 4,	null,'%Y-%m-%d %H:%M:%S', null,0, null, 63, 0, null),
(64, 'login',		'Логин',	'',	15, 1,	null,null, null,0, null, 64, 0, null),
(65, 'success',		'Успешно',	'',	15, 7,	null,null, null,0, null, 65, 0, null), 
(66, 'name',		'Название',	'',	16, 1,	null,null, null,0, null, 66, 0, null),
(67, 'active',		'Включен',	'',	16, 7,	null,null, null,0, null, 67, 0, null);

(68, 'module_name',	'Название модуля',	'',	17, 1,	null,null, null,0, null, 68, 0, null),
(69, 'key',			'Ключ',				'',	17, 1,	null,null, null,0, null, 69, 0, null),
(70, 'value',		'Значение',			'',	17, 1,	null,null, null,0, null, 70, 0, null);



INSERT INTO `bof_enum_types` (`id`, `name`, `number`, `hidden`) VALUES
(1, 'Выравнивание', 1, 0);


INSERT INTO `bof_enums` (`id`, `enum_id`, `name`, `number`, `hidden`) VALUES
(242, 1, 'По левому краю',	1, 0),
(243, 1, 'По центру', 		2, 0),
(244, 1, 'По правому краю', 3, 0)

