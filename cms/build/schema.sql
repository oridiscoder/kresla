DROP TABLE IF EXISTS `bof_users`;
CREATE TABLE `bof_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  `superuser` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` DATETIME DEFAULT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_groups`;
CREATE TABLE `bof_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;


DROP TABLE IF EXISTS `bof_razdels`;
CREATE TABLE `bof_razdels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) DEFAULT NULL, 
  `gui` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM   DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_pages`;
CREATE TABLE `bof_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viewname` varchar(100) NOT NULL DEFAULT '',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `razdel` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_objects`;
CREATE TABLE `bof_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `viewname` varchar(128) NOT NULL DEFAULT '',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  `sort_asc` tinyint(1) NOT NULL DEFAULT '0',
  `sort_col` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_fields`;
CREATE TABLE `bof_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `align` varchar(255) NOT NULL,
  `name` varchar(64)  NOT NULL DEFAULT '',
  `viewname` varchar(128)  NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  `param` varchar(32)  DEFAULT NULL,
  `param2` varchar(255) DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  `hidecolumn` int(11) NOT NULL DEFAULT '0',
  `obj_id` int(11) NOT NULL DEFAULT '0',
  `filter` varchar(255)NOT NULL,
  `hidden` int(11) NOT NULL DEFAULT '0',
  `sort_column` varchar(255) DEFAULT NULL,
  `hint` varchar(255) DEFAULT NULL,
  `is_url_field` TINYINT(1) NOT NULL DEFAULT 0, 
  PRIMARY KEY (`id`),
  KEY `obj_id` (`obj_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_fieldtypes`;
CREATE TABLE `bof_fieldtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_enums`;
CREATE TABLE `bof_enums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enum_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_enum_types`;
CREATE TABLE `bof_enum_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_users_access`;
CREATE TABLE `bof_users_access` (
  `user_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `write` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('object','page','section') NOT NULL DEFAULT 'object',
  PRIMARY KEY (`type`,`user_id`,`obj_id`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `access_fields`;
CREATE TABLE `access_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `D` int(11) NOT NULL,
  `U` int(11) NOT NULL,
  `I` int(11) NOT NULL,
  `S` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_informers`;
CREATE TABLE `bof_informers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `hidden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

CREATE TABLE IF NOT EXISTS `bof_informers2group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `informer_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_informers2user`;
CREATE TABLE `bof_informers2user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `informer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `hidden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_default_section_page`;
CREATE TABLE `bof_default_section_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `section_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_events`;
CREATE TABLE `bof_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_start` datetime DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL , 
  `object_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Index_date_start` (`date_start`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `cms_pages`;
CREATE TABLE `cms_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL, 
  `title` VARCHAR(255) DEFAULT NULL, 
  `keywords` VARCHAR(255) DEFAULT NULL, 
  `description` VARCHAR(255) DEFAULT NULL, 
  `content` TEXT DEFAULT NULL, 
  `in_menu` TINYINT(1) NOT NULL DEFAULT 1,
  `menu_name` VARCHAR(255) DEFAULT NULL, 
  `active` TINYINT(1) NOT NULL DEFAULT 1 , 
  `template` VARCHAR(255) DEFAULT NULL, 
  `is_section` TINYINT(1) DEFAULT 0, 
  `number` int(11) NOT NULL,
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Index_parent` (`parent_id`), 
  KEY `Index_by_name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `clusters`;
CREATE TABLE `clusters` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED NOT NULL DEFAULT 0, 
	`hidden` TINYINT(1) NOT NULL DEFAULT 0,
	`name` VARCHAR(255), 
	`url_prefix` VARCHAR(255), 
	`object_1` INT UNSIGNED,
	`object_1__table` INT UNSIGNED,  
	`object_2` INT UNSIGNED,
	`object_2__table` INT UNSIGNED,
	`object_3` INT UNSIGNED,
	`object_3__table` INT UNSIGNED,
	`target_id` INT UNSIGNED, 
	`module_name` VARCHAR(255), 
	`parent_id` INT UNSIGNED
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `clusters_pages`;
CREATE TABLE `clusters_pages` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED NOT NULL DEFAULT 0, 
	`hidden` TINYINT(1) NOT NULL DEFAULT 0, 
	`name` VARCHAR(255) NOT NULL,
	`title` VARCHAR(255),
	`cluster_id` INT NOT NULL, 
	`module_name` VARCHAR(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `bof_user_activity_log`;
CREATE TABLE `bof_user_activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `method` enum('POST','GET') DEFAULT NULL,
  `url` varchar(300) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS bof_users_logins;
CREATE TABLE bof_users_logins (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED NOT NULL DEFAULT 0, 
	`hidden` INT UNSIGNED NOT NULL DEFAULT 0, 
	`ip` VARCHAR(16),  
	`login` VARCHAR(255),
	`date` DATETIME, 
	`success` TINYINT(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `cms_modules`;
CREATE TABLE `cms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `cms_modules_config`;
CREATE TABLE `cms_modules_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `module_name` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`), 
  KEY (`module_name`)
) ENGINE=MyISAM DEFAULT CHARSET=UTF8;




