DROP TABLE IF EXISTS `bof_autotasks`;
CREATE TABLE `bof_autotasks` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED NOT NULL DEFAULT 0, 
	`hidden` TINYINT(1) NOT NULL DEFAULT 0, 
	`name` VARCHAR(255), 
	`description` TEXT DEFAULT NULL, 
	`project_type` INT UNSIGNED DEFAULT NULL, 
	`project_id` INT UNSIGNED DEFAULT NULL, 
	`worker_id` INT UNSIGNED DEFAULT NULL, 
	`role_name` VARCHAR(255) DEFAULT NULL, 
	`period_days` INT UNSIGNED DEFAULT 0, 
	`period_day_number` INT UNSIGNED DEFAULT 0,
	`deadline_plus` INT UNSIGNED DEFAULT 3, 
	`event_name` VARCHAR(255) DEFAULT NULL, 
	`user_id` INT UNSIGNED DEFAULT NULL, 
	`priority_id` INT UNSIGNED DEFAULT NULL, 
	`error_text` VARCHAR(500) DEFAULT NULL 
);

DROP TABLE IF EXISTS `bof_tasks_favorite`;
CREATE TABLE `bof_tasks_favorite` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`task_id` INT UNSIGNED NOT NULL, 
	`user_id` INT UNSIGNED NOT NULL, 
	KEY(`user_id`)
);
DROP TABLE IF EXISTS `yandex_regions`;
CREATE TABLE yandex_regions (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED DEFAULT 0, 
	`hidden` INT UNSIGNED DEFAULT 0, 
	`name` VARCHAR(255), 
	`code` INT UNSIGNED
);

INSERT INTO `yandex_regions` (`name`, `code`, `number`) VALUES
('������', 213, 1),
('�����-���������', 2, 2), 
('������', 225, 3);

SET @obj_start_id = 151;
SET @obj_start_number = 151;
SET @pages_start_id = 215;
SET @pages_start_number = 215;
SET @field_start_number = 842;

INSERT INTO `bof_objects` (`id`, `name`, `viewname`, `parent_id`, `number`, `hidden`, `sort_asc`) VALUES 
(@obj_start_id, 'bof_autotasks', '�������', '0', @obj_start_number, '0', '0'), 
(@obj_start_id + 1, 'bof_projects_types', '��� �������', '0', @obj_start_number + 1, '0', '0'),
(@obj_start_id + 2, 'yandex_regions', '��� �������', '0', @obj_start_number + 2, '0', '0');

INSERT INTO `bof_pages` (`id`, `viewname`, `parent_id`, `type`, `razdel`, `obj_id`, `name`, `number`, `hidden`) 
VALUES 
(@pages_start_id, '���������', '0', '2', '2',@obj_start_id, '', @pages_start_number, '0'),
(@pages_start_id + 1, '������� �������', '0', '2', '6',@obj_start_id + 2, '', @pages_start_number, '0');

INSERT INTO `bof_fields` 
(`name`, `viewname`,  `hint`, `obj_id`, `type`, `param`, `param2`, `filter`, `hidecolumn`, `align`, `number`,`hidden`, `sort_column`) 
VALUES
('name','��������', '', @obj_start_id, 1,	null,null, null,0, null, @field_start_number, 0, null),
('description','��������', '', @obj_start_id, 3,	null,null, null,0, null, @field_start_number + 1, 0, null),
('project_type','��� �������', '', @obj_start_id, 11,	@obj_start_id + 1,null, null,0, null, @field_start_number + 2, 0, null),
('project_id','������', '', @obj_start_id, 11,87,null, null,0, null, @field_start_number + 3, 0, null),
('worker_id','�������������', '', @obj_start_id, 11,5,null, null,0, null, @field_start_number + 4, 0, null),
('role_name','���� � �������', 'optimizator, manager', @obj_start_id, 1,null,null, null,0, null, @field_start_number + 5, 0, null),
('period_days','�������� ����������', '���������� ���� ', @obj_start_id, 1,null,null, null,0, null, @field_start_number + 6, 0, null),
('period_day_number','���� ������', '��������� ������ ����� � ��������� ����', @obj_start_id, 1,null,null, null,0, null, @field_start_number + 7, 0, null),
('deadline_plus','������� +', '���������� ����, ��������� �� ���������� �������', @obj_start_id, 1,null,null, null,0, null, @field_start_number + 8, 0, null),
('event_name','�������� �������', 'project_create, project_close', @obj_start_id, 1,null,null, null,0, null, @field_start_number + 9, 0, null), 
('priority_id','��������', '', @obj_start_id, 12,41,null, null,0, null, @field_start_number + 10, 0, null),
('error_text','������', '����������� �������������', @obj_start_id, 3,null,null, null,0, null, @field_start_number + 11, 0, null),
#�������� ��� ����� ��������
('name','��������', '', @obj_start_id + 1, 1,null,null, null,0, null, @field_start_number + 12, 0, null), 
#������� �������
('name','��������', '', @obj_start_id + 2, 1,null,null, null,0, null, @field_start_number + 13, 0, null),
('code','���', '��� ������� � �������', @obj_start_id + 2, 1,null,null, null,0, null, @field_start_number + 13, 0, null), 
#���. ���� ��� ��������
('anchors','������', '', 21, 3,null,null, null,0, null, @field_start_number + 14, 0, null), 
('region_id','������', '', 21, 11, @obj_start_id + 2,null, null,0, null, @field_start_number + 15, 0, null);


ALTER TABLE bof_projects_types 
ADD COLUMN `number` INT UNSIGNED DEFAULT 0, 
ADD COLUMN `hidden` TINYINT(1) DEFAULT 0;

ALTER TABLE bof_tasks 
ADD COLUMN `autotask_id` INT UNSIGNED DEFAULT NULL;

ALTER TABLE bof_tasks CHANGE COLUMN `date_start` `date_start` DATETIME;
ALTER TABLE bof_tasks ADD COLUMN `date_create` DATETIME DEFAULT NULL AFTER `date_start`;

ALTER TABLE queryes ADD COLUMN `anchors` TEXT;
ALTER TABLE queryes ADD COLUMN `region_id` INT UNSIGNED DEFAULT NULL;

UPDATE `queryes` SET url = REPLACE(url, 'http://', '');


#15 ����� 2013
DROP TABLE IF EXISTS `mailer_tasks_logs`;
CREATE TABLE IF NOT EXISTS `mailer_tasks_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `task_id` int(11) DEFAULT NULL,
  `text` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`), 
  KEY(`task_id`, `number`), 
  KEY(`number`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251;

ALTER TABLE `mailer_transports` ADD COLUMN `stability` DOUBLE DEFAULT 0;
ALTER TABLE `mailer_transports` ADD COLUMN `sent_total` INT UNSIGNED DEFAULT 0;
ALTER TABLE `mailer_transports` ADD COLUMN `sent_success` INT UNSIGNED DEFAULT 0;

SET @obj_start_id = 154;
SET @obj_start_number = 154;
SET @pages_start_id = 217;
SET @pages_start_number = 217;
SET @field_start_number = 858;

INSERT INTO `bof_objects` (`id`, `name`, `viewname`, `parent_id`, `number`, `hidden`, `sort_asc`) VALUES 
(@obj_start_id, 'mailer_tasks_logs', '���� �����', '0', @obj_start_number, '0', '0');

INSERT INTO `bof_pages` (`id`, `viewname`, `parent_id`, `type`, `razdel`, `obj_id`, `name`, `number`, `hidden`) 
VALUES 
(@pages_start_id, '���� �����', 209, '2', 12 ,@obj_start_id, '', @pages_start_number, '0');

INSERT INTO `bof_fields` 
(`name`, `viewname`,  `hint`, `obj_id`, `type`, `param`, `param2`, `filter`, `hidecolumn`, `align`, `number`,`hidden`, `sort_column`) 
VALUES
('task_id','������', '', @obj_start_id, 11,	146, null, null,0, null, @field_start_number, 0, null),
('date','����', '', @obj_start_id, 4,	null,null, null,0, null, @field_start_number + 1, 0, null),
('text','�����', '', @obj_start_id, 3,	null,null, null,0, null, @field_start_number + 2, 0, null),
('stability','������������', '������������ �������� ����� ����� ���� ���������',145, 1,	null,null, null,0, null, @field_start_number + 3, 0, null);

#21 ����� 2013 - ������� ���� �����������
DROP TABLE IF EXISTS bof_users_logins;
CREATE TABLE bof_users_logins (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED NOT NULL DEFAULT 0, 
	`hidden` INT UNSIGNED NOT NULL DEFAULT 0, 
	`ip` VARCHAR(16),  
	`login` VARCHAR(255),
	`date` DATETIME, 
	`success` TINYINT(1) DEFAULT 0
) ENGINE=MyISAM;

SET @obj_start_id = 155;
SET @obj_start_number = 155;
SET @pages_start_id = 218;
SET @pages_start_number = 218;
SET @field_start_number = 865;

INSERT INTO `bof_objects` (`id`, `name`, `viewname`, `parent_id`, `number`, `hidden`, `sort_asc`) VALUES 
(@obj_start_id, 'bof_users_logins', '���� �����������', '0', @obj_start_number, '0', '0');

INSERT INTO `bof_pages` (`id`, `viewname`, `parent_id`, `type`, `razdel`, `obj_id`, `name`, `number`, `hidden`, `gui`) 
VALUES 
(@pages_start_id, '���� �����������', 155, '2', 4 ,@obj_start_id, '', @pages_start_number, '0', '');

INSERT INTO `bof_fields` 
(`name`, `viewname`,  `hint`, `obj_id`, `type`, `param`, `param2`, `filter`, `hidecolumn`, `align`, `number`,`hidden`, `sort_column`) 
VALUES
('ip','ip', '', @obj_start_id, 1,		null, null, null,0, null, @field_start_number, 0, null),
('date','����', '', @obj_start_id, 4,	null,'%Y-%m-%d %H:%M:%S', null,0, null, @field_start_number + 1, 0, null),
('login','�����', '', @obj_start_id, 1,	null,null, null,0, null, @field_start_number + 2, 0, null),
('success','�������', '',@obj_start_id, 7,	null,null, null,0, null, @field_start_number + 3, 0, null);

#25 ����� 2013
INSERT INTO `bof_fieldtypes` (`id`, `name`, `value`, `number`, `hidden`) 
VALUES(20, '����� �����', 20, 16, 0);

#29 ����� ������� � ������
DROP TABLE IF EXISTS `bof_vacations`;
CREATE TABLE `bof_vacations`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`user_id` INT UNSIGNED NOT NULL, 
	`date_start` DATETIME, 
	`date_stop` DATETIME, 
	`comment` VARCHAR(500), 
	KEY(`date_stop`, `date_start`)
);

SELECT MAX(id) FROM bof_objects
UNION SELECT MAX(id) FROM bof_pages
UNION SELECT MAX(number) FROM bof_fields;


#15 ������ - ���� �������������
DROP TABLE IF EXISTS `bof_roles`;
CREATE TABLE `bof_roles` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED NOT NULL DEFAULT 0, 
	`hidden` TINYINT UNSIGNED NOT NULL DEFAULT 0, 
	`name` VARCHAR(300)
);
DROP TABLE IF EXISTS `bof_employee2role`;
CREATE TABLE `bof_employee2role` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	`number` INT UNSIGNED NOT NULL DEFAULT 0, 
	`hidden` TINYINT UNSIGNED NOT NULL DEFAULT 0, 
	`employee_id` INT UNSIGNED, 
	`role_id` INT UNSIGNED
);

SET @obj_start_id = 159;
SET @obj_start_number = 159;
SET @pages_start_id = 227;
SET @pages_start_number = 227;
SET @field_start_number = 897;

INSERT INTO `bof_objects` (`id`, `name`, `viewname`, `parent_id`, `number`, `hidden`, `sort_asc`) VALUES 
(@obj_start_id, 'bof_roles', '����', '0', @obj_start_number, '0', '0'),
(@obj_start_id + 1, 'bof_employee2role', '���� �������������', '0', @obj_start_number + 1, '0', '0'); 

INSERT INTO `bof_pages` (`id`, `viewname`, `parent_id`, `type`, `razdel`, `obj_id`, `name`, `number`, `hidden`, `gui`) 
VALUES 
(@pages_start_id, '����', 155, '2', 4 ,@obj_start_id, '', @pages_start_number, '0', ''),
(@pages_start_id + 1, '���� �������������', 155, '2', 4 ,@obj_start_id + 1, '', @pages_start_number + 1, '0', ''); 

INSERT INTO `bof_fields` 
(`name`, `viewname`,  `hint`, `obj_id`, `type`, `param`, `param2`, `filter`, `hidecolumn`, `align`, `number`,`hidden`, `sort_column`) 
VALUES
('name','��������', '', @obj_start_id, 1,null, null, null,0, null, @field_start_number, 0, null),
('employee_id','employee_id', '', @obj_start_id + 1, 11,	5,'%Y-%m-%d %H:%M:%S', null,0, null, @field_start_number + 1, 0, null),
('role_id','role_id', '', @obj_start_id + 1, 11,@obj_start_id, null, null,0, null, @field_start_number + 2, 0, null);

INSERT INTO `bof_roles` (`name`) VALUES('������������'), ('��������'), ('���������'), ('����-�����');
INSERT INTO `bof_employee2role` (`employee_id`, `role_id`) VALUES (80, 1), (81, 3), (81, 4), (82, 2), (82, 3);

