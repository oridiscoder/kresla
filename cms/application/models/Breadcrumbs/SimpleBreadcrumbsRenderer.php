<?php
require_once ('BreadcrumbsRendererAbstract.php');

class SimpleBreadcrumbsRenderer extends BreadcrumbsRendererAbstract {
	/* (non-PHPdoc)
	 * @see BreadcrumbsAbstract::render()
	 */
	public function render() {
		$wrapper = '<ul class="breadcrumbs">{items}</ul><br clear="all"/>';
		
		$html = '';
		$total = count($this->menuItems);
		foreach ($this->menuItems as $key=>$item) {
			if($key + 1 != $total) {
				$html .= sprintf('<li><a href="%s">%s</a></li><li class="delimeter">/</li>', $this->baseURL.$item['href'], $item['title']);
			} else {
				$html .= '<li class="end">'.$item['title'].'<li>';
			}
		}
		
		return str_replace("{items}", $html, $wrapper);
	}

}

?>