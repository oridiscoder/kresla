<?php
abstract class BreadcrumbsRendererAbstract {
	protected $menuItems;
	protected $baseURL;
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	
	/**
	 *
	 * @param Smarty $smarty
	 */
	public function __construct($smarty) {
		$this->smarty = $smarty;
	}
	
	public function setItems($items) {
		$this->menuItems = $items;
	}
	public function setBaseURL($url) {
		$this->baseURL = $url;
	}
	
	abstract public function render();
}

?>