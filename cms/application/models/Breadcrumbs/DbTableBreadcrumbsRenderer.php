<?php
require_once ('BreadcrumbsRendererAbstract.php');

class DbTableBreadcrumbsRenderer extends BreadcrumbsRendererAbstract {
	/**
	 *
	 * @var DbTablePage
	 */
	protected $dbTablePage;
	
	public function __construct($page) {
		$this->dbTablePage = $page;
		parent::__construct($this->dbTablePage->getSmarty());
	}
	/* (non-PHPdoc)
	 * @see BreadcrumbsAbstract::render()
	 */
	public function render() {
		$html = '';
		
		$actionName = $this->dbTablePage->getActionName();
		if($actionName == 'recycle') {
			$this->menuItems[] = array(
				'href' => $this->menuItems[count($this->menuItems) - 1]['href'] . 'recycle/',
				'title' => 'Корзина'
			);
		}
		
		$total = count($this->menuItems);
		foreach ($this->menuItems as $key=>$item) {
			if($key + 1 != $total) {
				$html .= sprintf('<a href="%s">%s</a> / ', $this->baseURL.$item['href'], isset($item['hint']) ? $item['title'].' ['.$item['hint'].']' : $item['title']);
			} else {
				$html .= $item['title'];
				if(isset($item['hint'])) {
					$html .= '['.$item['hint'].']';
				}
			}
		}

		$this->smarty->assign("breadcrumbs", $this->menuItems);
		
		return $this->smarty->fetch("table/breadcrumbs-dbtable.html");
	}


}

?>