<?php

class Bof_Enums {
	private $db;
	public function __construct($db = null) {
		if($db) {
			$this->db = $db;
		} elseif(class_exists('Application')) {
			$this->db = Application::getAppDbAdapter();
		} else {
			throw new Exception("Give me database adapter as first argument, please");
		}
	}
	
	public function getEnums($typeID) {
		$select = $this->db->select()
			->from("bof_enums")
			->where("hidden = 0")
			->where("enum_id = ?", $typeID);
		
		return $this->db->fetchAll($select);
	}
	
	public function getEnumName($id) {
		return $this->db->fetchOne("SELECT name FROM bof_enums WHERE id = ?", $id);
	}
	
	public function createNew($enum) {
		
	}
}