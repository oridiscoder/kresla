<?php

class InformerLoader {
	
	public static $informers;
	
	/**
	 *
	 * @var User
	 */
	private $user;
	
	private $informerPath;
	
	public function __construct($user, $path) {
		$this->user = $user;
		$this->informerPath = $path;
	}
	
	public function get($className) {
		
		$className = ucfirst($className);
		if(!isset(self::$informers[$className])) {
			$fileName = $className.'.php';
			if(file_exists($this->informerPath.'/'.$fileName)) {
				require_once $this->informerPath.'/'.$fileName;
			} else {
				throw new InformerException("Informer $this->informerPath/$fileName not found");
			}
			if(!class_exists($className)) {
				throw new InformerException("Informer class $className not found in $this->informerPath/$fileName");
			}
			
			$informer = new $className($this->user);
			
			if(!($informer instanceof InformerAbstract)) {
				throw new InformerException("Informer class $className must be an instance of InformerAbstract");
			}
			
			self::$informers[$className] =$informer;
		}
		
		return self::$informers[$className];
	}
	
	public function getUserInformers() {
		$db = Application::getAppDbAdapter();
		
		//информеры группы:
		$groupID = $this->user->getGroupID();
		$select = $db->select()
			->from(array('I2U' => 'bof_informers2group'), array())
			->join(array('I' => 'bof_informers'), "I.id = I2U.informer_id")
			->where("I2U.group_id = ?", $this->user->getGroupID())
			->order('I2U.number');
		
		$informers = $db->fetchAll($select);
		
		//информеры конкретного пользователя
		$select = $db->select()
			->from(array('I2U' => 'bof_informers2user'), array())
			->join(array('I' => 'bof_informers'), "I.id = I2U.informer_id")
			->where("I2U.user_id = ?", $this->user->getID())
			->order('I2U.number');
		$userInformers = $db->fetchAll($select);
		
		if(!empty($userInformers)) foreach ($userInformers as $uInformer) {
			$exist = false;
			if(!empty($informers)) foreach ($informers as $gInformer) {
				if($gInformer['id'] == $uInformer['id']) {
					$exist = true;
					break;
				}
			}
			
			if(!$exist) {
				$informers[] = $uInformer;
			}
		}
		
		$classes = array();
		if(!empty($informers)) {
			foreach ($informers as $key=>$informer) {
				$fileName = $informer['class_name'].'.php';
				if(file_exists($this->informerPath.'/'.$fileName)) {
					require_once $this->informerPath.'/'.$fileName;
					if(class_exists($informer['class_name'])) {
						$className = $informer['class_name'];
						$informer = new $className($this->user);
						if($informer instanceof InformerAbstract) {
							$classes[] = $informer;
						}
					}
				}
			}
		}
		
		return $classes;
	}
}

class InformerException extends Exception {
	
}

?>