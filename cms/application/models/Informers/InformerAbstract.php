<?php

abstract class InformerAbstract {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	
	/**
	 *
	 * @var User
	 */
	protected $worker;
	
	public function __construct($user) {
		$this->dbAdapter = Application::getAppDbAdapter();
		$this->smarty = clone Application::getAppSmarty();
		$this->smarty->setTemplateDir($this->smarty->getTemplateDir(0).'informers/'.get_class($this));
		$this->worker = $user;
	}
	
	public function getName() {
		return get_class($this);
	}
	
	/**
	 * @return strign - HTML текущего информера
	 */
	abstract public function render();
}

?>