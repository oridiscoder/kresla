<?php

require_once ('InformerAbstract.php');

abstract class StandartInformer extends InformerAbstract {
	
	abstract public function getTitle();
	abstract public function getText();
	/* (non-PHPdoc)
	 * @see InformerAbstract::render()
	 */
	public function render() {
		$this->smarty->setTemplateDir(APPLICATION_PATH.'/templates/informers/StandartInformer');
		$this->smarty->assign("title", $this->getTitle());
		$this->smarty->assign("text", $this->getText());
		return $this->smarty->fetch('index.html');
	}


}

?>