<?php

abstract class ClusterModuleAbstract {
	/**
	 *
	 * @var Cluster
	 */
	protected $cluster;
	
	/**
	 *
	 * @var Bof_Request
	 */
	protected $request;
	
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	private $pageTitle;
	private $pageKeywords;
	private $pageDescription;
	private $meta;
	/**
	 *
	 * @var ClusterDbTable
	 */
	private $dbTable;
	private $headScripts;
	private $headStyles;
	
	/**
	 *
	 * @param Cluster $cluster
	 */
	public function setCluster($cluster) {
		$this->cluster = $cluster;
		return $this;
	}
	public function setRequest($request) {
		$this->request = $request;
		return $this;
	}
	public function setSmarty($smarty) {
		$this->smarty = $smarty;
		return $this;
	}
	public function setDbAdapter($db) {
		$this->db = $db;
		return $this;
	}
	
	/**
	 * Получить мета-информацию о текущей странице кластера 
	 * Title, Keywords, Description, H1, Text
	 * @return array
	 */
	public function loadClusterMeta() {
		if($this->meta == null) {
			$select = $this->db->select()
				->from("clusters_meta")
				->where("hidden = 0")
				->where("cluster_id = ?", $this->cluster->getID())
				->where("url = ?", $this->request->getPathInfo())
				->limit(1);
							
			$page = $this->cluster->getCurrentSubpage();
			
			if($page) {
				//$select->where("cluster_page_id = ?", $page['id']);
			}
			
			echo $select;
			
			$meta = $this->db->fetchRow($select);
			$this->meta = $meta ? $meta : array(); 
		}
		
		return $this->meta;
	}
	
	
	public function getBreadcrumbs() {
		$breadcrumbs = array();
		$breadcrumbs[] = array(
			'url' => '/',
			'title' => 'Главная'
		);
		$clusterBreadcrumbs = $this->cluster->getBreadcrumbs();
		$breadcrumbs = array_merge($breadcrumbs, $clusterBreadcrumbs);
		return $breadcrumbs;
	}
	
	public function getTitle() {
		if(!$this->pageTitle) {
			$this->pageTitle = $this->cluster->getTitle();
		}
		return $this->pageTitle;
	}
	
	public function getKeywords() {
		if(!$this->pageKeywords) {
			$this->pageKeywords = $this->cluster->getKeywords();
		}
		return $this->pageKeywords;
	}
	
	public function getDescription() {
		if(!$this->pageDescription) {
			$this->pageDescription = $this->cluster->getDescription();
		}
		return $this->pageDescription;
	}
	
	public function getMeta() {
		return array(
			'title' => $this->getTitle(),
			'keywords' => $this->getKeywords(),
			'description' => $this->getDescription()
		);
	}
	public function setPageTitle($t) {
		$this->pageTitle = $t;
		return $this;
	}
	public function setPageKeywords($k) {
		$this->pageKeywords = $k;
		return $this;
	}
	public function setPageDescription($d) {
		$this->pageDescription = $d;
		return $this;
	}
	
	/**
	 * Получить дополнительные страницы текущего кластера.
	 * Например для ski/austria : tickets.html, tours.html
	 * @return array
	 */
	public function getClusterPages() {
		return $this->cluster->getPages();
	}

	/**
	 * Получить объект для работы с данными, ассоциированными с текущим кластером через target_id
	 * @return ClusterDbTable
	 */
	public function getDataObject() {
		if(!$this->dbTable) {
			require_once "ClusterDbTable.php";
			$this->dbTable = new ClusterDbTable($this->db, $this->cluster->getDataObjectID());
			$this->initDataObjectFilters();
		}
		return $this->dbTable;
	}
	
	public function initDataObjectFilters() {
		if($this->dbTable) {
			for($i = 1; $i <= Cluster::MAX_OBJECTS_COUNT; $i++) {
				$id = $this->cluster->getObjectID($i);
				$value = $this->cluster->getObjectValue($i);
				if(is_numeric($id)) {
					foreach ($this->dbTable->getFields() as $field) {
						/* @var $field DescendantField */
						if($field instanceof DescendantField) {
							if($field->getParentTableId() == $id) {
								$this->dbTable->setFieldValue($field->getName(), $value['id']);
							}
						}
					}
				} else {
					break;
				}
			}
		}
	}
	
	public function getRecords() {
		$dbTable = $this->getDataObject();
		return $dbTable->getRows();
	}
	
	/**
	 * Добавить указанный JS скрипт в тег <head>
	 * @param string $src - пусть к скрипту
	 * @return ClusterModuleAbstract
	 */
	public function appendHeadScript($src) {
		$this->headScripts[] = $src;
		return $this;
	}
	/**
	 * Добавить указанный CSS файл в тег <head>
	 * @param string $src
	 * @return ClusterModuleAbstract
	 */
	public function appendHeadStyle($src) {
		$this->headStyles[] = $src;
		return $this;
	}
	
	public function getHeadScripts() {
		return $this->headScripts;
	}
	public function getHeadStyles() {
		return $this->headStyles;
	}
	
	abstract public function render();
}

?>