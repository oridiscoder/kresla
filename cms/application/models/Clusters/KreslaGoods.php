<?php

require_once ('ClusterModuleAbstract.php');

class KreslaGoods extends ClusterModuleAbstract {
	public function getTitle() {
		$meta = $this->loadClusterMeta();
		if($meta) {
			return $meta['title'];
		} else {
			return "Не найден заголовок";
		}
	}
	public function getKeywords() {
		$meta = $this->loadClusterMeta();
		if($meta) {
			return $meta['keywords'];
		}
	}
	public function getBreadcrumbs() {
		
	}
	public function render() {
		
	}
}

?>