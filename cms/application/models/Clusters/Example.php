<?php

require_once ('ClusterModuleAbstract.php');

class Example extends ClusterModuleAbstract {
	/* (non-PHPdoc)
	 * @see ClusterModuleAbstract::render()
	 */
	public function render() {
		
		$dbTable = $this->getDataObject();
		
		$pages = $this->getClusterPages();
		
		$this->smarty->assign(array(
			'table' => $dbTable,
			'pages' => $pages,
			'baseURL' => '/'.$this->cluster->getURL()
		));
		
		return $this->smarty->fetch("table.html");
	}


}

?>