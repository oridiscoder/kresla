<?php
class Cluster {
	/**
	 * Максимальное количество объектов, которо может быть задано у кластера
	 * @var integer
	 */
	const MAX_OBJECTS_COUNT = 3;
	
	private $cluster;
	private $pages;
	/**
	 *
	 * @var Cluster
	 */
	private $parent;
	private $urlData;
	/**
	 * В кластере может быть задано от 1 до 3х объектов.
	 * Хорошо один раз высчитать, сколько объектов задано у данного кластера,
	 * чтоб потом использовать это число в циклах.
	 *
	 * @var integer
	 */
	private $objectsCount = 0;
	
	public function __construct($cluster = null) {
		if($cluster) {
			$this->cluster = $cluster;
			
			if(isset($cluster['_pages'])) {
				$this->pages = $cluster['_pages'];
				unset($this->cluster['_pages']);
			}
			
			/**
			 * Посчитаем, сколько объектов задано у данного кластера
			 */
			for($i = 0; $i < self::MAX_OBJECTS_COUNT; $i++) {
				if($this->cluster['object_'.($i + 1)]) {
					$this->objectsCount ++;
				} else {
					break;
				}
			}
		} else {
			$this->cluster = array(
				'id' => null
			);
		}
	}
	
	public function getID() {
		return $this->cluster['id'];
	}
	public function getObjectID($num) {
		return $this->cluster['object_'.$num];
	}
	public function getObjectName($num) {
		return $this->cluster['object_'.$num.'_name'];
	}
	public function getObjectValue($num) {
		if($num <= $this->objectsCount) {
			return $this->urlData[$num - 1];
		}
	}
	public function getDataObjectID() {
		return $this->cluster['target_id'];
	}
	
	public function getPages($name = null) {
		if($name) {
			return isset($this->pages[$name]) ? $this->pages[$name] : null;
		} else {
			return $this->pages ? $this->pages : array();
		}
	}
	
	public function getParentID() {
		return $this->cluster['parent_id'];
	}
	
	/**
	 *
	 * @param Cluster $p
	 */
	public function setParent($p) {
		$this->parent = $p;
	}
	/**
	 * @return Cluster
	 */
	public function getParent() {
		return $this->parent;
	}
	
	public function getUrlPrefix() {
		return $this->cluster['url_prefix'];
	}
	
	public function setData($data) {
		$this->urlData = $data;
	}
	public function getData() {
		return $this->urlData;
	}
	
	public function isSubpage() {
		return count($this->urlData) == $this->objectsCount + 1;
	}
	public function getCurrentSubpage() {
		return end($this->urlData);
	}
	
	/**
	 * Получить URL текущего кластера
	 * @return string
	 */
	public function getURL($withSubpageUrl = false) {
		$parts[] = $this->getUrlPrefix();
		
		if($this->urlData) {
			for($i = 1; $i <= $this->objectsCount; $i ++) {
				$fieldName = $this->cluster['object_'.$i.'_field_name'];
				$parts[] = $this->urlData[$i - 1][$fieldName];
			}
		}
		
		if($withSubpageUrl && $this->isSubpage()) {
			$page = $this->getCurrentSubpage();
			$parts[] = $page['name'];
		}
		
		$parts = array_filter($parts);
		
		return implode("/", $parts);
	}
	
	/**
	 * Получить название страницы текущего кластера.
	 * Собирается из полей name_1, name_2, name_3 соответствующих объектов (таблиц).
	 * @return string
	 */
	public function getTitle() {
		$title = "";
		if($this->urlData) {
			for($i = 1; $i <= $this->objectsCount; $i ++) {
				$title .= " ".$this->urlData[$i -1]['name'];
			}
		}
		if($this->isSubpage()) {
			$page = $this->getCurrentSubpage();
			$title .= " - ".$page['title'];
		}
		return $title;
	}
	
	/**
	 * Получить текущее название последнего объекта в списке объектов данного кластера.
	 * Например для кластера ski/january/austria вернёт Австрия
	 * @return string
	 */
	public function getLastDataTitle() {
		if($this->urlData) {
			return $this->urlData[$this->objectsCount - 1]['name'];
		}
	}
	
	/**
	 * Получить хлебные крошки. Рекурсивная функция.
	 * Для корректной работы необходимо заранее собрать цепь наследования через setParent()
	 * @return array - массив вида [url, title]
	 */
	public function getBreadcrumbs() {
		$breadcrumbs = array();
		$cluster = $this;
		while($cluster) {
			$breadcrumbs[] = array(
				'url' => '/'.$cluster->getURL().'/',
				'title' => $cluster->getLastDataTitle()
			);
			$cluster = $cluster->getParent();
		}
		$breadcrumbs = array_reverse($breadcrumbs);
		
		if($this->isSubpage()) {
			$page = $this->getCurrentSubpage();
			$breadcrumbs[] = array(
					'url' => $this->getURL().'/'.$page['name'],
					'title' => $page['title']
			);
		}
		return $breadcrumbs;
	}
	
	public function getKeywords() {
		$keywords = array();
		for($i = 1; $i <= $this->objectsCount; $i++) {
			$keywords[] = $this->urlData[$i-1]['name'];
		}
		return implode(",", $keywords);
	}
	
	public function getDescription() {
		
	}
	
	/**
	 * Получить имя модуля для данного кластера или для подстраницы данного кластера
	 * @return string - имя модуля
	 */
	public function getModuleName() {
		if(!$this->isSubpage()) {
			$moduleName = $this->cluster['module_name'];
		} else {
			$page = $this->getCurrentSubpage();
			$moduleName = $page['module_name'];
		}
		return ucfirst($moduleName);
	}
	
	/**
	 *
	 * @param integer $id
	 * @param Zend_Db_Adapter_Abstract $db
	 */
	public static function get($id, $db) {
		$select = $db->select()
			->from(array("C" => 'clusters'))
			->joinLeft(array("O1" => 'bof_objects'), "O1.id = C.object_1", array('object_1_name' => 'name'))
			->joinLeft(array("O1F" => 'bof_fields'), "O1F.obj_id = O1.id AND O1F.is_url_field = 1", array("object_1_field_name" => 'name'))
			->joinLeft(array("O2" => 'bof_objects'), "O2.id = C.object_2", array('object_2_name' => 'name'))
			->joinLeft(array("O2F" => 'bof_fields'), "O2F.obj_id = O2.id AND O2F.is_url_field = 1", array("object_2_field_name" => 'name'))
			->joinLeft(array("O3" => 'bof_objects'), "O3.id = C.object_3", array('object_3_name' => 'name'))
			->joinLeft(array("O3F" => 'bof_fields'), "O3F.obj_id = O3.id AND O3F.is_url_field = 1", array("object_3_field_name" => 'name'))
			->where("C.hidden = 0")
			->where("C.id = ?", $id);
		$cluster = $db->fetchRow($select);
		
		if($cluster) {
			$select = $db->select()
					->from("clusters_pages", array('name', 'module_name'))
					->where("hidden = 0")
					->where("cluster_id = ?", $cluster['id']);
			$pages = $db->fetchPairs($select);
			$cluster['_pages'] = $pages ? $pages : array();
			
			return new self($cluster);
		} else {
			return null;
		}
	}
	
}

?>