<?php
require_once 'User.php';
require_once 'UserFactory.php';

class Authoriser {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	/**
	 *
	 * @var UserFactory
	 */
	protected $userFactory;
	protected $cookieExpire = 7776000; 	// 90 days
	
	/**
	 *
	 * @param Zend_Db_Abstract $db
	 */
	public function __construct($db, $sessionName = 'bof') {
		session_name($sessionName);
		session_start();
		$this->db = $db;
		$this->userFactory = UserFactory::getInstance($db);
	}
	
	/**
	 * Функция авторизации. Проверит логи и пароль, если все верно - авторизует пользователя
	 * @param string $login
	 * @param string $password
	 * @return BOFUser or null
	 */
	public function authorise($login, $password) {
		
		$user = $this->db->fetchRow("SELECT * FROM bof_users WHERE login = ? AND password = MD5(?)", array($login, $password));
		if(!empty($user)) {
			return $this->userFactory->getUserById($user['id']);
		} else {
			return null;
		}
	}
	
	
	/**
	 * Авторизовать пользователя в системе
	 * @param BOFUser $user
	 */
	public function login(User $user, $remember = false) {
		$_SESSION["userbof_users"] = array(
			'login' => $user->getLogin(),
			'ip' => $_SERVER['REMOTE_ADDR'],
			'id' => $user->getId()
		);
		
		if($remember) {
			setcookie("cms_user_login", $user->getLogin(), time() + $this->cookieExpire, '/');
			setcookie("cms_user_password", $user->getPassword(), time() + $this->cookieExpire, '/');
		} else {
			setcookie("cms_user_login", null, 0);
			setcookie("cms_user_password", null, 0);
		}
	}
	
	/**
	 * Разлогиниться
	 */
	public function logout() {
		setcookie("cms_user_login", null, 0, '/');
		unset($_SESSION['userbof_users']);
	}
	
	public function setUserLastLoginTime($userID) {
		$this->db->update("bof_users", array('last_login' => date('Y-m-d H:i:s')), "id = $userID");
	}
	
	/**
	 *
	 * @param User $user
	 */
	public function logUserStep($user) {
		$step = array(
			'method' => $_SERVER['REQUEST_METHOD'],
			'url' => $_SERVER['REQUEST_URI'],
			'employee_id' => $user->getId(),
			'date' => date('Y-m-d H:i:s')
		);
		$this->db->insert("bof_user_activity_log", $step);
	}
	
	public function logUserAuth($ip, $login, $success) {
		$data = array(
			'ip' => $ip, 
			'login' => $login, 
			'success' => $success, 
			'date' => date('Y-m-d H:i:s'),
			'number' => 1 + (int) $this->db->fetchOne("SELECT MAX(number) FROM bof_users_logins")
		);
		
		$this->db->insert("bof_users_logins", $data);
	}
	
	/**
	 * Если пользователь авторизован, то вернем объект BOFUser, соотв. этому пользователю
	 * @return BOFUser
	 */
	public function getAuthorised() {
		if(isset($_SESSION['userbof_users'])) {
			$user =  $this->userFactory->getUserById($_SESSION['userbof_users']['id']);
			return $user;
		} elseif(isset($_COOKIE['cms_user_login']) && isset($_COOKIE['cms_user_password'])) {
			$login = $_COOKIE['cms_user_login'];
			$password = $_COOKIE['cms_user_password'];
			
			$user = $this->userFactory->getUserByLogin($login);
			if($user && $user->isMyPassword($password)) {
				$this->login($user, true);
				return $user;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}