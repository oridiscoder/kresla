<?php

require_once 'ProcedureAbstract.php';

class ProcedureLoader {
	public static function loadProcedures($tableID, $tableName, $action = null) {
		$baseDir = dirname(__FILE__);
		$dir = opendir($baseDir);
		$procedures = array();
		if($dir) {
			while(($fileName = readdir($dir)) != false) {
				if($fileName != '.' && $fileName != '..') {
					$fullFileName = APPLICATION_PATH . '/models/Procedures/'.$fileName;
					if(strpos($fileName, "Abstract") === false && is_file($fullFileName)) {
						$className = str_replace(".php", "", $fileName);
						require_once $fullFileName;
						if(class_exists($className) && is_subclass_of($className, "ProcedureAbstract")) {
							
							/*
							 * Получим объявленные по-умолчанию переменные класса, 
							 * нас интересуют 2 переменные
							 * tables - список таблиц, с которыми работает данная процедура
							 * actions - список действий, которая она обрабатывает
							 */
							$classVars = get_class_vars($className);
							$procedureTables = isset($classVars['tables']) ? $classVars['tables'] : array();
							$procedureActions = isset($classVars['actions']) ? $classVars['actions'] : array();
							
							if(empty($procedureTables) || in_array($tableID, $procedureTables) || in_array($tableName, $procedureTables)) {
								if(!$action || ($action && (in_array($action, $procedureActions) || in_array(ProcedureAbstract::TYPE_ALL, $procedureActions)))) {
									/* @var $procedure ProcedureAbstract */
									$procedure = new $className();
									$procedure->setTableID($tableID);
									$procedure->setTableName($tableName);
									$procedure->setAction($action);
									$procedure->setDbAdapter(Application::getAppDbAdapter());
									$procedure->setRequest(Application::getAppRequest());
									$procedure->setConfig(Application::getAppConfigs());
									$procedures[] = $procedure;
								}
							}
						}
					}
				}
			}
		}
		
		return $procedures;
	}
}

?>