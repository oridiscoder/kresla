<?php

require_once ('ProcedureAbstract.php');

class TemplatesProcedure extends ProcedureAbstract {
	
	public static $tables = array("cms_templates");
	public static $actions = array(self::TYPE_ALL);
	
	/* (non-PHPdoc)
	 * @see ProcedureAbstract::beforeAction()
	 */
	public function beforeAction($data) {
		if($this->action == self::TYPE_ADD || $this->action == self::TYPE_EDIT) {
			
			if(empty($data['sysname'])) {
				
				require_once "Bof/Transliteratorer.php";
				
				$name = iconv("utf-8", "cp1251", $data['name']);
				$sysname = Transliteratorer::toTranslit($name);
				$sysname = str_replace(" ", "_", $sysname);
				
				$t = $this->findTemplateBySysname($sysname);
				
				if($t && $t['id'] != $data['id']) {
					throw new ProcedureException("Шаблон с системным именем $sysname уже существует");
				} else {
					$data['sysname'] = $sysname;
					return $data;
				}
				
			}
		}
	}
	
	private function findTemplateBySysname($sysname) {
		$select = $this->dbAdapter->select()
			->from($this->tableName)
			->where("sysname = ?", $sysname);
		return $this->dbAdapter->fetchRow($select);
	}

	/* (non-PHPdoc)
	 * @see ProcedureAbstract::afterAction()
	 */
	public function afterAction($data) {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see ProcedureAbstract::where()
	 */
	public function where() {
		return " (module_name = '' OR module_name IS NULL)";
	}


}

?>