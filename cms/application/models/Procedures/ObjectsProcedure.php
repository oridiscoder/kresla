<?php

require_once ('ProcedureAbstract.php');

class ObjectsProcedure extends ProcedureAbstract {

	
	public static $tables = array('bof_objects');
	public static $actions = array(self::TYPE_ALL);
	
	
	/* (non-PHPdoc)
	 * @see ProcedureAbstract::beforeSave()
	 */
	public function beforeAction($data) {
		$tableName = $data['name'];
		
		$tableObject = $this->findTableInObjects($tableName);
		
		if($this->action == self::TYPE_ADD) {
			
			if(!empty($tableObject)) {
				throw new Exception("Table $tableName already exists in bof_objects");
			}
			
			if(!$this->tableAlreadyExistsInDb($tableName)) {
				$this->createTable($tableName);
			} else {
				$this->addNumberAndHidden($tableName);
			}
		}
	}

	/* (non-PHPdoc)
	 * @see ProcedureAbstract::afterSave()
	 */
	public function afterAction($data) {
		
	}

	/* (non-PHPdoc)
	 * @see ProcedureAbstract::where()
	 */
	public function where() {
		 
	}
	
	private function tableAlreadyExistsInDb($tableName) {
		return $this->dbAdapter->fetchRow("SHOW TABLES LIKE ".$this->dbAdapter->quote($tableName));
	}
	
	private function findTableInObjects($tableName) {
		return $this->dbAdapter->fetchRow("SELECT * FROM bof_objects WHERE name = ?", $tableName);
	}
	
	private function createTable($tableName) {
		$sql = "CREATE TABLE IF NOT EXISTS `$tableName`(".
				"`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,".
				"`number` INT NOT NULL DEFAULT 0,".
				"`hidden` TINYINT(1) NOT NULL DEFAULT 0".
				") ENGINE = MyISAM DEFAULT CHARSET=UTF8;";
		$this->dbAdapter->query($sql);
	}
	
	private function addNumberAndHidden($tableName) {
		$fields = $this->dbAdapter->fetchCol("DESCRIBE ".$this->dbAdapter->quoteIdentifier($tableName));
		
		if(!in_array('number', $fields)) {
			$this->dbAdapter->query("ALTER TABLE ".$this->dbAdapter->quoteIdentifier($tableName)." ADD COLUMN `number` INT NOT NULL DEFAULT 0");
		}
		if(!in_array('hidden', $fields)) {
			$this->dbAdapter->query("ALTER TABLE ".$this->dbAdapter->quoteIdentifier($tableName)." ADD COLUMN `hidden` TINYINT(1) NOT NULL DEFAULT 0");
		}
		
	}


}

?>