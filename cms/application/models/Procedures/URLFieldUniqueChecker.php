<?php

require_once ('ProcedureAbstract.php');

class URLFieldUniqueChecker extends ProcedureAbstract {

	public static $tables = null; //значит, навесить на все таблицы
	public static $actions = array(self::TYPE_ADD, self::TYPE_EDIT);
	
	/* (non-PHPdoc)
	 * @see ProcedureAbstract::beforeAction()
	 */
	public function beforeAction($data) {
		$urlField = $this->getTableURLField();
		if($urlField) {
			$value = $data[$urlField['name']];
			
			//валидный ли URL?
			if(!$this->isValidURL($value)) {
				throw new ProcedureException("Неверный формат для поля <strong>".$urlField['viewname'].'</strong>');
			}
			//нет ли уже такого URL?
			if(($tables = $this->isUniqueValue($value)) != null ) {
				$names = array();
				foreach ($tables as $table) {
					if($table['id'] != $this->tableID) {
						$names[] = $table['viewname'];
					} else {
						if($data['id'] && count($data['id']) == 1 &&  in_array($data['id'], $table['ids'])) {
							
						} else {
							$names[] = $table['viewname'];
						}
					}
				}
				if($names) {
					throw new ProcedureException("Элемент с таким URL уже встречается в других таблицах: ".implode(", ", $names));
				}
			}
			
		}
	}

	/* (non-PHPdoc)
	 * @see ProcedureAbstract::afterAction()
	 */
	public function afterAction($data) {
		// TODO Auto-generated method stub
	}

	/* (non-PHPdoc)
	 * @see ProcedureAbstract::where()
	 */
	public function where() {
		// TODO Auto-generated method stub
	}
	
	private function isValidURL($url) {
		return preg_match('/^[-\w]+$/', $url);
	}
	
	private function getTableURLField() {
		$select = $this->dbAdapter->select()
			->from("bof_fields")
			->where("obj_id = ?", $this->tableID)
			->where("hidden = 0")
			->where("is_url_field = 1");
		return $this->dbAdapter->fetchRow($select);
	}
	
	/**
	 * Проверить сохраняемый URL на уникальность во всех таблицах, имеющих поле типа URL
	 * @param string $value - проверяемое значение
	 * @return array | null - список найденных таблиц, или null
	 */
	private function isUniqueValue($value) {
		
		$urlFields = array();
		$select = $this->dbAdapter->select()
			->from(array("F" => "bof_fields"))
			->join(array("T" => 'bof_objects'), "T.id = F.obj_id", array('table_name' => 'name', 'table_viewname' => 'viewname'))
			->where("F.is_url_field = 1")
			->where("F.hidden = 0")
			->where("T.hidden = 0");
		$urlFields = $this->dbAdapter->fetchAll($select);
		
		$tables = null;
		
		if(!empty($urlFields)) foreach ($urlFields as $field) {
			$tableName = $field['table_name'];
			
			$select = $this->dbAdapter->select()
				->from($tableName, array('id'))
				->where($field['name'].' = ?', $value)
				->where('hidden = 0');

			$rows = $this->dbAdapter->fetchCol($select);
			
			if($rows) {
				$tables[] = array(
					'id' => $field['obj_id'],
					'viewname' => $field['table_viewname'],
					'name' => $tableName,
					'ids' => $rows
				);
			}
		}
		
		return $tables;
	}


}

?>