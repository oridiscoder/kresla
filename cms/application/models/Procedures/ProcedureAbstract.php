<?php

abstract class ProcedureAbstract {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	/**
	 *
	 * @var Bof_Request
	 */
	protected $request;
	protected $tableID;
	protected $tableName;
	protected $action;
	
	/**
	 * 
	 * @var Список таблиц, на которые подвешивается процедура
	 */
	public static $tables;
	/**
	 * 
	 * @var Список действий, на которые будет реагировать процедура
	 */
	public static $actions;
	
	/**
	 *
	 * @var Zend_Config
	 */
	protected $config;
	
	const TYPE_ADD = "add";
	const TYPE_EDIT = "edit";
	const TYPE_DELETE = "delete";
	const TYPE_ALL = "*";
	
	public function setDbAdapter($db) {
		$this->dbAdapter = $db;
	}
	public function setRequest($request) {
		$this->request = $request;
	}
	public function setTableID($id) {
		$this->tableID = $id;
	}
	public function setTableName($name) {
		$this->tableName = $name;
	}
	public function setAction($action) {
		$this->action = $action;
	}
	public function setConfig($config) {
		$this->config = $config;
	}
	
	abstract public function beforeAction($data);
	abstract public function afterAction($data);
	abstract public function where();
}

class ProcedureException extends Exception {
	
}

?>