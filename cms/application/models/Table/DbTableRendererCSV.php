<?php

require_once ('DbTableRenderer.php');
/**
 * Я не тестировал этот класс!!!
 * Он лишь является примером переопределения функции out()
 *
 */
class DbTableRendererCSV extends DbTableRenderer {
	protected function out($data) {
		$tmpFile = tmpfile();
			
		$length = 0;
			
		foreach($data['rows'] as $row) {
			$csvData = array();
			foreach ($data['fields'] as $key=>$field) {
				$value = @$row[$field['name']];
				if(is_float($value)) {
					$value = number_format($value, 2, '.', '');
				}
				$csvData[] = $value;
			}
			$length += fputcsv($tmpFile, $csvData, ";");
		}
		
		fseek($tmpFile, 0);
		return fread($tmpFile, $length);
	}
}

?>