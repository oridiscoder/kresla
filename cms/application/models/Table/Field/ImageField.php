<?php
require_once ('DbFieldAbstract.php');

class ImageField extends DbFieldAbstract {
	
	const PROCESS_DRAW_INTO = 3;
	const PROCESS_CROP_INTO = 4;

	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		$value = $this->extractValue($dataRow);
		if(!empty($value)) {
			/* @var Zend_Config $config */
			$config = Application::getAppConfigs();
			
			$imageURL = $config->images->url.'/'.$this->getMyTableName().'/'.$dataRow['id'].'_'.$this->getName().'_'.$dataRow[$this->getName()];
			$imageThumbURL = $config->images->url.'/'.$this->getMyTableName().'/'.$dataRow['id'].'_'.$this->getName().'_thumb_'.$dataRow[$this->getName()];
			
			return '<a href="'.$imageURL.'" class="prettyPhoto"><img src="'.$imageThumbURL.'" /></a>';
		} else {
			return '<span class="no-photo">Нет изображения</span>';
		}
	}
	
	/**
	 * Получить URL изображения
	 * @param array $dataRow
	 * @param boolean $original - оригинальное, или обрезанное
	 * @return string
	 */
	public function getURL($dataRow, $original = false) {
		$value = $this->extractValue($dataRow);
		$url = null;
		if(!empty($value)) {
			/* @var Zend_Config $config */
			$config = Application::getAppConfigs();
			
			if($this->field['param'] && $original == false) {
				$suffix = $this->field['param2'].'_';
			} else {
				$suffix = null;
			}
			
			$url = $config->images->url.'/'.$this->getMyTableName().'/'.$dataRow['id'].'_'.$this->getName().'_'.$suffix.$value;
		}

		return $url;
	}
	
	/**
	 * Получить URL оригинального изображения (в случае, когда оно подхоняется под размеры)
	 * @param array $dataRow - текущая запись БД
	 * @return string
	 */
	public function getOriginalURL($dataRow) {
		return $this->getURL($dataRow, true);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		return null;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		$config = Application::getAppConfigs();
		$fields = $this->dbTable->getFields();
		$idField = $fields['id'];
		if(!empty($this->value)) {
			$html = '
			<img src="'.$config->images->url.'/'.$this->getMyTableName().'/'.$idField->getValue().'_'.$this->getName().'_thumb_'.$this->value.'"><br />
			<input type="file" name="'.$this->getName().'" /><br />
			<input id="ImageDelete'.$this->getName().'" type="checkbox" name="'.$this->getName().'_delete" />
			<label for="ImageDelete'.$this->getName().'">Удалить изображение</label>
			';
		} else {
			$html = '<input type="file" name="'.$this->getName().'" />';
		}
		return $html;

	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	*/
	public function makeRestrictions($select) {
		/* if(!empty($this->value)) {
			$select->where($this->getName()." LIKE ?", "%".$this->value."%");
		} */
		if($this->sorting) {
			$select->order($this->getMyTableName().'.'.$this->getName()." ".$this->sorting);
		}
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		if($this->imageUploaded()) {
			$fileName = $this->normilizeFileName($_FILES[$this->getName()]['name']);
			$data[$this->getName()] = $fileName;
		} elseif ($this->isDeleteCommand($data)) {
			$data[$this->getName()] = null;
		}
		return $data;
	}
	
	protected function normilizeFileName($name) {
		require_once 'Bof/Transliteratorer.php';
		$name = Transliteratorer::toTranslit($name);
		$name = preg_replace("[^-_a-zA-Z0-9]", "", $name);
		return $name;
	}
	
	protected function toTranslit($string) {
		
	}
	
	public function catchUploadExceptios($errNo, $errStr) {
		$this->uploadExceptions[] = $errStr;
	}
	
	protected function imageUploaded() {
		if(isset($_FILES[$this->getName()]) && $_FILES[$this->getName()]['error'] == UPLOAD_ERR_OK) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function isDeleteCommand($data) {
		return isset($data[$this->getName().'_delete']);
	}
	
	public function afterSave($data) {
		if($this->imageUploaded()) {
			
			$this->deleteOldFieldImages($data['id']);
			
			require_once 'Bof/ImageCropper.php';
			
			$imageFileName = $data[$this->getName()];
		
			$config = Application::getAppConfigs();
			
			$imageSource = $_FILES[$this->getName()]['tmp_name'];
				
			if(!isset($config->images->upload_dir)) {
				throw new DbFieldException("Section images.upload_dir not set in application config");
			}
			
			$imageDestination = $this->getDestinationFileName($imageFileName, $data['id'].'_'.$this->getName());
			
			$this->moveImage($imageSource, $imageDestination);
			
			if(!isset($config->images->thumb_size)) {
				throw new DbFieldException("section [images.thumb_size] is not set in application config. Example: images.thumb_size = 100x100");
			} else {
				list($thumbWidth, $thumbHeight) = explode("x", trim($config->images->thumb_size));
				if(!$thumbWidth || !$thumbHeight) {
					throw new DbFieldException("In section [images.thumb_size] width or height is not set. Example: images.thumb_size = 100x100");
				}

				$thumbDestination = $this->getDestinationFileName($imageFileName, $data['id'].'_'.$this->getName().'_thumb');				
				$imageCropper = new ImageCropper($imageDestination);
				$imageCropper->drawIn($thumbWidth, $thumbHeight)->saveAs($thumbDestination);
				unset($imageCropper);
			}
			
				
			switch($this->field['param']) {
				case 3: {
					list($width, $height) = explode("x", $this->field['param2']);
					if(!$width || !$height) {
						throw new DbFieldException("Destination image sizes not set (param2).");
					}
					
					$imageCropper = new ImageCropper($imageDestination);
					$imageDestination = $this->getDestinationFileName($imageFileName, $data['id']."_{$this->getName()}_{$width}x{$height}");
					$imageCropper->drawIn($width, $height)->saveAs($imageDestination);
					break;
				}
				case 4: {
					list($width, $height) = explode("x", $this->field['param2']);
					if(!$width || !$height) {
						throw new DbFieldException("Destination image sizes not set (param2).");
					}
					$imageCropper = new ImageCropper($imageDestination);
					$imageDestination = $this->getDestinationFileName($imageFileName, $data['id']."_{$this->getName()}_{$width}x{$height}");
					$imageCropper->drawInAndCrop($width, $height)->saveAs($imageDestination);
					break;
				}
				default:
					//ничего не делаем.
			}
		} elseif($this->isDeleteCommand($data)) {
			$this->deleteOldFieldImages($data['id']);
		} else {
			//print_r($data);
		}
	}
	
	protected function getDestinationFileName($imageFileName, $prefix = null) {
		$config = Application::getAppConfigs();
		if($prefix) {
			return realpath($config->images->upload_dir).'/'.$this->getMyTableName().'/'.$prefix.'_'.$imageFileName;
		} else {
			return realpath($config->images->upload_dir).'/'.$this->getMyTableName().'/'.$imageFileName;
		}
	}
	
	/**
	 * Перенести изображение из временной папки в место его постоянной дислокации
	 * @param string $imageSource
	 * @param string $imageDestination
	 * @throws DbFieldException
	 */
	protected function moveImage($imageSource, $imageDestination) {
		$this->uploadExceptions = null;
		$destinationFolder = dirname($imageDestination);
		set_error_handler(array($this, 'catchUploadExceptios'));
		if(!file_exists($destinationFolder)) {
			mkdir($destinationFolder, 0777);
		}
		//$success = move_uploaded_file($imageSource, $imageDestination);
		$success = copy($imageSource, $imageDestination);
		
		if(!$success) {
			throw new DbFieldException("Can't upload image to $imageDestination. Has got an errors: \n".implode("\n", $this->uploadExceptions));
		}
	}
	
	protected function deleteOldFieldImages($id) {
		$config = Application::getAppConfigs();
		$uploadDir = $config->images->upload_dir.'/'.$this->getMyTableName();
		$dirHandler = opendir($uploadDir);
		$filePrefix = $id.'_'.$this->getName();
		if($dirHandler) {
			while(($fileName = readdir($dirHandler)) != false) {
				if($fileName != '.' && $fileName != '..') {
					if(preg_match('/^'.$filePrefix.'/', $fileName)) {
						unlink($uploadDir.'/'.$fileName);
					}
				}
			}
		}
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	*/
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` VARCHAR(255)";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}
}

?>