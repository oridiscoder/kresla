<?php

require_once APPLICATION_PATH . '/models/Table/DbTable.php';
require_once ('DbFieldAbstract.php');

/**
 * Класс для управления связями N-M
 * INIT SQL: INSERT INTO `bof_fieldtypes` (`id`, `name`, `value`, `number`, `hidden`) VALUES  (22, 'Связующая таблица', '0', 22, '0'); 
 * @author Gourry
 *
 */
class AssociativeEntityField extends DbFieldAbstract {
	
	protected $virtual = true;
	/**
	 * 
	 * @var DbTable
	 */
	private $ae;
	/**
	 * 
	 * @var DescendantField
	 */
	private $thisTableField;
	private $thatTableField;
	
	private $cache;
	
	public function __construct($field) {
		parent::__construct($field);
		
		$this->ae = new DbTable($this->dbAdapter, $field['param']);
		
		foreach ($this->ae->getFields() as $field) {
			if($field instanceof DescendantField) {
				if($field->getParentTableId() == $this->getTableID()) {
					$this->thisTableField = $field;
				} else {
					$this->thatTableField = $field;
				}
			}
		}
		//var_dump($this->thatTableField);
		//var_dump($this->thisTableField);
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::extractValue()
	 */
	public function extractValue($dataRow) {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		/* надо сбросить значение для поля thatTableField, которое могло быть установлено в makeRestrictions */
		$this->thatTableField->setValue("");
		$this->ae->setFieldValue($this->thisTableField->getName(), $dataRow['id']);
		$strings = array();
		$rows = $this->ae->getRows();
		if($rows) foreach($rows as $row) {
			$strings[] = $this->thatTableField->renderCell($row);
		}
		return implode(", ", $strings);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		
		$this->thatTableField->setName($this->getName());
		$this->thatTableField->setValue($this->getValue());
		
		return $this->thatTableField->renderFilter();
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		/* вытащим все записи из второй таблицы */
		$thatTable = new DbTable($this->dbAdapter, $this->thatTableField->getParentTableId());
		$thatTableFirstField = $thatTable->getFirstField();
		$rows = $thatTable->getRows();
		
		/* какие связи уже есть в связующей таблице? */
		$this->ae->setFieldValue($this->thisTableField->getName(), $dataRow['id']);
		$links = $this->ae->getRows();

		$data = array();
		
		if($rows) {
			foreach ($rows as $row) {
				$checked = false;
				if($links) {
					/* $link - это запись из связующей таблицы. Например: {good_id, category_id} */
					foreach($links as $link) {
						if($row['id'] == $link[$this->thatTableField->getName()] && $dataRow['id'] != '') {
							/* ссылка на $row присутствует в связующей таблице */
							$checked = true;
							break;
						}
					}
				}
				
				$data[] = array(
					'title' => $thatTable->getFirstField()->extractValue($row), 
					'value' => $row['id'], 
					'checked' => $checked
				);
			}
		}
		
		$checkboxesHTML = "";
		$checkboxTemplate = '<label class="associative_entity"><input type="checkbox" name="{name}" value="{value}" {checked} /> {title}</label>';
		foreach ($data as $dataItem) {
			$checkboxesHTML .= str_replace(
				array("{title}", "{value}", "{name}", "{checked}"), 
				array(
					$dataItem['title'], 
					$dataItem['value'], 
					$this->getName().'[]', 
					$dataItem['checked'] ? 'checked="checked"' : '' 
				), 
				$checkboxTemplate
			);
		}
		
		return $checkboxesHTML;
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		
		/* 
		 * Здесь мы ещё можем получить нефильтрованные данные.
		 * Т.е. в данные в том виде, в котором они ушли с формы.
		 * Запоминаем их.
		 */ 
		$this->cache['ids'] = isset($data[$this->getName()]) ? $data[$this->getName()] : array();
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	 */
	public function afterSave($data) {
		
		/* 
		 * после сохранения записи нам становится доступен ID записи 
		 * и мы можем сохранить данные с связующую таблицу
		 */ 
		$id = $data['id'];
		$ids = $this->cache['ids'];
		
		/* определим, какие связи уже есть в связующей таблице */
		$oldIds = array();
		$this->thisTableField->setValue("");
		$this->thatTableField->setValue("");
		$this->ae->setFieldValue($this->thisTableField->getName(), $id);
		$links = $this->ae->getRows();
		if($links) foreach($links as $link) {
			$oldIds[] = $link[$this->thatTableField->getName()];
		}
		
		/* какие из связей новые, а какие старые */
		$idsNew = array_diff($ids, $oldIds);
		$idsOld = array_diff($oldIds, $ids);
		
		/* новые - добавляем */
		if($idsNew) {
			foreach ($idsNew as $newID) {
				$record = array(
					$this->thisTableField->getName() => $id, 
					$this->thatTableField->getName() => $newID
				);
				$this->ae->save($record);
			}
		}
		
		/* старые - удаляем */
		if($idsOld) {
			$this->ae->delete(array(
				$this->thisTableField->getName() . ' = ?' => $id, 
				$this->thatTableField->getName() . ' IN (?)' => $oldIds
			));
		}
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		return null; //виртуальный столбец
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::total()
	 */
	public function total($rows = null) {
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		$field = $this->thatTableField;
		if($this->getValue()) {
			$field->setValue($this->getValue());
			$rows = $this->ae->getRows();
				
			$ids = array();
			if($rows) {
				require_once "Bof/Utils.php";
				$ids = Utils::extractColumn($rows, $this->thisTableField->getName());
				$select->where($this->dbTable->getName().".id IN (?)", $ids);
			} else {
				$select->where("1 = 0");
			}
		}
	}


}

?>