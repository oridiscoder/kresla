<?php
require_once "DbFieldAbstract.php";

class PasswordField extends DbFieldAbstract {

	
	public function extractValue($dataRow) {
		return null;
	}
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	function renderCell($cell) {
		return null;
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	function renderFilter() {
		return null;
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	function renderEdit($dataRow) {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="password" name="'.$this->getName().'" value="'.$escapedValue.'" />';
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	*/
	public function prepareSelect($select) {
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	*/
	public function prepareDataForSave($data) {
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		// TODO Auto-generated method stub
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	*/
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` VARCHAR(255)";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}

}

?>