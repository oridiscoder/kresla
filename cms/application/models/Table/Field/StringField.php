<?php
require_once "DbFieldAbstract.php";

class StringField extends DbFieldAbstract {
	
	private $summarValue = 0;
	private $summarIsNumber = true;

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	public function renderFilter() {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$this->comparisonOperation->getSign().$escapedValue.'" />';
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$escapedValue.'" />';
	}
	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		$value = $this->extractValue($dataRow);
		if($this->summarIsNumber && is_numeric($value) || empty($value)) {
			$this->summarValue += $value;
		} else {
			$this->summarIsNumber = false;
		}
		
		$value = is_float($value) ? number_format($value, 2) : $value;
		$value = strip_tags($value);
		$length = mb_strlen($value);
		if($length > 50) {
			$firstPart = mb_substr($value, 0, 50);
			$secondPart = mb_substr($value, 50);
			return $firstPart.'<div class="hidden_text">'.$secondPart.'</div><a href="#" class="show_hidden_text">...</a>';
		} else {
			return $value;
		}
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	*/
	public function prepareDataForSave($data) {
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		$baseTableName = $this->getMyTableName();
		if($this->value !== null) {
			$operator = $this->comparisonOperation->getMySqlComparisonOperator();
			$value = $this->comparisonOperation->getValueForSelect();
			$select->where($baseTableName.".`".$this->getName()."` $operator ?", $value);
		}
		if($this->sorting) {
			$select->order($baseTableName.".".$this->getName()." ".$this->sorting);
		}
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	*/
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` VARCHAR(255)";
	}
	
	public function total($rows = null) {
		if($this->summarIsNumber) {
			return number_format($this->summarValue, 2);
		} else {
			return "&nbsp;";
		}
		
	}


}

?>