<?php

require_once ('VirtualColumnHandler.php');

class VCLtSiteCost extends VirtualColumnHandler {
	 
	public function __construct() {
		require_once 'Bof/LtSiteCost.php';
	}
	/* (non-PHPdoc)
	 * @see VirtualColumnHandler::getValue()
	 */
	public function getValue($rowData) {
		return LtSiteCost::cost($rowData);
	}

}

?>