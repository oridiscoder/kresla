<?php

abstract class VirtualColumnHandler {
	/**
	 *
	 * @var VirtualColumn
	 */
	private $field;
	
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	public function setField($f) {
		$this->field = $f;
	}
	public function getField() {
		return $this->field;
	}
	public function setDbAdapter($db) {
		$this->db = $db;
	}
	
	abstract public function getValue($rowData);
}

?>