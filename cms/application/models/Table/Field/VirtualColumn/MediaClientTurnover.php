<?php

require_once ('VirtualColumnHandler.php');

class MediaClientTurnover extends VirtualColumnHandler {
	
	/* (non-PHPdoc)
	 * @see VirtualColumnHandler::getValue()
	 */
	public function getValue($rowData) {
		$turnover = 0;
		$clientID = $rowData['client_id'];
		if(is_numeric($clientID)) {
			$select = $this->db->select()
				->from("dogovors", array('id'))
				->where("company_id = ?", $clientID)
				->where("service_type = 157")	//продажа медийки
				->where("hidden = 0");
			$ids = $this->db->fetchCol($select);
			
			if($ids) {
				$select = $this->db->select()
					->from("invoices", array("summ" => 'SUM(cost)'))
					->where("dogovor_id IN (?)", $ids)
					->where("hidden = 0");
				$turnover = $this->db->fetchOne($select);
				$turnover = $turnover ? $turnover : 0;
			}
		}

		return $this->formatCost($turnover);
	}
	
	private function formatCost($cost) {
		return number_format($cost, 2, '.', ' ');
	}
}

?>