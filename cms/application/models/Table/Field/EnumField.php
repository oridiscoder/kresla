<?php
require_once "DbFieldAbstract.php";

class EnumField extends DbFieldAbstract {
	
	
	protected $enumsTableName = "bof_enums";
	protected $enums;
	
	public function __construct($field) {
		parent::__construct($field);
		
		$enumCategoryID = $this->field['param'];
		if(empty($enumCategoryID)) {
			throw new DbFieldException("Enum section not set for column {$this->name}");
		}
		$select = $this->dbAdapter->select()
			->from($this->enumsTableName)
			->where("enum_id = ?", $enumCategoryID)
			->where("hidden = 0")
			->order("number");
		$enums = $this->dbAdapter->fetchAll($select);
		
		if(!empty($enums)) {
			foreach ($enums as $enum) {
				$this->enums[$enum['id']] = $enum;
			}
		} else {
			$this->enums = array();
		}
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	function renderFilter() {
		
		$select = '<select name="'.$this->name.'">{options}</select>';
		$options = "<option value=\"\">Не выбрано</option>\n";
		foreach($this->enums as $enum) {
			$options .= '<option value="'.$enum['id'].'" '.($this->value == $enum['id'] ? 'selected="selected"' : '').'>'.strip_tags($enum['name']).'</option>'."\n";
		}
		foreach($this->enums as $enum) {
			$options .= '<option value="-'.$enum['id'].'" '.($this->value == -$enum['id'] ? 'selected="selected"' : '').'>Кроме '.strip_tags($enum['name']).'</option>'."\n";
		}
		return str_replace("{options}", $options, $select);
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	function renderEdit($dataRow) {
		$selectHTML = '<select name="'.$this->getName().'">{options}</select>';
		if($this->allowEmptyValue()) {
			$options = '<option value="0">Не выбрано</option>'."\n";
		} else {
			$options = '';
		}
		if(!empty($this->enums)) foreach($this->enums as $enum) {
			$options .= '<option value="'.$enum['id'].'" '.($this->value == $enum['id'] ? 'selected="selected"' : '').'>'.strip_tags($enum['name']).'</option>'."\n";
		}
		return str_replace("{options}", $options, $selectHTML);
	}
	
	public function extractValue($dataRow) {
		$fieldName = "enum_".$this->getID().'_name';
		return $dataRow[$fieldName];
	}
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	function renderCell($dataRow) {
		return $this->extractValue($dataRow);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	*/
	public function prepareSelect($select) {
		$enumTableShortName = "enum_".$this->getID();
		$baseTableName = $this->getMyTableName();
		$select->joinLeft(
			array($enumTableShortName => $this->enumsTableName),
			"`$enumTableShortName`.`id` = `$baseTableName`.`{$this->getName()}`",
			array($enumTableShortName.'_name' => 'name')
		);
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		$enumTableShortName = "enum_".$this->id;
		$baseTableName = $this->getMyTableName();
		if(!empty($this->value)) {
			if($this->value >= 0) {
				$select->where($enumTableShortName.".id = ?", $this->value);
			} else {
				$select->where($enumTableShortName.".id <> ?", -$this->value);
			}
		}
		
		if($this->sorting) {
			$select->order($enumTableShortName.".name ".$this->sorting);
		}
		
	}
	
	public function getEnumByName($name) {
		foreach ($this->enums as $id=>$enum) {
			if($enum['name'] == $name) {
				return $enum;
			}
		}
		return null;
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		// TODO Auto-generated method stub
		return "`{$this->getName()}` INTEGER";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}
	
	protected function allowEmptyValue() {
		return !($this->params && isset($this->params["NOTNULL"]));
	}


}

?>