<?php
require_once "DbFieldAbstract.php";

class DateField extends DbFieldAbstract {
	

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	public function renderFilter() {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$this->comparisonOperation->getSign().$escapedValue.'" />';
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		if(!isset($dataRow['id'])) {
			$timeValue = time();
		} else {
			$timeValue = strtotime($this->value);
		}
		$inputValue = strftime("%Y-%m-%d", $timeValue);
		if($this->showTime()) {
			list($hour, $minute, $second) = explode(":", strftime("%H:%M:%S", $timeValue));
			$html = '
			<input type="text" name="'.$this->getName().'[date]" value="'.$inputValue.'" class="datepicker" style="width:198px;"/>
			<select name="'.$this->getName().'[hour]" style="width:50px;">
			'.$this->getHoursHTML($hour).'
			</select>
			<select name="'.$this->getName().'[minute]" style="width:50px;">
			'.$this->getMinutesHTML($minute).'
			</select>
			<select name="'.$this->getName().'[second]" style="width:50px;">
			'.$this->getSecondsHTML($second).'
			</select>
			';
		} else {
			$html = '<input type="text" name="'.$this->getName().'" value="'.$inputValue.'" class="datepicker" />';
		}
		
		$html .= '<button onclick="dateSetNow(this);return false;">Сейчас</button>';
		return $html;
	}
	private function getHoursHTML($hour) {
		$hours = "";
		for($i = 0; $i < 23; $i++) {
			$selected = $hour == $i ? 'selected="selected"' : '';
			$hours .= sprintf('<option value="%s" %s>%02s</option>', $i, $selected, $i);
			//$hours .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
		}
		return $hours;
	}
	private function getMinutesHTML($minute) {
		$hours = "";
		for($i = 0; $i < 60; $i++) {
			$selected = $minute == $i ? 'selected="selected"' : '';
			$hours .= sprintf('<option value="%s" %s>%02s</option>', $i, $selected, $i);
		}
		return $hours;
	}
	private function getSecondsHTML($second) {
		$hours = "";
		for($i = 0; $i < 60; $i++) {
			$selected = $second == $i ? 'selected="selected"' : '';
			$hours .= sprintf('<option value="%s" %s>%02s</option>', $i, $selected, $i);
		}
		return $hours;
	}
	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		$dateValue = $this->extractValue($dataRow);
		
		if(!empty($dateValue)) {
			$dateTime = strtotime($dateValue);
			$mask = $this->getDateMask() ? $this->getDateMask() : "%d.%m.%Y";
			$dateValue = strftime($mask, $dateTime);
		}		
		
		return $dateValue;
	}
	
	private function getDateMask() {
		return !empty($this->field['param2']) ? $this->field['param2'] : null;
	}
	
	public function showTime() {
		return $this->field['param'] == 'time';
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		if($this->showTime()) {
			$date = $this->extractValue($data);
			if(is_array($date)) {
				$data[$this->getName()] = sprintf("%s %s:%s:%s", $date['date'], $date['hour'], $date['minute'], $date['second']);
			}
		}
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		if($this->value) {
			if(strpos($this->value, '-')) {
				$parts = explode("-", $this->value);
				$from = strftime("%Y-%m-%d", strtotime(trim($parts[0])));
				$to = strftime("%Y-%m-%d", strtotime(trim($parts[1])));
				$select->where($this->getMyTableName().'.`'.$this->getName().'` BETWEEN DATE('.$this->dbAdapter->quote($from).') AND DATE('.$this->dbAdapter->quote($to).')');
			} elseif(strpos($this->value, '*') !== false) {
				$parts =  explode(".", $this->value);
				if(count($parts) == 3) {
					foreach ($parts as &$p) {
						$p = $p == '*' ? '.*' : preg_quote($p);
					}
					list($day, $month, $year) = $parts;
					$regexp = '^'.$year.'-'.$month.'-'.$day;
					$select->where($this->getMyTableName().'.`'.$this->getName().'` REGEXP ?', $regexp);
				}				
			} else {
				$date = strftime("%Y-%m-%d", strtotime($this->value));
				$mySqlComparisonOperator = $this->comparisonOperation->getMySqlComparisonOperator();
				if($this->showTime()) {
					$select->where('DATE('.$this->getMyTableName().'.`'.$this->getName().'`) '.$mySqlComparisonOperator.' DATE(?)', $date);
				} else {
					$select->where($this->getMyTableName().'.`'.$this->getName().'` '.$mySqlComparisonOperator.' DATE(?)', $date);
				}
				
			}
		}
		if($this->sorting) {
			$select->order($this->getMyTableName().'.'.$this->getName()." ".$this->sorting);
		}
		
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		
		return "`{$this->getName()}` DATETIME";
		
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}

	
	

}

?>