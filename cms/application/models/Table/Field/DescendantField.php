<?php
require_once "DbFieldAbstract.php";

class DescendantField extends DbFieldAbstract {
	
	/**
	 * Массив полей-потомков.
	 * Последний элемент массива [всегда] является не потомком.
	 * @todo Переделать по алгоритму: последний элемент должен быть любого типа DbField,
	 * напрмер, перечень, изображение, файл...
	 * @var array
	 */
	protected $joinFields;
	
	public function __construct($field) {
		parent::__construct($field);
		$this->joinFields = $this->getJoinFields();
	}
	
	protected function getJoinFields() {
		$stop = false;
		
		$field = $this->field;
		unset($field['_table_object']);
		
		$joinFields = array();
		
		while(!$stop) {
			
			$lastField = end($joinFields);
			if($lastField) {
				$field['table'] = $lastField['parentTable'];
			} else {
				$field['table'] = $this->getTableByID($field['obj_id']);
			}
			
			$field['parentTable'] = $this->getTableByID($field['param']);
			$joinFields[$field['id']] = $field;
			
			$field = $this->getTableFirstField($field['param']);
			
			if($field['type'] != DbFieldAbstract::TYPE_DESCENDANT) {
				$field['parentTable'] = null;
				$field['table'] = $this->getTableByID($field['obj_id']);
				$field['_db_adapter'] = $this->dbAdapter;
				$field['fieldObject'] = self::factory($field);
				$field['fieldObject']->setDbAdapter($this->dbAdapter);
				
				$joinFields[$field['id']] = $field;
				$stop = true;
			} elseif(isset($joinFields[$field['id']])) {
				$fNames = array();
				foreach ($joinFields as $jf) {
					$fNames[] = $jf['name'];
				}
				throw new DbFieldException("Recursion detected on field chain: ".implode(",", $fNames));
			}
		}
		return array_values($joinFields);
	}

	/**
	 * Получить id непосредственно родительской таблицы для данного поля.
	 * @return integer
	 * @throws DbFieldException
	 */
	public function getParentTableId() {
		if(isset($this->joinFields[0]['parentTable'])) {
			return $this->joinFields[0]['parentTable']['id'];
		} else {
			throw new DbFieldException("Field ".$this->getName()." has not parent table");
		}
	
	}
	
	/**
	 * Получить имя непосредственно родительской таблицы для данного поля.
	 * @return string
	 * @throws DbFieldException
	 */
	public function getParentTableName() {
		if(isset($this->joinFields[0]['parentTable'])) {
			return $this->joinFields[0]['parentTable']['name'];
		} else {
			throw new DbFieldException("Field ".$this->getName()." has not parent table");
		}
		
	}
	
	protected function getLastJoinDescendantField() {
		$total = count($this->joinFields);
		for($i = $total - 1; $i >=0; $i--) {
			if($this->joinFields[$i]['type'] == self::TYPE_DESCENDANT) {
				return $this->joinFields[$i];
			}
		}
	}
	
	protected function getTableFirstField($tableID) {
		$select = $this->dbAdapter->select()
			->from(self::TABLE_NAME)
			->where("obj_id = ?", $tableID)
			->where("hidden = 0")
			->order("number")
			->limit(1);
		return $this->dbAdapter->fetchRow($select);
	}
	
	protected function getTableFirstDescendantField($tableID) {
		$select = $this->dbAdapter->select()
			->from(self::TABLE_NAME)
			->where("obj_id = ?", $tableID)
			->where("hidden = 0")
			->where("type = ?", self::TYPE_DESCENDANT)
			->order("number")
			->limit(1);
		return $this->dbAdapter->fetchRow($select);
	}
	
	protected function getTableDescendantFieldByName($tableID, $fieldName) {
		$select = $this->dbAdapter->select()
			->from(self::TABLE_NAME)
			->where("obj_id = ?", $tableID)
			->where("hidden = 0")
			->where("type = ?", self::TYPE_DESCENDANT)
			->where("name = ?", $fieldName)
			->order("number")
			->limit(1);
		return $this->dbAdapter->fetchRow($select);
	}
	
	protected function getTableFirstStaticTypeField($tableID) {
		$select = $this->dbAdapter->select()
			->from(self::TABLE_NAME)
			->where("obj_id = ?", $tableID)
			->where("hidden = 0")
			->where("type IN  (?)", array(
				self::TYPE_STRING,
				self::TYPE_EMAIL,
				self::TYPE_HTML,
				self::TYPE_TEXT,
				self::TYPE_LINK
			))
			->order("number")
			->limit(1);
		return $this->dbAdapter->fetchRow($select);
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	*/
	public function prepareSelect($select) {
		
		$joinFieldsCount = count($this->joinFields);
		
		for($i = 0; $i < $joinFieldsCount - 1; $i ++) {
			$jField = $this->joinFields[$i];
			//$parentShortTableName = 'parent_'.$jField['id'];
			$parentShortTableName = 'parent_'.$this->id.'_'.$jField['id'];
			$parentTableName = $jField['parentTable']['name'];
			$parentTableFirstField = $this->joinFields[$i+1];
			if($i == 0) {
				//$currentTableName = $jField['table']['name']; - ошибка при virtual_column
				$currentTableName = $this->getMyTableName();
			} else {
				$currentTableName = 'parent_'.$this->id.'_'.$this->joinFields[$i-1]['id'];
			}
			$currentFieldName = $jField['name'];
			
			//echo $select."\n";
			
			$select->joinLeft(
				array($parentShortTableName => $parentTableName),
				$parentShortTableName.".id = $currentTableName.$currentFieldName",
				array($parentShortTableName.'_'.$currentFieldName => $parentTableFirstField['name'])
			);
		}
		
		/* @var $lastField DbFieldAbstract */;
		$lastField = end($this->joinFields);
		$lastField = $lastField['fieldObject'];
		
		$lastField->setTableName($parentShortTableName);
		$lastField->prepareSelect($select);
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	*/
	public function makeRestrictions($select) {
		
		$joinFieldsCount = count($this->joinFields);
		
		$jField = $this->joinFields[$joinFieldsCount - 2];
		
		$parentShortTableName = 'parent_'.$this->id.'_'.$jField['id'];
		$parentTableName = $jField['parentTable']['name'];
		$parentTableFirstField = $this->joinFields[$joinFieldsCount - 1];
		
		if($joinFieldsCount == 2) {	// имеем только одного parent (нет проброса)
			$currentTableName = $jField['table']['name'];
		} else {
			//$currentTableName = 'parent_'.$this->joinFields[$joinFieldsCount - 1]['id'];
			$currentTableName = 'parent_'.$this->id.'_'.$this->joinFields[$joinFieldsCount - 1]['id'];
		}
		$currentFieldName = $jField['name'];
		
		//$parentShortTableName = 'parent_'.$this->field['id']; по-моему, эту строку можно спокойно удалять, если фильтрация работает
		
		/* @var $lastField DbFieldAbstract */
		$lastField = end($this->joinFields);
		$lastField = $lastField['fieldObject'];
			
		$lastField->setTableName($parentShortTableName);
		$lastField->setSortingType($this->getSortingType());
		if($this->getValue() !== null) {
			$lastField->setValue($this->comparisonOperation->getSign().$this->getValue());
		}
		
		$lastField->makeRestrictions($select);
		
		if($this->dbTable && ($constraints = $this->dbTable->getParentTableConstraints()) != null) {
			if($constraints['field']['id'] == $this->id) {
				$select->where($this->dbTable->getName().'.'.$this->getName().' = ?', $constraints['value']);
			}
		}
		
	}
	
	public function extractValue($dataRow) {
		/* @var $field DbFieldAbstract */
		$field = end($this->joinFields);
		$field = $field['fieldObject'];
		
		$joinFieldsCount = count($this->joinFields);
		
		if($joinFieldsCount == 2) {
			$jField = $this->joinFields[$joinFieldsCount - 2];
			$dataRow['id'] = $dataRow[$jField['name']];
			$dataRow[$field->getName()] = $dataRow['parent_'.$this->id.'_'.$jField['id'].'_'.$jField['name']];
				
		} else {
			$jField = $this->joinFields[$joinFieldsCount - 3];
			$jNextField = $this->joinFields[$joinFieldsCount - 2];
		
			$dataRow['id'] = $dataRow['parent_'.$this->id.'_'.$jField['id'].'_'.$jField['name']];
			$dataRow[$field->getName()] = $dataRow['parent_'.$this->id.'_'.$jNextField['id'].'_'.$jNextField['name']];
		}
		return $field->extractValue($dataRow);
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		/* @var $field DbFieldAbstract */
 		$field = end($this->joinFields);
		$field = $field['fieldObject'];
		
		$joinFieldsCount = count($this->joinFields);
		
		$recordID = $dataRow['id'];
		
		if($joinFieldsCount == 2) {
			$jField = $this->joinFields[$joinFieldsCount - 2];
			$key = 'parent_'.$this->id.'_'.$jField['id'].'_'.$jField['name'];
			$dataRow['id'] = $dataRow[$this->getName()];
			$dataRow[$field->getName()] = isset($dataRow[$key]) ? $dataRow[$key] : null;
		} else {
			$jField = $this->joinFields[$joinFieldsCount - 3];
			$jNextField = $this->joinFields[$joinFieldsCount - 2];
			
			$key = 'parent_'.$this->id.'_'.$jNextField['id'].'_'.$jNextField['name'];
			$dataRow['id'] = $dataRow['parent_'.$this->id.'_'.$jField['id'].'_'.$jField['name']];
			$dataRow[$field->getName()] = isset($dataRow[$key]) ? $dataRow[$key] : null;
		}
		$cellHTML =  $field->renderCell($dataRow);
		$cellHTML .= '<span class="home_ico2" rel="'.$recordID.','.$this->getID().'"></span>';
		return $cellHTML;
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	function renderFilter() {
		return '<input type="text" name="'.$this->getName().'" value="'.$this->comparisonOperation->getSign().addslashes($this->value).'" />';
	}

	/* 
	 * Строим элемент редактирования поля Потомок. 
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	function renderEdit($dataRow) {
		
		BuildTimer::timing('descendant-start-'.$this->getName());
		
		$selectHTML = '<select name="'.$this->getName().'">{options}</select>';
		
		if($this->allowEmptyValue()) {
			$emptyValueOption = '<option value="0">Не указано</option>'."\n";
		} else {
			$emptyValueOption = '';
		}
		
		/* будем использовать класс DbTable, чтоб не запариваться ещё раз с "потомками, которые указывают на потомки и т.д." */
		$parentTable = new DbTable($this->dbAdapter, $this->getParentTableId());
		/* берем первое поле из родительской таблицы. Его значение и будем отображать в <option> */
		$first = $parentTable->getFirstField();
		
		if($this->isTreeSelect()) {
			/*
			 * Если надо показывать в виде дерева, то найдем поле потомок,
			* ссылающееся на эту же таблицу
			*/
			$treeField = null;
			foreach($parentTable->getFields() as $field) {
				if($field instanceof DescendantField) {
					if($field->getParentTableId() == $parentTable->getID()) {
						$treeField = $field;
						break;
					}
				}
			}
		}
		
		if($this->sectionsEnable()) {
			/*
			 * Если надо разделить <select> на группы, то найдем первого потомка, 
			 * указывающего на другую таблицу
			 */
			$sectionsField = null;
			foreach($parentTable->getFields() as $field) {
				if($field instanceof DescendantField) {
					if($field->getParentTableId() != $parentTable->getID()) {
						$sectionsField = $field;
						break;
					}
				}
			}
		}
		
		/* убираем из селекта ненужные поля */
		foreach ($parentTable->getFields() as $field) {
			if($field != $first && !($field instanceof IdField) && $field->getName() != $this->field['sort_column']) {
				if(!isset($treeField) || $field != $treeField) {
					if(!isset($sectionsField) || $field != $sectionsField) {
						$field->setHidden(true);
						$field->hide();
					}
				}
			}
		}
		
		if($this->field['sort_column']) {
			$parentTable->setOrderFieldName($this->field['sort_column'], 'ASC');
		} else {
			/* @TODO number может не существовать */
			$parentTable->setOrderFieldName('number', 'DESC');
		}
		
		//в WHERE у нас можно задавать условия fieldname => value
		if($this->whereSQL) {
			$sql = $this->whereSQL;
			if($dataRow) {
				foreach ($dataRow as $key => $value) {
					$sql = str_replace('{'.$key.'}', $value, $sql);
				}
			}
			$parentTable->setWhereSQL($sql);
		}
		
		//надо учитывать $constraints
		if(!isset($dataRow['id']) &&  !$dataRow['id'] && ($constraints = $this->dbTable->getParentTableConstraints()) != null) {
			if($constraints['field']['id'] == $this->id) {
				$this->value = $constraints['value'];
			}
		}
		
		
		
		$rows = $parentTable->getRows();
		
		/* если ссылаемся на ту же таблицу, то уберем из списка самого себя, чтоб не зациклиться */
		if($this->getTableID() == $parentTable->getID()) {
			foreach ($rows as $key => $row) {
				if($row['id'] == $dataRow['id']) {
					unset($rows[$key]);
				}
			}
		}

		if(isset($treeField)) {
			$rows = $this->getTree($rows, $treeField);
		}		
		
		if(isset($sectionsField)) {			
			$rows = $this->makeSections($rows, $parentTable, $sectionsField);			
			$optionsHTML = $this->compileGrandParentTreeSelect($rows, $first);
		} else {
			$optionsHTML = $this->compileTreeSelect($rows, $first);
		}
				
		BuildTimer::timing('descendant-stop-'.$this->getName());
		return str_replace('{options}', $emptyValueOption.$optionsHTML, $selectHTML);
	}
	
	/**
	 * 
	 * @param array $items
	 * @param DescendantField $field
	 */
	public function getTree($items, $field) {
		$parents = array();
		$tree = array();
		foreach ($items as $row) {
			$parents[$row['id']] = $row[$field->getName()];
		}
		
		foreach ($items as $item) {
			$id = $item['id'];
			$keys = $this->getTreeKeys($id, $parents);
			$keys = array_reverse($keys);
			$branch = &$tree;
			$keysLength = count($keys);
			foreach ($keys as $k => $key) {
				if(!isset($branch[$key])) {
					$branch[$key] = array(
						'_childs' => array()
					);
				}					
				if($k + 1 == $keysLength) {
					if(isset($branch[$key]['_childs'][$id])) {
						$branch[$key]['_childs'][$id] = array_merge($branch[$key]['_childs'][$id], $item);
					} else {
						$branch[$key]['_childs'][$id] = $item;
					}
				}
				$branch = &$branch[$key]['_childs'];
			}
		}

		$top = array_shift($tree);
		
		return $top['_childs'];
	}
	
	public function getTreeKeys($id, $parents) {
		$keys = array();
		while(array_key_exists($id, $parents)) {
			/* не допустим бесконечного цикла! */			
			if(!in_array($parents[$id], $keys)) {
				$keys[] = (int) $parents[$id];
				$id = $parents[$id];
			}
		}
		
		return $keys;
	}
	
	private function compileTreeSelect($items, $field, $deep = 0) {
		
		$prefix = str_repeat("-", $deep);
		$prefix = !empty($prefix) ? $prefix.' ' : $prefix;
		
		$options = "";
		
		foreach ($items as $item) {
			$options .= '<option value="'.$item['id'].'" '.($this->value == $item['id'] ? 'selected="selected"':'').'>'.$prefix.$field->renderCell($item).'</option>'."\n";
			if(isset($item['_childs'])) {
				$options .= $this->compileTreeSelect($item['_childs'], $field, $deep + 1);
			}
		}
		
		return $options;
	}
	
	
	/**
	 * Раскидать $items по группам, соответствующим записям из родительской для $parentTable таблицы 
	 * @param array $items
	 * @param DbTable $parentTable
	 * @param DescendantField $field
	 */
	protected function makeSections($items, $parentTable, $field) {
		
		$ids = array();
		foreach($items as $item) {
			$parentID = $item[$field->getName()];
			if(!in_array($parentID, $ids)) {
				$ids[] = $parentID;
			}
		}
		
		$grandParentTable = new DbTable($this->dbAdapter, $field->getParentTableId());
		$grandParentTable->getFields('id')->setValue(implode(',', $ids));
		$grandParentTable->setFieldValue('hidden', 0);
				
		$rows = $grandParentTable->getRows();
		
		$options = array();
		foreach ($rows as $row) {
			$options[$row['id']] = array('title' => $grandParentTable->getFirstField()->extractValue($row));
		}
		
		foreach ($items as $k => $item) {
			$sectionID = $item[$field->getName()];
			$options[$sectionID]['_childs'][] = $item;
		}
		
		return $options;
	}
	
	private function compileGrandParentTreeSelect($grandParentItems, $field) {
		$options = "";
		
		foreach ($grandParentItems as $grandParentItem) {
			$optgroup = '<optgroup label="'.$grandParentItem['title'].'">{options}</optgroup>'."\n";
			$optgroupOptions = $this->compileTreeSelect($grandParentItem['_childs'], $field)."\n";			
			$options .= str_replace("{options}", $optgroupOptions, $optgroup);
		}
		
		return $options;
	}
	

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $data;
	}

	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
	
	public function allowEmptyValue() {
		return !($this->params && isset($this->params["NOTNULL"]));
	}
	
	protected function isTreeSelect() {
		return $this->params && isset($this->params["TREE"]);
	}
	/*
	 * Включить <optgroup> в <select>
	 */
	protected function sectionsEnable() {
		return $this->params && isset($this->params["SECTIONS"]);
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		
		return "`{$this->getName()}` INTEGER DEFAULT NULL";
		
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}
	
	public function saveDOM() {
		$dom = parent::saveDOM();
		/* При сохранении в DOM актуальность ID таблиц теряется. 
		 * Чтоб не потерять связь, мы заменяем ID таблицы на её название
		 */
		$paramNode = $dom->getElementsByTagName("param")->item(0);
		$textNode = $dom->createTextNode($this->getParentTableName()); 
		$paramNode->replaceChild($textNode, $paramNode->firstChild);
		return $dom;
	}


}

?>