<?php
require_once ('DbFieldAbstract.php');

class FileField extends DbFieldAbstract {
	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		$value = $this->extractValue($dataRow);
		if(!empty($value)) {
			$config = Application::getAppConfigs();
			$href = $config->files->url.'/'.$this->getMyTableName().'/'.$dataRow['id'].'_'.$this->getName().'_'.$value;
			$html = '<a href="'.$href.'">'.$value.'</a>';
		} else {
			return '<span class="no-photo">Нет файла</span>';
		}
		return $html;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		$html =  '<input type="file" name="'.$this->getName().'" />';
		if(!empty($this->value)) {
			$html .= '<br /><input id="FileDelete'.$this->getName().'" type="checkbox" name="'.$this->getName().'_delete" /> <label for="FileDelete'.$this->getName().'">Удалить файл</label>';
		}
		return $html;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	*/
	public function makeRestrictions($select) {
		if($this->sorting) {
			$select->order($this->getMyTableName().'.'.$this->getName()." ".$this->sorting);
		}
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		if($this->fileUploaded()) {
			$fileName = $this->normilizeFileName($_FILES[$this->getName()]['name']);
			$data[$this->getName()] = $fileName;
		} elseif ($this->isDeleteCommand($data)) {
			$data[$this->getName()] = null;
		}
		return $data;
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		if($this->fileUploaded()) {
			$config = Application::getAppConfigs();
			if(!isset($config->files->upload_dir)) {
				throw new DbFieldException("Section files.upload_dir not set in application config");
			}
			
			$this->deleteOldFieldFiles($data['id']);
			
			$fileName = $data[$this->getName()];
			$prefix = $data['id'].'_'.$this->getName();
			$source = $_FILES[$this->getName()]['tmp_name'];
			$destination = $config->files->upload_dir.'/'.$this->getMyTableName().'/'.$prefix.'_'.$fileName;
			
			$this->moveFile($source, $destination);
			
		} elseif($this->isDeleteCommand($data)) {
			$this->deleteOldFieldFiles($data['id']);
		}
	}
	
	protected function fileUploaded() {
		if(isset($_FILES[$this->getName()]) && $_FILES[$this->getName()]['error'] == UPLOAD_ERR_OK) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function normilizeFileName($name) {
		require_once 'Bof/Transliteratorer.php';
		$name = Transliteratorer::toTranslit($name);
		$name = preg_replace("[^-_a-zA-Z0-9]", "", $name);
		return $name;
	}
	
	protected function isDeleteCommand($data) {
		return isset($data[$this->getName().'_delete']);
	}
	
	protected function deleteOldFieldFiles($id) {
		$config = Application::getAppConfigs();
		$uploadDir = $config->files->upload_dir.'/'.$this->getMyTableName();
		if(file_exists($uploadDir)) {
			$dirHandler = opendir($uploadDir);
			$filePrefix = $id.'_'.$this->getName();
			if($dirHandler) {
				while(($fileName = readdir($dirHandler)) != false) {
					if($fileName != '.' && $fileName != '..') {
						if(preg_match('/^'.$filePrefix.'/', $fileName)) {
							unlink($uploadDir.'/'.$fileName);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Перенести файл из временной папки в место его постоянной дислокации
	 * @param string $source
	 * @param string $destination
	 * @throws DbFieldException
	 */
	protected function moveFile($source, $destination) {
		$this->uploadExceptions = null;
		$destinationFolder = dirname($destination);
		set_error_handler(array($this, 'catchUploadExceptios'));
		if(!file_exists($destinationFolder)) {
			mkdir($destinationFolder, 0777);
		}
		//$success = move_uploaded_file($source, $destination);	- потому что при групповых операциях файл должен оставаться в временной папке
		$success = copy($source, $destination);
	
		if(!$success) {
			throw new DbFieldException("Can't upload file to $destination. Has got an errors: \n".implode("\n", $this->uploadExceptions));
		}
	}
	
	public function catchUploadExceptios($errNo, $errStr) {
		$this->uploadExceptions[] = $errStr;
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` VARCHAR(255)";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}
	

}

?>