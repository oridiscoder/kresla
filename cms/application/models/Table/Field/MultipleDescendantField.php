<?php

require_once ('DescendantField.php');

/**
 *
 * @author Gourry
 * Поле типа "Неявный потомок" или - по другому -  "Множественный потомок".
 * Предназначено для того, чтоб у каждой строки индивидуально указыать,
 * какая таблица у неё является родителем. Например, статья, может быть прикреплена к товару, стране и т.д.
 *
 *
 */
class MultipleDescendantField extends DbFieldAbstract {
	
	private $mdTables = null;
	private $mdTablesFirstFields = null;
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::extractValue()
	 */
	public function extractValue($dataRow) {
		$mdTables = $this->getMDTables();
		$mdFirstFields = $this->getMDTablesFirstFields();
		
		foreach ($mdTables as $mdKey => $mdTable) {
			if($mdTable['table_id'] == $dataRow[$this->getName().'__table']) {
				
				$mdField = $mdFirstFields[$mdKey];
				$mdTableAlias = $this->getName().'_'.$mdTable['table_name'];
				$mdTableFirstFieldAlias = $mdTableAlias.'_value';
				
				$dataRow[$mdField->getInitialName()] = $dataRow[$mdTableFirstFieldAlias];
				
				$mdField->setTableName($mdTableAlias);
				$mdField->setName($mdTableFirstFieldAlias);
				
				return $mdField->extractValue($dataRow);
			}
		}
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		return $this->extractValue($dataRow);
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		return '<input type="text" name="'.$this->getName().'" value="'.htmlspecialchars($this->getValue()).'" />';
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		/*
		 * Необходимо сформировать 2 селекта:
		 * 1. Селект для выбора тиблицы-родителя
		 * 2. Селект для выбора Элемента-родителя из выбранной таблицы
		 */
		
		/* @var $mdTables array - Разрешенный список возможный таблиц-родителей (bof_field_multiple_descendant)*/
		$mdTables = $this->getMDTables();

		
		//формируем select для выбора таблицы-родителя
		$selectHTMLTemplate = '<select name="{name}" onchange="loadMDTableVariants(this);">{options}</select>';
		$optionHTMLTemplate = '<option value="{value}" {selected}>{viewname}</option>';
		
		$chosenTableID = $dataRow[$this->getName().'__table'];

		if ($chosenTableID == '' && @$_GET[$this->getName().'__table'])
		  $chosenTableID = $_GET[$this->getName().'__table'];

		$chosenTable = null;
		
		$optionsHTML = '<option value="0">Не выбрано</option>';
		
		if(!empty($mdTables)) foreach ($mdTables as $mdTable) {
			$optionHTML = str_replace("{value}", $mdTable['table_id'], $optionHTMLTemplate);
			$optionHTML = str_replace("{viewname}", $mdTable['table_viewname'], $optionHTML);
			$optionHTML = str_replace("{selected}", $mdTable['table_id'] == $chosenTableID ? 'selected="selected"' : '', $optionHTML);
			$optionsHTML .= $optionHTML;
			
			if($mdTable['table_id'] == $chosenTableID) {
				$chosenTable = $mdTable;
			}
		}
		
		$selectHTML = str_replace('{name}', $this->getName().'__table', $selectHTMLTemplate);
		$selectHTML = str_replace('{options}', $optionsHTML, $selectHTML);

		/*
		 * если текущая запись (при редактировании элемента) уже указыает на какую-то родительскую таблицу,
		 * то необходимо сформировать второй селект, содержащий записи из этой выбранной таблицы,
		 * иначе этот селект будет сформирован AJAX'ом при изменении значения первого селекта.
		 */
		
		if($chosenTable) {
			
			$selectHTMLTemplate = '<select name="{name}">{options}</select>';
			$optionsHTML = '';
			$options = $this->getTableSelectOptions($chosenTable);
			
			foreach ($options as $option) {
				$is_select = ($option['id'] == $this->getValue()) | (@$_GET['type_id__id'] == $option['id']);
				$optionHTML = str_replace("{value}", $option['id'], $optionHTMLTemplate);
				$optionHTML = str_replace("{viewname}", $option['name'], $optionHTML);
				$optionHTML = str_replace("{selected}", $is_select ? 'selected="selected"' : '', $optionHTML);
				$optionsHTML .= $optionHTML;
			}
			
			$rowSelectHTML = str_replace("{name}", $this->getName(), $selectHTMLTemplate);
			$rowSelectHTML = str_replace('{options}', $optionsHTML, $rowSelectHTML);
			
			$selectHTML .= $rowSelectHTML;
		}
		
		return $selectHTML;
	}
	
	/**
	 * При смене значения первого селекта (выбор таблицы-родителя) приходит AJAX - запрос
	 * который ожидает JSON список записей данной таблицы
	 * @param Bof_Request $request
	 * @return string - JSON - encoded data
	 */
	public function processAjaxRequest($request) {
		
		$tableID = $request->getParam("table_id");
		$mdTables = $this->getMDTables();
		
		foreach ($mdTables as $mdTable) {
			if($tableID == $mdTable['table_id']) {
				
				$options = $this->getTableSelectOptions($mdTable);

				if(Application::getAppCharset() == 'windows-1251') {
					foreach ($options as $key=>$option) {
						foreach ($option as $k=>$v) {
							$options[$key][$k] = iconv('cp1251', 'utf-8', $v);
						}
					}
				}
				
				return json_encode($options);
			}
		}
	}
	
	/**
	 * Получить список записей в указанной таблице
	 * @param array $mdTable
	 * @return array $options - массив вида [id, name]
	 */
	protected function getTableSelectOptions($mdTable) {
		
		$options = array();
		if(!$mdTable['notnull']) {
			$options[] = array(
					'id' => 0,
					'name' => 'Не выбрано'
			);
		}
		
		$orderByFieldName = 'number';
		if($mdTable['table_sort_field_id']) {
			$orderByFieldName = $mdTable['sort_field_name'];
		}
		
		$mdTableDbTable = new DbTable($this->dbAdapter, $mdTable['table_id']);
		$mdTableDbTable->setOrderFieldName($orderByFieldName, 'ASC');
		$mdTableDbTable->setRowsOnPage(0);
		
		$rows = $mdTableDbTable->getRows();
		
		$tableFirstFieldObject = $mdTableDbTable->getFirstField();

		$mdTableAlias = $this->getName().'_'.$mdTable['table_name'];
		$mdTableFirstFieldAlias = $mdTableAlias.'_value';
		
		if(!empty($rows)) {
			foreach($rows as $row) {
				$options[] = array(
						'id' => $row['id'],
						'name' =>$tableFirstFieldObject->extractValue($row)
				);
			}
		}
		
		return $options;
	}
	
	/**
	 * Получить первое описанное в bof_fields поле для заданной таблицы
	 * @param integer $tableID
	 * @return array  - ассоциативный массив - результат выборки из бд (bof_fields)
	 */
	protected function getTableFirstField($tableID) {
		$select = $this->dbAdapter->select()
			->from(self::TABLE_NAME)
			->where("obj_id = ?", $tableID)
			->where("hidden = 0")
			->order("number")
			->limit(1);
		return $this->dbAdapter->fetchRow($select);
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $data;
	}
	
	/**
	 * Перед тем, как сохранить данные в БД, мы должны явно указать значение
	 * ЭтоПоле__table - указывающее на таблицу-родитель.
	 * Это необходимо потому, что у нас текущее поле разделяетя на 2:
	 * 1. ЭтоПоле (прим: object_id) - указывающее на родительский элемент
	 * 2. ЭтоПоле__table (прим: object_id__table) - указывающее на родительскую таблицу
	 * Первое поле заданно яявно текущим классом ($this->getName()), а про второе объект DbTable ничего не знает, и учитывать его не будет при сохранении.
	 * @param array
	 * @param array
	 * @return array
	 */
	public function modifyDataBeforeSave($dirty, $pure) {
		$mdTableFieldName = $this->getName().'__table';
		$pure[$mdTableFieldName] = $dirty[$mdTableFieldName];
		return $pure;
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	 */
	public function afterSave($data) {
		// TODO Auto-generated method stub
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` INTEGER";
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::total()
	 */
	public function total($rows = null) {
		return null;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
		//укажем явно, что нам тажке надо выбрать поле ЭтоПоле__table
		$select->columns($this->getMyTableName().'.'.$this->getName().'__table');
		
		$mdTables = $this->getMDTables();
		
		if(!empty($mdTables)) {
			//Будем делать JOIN по всем указанным таблицам. Terrible!
			foreach ($mdTables as $mdTable) {
				$mdTableFirstField = $this->getTableFirstField($mdTable['table_id']);
				
				//echo '<pre>';
				
				$mdTableFirstField['_db_adapter'] = $this->dbAdapter;
				$mdTableFirstFieldObject = self::factory($mdTableFirstField);
				
				$mdTableAlias = $this->getName().'_'.$mdTable['table_name'];
				$mdTableFirstFieldAlias = $mdTableAlias.'_value';
				
				$mdTableFirstFieldObject->setName($mdTableAlias);
				$mdTableFirstFieldObject->setTableName($mdTableAlias);
				
				
				$select->joinLeft(array($mdTableAlias => $mdTable['table_name']),
						"$mdTableAlias.id = ".$this->getMyTableName().'.'.$this->getName(),
						array($mdTableFirstFieldAlias => $mdTableFirstField['name']));
				
				$mdTableFirstFieldObject->prepareSelect($select);
				
				//echo $select;
				//exit();
			}
		}
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		
		if($this->getValue() !== null) {
			$mdTables = $this->getMDTables();
			
			$wheres = array();
			
			/*
			 * Сохраним начальное значение WHERE. Каждое поле будет накладывать своё условие на объект
			 * $select через AND. Мы никак не можем на это повлиять, поэтому мы идем другим путем:
			 * После того, как каждый DbField скажет свое WHERE, мы возьмем array_diff
			 * между начальным и конечным значением WHERE и то, что добавилось после этих строк,
			 * склеим с помощью OR
			 */
			$whereParts = $select->getPart(Zend_Db_Select::WHERE);
			$whereParts = $whereParts ? $whereParts : array();
			
			foreach ($mdTables as $mdTable) {
				$mdTableFirstField = $this->getTableFirstField($mdTable['table_id']);
				$mdTableFirstField['_db_adapter'] = $this->dbAdapter;
				$mdTableFirstFieldObject = self::factory($mdTableFirstField);
				
				$mdTableAlias = $this->getName().'_'.$mdTable['table_name'];
				$mdTableFirstFieldAlias = $mdTableAlias.'_value';
				
				$mdTableFirstFieldObject->setTableName($mdTableAlias);
				//$mdTableFirstFieldObject->setName($mdTableFirstFieldAlias);
								
				
				
				if($this->value !== null) {
					//каждое поле накладывает свое AND WHERE условие на объект $select
					$mdTableFirstFieldObject->setValue($this->comparisonOperation->getSign().$this->getValue());
					$mdTableFirstFieldObject->makeRestrictions($select);
				}
			}
			
			/*
			 * Теперь посмотрим, какие условия были наложены полями
			 * и сделаем условие вида WHERE ($whereParts) AND ($fieldWhere1 OR $fieldWhere2 OR ... OR $fieldWhereN)
			 */
			$parts = $select->getPart(Zend_Db_Select::WHERE);
			$diff = array_diff($parts, $whereParts);
			
			$select->reset(Zend_Db_Select::WHERE);
			if($whereParts) {
				foreach ($whereParts as $where) {
					$where = str_replace("AND", "", $where);
					$select->where($where);
				}
			}
			
			if($diff) {
				foreach ($diff as $key=>$where) {
					$diff[$key] = str_replace("AND", "", $where);
				}
				$select->where(implode(' OR ', $diff));
			}
		}
	}
	
	/**
	 * Получить список разрешенных вариантов таблиц-родителей
	 * @return array - массив вида [table_id,table_sort_field_id,table_name,table_viewname,sort_field_name,sort_field_viewname,notnull,tree]
	 */
	public function getMDTables() {
		if($this->mdTables === null) {
			$select = $this->dbAdapter->select()
				->from(array("MD" => "bof_field_multiple_descendant"))
				->join(array("O" => "bof_objects"), "O.id = MD.table_id", array('table_name' => 'name', 'table_viewname' => 'viewname'))
				->joinLeft(array("F" => 'bof_fields'), "F.id = MD.table_sort_field_id", array('sort_field_name' => 'name', 'sort_field_viewname' => 'viewname', 'sort_field_id' => 'id'))
				->where("MD.field_id = ?", $this->getID())
				->where("MD.hidden = 0")
				->where("O.hidden = 0")
				->where("F.id IS NULL OR F.hidden = 0");
			
			$this->mdTables = $this->dbAdapter->fetchAll($select);
		}
		
		return $this->mdTables;
	}
	
	/**
	 * Получить список первых полей разрешенных вариантов таблиц-родителей
	 * @return array
	 */
	public function getMDTablesFirstFields() {
		
		if(!$this->mdTablesFirstFields) {
			$mdTables = $this->getMDTables();
			$mdTablesFirstFields = array();
			if($mdTables) foreach ($mdTables as $table) {
				$field = $this->getTableFirstField($table['table_id']);
				$field['_db_adapter'] = $this->dbAdapter;
				$mdTablesFirstFields[] = self::factory($field);
			}
			$this->mdTablesFirstFields = $mdTablesFirstFields;
		}
		return $this->mdTablesFirstFields;
	}

}

?>