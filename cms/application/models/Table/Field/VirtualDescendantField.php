<?php

require_once ('DbFieldAbstract.php');

class VirtualDescendantField extends DbFieldAbstract {
	
	private $parentTable;
	private $parentTableField;
	private $linkedField;
	protected $totalValue;
	
	public function __construct($field) {
		parent::__construct($field);
		
		$select = $this->dbAdapter->select()
			->from("bof_fields")
			->where("id = ?", $field['param']);
		$parentTableField = $this->dbAdapter->fetchRow($select);
		$parentTableField['_db_adapter'] = $this->dbAdapter;
		$this->parentTableField = self::factory($parentTableField);
		
		$this->parentTable = $this->dbAdapter->fetchRow("SELECT * FROM bof_objects WHERE id = ?", $this->parentTableField->getTableID());
		
		$select = $this->dbAdapter->select()
			->from("bof_fields")
			->where("id = ?", $field['param2']);
		$this->linkedField = $this->dbAdapter->fetchRow($select);
		
		$this->virtual = true;
	}
	
	/**
	 * @return array
	 */
	public function getParentTable() {
		return $this->parentTable;
	}
	/**
	 * @return DbFieldAbstract
	 */
	public function getParentTableField() {
		return $this->parentTableField;
	}
	
	/**
	 * Получить поле, по которому происходит связь
	 * @return array
	 */
	public function getLinkedField() {
		return $this->linkedField;
	}
	
	public function extractValue($dataRow) {
		$parentShortTableName = 'parent_'.$this->field['id'];
		$currentFieldName = $this->linkedField['name'];
		
		$dataRow[$this->parentTableField->getName()] = $dataRow[$parentShortTableName.'_'.$currentFieldName];
		
		$value = $this->parentTableField->extractValue($dataRow);
		if(is_numeric($value)) {
			$this->totalValue += $value;
		}
		return $value; 
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
			
		$parentShortTableName = 'parent_'.$this->field['id'];
		$currentFieldName = $this->linkedField['name'];
		
		$dataRow[$this->parentTableField->getName()] = $dataRow[$parentShortTableName.'_'.$currentFieldName];
		
		$cell =  $this->parentTableField->renderCell($dataRow);
		$cell = str_replace('rel="'.$dataRow['id'].','.$this->parentTableField->getID().'"', 'rel="'.$dataRow['id'].','.$this->getID().'"', $cell);
		
		$value = strip_tags($cell);
		if(is_numeric($value)) {
			$this->totalValue += $value;
		}
		
		return $cell;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		$this->parentTableField->setValue($this->getValue());
		$this->parentTableField->setName($this->getName());
		return $this->parentTableField->renderFilter();
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		$key = 'parent_'.$this->id.'_'.$this->linkedField['name'];
		if(isset($dataRow[$key])) {
			$dataRow[$this->parentTableField->getName()] = $dataRow[$key];
		} else {
			$dataRow[$this->parentTableField->getName()] = null;
		}		
		//return $this->parentTableField->renderCell($dataRow);
		return $this->renderCell($dataRow);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	 */
	public function afterSave($data) {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::total()
	 */
	public function total($rows = null) {
		return $this->totalValue ? number_format($this->totalValue, 2) : 0;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
			$jField = $this->linkedField;
			$jField['parentTable'] = $this->parentTable;
			
			$parentShortTableName = 'parent_'.$this->field['id'];
			$parentTableName = $jField['parentTable']['name'];
			$parentTableFirstField = $this->parentTableField;
			$currentTableName = $this->dbTable->getName();
			
			$currentFieldName = $jField['name'];
			
			$select->joinLeft(
				array($parentShortTableName => $parentTableName),
				$parentShortTableName.".id = $currentTableName.$currentFieldName",
				array($parentShortTableName.'_'.$currentFieldName => $parentTableFirstField->getName())
			);
			
			$this->parentTableField->setTableName($parentShortTableName);
			$this->parentTableField->prepareSelect($select);
			
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		
		$this->parentTableField->setValue($this->getValue());
		$this->parentTableField->makeRestrictions($select);
	}

}

?>