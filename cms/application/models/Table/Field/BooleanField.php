<?php
require_once "DbFieldAbstract.php";

class BooleanField extends DbFieldAbstract {
	
	/**
	 * Суммарная информация о всех обработанных стоках
	 * @var array
	 */
	private $summarInfo = array(0=>0, 1=>0);
	
	public function extractValue($dataRow) {
		$value = $dataRow[$this->getName()];
		$this->summarInfo[(int)$value] ++;
		return  $value ? 'Да' : 'Нет';
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	function renderCell($dataRow) {
		return $this->extractValue($dataRow);
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	function renderFilter() {
		$this->value = $this->value === null ? null : (int) $this->value;
		$select = '<select name="'.$this->getName().'">'.
				  '<option value="">Все</option>'.
				  '<option value="1" '.($this->value == 1 ? 'selected="selected"' : '').'>Да</option>'.
				  '<option value="0" '.($this->value === 0 ? 'selected="selected"' : '').'>Нет</option>'.
				  '</select>';
		return $select;
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	function renderEdit($dataRow) {
		return '<input type="checkbox" name="'.$this->getName().'" value="1" '.($this->value ? 'checked="checked"' : '').' />';
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		if(!isset($data[$this->getName()])) {
			$data[$this->getName()] = 0;
		}
		return $data;
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	 */
	public function afterSave($data) {
		//ничего не делаем
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		if($this->value !== null) {
			if($this->value) {
				$select->where($this->getMyTableName().'.`'.$this->getName().'` = 1');
			} else {
				$select->where($this->getMyTableName().'.`'.$this->getName().'` = 0');
			}
		}
		
		if($this->sorting) {
			$select->order($this->getMyTableName().'.'.$this->getName()." ".$this->sorting);
		}
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` TINYINT(1) NOT NULL DEFAULT 0";
	}
	
	public function total($rows = null) {
		if($rows) {
			foreach ($rows as $dataRow) {
				$value = $dataRow[$this->getName()];
				$this->summarInfo[(int)$value] ++;
			}
		}
		$positive = $this->summarInfo[1];
		$total = $this->summarInfo[0] + $this->summarInfo[1];
		$result = "&nbsp;";
		if($total) {
			$percents = round(10000 * $positive / $total) / 100;
			$result = $percents .'%';
		}
		
		return $result;
	}


}

?>