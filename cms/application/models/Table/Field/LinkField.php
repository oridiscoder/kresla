<?php

require_once "DbFieldAbstract.php";

class LinkField extends DbFieldAbstract {

	
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	public function renderFilter() {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$this->comparisonOperation->getSign().$escapedValue.'" />';
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$escapedValue.'" />';
	}
	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		$value = str_replace("http://", "", $this->extractValue($dataRow));
		return '<a href="http://'.$value.'/">'.$value.'</a>';
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	*/
	public function prepareSelect($select) {
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	*/
	public function makeRestrictions($select) {
		$baseTableName = $this->getMyTableName();
		if(!empty($this->value)) {
			/*$operator = $this->comparisonOperation->getMySqlComparisonOperator();
			$value = $this->comparisonOperation->getValueForSelect();
			$select->where($baseTableName.".`".$this->getName()."` LIKE ?", $value);*/
			
			$value = $this->getValue();
			
			if(strpos($value, ",") !== false && !($this->comparisonOperation instanceof EnumOperation)) {
				require_once 'ComparisonOperation/EnumOperation.php';
				$this->comparisonOperation = new EnumOperation($value);
			}
			
			$operator = $this->comparisonOperation->getMySqlComparisonOperator();
			$value = $this->comparisonOperation->getValueForSelect();
			
			if(!$this->comparisonOperation instanceof EnumOperation) {
				$select->where($this->getMyTableName().'.`'.$this->getName()."` $operator ?", $value);
			} else {
				$select->where($this->getMyTableName().'.`'.$this->getName()."` $operator (?)", $value);
			}
		}
		if($this->sorting) {
			$select->order($baseTableName.".".$this->getName()." ".$this->sorting);
		}
	
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	*/
	public function prepareDataForSave($data) {
		return $data;
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	*/
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` VARCHAR(255)";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}

}

?>