<?php

require_once ('DbFieldAbstract.php');

class NumberField extends DbFieldAbstract {
	
	
	public function __construct($dbTable) {
		$this->dbTable = $dbTable;
		$this->dbAdapter = $dbTable->getAdapter();
		$this->name = 'number';
		$this->viewname = '№';
		$this->disableEdit();
		$this->hide();
	}
	
	public function isVisible() {
		return false;
	}
	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		return $this->extractValue($dataRow);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$escapedValue.'" />';
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		//Не редактируемое напрямую поле!
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		//установить number = max(number) + 1 ?
		//if(!isset($data[$this->getName()]) || empty($data[$this->getName()]) || !is_numeric($data[$this->getName()])) {
		if(!isset($data['id']) || empty($data['id']) || !is_numeric($data['id'])) {
			$select = $this->dbAdapter->select()
				->from($this->getMyTableName(), array('number' => new Zend_Db_Expr("MAX(number) + 1")));
			$data[$this->getName()] = (int) $this->dbAdapter->fetchOne($select);
		}
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	 */
	public function afterSave($data) {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		// TODO Auto-generated method stub
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	*/
	public function makeRestrictions($select) {
		// TODO Auto-generated method stub
	
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	*/
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` INTEGER";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}
}

?>