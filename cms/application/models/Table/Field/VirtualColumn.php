<?php

require_once ('DbFieldAbstract.php');

class VirtualColumn extends DbFieldAbstract {
	
	/**
	 *
	 * @var VirtualColumnHandler
	 */
	private $handler;
	
	public function __construct($field) {
		parent::__construct($field);
		
		//ищем заданный обработчик.
		$className = $this->field['param'];
		$fileName = $className.'.php';
		$fullFilename = dirname(__FILE__).'/VirtualColumn/'.$fileName;
		
		if(file_exists($fullFilename)) {
			require_once $fullFilename;
			
			if(class_exists($className)) {
				$this->handler = new $className();
				$this->handler->setField($this);
				$this->handler->setDbAdapter($this->dbAdapter);
			}
		}
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::extractValue()
	 */
	public function extractValue($dataRow) {
		if($this->handler) {
			return $this->handler->getValue($dataRow);
		}
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		return $this->extractValue($dataRow);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	 */
	public function afterSave($data) {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::total()
	 */
	public function total($rows = null) {
		// TODO Auto-generated method stub
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		/*
		 * Я, конечно, ожидал, что в зенде все гораздо проще будет.
		 * Но придется сначала столбцы разложить по таблицам,
		 * найти наш виртуальный столбец, удалить его,
		 * а затем снова все скормить назад в селект
		 */
		$before = $select->getPart(Zend_Db_Select::COLUMNS);
		$after = array();
		foreach ($before as $key=>$column) {
			if($column[1] != $this->getName()) {
				if(is_string($column[2])) {
					$after[$column[0]][$column[2]] = $column[1];
				} else {
					$after[$column[0]][] = $column[1];
				}
				
			}
		}
		
		$select->reset(Zend_Db_Select::COLUMNS);
		
		/*
		 * Нельзя скормить назад тот же массив, что был получен из $select->getPart(Zend_Db_Select::COLUMNS)
		 * Очень странно.
		 */
		foreach ($after as $tableName => $columns) {
			$select->columns($columns, $tableName);
		}
		
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		// TODO Auto-generated method stub
		
	}


}

?>