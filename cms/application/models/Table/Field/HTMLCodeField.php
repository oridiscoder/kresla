<?php

require_once ('HtmlField.php');

class HTMLCodeField extends HtmlField {
	public function renderEdit($dataRow) {
		$html = parent::renderEdit($dataRow);
		$html = str_replace("text wysiwyg", "text htmlcode", $html);
		return $html;
	}
}

?>