<?php

require_once ('ComparisonOperationAbstract.php');

class LowerThanOperation extends ComparisonOperationAbstract {
	
	/* (non-PHPdoc)
	 * @see ComparisonOperationAbstract::getSign()
	 */
	public function getSign() {
		return '<';
	}
	
	/* (non-PHPdoc)
	 * @see ComparisonOperationAbstract::getMySqlComparisonOperator()
	 */
	public function getMySqlComparisonOperator() {
		return '<';
	}
	
	public function getValueForSelect() {
		return $this->getValue();
	}
}

?>