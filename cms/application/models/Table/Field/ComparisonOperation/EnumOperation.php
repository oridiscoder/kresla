<?php

require_once ('ComparisonOperationAbstract.php');

class EnumOperation extends ComparisonOperationAbstract {
	/* (non-PHPdoc)
	 * @see ComparisonOperationAbstract::getSign()
	 */
	public function getSign() {
		return null;
	}

	/* (non-PHPdoc)
	 * @see ComparisonOperationAbstract::getMySqlComparisonOperator()
	 */
	public function getMySqlComparisonOperator() {
		return "IN";
	}

	/* (non-PHPdoc)
	 * @see ComparisonOperationAbstract::getValueForSelect()
	 */
	public function getValueForSelect() {
		$values = explode(",", $this->value);
		if(!empty($values)) {
			foreach ($values as $key=>$value) {
				$value = trim($value);
				if(empty($value) && !is_numeric($value)) {
					unset($values);
				} else {
					$values[$key] = $value;
				}
			}
		}
		return $values;
	}


}

?>