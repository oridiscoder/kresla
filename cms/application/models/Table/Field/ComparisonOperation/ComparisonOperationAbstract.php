<?php

abstract class ComparisonOperationAbstract {
	
	protected $value;
	
	public function __construct($value) {
		$this->value = $value;
	}
	
	/**
	 * @return string - символ операции стравнения
	 */
	abstract public function getSign();
	abstract public function getMySqlComparisonOperator();
	abstract public function getValueForSelect();
	
	public function getValue() {
		return empty($this->value) ? $this->value : substr($this->value, 1);
	}
	
	public static function factory($value) {
		$sign = empty($value) ? null : $value[0];
		if($sign == '=') {
			require_once 'EqualOperation.php';
			return new EqualOperation($value);
		} elseif($sign == '<') {
			require_once 'LowerThanOperation.php';
			return new LowerThanOperation($value);
		} elseif ($sign == '>') {
			require_once 'GreaterThanOperation.php';
			return new GreaterThanOperation($value);
		} elseif(preg_match('/^(\d+,\s*)+\d+$/', $value)) {
			require_once 'EnumOperation.php';
			return new EnumOperation($value);
		} else {
			require_once 'LikeOperation.php';
			return new LikeOperation($value);
		}
	}
}

?>