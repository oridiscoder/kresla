<?php
require_once ('ComparisonOperationAbstract.php');

class LikeOperation extends ComparisonOperationAbstract {
	/* (non-PHPdoc)
	 * @see ComparisonOperationAbstract::getSign()
	 */
	public function getSign() {
		return "";
	}
	
	public function getValue() {
		return $this->value;
	}
	/* (non-PHPdoc)
	 * @see ComparisonOperationAbstract::getMySqlComparisonOperator()
	 */
	public function getMySqlComparisonOperator() {
		return 'LIKE';
	}

	public function getValueForSelect() {
		return '%'.$this->getValue().'%';
	}

}

?>