<?php

require_once ('StringField.php');

class IntegerField extends StringField {
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` INT";
	}
	
	public function makeRestrictions($select) {
		$baseTableName = $this->getMyTableName();
		if($this->value !== null) {
			$operator = $this->comparisonOperation->getMySqlComparisonOperator();
			$value = $this->comparisonOperation->getValueForSelect();
			$isInterval = strpos($this->getValue(), "-");
			
			if($isInterval) {
				$points = explode("-", $this->getValue());
				$operator = "BETWEEN";
				$a = $this->dbAdapter->quote((int) $points[0]);
				$b = $this->dbAdapter->quote((int) $points[1]);
				$select->where($baseTableName.".`".$this->getName()."` $operator $a AND $b");
			} else {
				$select->where($baseTableName.".`".$this->getName()."` $operator ?", $value);
			}
			
		}
		if($this->sorting) {
			$select->order($baseTableName.".".$this->getName()." ".$this->sorting);
		}
	}
}

?>