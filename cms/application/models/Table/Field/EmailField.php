<?php
require_once "DbFieldAbstract.php";

class EmailField extends DbFieldAbstract {
	
	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		return htmlspecialchars($this->extractValue($dataRow));
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderFilter()
	 */
	public function renderFilter() {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$this->comparisonOperation->getSign().$escapedValue.'" />';
	}

	/* (non-PHPdoc)
	 * @see DbFieldRendererAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$escapedValue.'" />';
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		$baseTableName = $this->getMyTableName();
		if(!empty($this->value)) {
			$operator = $this->comparisonOperation->getMySqlComparisonOperator();
			$value = $this->comparisonOperation->getValueForSelect();
			$select->where($baseTableName.".`".$this->getName()."` $operator ?", $value);
		}
		if($this->sorting) {
			$select->order($this->getMyTableName().'.'.$this->getName()." ".$this->sorting);
		}
		
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` VARCHAR(255)";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}


}

?>