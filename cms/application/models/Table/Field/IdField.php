<?php
require_once ('DbFieldAbstract.php');

class IdField extends DbFieldAbstract {
	
	public function __construct($dbTable) {
		$this->dbTable = $dbTable;
		$this->dbAdapter = $dbTable->getAdapter();
		$this->name = 'id';
		$this->viewname = 'ID';
		$this->disableEdit();
		$this->comparisonOperation = new LikeOperation(null);
	}
	
	public function extractValue($dataRow) {
		return $dataRow[$this->getName()];
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		return $this->extractValue($dataRow);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		$escapedValue = htmlspecialchars($this->value);
		return '<input type="text" name="'.$this->getName().'" value="'.$this->comparisonOperation->getSign().$escapedValue.'" />';
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		// Это поле не редактируется!
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		
	}
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $data;
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	*/
	public function afterSave($data) {
		//ничего не делаем
	}
/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		if($this->value) {
			$operator = $this->comparisonOperation->getMySqlComparisonOperator();
			$value = $this->comparisonOperation->getValueForSelect();
			if(!$this->comparisonOperation instanceof EnumOperation) {
				$select->where($this->getMyTableName().'.`'.$this->getName()."` $operator ?", $value);
			} else {
				$select->where($this->getMyTableName().'.`'.$this->getName()."` $operator (?)", $value);
			}
			
		}
		if($this->sorting) {
			$select->order($this->getMyTableName().'.'.$this->getName()." ".$this->sorting);
		}
		
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	*/
	public function getCreateColumnDefinition() {
		return "`{$this->getName()}` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY";
	}
	
	public function total($rows = null) {
		return "&nbsp;";
	}


}

?>