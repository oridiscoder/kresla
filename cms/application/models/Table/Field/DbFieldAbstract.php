<?php

require_once 'ComparisonOperation/ComparisonOperationAbstract.php';
require_once 'ComparisonOperation/LikeOperation.php';

abstract class DbFieldAbstract {
	
	const TYPE_ID = 'ID';
	const TYPE_NUMBER = 'NUMBER';
	const TYPE_STRING = 1;
	const TYPE_PASSWORD = 2;
	const TYPE_TEXT = 3;
	const TYPE_DATE = 4;
	const TYPE_LINK = 5;
	const TYPE_EMAIL = 6;
	const TYPE_BOOLEAN = 7;
	const TYPE_HTML = 8;
	const TYPE_ENUM = 12;
	const TYPE_DESCENDANT = 11;
	const TYPE_MULTIPLE_DESCENDANT = 15;
	const TYPE_VIRTUAL_DESCENDANT = 17;
	const TYPE_VIRTUAL_COLUMN = 16;
	const TYPE_FILE = 18;
	const TYPE_IMAGE = 19;
	const TYPE_INT = 20;
	const TYPE_HTMLCODE = 21;
	const TYPE_ASSOCIATIVE_ENTITY = 22;	//связующая таблица
	
	const ALIGN_LEFT = 242;
	const ALIGN_CENTER = 243;
	const ALIGN_RIGHT = 244;
	
	const SORT_ASC = 'ASC';
	const SORT_DESC = 'DESC';
	
	const TABLE_NAME = 'bof_fields';
	
	protected $id;
	protected $name;
	protected $initialName;
	protected $aliasName;
	protected $myTableName;
	protected $value;
	protected $whereSQL;
	protected $comparisonOperation;
	protected $viewname;
	protected $field;
	protected $editable = true;
	protected $viewable = true;
	protected $virtual = false;
	protected $sorting = null;
	protected $params = null;
	protected $useComparisonOperator = true;
	/**
	 *
	 * @var DbTable
	 */
	protected $dbTable;
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	
	/**
	 *
	 * @param array $field
	 * @param Zend_Db_Adapter_Abstract $db
	 * @throws DbFieldException
	 */
	public function __construct($field) {
		/* if(empty($field['id'])) {
			throw new DbFieldException("field[id] is empty");
		} */
		if(empty($field['name'])) {
			throw new DbFieldException("field[name] is empty");
		}
		
		if(isset($field['_db_adapter'])) {
			$this->dbAdapter = $field['_db_adapter'];
		}
		
		if(empty($field['_table_object'])) {
			//throw new DbFieldException("field[_table_object] must be an instance of DbTable");
		} else {
			$this->dbAdapter = $field['_table_object']->getAdapter();
			$this->dbTable = $field['_table_object'];
		}
		
		$this->id = isset($field['id']) ?  $field['id'] : null;
		$this->name = $field['name'];
		$this->initialName = $this->name;
		$this->viewname = $field['viewname'];
		$this->whereSQL = $field['filter'];
		$this->field = $field;
		
		$this->comparisonOperation = new LikeOperation(null);
		
		if($field['hidecolumn']) {
			$this->hide();
		}
		
		$this->params = $this->parseFieldParams($field);
	}
	
	private function parseFieldParams($field) {
		$params = null;
		$paramString = trim($field['param2']);
		if(!empty($paramString)) {
			$paramStringParts = explode(",", $paramString);
			foreach ($paramStringParts as $param) {
				if(preg_match('/^([^\]]+)\[([^\]]+)\]$/', $param, $matches)) {
					$paramName = strtoupper(trim($matches[1]));
					$params[$paramName] = $matches[2];
				} else {
					$paramName = strtoupper($param);
					$params[$paramName] = $paramName;
				}
				
			}
		}
		
		return $params;
	}
	
	public function getID() {
		return $this->field['id'];
	}
	
	public function getName() {
		return $this->aliasName ? $this->aliasName : $this->name;
	}
	public function getInitialName() {
		return $this->initialName;
	}
	public function getValue() {
		return $this->value;
	}
	public function getViewname() {
		return $this->viewname;
	}
	
	public function getTable() {
		return $this->getTableByID($this->field['obj_id']);
	}
	public function getTableID() {
		return $this->field['obj_id'];
	}
	/**
	 * Возвращает простую запись из БД о таблице. 
	 * @param integer $tableID
	 * @return array
	 */
	public function getTableByID($tableID) {
		$select = $this->dbAdapter->select()
			//->from(DbTable::OBJECTS_TABLE_NAME)
			->from("bof_objects")
			->where("id = ?", $tableID)
			->where("hidden = 0");
		return $this->dbAdapter->fetchRow($select);
	}
	
	public function getHint() {
		return $this->field['hint'];
	}
	
	public function setValue($value) {
		if($this->useComparisonOperator) {
			$this->comparisonOperation = ComparisonOperationAbstract::factory($value);
			$this->value = $this->comparisonOperation->getValue();
		} else {
			$this->value = $value;
		}
	}

	public function setDbAdapter($adapter) {
		$this->dbAdapter = $adapter;
	}
	
	public function getMyTableName() {
		if(!$this->myTableName) {
			if($this->dbTable) {
				$this->myTableName =  $this->dbTable->getName();
			} else if($this->field) {
				$myTable = $this->getTable();
				$this->myTableName = $myTable['name'];
			}
		}
		return $this->myTableName;
	}
	
	public function setTableName($name) {
		$this->myTableName = $name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function disableEdit() {
		$this->editable = false;
	}
	public function disableComparisonOperator() {
		$this->useComparisonOperator = false;
	}
	public function isEditable() {
		return $this->editable;
	}
	public function hide() {
		$this->viewable = false;
	}
	public function show() {
		$this->viewable = true;
	}
	public function isVisible() {
		return $this->viewable;
	}
	
	/**
	 * Внимание! Возвращает true если поле имеет аттрибут hidecolumn, установленный в 1,
	 * а не если поле в данный момент не видимо.
	 * Пример: поле $number->isVisible() = false,
	 * 				$number->isHidden() = false
	 * На странице редактирования поле number мы не видим, а поля, отмеченные как 
	 * hidden = 1 видим!
	 *
	 */
	public function isHidden() {
		return $this->field['hidecolumn'];
	}
	public function setHidden($flag) {
		$this->field['hidecolumn'] = $flag;
	}
	
	public function isVirtual() {
		return $this->virtual;
	}
	
	public function getAlign() {
		switch($this->field['align']) {
			case self::ALIGN_LEFT:
				return "left";
			case self::ALIGN_CENTER:
				return "center";
			case self::ALIGN_RIGHT:
				return "right";
			default:
				return "left";
		}
	}
	public function setSortingType($type) {
		if($type) {
			switch($type) {
				case 'ASC':
					$this->sorting = self::SORT_ASC;
					break;
				case 'DESC':
					$this->sorting = self::SORT_DESC;
					break;
				default:
					$this->sorting = self::SORT_ASC;
			}
		} else {
			$this->sorting = null;
		}
	}
	public function getSortingType() {
		return $this->sorting;
	}
	
	abstract public function extractValue($dataRow);

	abstract public function renderCell($dataRow);
	
	abstract public function renderFilter();
	
	abstract public function renderEdit($dataRow);
	
	abstract public function prepareDataForSave($data);
	
	abstract public function afterSave($data);
	
	abstract public function getCreateColumnDefinition();
	
	abstract public function total($rows = null);
	
	/**
	 *
	 * @param Zend_Db_Table_Select $select
	 */
	abstract public function prepareSelect($select);
	/**
	 *
	 * @param Zend_Db_Table_Select $select
	 */
	abstract public function makeRestrictions($select);
	/**
	 *
	 * @param Zend_Db_Table_Select $select
	 */
	public function prepareSelectWithRestrictions($select) {
		$this->prepareSelect($select);
		$this->makeRestrictions($select);
	}
	
	/**
	 * Функция позволяет полям произвести последнюю модификацию данных
	 * уже после их нормализации и перед сохранение в БД
	 * @param array $dirtyData - грязные POST данные
	 * @param array $data - чистые, готовые для вставку в БД, данные
	 * @return array - чистые, готовые для вставку в БД данные!
	 */
	public function modifyDataBeforeSave($dirtyData, $pureData) {
		return $pureData;
	}
	
	/**
	 *
	 * @param Bof_Request $request
	 */
	public function processAjaxRequest($request) {
		return null;
	}
	
	public function isURLField() {
		return $this->field['is_url_field'];
	}
	
	/**
	 * Преобразовать текущее поле в объект DOM
	 * @return DOMDocument
	 */
	public function saveDOM() {
		$dom = new DOMDocument("1.0", "UTF-8");
		$fieldNode = $dom->createElement($this->getName());
		
		$params = $this->dbAdapter->fetchCol("SELECT name FROM bof_fields WHERE hidden = 0 AND obj_id = 3 AND name NOT IN ('obj_id') ORDER BY number");
		foreach ($params as $paramName) {
			$paramNode = $dom->createElement($paramName, $this->field[$paramName]);
			$fieldNode->appendChild($paramNode);
		}
		$dom->appendChild($fieldNode);
		return $dom;
	}
	
	/**
	 *
	 * @param array $field - array, containing: [id, name, type, _table_object]. For type = 'ID' could contain only [type, _table_object]
	 * @return DbFieldAbstract
	 * @throws DbFieldException
	 */
	public static function factory($field) {
		if(!isset($field['type']) || empty($field['type'])) {
			throw new DbFieldException("Field type is not set or empty");
		}
		
		$fieldType = $field['type'];
		
		switch($fieldType) {
			case self::TYPE_ID:
				require_once 'IdField.php';
				return new IdField($field['_table_object']);
			case self::TYPE_NUMBER:
				require_once 'NumberField.php';
				return new NumberField($field['_table_object']);
			case self::TYPE_STRING:
				require_once 'StringField.php';
				return new StringField($field);
			case self::TYPE_PASSWORD:
				require_once 'PasswordField.php';
				return new PasswordField($field);
			case self::TYPE_TEXT:
				require_once 'TextField.php';
				return new TextField($field);
			case self::TYPE_DATE:
				require_once 'DateField.php';
				return new DateField($field);
			case self::TYPE_LINK:
				require_once 'LinkField.php';
				return new LinkField($field);
			case self::TYPE_EMAIL:
				require_once 'EmailField.php';
				return new EmailField($field);
			case self::TYPE_BOOLEAN:
				require_once 'BooleanField.php';
				return new BooleanField($field);
			case self::TYPE_HTML:
				require_once 'HtmlField.php';
				return new HtmlField($field);
			case self::TYPE_ENUM:
				require_once 'EnumField.php';
				return new EnumField($field);
			case self::TYPE_DESCENDANT:
				require_once 'DescendantField.php';
				return new DescendantField($field);
			case self::TYPE_MULTIPLE_DESCENDANT:
				require_once 'MultipleDescendantField.php';
				return new MultipleDescendantField($field);
			case self::TYPE_IMAGE:
				require_once 'ImageField.php';
				return new ImageField($field);
			case self::TYPE_FILE:
				require_once 'FileField.php';
				return new FileField($field);
			case self::TYPE_VIRTUAL_DESCENDANT:
				require_once 'VirtualDescendantField.php';
				return new VirtualDescendantField($field);
			case self::TYPE_VIRTUAL_COLUMN:
				require_once 'VirtualColumn.php';
				return new VirtualColumn($field);
			case self::TYPE_INT:
				require_once 'IntegerField.php';
				return new IntegerField($field);
			case self::TYPE_HTMLCODE:
				require_once 'HTMLCodeField.php';
				return new HTMLCodeField($field);
			case self::TYPE_ASSOCIATIVE_ENTITY:
				require_once 'AssociativeEntityField.php';
				return new AssociativeEntityField($field);
				
			default:
				throw new DbFieldException("Unknown field type: $fieldType. Field name: ".$field['name']);
		}
	}
}

class DbFieldException extends Exception {
	
}

class InvalidFieldDataException extends DbFieldException {
	
}

?>