<?php
require_once "Field/DbFieldAbstract.php";
require_once "Field/IdField.php";
require_once APPLICATION_PATH . '/models/Procedures/ProcedureLoader.php';

class DbTable  {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	protected $tableID;
	protected $_name;
	protected $viewname;
	protected $defaultSortColumn;
	protected $defaultSortIsAsc = false;
	const  OBJECTS_TABLE_NAME = 'bof_objects';
	protected $tableFields;
	protected $tableRealFields;
	protected $onPage;
	protected $pageNumber = 0;
	protected $sortingField;
	protected $dependentTables;
	protected $parentTableContraints;
	protected $showHiddenOnly = false;
	protected $showHiddenFields = false;
	protected $fieldValues;
	protected $where;
	/**
	 *
	 * @var Zend_Db_Select
	 */
	protected $selectObject;
	/**
	 *
	 * @param Zend_Db_Adapter_Abstract $dbAdapter
	 * @param integer $tableID - id записи из таблицы bof_objects
	 */
	public function __construct(Zend_Db_Adapter_Abstract $dbAdapter, $tableID) {
		$this->dbAdapter = $dbAdapter;
		$this->tableID = $tableID;
		$this->_setupTableName();
		$this->init();
	}
	
	protected function _setupTableName() {
		$table = $this->findTableById($this->tableID);
		if(empty($table)) {
			throw new DbTableException("Table with id = {$this->tableID} not found in {self::OBJECTS_TABLE_NAME}, maybe it marked as hidden");
		}
		$this->_name = $table['name'];
		$this->viewname = $table['viewname'];
		$this->defaultSortColumn = $table['sort_col']; 
		$this->defaultSortIsAsc = $table['sort_asc'] ? true : false;
		$this->onPage = isset($table['onpage']) ? $table['onpage'] : null;
	}
	
	protected function findTableById($id) {
		$select = $this->getAdapter()->select()
			->from(self::OBJECTS_TABLE_NAME)
			->where("id = ?", $id)
			->where("hidden = 0");
		return $this->getAdapter()->fetchRow($select);
	}
	
	public function init() {
		$this->initTableFields();
		$this->initDependentTables();
	}
	
	/**
	 * Получить список столбцов, реально содержащихся в заданной таблице
	 */
	protected function getRealFields() {
		if(empty($this->tableRealFields)) {
			$this->tableRealFields = $this->dbAdapter->fetchAll("DESCRIBE `{$this->getName()}`");
		}
		return $this->tableRealFields;
	}
	
	protected function getRealFieldsNames() {
		$names = array();
		$fields = $this->getRealFields();
		if(!empty($fields)) foreach ($fields as $field) {
			$names[] = $field['Field'];
		}
		return $names;
	}
	
	protected function initTableFields() {
		
		$realColumns = $this->getRealFieldsNames();
		
		$this->tableFields['id'] = DbFieldAbstract::factory(array(
			'type' => 'ID',
			'_table_object' => $this
		), $this->getAdapter());
		
		if(in_array("number", $realColumns)) {
			$this->tableFields['number'] = DbFieldAbstract::factory(array(
					'type' => 'NUMBER',
					'_table_object' => $this
			), $this->getAdapter());
		}
		
		$select = $this->getAdapter()->select()
			->from(DbFieldAbstract::TABLE_NAME)
			->where("obj_id = ?", $this->tableID)
			->where("hidden = 0")
			->order("number");
		
		$fields = $this->getAdapter()->fetchAll($select);
		if(!empty($fields)) foreach ($fields as $field) {
			$field['_table_object'] = $this;
			$fieldObject = DbFieldAbstract::factory($field, $this->getAdapter());
			
			if(strtolower($field['name']) == 'number') {
				$fieldObject->setSortingType('ASC');
				$this->sortingField = $fieldObject;
			}
			
			$this->tableFields[$field['name']] = $fieldObject;
		}
	}
	
	protected function initDependentTables() {
		$select = $this->getAdapter()->select()
			->from(array('F' => DbFieldAbstract::TABLE_NAME), array('fid' => 'id', 'fname' => 'viewname'))
			->join(array('O' => self::OBJECTS_TABLE_NAME), 'F.obj_id = O.id')
			->where("F.type = ".DbFieldAbstract::TYPE_DESCENDANT)
			->where("F.param = ? ", $this->tableID)
			->where("F.hidden = 0");
		 $tables = $this->getAdapter()->fetchAll($select);
		 
		 $processedTables = array();
		 if(!empty($tables)) foreach($tables as $table) {
		 	$fields = array();
		 	foreach ($tables as $tbl) {
		 		if($tbl['id'] == $table['id']) {
		 			$fields[] = array(
		 				'id' => $tbl['fid'],
		 				'name' => $tbl['fname']
		 			);
		 		}
		 	}
		 	if(count($fields) > 1) {
		 		$table['_fields'] = $fields;
		 	}
		 	
		 	if(!isset($processedTables[$table['id']])) {
		 		$processedTables[$table['id']] = $table;
		 	}
		 }
		 $this->dependentTables = $processedTables;
	}
	
	public function getRows() {
		$select = $this->prepareSelect();
		
		//echo "<pre>".$select."</pre>";
		$this->selectObject = clone $select;
		$select->limit($this->onPage, $this->pageNumber * $this->onPage);
		return $this->dbAdapter->fetchAll($select);
	}
	
	public function getRecord($id) {
		$select = $this->prepareOneRecordSelect();
		//echo "<pre>".$select."</pre>";
		$select->where($this->getName().'.id = ?', $id);
		return $this->dbAdapter->fetchRow($select);
	}
	
	public function getRowsCount() {
		if(!($this->selectObject instanceof Zend_Db_Select)) {
			$this->selectObject = $this->prepareSelect();
		}
		$this->selectObject->reset(Zend_Db_Select::COLUMNS);
		$this->selectObject->reset(Zend_Db_Select::ORDER);
		$this->selectObject->columns(array('count' => new Zend_Db_Expr("COUNT(*)")));
		
		return $this->dbAdapter->fetchOne($this->selectObject);
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	protected function prepareSelect() {
		/* В селекте участвуют только явно указанные поля таблицы, чтоб не таскать мусор */
		$fieldNames = $this->getFieldNames();
		
		/* если у нас не указано явно "Показывать все поля", то не будем  учитывать скрытые для увеличения производительности */
		if(!$this->showHiddenFields) {
			foreach ($this->tableFields as $field) {
				/* @var $field DbFieldAbstract */
				if($field->isHidden()) {
					$key = array_search($field->getName(), $fieldNames);
					if($key !== false) {
						unset($fieldNames[$key]);
					}					
				}
			}
		}
		
		
		$select = $this->dbAdapter->select()
			->from($this->getName(), $fieldNames);
		
		if(!$this->showHiddenOnly) {
			$select->where($this->getName().'.hidden = 0');
		} else {
			$select->where($this->getName().'.hidden = 1');
		}
		
		/* установим сортировку по-умолчанию если она не задана явно */
		if(!$this->sortingField) {
			if($this->defaultSortColumn) {
				$sortColumnName = $this->defaultSortColumn;
			} elseif (isset($this->tableFields['number'])) {
				$sortColumnName = 'number';
			} else {
				$sortColumnName = 'id';
			}
			$sortDirection = $this->defaultSortIsAsc ? 'ASC' : 'DESC';
			$select->order($this->getName().'.'.$sortColumnName.' '.$sortDirection);
		}
		
		//поля
		foreach ($this->tableFields as $tField) {
			
			if($this->showHiddenFields || !$tField->isHidden()) {
				/* @var $tField DbFieldAbstract */
				$tField->prepareSelectWithRestrictions($select);
				
				//для таких полей, как DescendantField нужна возможность задания точного значения (циферного) поля (когда мы наклыдываем фильтр по такому полю через интерфейс таблицы, то получаем строку, а не числовое значение)
				$fieldName = $tField->getName();
				if(isset($this->fieldValues[$fieldName])) {
					$select->where($this->getName().'.'.$fieldName.' =  ?', $this->fieldValues[$fieldName]);
					$tField->hide();
				}
			}
		}
		
		//процедуры
		$procedures = ProcedureLoader::loadProcedures($this->tableID, $this->getName());
		/* очень странная логика построения WHERE 
		 * По-идее, процедурам не надо учавствовать в построении where условий. 
		 * В старом бофе они это делали, а может и не делали, просто был такой функционал.
		 */
		$where = $this->where;
		if(!$where) {
	 		foreach ($procedures as $procedure) {
				/* @var $procedure ProcedureAbstract */
				if(($procedureWhere = $procedure->where()) != null) {
					$where = $procedureWhere;
				}
			}
		}
		if($where) $select->where($where);
		
		return $select;
	}
	
	/**
	 * @return Zend_Db_Select
	 */
	protected function prepareOneRecordSelect() {
		$fieldNames = $this->getFieldNames();
		$select = $this->dbAdapter->select()
			->from($this->getName(), $fieldNames);
		
		foreach ($this->tableFields as $tField) {
			/* @var $tField DbFieldAbstract */
			$tField->prepareSelect($select);
		}
		
		return $select;
	}
	
	protected function getFieldNames() {
		$names = array();
		if(!empty($this->tableFields)) foreach($this->tableFields as $field) {
			if(!$field->isVirtual()) {
				$names[] = $field->getName();
			}
		}
		return $names;
	}
	
	public function getFields($fieldName = null) {
		if($fieldName) {
			if(isset($this->tableFields[$fieldName])) {
				return $this->tableFields[$fieldName];
			} else {
				throw new DbTableException("Table {$this->getName()} has not column named {$fieldName}.");
			}
		} else {
			return $this->tableFields;
		}
	}
	
	/**
	 * @return DbFieldAbstract
	 */
	public function getURLField() {
		foreach ($this->getFields() as $field) {
			/* @var $field DbFieldAbstract */
			if($field->isURLField()) {
				return $field;
			}
		}
	}
	
	/**
	 *
	 * @param string $name
	 * @param DbTableFieldAbstract $fieldObject
	 * @return DbTable
	 */
	public function setField($name, $fieldObject) {
		$this->tableFields[$name] = $fieldObject;
		return $this;
	}
	
	/**
	 * Удалить столбец по его названию
	 * @param string $name - название столбца в таблице
	 */
	public function removeField($name) {
		unset($this->tableFields[$name]);
	}
	
	public function getFirstField() {
		foreach ($this->tableFields as $field) {
			/* @var $field DbFieldAbstract */
			if($field->getName() != 'id' && $field->getName() != 'number') {
				return $field;
			}
		}
	}
	
	public function getName() {
		return $this->_name;
	}
	public function getViewname() {
		return $this->viewname;
	}
	public function setRowsOnPage($count) {
		$this->onPage = $count;
	}
	public function getRowsOnPage() {
		return $this->onPage ? $this->onPage : null;
	}
	public function setPageNumber($pageNumber) {
		$this->pageNumber = $pageNumber;
	}
	public function getPageNumber() {
		return $this->pageNumber;
	}
	public function getDependentTables() {
		return $this->dependentTables;
	}
	public function setQueryFilters($filters) {
		foreach ($this->tableFields as $field) {
			if(isset($filters[$field->getName()])) {
				/* @var $field DbFieldAbstract */
				$field->setValue($filters[$field->getName()]);
			}
		}
	}
	
	/**
	 * Для таких полей, как Потомок, необходима возможность задать 
	 * значение по id а не по строке, т.е. parent_id = 5 а не parent_table.first_field = Дом из бруса (как идет по умолчанию)
	 * @param string $fieldName
	 * @param string $value
	 */
	public function setFieldValue($fieldName, $value) {
		$this->fieldValues[$fieldName] = $value;
	}
	
	public function disableComparisonOperators() {
		foreach ($this->tableFields as $field) {
			$field->disableComparisonOperator();
		}
	}
	
	public function populateFieldsWithData($data) {
		foreach ($this->tableFields as $field) {
			if(isset($data[$field->getName()])) {
				$field->setValue($data[$field->getName()]);
			}
		}
	}
	
	/**
	 * Если нам нужны какие-то дополнительные условия, которые не укладываются
	 * в список полей таблицы, то можно их указать тут в виде чистого SQL
	 * @param string $whereSQL
	 */
	public function setWhereSQL($whereSQL) {
		$this->where = $whereSQL;
	}
	
	
	public function setOrderFieldName($sortingFieldName, $sortingFieldType) {
		if($sortingFieldName) {
			foreach ($this->tableFields as $field) {
				if($sortingFieldName == $field->getName()) {
					$field->setSortingType($sortingFieldType);
					$this->sortingField = $field;
				} else {
					$field->setSortingType(null);
				}
			}
		}
	}
	
	public function save($data) {
		
		$procedures = ProcedureLoader::loadProcedures($this->tableID, $this->getName(), isset($data['id']) ? ProcedureAbstract::TYPE_EDIT : ProcedureAbstract::TYPE_ADD);
		foreach ($procedures as $procedure) {
			/* @var $procedure ProcedureAbstract */
			$result = $procedure->beforeAction($data);
			if(is_array($result)) {
				$data = $result;
			}
		}
		
		$data = $this->prepareData($data);
		$pureData = $this->normilizeData($data);
		$pureData = $this->rightBeforeSaveModifications($data, $pureData);
		
		if(isset($pureData['id']) && $pureData['id']) {
			$id = $pureData['id'];
			if(!is_numeric($id)) {
				throw new DbTableException("Field id($id) is not numeric!");
			}
			
			$this->update($pureData, "id = $id");
		} else {
			$data['id'] = $this->insert($pureData);
		}
		
		foreach ($this->tableFields as $field) {
			$field->afterSave($data);
		}
		
		foreach ($procedures as $procedure) {
			/* @var $procedure ProcedureAbstract */
			$procedure->afterAction($data);
		}
		
		return $data['id'];
	}
	
	public function prepareData($data) {
		$fieldNames = $this->getFieldNames();
		
		foreach ($this->tableFields as $field) {
			$data = $field->prepareDataForSave($data);
		}
		return $data;
	}
	
	public function normilizeData($data) {
		$fieldNames = $this->getFieldNames();
		
		$pureData = array();
		
		foreach ($data as $key=>$value) {
			if(in_array($key, $fieldNames)) {
				$pureData[$key] = $value;
			}
		}
		return $pureData;
	}
	
	public function rightBeforeSaveModifications($data, $pureData) {
		$fieldNames = $this->getFieldNames();
		
		foreach ($this->tableFields as $field) {
			$pureData = $field->modifyDataBeforeSave($data, $pureData);
		}
		return $pureData;
	}
	
	public function updateFieldValueForRows($field, $ids) {
		foreach ($ids as $id) {
			$data = array(
				'id' => $id,
				$field->getName() => $field->getValue()
			);
			
			$data = $field->prepareDataForSave($data);
			$this->update($data, "id = $id");
			$field->afterSave($data);
		}
	}
	
	public function moveToRecycleBin($ids) {
		$where = null;
		if(is_array($ids)) {
			$where = $this->getName().'.id IN ('.implode(',', $ids).')';
		} elseif(is_numeric($ids)) {
			$where = $this->getName().'.id = '.$ids;
		}
		
		if($where)	$this->update(array('hidden' => 1), $where);
	}
	
	public function restoreFromRecycleBin($ids) {
		$where = null;
		if(is_array($ids)) {
			$where = $this->getName().'.id IN ('.implode(',', $ids).')';
		} elseif(is_numeric($ids)) {
			$where = $this->getName().'.id = '.$ids;
		}
		
		if($where)	$this->update(array('hidden' => 0), $where);
	}
	
	public function deleteItems($ids) {
		if(!is_array($ids)) {
			throw new DbTableException('$ids must be an array!');
		}
		$where = $this->getName().'.id IN ('.implode(',', $ids).')';
		
		
		$procedures = ProcedureLoader::loadProcedures($this->tableID, $this->getName(), ProcedureAbstract::TYPE_DELETE);
		$records = array();
		foreach ($ids as $id) {
			$records[$id] = $this->getRecord($id);
			foreach ($procedures as $procedure) {
				$procedure->beforeAction($records[$id]);
			}
		}
		
		$this->delete($where);
		
		foreach ($ids as $id) {
			foreach ($procedures as $procedure) {
				$procedure->afterAction($records[$id]);
			}
		}
	}
	
	public function setSortingOrder($items) {
		if(!empty($items)) foreach ($items as $number => $id) {
			$this->update(array('number' => $number), "id = $id");
		}
	}
	
	public function setParentTableConstraints($field, $value) {
		$this->parentTableContraints = array(
			'field' => $field,
			'value' => $value
		);
	}
	
	public function getParentTableConstraints() {
		return $this->parentTableContraints;
	}
	
	public function getID() {
		return $this->tableID;
	}
	
	public function showHiddenOnly() {
		$this->showHiddenOnly = true;
	}
	
	public function getAdapter() {
		return $this->dbAdapter;
	}
	
	public function ascIsDefaultSortType() {
		return $this->defaultSortIsAsc;
	}
	
	public function insert($data) {
		$this->dbAdapter->insert($this->getName(), $data);
		$id = $this->dbAdapter->lastInsertId();
		return $id;
	}
	
	public function update($data, $condition) {
		$this->dbAdapter->update($this->getName(), $data, $condition);
	}
	
	public function delete($condition) {
		$this->dbAdapter->delete($this->getName(), $condition);
	}
	
	/**
	 * Есть ли среди полей помеченные как hidecolumn
	 * @return boolean
	 */
	public function hasHiddenFields() {
		foreach ($this->tableFields as $field) {
			if($field->isHidden()) {
				return true;
			}
		}
		return false;
	}
	
	public function showHiddenFields() {
		$this->showHiddenFields = true;
		foreach ($this->tableFields as $field) {
			$field->show();
		}
	}
	
	public function isVisibleHiddenFields() {
		return $this->showHiddenFields;
	}
	
	public function fetchNew() {
		$result = array();
		$fieldNames = $this->getRealFieldsNames();
		foreach ($fieldNames as $fname) {
			$result[$fname] = null;
		}
		return $result;
	}
	
	/**
	 * Сохранить структуру таблицы в XML
	 */
	public function saveDOM() {
		/* сначала сгенерируем xml полей */
		$xml = "";
		$dom = new DOMDocument("1.0", "UTF-8");
		$tableNode = $dom->createElement("table");
		$table = $this->findTableById($this->tableID);
		
		/* свойства таблицы */
		$objectFields = $this->dbAdapter->fetchCol("SELECT name FROM bof_fields WHERE hidden = 0 AND obj_id = 1 ORDER BY number");
		foreach ($objectFields as $fieldName) {
			$node = $dom->createElement($fieldName, $table[$fieldName]);
			$tableNode->appendChild($node);
		}
		
		/* поля таблицы */
		$fieldsNode = $dom->createElement("fields");
		$excludeFields = array("id", "number");
		foreach ($this->getFields() as $field) {
			/* @var $field DbFieldAbstract */
			if(!in_array($field->getName(), $excludeFields)) {
				$fieldDOM = $field->saveDOM();
				$fieldNode = $fieldDOM->documentElement;
				$fieldNode = $dom->importNode($fieldNode, true);
				$fieldsNode->appendChild($fieldNode);
			}
		}
		
		$tableNode->appendChild($fieldsNode);
		$dom->appendChild($tableNode);
		
		return $dom;
	}
}

class DbTableException extends Exception {
	
}

?>