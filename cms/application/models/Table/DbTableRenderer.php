<?php

require_once "TableRenderer.php";

class DbTableRenderer extends TableRenderer {
	
	/**
	 * 
	 * @var DbTable
	 */
	protected $dbTable;
	
	/**
	 * 
	 * @var Bof_Request
	 */
	protected $request;
	
	protected $basePath;
	
	public function __construct(Smarty $smarty, $options = array()) {
		
		/* 
		 * я не могу изменить параметры конструктора, поэтому важные параметры передаем
		 * через $options. Required параметры проверяем и бросаем исключения, если 
		 * что-то не указана
		 */
		if(!isset($options['dbTable']) && !($options['dbTable'] instanceof DbTable)) {
			throw new Exception('You must specify $options[\'dbTable\'] as instance of DbTable');
		} else {
			$this->dbTable = $options['dbTable'];
		}
		
		if(isset($options['request'])) {
			$this->request = $options['request'];
		} else {
			$this->request = Application::getAppRequest();
		}
		
		if(isset($options['base_path'])) {
			$this->basePath = $options['base_path'];
		} else {
			$this->basePath = $this->request->getBasePath();
		}
		
		self::prepareDbTable($this->dbTable, $this->request, isset($options['dependencies']) ? $options['dependencies'] : null);
		
		require_once APPLICATION_PATH . "/models/Table/DbTablePaginator.php";
		$paginator = new DbTablePaginator(
				$this->dbTable->getRowsCount(),
				$this->dbTable->getRowsOnPage(),
				$this->dbTable->getPageNumber(),
				$this->request
		);
		$options['paginator'] = $paginator;
		
		/* Данные таблицы */
		$fields = $this->dbTable->getFields();
		$rows = $this->dbTable->getRows();
		
		$fieldsData = array();
		$rowsData = array();
		
		foreach($fields as $field) {
			/* @var $field DbFieldAbstract */
			if($field->isVisible()) {
				foreach($rows as $key => $row) {
					$rowsData[$key][$field->getName()] = $field->renderCell($row);
				}
				
				$fieldsData[$field->getName()]  = array(
					'name' => $field->getName(),
					'viewname' => $field->getViewname(),
					'visible' => $field->isVisible(),
					'filter_html' => $field->renderFilter(),
					'total_value' => $field->total(),			
					'editable' => $field->isEditable(), 
					'align' => $field->getAlign()
				);
			}
		}
		
		/* операции */
		require_once APPLICATION_PATH . '/models/Operations/OperationLoader.php';
		
		// Загружаем базовые операции, такие как, сохранить, редактировать и т.п.
		$operationsBase = OperationLoader::loadBaseOperations($this->dbTable);
		// Загружаем дополнительные операции, которые навешены на эту таблицу
		$operationsCustom = OperationLoader::loadOperations($this->dbTable);
		
		$operations = array();
	
		foreach ($operationsBase['record'] as $operation) {
			$operations['record'][$operation->getName()] = $operation;
		}
		foreach ($operationsBase['table'] as $operation) {
			$operations['table'][$operation->getName()] = $operation;
		}
	
		if(!empty($operationsCustom['record'])) foreach($operationsCustom['record'] as $operation) {
			$name = $operation->getName();
			$operations['record'][$name] = $operation;
		}
		if(!empty($operationsCustom['table'])) foreach($operationsCustom['table'] as $operation) {
			$name = $operation->getName();
			$operations['table'][$name] = $operation;
		}
		
		if($this->request->getParam("is_recycle")) {
			unset($operations['record']['to_recycle_bin']);
			unset($operations['record']['edit']);
			unset($operations['record']['dependency']);
			$options['is_recycle'] = true;
		} else {
			unset($operations['record']['restore_from_recycle_bin']);
			$dependTables = $this->dbTable->getDependentTables();
			if(empty($dependTables)) {
				unset($operations['record']['dependency']);
			}
			$options['is_recycle'] = false;
		}
		if(!$this->userCanWrite()) {
			unset($operations['record']['edit']);
		}
		if(!$this->userCanDelete()) {
			unset($operations['record']['to_recycle_bin']);
			unset($operations['record']['restore_from_recycle_bin']);
		}
		if(!$this->dbTable->getDependentTables()) {
			unset($operations['record']['DependencyOperation']);
		}
		
		/* переведём операции в вид ассоциативного массива */
		foreach ($rowsData as $key => $row) {
			foreach ($operations['record'] as $operation) {
				
				$url = $this->request->getPathInfo() . '/operation/' . get_class($operation).'/'.$row['id'].'/';
				$url = str_replace("//", "/", $url);
				$rowsData[$key]['_operations'][$operation->getName()] = array(
						'name' => $operation->getName(),
						'title' => $operation->getTitle(),
						'img' => $operation->getImageSource(),
						'css_class' => $operation->getClassName(),
						'confirm' => $operation->getConfirmText(),
						'onclick' => $operation->getOnClickJSCode(),
						'url' => $url
				);
				
			}			
		}
		
		$options['fields'] = $fieldsData;
		$options['rows'] = $rowsData;
		
		/* сделаем-ка мы дерево */
		$isTree = false;
		foreach ($fields as $field) {
			if($field instanceof DescendantField) {
				if($field->getParentTableId() == $this->dbTable->getID()) {
					$isTree = true;
					$treeField = $field;
				}
				break;
			}
		}
		
		if($isTree && $rows) {
			require_once "Bof/PlainTree.php";
			$pt = new PlainTree($rows, 'id', $treeField->getName());
			$tree = $pt->getTree();
			$rows = PlainTree::multisort($rows, array_keys($tree));
			$rowsData = PlainTree::multisort($rowsData, array_keys($tree));
			$stack = array();
			$stackLength = 0;
			$ffname = $this->dbTable->getFirstField()->getName();
			
			foreach ($rows as $key => $row) {
				$current = $stackLength ? $stack[$stackLength - 1] : null;
				$previous = $stackLength > 1 ? $stack[$stackLength - 2] : null;
				
				$value = $row[$treeField->getName()];
				if($value != $current) {
					if($value == $previous) {
						array_pop($stack);
						$stackLength --;
					} else {
						array_push($stack, $value);
						$stackLength ++;
					}
				}
				
				if($stackLength > 1) {
					$rowsData[$key][$ffname] = str_repeat("-", $stackLength - 1) . " ". $rowsData[$key][$ffname];
				}
			}
			
			$options['rows'] = $rowsData;
		}
		
		require_once 'Bof/URLBuilder.php';
		$options['url_builder'] = new Bof_URLBuilder($this->request);
		
		$options['recycle_enable'] = true;
		$options['enable_group_operations'] = true;
		$options['has_hidden_fields'] = $this->dbTable->hasHiddenFields();
		
		$this->setTemplateName("table/table-array.html");
		$this->setDataTemplateName("table/table-array-datarows.html");

		/* операции предназначенные для обработки всей таблицы */
		$tableOperations = array();
		
		if($operations['table']) foreach($operations['table'] as $operation) {
			$tableOperations[$operation->getName()] = array(
				'title' => $operation->getTitle(),
				'description' => $operation->getDescription(),
				'img' => $operation->getImageSource(),
				'css_class' => $operation->getClassName(),
				'url' => str_replace('//', '/', $this->basePath.'/operation/'.$operation->getName().'/')
			);
		}
		/* если есть, значит, нам позволено редактировать данную страницу */
		if(isset($operations['record']['EditOperation'])) {
			$operation = $operations['record']['EditOperation'];
			$tableOperations['add'] = array(
					'title' => 'Добавить запись',
					'description' => 'Создать новую запись в текущей таблице',
					'img' => '/cms/images.old/record.jpg',
					'css_class' => 'add-record',
					'url' => str_replace("//", "/", $this->request->getPathInfo() . '/operation/' . get_class($operation).'/')
			);
		}
		
		/* групповые операции */
		$groupOperations = array();
		
		if($this->userCanWrite()) {
			$groupOperations['_group'] = 	array(
					'title' => 'Изменить'
			);
			foreach($fields as $field) {
				if($field->isEditable() && Application::getAppUser()->hasFieldAccess($field->getID())) {
					$groupOperations['_group']['edit_'.$field->getName()] = array(
							'url' => $this->basePath . '/operation/EditOperation/',
							'title' => $field->getViewname(),
							'confirm' => false,
							'ajax' => true, 
							'css_class' => null
					);
				}			
			}
			
			if($options['is_recycle']) {
				$groupOperations['from_recycle'] = array(
						'url' => $this->basePath . '/operation/FromRecycleBin/',
						'title' => "Восстановить из корзины",
						'confirm' => false,
						'ajax' => false, 
						'css_class' => null
				);
			} else {
				$groupOperations['to_recycle'] = array(
						'url' => $this->basePath . '/operation/ToRecycleBinOperation/',
						'title' => "Поместить в корзину",
						'confirm' => "Точно хотите поместить выбранные элементы в корзину?",
						'ajax' => false, 
						'css_class' => null
				);
			}
			
			$groupOperations['delete'] = array(
					'url' => $this->basePath . '/operation/DeleteOperation/',
					'title' => "Удалить",
					'confirm' => "Вы уверены, что хотите удалить выбранные элементы?",
					'ajax' => false, 
					'css_class' => 'delete'
			);
			
			if($operations['table']) foreach($operations['table'] as $operation) {
				/* @var $operation TableOperationAbstract */
				if($operation->showInGroupSelect()) {
					$groupOperations[$operation->getName()] = array(
							'url' => $this->basePath . '/operation/'.$operation->getName().'/',
							'title' => $operation->getTitle(),
							'confirm' => false,
							'ajax' => false,
							'css_class' => null
					);
				}
			}
		}
		
		$operations = array(
			'table' => $tableOperations,
			'group' => $groupOperations
		);
		$options['operations'] = $operations;
		
		parent::__construct($smarty, $options);
	}
	
	/**
	 * Натроить DbTable исходя из заданных в request параметрах 
	 * @param DbTable $dbTable
	 * @param Bof_Request $request
	 * @param DependencyList $dependencies
	 */
	public static function  prepareDbTable($dbTable, $request, $dependencies = null) {
		$requestQuery = $request->getQuery();
			
		$pageNumber = 0;
		if(isset($requestQuery['pageNumber']) && is_numeric($requestQuery['pageNumber']) && $requestQuery['pageNumber'] > 0) {
			$pageNumber = $requestQuery['pageNumber'];
		}

		/* в cms добавлено поле onpage для таблиц. смотрим теперь сначала на него */
		$rowsOnPage = $dbTable->getRowsOnPage();
		
		if(empty($rowsOnPage)) {
			if(($config = Application::getAppConfigs()) !== null) {
				$rowsOnPage = $config->table->onpage;
			} else {
				$rowsOnPage = self::ROWS_ON_PAGE;
			}
		}
		
		/* ну и onpage из GET параметра конечно же важнее */ 
		if(isset($requestQuery['onPage']) && is_numeric($requestQuery['onPage'])) {
			$rowsOnPage = $requestQuery['onPage'];
		}
			
		$orderBy = $request->get("orderBy");
		$orderType  = $request->get("orderType");
	
		$dbTable->setQueryFilters($requestQuery);
		$dbTable->setRowsOnPage($rowsOnPage);
		$dbTable->setPageNumber($pageNumber);
		$dbTable->setOrderFieldName($orderBy, $orderType);
		
		/* скрытые поля */
		if($request->getParam("_show_hidden_columns")) {
			$dbTable->showHiddenFields();
		}
		/* показать корзину */
		if($request->getParam("is_recycle")) {
			$dbTable->showHiddenOnly();
		}
	
		if($dependencies) {
			$lastDependencyItem = $dependencies->getLast();
			$dbTable->setParentTableConstraints($lastDependencyItem->getField(), $lastDependencyItem->getParentRowID());
		}
	}
	
	protected function userCanWrite() {
		return Application::getAppUser()->hasTableAccess($this->dbTable->getID(), User::ACCESS_WRITE);
	}
	protected function userCanDelete() {
		return Application::getAppUser()->hasTableAccess($this->dbTable->getID(), User::ACCESS_DELETE);
	}
}

?>