<?php

require_once ('DbFieldDecorator.php');

class TunnelUrlDecorator extends DbFieldDecorator {
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		if($dataRow['site_id']) {
			$site = $this->getSite($dataRow['site_id']);
			if($site) {
				$url = "http://".$site['url'].$site['linkator_path']."/tunnel.php?PGrabberGETURL=".urlencode("http://linkator.oridis.ru/tunnel-test.html");
				$anchor = "http://".$site['url'].$site['linkator_path']."/tunnel.php";
				return '<a target="_blank" href="'.$url.'">'.$anchor.'</a>';
			} else {
				return null;
			}
		} else {
			return '<a target="_blank" href="'.$dataRow['url'].'">'.$dataRow['url'].'</a>';
		}
	}
	
	private function getSite($id) {
		return $this->dbAdapter->fetchRow("SELECT * FROM linkator_sites WHERE id = ?", $id);
	}


}

?>