<?php

require_once ('DbFieldDecorator.php');

class TaskerNextRunDecorator extends DbFieldDecorator {
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		$task = $this->dbAdapter->fetchRow("SELECT * FROM tasker WHERE id = ?", $dataRow['id']);
		if($task['run_immediately']) {
			return '<span style="color:red">Немедленно</span>';
		} else {
			return $this->fieldObject->renderCell($dataRow);
		}
	}
}

?>