<?php

require_once 'DbFieldDecorator.php';

class InvoicesCostDecorator extends DbFieldDecorator {
	/* (non-PHPdoc)
	 * @see DbFieldDecorator::renderCell()
	 */
	public function renderCell($dataRow) {
		$html = $this->fieldObject->renderCell($dataRow);
		$akt = $this->getAkt($dataRow['dogovor_id']);
		if($akt && $akt['cost'] == $dataRow['cost']) {
			$title = 'Счет закрыт актом на ту же сумму.';
			$url = '/section/11/96/?id='.$akt['id'];
			$html = '<a style="color:green;" title="'.$title.'" href="'.$url.'">'.$html.'</a>';
		} elseif($akt) {
			$color = 'brown';
			$title = 'Счет закрыт актом на другую сумму';
			$html = '<span style="color:'.$color.'" title="'.$title.'">'.$html.'</span>';
		} else {
			$color = '#000';
			$title = 'Ещё не закрыт актом';
			$html = '<span style="color:'.$color.'" title="'.$title.'">'.$html.'</span>';
		}
		return $html;
	}
	
	private function getAkt($dogovorID) {
		return $this->dbAdapter->fetchRow("SELECT * FROM akts WHERE dogovor_id = ? AND hidden = 0", $dogovorID);
	}


}

?>