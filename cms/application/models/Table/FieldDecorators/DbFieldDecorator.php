<?php

require_once APPLICATION_PATH . '/models/Table/Field/DbFieldAbstract.php';

abstract class DbFieldDecorator extends DbFieldAbstract {
	
	/**
	 *
	 * @var DbFieldAbstract
	 */
	protected $fieldObject;
	
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	
	/**
	 *
	 * @param DbFieldAbstract $field
	 */
	public function __construct(DbFieldAbstract $field, $dbAdapter) {
		$this->fieldObject = $field;
		$this->dbAdapter = $dbAdapter;
	}
	
	public function getName() {
		return $this->fieldObject->getName();
	}
	public function getValue() {
		return $this->fieldObject->getValue();
	}
	public function getViewname() {
		return $this->fieldObject->getViewname();
	}
	public function getTableID() {
		return $this->fieldObject->getTableID();
	}
	
	public function getHint() {
		return $this->fieldObject->getHint();
	}
	
	public function setValue($value) {
		$this->fieldObject->setValue($value);
	}
	
	public function setDbAdapter($adapter) {
		$this->fieldObject->setDbAdapter($adapter);
	}
	
	public function getMyTableName() {
		return $this->fieldObject->getMyTableName();
	}
	
	public function setTableName($name) {
		return $this->fieldObject->setTableName($name);
	}
	public function setName($name) {
		return $this->fieldObject->setName($name);
	}
	public function disableEdit() {
		return $this->fieldObject->disableEdit();
	}
	public function isEditable() {
		return $this->fieldObject->isEditable();
	}
	
	public function hide() {
		return $this->fieldObject->hide();
	}
	public function show() {
		return $this->fieldObject->show();
	}
	public function isVisible() {
		return $this->fieldObject->isVisible();
	}
	public function isHidden() {
		return $this->fieldObject->isHidden();
	}
	
	public function isVirtual() {
		return $this->fieldObject->isVirtual();
	}
	
	public function getAlign() {
		return $this->fieldObject->getAlign();
	}
	public function setSortingType($type) {
		return $this->fieldObject->setSortingType($type);
	}
	public function getSortingType() {
		return $this->fieldObject->getSortingType();
	}

	/**
	 *
	 * @param Zend_Db_Table_Select $select
	 */
	public function prepareSelectWithRestrictions($select) {
		$this->fieldObject->prepareSelectWithRestrictions($select);
	}
	
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::extractValue()
	 */
	public function extractValue($dataRow) {
		return $this->fieldObject->extractValue($dataRow);
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderFilter()
	 */
	public function renderFilter() {
		return $this->fieldObject->renderFilter();
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderEdit()
	 */
	public function renderEdit($dataRow) {
		return $this->fieldObject->renderEdit($dataRow);
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareDataForSave()
	 */
	public function prepareDataForSave($data) {
		return $this->fieldObject->prepareDataForSave($data);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::afterSave()
	 */
	public function afterSave($data) {
		return $this->fieldObject->afterSave($data);
	}

	/* (non-PHPdoc)
	 * @see DbFieldAbstract::getCreateColumnDefinition()
	 */
	public function getCreateColumnDefinition() {
		return $this->fieldObject->getCreateColumnDefinition();
		
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::total()
	 */
	public function total($rows = null) {
		return $this->fieldObject->total($rows);
		
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::prepareSelect()
	 */
	public function prepareSelect($select) {
		return $this->fieldObject->prepareSelect($select);
		
		
	}

/* (non-PHPdoc)
	 * @see DbFieldAbstract::makeRestrictions()
	 */
	public function makeRestrictions($select) {
		return $this->fieldObject->makeRestrictions($select);
	}
	
	
	public function __call($name, $args) {
		echo $name."\n";
		if(method_exists($this->fieldObject, $name)) {
			return call_user_method_array($name, $this->fieldObject, $args);
		} else {
			throw new Exception("No such method supported");
		}
	}



}

?>