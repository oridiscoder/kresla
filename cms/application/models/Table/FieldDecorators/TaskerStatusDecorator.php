<?php

require_once ('DbFieldDecorator.php');

class TaskerStatusDecorator extends DbFieldDecorator {
	const STATUS_WAIT = 167;
	const STATUS_WORK = 168;
	/* (non-PHPdoc)
	 * @see DbFieldAbstract::renderCell()
	 */
	public function renderCell($dataRow) {
		$status =  $dataRow[$this->fieldObject->getName()];
		$value = $this->fieldObject->renderCell($dataRow);
		switch($status) {
			case self::STATUS_WORK:
				return '<strong>'.$value.'</strong>';
			case self::STATUS_WAIT:
				return '<span style="color:#ccc">'.$value.'</span>';
			default:
				return $value;
		}
	}


}

?>