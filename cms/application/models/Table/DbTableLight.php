<?php

require_once "Field/DbFieldAbstract.php";

class DbTableLight {
	/**
	 * 
	 * @var Zend_Db_Adapter_Abstract
	 */
	private $db;
	private $table;
	private $fields;
	private $dependentTables;
	private $parentTables;
	
	public function __construct($db, $tableName) {
		$this->db = $db;
		$this->table = $this->db->fetchRow("SELECT * FROM bof_objects WHERE name = ? AND hidden = 0", $tableName);
		
		if(empty($this->table)) {
			throw new DbTable_TableNotFoundException("Table «".$tableName."» not found");
		}
	}
	
	public function getId() {
		return $this->table['id'];
	}
	public function getName() {
		return $this->table['name'];
	}
	public function getAdapter() {
		return $this->db;
	}
	
	public function getFields() {
		if($this->fields == null) {
			$rows = $this->loadFields();
			if($rows) foreach($rows as $row) {
				$this->fields[$row['name']] = $row;
			}
		}
		$fields = array();
		if($this->fields) {
			$names = array_keys($this->fields);
			foreach ($names as $name) {
				$fields[$name] = $this->getField($name);
			}
		}
		return $fields;		
	}
	
	public function getField($name) {
		if(!isset($this->fields[$name])) {
			$this->loadField($name);
		}
		$field = $this->fields[$name];
		$field['_table_object'] = $this;
		
		$field =  DbFieldAbstract::factory($field);
		$field->setTableName($this->table['name']);
		return $field;
	}
	
	/**
	 * Получить поле данной таблицы по его id
	 * @param integer $id
	 * @return \DbFieldAbstract
	 */
	public function getFieldById($id) {
		$fields = $this->getFields();
		if($fields) foreach ($fields as $field) {
			if($field->getID() == $id) {
				return $field;
			}
		}
	}
	
	public function loadField($name) {
		$select = $this->db->select()
			->from("bof_fields")
			->where("obj_id = ?", $this->table['id'])
			->where("hidden = 0")
			->where("name = ?", $name);
		$this->fields[$name] = $this->db->fetchRow($select);
	}
	
	public function loadFields() {
		$select = $this->db->select()
			->from("bof_fields")
			->where("obj_id = ?", $this->table['id'])
			->where("hidden = 0");
		return $this->db->fetchAll($select);
	}
	
	/**
	 * Раскрыть урлы к картинкам
	 * @param array $rows - строки из БД
	 * @return array $rows - строки, где каждое ImageField заменено на полный путь к картинке
	 */
	public function expandImageUrls($rows) {
		foreach ($this->getFields() as $field) {
			if($field instanceof \ImageField) {
				foreach ($rows as $key => $row) {
					$rows[$key][$field->getName()] = $field->getURL($row);
					$rows[$key][$field->getName().'_orig'] = $field->getOriginalURL($row);
				}
			}
		}
		
		return $rows;
	}
	
	/**
	 * Получить список зависимых таблиц
	 * @return array
	 */
	public function getDependentTables() {
		if($this->dependentTables == null) {
			/* выбираем всех потомков, которые указывают на нас */
			$select = $this->getAdapter()->select()
				->from(array('F' => "bof_fields"), array('fid' => 'id', 'fname' => 'name', 'fviewname' => 'viewname'))
				->join(array('O' => "bof_objects"), 'F.obj_id = O.id')
				->where("F.type = ".DbFieldAbstract::TYPE_DESCENDANT)
				->where("F.param = ? ", $this->getId())
				->where("F.hidden = 0");
			 $tables = $this->getAdapter()->fetchAll($select);
			 
			 $dependentTables = array();
			 if($tables) foreach($tables as $table) {
			 	$fields = array();
			 	foreach ($tables as $tbl) {
			 		if($tbl['id'] == $table['id']) {
			 			$fields[] = array(
			 				'id' => $tbl['fid'],
			 				'name' => $tbl['fname'], 
			 				'viewname' => $tbl['fviewname']
			 			);
			 		}
			 	}
			 	$table['_fields'] = $fields;
			 	
			 	if(!isset($dependentTables[$table['id']])) {
			 		$dependentTables[$table['id']] = $table;
			 	}
			 }
			 $this->dependentTables = $dependentTables;
		}
		
		return $this->dependentTables;
	}
	/**
	 * Получить все родительские таблицы (те, на которые ссылаются пои поля-потомки )
	 * @return array
	 */
	public function getParentTables() {
		if($this->parentTables == null) {
			/* выбираем всех потомков, которые указывают на нас */
			$select = $this->getAdapter()->select()
				->from(array('F' => "bof_fields"), array('fid' => 'id', 'fname' => 'name', 'fviewname' => 'viewname'))
				->join(array('O' => "bof_objects"), 'F.param = O.id')
				->where("F.type = ".DbFieldAbstract::TYPE_DESCENDANT)
				->where("F.obj_id = ? ", $this->getId())
				->where("F.hidden = 0");
			$tables = $this->getAdapter()->fetchAll($select);
		
			$parentTables = array();
			if($tables) foreach($tables as $table) {
				$fields = array();
				foreach ($tables as $tbl) {
					if($tbl['id'] == $table['id']) {
						$fields[] = array(
								'id' => $tbl['fid'],
								'name' => $tbl['fname'],
								'viewname' => $tbl['fviewname']
						);
					}
				}
				$table['_fields'] = $fields;
					
				if(!isset($parentTables[$table['id']])) {
					$parentTables[$table['id']] = $table;
				}
			}
			$this->parentTables = $parentTables;
		}
		
		return $this->parentTables;
	}
}

class DbTable_TableNotFoundException extends Exception {
	
}

?>