<?php

require_once "models/Table/Field/DbFieldAbstract.php";

class DbTableCreator {
	/**
	 * 
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	protected $objectsTableName = 'bof_objects';
	protected $fieldsTableName = 'bof_fields';
	
	/**
	 * 
	 * @var integer - ID последней создаваемой таблицы
	 */
	protected $lastTableID;
	/**
	 * 
	 * @var integer - ID последнего созданного поля
	 */
	protected $lastFieldID;
	
	public function __construct($db) {
		$this->db = $db;
	}
	
	public function setTableId($id) {
		$this->lastTableID = $id;
	}
	public function getTableId() {
		return $this->lastTableID;
	}
	public function getLastFieldId() {
		return $this->lastFieldID;
	}
	
	/**
	 * Добавить новую таблицу - запись в таблицу bof_objects
	 * @param string $name - название таблицы
	 * @param array $options - дополнительные параметры таблицы (поля таблицы в БД)
	 * @return DbTableCreator
	 */
	public function addTable($name, $viewname = "", $options = array()) {
		
		$table = $this->getTableByName($name);
		if($table) {
			$this->deleteTable($table['id']);
		}
		
		$options = array_merge(array(
				'number' => $this->db->fetchOne("SELECT MAX(number) + 1 FROM {$this->objectsTableName}"),
				'sort_asc' => 'false',
				'sort_col' => null, 
				'viewname' => $viewname
		), $options);
	
		$options['name'] = $name;
	
		$this->db->insert($this->objectsTableName, $options);
		$this->lastTableID = $this->db->lastInsertId();
		return $this;
	}
	
	/**
	 * Установить текущую таблицу
	 * @param string $tableName - имя таблицы
	 * @return DbTableCreator
	 * @throws Exception
	 */
	public function setTable($tableName) {
		$table = $this->getTableByName($tableName);
		if($table) {
			$this->setTableId($table['id']);
			return $this;
		} else {
			throw new Exception("Table $tableName not found");
		}
	}

	/**
	 * Найти таблицу по её имени
	 * @param string $name - имя таблицы
	 * @return array - информация о таблице
	 */
	public function getTableByName($name) {
		$select = $this->db->select()
			->from($this->objectsTableName)
			->where("name = ?", $name)
			->where("hidden = 0");
		return $this->db->fetchRow($select);
	}
	
	/**
	 * Найти таблицу по её id
	 * @param integer $id
	 * @return array - информация о таблице
	 */
	public function getTableById($id) {
		$select = $this->db->select()
			->from($this->objectsTableName)
			->where("id = ?", $id);
		return $this->db->fetchRow($select);
	}
	/**
	 * Удалить таблицу (запись в bof_objects)
	 * Данный метод не удаляет физическую таблицу, а только запись о ней в bof_objects
	 * @param integer $id
	 */
	public function deleteTable($id) {
		$this->db->delete($this->objectsTableName, array("id = ?" => $id));
		$this->db->delete($this->fieldsTableName, array("obj_id = ?" => $id));
	}
	
	/**
	 * Получить список всех таблиц системы
	 * @return array
	 */
	public function getTables() {
		$select = $this->db->select()
			->from("bof_objects")
			->where("hidden = 0")
			->order("number");
		return $this->db->fetchAll($select);
	}
	
	/**
	 * 
	 * @param integer|array $tableID
	 * @param array $options
	 * @return DbTableCreator
	 * @throws Exception
	 */
	public function addField($name, $viewname, $type, $options = array()) {
		
		if(($tableID = $this->getTableId()) == null) {
			throw new Exception("No last table ID specified");
		}
		
		$bind = array(
				'name' => $name,
				'viewname' => $viewname,
				'type' => $type, 
				'obj_id' => $tableID 
		);
		$bind = array_merge($bind, $options);
		
		/* рассмотрим случай, когда мы создаем потомка и указываем не ID таблицы, а её название */
		if($type == DbFieldAbstract::TYPE_DESCENDANT && !is_numeric($options['param'])) {
			$table = $this->getTableByName($options['param']);
			if($table) {
				$bind['param'] = $table['id'];
			} else {
				throw new Exception("Table with name '".$options['param']."' not found");
			}
		}
		
		$dbField = $this->db->fetchRow("SELECT * FROM ".$this->fieldsTableName." WHERE obj_id = $tableID AND name = ?", $name);
		if($dbField) {
			$this->db->update($this->fieldsTableName, $bind, "id = ".$dbField['id']);
		} else {
			$bind['number'] = $this->db->fetchOne("SELECT MAX(number) + 1 FROM ".$this->fieldsTableName);
			$this->db->insert($this->fieldsTableName, $bind);
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addStringField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_STRING, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addTextField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_TEXT, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addDateField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_DATE, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addBooleanField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_BOOLEAN, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addEmailField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_EMAIL, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addFileField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_FILE, $options);
	}
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addEnumField($name, $viewname, $enumID, $options = array()) {
		$options['param'] = $enumID;
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_ENUM, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addDescendantField($name, $viewname, $parentTableID, $notnull = false, $tree = false, $sections = false, $options = array()) {
		$params = array();
		if($notnull) {
			$params[] = 'NOTNULL';
		}
		if($tree) {
			$params[] = 'TREE';
		}
		if($sections) {
			$params[] = 'SECTIONS';
		}
		$options['param'] = $parentTableID;
		$options['param2'] = implode(",", $params);
		
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_DESCENDANT, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addHTMLCodeField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_HTMLCODE, $options);
	}
	
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addHTMLField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_HTML, $options);
	}
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addImageField($name, $viewname, $options = array()) {
		
		require_once "models/Table/Field/ImageField.php";
		
		/* врисовать в прямоугольник */
		if(isset($options['draw_into'])) {
			$size = $options['draw_into'];
			$options['param'] = ImageField::PROCESS_DRAW_INTO;
			$options['param2'] = $options['draw_into'];
			unset($options['draw_into']);
		}
		/* обрезать под заданные размеры прямоугольника */
		if(isset($options['crop_into'])) {
			$size = $options['crop_into'];
			$options['param'] = ImageField::PROCESS_CROP_INTO;
			$options['param2'] = $options['draw_into'];
			unset($options['crop_into']);
		}
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_IMAGE, $options);
	}
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addIntegerField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_INT, $options);
	}
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addLinkField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_LINK, $options);
	}
	/**
	 *
	 * @param string $name
	 * @param string $viewname
	 * @param array $options
	 * @return DbTableCreator
	 */
	public function addPasswordField($name, $viewname, $options = array()) {
		return $this->addField($name, $viewname, DbFieldAbstract::TYPE_PASSWORD, $options);
	}
	
	/**
	 * 
	 * @param string $fieldName
	 * @param integer $tableID
	 * @return DbTableCreator
	 */
	public function deleteField($fieldName, $tableID = null) {
		/* можно либо явно задать таблицу, либо продолжить работать с текущей, если она есть */
		if($tableID == null) {
			$tableID = $this->lastTableID;
		} else {
			$this->lastTableID = $tableID;
		}
		
		if($tableID) {
			$this->db->delete($this->fieldsTableName, array("obj_id = ?" => $tableID, "name = ?" => $fieldName));
		}
		
		return $this;
	}
}

?>