<?php

class TableRenderer {
	
	/**
	 * 
	 * @var Smarty
	 */
	private $smarty;
	
	private $templateName = 'table.html';
	private $dataTemplateName = 'table-datarows.html';
	
	/**
	 * 
	 * @var DbTablePaginator
	 */
	private $paginator;
	
	private $operations;
	/**
	 * 
	 * @var Bof_URLBuilder
	 */
	private $urlBuilder;
	private $rows;
	private $fields;
	private $enableRecycle = true;
	private $isRecycle = false;
	private $enableGroupOperations = true;
	private $showHiddenFieldsButton = false;
	
	
	public function __construct(Smarty $smarty, $options = array()) {
		$this->smarty = $smarty;
		
		if(isset($options['paginator'])) {
			$this->paginator = $options['paginator'];
		}
		if(isset($options['operations'])) {
			$this->operations = $options['operations'];
		} else {
			$this->operations = array(
				'table' => array(), 
				'record' => array()
			);
		}
		
		$this->isRecycle = isset($options['is_recycle']) ? $options['is_recycle'] : false;
		
		if(isset($options['url_builder'])) {
			$this->urlBuilder = $options['url_builder'];
		}
		
		if(isset($options['fields'])) {
			$this->fields = $options['fields'];
		}
		if(isset($options['has_hidden_fields'])) {
			$this->showHiddenFieldsButton = $options['has_hidden_fields'];
		}
		if(isset($options['rows'])) {
			$this->rows = $options['rows'];
		}
		if(isset($options['recycle_enable'])) {
			$this->enableRecycle = $options['recycle_enable'];
		}
		if(isset($options['enable_group_operations'])) {
			$this->enableGroupOperations = $options['enable_group_operations'];
		}
	}
	/**
	 * @return Smarty
	 */
	public function getSmarty() {
		return $this->smarty;
	}

	/**
	 * @return string
	 */
	public function getTemplateName() {
		return $this->templateName;
	}
	public function getDataTemplateName() {
		return $this->dataTemplateName;
	}
	public function setDataTemplateName($name) {
		$this->dataTemplateName = $name;
	}

	/**
	 * @return DbTablePaginator
	 */
	public function getPaginator() {
		return $this->paginator;
	}

	/**
	 * @return array
	 */
	public function getOperations() {
		return $this->operations;
	}

	/**
	 * @return Bof_URLBuilder
	 */
	public function getUrlBuilder() {
		return $this->urlBuilder;
	}

	/**
	 * @return array
	 */
	public function getRows() {
		return $this->rows;
	}

	/**
	 * @return array
	 */
	public function getFields() {
		return $this->fields;
	}

	/**
	 * @param Smarty $smarty
	 */
	public function setSmarty($smarty) {
		$this->smarty = $smarty;
	}

	/**
	 * @param string $templateName
	 */
	public function setTemplateName($templateName) {
		$this->templateName = $templateName;
	}

	/**
	 * @param DbTablePaginator $paginator
	 */
	public function setPaginator($paginator) {
		$this->paginator = $paginator;
	}

	/**
	 * @param array $operations
	 */
	public function setOperations($operations) {
		$this->operations = $operations;
	}

	/**
	 * @param Bof_URLBuilder $urlBuilder
	 */
	public function setUrlBuilder($urlBuilder) {
		$this->urlBuilder = $urlBuilder;
	}

	/**
	 * @param array $data
	 */
	public function setRows($rows) {
		$this->rows = $rows;
	}

	/**
	 * @param array $fields
	 */
	public function setFields($fields) {
		$this->fields = $fields;
	}
	
	public function setRecycleURL($url) {
		$this->recycleURL = $url;
	}
	public function getRecycleURL() {
		return $this->recycleURL;
	}
	public function isEnabledRecycle() {
		return $this->enableRecycle;
	}
	public function isEnabledGroupOperations() {
		return $this->enableGroupOperations;
	}
	
	/**
	 * Урлы для построения списка сортировки по полям. 
	 * @return array
	 */
	public function getSortingUrls() {
		$fields = $this->getFields();
		$urls = array();
		foreach ($fields as $field) {
			$urls[$field['name']] = $this->getUrlBuilder()->orderBy($field['name']);
		}
		return $urls;
	}
	
	protected function getData() {
		$data = array(
				'paginator' => array(
						'pageNumber' => $this->paginator->getPageNumber(),
						'onPage' => $this->paginator->getOnPage(),
						'pagesCount' => $this->paginator->getPagesCount(),
						'html' => $this->paginator->getPaginator(),
						'onpageHTML' => $this->paginator->getOnpageSelector()
				),
				'operations' => $this->getOperations(),
				'data' => array(
						'fields' => $this->getFields(),
						'rows' => $this->getRows()
				),
				'isRecycle' => $this->isRecycle,
				'ulrBuilder' => $this->getUrlBuilder(),
				'recycleURL' => $this->getUrlBuilder()->getRecycleURL(),
				'normalURL' => $this->getUrlBuilder()->getClearURL(),
				'recycleEnable' => $this->isEnabledRecycle(),
				'enableGroupOperations' => $this->enableGroupOperations,
				'has_hidden_fields' => $this->showHiddenFieldsButton,
				'urls' => array(
						'orderby' => $this->getSortingUrls(),
						'show_hidden' => $this->getUrlBuilder()->showHiddenColumns(),
						'show_normal' => $this->getUrlBuilder()->getRecycleUrl(),
						'show_recycled' => $this->getUrlBuilder()->getRecycleURL()
				)
		);
		return $data;
	}

	public function render() {
		return $this->out($this->getData());
	}
	
	protected function out($data) {
		$this->smarty->assign($data);
		$request = $this->getUrlBuilder()->getRequest();
		if($request->isXmlHttpRequest()) {
			header("Content-type:text/html; charset=utf-8");
			echo $this->smarty->fetch($this->getDataTemplateName());
			exit();
		} else {
			return $this->smarty->fetch($this->getTemplateName());
		}
	}
}

?>