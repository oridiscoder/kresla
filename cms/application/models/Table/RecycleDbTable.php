<?php

require_once ('DbTable.php');

class RecycleDbTable extends DbTable {
	protected function prepareSelect() {
		$select = parent::prepareSelect();
		
		$where = $select->getPart("where");
		$where[0] = $this->getName().'.hidden = 1';
		
		$select->reset("where");
		foreach ($where as $w) {
			$select->where($w);
		}
		
		return $select;
	}
}

?>