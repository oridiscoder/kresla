<?php
require_once "DbTable.php";
class DbTablePaginator {
	
	const SHIFT_LEFT = 5;
	const SHIFT_RIGHT = 5;
		
	/**
	 *
	 * @var DbTable
	 */
	protected $dbTable;
	/**
	 *
	 * @var Zend_Controller_Request_Http
	 */
	protected $request;
	
	protected $rowsCount;
	protected $onPage;
	protected $pageNumber;
	protected $pagesCount;
	
	private $linkActiveTemplate;
	private $linkTemplate;
	private $linkDelimeter;
	
	public function __construct($total, $onpage, $pageNumber, $request) {
		$this->rowsCount = $total;
		$this->onPage = $onpage;
		$this->pageNumber = $pageNumber;
		$this->request = $request;
		
		$this->linkActiveTemplate = '{title}';
		$this->linkTemplate = '<a href="{href}">{title}</a>';
		$this->linkDelimeter = ' ';
	}
	
	public function setLinkTemplate($str) {
		$this->linkTemplate = $str;
	}
	public function setLinkActiveTemplate($str) {
		$this->linkActiveTemplate = $str;
	}
	public function setLinkDelimeter($str) {
		$this->linkDelimeter = $str;
	}
	public function getRowsCount() {
		return $this->rowsCount;
	}
	public function getPageNumber() {
		return $this->pageNumber;
	}
	public function getCurrentPageNumber() {
		return $this->pageNumber;
	}
	public function getOnPage() {
		return $this->onPage;
	}
	public function setRowsCount($count) {
		$this->rowsCount = $count;
	}
	public function setPageNumber($number) {
		$this->pageNumber = $number;
	}
	public function setOnPage($onpage) {
		$this->onPage = $onpage;
	}
	public function getPagesCount() {		
		return $this->onPage ? ceil($this->rowsCount / $this->onPage) : 0;
	}
	
	/**
	 *
	 * @param Zend_Controller_Request_Http $this->request
	 */
	public function getPaginator() {
		
		$fullURL = $this->request->getScheme()."://".$this->request->getServer().$this->request->getRequestUri();
		$uri = Zend_Uri_Http::fromString($fullURL);
		
		if($this->onPage) {
			$this->pagesCount = ceil($this->rowsCount / $this->onPage);
		} else {
			$this->pagesCount = 0;
		}
		
		$left = 0;
		$right = $this->pagesCount;
		
		$html = '';
		$paginatorItems = array();
		
		for ($i=0; $i < $this->pagesCount; $i++)	{
			if ($i < 5 || $i> $this->pagesCount - 6 || abs($this->pageNumber -  $i) < 5)
			{
				$flag_skip = false;
				$uri->addReplaceQueryParameters(array('pageNumber' => $i));
				$href = $this->request->getPathInfo().'?'.$uri->getQuery();
				$title = $i+1;
				
				if($i == $this->pageNumber) {
					$itemHTML = $this->linkActiveTemplate;
				} else {
					$itemHTML = $this->linkTemplate;
				}
				
				$itemHTML = str_replace("{title}", $title, $itemHTML);
				$itemHTML = str_replace("{href}", $href, $itemHTML);
				
				$paginatorItems [] = $itemHTML;
			}
			elseif (!$flag_skip)	{
				$paginatorItems [] = "...";
				$flag_skip = true;
			}
		}
		
		return implode($this->linkDelimeter, $paginatorItems);
	}
	
	public function getOnpageSelector() {
		
		$total = $this->getRowsCount();
		$variants = array(
				10 => 10,
				20 => 20,
				50 => 50,
				100 => 100,
				0 => 'Все ('.$total.')'
		);
		
		$fullURL = $this->request->getScheme()."://".$this->request->getServer().$this->request->getRequestUri();
		$uri = Zend_Uri_Http::fromString($fullURL);
		
		$items = array();
		foreach ($variants as $onPageVariant => $onPageVariantTitle) {
			$uri->addReplaceQueryParameters(array(
					'onPage' => $onPageVariant,
					'pageNumber' => 0
			));
			
			$href = $this->request->getPathInfo().'?'.$uri->getQuery();
			$title = $onPageVariantTitle;
			
			if($this->onPage == $onPageVariant) {
				$itemHTML = $this->linkActiveTemplate;
			} else {
				$itemHTML = $this->linkTemplate;
			}
			
			$itemHTML = str_replace("{href}", $href, $itemHTML);
			$itemHTML = str_replace("{title}", $title, $itemHTML);
			$items[] = $itemHTML;
		}
		
		return implode($this->linkDelimeter, $items);
	}
}

?>