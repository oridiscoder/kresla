<?php
require_once 'Zend/Acl/Role/Interface.php';

class User implements Zend_Acl_Role_Interface {
	
	protected $id;
	protected $name;
	protected $email;
	protected $login;
	protected $password;
	protected $jobId;
	protected $groupID;
	protected $superuser = false;
	protected $accessObjectsList; //доступы пользователя
	protected $groupObjectAccessList;	//досупы группы пользователя
	protected $restrictedFields;
	protected $employeeID;
	protected $projectsIds;
	
	const JOB_OPTIMISATOR = 9;
	const JOB_PROGRAMMER = 12;
	const JOB_LINKS = 129;
	
	const ACCESS_READ = 'read';
	const ACCESS_WRITE = 'write';
	const ACCESS_DELETE = 'delete';
	
	const PROJECT_TYPE_SEO = 1;
	const PROJECT_TYPE_LT = 2;
	const PROJECT_TYPE_DEV = 3;
	
	const GROUP_LT_SUPERUSER = 11;
	const GROUP_LT_SUPERUSER_ASSISTANT = 20;

	public function __construct($id, $login = "") {
		$this->id = $id;
		$this->login = $login;
		$this->restrictedFields = array();
	}
	
	public function getId() {
		return $this->id;
	}
	public function setEmployeeID($id) {
		$this->employeeID = $id;
	}
	public function getEmployeeID() {
		return $this->employeeID;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setEmail($email) {
		$this->email = $email;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function getLogin() {
		return $this->login;
	}
	
	public function setJobId($id) {
		$this->jobId = $id;
	}
	
	public function getGroupID() {
		return $this->groupID;
	}
	public function setGroupID($id) {
		$this->groupID = $id;
	}
	
	public function setSuperuser($super) {
		$this->superuser = $super ? true : false;
	}
	
	public function isSuperuser() {
		return $this->superuser;
	}
	public function isLtSuperuser() {
		return $this->groupID == self::GROUP_LT_SUPERUSER;
	}
	public function isLtSuperuserAssistant() {
		return $this->groupID == self::GROUP_LT_SUPERUSER_ASSISTANT;
	}
	public function setAccessList($list) {
		if(!empty($list)) foreach ($list as $item) {
			$this->accessObjectsList[$item['type']][$item['obj_id']] = array(
				'read' => $item['read'] ? true : false,
				'write' => $item['write'] ? true : false,
				'delete' => $item['delete'] ? true : false
			);
		}
	}
	public function setGroupAccessList($list) {
		if(!empty($list)) foreach ($list as $item) {
			$this->groupObjectAccessList[$item['type']][$item['obj_id']] = array(
					'read' => $item['read'] ? true : false,
					'write' => $item['write'] ? true : false,
					'delete' => $item['delete'] ? true : false
			);
		}
	}
	/**
	 * Задать ограничение на доступ к списку столбцов
	 * @param array $ids - массив, состоящий из id элементов bof_fields к которым данный пользователь не имеет доступа
	 */
	public function setRestrictedFields($ids) {
		$this->restrictedFields = $ids ? $ids : array();
	}
	public function getRestrictedFields() {
		return $this->restrictedFields;
	}
	
	public function hasFieldAccess($fieldID) {
		return !in_array($fieldID, $this->restrictedFields);
	}
	public function hasTableAccess($tableID, $accessType) {
		return $this->isSuperuser() 
		|| (isset($this->accessObjectsList['object'][$tableID]) && $this->accessObjectsList['object'][$tableID][$accessType])
		|| (isset($this->groupObjectAccessList['object'][$tableID]) && $this->groupObjectAccessList['object'][$tableID][$accessType]);
	}
	public function hasSectionAccess($sectionID, $accessType) {
		return $this->isSuperuser() 
		|| (isset($this->accessObjectsList['section'][$sectionID]) && $this->accessObjectsList['section'][$sectionID][$accessType])
		|| (isset($this->groupObjectAccessList['section'][$sectionID]) && $this->groupObjectAccessList['section'][$sectionID][$accessType]);
	}
	public function hasPageAccess($pageID, $accessType) {
		return $this->isSuperuser() 
		|| (isset($this->accessObjectsList['page'][$pageID]) && $this->accessObjectsList['page'][$pageID][$accessType])
		|| (isset($this->groupObjectAccessList['page'][$pageID]) && $this->groupObjectAccessList['page'][$pageID][$accessType]);
	}
	public function hasProjectAreaAccess($projectType, $areaName) {
		return $this->isSuperuser() || isset($this->accessObjectsList['project_area'][$projectType][$areaName]);
	}
	public function setProjectAreaAccess($access) {
		$this->accessObjectsList['project_area'] = $access;
	}
	
	public function getJobId() {
		return $this->jobId;
	}
	public function getFullName() {
		return $this->name ? $this->name : $this->getLogin();
	}
	public function setPassword($pass) {
		$this->password = $pass;
	}
	public function getPassword() {
		return $this->password;
	}
	public function isMyPassword($password) {
		if(empty($this->password)) {
			throw new Exception("User object doesn't know his password to check it");
		}
		return ($password == $this->password);
	}
	
	public function setProjects($ids) {
		$this->projectsIds = $ids;
	}
	public function getProjects() {
		return $this->projectsIds;
	}
	
	public function getRoleId() {
		return $this->id;
	}
	
	/**
	 *
	 * @param TaskItem $task
	 * @return bool
	 */
	public function canEditTask($task) {
		return $this->isSuperuser()
				|| $task->getManagerID() == $this->getEmployeeID()
				|| ($task->isNotice() && $task->getWorkerID() == $this->getEmployeeID())
				|| ($task->getProjectType() == self::PROJECT_TYPE_LT && $this->getGroupID() == self::GROUP_LT_SUPERUSER);
	}
	
	public static function fromArray($userA) {
		$user = new self($userA['id'], $userA['login']);
		$user->setName($userA['name']);
		$user->setJobId($userA['job_id']);
		$user->setPassword($userA['password']);
		$user->setSuperuser($userA['superuser']);
	}
	
}

class AccessException extends Exception {
	const OBJECT_TYPE_PAGE = 'page';
}