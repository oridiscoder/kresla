<?php

class UserFactory {
	
	protected static $instance;
	
	/**
	 *
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $db;
	
	/**
	 *
	 * @param Zend_Db_Adapter_Mysqli $db
	 */
	protected function __construct(Zend_Db_Adapter_Mysqli $db = null) {
		$this->db = $db;
	}
	
	/**
	 *
	 * @param Zend_Db_Adapter_Mysqli $db
	 * @return BOFUserFactory
	 */
	public static function getInstance(Zend_Db_Adapter_Mysqli $db = null) {
		if(!self::$instance) {
			self::$instance = new self($db);
		}
		return self::$instance;
	}
	
	/**
	 * Получить объект BOFUser по заданому id пользователя
	 * @param integer $id
	 * @return User
	 */
	public function getUserById($id) {
		$user = $this->db->fetchRow("SELECT * FROM bof_users WHERE id = ?", $id);
		if(!empty($user)) {
			
			$access = $this->db->fetchAll("SELECT * FROM bof_users_access WHERE user_id = ?", $id);
			//$groupAccess = $this->db->fetchAll("SELECT * FROM bof_groups_access WHERE group_id = ?", $user['group_id']);
			
			$fieldsDenyAccess = $this->db->fetchCol("SELECT field_id FROM access_fields WHERE group_id = ?", $user['group_id']);
								
			$validUser = new User($user['id'], $user['login']);
			$validUser->setPassword($user['password']);
			$validUser->setAccessList($access);
			//$validUser->setGroupAccessList($groupAccess);
			$validUser->setRestrictedFields($fieldsDenyAccess);
			$validUser->setSuperuser($user['superuser']);
			$validUser->setGroupID($user['group_id']);
			
			if(isset($user['employier_id'])) {
				$person = $this->db->fetchRow("SELECT * FROM employies WHERE id = ?", $user['employier_id']);
				if(!empty($person)) {
					$validUser->setName(trim($person['name'].' '.$person['family']));
					$validUser->setEmail($person['email']);
					$validUser->setJobId($person['jobtitle_id']);
					$validUser->setEmployeeID($person['id']);
					$sql = "SELECT DISTINCT project_id
						FROM bof_projects_workers WHERE employee_id = {$person['id']}
						UNION
						SELECT id FROM bof_projects WHERE manager_id = {$person['id']} OR  worker_id = {$person['id']}";
					$projects = $this->db->fetchCol($sql);
					sort($projects);
					$validUser->setProjects($projects);
				} else {
					$validUser->user->setName($user['employier_id']);
				}
			}
			return $validUser;
		} else {
			return null;
		}
	}
	
	/**
	 *
	 * @param string $login
	 * @return User
	 */
	public function getUserByLogin($login) {
		$user = $this->db->fetchRow("SELECT * FROM bof_users WHERE login = ?", $login);
		if($user) {
			return $this->getUserById($user['id']);
		} else {
			return null;
		}
	}
	
	public function getUserByEmployeeID($id) {
		$user = $this->db->fetchRow("SELECT * FROM bof_users WHERE employier_id = ? AND hidden = 0", $id);
		if($user) {
			return $this->getUserById($user['id']);
		} else {
			return null;
		}
	}
}