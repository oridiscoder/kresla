<?php

require_once ('ModuleAbstract.php');

class Modules extends ModuleAbstract {
	
	protected $modulePath;
	/**
	 * 
	 * @var Di
	 */
	protected $di;
	
	protected $basePath;
	
	protected $urlParts;
	
	protected $actionName;
	
	/* (non-PHPdoc)
	 * @see ModuleAbstract::render()
	 */
	public function render() {
		define('ROOT', realpath(APPLICATION_PATH . '/../../'));
		set_include_path(ROOT . PATH_SEPARATOR . get_include_path());
		
		require_once "front/Autoloader.php";
		spl_autoload_register(array('front\Autoloader', 'autoload'));
		
		$this->modulePath = ROOT . '/front/modules';
		
		$section = $this->request->getParam("section");
		$page = $this->request->getParam("page");
		
		/* URL до начала этого модуля */
		$this->basePath = preg_replace('#^(.*/\d+/\d+).*$#', '\\1', $this->request->getPathInfo());
		
		require_once "front/models/Di.php";
		$this->di = \front\models\Di::getInstance();
		$this->di->register('db', $this->dbAdapter);
		$this->di->register("config", Application::getAppConfigs());
		
		$this->urlParts = array_values(array_filter(explode("/", str_replace($this->basePath, "", $this->request->getPathInfo()))));
		$action = $this->urlParts ? $this->urlParts[0] : 'index';
		$this->actionName = $action;
		$action .= 'Action';
		
		if(method_exists($this, $action)) {
			return call_user_func(array($this, $action));
		} else {
			throw new Exception("Не найден метод $action");
		}
	}

	public function getTitle() {
		return "Управление модулями сайта";
	}
	public function getBreadcrumbs() {
		switch($this->actionName) {
			case 'config': 
				return array(
					array(
						'href' => '', 
						'title' => 'Настройки модуля'
					)
				);
			case 'templates':
				return array(
					array(
						'href' => '',
						'title' => 'Шаблоны модуля'
					)
			);
		}
	}
	
	public function indexAction() {

		$moduleList = $this->getModuleList();
		
		$fields = array(
			'id' => array(
				'name' => 'id',
				'viewname' => 'ID',
				'visible' => true,
				'filter_html' => '<input type="text" name="id" value="{value}"/>',
				'total_value' => 0,
				'editable' => false
			),
			'title' => array(
				'name' => 'title',
				'viewname' => 'Название',
				'visible' => true,
				'filter_html' => '<input type="text" name="title" value="{value}"/>',
				'total_value' => 0,
				'editable' => false
			),
			'description' => array(
				'name' => 'description',
				'viewname' => 'Описание',
				'visible' => true,
				'filter_html' => '<input type="text" name="description" value="{value}"/>',
				'total_value' => 0,
				'editable' => false
			)
		);
		foreach($fields as $fieldname => $field) {			
			$fields[$fieldname]['filter_html'] = str_replace('{value}', htmlspecialchars($this->request->getParam($fieldname)), $field['filter_html']);
		}
		$rows = array();
		
		if($moduleList) foreach ($moduleList as $number=>$module) {
			
			/* @var $module \front\modules\Module */
			
			$operations = array();
			
			if(!$module->isInstalled()) {
				$operations['install'] = array(
					'title' => 'Установить',
					'img' => '/cms/images/btn5.png',
					'url' => $this->basePath.'/install/'.$module->getName().'/', 
					'confirm' => null, 
					'css_class' => null,
					'onclick' => null
				);
			} else {
				/* модуль установлен. можно делать с ним что угодно */
				$operations['edit'] = array(
						'title' => 'Настройки',
						'img' => '/cms/images.old/edit.png',
						'css_class' => 'edit-link',
						'url' => $this->basePath.'/config/'.$module->getName().'/',
						'confirm' => null,
						'onclick' => null
				);
				$operations['templates'] = array(
						'title' => 'Шаблоны модуля',
						'img' => '/cms/images/template-ico.png',
						'css_class' => 'edit-link',
						'url' => $this->basePath.'/templates/'.$module->getName().'/',
						'confirm' => null,
						'onclick' => null
				);
				$operations['uninstall'] = array(
						'title' => 'Удалить',
						'img' => '/cms/images.old/trash.png',
						'url' => $this->basePath.'/remove/'.$module->getName().'/', 
						'confirm' => 'Действительно хотите удалить этот модуль?', 
						'confirm' => null, 
						'css_class' => null,
						'onclick' => null
				);
				
				if($module->isActive()) {
					$operations['off'] = array(
							'title' => 'Выключить',
							'img' => '/cms/images/bulb_off.png',
							'url' => $this->basePath.'/off/'.$module->getName().'/',
							'confirm' => 'Действительно хотите отключить этот модуль?',
							'confirm' => null,
							'css_class' => null,
							'onclick' => null
					);
				} else {
					$operations['on'] = array(
							'title' => 'Включить',
							'img' => '/cms/images/bulb_on.png',
							'url' => $this->basePath.'/on/'.$module->getName().'/',
							'confirm' => null,
							'confirm' => null,
							'css_class' => null,
							'onclick' => null
					);
				}
			}
			
			if(($id = $this->request->getParam("id")) != false) {
				if($id != $number + 1) {
					continue;
				}
			}
			if(($title = $this->request->getParam("title")) != false) {
				if(stripos($module->getTitle(), $title) === false) {
					continue;
				}
			}
			if(($description = $this->request->getParam("description")) != false) {
				if(stripos($module->getDescription(), $description) === false) {
					continue;
				}
			}
			
			$rows[] = array(
				'id' => $number + 1, 
				'title' => $module->getTitle(), 
				'description' => $module->getDescription(),
				'_operations' => $operations
			);
		}
		
		/* $this->smarty->assign(array(
				"basePath" => $this->basePath,
				"modules" => $moduleList, 
				"installed" => $this->getInstalledModules()
		)); */
		
		require_once 'models/Table/TableRenderer.php';
		require_once 'models/Table/DbTablePaginator.php';
		require_once 'Bof/URLBuilder.php';
		$urlBuilder = new Bof_URLBuilder($this->request);
		$render = new TableRenderer(Application::getAppSmarty(), array(
			'paginator' => new DbTablePaginator(count($moduleList), 1000, 1, $this->request), 
			'operations' => array(
				'table' => array(
					/*'export' => array(
						'title' => 'Экспорт',
						'description' => '',
						'img' => '/cms/images.old/ex_in.jpg',
						'css_class' => 'export-link',
						'url' => '/section/6/26/operation/ExportOperation/?y_1=%3E0&pageNumber=2'						
					),
					'import' => array(
							'title' => 'Импорт',
							'description' => '',
							'img' => '/cms/images.old/ex_out.jpg',
							'css_class' => 'import-link',
							'url' => '/section/6/26/operation/ExportOperation/?y_1=%3E0&pageNumber=2'
					),
					'add' => array(
						'title' => 'Добавить запись',
						'description' => '',
						'img' => '/cms/images.old/record.jpg',
						'css_class' => 'add-record',
						'url' => '/section/6/26/operation/ExportOperation/?y_1=%3E0&pageNumber=2'
					)*/
				), 
				'group' => array(
					'install' => array(
						'url' => $this->basePath.'/install/', 
						'title' => 'Установить', 
						'confirm' => 'Вы действительно хотите установить выбранные модули?', 
						'ajax' => false, 
						'css_class' => null,
					), 
					'remove' => array(
							'url' => $this->basePath.'/remove/',
							'title' => 'Удалить',
							'confirm' => 'Вы действительно хотите удалить выбранные модули?', 
							'ajax' => false,
							'css_class' => null,
					),
					'_group' => array(
						'title' => 'Вкл\Выкл',
						'on' => array(
								'url' => $this->basePath.'/on/',
								'title' => 'Включить',
								'confirm' => 'Включить выбранные модули?', 
								'ajax' => true,
								'css_class' => null,
						),
						'off' => array(
								'url' => $this->basePath.'/off/',
								'title' => 'Выключить',
								'confirm' => 'Отключить выбранные модули?', 
								'ajax' => false,
								'css_class' => null,
						), 
					)
				)					
			),
			'fields' => $fields, 
			'rows' => $rows,
			'url_builder' => $urlBuilder, 
			'is_recycle' => $this->request->getParam("is_recycle"),
			'recycle_enable' => false 
		));
		$render->setTemplateName('table/table-array.html');
		$render->setDataTemplateName("table/table-array-datarows.html");
		
		$tableHTML = $render->render();
		
		$this->smarty->assign("tableHTML", $tableHTML);
		
		
		return $this->smarty->fetch("list.html");
	}
	
	public function installAction() {
		
		$moduleName = $this->getRequestedModuleName();
		if(empty($moduleName)) {
			return "Необходимо указать название модуля";
		}
		
		$module = $this->getModuleByName($moduleName);
		if(!$module) {
			return "Модуль $moduleName не найден";
		}
		
		try {
			if($module->install()) {
				$this->dbAdapter->insert("cms_modules", array(
					'name' => $module->getName(), 
					'active' => true
				));
			}
		} catch (Exception $ex) {
			return "Невозможно установить модуль. <br />
			<code>
			{$ex->getMessage()}
			</code>
			<pre>
			{$ex->getTraceAsString()}
			</pre>";
		}
		header("Location: ".$this->basePath);
	}
	
	public function removeAction() {
		
		$moduleName = $this->getRequestedModuleName();
		if(empty($moduleName)) {
			return "Необходимо указать название модуля";
		}
		
		$module = $this->getModuleByName($moduleName);
		
		if(!$module) {
			return "Модуль $moduleName не найден";
		}
		
		try {
			if($module->remove()) {
				$this->dbAdapter->delete("cms_modules", array("name = ?" => $module->getName()));
			}
		} catch (Exception $ex) {
			return "Невозможно удалить модуль. <br />
			<code>
			{$ex->getMessage()}
			</code>
			<pre>
			{$ex->getTraceAsString()}
			</pre>";
		}
		header("Location: ".$this->basePath);
	}
	
	public function configAction() {
		$moduleName = $this->getRequestedModuleName();
		if(empty($moduleName)) {
			return "Необходимо указать название модуля";
		}
		
		if(($module = $this->getModuleByName($moduleName)) != null) {
			
			/* обработаем сохранение формы вначале */
			if($this->request->isPost()) {
				$config = $this->request->getPost("config");
				if($config) {
					$module->saveDbConfig($config);
					header("Location: ".$this->basePath.'/config/'.$moduleName.'/');
					exit();
				}
			}
			
			/* запрашиваем у модуля его конфиг.параметры */
			$moduleFields = $module->getConfig();
			
			$this->smarty->assign(array(
				'fields' => $moduleFields, 
				'module' => $module, 
				'basePath' => $this->basePath
			));
			
			return $this->smarty->fetch("config.html");
		}
	}
	
	public function onAction() {
		$moduleName = $this->getRequestedModuleName();
		if(empty($moduleName)) {
			return "Необходимо указать название модуля";
		}
		
		$module = $this->getModuleByName($moduleName);
		
		if(!$module) {
			return "Модуль $moduleName не найден";
		}
		
		$this->dbAdapter->update("cms_modules", array("active" => 1), array("name = ?" => $module->getName()));
		
		header("Location: ".$this->basePath);
	}
	
	public function offAction() {
		$moduleName = $this->getRequestedModuleName();
		if(empty($moduleName)) {
			return "Необходимо указать название модуля";
		}
		
		$module = $this->getModuleByName($moduleName);
		
		if(!$module) {
			return "Модуль $moduleName не найден";
		}
		
		$this->dbAdapter->update("cms_modules", array("active" => 0), array("name = ?" => $module->getName()));
		
		header("Location: ".$this->basePath);
	}
	
	public function dependenciesAction() {
		$res = array(
			1 => array(
				'id' => 1, 
				'fid' => 45, 
				'url' => '', 
				'title' => 'Шаблоны', 
				'_fields' => array(
					array(
						'id' => 1, 
						'fid' => 45, 
						'title' => 'Постановщик', 
						'url' => '#'
					), 
					array(
						'id' => 1,
						'fid' => 47,
						'title' => 'Ответственный', 
						'url' => '#'
					),
				)
			)
		);
		
		return  json_encode($res);
	}
	
	public function getCSSFiles() {
		return array('/cms/css/module_modules.css');
	}
	
	
	public function templatesAction() {
		
		$moduleName = $this->getRequestedModuleName();
		if(empty($moduleName)) {
			return "Необходимо указать название модуля";
		}
		
		if(($module = $this->getModuleByName($moduleName)) != null) {
			
			/* обработаем сохранение формы вначале */
			if($this->request->isPost()) {
				$template = $this->request->getPost("template");
				if($template) {
					if($this->request->getPost("save")) {
						$module->saveTemplate($template['name'], $template['content'], $template['id']);
						header("Location: ".$this->basePath.'/templates/'.$moduleName.'/');
						exit();
					}
					if($this->request->getPost("delete")) {
						$module->deleteTemplate($template['id']);
						header("Location: ".$this->basePath.'/templates/'.$moduleName.'/');
						exit();
					}					
				}
			}
			
			/* запрашиваем у модуля его конфиг.параметры */
			$filter = array();
			$templates = $module->getTemplates($filter);
			
			if($templates) foreach($templates as $key => $template) {
				$length = strlen($template['content']);
				$templates[$key]['content'] = $length > 100 ? substr($template['content'], 0, 100) : $template['content'];
			}
			
			$this->smarty->assign(array(
				'templates' => $templates, 
				'module' => $module, 
				'basePath' => $this->basePath
			));
			
			return $this->smarty->fetch("templates.html");
		}
	}
	
	public function templateAction() {
		$response = array(
			'error' => false
		);
		$moduleName = $this->getRequestedModuleName();
		if($moduleName) {
			$module = $this->getModuleByName($moduleName);
			if($module) {
				$id = isset($this->urlParts[2]) ? $this->urlParts[2] : null;
				
				if($id) {
					try {
						$template = $module->getTemplateArray($id);
						$response = $template;
					} catch (Exception $ex) {
						$response['error'] = "Шаблон с id = $id не найден. Ошибка: ".$ex->getMessage();
					}
				} else {
					$response['error'] = 'Не указан ID запрашиваемого шаблона';
				}
			} else {
				$response['error'] = 'Не найден модуль "'.$moduleName.'"';
			}
		} else {
			$response['error'] = 'Не указано имя модуля';
		}
		
		echo json_encode($response);
		exit();
	}
	
	/**
	 * Получить список доступных модулей
	 * @return array - массив из FrontModuleAbstract
	 */
	public function getModuleList() {
		
		$moduleList = array();
		
		$path = $this->modulePath;
		$dh = opendir($path);
		if($dh) {
			while(($file = readdir($dh)) != false) {
				if($file != '.' && $file != '..') {
					$filename = $path . '/' . $file;
					if(is_dir($filename)) {
						$moduleFilename = $file.'.php';
						$moduleFullFilename = $filename . '/'.$moduleFilename;
						if(file_exists($moduleFullFilename)) {
							require_once $moduleFullFilename;
							$className = '\\front\\modules\\'.$file;
							if(class_exists($className)) {
								$class = new ReflectionClass($className);
								if(!$class->isAbstract()) {
									/* @var $module \front\module\Module */
									$module = new $className; 
									$module->setDi($this->di);
									$moduleList[] = $module;	
								}							
							} 
						}
					}
				}
			}
		}
		
		return $moduleList;
	}
	
	/**
	 * Получить объект модуля по его названию
	 * @param string $name
	 * @return FrontModuleAbstract
	 */
	public function getModuleByName($name) {
		foreach($this->getModuleList() as $module) {
			if($module->getName() == $name) {
				return $module;
			}
		}
	}
	
	/**
	 * Получить имя запрашиваемого через URL модуля
	 * 
	 * @return string
	 */
	public function getRequestedModuleName() {
		return isset($this->urlParts[1]) ? $this->urlParts[1] : null;
	}
}

?>