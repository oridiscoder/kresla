<?php

abstract class ModuleAbstract {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	
	/**
	 *
	 * @var Bof_Request
	 */
	protected $request;
	
	/**
	 *
	 * @var Zend_Controller_Router_Rewrite
	 */
	protected $router;
	
	private $cssFiles;
	private $jsFiles;
	
	public function getCSSFiles() {
		return $this->cssFiles ? $this->cssFiles : array();
	}
	public function getJSFiles() {
		return $this->jsFiles ? $this->jsFiles : array();
	}
	public function appendCSSFiles($files) {
		if(is_array($this->cssFiles)) {
			$this->cssFiles = array_merge($this->cssFiles, $files);
		} else {
			$this->cssFiles = $files;
		}
	}
	public function appendJSFiles($files) {
		if(is_array($this->jsFiles)) {
			$this->jsFiles = array_merge($this->jsFiles, $files);
		} else {
			$this->jsFiles = $files;
		}
	}
	
	/**
	 *
	 * @param SectionPage $page
	 */
	public function __construct($request, $db, $smarty) {
		$this->dbAdapter = $db;
		$this->request = $request;
		$this->smarty = clone $smarty;
		$this->smarty->setTemplateDir($this->smarty->getTemplateDir(0).'/modules/'.get_class($this));
		$this->router = new Zend_Controller_Router_Rewrite();
		$this->router->removeDefaultRoutes();
	}
	/**
	 * @param string name of the route
	 * @param Zend_Controller_Router_Route $route
	 */
	protected function addRoute($name, $route) {
		$this->router->addRoute($name, $route);
	}
	
	public function routeRequest() {
		$this->router->route($this->request);
		$actionName = $this->request->action;
		$methodName = 'render'.ucfirst($actionName);
		if(method_exists($this, $methodName)) {
			return call_user_method($methodName, $this);
		} else {
			throw new Exception("Method name $methodName not found in module ".get_class($this));
		}
	}
	/**
	 * @return string - the content to show
	 */
	abstract public function render();
	abstract public function getTitle();
	abstract public function getBreadcrumbs();
}

?>