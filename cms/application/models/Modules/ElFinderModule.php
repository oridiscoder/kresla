﻿<?php
require_once "ModuleAbstract.php";

class ElFinderModule extends ModuleAbstract {
	
	public function getTitle() {
		return "Проводник";
	}
	public function getBreadcrumbs() {
	}
	public function render() {
		
		if(isset($_GET['connector'])) {
			$this->runConnector();
		} else {
			return $this->smarty->fetch("index.html");
		}
	}
	
	public function runConnector() {
		include_once 'ElFinder/elFinderConnector.class.php';
		include_once 'ElFinder/elFinder.class.php';
		include_once 'ElFinder/elFinderVolumeDriver.class.php';
		include_once 'ElFinder/elFinderVolumeLocalFileSystem.class.php';
		
		
		
		$opts = array(
			'debug' => false,
			'roots' => array(
				array(
					'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
					'path'          => realpath(Application::getAppConfigs()->elfinder->path),
					'URL'           => '/img/'
				)
			)
		);
		
		$connector = new elFinderConnector(new elFinder($opts));
		$connector->run();
		exit();
	}
	
	public function access($attr, $path, $data, $volume) {
	return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
		? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
		:  null;                                    // else elFinder decide it itself
	}
	
	
}