<?php

require_once ('ModuleAbstract.php');

class UserManager extends ModuleAbstract {
	private $breadcrumbs;
	private $errors;
	/* (non-PHPdoc)
	 * @see ModuleAbstract::render()
	 */
	public function render() {
		
		$this->addRoute("list", new Zend_Controller_Router_Route_Regex('.*section/(\d+)/(\d+)',
			array('action' => 'list'),
			array(1=> 'sectionID', 2=>'pageID')
		));
		$this->addRoute("user", new Zend_Controller_Router_Route_Regex('.*section/(\d+)/(\d+)/user/(\d+)',
				array('action' => 'user'),
				array(1=> 'sectionID', 2=>'pageID', 3=>'userID')
		));
		$this->addRoute("group", new Zend_Controller_Router_Route_Regex('.*section/(\d+)/(\d+)/group/(\d+)',
				array('action' => 'group'),
				array(1=> 'sectionID', 2=>'pageID', 3=>'groupID')
		));
		$this->addRoute("groupAccess", new Zend_Controller_Router_Route_Regex('.*section/(\d+)/(\d+)/group_access/(\d+)',
				array('action' => 'groupAccess'),
				array(1=> 'sectionID', 2=>'pageID', 3=>'groupID')
		));
		$this->addRoute("fieldAccess", new Zend_Controller_Router_Route_Regex('.*section/(\d+)/(\d+)/field_access/(\d+)',
				array('action' => 'fieldAccess'),
				array(1=> 'sectionID', 2=>'pageID', 3=>'groupID')
		));
		return $this->routeRequest();
	}
	
	public function  renderList() {
		
		$groups = $this->dbAdapter->fetchAll("SELECT id, name FROM bof_groups WHERE hidden = 0 ORDER BY name");
		foreach ($groups as $key=>$group) {
			$groups[$key]['_users'] = $this->dbAdapter->fetchAll("SELECT * FROM bof_users WHERE group_id = ? ORDER BY login", $group['id']);
		}
		
		$baseURL = $this->request->getPathInfo();

		$this->breadcrumbs[] = array(
			'href' => $baseURL,
			'title' => $this->getTitle()
		);
		$this->smarty->assign('baseURL', $baseURL);
		$this->smarty->assign('groups', $groups);
		return $this->smarty->fetch("list.html");
	}
	
	public function renderUser() {
		
		$baseURL = '/section/'.$this->request->sectionID.'/'.$this->request->pageID.'/';
		
		if($this->request->isPost()) {
			if($this->request->getParam("delete")) {
				$this->deleteUser($this->request->getPost("id"));
				header("Location: {$baseURL}");
				exit();
			} else {
				$id = $this->saveUser();
				$location = $baseURL.'user/'.$id.'/?saved=1';
				header("Location: $location");
				die();
			}
		}
		
		$id = $this->request->userID;
		
		if($id) {
			$user = $this->dbAdapter->fetchRow("SELECT * FROM bof_users WHERE id = ?", $id);
		} else {
			$user = array(
					'id' => 0,
					'login' => null,
					'password' => null,
					'group_id' => isset($this->request->group_id) ? $this->request->group_id : null,
					'superuser' => false
			);
		}
		$groups = $this->dbAdapter->fetchPairs("SELECT id, name FROM bof_groups WHERE hidden = 0 ORDER BY name");
		
		$tables = $this->getTables();
		$bofAreas = $this->getAreas();
		$bofPages = $this->getPages();
		
		if($id) {
			$tablesAccess = $this->getUserTableAccess($id, $tables);
			$sectionAccess = $this->getUserSectionAccess($id, $bofAreas);
			$pageAccess = $this->getUserPageAccess($id, $bofPages);
		} else {
			$keys = array_keys($groups);
			$groupID = $keys[0];
			$tablesAccess = $this->getGroupTableAccess($groupID, $tables);
			$sectionAccess = $this->getGroupSectionAccess($groupID, $bofAreas);
			$pageAccess = $this->getGroupPageAccess($groupID, $bofPages);
		}
		
		$currentURL = $baseURL.'user/'.$user['id'].'/';
		
		$this->breadcrumbs[] = array(
				'href' => $baseURL,
				'title' => $this->getTitle()
		);
		$this->breadcrumbs[] = array(
				'href' => $currentURL,
				'title' => $user['login'] ? $user['login'] : 'Новый пользователь'
		);
		
		$this->smarty->assign("user", $user);
		$this->smarty->assign("groups", $groups);
		$this->smarty->assign("tables", $tables);
		$this->smarty->assign("areas", $bofAreas);
		$this->smarty->assign("access", $tablesAccess);
		$this->smarty->assign("pagesAccess", $pageAccess);
		$this->smarty->assign("sectionAccess", $sectionAccess);
		$this->smarty->assign("formURL", $currentURL);
		return $this->smarty->fetch("user-form.html");
	}
	
	private function getTables() {
		return $this->dbAdapter->fetchAll("SELECT * FROM bof_objects WHERE hidden = 0 ORDER BY name");
	}
	private function getAreas() {
		$bofAreas = $this->dbAdapter->fetchAll("SELECT * FROM bof_razdels WHERE hidden = 0 ORDER BY name");
		foreach ($bofAreas as $key=>$section) {
			$pages = $this->getPages($section['id']);
			if(!empty($pages)) {
				$bofAreas[$key]['pages'] = $pages;
			}
		}
		return $bofAreas;
	}
	private function getPages($areaID = null) {
		$select = $this->dbAdapter->select()
			->from('bof_pages')
			->where('hidden = 0')
			->order('viewname');
		if($areaID) {
			$select->where("razdel = ?", $areaID);
		}
		return $this->dbAdapter->fetchAll($select);
	}
	private function getProjectAccessTypes() {
		$select = $this->dbAdapter->select()
			->from(array('AT' => 'bof_projects_access_types'))
			->join(array('PT'=>'bof_projects_types'), 'PT.id = AT.project_type', array('access_name', 'access_title' => 'name'))
			->order("AT.id");
		$rows =  $this->dbAdapter->fetchAll($select);
		$result = array();
		foreach($rows as $row) {
			$result[$row['access_name']]['title'] = $row['access_title'];
			$result[$row['access_name']]['rows'][] = $row;
		}
		return $result;
	}
	
	private function getLogins() {
		$select = $this->dbAdapter->select()
			->from("bof_projects_logins_types")
			->order("title");
		return $this->dbAdapter->fetchAll($select);
	}
	
	private function getUserTableAccess($userID, $tables) {
		$access = $this->dbAdapter->fetchAll("SELECT * FROM bof_users_access WHERE type='object' AND user_id = ?", $userID);
		return $this->popullateAccessArray($access, $tables);
	}
	
	private function getUserSectionAccess($userID, $bofAreas) {
		$access = $this->dbAdapter->fetchAll("SELECT * FROM bof_users_access WHERE type='section' AND user_id = ?", $userID);
		return $this->popullateAccessArray($access, $bofAreas);
	}
	
	private function getUserPageAccess($userID, $bofPages) {
		$access = $this->dbAdapter->fetchAll("SELECT * FROM bof_users_access WHERE type='page' AND user_id = ?", $userID);
		return $this->popullateAccessArray($access, $bofPages);
	}
	
	private function getGroupTableAccess($groupID, $tables) {
		$access = $this->dbAdapter->fetchAll("SELECT * FROM bof_groups_access WHERE type='object' AND group_id = ?", $groupID);
		return $this->popullateAccessArray($access, $tables);
	}
	
	private function getGroupSectionAccess($groupID, $bofAreas) {
		$access = $this->dbAdapter->fetchAll("SELECT * FROM bof_groups_access WHERE type='section' AND group_id = ?", $groupID);
		return $this->popullateAccessArray($access, $bofAreas);
	}
	
	private function getGroupPageAccess($groupID, $bofPages) {
		$access = $this->dbAdapter->fetchAll("SELECT * FROM bof_groups_access WHERE type='page' AND group_id = ?", $groupID);
		return $this->popullateAccessArray($access, $bofPages);
	}
	
	private function getGroupProjectAccess($groupID, $accessTypes) {
		$rows = $this->dbAdapter->fetchCol("SELECT access_type FROM bof_groups_project_access WHERE group_id = ?", $groupID);
		$access = array();
		$rows = $rows ? $rows : array();
		foreach ($accessTypes as $type) {
			foreach ($type['rows'] as $row) {
				$access[$row['id']] = in_array($row['id'], $rows);
			}
		}
		return $access;
	}
	
	private function getGroupLoginsAccess($groupID, $type, $logins) {
		$access = $this->dbAdapter->fetchPairs("SELECT obj_id, `read` FROM bof_groups_access WHERE type='logins_$type' AND group_id = ?", $groupID);
		foreach($logins as $login) {
			if(!isset($access[$login['id']])) {
				$access[$login['id']] = false;
			}
		}
		return $access;
	}

	
	private function popullateAccessArray($access, $objects) {
		$array = array();
		if(!empty($access)) foreach($access as $row) {
			$array[$row['obj_id']] = $row;
		}
		$access = $array;
		if(!empty($objects)) foreach($objects as $object) {
			if(!isset($access[$object['id']])) {
				$access[$object['id']] = array(
						'read' => false,
						'write' => false,
						'delete' => false
				);
			}
		}
		return $access;
	}
	
	
	
	public function saveUser() {
		$id = $this->request->getPost('id');
		$user = $this->request->getPost('user');
		$access = $this->request->getPost('access');
		$sectionAccess = $this->request->getPost('section_access');
		$pageAccess = $this->request->getPost('page_access');
		
		if(empty($user['password'])) {
			unset($user['password']);
		} else {
			$user['password'] = md5($user['password']);
		}
		$user['superuser'] = isset($user['superuser']) ? 1 : 0;
		
		if($id) {
			$this->dbAdapter->update("bof_users", $user, "id = $id");
		} else {
			$this->dbAdapter->insert("bof_users", $user);
			$id = $this->dbAdapter->lastInsertId();
		}
		
		$this->dbAdapter->delete("bof_users_access", "user_id = $id");
		
		if(!empty($access)) foreach ($access as $tableID => $mode) {
			$this->dbAdapter->insert("bof_users_access", array(
					'obj_id' => $tableID,
					'user_id' => $id,
					'read' => isset($mode['read']) ? true : false,
					'write' => isset($mode['write']) ? true : false,
					'delete' => isset($mode['delete']) ? true : false,
			));
		}
		if(!empty($sectionAccess)) foreach ($sectionAccess as $tableID => $mode) {
			$this->dbAdapter->insert("bof_users_access", array(
					'obj_id' => $tableID,
					'user_id' => $id,
					'read' => isset($mode['read']) ? true : false,
					'write' => isset($mode['write']) ? true : false,
					'delete' => isset($mode['delete']) ? true : false,
					'type' => 'section'
			));
		}
		
		if(!empty($pageAccess)) foreach ($pageAccess as $tableID => $mode) {
			$this->dbAdapter->insert("bof_users_access", array(
					'obj_id' => $tableID,
					'user_id' => $id,
					'read' => isset($mode['read']) ? true : false,
					'write' => isset($mode['write']) ? true : false,
					'delete' => isset($mode['delete']) ? true : false,
					'type' => 'page'
			));
		}
		
		return $id;
	}
	
	public function deleteUser($id) {
		$this->dbAdapter->delete("bof_users", "id = ".$this->dbAdapter->quote($id));
		$this->dbAdapter->delete("bof_users_access", "user_id = ".$this->dbAdapter->quote($id));
	}
	
	public function renderGroup() {
		
		$baseURL = '/section/'.$this->request->sectionID.'/'.$this->request->pageID.'/';
		
		if($this->request->isPost()) {
			if($this->request->getParam("delete")) {
				if($this->deleteGroup($this->request->getPost("id"))) {
					header("Location: {$baseURL}");
					exit();
				} else {
					$this->errors[] = "Невозможно удалить непустую группу";
				}
			} else {
				$groupID = $this->saveGroup();
				header("Location: {$baseURL}group/$groupID/?saved=1");
				exit();
			}
		}
		
		$group = $this->dbAdapter->fetchRow("SELECT * FROM bof_groups WHERE id = ?", $this->request->groupID);
		if(empty($group)) {
			$group = array(
				'id' => 0,
				'name' => null,
				'parent_id' => null
			);
		}
		
		$groups = $this->dbAdapter->fetchPairs("SELECT id, name FROM bof_groups WHERE hidden = 0 ORDER BY name");
		
		$tables = $this->getTables();
		$bofAreas = $this->getAreas();
		$bofPages = $this->getPages();
		$projectAccessTypes = $this->getProjectAccessTypes();
		$logins = $this->getLogins();
		
		$tableAccess = $this->getGroupTableAccess($group['id'], $tables);
		
		$pageAccess = $this->getGroupPageAccess($group['id'], $bofPages);
		
		$sectionAccess = $this->getGroupSectionAccess($group['id'], $bofAreas);
		
		$projectAccess = $this->getGroupProjectAccess($group['id'], $projectAccessTypes);
		
		$loginsAccess = array(
			'seo' => $this->getGroupLoginsAccess($group['id'], 'seo', $logins),
			'lt' => $this->getGroupLoginsAccess($group['id'], 'lt', $logins),
			'dev' => $this->getGroupLoginsAccess($group['id'], 'dev', $logins)
		);
		
		$deniedFields = $this->getDeniedFields($group['id']);
			
		$currentURL = $baseURL.'group/'.$group['id'].'/';
		
		$this->breadcrumbs[] = array(
				'href' => $baseURL,
				'title' => $this->getTitle()
		);
		$this->breadcrumbs[] = array(
				'href' => $currentURL,
				'title' => $group['name']
		);
		
		$tablePairs = array();
		foreach ($tables as $table) {
			$tablePairs[$table['id']] = $table['name'];
		}
		
		$this->smarty->assign('group', $group);
		$this->smarty->assign("groups", $groups);
		$this->smarty->assign("logins", $logins);
		$this->smarty->assign("tables", $tables);
		$this->smarty->assign("tablePairs", $tablePairs);
		$this->smarty->assign("deniedFields", $deniedFields);
		$this->smarty->assign("areas", $bofAreas);
		$this->smarty->assign("projectAccessTypes", $projectAccessTypes);
		$this->smarty->assign("access", $tableAccess);
		$this->smarty->assign("pagesAccess", $pageAccess);
		$this->smarty->assign("sectionAccess", $sectionAccess);
		$this->smarty->assign("projectAccess", $projectAccess);
		$this->smarty->assign("loginsAccess", $loginsAccess);
		$this->smarty->assign("formURL", $currentURL);
		$this->smarty->assign("errors", $this->errors);
		return $this->smarty->fetch("group-form.html");
	}
	
	public function saveGroup() {
		$group = $this->request->getPost('group');
		$id = $this->request->getPost("id");
		$access = $this->request->getPost('access');
		$sectionAccess = $this->request->getPost('section_access');
		$pageAccess = $this->request->getPost('page_access');
		$projectAccess = $this->request->getPost('project_access');
		$loginAccess = $this->request->getPost("login_access");
		
		if($id) {
			$this->dbAdapter->update("bof_groups", $group, "id = $id");
		} else {
			$this->dbAdapter->insert("bof_groups", $group);
			$id = $this->dbAdapter->lastInsertId();
		}
		
		$this->dbAdapter->delete("bof_groups_access", "group_id = $id");
		
		if(!empty($access)) foreach ($access as $tableID => $mode) {
			$this->dbAdapter->insert("bof_groups_access", array(
				'obj_id' => $tableID,
				'group_id' => $id,
				'read' => isset($mode['read']) ? true : false,
				'write' => isset($mode['write']) ? true : false,
				'delete' => isset($mode['delete']) ? true : false,
				'type' => 'object'
			));
		}
		
		if(!empty($sectionAccess)) foreach ($sectionAccess as $tableID => $mode) {
			$this->dbAdapter->insert("bof_groups_access", array(
					'obj_id' => $tableID,
					'group_id' => $id,
					'read' => isset($mode['read']) ? true : false,
					'write' => isset($mode['write']) ? true : false,
					'delete' => isset($mode['delete']) ? true : false,
					'type' => 'section'
			));
		}
		
		if(!empty($pageAccess)) foreach ($pageAccess as $tableID => $mode) {
			$this->dbAdapter->insert("bof_groups_access", array(
					'obj_id' => $tableID,
					'group_id' => $id,
					'read' => isset($mode['read']) ? true : false,
					'write' => isset($mode['write']) ? true : false,
					'delete' => isset($mode['delete']) ? true : false,
					'type' => 'page'
			));
		}
		
		if(!empty($loginAccess)) foreach ($loginAccess as $projectType => $items) {
			foreach ($items as $loginID => $s) {
				$this->dbAdapter->insert("bof_groups_access", array(
						'obj_id' => $loginID,
						'group_id' => $id,
						'read' => true,
						'write' => false,
						'delete' => false,
						'type' => 'logins_'.$projectType
				));
			}
		}
		
		
		
		$this->dbAdapter->delete("bof_groups_project_access", "group_id = $id");
		if(!empty($projectAccess)) foreach ($projectAccess as $accessID=>$value) {
			$this->dbAdapter->insert("bof_groups_project_access", array(
					'group_id' => $id,
					'access_type' => $accessID
			));
		}
		
		return $id;
	}
	
	public function deleteGroup($id) {
		$users = $this->dbAdapter->fetchAll("SELECT * FROM bof_users WHERE group_id = ?", $id);
		if(empty($users)) {
			$this->dbAdapter->delete("bof_groups", "id = ".$this->dbAdapter->quote($id));
			$this->dbAdapter->delete("bof_groups_access", "group_id = ".$this->dbAdapter->quote($id));
			return true;
		} else {
			return false;
		}
	}
	
	public function renderGroupAccess() {
		$groupID = $this->request->groupID;
		
		$tables = $this->getTables();
		$bofAreas = $this->getAreas();
		$bofPages = $this->getPages();
		
		$tableAccess = $this->getGroupTableAccess($groupID, $tables);
		$pageAccess = $this->getGroupPageAccess($groupID, $bofPages);
		$sectionAccess = $this->getGroupSectionAccess($groupID, $bofAreas);
		
		$this->smarty->assign("areas", $bofAreas);
		$this->smarty->assign("pagesAccess", $pageAccess);
		$this->smarty->assign("sectionAccess", $sectionAccess);
		$this->smarty->assign("access", $tableAccess);
		$this->smarty->assign("tables", $tables);
		return $this->smarty->fetch("access.html");
	}
	
	public function renderFieldAccess() {
		if($this->request->getParam("get_fields")) {
			$tableID = $this->request->getParam("table_id");
			$fields = $this->dbAdapter->fetchAll("SELECT id, name, viewname FROM bof_fields WHERE obj_id = ?", $tableID);
			foreach ($fields as $key=>$field) {
				foreach ($field as $k=>$v) {
					$fields[$key][$k] = iconv('cp1251', 'utf-8', $v);
				}
			}
			echo json_encode($fields);
			exit();
		}
		
		$group = $this->dbAdapter->fetchRow("SELECT * FROM bof_groups WHERE id = ?", $this->request->groupID);
		$this->smarty->assign("group", $group);
		
		if($this->request->getParam("deny_field")) {
			$field = $this->request->getPost("field");
			$groupID = $this->request->getParam("groupID");
			
			$select = $this->dbAdapter->select()
				->from("access_fields")
				->where("group_id = ? ", $groupID)
				->where("field_id = ?", $field['id']);
			
			$lastField = $this->dbAdapter->fetchRow($select);
			
			if($lastField) {
				$this->dbAdapter->update("access_fields", array('D' => 0, 'U' => 0, 'I' => 0, 'S' => 0), array(
					'id' => $lastField['id']
				));
			} else {
				$number = (int) $this->dbAdapter->fetchOne("SELECT MAX(number) + 1 FROM access_fields");
				$this->dbAdapter->insert("access_fields", array(
					'D' => 0,
					'U' => 0,
					'I' => 0,
					'S' => 0,
					'field_id' => $field['id'],
					'group_id' => $groupID,
					'number' => $number
				));
			}
			
			$deniedFields = $this->getDeniedFields($groupID);
			
			$this->smarty->assign("deniedFields", $deniedFields);
			header("Content-type: text/html; charset=utf-8");
			echo $this->smarty->fetch("denied-fields.html");
			exit();
		}
		
		if($this->request->getParam("allow_field")) {
			$groupID = $this->request->getParam("groupID");
			$fieldID = $this->request->getParam("field_id");
			
			$this->dbAdapter->delete("access_fields", array(
					'group_id = ?' => $groupID,
					'field_id = ?' => $fieldID
			));
			
			$deniedFields = $this->getDeniedFields($groupID);
				
			$this->smarty->assign("deniedFields", $deniedFields);
			header("Content-type: text/html; charset=utf-8");
			echo $this->smarty->fetch("denied-fields.html");
			exit();
		}
	}
	
	private function getDeniedFields($groupID) {
		$select = $this->dbAdapter->select()
			->from(array('FA' => 'access_fields'), array())
			->join(array('F' => 'bof_fields'), "F.id = FA.field_id", array('id', 'name', 'viewname'))
			->join(array('T' => 'bof_objects'), 'T.id = F.obj_id', array('table_name' => 'name'))
			->where("FA.group_id = ?", $groupID)
			->order("FA.number");
		return $this->dbAdapter->fetchAll($select);
	}
	
	public function getTitle() {
		return "Группы и пользователи";
	}
	
	public function getBreadcrumbs() {
		return $this->breadcrumbs;
	}


}

?>