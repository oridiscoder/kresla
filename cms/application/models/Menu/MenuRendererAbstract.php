<?php

abstract class MenuRendererAbstract {
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	
	protected $current;
	
	public function __construct($smarty) {
		$this->smarty = $smarty;
	}
	
	public function setCurrent($current) {
		$this->current = $current;
	}
	
	/**
	 *
	 * @param MenuBuilderAbstract $menuBuilder
	 * @return string
	 */
	abstract public function render($menuBuilder);
}

?>