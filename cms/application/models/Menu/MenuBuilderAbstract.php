<?php

abstract class MenuBuilderAbstract {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	public function __construct($dbAdapter) {
		$this->db = $dbAdapter;
	}
	
	abstract public function getMenuItems();
}