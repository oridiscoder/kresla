<?php

require_once ('MenuRendererAbstract.php');

class TopMenuRenderer extends MenuRendererAbstract {
	/* (non-PHPdoc)
	 * @see MenuRendererAbstract::render()
	 */
	public function render($menuBuilder) {
		$menuItems = $menuBuilder->getMenuItems();
		$this->smarty->assign("current", $this->current);
		$this->smarty->assign("menuItems", $menuBuilder->getMenuItems());
		return $this->smarty->fetch("menu/menu-top.html");
	}


}

?>