<?php

class LeftMenuRenderer {
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	private $items;
	private $baseURL;
	private $pageID;
	
	public function __construct($smarty) {
		$this->smarty = $smarty;
		$this->baseURL = "";
	}
	
	public function getItems() {
		return $this->items;
	}
	
	public function setItems($items) {
		$this->items = $items;
	}
	
	public function setSmarty($smarty) {
		$this->smarty = $smarty;
	}

	public function getBaseURL() {
		return $this->baseURL;
	}

	public function setBaseURL($baseURL) {
		$this->baseURL = $baseURL;
	}
	
	public function setPageID($id) {
		$this->pageID = $id;
	}
	
	public function getPageID() {
		return $this->pageID;
	}

	public function render() {
		$pages = $this->getItems();
		
		return $this->renderMenuBlock($pages);
	}
	
	public function renderMenuBlock($pages) {
		
		if(!empty($pages)) foreach ($pages as $key=>$page) {
			$pages[$key]['title'] = $page['viewname'];
			$pages[$key]['href'] = $this->getBaseURL().'/'.$page['id'].'/';
			if($page['id'] == $this->pageID) {
				$pages[$key]['active'] = true;
			} else {
				$pages[$key]['active'] = false;
			}
			if(isset($page['_childs'])) {
				$pages[$key]['children'] = $this->renderMenuBlock($page['_childs']);
			}
		}
		$this->smarty->assign("menu", $pages);
		return $this->smarty->fetch("menu/menu-left.html");
	}

}

?>