<?php
require_once "MenuBuilderAbstract.php";

class LeftMenuBuilder extends MenuBuilderAbstract {
	
	protected $currentSection;
	protected $currentPage;
	protected $menuItems;
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	
	public function __construct($dbAdapter, $smarty) {
		parent::__construct($dbAdapter);
		$this->smarty = $smarty;
	}
	
	public function setCurrentSection($section) {
		$this->currentSection = $section;
	}
	public function setCurrentPage($page) {
		$this->currentPage = $page;
	}
	
	public function getMenuItems() {
		if($this->currentSection) {
			$this->menuItems = $this->getAreaItems($this->currentSection, null);
		}
		return $this->menuItems;
	}
	
	protected function getAreaItems($areaID, $parentID) {
		$select = $this->db->select()
			->from("bof_pages")
			->where("razdel = ?", $areaID)
			->order("number");
		
		if($parentID) {
			$select->where("parent_id = ?", $parentID);
		} else {
			$select->where("parent_id = 0 OR parent_id IS NULL");
		}
		
		$items = $this->db->fetchAll($select);
		
		if(!empty($items)) foreach($items as &$item) {
			$childs = $this->getAreaItems($areaID, $item['id']);
			if(!empty($childs)) {
				$item['_childs'] = $childs;
			}
		}
		
		return $items;
	}
	
	public function render() {
		if(!empty($this->menuItems)) {
			foreach ($this->menuItems as $menuItem) {
				
			}
		}
	}
	
	protected function renderMenuItem($item) {
		
	}
	
}