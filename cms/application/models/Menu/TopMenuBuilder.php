<?php
require_once "MenuBuilderAbstract.php";
/**
 *
 * @author Gourry
 * Стоит верхнее меню бофа (список разделов)
 */
class TopMenuBuilder extends MenuBuilderAbstract {
	
	public function getMenuItems() {
		$select = $this->db->select()
			->from('bof_razdels')
			->where("hidden = 0")

			->order("number");
		$items = $this->db->fetchAll($select);
		$items = $this->filterItems($items);
		
		return $this->appendHref($items);
	}
	
	private function appendHref($items) {
		if(!empty($items)) foreach ($items as &$item) {
			if(!empty($item['url'])) {
				$item['href'] = '/'.$item['url'].'/';
			} else {
				$item['href'] = '/section/'.$item['id'].'/';
			}
		}
		return $items;
	}
	
	private function filterItems($items) {
		$user = Application::getAppUser();
		foreach ($items as $key=>$item) {
			if(!$user->hasSectionAccess($item['id'], User::ACCESS_READ)) {
				unset($items[$key]);
			}
		}
		return $items;
	}
}