<?php
require_once ('BaseOperationAbstract.php');

class EditOperation extends BaseOperationAbstract {
	
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		// TODO Auto-generated method stub
		return "Редактировать запись";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		// TODO Auto-generated method stub
		return '/cms/images.old/edit.png';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		// TODO Auto-generated method stub
		return 'operation-edit';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		// TODO Auto-generated method stub
		return 'Редактировать выбранную запись';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		
		$id = $this->recordID;
		
		$dbTable = $this->dbTable;
		
		if($this->request->isXmlHttpRequest() && $this->request->isPost()) {
			return $this->renderFieldEditForm();
		} elseif($this->request->isPost() && $this->request->getPost("_item")) {
			return $this->groupUpdate();
		}
			
		if(!$this->userCanEditTable($dbTable->getID())) {
			throw new AccessException("Access denied");
		}
		
		$this->dbTable->showHiddenFields();
		
		/*
		 * Сделано для поля "Неявный потомок" (MultipleDescendantField)
		 * Чтоб оно могло по аяксу подгружать необходимые данные
		 */
		if($this->request->isXmlHttpRequest()) {
			if($this->request->getParam("request_field_via_ajax")) {
				$fieldName = $this->request->getParam("field_name");
				$dbTableField =  $this->dbTable->getFields($fieldName);
				
				if($dbTableField) {
					echo $dbTableField->processAjaxRequest($this->request);
					exit();
				}
			}
		}
		
		$dependencies = $request->getParam("dependencies");
		
		if($dependencies) {
			$lastDependencyItem = $dependencies->getLast();
			$this->dbTable->setParentTableConstraints($lastDependencyItem->getField(), $lastDependencyItem->getParentRowID());
		}
		
		$fieldDataError = null;
		$saved = $this->request->saved ? true : false;
		
		$afterSaveAction = 'edit';
		
		if($this->request->isPost()) {
			
			$zUri = Zend_Uri_Http::fromString($this->request->getScheme().'://'.$this->request->getHttpHost().$this->request->getRequestUri());
			
			$data = $this->request->getPost();
			
			$afterSaveAction = $data['_afterSaveAction'];
			
			if(isset($data['delete'])) {
				//может сыграть злую шутку, если в форме есть поле с именем delete
				$this->dbTable->deleteItems(array($data['id']));
				
				$url = preg_replace('#^(.*)edit/.*#', '\\1', $zUri->getPath());
				header("Location: $url");
				exit();
			} else {
				$zUri->setQuery(array(
					'saved' => true,
					'_afterSaveAction' => $afterSaveAction
				));
				try {
					
					$recordID = $this->dbTable->save($data);
					
					//что же делать дальше?
					$returnPath = $this->request->getBasePath();
					$returnQuery['_afterSaveAction'] = $afterSaveAction;
					switch($afterSaveAction) {
						case 'edit': {
							$returnPath = $request->getBasePath().'/operation/'.$this->getName().'/'.$recordID.'/';
							break;
						}
						case 'add': {
							$returnPath = $request->getBasePath().'/operation/'.$this->getName().'/';
							break;
						}
						case 'add_the_same': {
							$returnPath = $request->getBasePath().'/operation/'.$this->getName().'/'.$recordID.'/';
							$returnQuery = array('add_the_same' => 1);
							break;
						}
						case 'show_table': {
							$returnPath = $request->getPost("_return_path");
							unset($returnQuery['_afterSaveAction']);
							break;
						}
						default:
							$returnURL = $request->getBasePath();
							break;
					}
					
					$returnURL = $returnPath . (($returnQuery) ? '?'.http_build_query($returnQuery) : '');
					
					header("Location: ".$returnURL);
					exit();
				} catch (InvalidFieldDataException $ex) {
					$fieldDataError = $ex->getMessage();
				} catch (ProcedureException $ex) {
					$fieldDataError = $ex->getMessage();
				}
			}
		} else {
			
			if($id) {					
				$data = $this->dbTable->getRecord($id);					
				if(!$data) {
					throw new DbTableException("Record with id = $id not found in ".$this->dbTable->getName());
				}
				
				if(isset($this->request->add_the_same)) {
					$data['id'] = null;
					$afterSaveAction = 'add_the_same';
				}

				$this->dbTable->disableComparisonOperators();
			} else {
				$data = $this->dbTable->fetchNew();
			}
			
			if(isset($this->request->_afterSaveAction)) {
				$afterSaveAction = $this->request->_afterSaveAction;
			} else {
				$afterSaveAction = 'show_table';
			}				
		}

		$referer =  $this->request->getServer("HTTP_REFERER");
		if(strpos($referer, "/operation/")) {
			/* пришли с страницы редактирвоания */
			$returnPath = $request->getBasePath();
		} else {
			/* пришли скорее всего с страницы таблицы */
			$returnPath = $referer ? $referer : $request->getBasePath();
		}
		
		/* возможно, некоторые поля не слдует показывать пользователю */
		$fields = $this->dbTable->getFields();
		foreach($fields as $key => $field) {
			/* @var $field DbFieldAbstract */
			if(!$this->userCanEditField($field->getID())) {
				unset($fields[$key]);
			}
		}
		
		$this->dbTable->populateFieldsWithData($data);
		
		$this->smarty->assign(array(
			'action' => $request->getPathInfo(), 
			'fields' => $fields, 
			'fieldDataError' => $fieldDataError, 
			'saved' => $saved, 
			'_afterSaveAction' => $afterSaveAction, 
			'_return_path' => $returnPath,
			'data' => $data
		));			
		
		return $this->smarty->fetch("form.html");
	}
	
	public function renderLink($dataRow) {
		$link = parent::renderLink($dataRow);
		return str_replace("operation/{$this->getClassName()}", "edit", $link);
	}
	
	public function renderFieldEditForm() {
		$form = array(
			'action' => $this->request->getRequestUri(),
			'ids' => $this->request->getPost("item") 
		);
		$fieldName = str_replace("edit_", "", $this->request->getPost("name"));
		$field = $this->dbTable->getFields($fieldName);		
		$field = array(
			'name' => $field->getName(), 
			'edit_html' => $field->renderEdit($this->dbTable->fetchNew())
		);
		$this->smarty->assign(array(
			'form' => $form, 
			'field' => $field
		));
		return $this->smarty->fetch("edit_field_form.html");
	}
	
	public function groupUpdate() {
		$ids = $this->request->getPost("_item");
		$fieldName = $this->request->getPost("_field_name");
		$fieldValue = $this->request->getPost($fieldName);
		$field = $this->dbTable->getFields($fieldName);
		
		if($ids && $field) {
			$field->setValue($fieldValue);
			$this->dbTable->updateFieldValueForRows($field, $ids);
		}
		
		header("Location: ".$this->request->getServer("HTTP_REFERER"));
		exit();
	}
	
	protected function userCanEditTable($tableID) {
		return Application::getAppUser()->hasTableAccess($tableID, User::ACCESS_WRITE);
	}
	protected function userCanEditField($fieldID) {
		return Application::getAppUser()->hasFieldAccess($fieldID);
	}

}

?>