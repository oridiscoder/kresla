<?php

require_once ('BaseOperationAbstract.php');

class DependencyOperation extends BaseOperationAbstract {
	
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Показать зависимые таблицы";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/tree.png';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		return 'dependencies';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return 'Отобразить таблицы, связанные с заданной.';
	}
	
	public function getOnClickJSCode() {
		return 'return Dependencies.showList(this);';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		if($request->isXmlHttpRequest()) {

			$tables = array();
			$depends = $this->dbTable->getDependentTables();
			
			$basePath =  $request->getBasePath();
			
			foreach ($depends as $table) {
				$t = array(
					'title' => $table['viewname'], 
					'url' => $basePath . '/'.implode(',', array($this->recordID, $table['id'], $table['fid'])).'/'
				);
				if(isset($table['_fields'])) foreach($table['_fields'] as $field) {
					$t['_fields'][] = array(
						'title' => $field['name'], 
						'url' => $basePath . '/'.implode(',', array($this->recordID, $table['id'], $field['id'])).'/'
					);
				}
				$tables[] = $t;
			}
			
			return json_encode($tables);
		}		
	}
}

?>