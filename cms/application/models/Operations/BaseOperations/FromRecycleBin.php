<?php

require_once ('BaseOperationAbstract.php');

class FromRecycleBin extends BaseOperationAbstract {
	
	public function getName() {
		return 'restore_from_recycle_bin';
	}
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return 'Восстановить запись из мусорной корзины';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return "/cms/images.old/trash-restore.png";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		return 'operation-from-recycle-bin';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return 'Восстановить запись из мусорной корзины';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		$id = $request->getQuery("id");
		$ids = $request->getQuery("ids");
		if($id) {
			$this->dbTable->update(array('hidden' => 0),$this->dbAdapter->quoteInto("id = ?", $id));
		} elseif($ids) {
			$ids = array_filter(explode(",", $ids));
			if($ids) {
				$this->dbTable->update(array("hidden" => 0), array("id IN (?)" => $ids));
			}
		}
		header("Location: ".$request->getServer("HTTP_REFERER"));
		exit();
		
	}
	
	public function renderLink($dataRow) {
		$link = parent::renderLink($dataRow);
		$link = str_replace("operation/".$this->getClassName(), "restore_from_recycle_bin", $link);
		return $link;
	}


}

?>