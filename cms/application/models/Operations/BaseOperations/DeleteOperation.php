<?php

require_once ('BaseOperationAbstract.php');

class DeleteOperation extends BaseOperationAbstract {
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Удалить насовсем";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images/delete_10x10.png';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		
	}
	
	public function getConfirmText() {
		return "Вы уверены, что хотите удалить навсегда выбранные записи?";
	}
	

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		$id = $this->recordID;
		$ids = $request->getQuery("ids");
		if($id) {
			$this->dbTable->delete(array("id = ?" => $id));
		} elseif($ids) {
			$ids = array_filter(explode(",", $ids));
			if($ids) {
				$this->dbTable->delete(array("id IN (?)" => $ids));
			}
		}
		header("Location: ".$request->getServer("HTTP_REFERER"));
		exit();
	}

}

?>