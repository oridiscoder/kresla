<?php

require_once ('BaseOperationAbstract.php');

class ToRecycleBinOperation extends BaseOperationAbstract {
	
	public function getName() {
		return 'to_recycle_bin';
	}
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Поместить запись в мусорную корзину";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/trash.png';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		return 'operation-to-recycle-bin';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return 'Поместить запись в мусорную корзину';
	}
	
	public function getOnClickJSCode() {
		return 'return recycleBinConfirm({id});';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {		
		$id = $request->getQuery("id");
		$ids = $request->getQuery("ids");
		if($id) {
			$this->dbTable->update(array('hidden' => 1), $this->dbAdapter->quoteInto("id = ?", $id));
		} elseif($ids) {
			$ids = explode(",", $ids);			
			$this->dbTable->update(array('hidden' => 1), array("id IN (?)" =>  $ids));
		}
		header("Location: ".$request->getServer("HTTP_REFERER"));
		exit();
	}
	
	public function renderLink($dataRow) {
		$link = parent::renderLink($dataRow);
		$link = str_replace('operation/'.$this->getClassName(), 'to_recycle_bin', $link);
		$link = str_replace('{id}', $dataRow['id'], $link);
		return $link;
	}


}

?>