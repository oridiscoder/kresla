<?php

require_once ('RecordOperationAbstract.php');

class EditFieldOperation extends RecordOperationAbstract {
	
	private $data;
	private $fieldTypes;
	private $objects;
	private $enums;
	private $aligns;
	private $errors;
	
	const FIELD_TYPE_DESCENDANT = 11;
	const FIELD_TYPE_ENUM = 12;
	const FIELD_TYPE_IMAGE = 19;
	const ENUM_ALIGN = 1;
	
	
	public static $tables = array(3);
	
	public function getName() {
		return "EditOperation";
	}
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return 'Изменить';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/edit.png';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		// TODO Auto-generated method stub
		return 'operation-edit';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		// TODO Auto-generated method stub
		return 'Расширенная версия добавления/редактирования поля';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		
		$data = $this->getRecord($request->recordID);
		
		if($request->isPost()) {
			if($this->saveField($request)) {
				$url = $request->getRequestUri();
				$location = substr($url, 0, strpos($url, "operation"));
				header("Location: $location");
				exit();
			} else {
				$data = $request->getPost('data');
				$data['hidecolumn'] = isset($data['hidecolumn']) ? true : false;
				$data['id'] = $request->recordID;
			}
		}
		
		$action = $request->target ? $request->target : 'index';
		
		
		$dependencies = $request->getParam("dependencies");
		if($dependencies) {
			/* @var $dependencyItem DependencyListItem */
			$dependencyItem = $dependencies->get(0);
			$data['obj_id'] = $dependencyItem->getRowID();
		}
		
		$this->fieldTypes = $this->dbAdapter->fetchPairs("SELECT id, name FROM bof_fieldtypes WHERE hidden = 0 ORDER BY name");
		$this->objects = $this->dbAdapter->fetchPairs("SELECT id, name FROM bof_objects WHERE hidden = 0 ORDER BY name");
		$this->aligns = $this->dbAdapter->fetchPairs("SELECT id, name FROM bof_enums WHERE hidden = 0 AND enum_id = ? ORDER BY name", self::ENUM_ALIGN);
		$this->enums = $this->dbAdapter->fetchPairs("SELECT id, name FROM `bof_enum_types` WHERE hidden = 0 ORDER BY name");
		
		$url = $request->getPathInfo();
		
		if($data['type'] == self::FIELD_TYPE_DESCENDANT) {
			$parentTableFields = $this->getTableFieldsForSelect($data['param']);
			$this->smarty->assign("parent_table_fields", $parentTableFields);
		}
		
		
		$this->smarty->assign('fieldTypes', $this->fieldTypes);
		$this->smarty->assign('tables', $this->objects);
		$this->smarty->assign('aligns', $this->aligns);
		$this->smarty->assign("enums",$this->enums);
		$this->smarty->assign('formURL', $url);
		$this->smarty->assign('data', $data);
		$this->smarty->assign('fieldDataError', $this->errors);
		$this->smarty->assign('saved', false);
		
		$this->data = $data;
		
		if($action == 'index') {
			$details = $this->getDetailedContent($data['type']);
			$this->smarty->assign('details', $details);
			return $this->renderForm();
		} elseif($action == 'getDetails') {
			$content = $this->getDetailedContent($request->getParam("type_id"));
			header("Content-type:text/html; charset=utf-8;");
			echo $content;
			exit();
		} elseif($action == 'getTableFields') {
			$table = $this->dbAdapter->fetchRow("SELECT * FROM bof_objects WHERE id = ?", $this->request->getParam("table_id"));
			if(!$table) {
				throw new ApplicationException("Table with id ".$this->request->getParam("table_id").' not found');
			}
			
			$fields = $this->dbAdapter->fetchAll("SELECT id, name, viewname FROM bof_fields WHERE obj_id = ? AND hidden = 0", $table['id']);
			$naitiveFields = $this->getNativeTableFields($table['name']);
			if(in_array('number', $naitiveFields)) {
				$fields = array_merge(array(array('id' => 'number', 'name' => 'number', 'viewname' => 'number')), $fields);
			}
			
			echo json_encode($fields);
			exit();
		}
	}
	
	private function getDetailedContent($type) {
		$content = null;
		switch($type) {
			case DbFieldAbstract::TYPE_DESCENDANT: {
				$content = $this->renderDescendantPart();
				break;
			}
			case DbFieldAbstract::TYPE_MULTIPLE_DESCENDANT:
				$content = $this->renderMultipleDescendant();
				break;
			case DbFieldAbstract::TYPE_ENUM: {
				$content = $this->renderEnumPart();
				break;
			}
			case DbFieldAbstract::TYPE_IMAGE: {
				$content = $this->renderImagePart();
				break;
			}
			case DbFieldAbstract::TYPE_VIRTUAL_DESCENDANT:
				$content = $this->renderVirtualDescendent();
				break;
			case DbFieldAbstract::TYPE_VIRTUAL_COLUMN:
				$content = $this->renderVirtualColumn();
				break;
			case DbFieldAbstract::TYPE_DATE:
				$content = $this->renderDatePart();
				break;
			case DbFieldAbstract::TYPE_TEXT:
				$content = $this->renderTextPart();
				break;
			case DbFieldAbstract::TYPE_ASSOCIATIVE_ENTITY:
				$content = $this->renderAssociativeEntityPart();
				break;
			default:
				$content = null;
		}
		return $content;
	}
	
	private function getRecord($id) {
		if($id) {
			$data = $this->dbAdapter->fetchRow("SELECT * FROM bof_fields WHERE id = ?", $id);
			$data['_descendant'] = array(
					'tree' => false,
					'notnull' => false, 
					'sections' => false
			);
			$data['_image'] = array(
					'width' => false,
					'height' => false
			);
			$data['_enums'] = array(
					'notnull' => false
			);
			if($data['type'] == DbFieldAbstract::TYPE_DESCENDANT) {
				$param2 = $data['param2'];
				if(strpos($param2, "TREE")!==false) {
					$data['_descendant']['tree'] = true;
				}
				if(strpos($param2, "NOTNULL")!==false) {
					$data['_descendant']['notnull'] = true;
				}
				if(strpos($param2, "SECTIONS")!==false) {
					$data['_descendant']['sections'] = true;
				}
			}
			if($data['type'] == DbFieldAbstract::TYPE_IMAGE) {
				
				$param2 = $data['param2'];
				if(strpos($param2, "x")!==false) {
					$size = explode("x", $param2);
					$data['_image']['width'] = $size[0];
					$data['_image']['height'] = $size[1];
				}
			}
			if($data['type'] == DbFieldAbstract::TYPE_ENUM) {
				$data['_enums'] = array(
						'notnull' => $data['param2']
				);
			}
		} else {
			$data = array(
					'id' => null,
					'type' => null,
					'name' => null,
					'viewname' => null,
					'hint' => null,
					'obj_id' => null,
					'hidecolumn' => false,
					'filter' => null,
					'align' => null,
					'sort_column' => null,
					'param' => null,
					'param2' => null,
					'_descendant' => array(
						'tree' => false,
						'notnull' => false, 
						'sections' => false
					),
					'_image' => array(
						'width' => null,
						'height' => null
					),
					'_enums' => array(
						'notnull' => false
					),
					'is_url_field' => false
			);
		}
		return $data;
	}
	
	private function renderForm() {
		return $this->smarty->fetch("form.html");
	}
	
	private function renderDescendantPart() {
		
		return $this->smarty->fetch("type-descendant.html");
	}
	
	private function renderMultipleDescendant() {
		
		
		
		if($this->request->recordID) {
			$mdFields = $this->dbAdapter->fetchAll("SELECT * FROM bof_field_multiple_descendant WHERE field_id = ? ORDER BY number", $this->request->recordID);
			if(!empty($mdFields)) {
				foreach ($mdFields as $key=>$mdField) {
					$mdFields[$key]['table_fields'] = $this->getTableFieldsForSelect($mdField['table_id']);
				}
			} else {
				$mdFields = $this->getDefaultMDFields();
			}
		} else {
			$mdFields = $this->getDefaultMDFields();
		}
		
		$this->smarty->assign("md_fields", $mdFields);
		
		return $this->smarty->fetch("type-multiple-descendant.html");
	}
	private function getDefaultMDFields() {
		return array(
				array(
						'id' => null,
						'field_id' => null,
						'table_id' => null,
						'table_sort_field_id' => null,
						'table_fields' => array(),
						'tree' => false,
						'notnull' => false
				)
		);
	}
	private function renderEnumPart() {
		return $this->smarty->fetch("type-enums.html");
	}
	private function renderImagePart() {
		return $this->smarty->fetch("type-image.html");
	}
	private function renderDatePart() {
		return $this->smarty->fetch("type-date.html");
	}
	private function renderTextPart() {
		return $this->smarty->fetch("type-text.html");
	}
	private function renderAssociativeEntityPart() {
		
		$select = $this->dbAdapter->select()
			->from(array("F" => "bof_fields"), array())
			->join(array("T" => "bof_objects"), "T.id = F.obj_id", array("id", "viewname"))
			->where("T.associative_entity = 1")
			->where("T.hidden = 0")
			->where("F.hidden = 0")
			->where("F.type = ?", DbFieldAbstract::TYPE_DESCENDANT)
			->where("F.param = ?", $this->data['obj_id'])
			->order("T.number");
		
		$tables = $this->dbAdapter->fetchPairs($select);
		
		$this->smarty->assign("tables", $tables);
		
		return $this->smarty->fetch("type-associative-entity.html");
	}
	private function renderVirtualDescendent() {
		$tables = $this->dbAdapter->fetchAll("SELECT id, name, viewname FROM bof_objects WHERE hidden = 0 order by name");
		$fieldPairs = array();
		foreach($tables as $key=>$table) {
			$fieldPairs[$table['name']] = $this->dbAdapter->fetchPairs("SELECT id, name FROM bof_fields WHERE obj_id = ? AND hidden = 0 ORDER BY name", $table['id']);
		}
		$this->smarty->assign('vdFieldPairs', $fieldPairs);
		
		$tableID = $this->request->getParam("table_id") ? $this->request->getParam("table_id") : $this->data['obj_id'];
		$tableFields = $this->dbAdapter->fetchPairs("SELECT id, name FROM bof_fields WHERE obj_id = ? AND hidden = 0 ORDER BY name", $tableID);
		$this->smarty->assign("selectedTableFields", $tableFields);
		
		
		return $this->smarty->fetch("type-virtual-descendent.html");
	}
	private function renderVirtualColumn() {
		return $this->smarty->fetch("type-virtual-column.html");
	}
	
	/**
	 *
	 * @param Bof_Request $request
	 */
	private function saveField($request) {
		$postData = $request->getPost('data');
		
		if(isset($postData['sort_column']) && is_numeric($postData['sort_column'])) {
			$field = $this->dbAdapter->fetchRow("SELECT * FROM bof_fields WHERE id = ?", $postData['sort_column']);
			if($field) {
				$postData['sort_column'] = $field['name'];
			} else {
				$postData['sort_column'] = null;
			}
		}
		
		$data = array(
			'type' => $postData['type'],
			'name' => $postData['name'],
			'viewname' => $postData['viewname'],
			'hint' => $postData['hint'],
			'sort_column' => isset($postData['sort_column']) ? $postData['sort_column'] : null,
			'obj_id' => $postData['obj_id'],
			'hidecolumn' => isset($postData['hidecolumn']) ? 1 : 0,
			'is_url_field' => isset($postData['is_url_field']) ? 1 : 0,
			'filter' => $postData['filter'],
			'param' => isset($postData['param']) ? $postData['param'] : null,
			'param2' => isset($postData['param2']) ? $postData['param2'] : null,
			'align' => $postData['align']
		);
		
		$id = $request->getPost("id");
		
		if(!$this->urlFieldIsUnique($data, $id)) {
			$this->errors[] = "В текущей таблице уже есть поле с галочкой URL!";
			return false;
		}
		
		//param
		switch($postData['type']) {
			case DbFieldAbstract::TYPE_DESCENDANT:
				$param2Prts = array();
				if(isset($postData['_descendant']['notnull'])) {
					$param2Prts[] = 'NOTNULL';
				}
				if(isset($postData['_descendant']['tree'])) {
					$param2Prts[] = 'TREE';
				}
				if(isset($postData['_descendant']['sections'])) {
					$param2Prts[] = 'SECTIONS';
				}				
				if(!empty($param2Prts)) {
					$data['param2'] = implode(",", $param2Prts);
				}
				break;
			case DbFieldAbstract::TYPE_MULTIPLE_DESCENDANT:
				$md = $postData['_md'];
				$mdFiltered = array();
				if(!empty($md)) {
					foreach ($md['tables'] as $number => $mdTableID) {
						if($mdTableID) {
							if(!isset($mdFiltered[$mdTableID])) {
								$mdFiltered[$mdTableID] = array(
									'table_id' => $mdTableID,
									'field_id' => $md['fields'][$number] == 'number' ? 0 : $md['fields'][$number],
									'notnull' => isset($md['notnull'][$number]) ? true : false,
									'tree' => isset($md['tree'][$number]) ? true : false
								);
							} else {
								$this->errors[] = "Таблица с ID $mdTableID указана более одного раза. Повторения не будут учтены.";
							}
						}
					}
				}
				
				if(empty($mdFiltered) || count($mdFiltered) == 1) {
					$this->errors[] = "Для данного типа поля необходимо указать более одной связанной таблицы. Для одной таблицы используйте поле <b>потомок</b>";
					return false;
				}
				break;
			case DbFieldAbstract::TYPE_IMAGE:
				$width = $postData['_image']['width'];
				$height = $postData['_image']['height'];
				$data['param2'] = $width.'x'.$height;
				break;
			case DbFieldAbstract::TYPE_ENUM:
				if(isset($postData['_enums']['notnull'])) {
					$data['param2'] = 'NOTNULL';
				}
				break;
		}
		
		if($id) {
			if($request->getPost('delete_this_record')) {
				$this->dropField($data);
				
				if($data['type'] == DbFieldAbstract::TYPE_MULTIPLE_DESCENDANT) {
					$mdData = $data;
					$mdData['name'] = $mdData['name'].'__table';
					$this->dropField($mdData);
				}
				
				$this->dbAdapter->delete("bof_fields", "id = $id");
			} else {
				$oldField = $this->dbAdapter->fetchRow("SELECT * FROM bof_fields WHERE id = ?", $id);
				if($oldField['obj_id'] == $data['obj_id']) {
					$this->createField($data);
				} else {
					$this->errors[] = "Внимание! При переносе поля от одного объекта к другому Вам необходимо Руками удалять и создавать поля соответствующих таблиц";
				}
				$this->dbAdapter->update("bof_fields", $data, "id = $id");
			}
		} else {
			
			$data['number'] = (int)$this->dbAdapter->fetchOne("SELECT MAX(number) + 1 FROM bof_fields");
			
			if($this->tableHasField($data['obj_id'], $data['name'])) {
				$this->errors[] = "Поле с именем ".$data['name']." уже существует в таблице (".$data['obj_id'].')';
				return false;
			}
			
			try {
				$this->createField($data);
				
				if($data['type'] == DbFieldAbstract::TYPE_MULTIPLE_DESCENDANT) {
					$mdData = $data;
					$mdData['name'] = $mdData['name'].'__table';
					$this->createField($mdData);
				}
				
				$this->dbAdapter->insert("bof_fields", $data);
				$id = $this->dbAdapter->lastInsertId();
			} catch (Exception $ex) {
				$this->errors[] = "В процессе создания поля ".$data['name']." произошла ошибка:".$ex->getMessage();
				return false;
			}
		}
		
		if($postData['type'] == DbFieldAbstract::TYPE_MULTIPLE_DESCENDANT) {
			$this->dbAdapter->delete("bof_field_multiple_descendant", array("field_id = ?" => $id));
			$number = 1 + (int) $this->dbAdapter->fetchOne("SELECT MAX(number) FROM bof_field_multiple_descendant");
			
			$inserts = array();
			foreach ($mdFiltered as $mdField) {
				$inserts[] = '(' . implode(',', array(
					$number ++,
					$id,
					$mdField['table_id'],
					$mdField['field_id'],
					(int) $mdField['notnull'],
					(int) $mdField['tree']
				)). ')';
			}
			
			$this->dbAdapter->query("INSERT INTO
					bof_field_multiple_descendant(number, field_id, table_id, table_sort_field_id, notnull, tree)
					VALUES ".implode(',', $inserts));
		}
		
		return true;
	}
	
	private function tableHasField($tableID, $fieldName) {
		$field = $this->dbAdapter->fetchRow("SELECT name FROM bof_fields WHERE obj_id = $tableID AND name = ?", $fieldName);
		return $field ? true : false;
	}
	
	private function createField($fieldData) {
		if(!$this->isVirtual($fieldData['type'])) {
			$table = $this->dbAdapter->fetchRow("SELECT * FROM bof_objects WHERE id = ?", $fieldData['obj_id']);
			$tableName = $table['name'];
			$fieldName = $fieldData['name'];
			
			$field = $fieldData;
			$field['_table_object'] = $this->dbTable;
			$field['id'] = null;
			$field = DbFieldAbstract::factory($field);
			
			$fields = $this->getNativeTableFields($tableName);
			
			$columnDefinition = $field->getCreateColumnDefinition();
			
			if($columnDefinition) {
				if(!in_array($fieldName, $fields)) {
					$sql = "ALTER TABLE $tableName ADD COLUMN $columnDefinition";
				} else {
					$sql = "ALTER TABLE $tableName MODIFY COLUMN $columnDefinition";
				}
				
				$this->dbAdapter->query($sql);
			}
		}
	}
	
	private function dropField($fieldData) {
		if($fieldData['type'] != DbFieldAbstract::TYPE_VIRTUAL_DESCENDANT) {
			$table = $this->dbAdapter->fetchRow("SELECT * FROM bof_objects WHERE id = ?", $fieldData['obj_id']);
			$tableName = $table['name'];
			$fieldName = $fieldData['name'];
			$fields = $this->getNativeTableFields($tableName);
			if(in_array($fieldName, $fields)) {
				$this->dbAdapter->query("ALTER TABLE `$tableName` DROP $fieldName");
			}
		}
	}
	
	private function getNativeTableFields($tableName) {
		return $this->dbAdapter->fetchCol("DESCRIBE `$tableName`");
	}
	
	/**
	 * Получить список зарегистрированных полей у заданной таблицы,
	 * добавить к этому списку поле number
	 * @param integer $tableID - id таблицы
	 * @return array
	 */
	private function getTableFieldsForSelect($tableID) {
		$parentTableFields = $this->dbAdapter->fetchAll("SELECT * FROM bof_fields WHERE obj_id = ?", $tableID);
		$parentTableFields = array_merge(array(array(
				'id' => 'number',
				'name' => 'number',
				'viewname' => 'number'
		)), $parentTableFields);
		return $parentTableFields;
	}
	
	private function isVirtual($fieldType) {
		return $fieldType == DbFieldAbstract::TYPE_VIRTUAL_DESCENDANT || $fieldType == DbFieldAbstract::TYPE_VIRTUAL_COLUMN;
	}
	
	/**
	 * Проверка поля на уникальность галочки is_url_field в пределах текущей таблицы
	 * @param array $field - данные, подготовленный для сохранения в bof_fields
	 * @param integer|null $id - текущего сохраняемого поля
	 */
	private function urlFieldIsUnique($field, $id) {
		if($field['is_url_field']) {
			$select = $this->dbAdapter->select()
				->from("bof_fields", array('id'))
				->where("obj_id = ?",  $field['obj_id'])
				->where("is_url_field = 1");
			if($id) {
				$select->where("id <> $id");
			}
			$result = $this->dbAdapter->fetchCol($select);
			return empty($result);
		} else {
			return true;
		}
	}
}

?>