<?php

require_once ('TableOperationAbstract.php');

class ExportOperation extends TableOperationAbstract {

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/ex_in.jpg';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		return 'operation-export';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return 'Функция экспорта данной таблицы в текстовый файл с разделителями табуляцией. Такой файл можно открыть MS Excel.';
	}

	/**
	 * (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		
		$dbTable = new DbTable($this->dbAdapter, $this->tableID);
		
		require_once "models/Table/DbTableRenderer.php";
		
		if(!$request->isPost()) {
			$this->smarty->assign("fields", $this->getAmbiguousFields($dbTable));
			$content = $this->smarty->fetch("index.html");
			
			$renderer = new DbTableRenderer(Application::getAppSmarty(), array(
				'dbTable' => $dbTable
			));
			$tableContent = $renderer->render();
			
			return $content . $tableContent;
		} else {
			$part = $request->getPost("part");
			if($part == 'all') {
				$request->setQuery("onPage", 0);
			}
			
			DbTableRenderer::prepareDbTable($dbTable, $this->request);			
			$dbTable->showHiddenFields();
			
			$fields = $dbTable->getFields();
			//убрать поле Number
			foreach ($fields as $key=>$field) {
				if($field instanceof NumberField) {
					unset($fields[$key]);
				}
			}
			$fields = array_values($fields);
			$rows = $dbTable->getRows();
						
			$result = "";
			$fieldsCount = count($fields);
			$ambiguousFields = $request->getPost("field");
			
			$tmpFile = tmpfile();
			
			$length = 0;
			
			foreach($rows as $row) {
				$data = array();
				foreach ($fields as $key=>$field) {
					if($field instanceof IdField && !$request->getPost('include_id')) {
						continue;
					}
					if($field instanceof DescendantField && $ambiguousFields[$field->getName()] == 'value') {
						$data[] = $field->extractValue($row);
					}elseif($field instanceof EnumField) {
						$data[] = $field->extractValue($row);
					} else {
						$value = @$row[$field->getName()];
						if(is_float($value)) {
							$value = number_format($value, 2, '.', '');
						}
						$data[] = $value;
					}
				}
				$length += fputcsv($tmpFile, $data, ";");
			}

			if($request->getPost("get_as_file")) {
				header("Content-Disposition: attachment; filename=".$dbTable->getName().".csv");
				header("Content-type: text/txt; charset=utf-8");
				fseek($tmpFile, 0);
				echo fread($tmpFile, $length);
				exit();
			} else {
				fseek($tmpFile, 0);
				return fread($tmpFile, $length);
			}
		}
		
	}
	private function getAmbiguousFields($dbTable) {
		$ambiguousFields = array();
		$fields = $dbTable->getFields();
		foreach ($fields as $field) {
			if($field instanceof DescendantField) {
				$ambiguousFields[] = $field;
			}
		}
		return $ambiguousFields;
	}
	
	private function code_system_chars($in)	{
		$in = str_replace("\t","\\t",$in);
		$in = str_replace("\n","\\n",$in);
		$in = str_replace("\r","\\r",$in);
		return $in;
	}
	
	private function export() {
		
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Экспорт в Excel";
	}


}

?>