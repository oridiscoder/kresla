<?php

require_once ('TableOperationAbstract.php');

class ImportOperation extends TableOperationAbstract {
	private $errors;
	
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Импортировать из Excel";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/ex_out.jpg';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		return 'operation-import';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return 'Функция импорта из текстового файла с разделителями табуляцией. ВНИМАНИЕ. Будьте аккуратны при использовании данной функции.';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		$this->request = $request;
		
		$this->dbTable = $this->dbTable;
		
		if(!$request->isPost()) {
			$content = $this->renderForm();
		} else {
			$content = $this->processImport();
		}
		return $content;
	}
	
	private function renderForm() {
		$this->smarty->assign("fields", $this->getAmbiguousFields());
		$content = $this->smarty->fetch("import-form.html");
		return $content;
	}
	
	private function processImport() {
		if($this->uploadedImportFile()) {
			
			if($this->request->getParam("make_backup")) {
				$this->createBackup();
			}
			
			$this->dbTable->showHiddenFields();
		
			$fields = $this->dbTable->getFields();
			$fields = $this->removeNumberAndIDFromFields($fields);
			$fieldsCountInRow = $this->request->getPost("include_id") ? count($fields) + 1 : count($fields);
		
			ini_set("auto_detect_line_endings", true);
			
			$fh = fopen($_FILES['dump']['tmp_name'], "r");
			
			$successefullProcessed = 0;
			
			$rowNumber = 0;
		
			if($fh) {
				while(($row = fgetcsv($fh, 0, ";")) != null) {
					
					$columns = $row;
					
					$rowNumber ++;
					
					if(count($columns) != $fieldsCountInRow) {
						$this->errors[] = "Строка ".($rowNumber)." игнорирована из-за неверного кол-ва столбцов (".count($columns)."), ожидалось: $fieldsCountInRow";
						continue;
					}
		
					$id = null;
					if($this->request->getPost("include_id")) {
						$id = array_shift($columns);
						if(!is_numeric($id)) {
							$this->errors[] = "Строка ".($rowNumber)." игнорирована из-за того, что первый столбец имеет нецелочисленное значение";
							continue;
						}
						$columns = array_values($columns);
					}
		
					$data = array();
					foreach ($fields as $key=>$field) {
						$value = $columns[$key];
						
						if(is_float($value)) {
							$value = str_replace(",", ".", $value);
						}
						
						$data[$field->getName()] = $value;
						
						if($field instanceof DescendantField) {
							if(!empty($value) && !is_numeric($value)) {
								$this->errors[] = "Для поля типа Потомок задано нецеллочисленное значение {$value}. Строка: ".($rowNumber+1).", столбец $key";
								continue 2;
							}
						}
						if($field instanceof EnumField) {
							if($value) {
								$enum = $field->getEnumByName($value);
								if(!$enum) {
									$this->errors[] = "Не найдено значение перечисления. Строка: ".($rowNumber+1).", столбец $key";
									continue 2;
								}
								$data[$field->getName()] = $enum['id'];
							} else {
								$data[$field->getName()] = null;
							}							
						}
						if($field instanceof DateField) {
							/* @var $field DateField */
							if($field->showTime()) {
								$data[$field->getName()] = strftime("%Y-%m-%d %H:%M:%S", strtotime($value));
							} else {
								$data[$field->getName()] = strftime("%Y-%m-%d", strtotime($value));
							}
						}
					}
		
					try {
						if($id) {
							$this->dbAdapter->update($this->dbTable->getName(), $data, "id = $id");
						} else {
							$this->dbAdapter->insert($this->dbTable->getName(), $data);
						}
						$successefullProcessed ++;
					} catch (Exception $ex) {
						$this->errors[] = "Ошибка при попытке сохранения строки ".($rowNumber+1).":".$ex->getMessage();
					}
				}
			}
		
			$this->smarty->assign("totalImportRows", $rowNumber);
			$this->smarty->assign("successefullProcessed", $successefullProcessed);
			$this->smarty->assign("errors", $this->errors);
			return $this->renderForm();
		}
	}
	
	private function uploadedImportFile() {
		return isset($_FILES['dump']) && $_FILES['dump']['error'] == UPLOAD_ERR_OK;
	}
	
	private function removeNumberAndIDFromFields($fields) {
		foreach ($fields as $key=>$field) {
			if($field instanceof NumberField) {
				unset($fields[$key]);
			}
			if($field instanceof IdField) {
				unset($fields[$key]);
			}
		}
		$fields = array_values($fields);
		return $fields;
	}
	
	private function getAmbiguousFields() {
		$ambiguousFields = array();
		$fields = $this->dbTable->getFields();
		foreach ($fields as $field) {
			if($field instanceof DescendantField) {
				$ambiguousFields[] = $field;
			}
		}
		return $ambiguousFields;
	}
	
	private function createBackup() {
		$sourceTableName = $this->dbTable->getName();
		$row = $this->dbAdapter->fetchRow("SHOW CREATE TABLE `".$sourceTableName."`");
		$createSql = $row['Create Table'];

		$createSql = str_replace("ENGINE=InnoDB","ENGINE=myISAM",$createSql);
		$backupTablename = "`_".$sourceTableName."_".gmdate("Y_m_d_H-i-s")."`";
		$createSql = str_replace("`".$sourceTableName."`",$backupTablename,$createSql);
		try {
			$this->dbAdapter->query($createSql);
			$insertSql = "INSERT INTO $backupTablename SELECT * FROM `".$sourceTableName."`;";
			$this->dbAdapter->query($insertSql);
		} catch(Exception $ex) {
			$this->errors[] = "В результате создания резервной копии таблицы $sourceTableName => $backupTablename произошла ошибка: ".$ex->getMessage();
		}
	}
	
	public function showInGroupSelect() {
		return true;
	}


}

?>