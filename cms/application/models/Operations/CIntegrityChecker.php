<?php

require_once ('TableOperationAbstract.php');

class ErrorFieldDecorator {
	private $fieldObject;
	private $cRows;
	private $cIds;
	public function __construct($field, $corruptedRows) {
		
		$this->fieldObject = $field;
		$this->cRows = $corruptedRows;
		$this->cIds = array_keys($corruptedRows);
	}
	public function renderCell($dataRow) {
		
		$cell = $this->fieldObject->renderCell($dataRow);
		if(in_array($this->fieldObject->getName(), $this->cRows[$dataRow['id']])) {
			return '<div style="background:#F00;height:100%;">'.$cell."</div>";
		} else {
			return $cell;
		}
	}
	
	public function __call($name, $arguments) {
		if($name !== 'renderCell') {
			return call_user_func_array(array($this->fieldObject, $name), $arguments);
		}
	}
}

class CIntegrityChecker extends TableOperationAbstract {
	
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Проверка целостности данный таблицы.";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/integrity.png';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return "Скрипт ищет записи в таблице, которые содержат битые ссылки на родительские элементы, т.е. на несуществующие элементы.";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		
		$table = new DbTable($this->dbAdapter, $this->tableID);
		$fieldsToSelect = array();
		foreach ($table->getFields() as $field) {
			if($field instanceof DescendantField) {
				$fieldsToSelect[] = $field->getName();
			}
		}
		$select = $this->dbAdapter->select()
			->from($table->getName(), array_merge(array('id'), $fieldsToSelect));
		$tableRows = $this->dbAdapter->fetchAll($select);
		
		$corruptedRows = array();
		
		if(!empty($tableRows)) {
			foreach ($tableRows as $row) {
				foreach ($fieldsToSelect as $fieldName) {
					$field = $table->getFields($fieldName);
					$parentTableName = $field->getParentTableName();
					$value = $row[$fieldName];
					
					if($value || !$field->allowEmptyValue()) {
						$parent = $this->dbAdapter->fetchRow("SELECT * FROM `$parentTableName` WHERE id = ?", $value);
							
						if(!$parent) {
							$corruptedRows[$row['id']][] = $fieldName;
						}
					}
				}
			}
		}
		
		if(!empty($corruptedRows)) {
			
			$ids = array_keys($corruptedRows);
		
			$request = clone $this->getRequest();
			$request->setQuery("id", implode(",", $ids));
			
			$page = new DbTablePage($this->page->getPageID(), $this->dbAdapter, $this->page->getSmarty(), $request);
			$page->getDbTable()->showHiddenFields();
			$fields = $page->getDbTable()->getFields();
			foreach ($fields as &$field) {
				if(in_array($field->getName(), $fieldsToSelect)) {
					$page->getDbTable()->setField($field->getName(), new ErrorFieldDecorator($field, $corruptedRows));
				}
			}
			$this->smarty->assign("page", $page);
		}
		
		$this->smarty->assign("rows", $tableRows);
		$this->smarty->assign("corruptedRows", $corruptedRows);
		$this->smarty->assign("table", $table);
		
		
		return $this->smarty->fetch("form.html");
	}


}

?>