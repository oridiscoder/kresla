<?php

require_once ('RecordOperationAbstract.php');

class CMoveFirstOperation extends RecordOperationAbstract {
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Передвинуть запись в начало списка";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return "/cms/images.old/move_first.png";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return $this->getTitle();
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		
		$table = $this->getTableById($this->tableID);
		$tableName = $table['name'];
		
		if(Application::getAppUser()->hasTableAccess($this->tableID, User::ACCESS_WRITE)) {
			$itemCurrentNumber = $this->dbAdapter->fetchOne("SELECT number FROM `$tableName` WHERE id = ?", $this->recordID);
			if($table['sort_asc']) {
				$number = 0;
				$this->dbAdapter->query("UPDATE `$tableName` SET number = number + 1 WHERE number < $itemCurrentNumber");
			} else {
				$number = $this->dbAdapter->fetchOne("SELECT MAX(number) FROM `$tableName`");
				$this->dbAdapter->query("UPDATE `$tableName` SET number = number - 1 WHERE number > $itemCurrentNumber");
			}
			
			$this->dbAdapter->update($tableName, array('number' => $number), 'id = '.$this->recordID);
		}
		
		header("Location: ".$this->request->getServer("HTTP_REFERER"));
		exit();
	}


}

?>