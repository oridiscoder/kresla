<?php

abstract class OperationAbstract {
	
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	
	/**
	 *
	 * @var Bof_Request
	 */
	protected $request;
	protected $tableID;
	/**
	 * 
	 * @var DbTable
	 */
	protected $dbTable;
	protected $recordID;
	
	const TARGET_SELF = '_self';
	const TARGET_BLANK = '_blank';
	const TARGET_PARENT = '_parent';
	const TARGET_TOP = '_top';
	
	/**
	 *
	 * @param DbTable $dbTable
	 */
	public function __construct($dbTable) {
		$this->dbAdapter = Application::getAppDbAdapter();
		$this->smarty = clone Application::getAppSmarty();
		$this->tableID = $dbTable->getID();
		$this->dbTable = $dbTable;
		$this->smarty->setTemplateDir($this->smarty->getTemplateDir(0).'/operations/'.get_class($this));
		$this->request = Application::getAppRequest();
	}
	
	/**
	 * Функция getName будет использоваться для генерации ключа ассоциативного массива
	 * с целью получить возможность переопределить базовую операцию.
	 * Пример:
	 *   Базовая операция EditOperation::getName() == 'edit'
	 *   Кастомная операция EditUserCart::getName() == 'edit'
	 *   В результате вместо кнопки базовой операции будет отображена кастомная операция.
	 */
	public function getName() {
		return get_class($this);
	}
	/**
	 * Функция getClassName будет использоваться для генерации ссылки на текущую операцию.
	 * т.е. строки вида /operation/OperationAbstract::getClassName()/recordID/
	 */
	public function getClassName() {
		return get_class($this);
	}
	public function setDbAdapter($adapter) {
		$this->dbAdapter = $adapter;
	}
	public function setRecordID($id) {
		$this->recordID = $id;
	}
	public function setSmarty($smarty) {
		$this->smarty = $smarty;
	}
	
	public function getTitle() {
		return $this->title();
	}
	
	public function getTableById($tableID) {
		$table = $this->dbAdapter->fetchRow("SELECT * FROM bof_objects WHERE id = ?", $tableID);
		return $table;
	}
	
	/**
	 * @return Bof_request
	 */
	public function getRequest() {
		return $this->request;
	}
	
	/**
	 * @return string - Читаемое название данной операции (для хлебных крошек например)
	 */
	abstract protected function title();
	abstract public function getImageSource();
	abstract public function getLinkClass();
	public function getOnClickJSCode() {
		return null;
	}
	public function getConfirmText() {
		return null;
	}
	public function getLinkTarget() {
		return self::TARGET_SELF;
	}
	abstract public function getDescription();
	/**
	 *
	 * @param Bof_Request $request
	 */
	abstract public function render($request);
	
}

class OperationExceptuion extends Exception {
	
}

?>