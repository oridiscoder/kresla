<?php

require_once ('RecordOperationAbstract.php');

class EditDefaultSectionPage extends RecordOperationAbstract {
	
	public static $tables = array(112);
	
	public function getName() {
		return "edit";
	}
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Изменить";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/edit.png';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		return 'operation-edit';
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
		return "Расширенная версия добавления или редактирования таблиц по-умолчанию для разделов.";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		
		$error = null;
		
		if($this->request->isPost()) {
			try {
				$this->saveRecord();
				$returnPath = $this->request->getPathInfo();
				$returnPath = substr($returnPath, 0, strpos($returnPath, "operation/"));
				header("Location: $returnPath");
				exit();
			} catch(Exception $ex) {
				$error = $ex->getMessage();
			}
		}
		
		$this->smarty->assign("error", $error);
		
		$target = $this->request->getParam("target") ? $this->request->getParam("target") : 'index';

		$methodName = $target."Action";
		
		if(!method_exists($this, $methodName)) {
			throw new OperationExceptuion("Method $methodName doesn't exists");
		}
		
		$content = call_user_method($methodName, $this);
		return $content;
	}
	
	public function indexAction() {
		$record = $this->getRecord();
		$sections = $this->getSections();
		$currentSectionID = $record['section_id'] ? $record['section_id'] : $sections[0]['id'];
		$pages = $this->getPages($currentSectionID);
		$employees = $this->getEmployees();
		
		$formURL = $this->request->getPathInfo();
		
		$this->smarty->assign(array(
			'record' => $record,
			'sections' => $sections,
			'pages' => $pages,
			'employees' => $employees,
			'formURL' => $formURL
		));
		
		return $this->smarty->fetch("form.html");
	}
	
	private function getRecord() {
		if($this->request->isPost()) {
			$record = array(
					'id' => $this->request->getPost("id"),
					'section_id' => $this->request->getPost("section_id"),
					'page_id' => $this->request->getPost("page_id"),
					'user_id' => $this->request->getPost("user_id")
			);
		} else {
			if($this->recordID) {
				$record = $this->dbAdapter->fetchRow("SELECT * FROM bof_default_section_page WHERE id = ?", $this->recordID);
			} else {
				$record = array(
						'id' => null,
						'section_id' => null,
						'page_id' => null,
						'user_id' => null
				);
			}
		}
		return $record;
	}
	
	private function getSections() {
		return $this->dbAdapter->fetchAll("SELECT * FROM bof_razdels WHERE hidden = 0 ORDER BY number");
	}
	
	private function getPages($sectionID = null) {
		$select = $this->dbAdapter->select()
			->from("bof_pages")
			->where("hidden = 0")
			->order("number");
		if($sectionID) {
			$select->where("razdel = ?", $sectionID);
		}
		
		return $this->dbAdapter->fetchAll($select);
	}
	
	private function getEmployees() {
		$select = $this->dbAdapter->select()
			->from("bof_users")
			->where("hidden = 0")
			->order("login");
		return $this->dbAdapter->fetchAll($select);
	}
	
	public function getPagesAction() {
		$sectionID = $this->request->getParam("section_id");
		
		$pages = $this->getPages($sectionID);
		foreach ($pages as $key=>$page) {
			foreach ($page as $k=>$v) {
				$pages[$key][$k] = iconv('cp1251', 'utf-8', $v);
			}
		}
		
		echo json_encode($pages);
		exit();
	}
	
	public function saveRecord() {
		$id = $this->request->getPost("id");
		$data = array(
			'section_id' => $this->request->getPost("section_id"),
			'page_id' => $this->request->getPost("page_id"),
			"user_id" => $this->request->getPost("user_id")
		);
		
		
		
		if(is_numeric($id)) {
			$select = $this->dbAdapter->select()
				->from("bof_default_section_page")
				->where("user_id = ?", $data['user_id'])
				->where("section_id = ?", $data['section_id'])
				->where("id <> $id");
			$row = $this->dbAdapter->fetchRow($select);
			
			if($row) {
				throw new OperationExceptuion("Default page for section [id:".$data['section_id']."] and for user [id:".$data['user_id']."] has already been set");
			} else {
				$this->dbAdapter->update("bof_default_section_page", $data, "id = $id");
			}
		} else {
			$select = $this->dbAdapter->select()
				->from("bof_default_section_page")
				->where("user_id = ?", $data['user_id'])
				->where("section_id = ?", $data['section_id']);
			$row = $this->dbAdapter->fetchRow($select);
			if($row) {
				throw new OperationExceptuion("Default page for section [id:".$data['section_id']."] and for user [id:".$data['user_id']."] has already been set");
			} else {
				$this->dbAdapter->insert("bof_default_section_page", $data);
			}
		}
		
	}


}

?>