<?php

require_once ('OperationAbstract.php');

abstract class RecordOperationAbstract extends OperationAbstract {
	
	protected $imageSource;
	
	public function renderLink($dataRow) {
		$template = '<a href="{href}" title="{title}" class="{class}" {target} {onclick}><img src="{img_src}" border="0" /></a>';
		
		$href = $this->getHref($dataRow);
		$class = $this->getLinkClass();
		$target = $this->getLinkTarget();
		$imgSrc = $this->imageSource ? $this->imageSource : $this->getImageSource();
		$this->imageSource = null;
		$onClick = $this->getOnClickJSCode();
		
		$buttonHTML = str_replace('{href}', str_replace('//', '/', $href), $template);
		$buttonHTML = str_replace('{title}', $this->getTitle(), $buttonHTML);
		$buttonHTML = str_replace('{class}', $class, $buttonHTML);
		$buttonHTML = str_replace('{target}', $target ? 'target="'.$target.'"' : null, $buttonHTML);
		$buttonHTML = str_replace('{img_src}', $imgSrc, $buttonHTML);
		$buttonHTML = str_replace('{onclick}', $onClick ? 'onclick="'.$onClick.'"' : null, $buttonHTML);
		
		return $buttonHTML;
	}
	
	public function getHref($dataRow) {
		return Application::getAppBaseURL().'/section/'.$this->page->getSectionID().'/'.$this->page->getPageID().'/'.$this->page->getDependencyURL().'/operation/'.$this->getClassName().'/'.$dataRow['id'].'/';
	}
	
	public function setImageSource($src) {
		$this->imageSource = $src;
		return $this;
	}
}

?>