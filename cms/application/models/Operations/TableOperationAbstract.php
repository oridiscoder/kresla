<?php

require_once ('OperationAbstract.php');

abstract class TableOperationAbstract extends OperationAbstract {
	
	public function renderLink() {
		$template = '<a href="{href}" class="{class}" title="{title}" {target}><img src="{img_src}" border="0" /></a>';
		
		$request = $this->page->getRequest();
		$zUri = Zend_Uri_Http::fromString($request->getScheme().'://'.$request->getHttpHost().$request->getRequestUri());
		$path = Application::getAppBaseURL().'/section/'.$this->page->getSectionID().'/'.$this->page->getPageID().'/'.$this->page->getDependencyURL().'/operation/'.$this->getName().'/';
		$zUri->setPath(str_replace("//", '/', $path));
		
		$class = $this->getLinkClass();
		$target = $this->getLinkTarget();
		$imgSrc = $this->getImageSource();
		
		$buttonHTML = str_replace('{href}',$zUri->getUri(), $template);
		$buttonHTML = str_replace('{title}',$this->getTitle(), $buttonHTML);
		$buttonHTML = str_replace('{class}', $class, $buttonHTML);
		$buttonHTML = str_replace('{target}', $target ? 'target="'.$target.'"' : null, $buttonHTML);
		$buttonHTML = str_replace('{img_src}', $imgSrc, $buttonHTML);
		
		return $buttonHTML;
	}
	
	public function showInGroupSelect() {
		return false;
	}
}

?>