<?php

require_once ('RecordOperationAbstract.php');

class EditClusterMetaOperation extends RecordOperationAbstract {
	
	public static $tables = array(1381);
	
	public function getName() {
		return "EditOperation";
	}
	/* (non-PHPdoc)
	 * @see OperationAbstract::title()
	 */
	protected function title() {
		return "Редактировать";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getImageSource()
	 */
	public function getImageSource() {
		return '/cms/images.old/edit.png';		
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getLinkClass()
	 */
	public function getLinkClass() {
		return "EditOperation";
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::getDescription()
	 */
	public function getDescription() {
				
	}

	/* (non-PHPdoc)
	 * @see OperationAbstract::render()
	 */
	public function render($request) {
		
		if($this->recordID) {
			$meta = $this->dbTable->getRecord($this->recordID);
		} else {
			$meta = $this->dbTable->fetchNew();
		}
		
		$this->dbTable->populateFieldsWithData($meta);
		
		if($request->getParam("action_name") == "get_objects") {
			$meta['cluster_id'] = $request->getParam("cluster_id"); 
			$objectsHTML = $this->renderObjectsSelects($meta);
			return $objectsHTML;
		}
		
		if($request->isPost()) {
			$meta = $request->getPost();
			
			/* сгенерируем URL для наглядного поиска */
			$objects = $this->getMetaObjects($meta);
			$urls = array();
			foreach ($objects as $object) {
				foreach ($object['rows'] as $id => $url) {
					if($id == $object['selected']) {
						$urls[] = $url;
						break;
					}
				}
			}
			$meta['url'] = str_replace("//", "/", '/'.implode("/", $urls).'/');
			
			$this->dbTable->save($meta);
			
			$url = substr($request->getRequestUri(), 0, strpos($request->getRequestUri(), "operation/"));
			header("Location: $url");
			exit();
		}
		
		$objectsHTML = $this->renderObjectsSelects($meta);
		
		$this->smarty->assign(array(
			"table" => $this->dbTable, 
			"meta" => $meta, 
			"objectsHTML" => $objectsHTML, 
			"formURL" => $request->getPathInfo()
		));
		
		return $this->smarty->fetch("form.html");
	}
	
	protected function getMetaObjects($meta) {
		$objects = array();
		if($meta['cluster_id']) {
			$cluster = $this->dbAdapter->fetchRow("SELECT * FROM clusters WHERE id = ?", $meta['cluster_id']);
			if($cluster) {
				for($i = 1; $i < 4; $i++) {
					if($cluster['object_'.$i]) {
						$table = new DbTable($this->dbAdapter, $cluster['object_'.$i]);
						foreach ($table->getFields() as $field) {
							/* @var $field DbFieldAbstract */
							if($field->isURLField()) {
								$select = $this->dbAdapter->select()
								->from($table->getName(), array("id", $field->getName()))
								->where("hidden = 0")
								->order($field->getName());
								$rows = $this->dbAdapter->fetchPairs($select);
								$objects[$i] = array(
										"title" => $table->getViewname(),
										"rows" => $rows,
										"selected" => $meta['object_'.$i.'_id']
								);
							}
						}
					}
				}
			}
		}
		return $objects;
	}
	
	protected function renderObjectsSelects($meta) {
		
		$objects = $this->getMetaObjects($meta);
		
		$this->smarty->assign("objects", $objects);
		return $this->smarty->fetch("objects.html");
	}


}

?>