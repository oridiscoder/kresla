<?php
class OperationLoader {
	
	/**
	 *
	 * @param string $name
	 * @param DbTablePage $page
	 * @return OperationAbstract
	 * @throws OperationLoaderException
	 */
	public static function loadOperation($name, $dbTable) {
		$fileName = $name.'.php';
		$baseDir = dirname(__FILE__);
		if(!file_exists($baseDir.'/'.$fileName)) {
			if(!file_exists($baseDir.'/BaseOperations/'.$fileName)) {
				throw new OperationLoaderException("File $fileName not found in $baseDir");
			} else {
				$baseDir = $baseDir.'/BaseOperations';
			}
		}
		require_once $baseDir.'/'.$fileName;
		
		if(!class_exists($name)) {
			throw new OperationLoaderException("Class $name not found");
		}
		
		return new $name($dbTable);
	}
	
	/**
	 *
	 * @param DbTable $dbTable
	 * @throws DbTableException
	 */
	public static function loadOperations($dbTable) {
		return self::load($dbTable, dirname(__FILE__));
	}
	
	/**
	 *
	 * @param DbTable $dbTable
	 * @param string $path
	 */
	private static function load($dbTable, $path) {
		$baseDir = $path;
		$dir = opendir($baseDir);
		$operations = array(
				'record' => array(),
				'table' => array()
		);
		if($dir) {
			while(($fileName = readdir($dir)) != false) {
				if($fileName != '.' && $fileName != '..' && is_file($baseDir.'/'.$fileName)) {
					if(strpos($fileName, "Abstract") === false) {
						$className = str_replace(".php", "", $fileName);
						require_once $baseDir.'/'.$fileName;
						if(class_exists($className) && is_subclass_of($className, "OperationAbstract")) {
								
							$classVars = get_class_vars($className);
							if(self::requiredOperation($dbTable->getID(), $className)) {
								/* @var $operation OperationAbstract */
								$operation = new $className($dbTable);
								if($operation instanceof RecordOperationAbstract) {
									$operations['record'][] = $operation;
								}elseif($operation instanceof TableOperationAbstract) {
									$operations['table'][] = $operation;
								} else {
									throw new DbTableException("$className must be an instance of RecordOperationAbstract or TableOperationAbstract");
								}
							}
								
						}
					}
				}
			}
		}
		
		return $operations;
	}
	
	private static function requiredOperation($tableID, $className) {
		if(!$tableID) {
			return true;
		} else {
			$classVars = get_class_vars($className);
			if(!empty($classVars['tables'])) {
				return in_array($tableID, $classVars['tables']);
			} else {
				return true;
			}
		}
		
	}
	
	/**
	 *
	 * @param DbTable $page
	 */
	public static function loadBaseOperations($dbTable) {
		return self::load($dbTable, dirname(__FILE__).'/BaseOperations');
	}
}

class OperationLoaderException extends Exception {
	
}

?>