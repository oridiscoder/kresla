<?php
require_once ('Page.php');

class SectionPage extends Page {
	
	/**
	 *
	 * @var LeftMenuRenderer
	 */
	private $leftMenuRenderer;

	/**
	 * Иерархия страниц
	 * @var array
	 */
	private $hierarcy;

	public function __construct($sectionID, $dbAdapter, $smarty, $request) {
		
		parent::__construct($dbAdapter, $smarty, $request);
		
		$select = $this->db->select()
			->from('bof_razdels')
			->where('id = ?', $sectionID);
		
		$section = $this->db->fetchRow($select);
		if(!$section) {
			throw new PageNotFoundException("Section {$this->sectionID} not found");
		} elseif ($section['hidden']) {
			throw new PageNotFoundException("Section {$this->sectionID} disabled");
		}
		$this->section = $section;
		$this->sectionPages = $this->getPages();
	}
	
	public function getPages($parentID = 0) {
		$select = $this->db->select()
			->from(array('P'=>'bof_pages'))
			->joinLeft(array("OBJ" => 'bof_objects'), "OBJ.id = P.obj_id", array('table_name' => "name"))
			->where('P.razdel = ?', $this->section['id'])
			->where("P.hidden = 0")
			->order("P.number");
		if($parentID) {
			$select->where("P.parent_id = ?", $parentID);
		} else {
			$select->where("P.parent_id = 0 OR P.parent_id IS NULL");
		}
	
		$pages = $this->db->fetchAll($select);
		
		$user = Application::getAppUser();
		foreach ($pages as $key=>$page) {
			if(!$user->hasPageAccess($page['id'], User::ACCESS_READ)) {
				unset($pages[$key]);
			}
		}
	
		if(!empty($pages)) foreach($pages as $key=>$page) {
			$childs = $this->getPages($page['id']);
			if(!empty($childs)) {
				$pages[$key]['_childs'] = $childs;
			}
		}
		return $pages;
	}
	
	protected function assignCurrentPageID($id) {
		$this->smarty->assign("currentPageID", $id);
	}
	
	public function getSectionName() {
		return $this->section['name'];
	}
	public function getSectionID() {
		return $this->section['id'];
	}
	
	/**
	 * @return LeftMenuRenderer
	 */
	public function getLeftMenuRenderer() {
		if(!$this->leftMenuRenderer) {
			require_once APPLICATION_PATH . '/models/Menu/LeftMenuRenderer.php';
			$this->leftMenuRenderer = new LeftMenuRenderer($this->smarty);
		}
		return $this->leftMenuRenderer;
	}
	
	/**
	 * @param LeftMenuRenderer $leftMenuRenderer
	 */
	public function setLeftMenuRenderer($leftMenuRenderer) {
		$this->leftMenuRenderer = $leftMenuRenderer;
	}
	
	public function render() {
		// nothing here yet
	}
	
	public function getPageTitle() {
		return $this->section['name'];
	}
	public function getPageID() {
		return $this->page['id'];
	}
	public function getDefaultPageID($userID) {
		$select = $this->db->select()
			->from("bof_default_section_page")
			->where("section_id = ?", $this->section['id'])
			->where("user_id = ?", $userID);
		$record = $this->db->fetchRow($select);
		
		return $record ? $record['page_id'] : null;
	}
	
	public function getBreadcrumbs() {
		$breadcrumbs = array(
				array(
						'href' => '/',
						'title' => 'Главная'
				),
				array(
						'href' => '/section/'.$this->section['id'].'/',
						'title' => $this->getSectionName()
				)
		);
		return $breadcrumbs;
	}
	
	/* (non-PHPdoc)
	 * @see Page::renderMenu()
	 */
	public function renderMenu() {
		$renderer = $this->getLeftMenuRenderer();
		$renderer->setPageID($this->getPageID());
		$renderer->setItems($this->sectionPages);
		$renderer->setBaseURL(str_replace("//", "/", $this->baseURL.'/'.$this->section['id']));
		return $renderer->render();
	}
}