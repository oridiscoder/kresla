<?php
require_once 'SectionPage.php';
require_once "DbTablePage/DependencyExtractor.php";
require_once "DbTablePage/DependencyList.php";
require_once "DbTablePage/DbTablePageRequestRouter.php";
require_once APPLICATION_PATH . '/models/Table/DbTable.php';

/**
 * DbTablePage по-сути, является Контроллером действия, который работает с таблицами БОФа
 * При вызове метода render() он анализирует, какой запрос был отправлен клиентом
 * и выбирает соответствующий метод-обработчик вида renderActionname()
 *
 */
class DbTablePage extends SectionPage {
	
	/**
	 *
	 * @var DbTable
	 */
	protected $dbTable;
	/**
	 * При следовании по иерархии зависимых таблиц, в URL сохраняются
	 * соответствующие метки зависимых таблиц вида /1,23,45/
	 * @var DependencyList - текущая цепочка зависимых таблиц
	 */
	protected $dependencies = null;
	protected $dependenciesURL = null;
	protected $actionName;
	protected $breadcrumbs = array();
	protected $hierarcy = null;
	/**
	 *
	 * @var User
	 */
	protected $user;
	
	const ROWS_ON_PAGE = 15;
	
	public function __construct($pageID, $dbAdapter, $smarty, $request) {
		
		$page = self::getPage($pageID, $dbAdapter);
		
		parent::__construct($page['razdel'], $dbAdapter, $smarty, $request);
		
		$this->page = $page;
		
		$this->sectionPages = $this->getPages();
		
		$this->hierarcy = $this->getPageHierarcy();
		
		$dependencyExtractor = new DependencyExtractor($this->db, $this->page['obj_id']);
		$this->dependencies = $dependencyExtractor->extract($this->request->getPathInfo());
		
		if($this->dependencies) {
			$this->dependenciesURL = $this->dependencies->generateDependencyURL();
			$tableID = $this->dependencies->getLastTableID();
		} else {
			$tableID = $this->page['obj_id'];
		}
				
		$this->dbTable = new DbTable($this->db, $tableID);
		
		$this->user = Application::getAppUser();
		
		
		$request = DbTablePageRequestRouter::route();
		$this->actionName = $request->getActionName();
		$request->setParam("section", $this->request->getParam("section"));
		$request->setParam("page", $this->request->getParam("page"));
		$request->setParam("dependencies", $this->dependencies);
		$request->setParam("dependencies_url", $this->dependenciesURL);
		$this->request = $request;
		
		$this->smarty->assign("pageNumber", $this->request->getParam("pageNumber"));
		$this->smarty->assign("onPage", $this->request->getParam("onPage"));
		$this->smarty->assign("isRecycle", false);
	}
	
	/**
	 * Получить иерархию для текущей страницы
	 */
	public function getPageHierarcy() {
		if($this->hierarcy === null) {
			$page = $this->page;
			$hierarcy = array();
			$hierarcy[] = $page;
			while($page['parent_id']) {
				$page = self::getPage($page['parent_id'], $this->db);
				$hierarcy[] = $page;
			}
			return array_reverse($hierarcy);
			$this->hierarcy = $hierarcy;
		}
		
		return $this->hierarcy;
	}
	
	public function initBaseURL() {
		/* определимся сразу с URL до текущей страницы */
		$basePath = $this->baseURL . '/' . $this->section['id'].'/'.$this->page['id'];
		if($this->dependenciesURL) {
			$basePath .= '/'.$this->dependenciesURL;
		}
		$this->request->setBasePath(str_replace('//', '/', $basePath));
	}
	
	
	public function render() {
		
		$this->checkPageAccess();
		
		$this->initBaseURL();
		
		if(!$this->actionName) {
			throw new ApplicationException("Action $this->actionName not found");
		}
		
		$methodName = 'render'.ucfirst($this->actionName);
		
		if(method_exists($this, $methodName)) {
			return call_user_func(array($this, $methodName));
		} else {
			throw new ApplicationException("Action $this->actionName not found");
		}
	}
	
	public function renderIndex() {
		
		if(!$this->userCanRead()) {
			throw new AccessException("Access denied");
		}
		
		//к некоторым полям у некоторых пользователей нет доступа
		$this->hideRestrictedFields();
		
		//декораторы полей
		$this->initFieldDecorators();

		require_once APPLICATION_PATH . '/models/Table/DbTableRenderer.php';
		$renderer = new DbTableRenderer($this->smarty, array(
			'dbTable' => $this->dbTable, 
			'request' => $this->request, 
			'base_path' => $this->request->getBasePath(),
			'dependencies' => $this->dependencies
		));
		
		return $renderer->render();
	}
	
	public function hideRestrictedFields() {
		$fields = $this->dbTable->getFields();
		foreach($fields as $field) {
			/* @var $field DbFieldAbstract */
			if(!$this->user->hasFieldAccess($field->getID())) {
				$this->dbTable->removeField($field->getName());
			}
		}
	}
	
	public function initFieldDecorators() {
		$fieldDecoratorConfig = new Zend_Config_Ini(APPLICATION_PATH . "/configs/field-decorators.ini");
		foreach ($fieldDecoratorConfig as $tableName => $fields) {
			if($tableName == $this->dbTable->getName()) {
				foreach ($fields as $fieldName => $decoratorName) {
					$decoratorFilePath = APPLICATION_PATH . '/models/Table/FieldDecorators/'.$decoratorName.'.php';
			
			
					if(!file_exists($decoratorFilePath)) {
						throw new PageException("Decorator file $decoratorName.php not found!");
					}
			
					require_once $decoratorFilePath;
			
					if(!class_exists($decoratorName)) {
						throw new PageException("Decorator class $decoratorName not found");
					}
			
					$decorator = new $decoratorName($this->dbTable->getFields($fieldName), $this->db);
					$this->dbTable->setField($fieldName, $decorator);
				}
			}
		}
	}
	
	public function renderRecycle() {
		$this->dbTable->showHiddenOnly();
		$this->request->setParam("isRecycle", true);
		return $this->renderIndex();
	}
	
	public function renderToRecycle() {
		if(!$this->userCanDelete()) {
			throw new AccessException("Access denied");
		}
		if($this->request->isPost()) {
			$postData = $this->request->getPost();
			$items = $postData['item'];
			if(!empty($items)) {
				$this->dbTable->moveToRecycleBin($items);
			}
		} elseif(is_numeric($this->request->id)) {
			$this->dbTable->moveToRecycleBin($this->request->id);
			$location = $_SERVER['HTTP_REFERER'];
			header("Location: $location");
			exit();
		}
	}
	
	public function renderFromRecycle() {
		if($this->request->isPost()) {
			$postData = $this->request->getPost();
			$items = $postData['item'];
			if(!empty($items)) {
				$this->dbTable->restoreFromRecycleBin($items);
			}
		} elseif(is_numeric($this->request->id)) {
			$this->dbTable->restoreFromRecycleBin($this->request->id);
			$location = $_SERVER['HTTP_REFERER'];
			header("Location: $location");
		}
	}
	
	public function renderDelete() {
		if(!$this->userCanDelete()) {
			throw new AccessException("Access denied");
		}
		if($this->request->isPost()) {
			$postData = $this->request->getPost();
			if(isset($postData['delete']) && $postData['id']) {
				$items = array($postData['id']);
			} else {
				$items = $postData['item'];
			}
			if(!empty($items)) {
				$this->dbTable->deleteItems($items);
			}
		}
	}
	
	public function renderFieldControl() {
		
		$fieldName = $this->request->getParam("field_name");
		$items = $this->request->get("item");
		
		$dbField = $this->dbTable->getFields($fieldName);
		$this->smarty->assign("field", $dbField);
		$this->smarty->assign("items", $items);
		$this->smarty->assign("sectionID", $this->getSectionID());
		$this->smarty->assign("pageID", $this->getPageID());
		return  $this->smarty->fetch("record/edit-single-field.html");
	}
	
	public function renderGroupEdit() {
	
		if($this->request->isPost()) {
			$postData = $this->request->getPost();
			$items = $postData["_item"];
			$fieldName = $postData['_field_name'];
			$field = $this->dbTable->getFields($fieldName);
			if(isset($postData[$field->getName()])) {
				$field->setValue($postData[$field->getName()]);
			}
				
			if(!empty($items) && $field) {
				$this->dbTable->updateFieldValueForRows($field, $items);
			}
				
			$location = $_SERVER['HTTP_REFERER'];
			header("Location: $location");
			exit();
		}
	}
	
	public function renderSort() {
		if($this->request->isPost()) {
			$items = $this->request->getPost('item');
			$itemsCount = count($items);
			$pageNumber = $this->request->getParam('pageNumber');
			$onPage = $this->request->getParam('onPage');
			$totalInTable = $this->dbTable->getRowsCount();
			$pages = ceil($totalInTable / $onPage);
			
			$itemsAbsoluteSorting = array();
			
			if($this->dbTable->ascIsDefaultSortType()) {
				for ($i = 0; $i < $itemsCount; $i++) {
					$itemsAbsoluteSorting[$pageNumber * $onPage + $i] = $items[$i];
				}
			} else {
				for ($i = 0; $i < $itemsCount; $i++) {
					$number = $itemsCount - $i - 1;
					$absoluteNumber = ($pages - $pageNumber - 1) * $onPage  + $number;
					$itemsAbsoluteSorting[$absoluteNumber] = $items[$i];
					echo $i ."=>". $number.'=>'.$absoluteNumber."\n";
				}
			}
			
			$this->dbTable->setSortingOrder($itemsAbsoluteSorting);
		}
	}
	
	public function renderOperation() {
		require_once APPLICATION_PATH . '/models/Operations/OperationLoader.php';
		$operation = OperationLoader::loadOperation($this->request->getParam("operationName"), $this->dbTable);
		$operation->setRecordID($this->request->getParam("recordID"));
		
		$this->breadcrumbs[] = array(
			'href' => $this->request->getRequestUri(),
			'title' => $operation->getTitle()
		);
		
		if($this->request->isPost()) {
			$this->breadcrumbs[] = array(
					'href' => $this->request->getRequestUri(),
					'title' => "Результат"
			);
		}
		return $operation->render($this->request);
	}
	
	public function renderLoadParent() {
		$recordID = $this->request->getParam("record_id");
		$fieldID = $this->request->getParam("field_id");
		
		$error = null;
		
		$data = array(
			'record_id' => $recordID,
			'field_id' => $fieldID
		);
		
		if(is_numeric($recordID) && is_numeric($fieldID)) {
			
			$field = $this->getFieldById($fieldID);
			
			if($field) {
			
				/*
				 * Когда виртуальный потомок указывает на поле "потомок", 
				 * оно тоже может подгружать информацию о родителе, но для этого
				 * надо сделать пару махинаций:
				 */
				if($field instanceof VirtualDescendantField) {
					/*@var $field VirtualDescendantField */
					//создаем объект текущей таблицы (в которой задан Виртуальный потомок)
					$currentTable = new DbTable($this->db, $field->getTableID());
					//получаем текущую запись из этой таблицы
					$currentRecord = $currentTable->getRecord($recordID);
					//получаем связующее поле (простой массив) - поле, по которому осуществляется связь с таблицей-родителем
					$linkedField = $field->getLinkedField();
					
					//находим id записи в Таблице-родителе, по которой будем работать 
					$recordID = $currentRecord[$linkedField['name']];

					//изменяем текущее поле на поле в таблице-родителе
					$field = $field->getParentTableField();
					
				}
				
				if($field instanceof DescendantField) {
				
					$currentTableID = $field->getTableID();
					$parentTableID = $field->getParentTableId();
					
					if($this->user->hasTableAccess($parentTableID, User::ACCESS_READ)) {
						try {
							$currentTable = new DbTable($this->db, $currentTableID);
							$parentTable = new DbTable($this->db, $parentTableID);
								
							$currentRow = $currentTable->getRecord($recordID);
							if($currentRow) {
								$parentTableRecordID = $currentRow[$field->getName()];
								
								$parentTableRecord = is_numeric($parentTableRecordID) ? $parentTable->getRecord($parentTableRecordID) : null;
								
								if($parentTableRecord) {
									$data['id'] = $parentTableRecordID;
									foreach ($parentTable->getFields() as $f) {
										/* @var DbFieldAbstract $f */
										if($this->user->hasFieldAccess($f->getID())) {
											$data['data'][] = array(
												'name' => $f->getName(),
												'viewname' => $f->getViewname(),
												'html' => $f->renderCell($parentTableRecord)
											);
										}
									}
								} else {
									$error = "Parent table record doesn't exists";
								}
							} else {
								$error = "Current table record doesn't exists";
							}
						} catch (DbTableException $ex) {
							$error = $ex->getMessage();
						}
					} else {
						$error = "У Вас недостаточно прав для просмотра выбранной записи.";
					}
				} else {
					$error = "Field doesn't exists or is not descendant";
				}
			} else {
				$error = "Field doesn't exists or is not descendant";
			}
		} else {
			$error = "Wrong input data";
		}
		$data['error'] = $error ? $error : null;
		
		if(Application::getAppCharset() == 'windows-1251') {
			if(isset($data['data'])) {
				foreach ($data['data'] as $key => $value) {
					if(is_array($value)) {
						foreach($value as $k => $v) {
							$data['data'][$key][$k] = iconv('cp1251', 'utf-8', $v);
						}
					} else {
						$data[$key] = iconv('cp1251', 'utf-8', $value);
					}
				}
			}
			$data['error'] = iconv('cp1251', 'utf-8', $data['error']);
		}
		
		header("Content-type: text/html; charset=utf-8");
		echo json_encode($data);
		exit();
	}
	
	/**
	 *
	 * @param integer $id
	 * @return DescendantField
	 */
	private function getFieldById($id) {
		$select = $this->db->select()
			->from("bof_fields")
			->where("id = ?", $id);
		$field =  $this->db->fetchRow($select);
		if($field) {
			$field['_db_adapter'] = $this->db;
			return DbFieldAbstract::factory($field);
		} else {
			return null;
		}
	}
	
	public function getBreadcrumbs() {
		require_once "DbTablePage/Breadcrumbs.php";
		$breadcrumbsMaker = new Breadcrumbs($this);
		$breadcrumbs = $breadcrumbsMaker->getBreadcrumbs($this->dependencies);
		
		if($this->actionName == 'edit') {
			$breadcrumbs[] = array(
				'href' => $this->request->getPathInfo(),
				'title' => $this->request->id ? 'Редактировать' : 'Добавить'
			);
		}
		if($this->request->getParam("is_recycle")) {
			$breadcrumbs[] = array(
				'href' => '/recycle/section/'.$this->section['id'].'/'.$this->page['id'].'/',
				'title' => 'Корзина'
			);
		}
		$breadcrumbs = array_merge($breadcrumbs, $this->breadcrumbs);
		return $breadcrumbs;
	}
	
	public function getBreadcrumbsRenderer() {
		require_once APPLICATION_PATH . '/models/Breadcrumbs/DbTableBreadcrumbsRenderer.php';
		return new DbTableBreadcrumbsRenderer($this);
	}
	
	public function getPageTitle() {
		return $this->section['name'].' | '.$this->page['viewname'];
	}
	public function getName() {
		return $this->page['viewname'];
	}
	
	public function getObjectID() {
		return isset($this->page['obj_id']) ? $this->page['obj_id'] : null;
	}
	
	public function getActionName() {
		return $this->actionName;
	}
	public function getDependencyURL() {
		return $this->dependenciesURL;
	}
	public function getDependencies() {
		return $this->dependencies;
	}
	/**
	 * @return DbTable
	 */
	public function getDbTable() {
		return $this->dbTable;
	}
	/**
	 * @return Bof_Request
	 */
	public function getRequest() {
		return $this->request;
	}
	
	protected function userCanRead() {
		return $this->user->hasTableAccess($this->dbTable->getID(), User::ACCESS_READ);
	}
	protected function userCanWrite() {
		return $this->user->hasTableAccess($this->dbTable->getID(), User::ACCESS_WRITE);
	}
	protected function userCanDelete() {
		return $this->user->hasTableAccess($this->dbTable->getID(), User::ACCESS_DELETE);
	}
	
}

class PageException extends Exception {
	
}