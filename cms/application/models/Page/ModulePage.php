<?php
require_once ('SectionPage.php');
//require_once ('DbTablePage.php');

class ModulePage extends SectionPage {
	/**
	 *
	 * @var ModuleAbstract
	 */
	protected $module;
	
	protected $hierarcy;
	
	public function __construct($pageID, $dbAdapter, $smarty, $request) {
	
		$page = self::getPage($pageID, $dbAdapter);
	
		parent::__construct($page['razdel'], $dbAdapter, $smarty, $request);
	
		$this->page = $page;
	
		$this->sectionPages = $this->getPages();
		
		$this->module = $this->loadModule($this->page['name']);
	}
	
	/**
	 * Получить иерархию для текущей страницы
	 */
	public function getPageHierarcy() {
		if($this->hierarcy === null) {
			$page = $this->page;
			$hierarcy = array();
			$hierarcy[] = $page;
			while($page['parent_id']) {
				$page = self::getPage($page['parent_id'], $this->db);
				$hierarcy[] = $page;
			}
			return array_reverse($hierarcy);
			$this->hierarcy = $hierarcy;
		}
	
		return $this->hierarcy;
	}
	
	private function loadModule($moduleName) {
		$baseDir = APPLICATION_PATH . '/models/Modules';
		if(!file_exists($baseDir.'/'.$moduleName.'.php')) {
			if(!file_exists(APPLICATION_PATH. '/bof.old/modules/'.$moduleName.'.php')) {
				throw new ModuleNotFoundException("File ".$baseDir.'/'.$moduleName.'.php not found!');
			} else {
				$this->request->setParam("old_module_name", $moduleName);
				$moduleName = 'OldModulesAdapter';
			}
		}
		require_once $baseDir.'/'.$moduleName.'.php';
		
		if(!class_exists($moduleName)) {
			throw new ModuleNotFoundException("Class $moduleName not found in $baseDir/$moduleName.php");
		}
		
		$module = new $moduleName($this->request, $this->db, $this->smarty);
		
		return $module;
	}
	
	public function render() {
		$this->checkPageAccess();
		$this->appendCSSFiles($this->module->getCSSFiles());
		$this->appendJSFiles($this->module->getJSFiles());
		return $this->module->render();
	}
	
	public function getBreadcrumbs() {
		$breadcrumbs = parent::getBreadcrumbs();
		$hierarcy = $this->getPageHierarcy();
		
		if($hierarcy) {
			foreach ($hierarcy as $page) {
				$breadcrumbs[] = array(
					'href' => '/section/'.$this->section['id'].'/'.$page['id'].'/',
					'title' => $page['viewname']
				);
			}
		}
		
		$moduleBreadcrumbs = $this->module->getBreadcrumbs();
				
		if(!empty($moduleBreadcrumbs)) {
			$breadcrumbs = array_merge($breadcrumbs, $moduleBreadcrumbs);
		}/*  else {
			$breadcrumbs[] = array(
				'href' => '/section/'.$this->section['id'].'/'.$this->page['id'].'/',
				'title' => $this->module->getTitle()
			);
		} */
		return $breadcrumbs;
	}
}

class ModuleNotFoundException extends Exception {
	
}

?>