<?php
class DependencyListItem {
	/**
	 *
	 * @var DbTable
	 */
	protected $table;
	protected $field;
	protected $row;
	
	/**
	 *
	 * @var DependencyListItem
	 */
	protected $parent = null;
	
	public function __construct($table, $row, $field = null, $parent = null) {
		$this->table = $table;
		$this->row = $row;
		$this->field = $field;
		$this->parent = $parent;
	}
	
	public function getTableID() {
		return $this->table->getID();
	}
	public function getRowID() {
		return $this->row['id'];
	}
	public function getFieldID() {
		return $this->field['id'];
	}
	public function getFieldName() {
		return $this->field['name'];
	}
	public function getFieldViewname() {
		return $this->field['viewname'];
	}
	public function getField() {
		return $this->field;
	}
	public function getTableViewname() {
		return $this->table->getViewname();
	}
	public function getTable() {
		return $this->table;
	}
	
	public function renderTableFirstField() {
		$field = $this->table->getFirstField();
		return $field->renderCell($this->row);
	}
	
	public function getParentTableID() {
		if($this->parent) {
			return $this->parent->getTableID();
		} else {
			return null;
		}
	}
	public function getParentRowID() {
		if($this->parent) {
			return $this->parent->getRowID();
		} else {
			return null;
		}
	}
	
}

?>