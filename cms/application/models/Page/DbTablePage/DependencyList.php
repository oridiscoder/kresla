<?php
require_once "DependencyListItem.php";
require_once APPLICATION_PATH . '/models/Table/Field/DbFieldAbstract.php';

class DependencyList {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	protected $list;
	
	protected $position;
	
	public function __construct($db) {
		$this->db = $db;
		$this->list = array();
		$this->position = 0;
	}
	
	public function push($tableID, $rowID, $fieldID = null) {
		if(empty($this->list)) {
			$currentTable = $this->getTable($tableID);
			$currentRow = $currentTable->getRecord($rowID);//$this->getTableRow($currentTable['name'], $rowID);
			$this->list[] = new DependencyListItem($currentTable, $currentRow);
		} else {
			if(!$fieldID) {
				throw new DependencyListException("Field ID must be setted for dependency tables! [$tableID, $rowID, $fieldID]");
			}
			
			$currentTable = $this->getTable($tableID);
			if($rowID) {
				$currentRow = $currentTable->getRecord($rowID);//$currentRow = $this->getTableRow($currentTable['name'], $rowID);
			} else {
				$currentRow = null;
			}
			$relationalField = $this->getField($fieldID);
			
			$prevItem = end($this->list);
			$parentTable = $prevItem->getTable();
			
			if($this->isCorrectDependentTable($currentTable, $parentTable, $relationalField)) {
				$this->list[] = new DependencyListItem($currentTable, $currentRow, $relationalField, $prevItem);
			} else {
				throw new DependencyListException("Table {$currentTable->getName()} is not depends from {$parentTable->getName()} by field {$relationalField['name']}");
			}
		}
	}
	
	/**
	 *
	 * @param integer $id
	 * @return DbTable
	 */
	protected function getTable($id) {
		/* $table =  $this->db->fetchRow("SELECT * FROM bof_objects WHERE id = ?", $id);
		if(!$table) {
			throw new DependencyListException("Can't find table with id = $id");
		} */
		//$table['_firstField'] = $this->getTableFirstField($id);
		$table = new DbTable($this->db, $id);
		
		return $table;
	}
	
	protected function getTableRow($tableName, $id) {
		$row =  $this->db->fetchRow("SELECT * FROM $tableName WHERE id = ?", $id);
		/* if(!$row) {
			throw new DependencyListException("Can't find table row with id = $id");
		} */
		return $row;
	}
	
	protected function getField($id) {
		$field =  $this->db->fetchRow("SELECT * FROM bof_fields WHERE id = ".$id);
		if(!$field) {
			throw new DependencyListException("Can't find field with id = $id");
		}
		return $field;
	}
	
	protected function getTableFirstFieldValue($tableID) {
		$field = $this->db->fetchRow("SELECT * FROM bof_fields WHERE obj_id = ? AND hidden = 0 ORDER BY number LIMIT 1", $tableID);
		
	}
	
	/**
	 *
	 * @param DbTable $current
	 * @param DbTable $parent
	 * @param array $field
	 */
	protected function isCorrectDependentTable($current, $parent, $field) {
		return ($current->getID() == $field['obj_id'] && $parent->getID() == $field['param']);
	}
	
	public function getLast() {
		return end($this->list);
	}
	
	public function getLastTableID() {
		return $this->list[count($this->list) - 1]->getTableID();
	}
	
	public function generateDependencyURL() {
		$depsCount = count($this->list);
		$url = '';
		for($i = 1; $i < $depsCount; $i++) {
			$current = $this->list[$i];
			$parent = $this->list[$i-1];
			$url .= $parent->getRowID().','.$current->getTableID().','.$current->getFieldID().'/';
		}
		return $url;
	}
	
	public function length() {
		return count($this->list);
	}
	
	/**
	 *
	 * @param DependencyListItem $key
	 */
	public function get($key) {
		return isset($this->list[$key]) ? $this->list[$key] : null;
	}
	
	public function getColumnValue($key) {
		$item = $this->list[$key];
		return $item->getDependentColumnValue();
	}
}

class DependencyListException extends Exception {
}

?>