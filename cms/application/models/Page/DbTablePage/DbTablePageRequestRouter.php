<?php
class DbTablePageRequestRouter {
	public static function route() {
		$request = new Bof_Request();
		
		$router = new Zend_Controller_Router_Rewrite();
		$router->removeDefaultRoutes();
		
		$routes = array();
		
		$routes['index'] = new Zend_Controller_Router_Route_Regex('.*section/\d+/\d+(/[,\d]+)*',
			array('action' => 'index'),
			array(1 => 'id')
		);
		
		$routes['edit'] = new Zend_Controller_Router_Route_Regex('.*/edit(?:/)?(\d+)?',
			array('action' => 'edit'),
			array(1 => 'id')
		);
		
		$routes['recycle'] = new Zend_Controller_Router_Route_Regex('.*/recycle',
			array('action' => 'recycle')
		);
		
		$routes['to_recycle_bin'] = new Zend_Controller_Router_Route_Regex('.*/to_recycle_bin(?:/)?(\d+)?',
			array('action' => 'toRecycle'),
			array(1 => 'id')
		);
		
		$routes['from_recycle_bin'] = new Zend_Controller_Router_Route_Regex('.*/restore_from_recycle_bin(?:/)?(\d+)?',
			array('action' => 'fromRecycle'),
			array(1 => 'id')
		);
		
		$routes['delete'] = new Zend_Controller_Router_Route_Regex('.*/delete(?:/)?(\d+)?',
			array('action' => 'delete'),
			array(1 => 'id')
		);
		
		$routes['get_field_control'] = new Zend_Controller_Router_Route_Regex('.*/get_field_control',
			array('action' => 'fieldControl')
		);
		
		$routes['group_edit'] = new Zend_Controller_Router_Route_Regex('.*/group_edit',
			array('action' => 'groupEdit')
		);
		
		$routes['sort'] = new Zend_Controller_Router_Route_Regex('.*/sort',
			array('action' => 'sort')
		);
		
		$routes['operations'] = new Zend_Controller_Router_Route_Regex('.*/operation/([^/]+)/?(\d+)?',
				array('action' => 'operation'),
				array(1 => 'operationName', 2=> 'recordID')
		);
		
		$routes['load_parent'] = new Zend_Controller_Router_Route_Regex('.*/load_parent.*',
				array('action' => 'loadParent')
		);
		
		foreach ($routes as $routeName => $route) {
			$router->addRoute($routeName, $route);
		}
		
		$router->route($request);
		
		return $request;
	}
}

?>