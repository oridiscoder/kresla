<?php

require_once "DependencyList.php";

class DependencyExtractor {
	/**
	 * Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	protected $objectID;
	
	public function __construct($db, $objectID) {
		$this->db = $db;
		$this->objectID = $objectID;
	}
	
	public function extract($pathName) {
		$matches = array();
		$dependencies = null;
		if(self::isURLWithDependency($pathName)) {
			preg_match_all('#(\d+),(\d+),(\d+)#', $pathName, $matches);
				
			$dependencies = array();
			$dependencies[] = array(
					'tableID' => $this->objectID,
					'rowID' =>  $matches[1][0]
			);
			foreach ($matches[0] as $key=>$match) {
				$dep = array(
						'tableID' => $matches[2][$key],
						'fieldID' => $matches[3][$key]
				);
				if(isset($matches[1][$key+1])) {
					$dep['rowID'] = $matches[1][$key+1];
				}
				$dependencies[] = $dep;
			}
		}
		$list =  $this->convertToDependencyList($dependencies);
		if($list->length()) {
			return $list;
		} else {
			return null;
		}
	}
	
	protected function convertToDependencyList($deps) {
		$depsCount = count($deps);
		$depsList = new DependencyList($this->db);
		for($i = 0; $i < $depsCount; $i++) {
			$depsList->push(
				$deps[$i]['tableID'],
				isset($deps[$i]['rowID']) ? $deps[$i]['rowID'] : null,
				isset($deps[$i]['fieldID']) ? $deps[$i]['fieldID'] : null
			);
		}
		return $depsList;
	}
	
	public static function isURLWithDependency($url) {
		return preg_match('#/\d+,\d+,\d+?/#', $url);
	}
}

?>