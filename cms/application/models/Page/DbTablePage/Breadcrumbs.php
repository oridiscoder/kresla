<?php

require_once APPLICATION_PATH . '/models/Table/Field/DbFieldAbstract.php';

class Breadcrumbs {
	/**
	 *
	 * @var DbTablePage
	 */
	protected $dbPage;
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	public function __construct($dbPage) {
		$this->dbPage = $dbPage;
		$this->db = $this->dbPage->getDbAdapter();
	}
	
	public function getBreadcrumbs($dependencies) {
		if($dependencies) {
			$breadcrumbs = $this->getWithDependency($dependencies);
		} else {
			$breadcrumbs = $this->getSimple();
		}
		return $breadcrumbs;
	}
	
	public function getSimple() {
		$breadcrumbs = array(
				array(
						'href' => '/',
						'title' => 'Главная'
				),
				array(
						'href' => '/section/'.$this->dbPage->getSectionID().'/',
						'title' => $this->dbPage->getSectionName()
				)
		);
				
		if($hierarcy = $this->dbPage->getPageHierarcy()) {
			foreach ($hierarcy as $page) {
				$breadcrumbs[] = array(
					'href' => '/section/'.$this->dbPage->getSectionID().'/'.$page['id'].'/',
					'title' => $page['viewname']
				);
			}
		}
		
		return $breadcrumbs;
	}
	
	/**
	 *
	 * @param DependencyList $dependencies
	 */
	public function getWithDependency($dependencies) {
		
		$breadcrumbs = $this->getSimple();
		
		$lastBreadcrumbs = array_pop($breadcrumbs);
		
		$href = $lastBreadcrumbs['href'];
		$title = $lastBreadcrumbs['title'];
			
		$depsCount = $dependencies->length();
		
		for($i = 0; $i < $depsCount; $i++) {
						
			/* @var $prev DependencyListItem */
			$prev = $dependencies->get($i-1);
			/* @var $current DependencyListItem */
			$current = $dependencies->get($i);
			/* @var $next DependencyListItem */
			$next = $dependencies->get($i + 1);
			$item = array(
					'href' => $href,
					'title' => $i == 0 ? $title : $current->getTableViewname()
			);
			
			if($prev) {
				$breadcrumbs[] = array(
					'href' => $breadcrumbs[count($breadcrumbs) - 1]['href'],
					'title' => $prev->renderTableFirstField(),
					'dependentTables' => $this->getDependentTables($prev->getTableID()),
					'rowID' => $prev->getRowID()
				);
				$item['fieldViewname'] = $current->getFieldViewname();
			}
			
			$breadcrumbs[] = $item;
			
			
			if($next) {
				$href .= $current->getRowID().','.$next->getTableID().','.$next->getFieldID().'/';
			}
		}
		
		return $breadcrumbs;
	}
	
	protected function getDependentTables($tableID) {
		$select = $this->db->select()
			->from(array('F' => "bof_fields"), array('fid' => 'id', 'fname' => 'viewname'))
			->join(array('O' => "bof_objects"), 'F.obj_id = O.id')
			->where("F.type = ".DbFieldAbstract::TYPE_DESCENDANT)
			->where("F.param = ? ", $tableID)
			->where("F.hidden = 0");
		$tables = $this->db->fetchAll($select);
			
		$processedTables = array();
		if(!empty($tables)) foreach($tables as $table) {
			$fields = array();
			foreach ($tables as $tbl) {
				if($tbl['id'] == $table['id']) {
					$fields[] = array(
							'id' => $tbl['fid'],
							'name' => $tbl['fname']
					);
				}
			}
			if(count($fields) > 1) {
				$table['_fields'] = $fields;
			}
	
			if(!isset($processedTables[$table['id']])) {
				$processedTables[$table['id']] = $table;
			}
		}
		return $processedTables;
	}
	
}

?>