<?php
class PageCreator {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	public function __construct($db) {
		$this->db = $db;
	}
	
	const SECTION_SYSTEM = 1;
	const SECTION_SITE = 2;
	
	/**
	 * 
	 * @param string $viewname
	 * @param integer $sectionID
	 * @param integer $tableID
	 * @param string $moduleName
	 * @param integer $parentID
	 * @return integer page id
	 */
	public function addPage($viewname, $sectionID, $tableID = null, $moduleName = null, $parentID = 0) {
		
		$bind = array(
			'viewname' => $viewname, 
			'type' => $moduleName ? 3 : 2, 
			'razdel' => $sectionID, 
			'obj_id' => $tableID, 
			'number' => $this->db->fetchOne("SELECT MAX(number) + 1 FROM bof_pages"), 
			'parent_id' => $parentID
		);
		
		$this->db->insert("bof_pages", $bind);
		
		return $this->db->lastInsertId();
	}
	
	public function findPage($whereCond) {
		$select = $this->db->select()
			->from("bof_pages")
			->where("hidden = 0");
		if(is_string($whereCond)) {
			$whereCond = array($whereCond);
		}			
		foreach($whereCond as $where) {
			$select->where($where);
		}
		
		return $this->db->fetchRow($select);
	}
	
	public function getPageById($id) {
		$select = $this->db->select()
			->from("bof_pages")
			->where("id = ?", $id);
		return $this->db->fetchRow($select);
	}
	
	public function deletePage($id) {
		$this->db->delete("bof_pages", array("id = ?" => $id));
	}
}

?>