<?php

/**
 * Класс для работы с cms_pages
 *
 */
class CMSPage {
	
	/**
	 * 
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	/**
	 * 
	 * @var integer
	 */
	private $lastPageID;
	private $pagesStack;
	
	public function __construct($db) {
		$this->db = $db;
	}
	
	public function findPage($pagename, $parentID = null) {

		$select = $this->db->select()
			->from("cms_pages")
			->where("name = ?", $pagename)
			->where("hidden = 0");
		
		if($parentID) {
			$select->where("parent_id = ?", $parentID);
			
			/* найдем среди дочерних страниц страницы-плейсхолдеры: 
			 * имеющие название вида :article с двоеточием вначале */
			$placeholderPagesSelect = $this->db->select()
				->from("cms_pages")
				->where("name LIKE ':%'")
				->where("hidden = 0")
				->where("table_id <> 0")
				->where("table_id IS NOT NULL")
				->where("parent_id = ?", $parentID)
				->order("number DESC");
			
			$rows = $this->db->fetchAll($placeholderPagesSelect); 
			if($rows) foreach ($rows as $placeholderPage) {
				require_once "models/Table/DbTable.php";
				$table = new DbTable($this->db, $placeholderPage['table_id']);
				$field = $table->getURLField();
				$table->setFieldValue($field->getName(), $pagename);
				$rows = $table->getRows();
				
				if($rows) {
					$placeholder = substr($placeholderPage['name'], 1);
					$placeholderPage[$placeholder] = $rows[0];
					return $placeholderPage;
				}
			}
		}
		
		
		return $this->db->fetchRow($select);
	}
	
	/**
	 * Добавить новю страницу в таблицу cms_pages
	 * @param string $name
	 * @param integer $templateID
	 * @param string $content
	 * @param integer $parentID
	 * @param array $options
	 * @return CMSPage
	 */
	public function addPage($name, $templateID = 0, $content = '', $parentID = null, $options = array()) {
		$page = $options;
		$page['name'] = $name;
		$page['template'] = $templateID;
		$page['content'] = $content;
		$page['parent_id'] = $parentID;
		$page['number'] = 1 + (int) $this->db->fetchOne("SELECT MAX(number) FROM cms_pages");
		
		$this->db->insert("cms_pages", $page);
		
		$this->lastPageID = $this->db->lastInsertId();
		array_push($this->pagesStack, $this->lastPageID);
		
		return $this;
	}
	
	/**
	 * Добавить страницу дочернюю текущей 
	 * @param string $name
	 * @param integer $templateID
	 * @param string $content
	 * @param array $options
	 * @return CMSPage
	 * @throws Exception
	 */
	public function addChild($name, $templateID = 0, $content = '', $options = array()) {
		if($this->lastPageID) {
			$parentID = $this->lastPageID;
			$this->addPage($name, $templateID, $content, $parentID, $options);
			$this->lastPageID = $parentID;
			return $this;
		} else {
			throw new Exception("Last page id is not set");
		}
	}
	
	/**
	 * @return integer
	 */
	public function getLastPageID() {
		return $this->lastPageID;
	}
	
	public function deletePage($options) {
		if(is_numeric($options)) {
			$this->db->delete("cms_pages", array("id = ?" => $options));
		} else {
			$where = array();
			foreach ($options as $colname => $colvalue) {
				$where["`$colname` = ?"] = $colvalue;
			}
			$this->db->delete("cms_pages", $where);
		}
	}
	
	/**
	 * @return CMSPage
	 */
	public function setParent() {
		$this->lastPageID = end($this->pagesStack);
		return $this;
	}
}

?>