<?php
abstract class Page {
	
	const TYPE_MODULE = 3;
	const TYPE_DBTABLE = 2;
	const TYPE_ANOTHERTYPE = 1;
	
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	/**
	 *
	 * @var Bof_Request
	 */
	protected $request;
	
	protected $section;
	protected $sectionPages;
	protected $page;
	protected $baseURL;
	/**
	 *
	 * @var User
	 */
	protected $user;
	
	private $cssFiles;
	private $jsFiles;
	
	public function getCSSFiles() {
		return $this->cssFiles ? $this->cssFiles : array();
	}
	public function getJSFiles() {
		return $this->jsFiles ? $this->jsFiles : array();
	}
	public function appendCSSFiles($files) {
		if(is_array($this->cssFiles)) {
			$this->cssFiles = array_merge($this->cssFiles, $files);
		} else {
			$this->cssFiles = $files;
		}
	}
	public function appendJSFiles($files) {
		if(is_array($this->jsFiles)) {
			$this->jsFiles = array_merge($this->jsFiles, $files);
		} else {
			$this->jsFiles = $files;
		}
	}
	
	public function __construct($dbAdapter, $smarty, $request) {
		$this->db = $dbAdapter;
		$this->smarty = $smarty;
		$this->request = $request;
		$this->section = array(
			'id' => null,
			'name' => null
		);
		$this->page = array(
				'id' => null,
				'viewname' => null
		);
	}
	
	abstract public function renderMenu();
	abstract public function getPageTitle();
	abstract public function getBreadcrumbs();
	abstract public function render();
	
	/**
	 * @return BreadcrumbsRendererAbstract
	 */
	public function getBreadcrumbsRenderer() {
		require_once APPLICATION_PATH . '/models/Breadcrumbs/SimpleBreadcrumbsRenderer.php';
		return new SimpleBreadcrumbsRenderer($this->smarty);
	}
	
	public function renderPage() {
		$this->smarty->assign("page", $this->page);
		$this->smarty->assign("section", $this->section);
		return $this->render();
	}
	
	/**
	 * @return Zend_Db_Adapter_Abstract
	 */
	public function getDbAdapter() {
		return $this->db;
	}
	/**
	 * @return Smarty
	 */
	public function getSmarty() {
		return $this->smarty;
	}
	
	public function setBaseURL($url) {
		$this->baseURL = $url;
	}
	
	public function setUser($user) {
		$this->user = $user;
	}
	
	public function checkPageAccess() {
		if(!$this->user || !$this->user->hasPageAccess($this->getPageID(), User::ACCESS_READ)) {
			throw new AccessException("Access denied");
		}
	}
	
	/**
	 *
	 * @param integer $id
	 * @param Zend_Db_Adapter_Abstract $dbAdapter
	 */
	public static function getPage($id, $dbAdapter) {
		$select = $dbAdapter->select()
			->from("bof_pages")
			->where("id = ?", $id);
		$page = $dbAdapter->fetchRow($select);
		if(!$page) {
			throw new PageNotFoundException("Page with id = $id not found");
		} elseif($page['hidden']) {
			throw new PageNotFoundException("Page with id = $id id disabled");
		}
		return $page;
	}
	
}

class PageNotFoundException extends Exception {
	
}

class PageTypeException extends Exception {
	
}

?>