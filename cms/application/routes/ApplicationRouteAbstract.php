<?php

require_once ('Zend/Controller/Router/Route/Abstract.php');

abstract class ApplicationRouteAbstract extends Zend_Controller_Router_Route_Abstract {
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	private $db;
	
	public function setDbAdapter($db) {
		$this->db = $db;
	}
	public function getDbAdapter() {
		return $this->db;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Zend_Controller_Router_Route_Abstract::getVersion()
	 */
	public function getVersion() {
		return 1;
	}
}

?>