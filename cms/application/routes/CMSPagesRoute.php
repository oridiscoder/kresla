<?php

require_once ('ApplicationRouteAbstract.php');
require_once "models/Page/CMSPage.php";

class CMSPagesRoute extends ApplicationRouteAbstract {
	
	/**
	 * 
	 * @var CMSPage
	 */
	private $pagesModel;
	
	/**
	 * 
	 * @var CMSPagesRoute
	 */
	private static $instance;
	
	public function __construct() {
		self::$instance = $this;
	}
	
	/**
	 * Найти страницу по заданному URL
	 * @param string $path - url
	 * @param integer $startSectionCheck - с какого номера подстраницы проверять, является ли страница разделом
	 */
	public function match($path, $startSectionCheck = 0) {
		
		$cmsPages = $this->getCMSPagesModel();
		
		$parts = explode('/', $path);
		$parts = array_values(array_filter($parts));
		$parts = array_merge(array("/"), $parts);	//добавляем главную страницу в начало
		
		$currentPage = null;
		$partsCount = count($parts);
		for($i = 0; $i < $partsCount; $i ++) {
		
			$parentID = $currentPage ? $currentPage['id'] : null;
			$page = $cmsPages->findPage($parts[$i], $parentID);
			if(empty($page)) {
				$currentPage = null;
				break;
			} else {
				
				$page['_level'] = $i;	//уровень вложенности страницы
				
				if($currentPage) {
					$page['_parent'] = $currentPage;
				}
				$currentPage = $page;
				if($startSectionCheck <= $i && $currentPage['is_section']) {
					$currentPage['_child_url_parts'] = array_slice($parts, $i + 1);
					//дальше страница сама разберётся что делать
					break;
				}
			}
		}
		
		if($currentPage) {
			require_once "front/Controller.php";
			return array(
				'controller' => '\\front\\Controller', 
				'action' => 'index', 
				'cms_page' => $currentPage
			);
		} else {
			return false;
		}
	}
	
	/**
	 * @return CMSPage
	 */
	private function getCMSPagesModel() {
		if($this->pagesModel == null) {
			$this->pagesModel = new CMSPage($this->getDbAdapter());
		}
		return $this->pagesModel;
	}

	public function assemble($data = array(), $reset = false, $encode = false) {
		
	}

	public static function getInstance(Zend_Config $config) {
		if(!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}

?>