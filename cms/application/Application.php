<?php
require_once 'Bof/Request.php';
require_once 'Bof/BOFEvent.php';

class Application {
	
	private static $appConfigs;
	private static $appEnvironment;
	private static $authorisedUser;
	/**
	 *
	 * @var Application
	 */
	private static $instance;
	/**
	 *
	 * @var Bof_Request
	 */
	private $request;
	/**
	 *
	 * @var Smarty
	 */
	private $smarty;
	/**
	 *
	 * @var Zend_Config
	 */
	private $config;
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	private $environment = 'production';
	private $baseCMSURL = '/';
	
	private $errorController;
	
	private function __construct() {
		$this->environment = self::$appEnvironment;
		$this->loadConfig(self::$appConfigs);
		BuildTimer::timing("app-config-loaded");
		$this->request = new Bof_Request();
		BuildTimer::timing("app-request-init");
		$this->initDbAdapter();
		BuildTimer::timing("app-db-init");
		mb_internal_encoding($this->config->charset);
	}
	
	protected function loadConfig($configs) {
		if(is_string($configs)) {
			$this->config = new Zend_Config_Ini($configs, $this->environment);
		} elseif(is_array($configs)) {
			foreach ($configs as $config) {
				if(is_string($config)) {
					if($this->config) {
						$this->config = $this->config->merge(new Zend_Config_Ini($config, $this->environment));
					} else {
						$this->config = new Zend_Config_Ini($config, $this->environment, array('allowModifications' => true));
					}
				} else {
					throw new Zend_Config_Exception("Expected string for config file name");
				}
			}
		}
	}
	
	protected function initSmarty() {
		
		define('SMARTY_RESOURCE_CHAR_SET', $this->config->charset);
		require_once 'Smarty/Smarty.class.php';
		
		$this->smarty = new Smarty();
		if(!isset($this->config->smarty)) {
			throw new Exception("Не указана директива smarty в настройках приложения");
		}
		if(!isset($this->config->smarty->template_dir)) {
			throw new Exception("Не указана директива smarty.template_dir в настройках приложения");
		}
		if(!isset($this->config->smarty->compile_dir)) {
			throw new Exception("Не указана директива smarty.compile_dir в настройках приложения");
		}
		$this->smarty->setTemplateDir($this->config->smarty->template_dir);
		$this->smarty->setCompileDir($this->config->smarty->compile_dir);
	}
	
	protected function initDbAdapter() {
		if(!isset($this->config->db)) {
			throw new ApplicationException("Не указаны настройки подключения к БД (db section)");
		}
		$this->dbAdapter = Zend_Db::factory($this->config->db);
		$this->dbAdapter->query("SET NAMES UTF8");
	}
	
	public function dispatch() {
				
		$router = new Zend_Controller_Router_Rewrite();
		
		/* т.к. Zend_Router проверяет роуты с конца, то в начале надо добавить роуты фронта, а в конце - админки */
		$this->loadAdditionalRoutes($router);
		
		if(!isset($this->config->routes)) {
			throw new ApplicationException("Не задана секция routes в конфиг.файле");
		}		
		$router->addConfig($this->config->routes);
		$indexRoute = $router->getRoute("index");
		if(!$indexRoute) {
			throw new ApplicationException("Не указан базовый route (index)");
		} else {
			$this->baseCMSURL = '/'.$indexRoute->assemble();
		}
		BuildTimer::timing("application-routes-init");
		
		
		try {
			
			$router->route($this->request);
			BuildTimer::timing("application-routes-route");
			
			$controllerName = $this->request->getControllerName();			
			$controllerClassName = $controllerName;

			$actionName = $this->request->getActionName();
			$methodName = $actionName.'Action';
			
			/* возможно,  route уже сам подгрузил контроллер, который он хочет */
			if(!class_exists($controllerName)) {
				
				/* если же нет, то идем по стандартному пути */
				$controllerClassName = ucfirst($controllerName).'Controller';
				$controllerFileName = $controllerClassName.'.php';
				$controllersDir = realpath(APPLICATION_PATH . '/controllers');
				
				
				if(!file_exists($controllersDir.'/'.$controllerFileName)) {
					throw new ApplicationException("Controller $controllerFileName not found in $controllersDir");
				}
				
				require_once $controllersDir.'/'.$controllerFileName;
				
				if(!class_exists($controllerClassName)) {
					throw new ApplicationException("Controller class $controllerClassName not found in $controllersDir/$controllerFileName ");
				}
			}
			
			$controller = new $controllerClassName($this);
			BuildTimer::timing("controller-construct");
			
			if(!method_exists($controller, $methodName)) {
				throw new ApplicationException("Action  $methodName not found in $controllersDir/$controllerFileName ");
			}
			
            call_user_func(array($controller, $methodName));
			BuildTimer::timing("controller-method-invoke");
			
		} catch (Exception $ex) {
			
			$errorController = $this->getErrorController();
			$errorController->setException($ex);
			$errorController->errorAction();
			
			switch($ex->getCode()) {
				
				case 500:
					header($_SERVER['SERVER_PROTOCOL'].' 500 Internal Server Error', true, 500);
					break;
				default:
					header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found', true, 404);
					break;
			}
		}
	}
	
	public function getErrorController() {
		if(!$this->errorController) {
			require_once APPLICATION_PATH . '/controllers/ErrorController.php';
			$this->errorController = new ErrorController($this);
		}
		return $this->errorController;
	}
	
	/**
	 * Загружаем дополнительные кастомные сложные Роуты.
	 *
	 * @param Zend_Controller_Router_Rewrite $router
	 */
	public function loadAdditionalRoutes($router) {
		$path = $this->config->routes_path;
		
		if(file_exists($path)) {
			$dh = opendir($path);
			if($dh) {
				while(($filename = readdir($dh)) != false) {
					if($filename != '.' && $filename != '..' && !strpos($filename, "Abstract")) {
						require_once $path.'/'.$filename;
						$className = str_replace(".php", "", $filename);
						if($className == 'ClusterRouter' && !$this->config->clusters->enable) {
							continue;
						}
						if($className == 'CMSPagesRoute' && !$this->config->cms_pages->enable) {
							continue;
						}
						if(class_exists($className)) {
							/* @var $route ApplicationRouteAbstract */
							$route = call_user_func_array(array($className, "getInstance"), array($this->config));
							$route->setDbAdapter($this->getDbAdapter());
							$router->addRoute($className, $route);
						}
					}
				}
			}
		}
	}
	
	public function getSmarty() {
		if($this->smarty === null) {
			$this->initSmarty();
		}
		return $this->smarty;
	}
	public function getConfig() {
		return $this->config;
	}
	public function getRequest() {
		return $this->request;
	}
	public function getBaseURL() {
		return $this->baseCMSURL;
	}
	public function getDbAdapter() {
		return $this->dbAdapter;
	}
	public function getCharset() {
		return $this->config->charset;
	}
	
	public static function setConfigs($configs) {
		self::$appConfigs = $configs;
	}
	
	public static function setEnvironment($env) {
		self::$appEnvironment = $env;
	}
	
	/**
	 *
	 * @return Application
	 */
	public static function getInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * @return Zend_Config
	 * @throws ApplicationException
	 */
	public static function getAppConfigs() {
		if(self::$instance) {
			return self::$instance->getConfig();
		} else {
			throw new ApplicationException("Application instance initialize required");
		}
	}
	
	/**
	 * @return Zend_Db_Adapter_Abstract
	 * @throws ApplicationException
	 */
	public static function getAppDbAdapter() {
		if(self::$instance) {
			return self::$instance->getDbAdapter();
		} else {
			throw new ApplicationException("Application instance initialize required");
		}
	}
	
	/**
	 * @return Smarty
	 * @throws ApplicationException
	 */
	public static function getAppSmarty() {
		if(self::$instance) {
			return self::$instance->getSmarty();
		} else {
			throw new ApplicationException("Application instance initialize required");
		}
	}
	
	/**
	 * @return string
	 * @throws ApplicationException
	 */
	public static function getAppBaseURL() {
		if(self::$instance) {
			return self::$instance->getBaseURL();
		} else {
			throw new ApplicationException("Application instance initialize required");
		}
	}
	
	/**
	 * @return Bof_Request
	 * @throws ApplicationException
	 */
	public static function getAppRequest() {
		if(self::$instance) {
			return self::$instance->getRequest();
		} else {
			throw new ApplicationException("Application instance initialize required");
		}
	}
	
	public static function getAppCharset() {
		if(self::$instance) {
			return self::$instance->getCharset();
		} else {
			throw new ApplicationException("Application instance initialize required");
		}
	}
	
	/**
	 * @return User
	 */
	public static function getAppUser() {
		return self::$authorisedUser;
	}
	/**
	 * @return ErrorController
	 */
	public static function getAppErrorController() {
		return self::getInstance()->getErrorController();
	}
	public static function setAuthorisedUser($user) {
		self::$authorisedUser = $user;
	}
	
	public static function run() {
		self::getInstance()->dispatch();
	}
}

class ApplicationException extends Exception {
	
}