<?php

error_reporting(E_ALL);

if(!defined('APPLICATION_PATH')) {
	define('APPLICATION_PATH', dirname(__FILE__).'/..');
}


require_once APPLICATION_PATH.'/bof.old/libs/BOFEvent.php';
require_once APPLICATION_PATH.'/bof.old/libs/BOFEventManager.php';
require_once APPLICATION_PATH.'/bof.old/libs/utils.php';

set_include_path(
			APPLICATION_PATH . 
			PATH_SEPARATOR . APPLICATION_PATH . '/../library'.
			PATH_SEPARATOR . get_include_path()
);

require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();

require_once 'Bof/idna_convert.class.php';

abstract class BOFScriptAbstract {
	
	const TASK_PRIORITY_LOW = 215;
	const TASK_PRIORITY_MEDIUM = 216;
	const TASK_PRIORITY_HIGH = 217;
	
	const TASK_STATUS_NEW = 218;
	const TASK_STATUS_READ = 219;
	const TASK_STATUS_WORK = 220;
	const TASK_STATUS_COMPLETE = 221;
	const TASK_STATUS_APPROVE = 222;
	
	const TASKER_STATUS_WAIT = 167;
	const TASKER_STATUS_WORK = 168;
	const TASKER_STATUS_FAIL = 169;
	
	const EMPLOYEE_BOF = 93;
	
	protected $allowedTaskerStatuses = array(
		self::TASKER_STATUS_WAIT,
		self::TASKER_STATUS_WORK,
		self::TASKER_STATUS_FAIL
	);
	
	/**
	 *
	 * @var Zend_Config
	 */
	protected $config;
		
	/**
	 * id задачи из таблицы `tasker` - используется в случае, если скрипт был запущен из таскера БОФа
	 * инициализируется смостоятельно в конструкторе
	 * @var integer
	 */
	protected $taskerID = null;
	
	/**
	 * Скрипту можно передавать аргументы через --arg_name="argvalue"
	 * @var array
	 */
	private $consoleArgs = array();
	/**
	 * id скрипта. Устанавливается автоматически в случае,
	 * когда скрипт запускается через таскер и мы имеем установленный $taskerID
	 * @var integer
	 */
	private $scriptID;
	
	/**
	 *
	 * @var Zend_Db_Adapter_Mysqli
	 */
	protected $db = null;
	
	/**
	 *
	 * @var BOFEventManager
	 */
	private $eventManager;
	
	/**
	 * ID объекта, с которым работает скрипт. Это может быть сайт, страница, проект. Сам скрипт решает, что должно храниться в данной переменной.
	 * @var integer
	 */
	private $objectID;
	
	/**
	 *
	 * @var Подавлять вывод echo в функции $this->yell()
	 */
	private $suppressEcho = false;
	
	private $echoLog;
	
	public function __construct($db = null) {

		if($db instanceof Zend_Db_Adapter_Abstract) {
			$this->db = $db;
		} else {
			/* настройки подключения к БД */
			$dbConfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/database.ini', 'production');
			require_once 'Zend/Db/Adapter/Mysqli.php';
			
			$this->db = Zend_Db::factory($dbConfig->db);
			$this->db->query("SET NAMES UTF8");
		}
		
		/* настройки приложения */
		$this->config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
		
		$this->eventManager = new BOFEventManager($this->db);
		
		/* если скрипт запускается через Таскер Бофа, то ему передаётся task_id
		 * task_id - это id задачи в таблице bof_db.tasker
		 * по-хорошему, этим должен рулить отдельный класс
		 */
		global $argv, $argc;
		if(isset($argv) && $argc > 1) {
			foreach ($argv as $arg) {
				if(preg_match('/^--task_id=(\d+)$/', $arg, $matches)) {
					$this->taskerID = $matches[1];
				}
				if(preg_match('/^--(\w+)=(.*)$/', $arg, $matches)) {
					$this->consoleArgs[$matches[1]] = $matches[2];
				}
			}
		}
	}
	
	/**
	 * Получить значение аргумента, переданного через параметры консоли --argname=argvalue
	 * @param string $name
	 * @return string|array|null - если задано $name, то значение аргумента $name, иначе - весь массив аргументов
	 */
	public function getConsoleArg($name = null) {
		if($name) {
			return isset($this->consoleArgs[$name]) ? $this->consoleArgs[$name] : null;
		} else {
			return $this->consoleArgs;
		}
	}
	
	/**
	 * Установить значение консольной переменной
	 * @param string|array $name - имя переменной, или массив вида имя=>значение
	 * @param mixed $value - значение переменной
	 */
	public function setConsoleArg($name, $value = null) {
		if(is_array($name)) {
			$this->consoleArgs = $name;
		} elseif(is_string($name)) {
			$this->consoleArgs[$name] = $value;
		}
	}
	
	/**
	 * Установить ID объекта. Предполагается, что после этого скрипт выберет для обработки
	 * из всего пула объектов, только заданный. Сам скрипт решает, что делать, если задан objectID
	 * @param integer $id
	 */
	public function setObjectID($id) {
		$this->objectID = $id;
	}
	
	/**
	 * Получить ID заданного объекта
	 * @return integer | null
	 */
	public function getObjectID() {
		return $this->objectID;
	}
	
	public function setScriptID($id) {
		$this->scriptID = $id;
	}
	public function getScriptID() {
		if(!$this->scriptID) {
			if(!$this->taskerID) {
				return null;
			} else {
				$this->scriptID = $this->db->fetchOne("SELECT script_id FROM tasker WHERE id = ?", $this->taskerID);
			}
		}
		return $this->scriptID;
	}
	public function getTaskerID() {
		return $this->taskerID;
	}
	public function setTaskerID($id) {
		$this->taskerID = $id;
	}
	
	/**
	 *
	 * @param boolean $suppress - true|false - подавлять или нет вывод сообщений на экран
	 */
	public function suppressYelling($suppress) {
		$this->suppressEcho = $suppress;
	}
	
	/**
	 * Метод getExecuterId должен определить, кто ответственный по заданому событию
	 * @param BOFEvent $event
	 * @return integer - ID пользователя из таблицы `employies`
	 */
	abstract public function getExecuterId(BOFEvent $event);
	/**
	 * Метод getInitiatorId должен вернуть id постановщика задачи. По умолчанию - 93 (БОФ)
	 * @param BOFEvent $event
	 * @return integer
	 */
	public function getInitiatorId(BOFEvent $event) {
		return self::EMPLOYEE_BOF;	//ID записи из таблицы `employies`
	}
	/**
	 * Метод getEventPriority должен определять приоритет задачи, которая будет поставлена по заданному событию
	 * @param BOFEvent $event
	 * @return integer PRIORITY_LOW = 215, PRIORITY_MEDIUM = 216, PRIORITY_HIGH = 217;
	 */
	abstract public function getEventPriority(BOFEvent $event);
	
	/**
	 * Метод getProjectId должен вернуть ID проекта, к которому мы поставим задачу.
	 * @param BOFEvent $event
	 * @return integer - ID проекта
	 */
	abstract public function getProjectId(BOFEvent $event);
	
	/**
	 * В методе work надо описать всю  внутренность скрипта.
	 */
	abstract protected function work();
	
	/**
	 * Для начала работы скрипта необходимо вызвать этот метод.
	 */
	public final function run() {
		$this->work();
		$this->doneWork();
	}
	
	/**
	 * @return BOFEventManager
	 */
	public function getEventManager() {
		return $this->eventManager;
	}
	
	/**
	 * Метод вызывается после окончания основной работы
	 */
	protected function doneWork() {
		if($this->taskerID && is_numeric($this->taskerID)) {
			$this->db->update("tasker", array('status' => self::TASKER_STATUS_WAIT, 'last_run' => strftime('%Y-%m-%d %H:%M:%S', time())), "id = $this->taskerID");
		}
	}
	
	/**
	 * Если скрипт запущен таскером бофа, то можно самостоятельно изменить его статус
	 * @param integer $status [167, 168, 169]
	 */
	protected function setTaskerStatus($status) {
		if($this->taskerID && is_numeric($this->taskerID)) {
			if(in_array($status, array(self::TASKER_STATUS_WAIT, self::TASKER_STATUS_WORK, self::TASKER_STATUS_FAIL))) {
				$this->db->update("tasker", array('status' => $status), "id = $this->taskerID");
			} else {
				throw new BOFScriptException("Tasker status $status not allowed. Allowed statuses: ".implode(',', $this->allowedTaskerStatuses));
			}
		}
	}
	
	/**
	 * Запускает задачу.
	 * @param integer $taskID - ID задачи из таблицы tasker
	 */
	protected function runTask($taskID) {
		$this->db->query("UPDATE tasker SET run_immediately = 1, status = ".self::TASKER_STATUS_WAIT." WHERE id = ?", $taskID);
	}
	
	/**
	 * Эту функцию надо использовать вместо echo! Потому что в некоторых случаях, необходимо подавлять вывод сообщений в поток вывода.
	 * @param string $text
	 */
	protected function yell($text) {
		if(!$this->suppressEcho) {
			$str = $text."\n";
			echo $str;
			$this->echoLog .= $str;
		}
	}
	
	/**
	 * Получить одной строкой лог скрипта. 
	 * Используйте $this->yell() вместо echo в своих скриптах
	 * @return string
	 */
	public function getEchoLog() {
		return $this->echoLog;
	}
	
	protected function logMessage($message, $scriptID = null) {
		
		$scriptID = $scriptID ? $scriptID : $this->getScriptID();
		
		$number = 1 + (int) $this->db->fetchOne("SELECT MAX(number) FROM scripts_logs");
		
		$this->db->insert("scripts_logs", array(
			'script_id' => $scriptID,
			'text' => $message,
			'number' => $number,
			'date' => strftime("%Y-%m-%d %H:%M:%S", time())
		));
	}
	
	/**
	 * Открыть событие
	 * @param BOFEvent $event
	 */
	protected function openEvent(BOFEvent $event) {
		$event->setInitiatorId($this->getInitiatorId($event));
		$event->setExecuterId($this->getExecuterId($event));
		$priority = $this->getEventPriority($event);
		if($priority) {
			$event->setPriority($this->getEventPriority($event));
		} else {
			$event->setPriority(self::TASK_PRIORITY_LOW);
		}
		$event->setProjectId($this->getProjectId($event));
		$this->eventManager->openEvent($event);
	}
	
	/**
	 * Закрыть событие
	 * @param BOFEvent $event
	 */
	protected function closeEvent(BOFEvent $event) {
		$this->eventManager->closeEvent($event);
	}
	
	/**
	 * Получить задачу, которая была поставлена по заданному реальному (имеющему id) событию.
	 * @param BOFEvent $event
	 * @return array
	 */
	protected function getTaskByEvent(BOFEvent $event) {
		$id = $event->getID();
		if(!is_numeric($id)) {
			$event = $this->eventManager->findEvent($event);
			if(!$event) {
				//throw new BOFScriptException("Event has no id, tried to find with event manager - not found");
				$id = null;
			} else {
				$id = $event->getID();
			}
		}
		
		if($id) {
			$select = $this->db->select()
				->from("bof_tasks")
				->where("event_id = ?", $id);
			$task =  $this->db->fetchRow($select);
		} else {
			$task = null;
		}
		
		return $task;
	}
	
	protected function reopenTask($task) {
		if(in_array($task['status'], array(self::TASK_STATUS_COMPLETE, self::TASK_STATUS_APPROVE))) {
			$deadlineTime = strtotime($task['date_deadline']) - strtotime($task['date_start']);
			
			$updTask = array(
				'status' => self::TASK_STATUS_NEW,
				'date_deadline' => date('Y-m-d', time() + $deadlineTime)
			);
			
			$this->db->update("bof_tasks", $updTask, "id = ".$task['id']);
		}
	}
	
	/**
	 * Комментировать задачу от имени БОФа
	 * @param integer $taskID - id задачи
	 * @param string $comment - Текст комментария
	 */
	protected function commentTask($taskID, $comment) {
		$comment = array(
			'task_id' => $taskID,
			'text' => $comment,
			'date' => strftime("%Y-%m-%d %H:%M:%S", time()),
			'user_id' => self::EMPLOYEE_BOF
		);
		$this->db->insert("bof_tasks_comments", $comment);
	}
	
	/**
	 * Функция отправки почты. Очень полезная и нужная для скриптов.
	 * @param string $recipient - e-mail получателя
	 * @param string $subject - Тема письма
	 * @param string $mailHTMLBody - HTML текст письма. Все письма отправляются как HTML форматированные.
	 * @param array $copyRecipients = array() - e-mail адреса тех, кто должен получить скрытую копию
	 * @return boolean - отправилось письмо или нет
	 * @throws BOFScriptException
	 */
	protected function sendMail($recipient, $subject, $mailHTMLBody, $senderName = null, $copyRecipients = array()) {
		
		require_once 'PHPMailer/class.phpmailer.php';
		
		$fromEmail = $this->config->mail->defaultFrom->email;
		$fromName =  $senderName ? $senderName : $this->config->mail->defaultFrom->name;
		
		$smtpHost = $this->config->mail->transport->host;
		$smtpPort = $this->config->mail->transport->port;
		$smtpLogin = $this->config->mail->transport->username;
		$smtpPassword = $this->config->mail->transport->password;
		
		if(!$fromEmail || !$fromName || !$smtpHost || !$smtpLogin || !$smtpPassword) {
			throw new BOFScriptException("
				Can't find one or more mail settings in application config!\n
				Config example: \n
				mail.transport.type = smtp\n
				mail.transport.host = smtp.oridis.ru\n
				mail.transport.auth = login\n
				mail.transport.username = bof@oridis.ru\n
				mail.transport.password = somepassword\n
				mail.transport.register = true\n
				mail.defaultFrom.email = bof@oridis.ru\n
				mail.defaultFrom.name = МегаБОФ\n
			");
		}
		
		$mailer = new PHPMailer();
		$mailer->SetFrom($fromEmail, $fromName);
		$mailer->CharSet = 'windows-1251';
		$mailer->AddAddress($recipient);
		$mailer->MsgHTML($mailHTMLBody);
		$mailer->Subject = $subject;
		
		$mailer->IsSMTP();
		$mailer->SMTPAuth = true;
		if(isset($this->config->mail->transport->secure)) {
			$mailer->SMTPSecure = $this->config->mail->transport->secure;
		}
		$mailer->Host = $smtpHost;
		$mailer->Port = $smtpPort;
		$mailer->Username = $smtpLogin;
		$mailer->Password = $smtpPassword;
		$mailer->SMTPDebug = 1;
		
		$sendStatus =  $mailer->Send();
		unset($mailer);
		return $sendStatus;
	}
	
	/**
	 * Получить путь к дириктории, в которую можно складировать файлы, необходимые для работы скрипта.
	 * Кукисы, загружаемые файлы и т.п.
	 * @return string
	 */
	public function getDataPath() {
		return APPLICATION_PATH . '/../temp/scripts_data';
	}
	
}

class BOFScriptException extends Exception {};

?>