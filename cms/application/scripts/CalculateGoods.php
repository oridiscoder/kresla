<?php
require_once(__DIR__."/../../../Patch.php");

class CalculateGoods extends Patch{
	
	public function construct(){
		parent::_comstruct();
	}
	
	public function run(){
		$this->truncateCountTable();
		$sets = $this->getSets();
		foreach($sets as $k=>$set){
			$sets[$k]['count'] = $this->getCount($set['cat_id'], $set['mat_id'], $set['col_id']);
			echo "{$set['cat_title']}({$set['cat_id']}) {$set['mat_title']}({$set['mat_id']}) {$set['col_title']}({$set['col_id']}) --> {$sets[$k]['count']}   \n";
			$this->insertCount($set['cat_id'], $set['mat_id'], $set['col_id'], $sets[$k]['count']);
		}
	}
	
	public function getSets(){
		return $this->getDb()->fetchAll("SELECT U.url mat_title, U.id mat_id, C.url cat_title, C.id cat_id, CO.url col_title, CO.id col_id
FROM `kresla_upholstery_types` U,
	 `kresla_categories` C,
     `kresla_colors` CO
WHERE C.id IN (2, 3, 4, 5, 6, 8, 9)
ORDER BY C.id, U.id, CO.id");
	}
	public function getCount($cat_id, $mat_id, $col_id){
		return $this->getDb()->fetchOne("SELECT count(*)
FROM `kresla_goods` G 
LEFT JOIN `kresla_good_upholsteries` GU ON GU.good_id = G.id
LEFT JOIN `kresla_upholstery` U ON  U.id = GU.upholstery_id
LEFT JOIN `kresla_upholstery_types` UT ON UT.id = U.type_id
LEFT JOIN `kresla_good_categories` GC ON  GC.good_id=G.id

WHERE UT.id = ".$mat_id."
AND GC.category_id = ".$cat_id."
AND U.color_id = ".$col_id."
AND G.hidden = 0
GROUP BY GC.category_id, U.color_id, UT.id");
	}
	
	public function insertCount($cat_id, $mat_id, $col_id, $count){
		if($count)
			$this->getDb()->insert("kresla_goods_count", array(
					'category_id'=>$cat_id, 
					'type_id'=>$mat_id, 
					'color_id'=>$col_id,
					'count'=> $count
			));
		
	}
	
	public function truncateCountTable(){
		$this->getDb()->query("TRUNCATE TABLE "."kresla_goods_count");
	}
	
}

$Calc = new CalculateGoods();
$Calc->run();