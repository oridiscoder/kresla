<?php
require_once ('SuperController.php');

class IndexController extends SuperController {
	public function indexAction() {
		header('Location: '.$this->baseURL.'/section/2/');
		exit();
	}
}