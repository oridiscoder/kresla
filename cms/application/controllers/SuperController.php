<?php
require_once APPLICATION_PATH . '/models/Breadcrumbs/SimpleBreadcrumbsRenderer.php';

class SuperController {
	/**
	 *
	 * @var Smarty
	 */
	protected $smarty;
	/**
	 *
	 * @var Bof_Request
	 */
	protected $request;
	/**
	 *
	 * @var Zend_Config
	 */
	protected $config;
	
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $dbAdapter;
	protected $useLayout = true;
	protected $layoutFileName = null;
	protected $user;
	protected $pageTitle;
	protected $baseURL;
	protected $menuLeftHTML;
	protected $breadcrumbs;
	protected $layoutScripts = array();
	protected $layoutSlyles = array();
	private $disableMenuOverwrite = false;
	/**
	 *
	 * @var BreadcrumbsAbstract
	 */
	protected $breadcrumbsRenderer;
	
	/**
	 *
	 * @var MenuBuilderAbstract
	 */
	protected $topMenuBuilder;
	
	/**
	 * 
	 * @var InformerLoader
	 */
	protected $informers;
	
	/**
	 *
	 * @param Application $application
	 */
	public function __construct($application) {
		$this->smarty = $application->getSmarty();
		$this->config = $application->getConfig();
		$this->request = $application->getRequest();
		$this->baseURL = $application->getBaseURL();
		$this->dbAdapter = $application->getDbAdapter();
		
		if(isset($this->config->layout)) {
			$this->layoutFileName = $this->config->layout;
		}
		
		if($this->mustAuthorise()) {
			require_once APPLICATION_PATH . '/models/Authoriser.php';
			$authoriser = new Authoriser($this->dbAdapter, $this->config->auth->session_name);
			$this->user = $authoriser->getAuthorised();
			Application::setAuthorisedUser($this->user);
			if(!$this->user) {
				$location = $this->baseURL == '/' ? '/auth/' : $this->baseURL.'/auth/';
				$requestedURI = $this->request->getRequestUri();
				if($requestedURI != $location) {
					$location .= '?return_path='.urlencode($requestedURI);
				}
				header("Location: $location");
				exit();
			} else {
				$authoriser->setUserLastLoginTime($this->user->getId());
				$authoriser->logUserStep($this->user);
			}
		}
		
		$this->smarty->assign("user", $this->user);
		
		/*
		 * Уберём из $baseURL завершающий слэш. Т.е /auth/ => /auth, "/" => ""
		 * С таким базовым URL гораздо легче работать в шаблонах smarty
		 */
		$baseURL = preg_replace('#(.*?)/?$#', '$1', $this->baseURL);
		$this->smarty->assign("baseURL", $baseURL);
		
		$this->breadcrumbsRenderer = new SimpleBreadcrumbsRenderer($this->smarty);
		
		require_once APPLICATION_PATH . '/models/Menu/TopMenuBuilder.php';
		$this->topMenuBuilder = new TopMenuBuilder($this->dbAdapter);
		
		$this->init();
		
	}
	
	public function init() {
		
	}
	
	/**
	 * Определяет, необходима ли авторизация на текущей странице.
	 * Все страницы, которые находятся за базовым URL ($this->baseURL) требуют авторизации
	 * Пример: $this->baseURL = /cms-admin/
	 * Авторизацию будут запрашивать все  страницы вида /cms-admin/* кроме /cms-admin/auth/
	 * @return boolean
	 */
	protected function mustAuthorise() {
		$requestedPath = $this->request->getPathInfo();
		if(substr($requestedPath, 0, 1) != '/') {
			$requestedPath  = '/'.$requestedPath;
		}
		if(substr($requestedPath, strlen($requestedPath) - 1, 1) != '/') {
			$requestedPath = $requestedPath.'/';
		}
		if(strpos($requestedPath, $this->baseURL) === 0
				&& $this->request->getControllerName() != 'auth'
			) {
			return true;
		} else {
			return false;
		}
	}

	protected function render($fileName) {
		$pageContent = $this->smarty->fetch($fileName);
		$pageContent = $this->wrap($pageContent);
		return $pageContent;
	}
	
	protected function wrap($content) {
		$this->initDefaultSmartyVariables();
		if($this->useLayout) {
			if(!$this->layoutFileName || !file_exists($this->layoutFileName)) {
				throw new ApplicationException("Layout section not found in config file");
			}
			$this->smarty->assign("content", $content);
			$pageContent = $this->smarty->fetch($this->config->layout);
		} else {
			$pageContent = $content;
		}
		return $pageContent;
	}
	
	protected function initDefaultSmartyVariables() {
		
		require_once APPLICATION_PATH . '/models/Menu/TopMenuRenderer.php';
		
		$this->smarty->assign("userHelloPlate", $this->smarty->fetch("user-hello-plate.html"));
		
		$menu = array();
		
		$topMenuRenderer = new TopMenuRenderer($this->smarty);
		$topMenuRenderer->setCurrent($this->request->section);
		
		$this->smarty->assign("menuTop", $topMenuRenderer->render($this->topMenuBuilder));
		$this->smarty->assign("menuLeft", $this->menuLeftHTML);
		$this->smarty->assign("pageTitle", $this->pageTitle);
		$this->breadcrumbsRenderer->setItems($this->breadcrumbs);
		$this->smarty->assign("breadcrumbs", $this->breadcrumbs);
		$this->smarty->assign("breadcrumbsRenderer", $this->breadcrumbsRenderer);
		$this->smarty->assign("layoutScripts",$this->layoutScripts);
		$this->smarty->assign("layoutStyles", $this->layoutSlyles);
		
		require_once $this->config->bof->informers->src.'/InformerLoader.php';
		$this->informers = new InformerLoader($this->user, $this->config->bof->informers->src);
		$this->smarty->assign("informers",$this->informers);
	}
	
	public function enableLayout() {
		$this->useLayout = true;
	}
	public function disableLayout() {
		$this->useLayout = false;
	}
	protected function disableMenuOverwrite() {
		$this->disableMenuOverwrite = true;
	}
	protected function setLayout($layoutFileName) {
		$this->layoutFileName = $layoutFileName;
	}
	public function setPageTitle($title) {
		$this->pageTitle = $title;
	}
	public function setLeftMenuHTML($html) {
		if(!$this->disableMenuOverwrite) {
			$this->menuLeftHTML = $html;
		}
	}
	public function setBreadcrumbs($breadcrumbs) {
		$this->breadcrumbs = $breadcrumbs;
	}
	public function setBreadcrumbsRenderer($renderer) {
		$baseURL = preg_replace('#(.*?)/?$#', '$1', $this->baseURL);
		$this->breadcrumbsRenderer = $renderer;
		$this->breadcrumbsRenderer->setBaseURL($baseURL);
	}
	/**
	 * Добавить к странице JS файлы в тег <head>
	 * @param array|string $source - один или несколько URL до файлов с скриптами
	 */
	public function addLayoutScript($source) {
		if(!is_array($source)) {
			$source = array($source);
		}
		/* получим из source только те файлы, которые ещё не были добавлены в layoutScripts */
		$source = array_diff($source, $this->layoutScripts);
		$this->layoutScripts = array_merge($this->layoutScripts, $source);
		
	}
	/**
	 * Добавить один или несколько CSS файлов на страницу в тег <head>
	 * @param array|string $source - один или несколько URL CSS файлов
	 */
	public function addLayoutStyle($source) {
		if(!is_array($source)) {
			$source = array($source);
		}
		/* получим из source только те файлы, которые ещё не были добавлены в layoutSlyles */
		$source = array_diff($source, $this->layoutSlyles);
		$this->layoutSlyles = array_merge($this->layoutSlyles, $source);
	}
}

?>