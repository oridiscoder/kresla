<?php

require_once ('SuperController.php');
require_once APPLICATION_PATH . "/models/Authoriser.php";

class AuthController extends SuperController {
	
	public function indexAction() {
		$authData = array(
			'login' => null,
			'password' => null,
			'saveme' => 0
		);
		$authError = null;
		
		$authoriser = new Authoriser($this->dbAdapter, $this->config->auth->session_name);
		
		$authUser = $authoriser->getAuthorised();
		if($authUser) {
			header('Location: /');
		}
		
		if($this->request->isPost()) {
			$authData = $this->request->getPost();
			$authData['saveme'] = isset($authData['saveme']) ? $authData['saveme'] : 0;
									
			$user = $authoriser->authorise($authData['login'], $authData['password']);
			
			if(!$user) {
				$authError = "Неверный логин или пароль";
				$authoriser->logUserAuth($this->request->getServer("REMOTE_ADDR"), $authData['login'], false);
			} else {
				$authoriser->login($user, $authData['saveme']);
				$authoriser->logUserAuth($this->request->getServer("REMOTE_ADDR"), $authData['login'], true);
				$returnPath = $this->request->getPost("return_path");
				if(empty($returnPath)) {
					$returnPath = '/';
				}
				header("Location: $returnPath");
				die();
			}
			
		}
		
		$this->smarty->assign('auth', $authData);
		$this->smarty->assign('authError', $authError);
		$this->smarty->assign('return_path', $this->request->getParam("return_path"));
		$this->smarty->display("auth.html");
	}
	
	public function logoutAction() {
		$authoriser = new Authoriser($this->dbAdapter, $this->config->auth->session_name);
		$authoriser->logout();
		
		header("Location: ".$this->baseURL);
	}
	
	public function superloginAction() {
		$authoriser = new Authoriser($this->dbAdapter, $this->config->auth->session_name);
		$user = $authoriser->getAuthorised();
		if($user && $user->isSuperuser()) {
			if($this->request->isPost()) {
				$employeeID = $this->request->getPost("employee_id");
				if($employeeID) {					
					$userFactory = UserFactory::getInstance($this->dbAdapter);
					$user = $userFactory->getUserByEmployeeID($employeeID);
					$authoriser->login($user);
					header('Location: '.$this->request->getServer("HTTP_REFERER"));
					exit();
				}
			}
		}
	}
}

?>