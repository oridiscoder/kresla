<?php
require_once ('SuperController.php');

class ErrorController extends SuperController {
	private $errorTemplate = 'error.html';
	/**
	 *
	 * @var Exception
	 */
	private $exception;
	
	public function __construct($application, $exception = null) {
		parent::__construct($application);
		$this->exception = $exception;
		$this->user = Application::getAppUser();
		$this->smarty->assign("user", $this->user);
	}
	
	public function setException($ex) {
		$this->exception = $ex;
	}
	public function setErrorTemplate($filename) {
		$this->errorTemplate = $filename;
	}
	
	public function errorAction() {
		
		if($this->exception instanceof AccessException) {
			
			$content = $this->smarty->fetch("access-denied.html");
		} else {
			//die('404');
			if(isset($_COOKIE['tester']) && $_COOKIE['tester']=="magicmagic")
				$this->smarty->assign("exception", $this->exception);
			$content = $this->smarty->fetch($this->errorTemplate);
		}
		
		if($this->user && $this->useLayout) {
			echo $content;
		} else {
			echo $content;
		}
	}
	
	protected function mustAuthorise() {
		return false;
	}
}

?>