<?php

require_once ('SuperController.php');

class ClusterController extends SuperController {
	
	/**
	 *
	 * @var ClusterModuleAbstract
	 */
	private $clusterModule;
	
	
	public function indexAction() {
		
		$errContr = Application::getAppErrorController();
		$errContr->disableLayout();
		
		require_once APPLICATION_PATH . '/models/Clusters/Cluster.php';

		$this->smarty->setTemplateDir($this->config->clusters->template_dir);
		
		$clusterData = $this->request->cluster_data;
		
		$cluster = new Cluster($this->request->cluster);
		$current = $cluster;
		while(($parentID = $current->getParentID()) != false) {
			$parent = Cluster::get($parentID, $this->dbAdapter);
			if($parent) {
				$parent->setData($clusterData);
				$current->setParent($parent);
				$current = $parent;
			} else {
				break;
			}
		}
		
		$urlData = array();
		foreach ($clusterData as $row) {
			if(is_array($row)) {
				$urlData[] = $row;
			}
		}
		$cluster->setData($urlData);
		
		$moduleName = $cluster->getModuleName();
		$module = $this->loadClusterModule($moduleName);
		$module->setCluster($cluster)
			   ->setRequest($this->request)
			   ->setSmarty($this->smarty)
			   ->setDbAdapter($this->dbAdapter);
		
		$content = $module->render();
		
		$breadcrumbs = $module->getBreadcrumbs();
		$meta = $module->getMeta();
		
		$this->smarty->assign(array(
			'meta' => $meta,
			'breadcrumbs' => $breadcrumbs,
			'headScripts' => $module->getHeadScripts(),
			'headStyles' => $module->getHeadStyles(),
			'content' => $content
		));
		
		echo $this->smarty->fetch("layout.html");
	}
	
	/**
	 *
	 * @param string $moduleName
	 * @return ClusterModuleAbstract
	 */
	private function loadClusterModule($moduleName) {
		
		$className = ucfirst($moduleName);
		
		$fileName = $className . '.php';
		
		$path = APPLICATION_PATH . '/models/Clusters';
		
		if(!file_exists($path. '/' . $fileName)) {
			throw new ApplicationException("Module filename $fileName not found.");
		}
		
		require_once $path. '/' . $fileName;
		
		if(!class_exists($className)) {
			throw new ApplicationException("Module class $className not found.");
		}
		
		return new $className();
	}
}

?>