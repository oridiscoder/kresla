<?php
require_once ('SuperController.php');
require_once APPLICATION_PATH . '/models/Page/Page.php';

/**
 * Контроллер PagesController занимается обработкой разделов и страниц БОФа.
 * @author Gourry
 *
 */
class PagesController extends SuperController {
	
	public function indexAction() {
		
		$page = $this->getPage();
		$page->setBaseURL($this->baseURL.'/section');
		$page->setUser($this->user);
		
		$content = $page->renderPage();
		
		if(!$this->request->isXmlHttpRequest()) {
			
			$pageCSS = $page->getCSSFiles();
			$pageJS = $page->getJSFiles();
			
			$this->addLayoutStyle(array_merge(array(
					"/cms/css/table.old.css",
					"/cms/css/breadcrumbs.css"
			), $pageCSS));
			$this->addLayoutScript($pageJS);
			
			$this->setLeftMenuHTML($page->renderMenu());
			$this->setPageTitle($page->getPageTitle());
			$this->setBreadcrumbs($page->getBreadcrumbs());
			$this->setBreadcrumbsRenderer($page->getBreadcrumbsRenderer());			
		} else {
			header("Content-type: text/html; charset=utf-8");
			$this->disableLayout();
		}
		
		echo $this->wrap($content);
	}
	
	
	/**
	 * Метод-фабрика, который возвращает экземпляр класса страницы,
	 * в зависимости от типа страницы ($page['type']), указанного в БД.
	 * @return Page
	 * @throws PageTypeException
	 * @throws ApplicationException
	 */
	protected function getPage() {
		$sectionID = $this->request->getParam("section");
		$pageID = $this->request->getParam("page");

		if($sectionID) {
			if($pageID) {
				
				$page = Page::getPage($pageID, $this->dbAdapter);
				
				switch($page['type']) {
					case Page::TYPE_DBTABLE: {
						require_once APPLICATION_PATH . '/models/Page/DbTablePage.php';
						return new DbTablePage($page['id'], $this->dbAdapter, $this->smarty, $this->request);
					}
					case Page::TYPE_MODULE: {
						require_once APPLICATION_PATH . '/models/Page/ModulePage.php';
						return new ModulePage($page['id'], $this->dbAdapter, $this->smarty, $this->request);
					}
					default:
						throw new PageTypeException("Unknown page type ".$page['type']);
				}
			} else {
				require_once APPLICATION_PATH . '/models/Page/SectionPage.php';
				$sectionPage =  new SectionPage($sectionID, $this->dbAdapter, $this->smarty, $this->request);
				$defaultPageID = $sectionPage->getDefaultPageID($this->user->getId());
				if($defaultPageID) {
					$uri = $this->request->getPathInfo();
					header("Location: ".$uri.$defaultPageID.'/');
					exit();
				} else {
					return $sectionPage;
				}
				
			}
		} else {
			throw new ApplicationException("Section '$sectionID' not found");
		}
	}
}

?>