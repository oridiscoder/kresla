<?php

require_once "cms/library/Bof/Transliterator.php";
require_once "cms/library/Bof/Utils.php";
require_once "front/models/Di.php";
require_once "front/Autoloader.php";

class Patch {
	/**
	 * 
	 * @var Zend_Db_Adapter_Abstract
	 */
	private $db;
	
	/**
	 * 
	 * @var DbTableCreator
	 */
	private $tableFactory;
	
	private $errors;
	
	/**
	 * 
	 * @var \front\models\Di
	 */
	private $di;
	
	public function __construct() {
		
		$root = realpath(dirname(__FILE__));
		
		set_include_path(implode(PATH_SEPARATOR, array(
				$root . '/cms/application',
				realpath( $root . '/cms/library'),
				get_include_path(),
		)));
		
		require_once 'Zend/Loader/Autoloader.php';
		$autoloader = Zend_Loader_Autoloader::getInstance();
		
		$conf = new Zend_Config_Ini($root . '/cms/application/configs/database.ini', 'production', array('allowModifications' => true));
		$conf = $conf->merge(new Zend_Config_Ini($root . '/cms/application/configs/application.ini', 'production'));
		$this->db = Zend_Db::factory($conf->db);
		$this->db->query("SET NAMES utf8");
		
		require_once "models/Table/DbTableCreator.php";
		$this->tableFactory = new DbTableCreator($this->getDb());
		
		$autoloader = new \front\Autoloader();
		spl_autoload_register(array("\\front\\Autoloader", "autoload"));
		
        define("APPLICATION_PATH", $root . DIRECTORY_SEPARATOR . 'cms' . DIRECTORY_SEPARATOR . 'application');
		define("ROOT_PATH", $root);
		$this->di = \front\models\Di::getInstance();
		$this->di->register("config", $conf);
		$this->di->register("db", $this->db);
	}
	
	/**
	 * @return Zend_Db_Adapter_Abstract
	 */
	public function getDb() {
		return $this->db;
	}
	
	/**
	 * Выполнить sql команду
	 * @param string $sql
	 * @param array $bind - значения для вставки в sql
	 */
	public function runQuery($sql, $bind = null) {
		return $this->getDb()->query($sql, $bind);
	}
	
	/**
	 * @param string $tableName - имя таблицы, которую хотим утановить по-умолчанию (т.е. с ней сразу будем работать)
	 * @return DbTableCreator
	 */
	public function getTableFactory($tableName = null) {
		if($tableName) {
			$this->tableFactory->setTable($tableName);
		}
		return $this->tableFactory;
	}
	
	/**
	 * Импортировать .sql дамп в MySQL
	 * @param string $filename - полный путь к .sql файлу
	 * @return boolean - успешно или нет
	 */
	public function mysqlImport($filename) {
		$config = $this->db->getConfig();
		$username = $config['username'];
		$password = escapeshellarg($config['password']);
		$host = $config['host'];
		$dbname = $config['dbname'];
		
		$command = "mysql -h $host -u $username -p$password $dbname < $filename";
		
		$cmdOutput = "";
		$cmdResultCode = 0;
		exec($command, $cmdOutput, $cmdResultCode);
		
		if($cmdResultCode) {
			$this->logError("Error:\n".implode("\n", $cmdOutput));
			return false;
		} else {
			$this->log(basename($filename).": success!");
			return true;
		}
	}
	
	public function isInstalled($moduleName) {
		$moduleName = 'Module_'.$moduleName;
		$row = $this->getDb()->fetchRow("SELECT * FROM cms_modules WHERE name = ?", $moduleName);
		return $row ? $row : false;
	}
	
	/**
	 * 
	 * @param string $moduleName
	 * @return \front\modules\Module
	 */
	public function getModule($moduleName) {
		return $this->di->getModule($moduleName);
	}
	
	/**
	 * Установит модуль
	 * @param string $moduleName - название модуля
	 * @return boolean - удачно или нет
	 */
	public function installModule($moduleName) {
		$m = $this->getModule($moduleName);
		if($m->install()) {
			$this->getDb()->insert("cms_modules", array(
				'name' => $m->getName(), 
				'active' => true
			));
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Удалить модуль по имени
	 * @param string $moduleName - название модуля
	 * @return boolean
	 */
	public function removeModule($moduleName) {
		$m = $this->getModule($moduleName);
		if($m->remove()) {
			$this->getDb()->delete("cms_modules", array('name = ?' => $m->getName()));
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Импортировать данные БД (страницы, шаблоны, объекты) из XML файликов в БД
	 * @param $type - тип данных [templates|pages|objects]
	 */
	public function importXML($type = null) {
		
		$root = dirname(__FILE__);
		chdir($root);
		
		require_once "flusher.php";
		$f = new Flusher();
		if($type) {
			switch($type) {
				case 'templates':
					$f->importTemplates($root.'/db/templates');
					break;
				case 'pages':
					$f->importPages($root.'/db/pages');
					break;
				case 'objects':
					$f->importObjects($root.'/db/objects');
					break;
			}
		} else {
			$f->importTemplates($root.'/db/templates');
			$f->importPages($root. '/db/pages');
			$f->importObjects($root. '/db/objects');
		}
	}
	
	public function log($message) {
		$message .= "\n";
		echo $message;
	}
	
	public function logError($message) {
		$this->errors[] = $message;
		$this->log($message);
	}
	
}

?>