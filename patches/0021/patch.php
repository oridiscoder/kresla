<?php

require_once('Patch.php');

class MyPatch extends Patch {

	public function install() {
		$goods = $this->getDb()->fetchAll("SELECT id, title FROM kresla_goods order by id ASC");
		foreach($goods as $good){
			$name='';
			$name = $good['title'];
			$name = preg_replace('/[ \/]/', '_', $name);
			//$name = mb_strtolower($name, 'UTF-8');
			
			$name = $this->translit($name);
			$name = $this->str2url($name);
			//$name = urlencode($name);
			echo $good['title']." -> ".$name."\n";
			$this->getDb()->update('kresla_goods', array('URL'=>$name), "id =".$good['id']);
		}
	
	}
	
	public function toTranslit($string) {
		$string=strtr($string,"абвгдеёзийклмнопрстуфхъыэ",
				"abvgdeeziyklmnoprstufh’yei");
		$string=strtr($string,"АБВГДЕЁЗИЙКЛМНОПРСТУФХЪЫЭ",
				"ABVGDEEZIYKLMNOPRSTUFH’YEI");
		$string=strtr($string,
				array(
						"ж"=>"zh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh",
						"щ"=>"shch","ь"=>"", "ю"=>"yu", "я"=>"ya",
						"Ж"=>"ZH", "Ц"=>"TS", "Ч"=>"CH", "Ш"=>"SH",
						"Щ"=>"SHCH","Ь"=>"", "Ю"=>"YU", "Я"=>"YA",
						"ї"=>"i", "Ї"=>"Yi", "є"=>"ie", "Є"=>"Ye"
				)
		);
		return $string;
	}
	
	function str2url($str) {
		$str = strtolower($str);
		$str = preg_replace('~[^-a-z0-9]+~u', '_', $str);
		$str = trim($str, "_");
		return $str;
	}

	public function translit($str) {
    	$rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
   		$lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
   		return str_replace($rus, $lat, $str);
  	}
	


}


$p = new MyPatch();
$p->install();